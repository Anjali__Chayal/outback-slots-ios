﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Bitware.Utils;

public class TestScript : MonoBehaviour
{
	List<int> sprites = new List<int>{ 1, 2, 2, 10, 2 };
	// Use this for initialization
	void Start ()
	{
		GetMatchingIndexes ();
	}

	public void GetMatchingIndexes ()
	{
		var spriteToMatch = sprites [0];
		var matchedIndexes = new List<int> ();
		for (int i = 1; i < sprites.Count; i++) {

			if (sprites [i] == sprites [i - 1]) {
				if (i == 1) {
					matchedIndexes.Add (0);
				}
				matchedIndexes.Add (i);
				Debug.Log ("Adding " + sprites [i] + " as same at " + i + " and " + (i - 1));
			} else {
				if (spriteToMatch == 10) {
					if (sprites [i - 1] == 10) {
						if (i == 1) {
							matchedIndexes.Add (0);
						}
						matchedIndexes.Add (i);
						spriteToMatch = sprites [i];
						Debug.Log ("spriteToMatch " + spriteToMatch + " at " + i);
					} else {
						
						Debug.Log ("Breaking at " + sprites [i] + " as " + (i - 1) + " is not 10 and " + sprites [i - 1] + " is not equal to " + i);
						break;

					
					}
				} else {
					if (sprites [i] == 10) {
						if (i == 1) {
							matchedIndexes.Add (0);
						}
						matchedIndexes.Add (i);
					} else {
						if (sprites [i] == spriteToMatch) {
							if (i == 1) {
								matchedIndexes.Add (0);
							}
							matchedIndexes.Add (i);

						} else {
							Debug.Log ("Breaking at " + i);
							break;
						}
					
					}
				}
			}


//			if ((sprites [i] == spriteToMatch) || (sprites [i] == 10)) {
//				if (i == 1)
//					matchedIndexes.Add (0);
//				if (sprites [i - 1] == sprites [i]) {
//					matchedIndexes.Add (i);
//				} 
//				if (sprites [i] == 10) {
//					matchedIndexes.Add (i);
//				} 
//
//
//			} else {
//				if (sprites [i] == 10) {
//					matchedIndexes.Add (i);
//				} else {
//					if (((sprites[i] != spriteToMatch)&&(spriteToMatch != 10))||(sprites [i - 1] == 10) && (sprites [i] != 10)) {
//						matchedIndexes.Add (i);
//						if (i == 1)
//							matchedIndexes.Add (0);
//						//					matchedIndexes = matchedIndexes.FindAll (x => x == 10).ConvertAll (x => i).ToList ();
//						spriteToMatch = sprites [i];
//					}
//
//				}
//			
//			}
		

		}

		matchedIndexes = matchedIndexes.Distinct ().ToList ();
	
		Debug.Log ("Matched " + spriteToMatch + " at indexes " + matchedIndexes.Print ());
	}

}
