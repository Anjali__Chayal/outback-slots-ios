﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Linq;
using Sdkbox;
using Outback.Core;
using Outback.Managers;
using Bitware.Utils;

public class PurchaseHandler : MonoBehaviour
{
	public Text messageText;
	private Sdkbox.IAP _iap;
	private String mLogBuffer;
	private const int MAX_LOG_LINE = 5;

	// Use this for initialization
	void Start()
	{
		_iap = FindObjectOfType<Sdkbox.IAP>();
		if (_iap == null)
		{
			log("Failed to find IAP instance");
		}
	}

	public void getProducts()
	{
		if (_iap != null)
		{
			log ("About to getProducts, will trigger onProductRequestSuccess event");
			_iap.getProducts ();
		}
	}

	public void Purchase(string item)
	{
		if (_iap != null)
		{
			log("About to purchase " + item);
			_iap.purchase(item);
		}
	}

	public void Refresh()
	{
		if (_iap != null)
		{
			log("About to refresh");
			_iap.refresh();
		}
	}

	public void Restore()
	{
		if (_iap != null)
		{
			log("About to restore");
			_iap.restore();
		}
	}

	private void log(string msg)
	{
		mLogBuffer += msg;
		mLogBuffer += Environment.NewLine;
		int numLines = mLogBuffer.Split('\n').Length;
		if(numLines > MAX_LOG_LINE){
			string[] lines = mLogBuffer.Split(Environment.NewLine.ToCharArray()).Skip(numLines - MAX_LOG_LINE).ToArray();
			mLogBuffer = string.Join(Environment.NewLine, lines);
		}
			
		if (messageText)
		{
			messageText.text = mLogBuffer;
		}

		Debug.Log(msg);
	}

	//
	// Event Handlers
	//

	public void onInitialized(bool status)
	{
		log("Init " + status);
	}

	public void onSuccess(Product product)
	{
		Debug.Log("IAPScript.onSuccess: " + product.name);
		if (product.name == "Product100K") {
			//Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			Player.CurrentCoins += 100000;

			// The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
			//ScoreManager.score += 100;  //Commented this code to remove error
		}   else if (product.name == "Product400K") {
			//Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			Player.CurrentCoins += 400000;

			// TODO: The non-consumable item has been successfully purchased, grant this item to the player.
		}   else if (product.name ==  "Product2M") {
			//Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			Player.CurrentCoins += 2000000;
			// TODO: The subscription item has been successfully purchased, grant this to the player.
		}   else if (product.name == "Product8M") {
			//Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			Player.CurrentCoins += 8000000;
			// TODO: The non-consumable item has been successfully purchased, grant this item to the player.
		}   else if (product.name == "Product25M") {
			//Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			Player.CurrentCoins += 25000000;
			// TODO: The subscription item has been successfully purchased, grant this to the player.
		}    else {


			Debug.Log ("Else part of the ProcessPurchase Class is  called PurchaseProcessingResult");
			//Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
		}

		// Return a flag indicating whether this product has completely been received, or if the application needs 
		// to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
		// saving purchased products to the cloud, and when that save is delayed. 
		if (LobbyController.instance == null) {
			var creditsInGame = GameObject.FindObjectOfType<SlotGameManager> ().credits;
			var newCredits = Player.CurrentCoins;
			creditsInGame.text = TextFormatter.GetFormattedNumberText (Player.CurrentCoins, true);

		}   else {
			LobbyController.instance.coinsText.text = TextFormatter.GetFormattedNumberText (Player.CurrentCoins, true);
		}
		PlayerManager.ME.UpdateUsertable ("current_coins");
		log("onSuccess: " + product.name);
	}

	public void onFailure(Product product, string message)
	{
		log("onFailure " + message);
	}

	public void onCanceled(Product product)
	{
		log("onCanceled product: " + product.name);
	}

	public void onRestored(Product product)
	{
		log("onRestored: " + product.name);
	}

	public void onProductRequestSuccess(Product[] products)
	{
		foreach (var p in products)
		{
			log("Product: " + p.name + " price: " + p.price);
		}
	}

	public void onProductRequestFailure(string message)
	{
		log("onProductRequestFailure: " + message);
	}

	public void onRestoreComplete(string message)
	{
		log("onRestoreComplete: " + message);
	}
}
