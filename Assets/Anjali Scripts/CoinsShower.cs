﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using Bitware.Utils;

public class CoinsShower : MonoBehaviour
{

	public GameObject coinPrefab;

	public static CoinsShower instance;

	void Awake ()
	{
		instance = this;
	}
	// Use this for initialization
	void Start ()
	{
//		StartAnim ();

	}
	
	// Update is called once per frame
	public void StartAnim ()
	{
		for (int x = 0; x < 200; x++) {
			var coin = Instantiate (coinPrefab, transform, false) as GameObject;
			var r = Random.Range (-500, 500);
			coin.transform.localPosition = new Vector3 (r, 0, 0);

		}

		foreach (var coin in transform.GetComponentsInChildren <Image>().ToList<Image> ()) {
			var r = Random.Range (1, 3f);
			var a = 0f;

			DOTween.To (x => a = x, 0f, 12f, r).OnComplete (() => {
				coin.transform.DOLocalMoveY (-1500f, 2f, false).OnComplete (() => {
					Destroy (coin.gameObject);
				});
			
			});

		}
	}
}
