﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Facebook.Unity;
using Outback.Managers;


public class SettingsPanelController : MonoBehaviour
{
	public Button musicbutton;
	public Sprite soundOffImage;
	public Sprite soundOnImage;
	private Color colorfade = Color.white;
	public Button logoutButton;
	public Button promocodeButton;
	private Vector2 promobuttonInitPos;


	public static SettingsPanelController instance;

	void Awake ()
	{
		promobuttonInitPos = promocodeButton.transform.localPosition;
		instance = this;
	}

	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void CallPromoScreen ()
	{
		ScreenManager.instance.promocodePanel.SetActive (true);
		ScreenManager.instance.settingPanel.SetActive (false);
	}

	public void ClosePanel ()
	{
		gameObject.SetActive (false);
	}

	public void OpenPanel ()
	{
		gameObject.SetActive (true);
	}

	//Sound ON OFF Method
	public void SoundOnOFFMethod ()
	{
//		var audiosources = GameObject.FindObjectsOfType<AudioSource> ();


		if (SoundManager.instance.musicFlag) {
			//Image1 
			musicbutton.GetComponent<Image> ().sprite = soundOffImage;
			SoundManager.instance.musicFlag = false;

		} else if (!SoundManager.instance.musicFlag) {
			musicbutton.GetComponent<Image> ().sprite = soundOnImage;
			SoundManager.instance.musicFlag = true;
		}
		Debug.Log (" sound " + SoundManager.instance.musicFlag);

//		foreach (var audio in audiosources) {
//			audio.enabled = SoundManager.instance.musicFlag;
//
//		}

		// Turn Music On Off in the Game 
	}

	//LogOut Method
	public void LogOutMethod ()
	{
		//	LogOutMethod
		string UserIDSaved = null;
		string strno = @"NO";
		string facebook_id = null;
		PlayerPrefs.SetString ("logflag", strno);
		PlayerPrefs.SetString ("facebook_ids", facebook_id);
		PlayerPrefs.SetString ("UserIDSaved", UserIDSaved);
		FB.LogOut ();
		colorfade = new Color (0.5F, 0.5F, 0.5F, 1.0F);
		Initiate.Fade ("LoginScene", colorfade, 0.5f);
		Debug.Log ("LogOutMethod");
	}


	//TermsOfPolicy Method
	public void TermsOfServiceMethod ()
	{
		//	TermsOfServiceMethod
		Debug.Log ("TermsOfServiceMethod");
		Application.OpenURL ("http://103.224.243.154/PHP/webservices/outblackslotlive/users/termandcondition");
	}

	public void HideLogoutButton ()
	{
		logoutButton.gameObject.SetActive (false);
		promocodeButton.transform.localPosition = promobuttonInitPos + new Vector2 (170, 0);
	}

	public void ShowLogoutButton ()
	{
		logoutButton.gameObject.SetActive (true);
		promocodeButton.transform.localPosition = promobuttonInitPos;
	}

	//PrivacyPolicy Method
	public void PrivacyPolicyMethod ()
	{
		//PrivacyPolicyMethod
		Debug.Log ("PrivacyPolicyMethod");
		//Application.OpenURL(@"googlechrome://www.google.com");
		Application.OpenURL ("http://103.224.243.154/PHP/webservices/outblackslotlive/users/privacypolicy");
	}

}
