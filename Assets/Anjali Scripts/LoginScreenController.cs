﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Outback.Core;
using Outback.Managers;
using UnityEngine.UI;
using Outback.Views;


public class LoginScreenController : MonoBehaviour
{
	public static LoginScreenController instance;

	public GameObject detailsPanel;
	public GameObject guestButton, fbLoginButton;
	public InputField email;
	public InputField password;
	//	public InputField mobileNumber;

	private void Awake ()
	{
		instance = this;
	}

	// Use this for initialization
	void Start ()
	{
		HideAllPanels ();
//		ScreenManager.instance.loadingScreenObj.SetActive (true);
	}

	public void HideAllPanels ()
	{
		ScreenManager.instance.loadingScreenObj.SetActive (false);
		ScreenManager.instance.promocodePanel.SetActive (false); 
		ScreenManager.instance.settingPanel.SetActive (false);
		ScreenManager.instance.iapPanel.SetActive (false);
		ScreenManager.instance.playerDetailsPanel.SetActive (false);
		ScreenManager.instance.errorPanel.SetActive (false);
	}
	
	// Update is called once per frame
	public void UpdateDetailsPanel ()
	{
		// Check if email is nul
		if (Player.Email != null) {
			if (Player.Email != string.Empty) {
				email.text = Player.Email;
				//			email.placeholder.gameObject.SetActive (false);
				email.interactable = false;
			}
		} 

        
        
	}

	public void UpdatePlayerDetails ()
	{

//        if (mobileNumber.text == string.Empty || password.text == string.Empty || email.text == string.Empty)
		if (password.text == string.Empty || email.text == string.Empty) {
		} else {
           
		
			if (email.text != string.Empty) {
				Player.Email = email.text;
				if (password.text != string.Empty) {
					char[] SpecialChars = "!@#$%^&*();{},.<>/:;".ToCharArray ();
					int indexOf = password.text.IndexOfAny (SpecialChars);
					if (indexOf == -1) {
						// No special chars
						Player.password = password.text;
						PlayerManager.ME.PlayAsFBPlayer ();
						Debug.Log ("Email : " + Player.Email + " : "
							//				+ " Mobile Number : " + Player.mobileNumer
						+ " Password " + Player.password);
					} else {
						ErrorPanel.instance.ShowMessage ("Password cannot contain !@#$%^&*();{},.<>/:;", false);
						password.text = string.Empty;
					}
				}

				
			} else if (password.text == string.Empty || email.text == string.Empty) {
				ErrorPanel.instance.ShowMessage ("Please fill all the details to progress further", false);
			}
		
//            Player.mobileNumer = long.Parse(mobileNumber.text);

		}

	}

     
}
