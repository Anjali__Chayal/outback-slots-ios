﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;
using UnityEngine.UI;
using Outback.Core;
using Outback.Managers;
using Bitware.Utils;


public class PromoCodePanelController : MonoBehaviour
{
	public Text promoCodeText;

	public GameObject wonBonusGO;
	public Text won_coinText;
	public Text responseText;
	// Use this for initialization
	private void Awake ()
	{
		CloseBonusWinPanel (); 
	}

	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}


	public void CallPromoScreen_Cancel_Btn ()
	{
		//promocodeimg; 
		ScreenManager.instance.settingPanel.SetActive (true);
		ScreenManager.instance.promocodePanel.SetActive (false);
	}

	public void CallPromoScreen_OK_Btn ()
	{
		StartCoroutine (CallPromoCodeService ());
		StopCoroutine (CallPromoCodeService ());
	}

	IEnumerator CallPromoCodeService ()            //Do registration for new user. 
	{
		// URL : http://103.224.243.154/PHP/webservices/outblackslotlive/api/savepromocode
		// URL : http://103.224.243.154/PHP/webservices/outblackslotlive/api/getpromocodefromuser      //Latest Service
		//	PARAMETER : facebook_id , promocode , userid , update_count

		string highscore_url = FBHolder.BaseURL + "promoupdatecoins";

		//string  highscore_url = "http://103.224.243.154/PHP/webservices/outblackslotlive/api/savepromocode";

//		string update_count = "150000";

		string promocode = promoCodeText.text;
		// Create a form object for sending high score data to the server
		WWWForm form = new WWWForm ();
		// The userid
		form.AddField ("userid", Player.UserId);
		// The promocode
		form.AddField ("promocode", promocode);

		// Create a download object
		WWW download = new WWW (highscore_url, form);      // Registration of new user using facebook credentials

		// Wait until the download is done
		yield return download;

		Debug.Log ("CAlled PromoCode ");

		if (!string.IsNullOrEmpty (download.error)) {
			print ("registration Error downloading: " + download.error);
		} else {
			print ("Called else ");

			string json = MiniJSON.JSON.Serialize (download.text);
			var data = MiniJSON.JSON.Deserialize (download.text) as Dictionary<string,object>;
			Debug.Log ("dict" + json);
			// {"response_code":"success","response_message":"Your coins are updated successfully","userid":"14","current_coins":124551078}
			// {"response_code":"success","response_message":"This promo code has been already used!","userid":"14"}
			// {"response_code":"success","response_message":"Your coins are updated successfully","userid":"14","current_coins":248395843,"won_coin":"333333"}
			// {"response_code":"fail","response_message":"This promo code may be wrong!","userid":"14"}
			// Promo code to use coins51
			string responsecode = data ["response_code"].ToString ();
			string responsetext = data ["response_message"].ToString ();
			if (responsecode.ToString ().Equals ("success")) {
				var won_coin = long.Parse (data ["won_coin"].ToString ());
				Player.CurrentCoins += won_coin;
				responsecode = data ["response_code"].ToString ();
				responseText.text = responsetext; 
				won_coinText.text = won_coin.ToString ();   //data["response_code"].ToString();
				if (LobbyController.instance != null) {
					LobbyController.instance.coinsText.text = TextFormatter.GetFormattedNumberText (Player.CurrentCoins, true);
				}
				OpenBonusWinPanel ();
				Invoke ("CloseBonusWinPanel", 2f);

              
			} else {

				string str_message = data ["response_message"].ToString ();
				print (str_message);
				float delay = 2.0f;
				responseText.text = str_message;
				yield return new WaitForSeconds (delay);
				//ResponseText.setInactive (false);
				responseText.text = responsetext; 
				won_coinText.text = "";   //data["response_code"].ToString();
				OpenBonusWinPanel ();
				Invoke ("CloseBonusWinPanel", 2f);
				//	StartCoroutine(ShowMessage("Abc efghijkl mnopqrs tuvwxy", 2));
			}
		}
	}

	public void OpenBonusWinPanel ()
	{
		wonBonusGO.SetActive (true);
		ScreenManager.instance.promocodePanel.SetActive (false);
	}

	public void CloseBonusWinPanel ()
	{
		Debug.Log ("Close");
		wonBonusGO.SetActive (false);
	}

}
