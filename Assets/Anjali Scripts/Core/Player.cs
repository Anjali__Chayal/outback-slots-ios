﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Outback.Managers;
using Outback.Core;

namespace Outback {
	namespace Core {
		[System.Serializable]

public class Player : MonoBehaviour
{
	private static string userId = "";
	private static string machineId = "";
    private static string userName;
//	public static string country;
	private static	string email;
	private static long currentCoins = 0;
	private static  float spentCoins = 0;
	private static  float level = 0;
//    public static long mobileNumer = 0;
     public static string password = "";
      

        public static long CurrentCoins{
		get { return currentCoins; }
		set {
                    currentCoins = value;
                    PlayerPrefs.SetInt("Current_Coins", int.Parse(value.ToString()));
                }
	}
	public static string UserName{
		get { return userName; }
		set { userName = value; }
	}
	public static string MachineId{
		get { return machineId; }
		set { machineId = value; }
	}

	public static string UserId{
		get { return userId; }
		set { userId = value; }
	}
	public static string Email{
		get { return email; }
		set { email = value; }
	}
	public static float SpentCoins{
		get { return spentCoins; }
		set { spentCoins = value; }
	}
	public static float Level{
		get { return level; }
		set { level = value; }
	}


	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}
        }
    }
}
