﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Outback.Core;
using Outback.Managers;
using UnityEngine.UI;
using Outback.Views;
using DG.Tweening;
using Bitware.Utils;

namespace Outback
{
	namespace Core
	{
		public class Reward : MonoBehaviour
		{
			public string reward;
			public int rewardMultiplier;
			public long bonusReward;
			public Sprite twoTimes;
			public Sprite threeTimes;
			public Sprite fiveTimes;
			public int rewardNumber;
			public List<long> bonusRewards;
			public Text rewardAmount;
			public Vector3 rewardPos;
			public bool threeMultipliers, twoMultipliers;
			public Sprite originalImage;

			public GameObject rewardTextPosRef;
			//			public Text RewardAmoutText;
			// Use this for initialization

			void Start ()
			{
				if (reward == Rewards.Bonus.ToString ())
					rewardPos = rewardAmount.transform.position;
			}

			public void SetValues ()
			{
			
				if (reward == Rewards.FreeSpin.ToString ()) {
					var r = Random.Range (0, 100);
					var x = 0;
					if (twoMultipliers) {
						if (r < 50) {
							x = 0;
						} else {
							x = 2;
						}
					} else if (threeMultipliers) {
						if (r < 25) {
							x = 0;
						} else if (r >= 25 && r < 75) {
							x = 1;
						} else {
							x = 2;
						}
					}
					switch (x) {
					case 0:
						{

							rewardMultiplier = 2;
							break;
						}
					case 1:
						{
							rewardMultiplier = 3;
							break;

						}
					case 2:
						{
							rewardMultiplier = 5;
							break;

						}
					}
				} else if (reward == Rewards.Bonus.ToString ()) {
					var r = Random.Range (0, bonusRewards.Count);
					bonusReward = bonusRewards [r];
				}
			}
	
			// Update is called once per frame
			public void GiveReward ()
			{
				GiveRewards();
			}

			void GiveRewards ()
			{
				
				if (reward == Rewards.FreeSpin.ToString ()) {
					var amntWon = SlotGameManager.baseGame.freeSpinWinnings;
					var newAmount = amntWon * rewardMultiplier;
					Debug.Log ("Amount won " + amntWon + " new Amount " + newAmount);

					Player.CurrentCoins = Player.CurrentCoins + newAmount;
					SlotGameManager.baseGame.currentCoins.text = TextFormatter.GetFormattedNumberText (Player.CurrentCoins, true);
					SlotGameManager.baseGame.winValue.text = TextFormatter.GetFormattedNumberText ((newAmount),true);
					PlayerManager.ME.UpdateUsertable ("current_coins");
					switch (rewardMultiplier) {
					case 2:
						{
							transform.GetComponent <Button> ().image.sprite = twoTimes;
							break;
						}
					case 5:
						{
							transform.GetComponent <Button> ().image.sprite = fiveTimes;
							break;
						}
					case 3:
						{
							transform.GetComponent <Button> ().image.sprite = threeTimes;
							break;
						}
					}
					SlotGameManager.baseGame.HideGameRewardsExcept (rewardNumber);


					var a = 0;
					DOTween.To (x => x = a, a, 2f, 2f).OnComplete(()=> {
						foreach (var reward in SlotGameManager.baseGame.rewards) {
							reward.gameObject.SetActive (true);
						}
							SlotGameManager.baseGame.freeSpinRewardPopUp.SetActive (false);

						SlotGameManager.baseGame.rewardPopUp.SetActive (true);
						CoinsShower.instance.StartAnim ();
						RewardPanel.instance.ShowMessage (amntWon, newAmount);
						transform.GetComponent <Button> ().image.sprite = originalImage;
						SlotGameManager.baseGame.freeSpinWinnings = 0;
						foreach (var btn in SlotGameManager.instance.buttonsToDisableWhileSpinning) {
//							Debug.Log("Turning on btns as awarding free spins");
							btn.interactable = true;
						}
						SlotGameManager.baseGame.spinBtn.interactable = true;

						SlotGameManager.baseGame.stopBtn.interactable = true;

					});

//					var d = 0;
//					DOTween.To (x => x = d, d, 5f, 5f).OnComplete(()=> {
//
//						//						bonusReward = 0;
//					});
						//						yield return new WaitForSeconds (2f);
						
				
//					ErrorPanel.instance.ShowMessage (string.Format ("Congratulations ! You had won x{0} coins reward.\\n Your earlier winnings were {1}.\\n And now they are {2} ", rewardMultiplier, amntWon, newAmount));


				} else if (reward == Rewards.Bonus.ToString ()) {
					GetComponent <Button> ().interactable = false;
					var amntWon = SlotGameManager.baseGame.winAmount;
					var newAmount = amntWon + bonusReward;
					Player.CurrentCoins = Player.CurrentCoins + newAmount;
					SlotGameManager.baseGame.currentCoins.text = TextFormatter.GetFormattedNumberText (Player.CurrentCoins, true);
					SlotGameManager.baseGame.winValue.text = TextFormatter.GetFormattedNumberText ((amntWon + bonusReward),true);
					PlayerManager.ME.UpdateUsertable ("current_coins");
					SlotGameManager.baseGame.HideGameRewardsExcept (rewardNumber);
					rewardAmount.text = TextFormatter.GetFormattedNumberText (bonusReward, true);
					rewardAmount.transform.DOMove (rewardTextPosRef.transform.position, 1f, false).SetEase (Ease.OutSine);
					rewardAmount.transform.DOScale (Vector3.one, 1f);

//
					var a = 0;
					DOTween.To (x => x = a, a, 2f, 2f).OnComplete (() => {
//						yield return new WaitForSeconds (2f);
						SlotGameManager.baseGame.rewardPopUp.SetActive (true);
						SlotGameManager.baseGame.UnHideGameRewards ();
						SlotGameManager.baseGame.bonusGamePanel.SetActive (false);
						CoinsShower.instance.StartAnim ();
						RewardPanel.instance.ShowMessage (amntWon, newAmount);
//						bonusReward = 0;
					});
//					yield return new WaitForSeconds (5f);
//				
//					SlotGameManager.baseGame.bonusGamePanel.SetActive (false);

//					ScreenManager.instance.playerDetailsPanel.SetActive (true);
				}
				foreach (var btn in SlotGameManager.instance.buttonsToDisableWhileSpinning) {
//					Debug.Log("Turning on btns as awarding bonus");
					btn.interactable = true;
				}
				SlotGameManager.baseGame.spinBtn.interactable = true;

				SlotGameManager.baseGame.stopBtn.interactable = true;
			}



		}



		public enum Rewards
		{
			Bonus,
			FreeSpin
		}
	}
}