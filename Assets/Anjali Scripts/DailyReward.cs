﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Outback.Core;
using Bitware.Utils;
using Outback.Managers;

public class DailyReward : MonoBehaviour
{
	//UI
	public Text timeLabel;
	//only use if your timer uses a label
	public Button timerButton;
	//used to disable button when needed
	//	public Image _progress;
	//TIME ELEMENTS
	public int hours;
	//to set the hours
	public int minutes;
	//to set the minutes
	public int seconds;
	//to set the seconds
	public bool _timerComplete = false;
	public bool _timerIsReady;
	private TimeSpan startTime;
	private TimeSpan endTime;
	private TimeSpan remainingTime;
	//progress filler
	private float _value = 1f;
	//reward to claim
	public static int RewardToEarn;

	public GameObject spinWheel;



	//startup
	public void CheckSpinWheel ()
	{
//		Debug.Log ("timer " + PlayerPrefs.GetString ("_timer"));
		if (PlayerPrefs.GetString ("_timer") == "") { 
//			Debug.Log ("==> Enabling button");
			EnableSpinWheel ();
		} else {
			DisableSpinWheel ();
			StartCoroutine ("CheckTime");
		}
	}



	//update the time information with what we got some the internet
	private void UpdateTime ()
	{
		if (PlayerPrefs.GetString ("_timer") == "Standby") {
			PlayerPrefs.SetString ("_timer", TimeManager.sharedInstance.GetCurrentTimeNow ());
			PlayerPrefs.SetInt ("_date", TimeManager.sharedInstance.GetCurrentDateNow ());
		} else if (PlayerPrefs.GetString ("_timer") != "" && PlayerPrefs.GetString ("_timer") != "Standby") {
			int _old = PlayerPrefs.GetInt ("_date");
			int _now = TimeManager.sharedInstance.GetCurrentDateNow ();


			//check if a day as passed
			if (_now > _old) {//day as passed
//				Debug.Log ("Day has passed");
				EnableSpinWheel ();
				return;
			} else if (_now == _old) {//same day
//				Debug.Log ("Same Day - configuring now");
				ConfigTimerSettings ();
				return;
			} else {
//				Debug.Log ("error with date");
				return;
			}
		}
//		Debug.Log ("Day had passed - configuring now");
		ConfigTimerSettings ();
	}

	//setting up and configureing the values
	//update the time information with what we got some the internet
	private void ConfigTimerSettings ()
	{
		startTime = TimeSpan.Parse (PlayerPrefs.GetString ("_timer"));
		endTime = TimeSpan.Parse (hours + ":" + minutes + ":" + seconds);
		TimeSpan temp = TimeSpan.Parse (TimeManager.sharedInstance.GetCurrentTimeNow ());
		TimeSpan diff = temp.Subtract (startTime);
		remainingTime = endTime.Subtract (diff);
		//start timmer where we left off
		SetProgressWhereWeLeftOff ();

		if (diff >= endTime) {
			_timerComplete = true;
			EnableSpinWheel ();
		} else {
			_timerComplete = false;
			DisableSpinWheel ();
			_timerIsReady = true;
		}
	}

	//initializing the value of the timer
	private void SetProgressWhereWeLeftOff ()
	{
		float ah = 1f / (float)endTime.TotalSeconds;
		float bh = 1f / (float)remainingTime.TotalSeconds;
		_value = ah / bh;	
		timeLabel.text = remainingTime.ToString ();
//		_progress.fillAmount = _value;
	}



	//enable button function
	private void EnableSpinWheel ()
	{
		spinWheel.gameObject.SetActive (true);

		SpinWheelController.instance.OpenPopUp ();
		timerButton.interactable = true;
//		timeLabel.text = remainingTime.ToString ();
//		timeLabel.text = "CLAIM REWARD";
	}



	//disable button function
	private void DisableSpinWheel ()
	{

		SpinWheelController.instance.ClosePopUp ();

		timerButton.interactable = false;
//		timeLabel.text = remainingTime.ToString ();
	}


	//use to check the current time before completely any task. use this to validate
	private IEnumerator CheckTime ()
	{
		DisableSpinWheel ();
		timeLabel.text = remainingTime.ToString ();
//		Debug.Log ("==> Checking for new time");
		yield return StartCoroutine (
			TimeManager.sharedInstance.GetTime ()
		);
		UpdateTime ();
//		Debug.Log ("==> Time check complete!");

	}


	//trggered on button click
	public void RewardClicked ()
	{
//		Debug.Log ("==> Claim Button Clicked");
		ClaimReward (RewardToEarn);
		PlayerPrefs.SetString ("_timer", "Standby");
		StartCoroutine ("CheckTime");

	}



	//update method to make the progress tick
	void Update ()
	{
//		if (TimeManager.sharedInstance.GetCurrentTimeNow () != null) {
//
//		}

		if (_timerIsReady) {

			timeLabel.text = remainingTime.ToString ();

			if (!_timerComplete && PlayerPrefs.GetString ("_timer") != "") {
				StartCoroutine (TimeManager.sharedInstance.GetTime ());
				UpdateTime ();
				_value -= Time.deltaTime * 1f / (float)endTime.TotalSeconds;
//				_progress.fillAmount = _value;
				//this is called once only
				if (_value <= 0 && !_timerComplete) {
					//when the timer hits 0, let do a quick validation to make sure no speed hacks.
					ValidateTime ();
					_timerComplete = true;
				}
			} 
		} 
	}



	//validator
	private void ValidateTime ()
	{
//		Debug.Log ("==> Validating time to make sure no speed hack!");
		StartCoroutine ("CheckTime");
	}


	private void ClaimReward (int x)
	{
		Player.CurrentCoins += long.Parse (x.ToString ());
		LobbyController.instance.coinsText.text = TextFormatter.GetFormattedNumberText (Player.CurrentCoins, true);
		PlayerManager.ME.UpdateUsertable ("current_coins");
		Debug.Log ("YOU EARN " + x + " REWARDS");
	}

}