﻿using System.Collections.Generic;
using System;

namespace Bitware {
	namespace Utils {
		public static class BetterArray {




			/// <summary>
			/// Initialize an array with the default specified value.
			/// </summary>
			/// <param name="array">Array.</param>
			/// <param name="value">Value.</param>
			/// <typeparam name="T">The 1st type parameter.</typeparam>
			public static T[] Initialize<T> (this T[] array, T value) {
				for(int i = 0; i < array.Length; i++)
					array[i] = value;
				return array;
			}

			/// <summary>
			/// Initialize an array with the default specified value.
			/// </summary>
			/// <param name="array">Array.</param>
			/// <param name="value">Value.</param>
			/// <typeparam name="T">The 1st type parameter.</typeparam>
			public static T[,] Initialize<T> (this T[,] array, T value) {
				for(int i = 0; i < array.GetLength(0); i++)
					for(int j = 0; j < array.GetLength(1); j++)
						array[i, j] = value;
				return array;
			}


			public static T Find<T> (this T[,] array, Predicate<T> match)
			{
				if (array == null)
					throw new ArgumentNullException ("array");
				if (match == null)
					throw new ArgumentNullException ("match");

				for (int i = array.GetLowerBound(0); i < array.GetUpperBound(0); i++) {
					for(int j = array.GetLowerBound(1); j < array.GetUpperBound(1); j++) {
						if (match(array[i, j]))
							return array[i, j];
					}

				}
				return default(T);
			}


			/// <summary>
			/// Check if all elements in a list are the same by using the .Equals method.
			/// </summary>
			/// <returns><c>true</c>, if all elements the same was ared, <c>false</c> otherwise.</returns>
			/// <param name="list">List.</param>
			/// <typeparam name="T">The 1st type parameter.</typeparam>
			public static bool AreAllElementsTheSame<T> (this List<T> list) {
				if(list == null)
					return true;
				if(list.Count == 0)
					return true;
				foreach(T element in list) {
					if(!element.Equals(list[0]))
						return false;
				}

				return true;
			}


			//Does not work properly.
			//The output array here is modified, but the actual passed array does not get modified
			//	public static void DeepCopy<T> (this T[,] inputArray, T[,] outputArray) where T : ICloneable<T> {
			//		outputArray = new T[inputArray.GetLength(0), inputArray.GetLength(1)];
			//		for(int i = 0; i < inputArray.GetLength(0); i++) {
			//			for(int j = 0; j < inputArray.GetLength(1); j++) {
			//				outputArray[i, j] = inputArray[i, j].Clone();
			//				Debug.Log(outputArray[i, j]);
			//			}
			//		}
			//	}
		}

	}
}