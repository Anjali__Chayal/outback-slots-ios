using Random = UnityEngine.Random;
using UnityEngine;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

namespace Bitware {
	namespace Utils {
		public enum ScrollType{
			Horizontal,
			Vertical
		}

		public static class BetterList
		{
//			public static void MergeLists<T>(this List<T> mainList, params object[] args){
//				foreach (var item in args) {
//					var list = item as List<T>;
//					foreach (var listItem in list)
//						mainList.AddSafely (listItem);
//				}
//			}

			public static void FireAndUnsubAllActions (this Action action) {
				if(action != null) {
					action ();
					var subscribedEvents = action.GetInvocationList();
					foreach(var mEvent in subscribedEvents)
						action -= (mEvent as System.Action);
				}
			}

			public static void FireAndUnsubAllActions<T> (this Action<T> action, T param) {
				if(action != null) {
					action (param);
					var subscribedEvents = action.GetInvocationList();
					foreach(var mEvent in subscribedEvents)
						action -= (mEvent as System.Action<T>);
				}
			}

			public static void ResizeGridLayoutScrollRect(this ScrollRect scrollRect, GridLayoutGroup grid, bool reposition = true, bool ignoreHiddenChildren = true){
				var content = scrollRect.content;
				var size = grid.cellSize;
				var space = grid.spacing;
				var width = 0f;
				var height = 0f;

				var childCount = content.transform.childCount;
				for(var i = 0; i < content.transform.childCount; i++)
					if(ignoreHiddenChildren)
						if (!content.transform.GetChild (i).gameObject.activeInHierarchy)
							childCount--;

				if (size.x == 0)
					size.x = 1;
				if (size.y == 0)
					size.y = 1;
				if(grid.constraint == GridLayoutGroup.Constraint.FixedRowCount){
					var cols = (float)(childCount / grid.constraintCount);
					width = (cols * (size.x == 0 ? 1 : size.x)) + ((cols + 1) * (space.x == 0 ? 1 : space.x));
				}
				else{
					var rows = Mathf.Ceil ((float)childCount / grid.constraintCount);
					height = (rows * (size.y == 0 ? 1 : size.y)) + ((rows + 1) * (space.y == 0 ? 1 : space.y));
				}

				var r = content.rect;

				var anchMin = content.anchorMin;
				var anchMax = content.anchorMax;
				content.anchorMax = content.anchorMin = Vector2.one * 0.5f;

				content.sizeDelta = new Vector2 (
					grid.constraint == GridLayoutGroup.Constraint.FixedRowCount ? width : r.width,
					grid.constraint == GridLayoutGroup.Constraint.FixedColumnCount ? height : r.height
				);

				content.anchorMin = anchMin;
				content.anchorMax = anchMax;

				if(reposition){
					var pivot = content.pivot;
					//TODO Shraa1 check why this didnt happen
					// Currently have to set the anchor of scrollcontent to top center to have proper movemnt of panels across tabs
					if(grid.constraint == GridLayoutGroup.Constraint.FixedRowCount){
						if(pivot == Vector2.one * 0.5f)
							content.anchoredPosition = new Vector2 (content.rect.width / 2, 0.5f);
						else if(pivot == Vector2.up * 0.5f)
							content.anchoredPosition = Vector2.up * 0.5f;
					}
					else{
						if(pivot == Vector2.one * 0.5f)
							content.anchoredPosition = new Vector2 (0.5f, content.rect.height / 2);
						else if(pivot == Vector2.right * 0.5f)
							content.anchoredPosition = Vector2.right * 0.5f;
					}
				}


//				//TODO add grid height/width here too
//				for(var i = 0; i < content.transform.childCount; i++){
//					var r = content.transform.GetChild (i).GetComponent<RectTransform> ().rect;
//					width += scrollType == ScrollType.Horizontal ? r.width : 0f;
//					height += scrollType == ScrollType.Vertical ? r.height : 0f;
//				}
//				content.sizeDelta = new Vector2 (
//					scrollType == ScrollType.Horizontal ? width : content.rect.width,
//					scrollType == ScrollType.Vertical ? height : content.rect.height
//				);
//				content.anchoredPosition = new Vector2 (
//					scrollType == ScrollType.Horizontal ? changePivot ? 1f : -1f : 0.5f,
//					scrollType == ScrollType.Vertical ? height : content.rect.height
//				);
			}

			public static List<Transform> FindChildren(this Transform t){
				var list = new List<Transform> ();
				for (var i = 0; i < t.childCount; i++)
					list.Add (t.GetChild (i));
				return list;
			}

			public static List<GameObject> FindGameObjectOfName(string name){
				var gos = GameObject.FindObjectsOfType<GameObject>();
				var gosWithName = new List<GameObject> ();
				for (var i = 0; i < gos.Length; i++)
					if(gos[i].name == name)
						gosWithName.Add (gos[i]);
				return gosWithName;
			}

			public static string Remove(this string str, string stringToRemove){
				return str.Replace (stringToRemove, string.Empty);
			}

			public static RectTransform RT(this GameObject go){
				return go.GetComponent<RectTransform> ();
			}

			public static List<GameObject> gos<T>(this List<T> list) where T:Component{
				var goList = new List<GameObject> ();
				for (var i = 0; i < list.Count; i++)
					goList.Add (list [i].gameObject);
				return goList;
			}

			public static List<string> names(this List<Transform> list) {
				return names (list.gos());
			}

			public static List<string> names(this List<GameObject> list) {
				var goNames = new List<string> ();
				for (var i = 0; i < list.Count; i++)
					goNames.Add (list [i].gameObject.name);
				return goNames;
			}

			public static void Clear(this Text text){
				text.text = string.Empty;
			}

			private static RectTransform scrollContent;

			/// <summary>
			/// Adds the scroll element.
			/// </summary>
			/// <param name="scrollElement">Scroll element to add to the ScrollRect</param>
			public static void AddScrollElement(this ScrollRect scrollRect,
				GameObject scrollElement, ScrollType type, bool resizeAfterAdding = true)
			{
				scrollContent = scrollRect.GetComponent<ScrollRect>().content;
				var element = MonoBehaviour.Instantiate<GameObject>(scrollElement);
				element.transform.SetParent(scrollContent.transform, false);
				if (resizeAfterAdding)
					ResizeScrollRect(scrollRect, type);
			}

			/// <summary>
			/// Adds the scroll element.
			/// </summary>
			/// <param name="scrollElement">Scroll element to add to the ScrollRect</param>
			public static void AddScrollElement(this ScrollRect scrollRect,
				Transform scrollElement, ScrollType type, bool resizeAfterAdding = true)
			{
				AddScrollElement(scrollRect, scrollElement.gameObject, type, resizeAfterAdding);
			}

			/// <summary>
			/// Resizes the scroll rect based on the number of the children of the content for the scrollRect and their sizes.
			/// </summary>
			public static void ResizeScrollRect(this ScrollRect scrollRect, ScrollType type, float gap = 0f, bool ignoreHiddenChildren = false)
			{
				var scrollContent = scrollRect.GetComponent<ScrollRect>().content;
				var rectWidths = new List<float>();
				var rectHeights = new List<float>();
				var elements = new List<RectTransform>();

				var startSize = 0f;
				var ignoredElements = 0;
				for (var i = 0; i < scrollContent.transform.childCount; i++)
				{
					if (!scrollContent.transform.GetChild (i).gameObject.activeSelf && ignoreHiddenChildren){
						ignoredElements++;
						continue;
					}
					elements.Add(scrollContent.transform.GetChild(i).GetComponent<RectTransform>());
					i -= ignoredElements;
					startSize += (type == ScrollType.Horizontal) ?
						elements[i].rect.width :
						elements[i].rect.height;
					rectWidths.Add(elements[i].rect.width);
					rectHeights.Add(elements[i].rect.height);

					SetAnchors(elements[i], type);
					i += ignoredElements;
				}
				startSize += (scrollContent.transform.childCount - ignoredElements + 1) * gap;

				SetAnchors(scrollContent, type);

				for (var i = 0; i < scrollContent.transform.childCount - ignoredElements; i++)
				{
					elements[i].sizeDelta = new Vector2(
						(type == ScrollType.Horizontal) ? rectWidths[i] : 0f,
						(type == ScrollType.Horizontal) ? 0f : rectHeights[i]);
				}

				scrollContent.sizeDelta = new Vector2(
					type == ScrollType.Vertical ? scrollContent.sizeDelta.x : startSize,
					type == ScrollType.Vertical ? startSize : scrollContent.sizeDelta.y
				);


				var startPos = 0f;
				for (var i = 0; i < scrollContent.transform.childCount - ignoredElements; i++)
				{
					var rect = elements[i].rect;
					startPos -= (type == ScrollType.Horizontal) ? -gap : gap;
					startPos -= (type == ScrollType.Horizontal) ? -(rect.width / 2) : (rect.height / 2);
					elements[i].anchoredPosition = new Vector2(
						type == ScrollType.Horizontal ? startPos : 0f,
						type == ScrollType.Horizontal ? 0f : startPos
					);
					startPos -= (type == ScrollType.Horizontal) ? -(rect.width / 2) : (rect.height / 2);
				}
			}

			private static void SetAnchors(RectTransform rectTransform, ScrollType type)
			{
				rectTransform.anchorMax = new Vector2(
					(type == ScrollType.Vertical) ? rectTransform.anchorMax.x : 0f,
					(type == ScrollType.Horizontal) ? rectTransform.anchorMax.y : 1f
				);
				rectTransform.anchorMin = new Vector2(
					(type == ScrollType.Vertical) ? rectTransform.anchorMin.x : 0f,
					(type == ScrollType.Horizontal) ? rectTransform.anchorMin.y : 1f
				);
			}

			public static void MassSetActive(this List<GameObject> listToActivate, bool active){
				foreach (var item in listToActivate)
					item.SetActive (active);
			}

			public static string NoSpace(this string str){
				return str.Replace (" ",string.Empty);
			}

			//	public static List<T> AddOrUpdate<T> (this List<T> list, T key) {
			//		if(list.Contains(key))
			//			list;
			//		else
			//			dict.Add(key, value);
			//		return dict;
			//	}

			public static Dictionary<T, U> AddOrUpdate<T, U> (this Dictionary<T, U> dict, T key, U value ) {
				if(dict.ContainsKey(key))
					dict[key] = value;
				else
					dict.Add(key, value);
				return dict;
			}

			/// <summary>
			/// Whether contains all the strings of StringsInOriginal.
			/// </summary>
			/// <param name="str">String.</param>
			/// <param name="StringsInOriginal">Strings in original.</param>
			public static bool Contains(this string str,params string[] StringsInOriginal){
				for(int i=0;i<StringsInOriginal.Length;i++)
					if(!str.Contains(StringsInOriginal[i]))
						return false;
				return true;
			}

			public static Color Transparent (this Color color) {
				return new Color(1, 1, 1, 0);
			}

			public static Transform FindNested (this Transform trans, string path)
			{
				var split = path.Split ("/" [0]);
				var t = trans;
				foreach (var s in split) {
					t = t.Find (s);
					if (t == null)
						return null;
				}
				return t;
			}

			public static Transform Search (this Transform target, string name)
			{
				if (target.name == name)
					return target;

				for (int i = 0; i < target.childCount; ++i) {
					var result = Search (target.GetChild (i), name);

					if (result != null)
						return result;
				}

				return null;
			}

			public static T GetComponentNested<T> (this GameObject obj, string path) where T : Component
			{
				return GetComponentNested<T> (obj.transform, path);
			}

			public static T GetComponentNested<T> (this Transform trans, string path) where T : Component
			{
				var t = FindNested (trans, path);
				return t == null ? null : t.GetComponent<T> ();
			}

			public static bool IsGenericList (this object o)
			{
				return IsGenericList (o.GetType ());
			}

			public static bool IsGenericList (this System.Type mType)
			{
				return mType.IsGenericType && (mType.GetGenericTypeDefinition () == typeof(List<>));
			}

			public static bool IsAnEnum (this object o)
			{
				return o.GetType ().IsEnum;
			}

			public static bool IsAnEnum (this System.Type mType)
			{
				return mType.IsEnum;
			}

			public static List<T> ToList<T> (this T[] array)
			{

				List<T> list = new List<T> ();
				foreach (T element in array)
					list.Add (element);

				return list;
			}


			public static bool RemoveSafely<T> (this List<T> list, T element)
			{
				if (list.Contains (element)) {
					list.Remove (element);
					return true;
				}
				return false;
			}

			public static bool AddSafely<T> (this List<T> list, T element)
			{
				if (!list.Contains (element)) {
					list.Add (element);
					return true;
				}
				return false;
			}

			//	public static List<T> Init<T> (this List<T> list, int count, T defaultValue) {
			//		list = new List<T>();
			//		for(int i = 0; i < count; i++)
			//			list.Add(defaultValue);
			//		return list;
			//	}

			public static void Init<T> (this List<T> list, int count, T defaultValue)
			{
				list.Clear ();
				for (int i = 0; i < count; i++)
					list.Add (defaultValue);
			}

			public static void InitNested<T> (this List<List<T>> list, int iCount, int jCount, T defaultValue)
			{
				list.Clear ();
				for (int i = 0; i < iCount; i++) {
					List<T> tList = new List<T> ();
					for (int j = 0; j < jCount; j++) {
						tList.Add (defaultValue);
					}
					list.Add (tList);
				}
			}

			public static string Print<T> (this List<T> list)
			{
				if(list == null)
					return string.Empty;
				if(list.Count == 0)
					return string.Empty;
				string s = "";
				foreach (T item in list)
					s += item + ", ";
				s = s.Substring (0, s.Length - 2);
				return s;
			}

			public static string Print<T> (this T[] array)
			{
				if(array == null)
					return string.Empty;
				if(array.Length == 0)
					return string.Empty;
				string s = "";
				foreach (T item in array)
					s += item + ", ";
				s = s.Substring (0, s.Length - 2);
				return s;
			}

			public static string Print<T,U>(this Dictionary<T,U> dict){
				var s = string.Empty;
				foreach(var kvp in dict)
					s+="\nKey :: " + kvp.Key + " Value :: " + kvp.Value;
				return s;
			}

			public static List<T> DeepCopyClass<T> (this List<T> list)
			{
				List<T> newList = new List<T> ();
				foreach (T item in list)
					newList.Add (item);
				return newList;
			}

			public static List<T> DeepCopy<T> (this List<T> list) where T : struct
			{
				List<T> newList = new List<T> ();
				foreach (T item in list)
					newList.Add (item);
				return newList;
			}

			public static bool Contains<T> (this T[] array, T val) where T : struct
			{
				foreach (T element in array) {
					if (element .Equals (val)) {
						return true;
					}
				}
				return false;
			}

			public static List<T> Shuffle<T> (this List<T> list)
			{
				for (int i = list.Count - 1; i >= 0; i--) {
					var a = list [i];
					var r = Random.Range (0, i);
					list [i] = list [r];
					list [r] = a;
				}
				return list;
			}

			//
			//	public static void SortByName<T> (this List<T> list) {
			//		list.Sort()
			//	}
		}

	}
}

