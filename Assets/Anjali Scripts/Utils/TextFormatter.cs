﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System;
using System.Linq;

namespace Bitware {
	namespace Utils {
		public static class TextFormatter {
			private static List<Tuple<double, double, string, string>> NumberFormatters = new List<Tuple<double, double, string, string>>();

			static TextFormatter () {

				NumberFormatters = new List<Tuple<double, double, string, string>>() {
					Tuple.Create(0d,				1d,					"#,0", 			"#,0"),
					Tuple.Create(Math.Pow(10, 0),	Math.Pow(10, 0), 	"#,0", 			"#,0"),

					Tuple.Create(Math.Pow(10, 3),	Math.Pow(10, 3), 	"0.##K", 		"0.##K"),
					Tuple.Create(Math.Pow(10, 4),	Math.Pow(10, 3), 	"0.##K", 		"0.##K"),
					Tuple.Create(Math.Pow(10, 5),	Math.Pow(10, 3), 	"0.#K", 		"0.#K"),

					Tuple.Create(Math.Pow(10, 6),	Math.Pow(10, 6), 	"0.##M", 		"0.## Million"),
					Tuple.Create(Math.Pow(10, 8),	Math.Pow(10, 6), 	"0.#M", 		"0.# Million"),

					Tuple.Create(Math.Pow(10, 9),	Math.Pow(10, 9), 	"0.##B", 		"0.## Billion"),
					Tuple.Create(Math.Pow(10, 11),	Math.Pow(10, 9), 	"0.#B", 		"0.# Billion"),

					Tuple.Create(Math.Pow(10, 12),	Math.Pow(10, 12), 	"0.##T", 		"0.## Trillion"),
					Tuple.Create(Math.Pow(10, 14),	Math.Pow(10, 12), 	"0.#T", 		"0.# Trillion"),

					Tuple.Create(Math.Pow(10, 15),	Math.Pow(10, 15), 	"0.## Quad", 	"0.## Quadrillion"),
					Tuple.Create(Math.Pow(10, 17),	Math.Pow(10, 15), 	"0.# Quad", 	"0.# Quadrillion"),

					Tuple.Create(Math.Pow(10, 18),	Math.Pow(10, 18), 	"0.## Quint", 	"0.## Quintillion"),
					Tuple.Create(Math.Pow(10, 20),	Math.Pow(10, 18), 	"0.# Quint", 	"0.# Quintillion"),

					Tuple.Create(Math.Pow(10, 21),	Math.Pow(10, 21), 	"0.## Sex", 	"0.## Sextillion"),
					Tuple.Create(Math.Pow(10, 23),	Math.Pow(10, 21), 	"0.# Sex", 		"0.# Sextillion"),

					Tuple.Create(Math.Pow(10, 24),	Math.Pow(10, 24), 	"0.## Sept", 	"0.## Septillion"),
					Tuple.Create(Math.Pow(10, 26),	Math.Pow(10, 24), 	"0.# Sept", 	"0.# Septillion"),

					Tuple.Create(Math.Pow(10, 27),	Math.Pow(10, 27), 	"0.## Oct", 	"0.## Octillion"),
					Tuple.Create(Math.Pow(10, 29),	Math.Pow(10, 27), 	"0.# Oct",	 	"0.# Octillion"),

					Tuple.Create(Math.Pow(10, 30),	Math.Pow(10, 30), 	"0.## Non", 	"0.## Nonillion"),
					Tuple.Create(Math.Pow(10, 32),	Math.Pow(10, 30), 	"0.# Non",	 	"0.# Nonillion"),

					Tuple.Create(Math.Pow(10, 33),	Math.Pow(10, 33), 	"0.## Dec", 	"0.## Decillion"),
					Tuple.Create(Math.Pow(10, 35),	Math.Pow(10, 33), 	"0.# Dec",	 	"0.# Decillion"),

					Tuple.Create(Math.Pow(10, 36),	Math.Pow(10, 36), 	"0.## UnDec", 	"0.## Undecillion"),
					Tuple.Create(Math.Pow(10, 38),	Math.Pow(10, 36), 	"0.# UnDec", 	"0.# Undecillion"),

					Tuple.Create(Math.Pow(10, 39),	Math.Pow(10, 39), 	"0.## DuDec", 	"0.## Duodecillion"),
					Tuple.Create(Math.Pow(10, 41),	Math.Pow(10, 39), 	"0.# DuDec", 	"0.# Duodecillion"),

					Tuple.Create(Math.Pow(10, 42),	Math.Pow(10, 42), 	"0.## TrDec", 	"0.## Tredecillion"),
					Tuple.Create(Math.Pow(10, 44),	Math.Pow(10, 42), 	"0.# TrDec", 	"0.# Tredecillion"),

					Tuple.Create(Math.Pow(10, 45),	Math.Pow(10, 45), 	"0.## QtDec", 	"0.## Quattuordecillion"),
					Tuple.Create(Math.Pow(10, 47),	Math.Pow(10, 45), 	"0.# QtDec", 	"0.# Quattuordecillion"),

					Tuple.Create(Math.Pow(10, 48),	Math.Pow(10, 48), 	"0.## QnDec", 	"0.## Quindecillion"),
					Tuple.Create(Math.Pow(10, 50),	Math.Pow(10, 48), 	"0.# QnDec", 	"0.# Quindecillion"),

					Tuple.Create(Math.Pow(10, 51),	Math.Pow(10, 51), 	"0.## SxDec", 	"0.## Sexdecillion"),
					Tuple.Create(Math.Pow(10, 53),	Math.Pow(10, 51), 	"0.# SxDec", 	"0.# Sexdecillion"),

					Tuple.Create(Math.Pow(10, 54),	Math.Pow(10, 54), 	"0.## SpDec", 	"0.## Septendecillion"),
					Tuple.Create(Math.Pow(10, 56),	Math.Pow(10, 54), 	"0.# SpDec", 	"0.# Septendecillion"),

					Tuple.Create(Math.Pow(10, 57),	Math.Pow(10, 57), 	"0.## OcDec", 	"0.## Octodecillion"),
					Tuple.Create(Math.Pow(10, 58),	Math.Pow(10, 57), 	"0.# OcDec", 	"0.# Octodecillion"),

					Tuple.Create(Math.Pow(10, 60),	Math.Pow(10, 60), 	"0.## NoDec", 	"0.## Novemdecillion"),
					Tuple.Create(Math.Pow(10, 62),	Math.Pow(10, 60), 	"0.# NoDec", 	"0.# Novemdecillion"),

					Tuple.Create(Math.Pow(10, 63),	Math.Pow(10, 63), 	"0.## Vig", 	"0.## Vigintillion"),
					Tuple.Create(Math.Pow(10, 65),	Math.Pow(10, 63), 	"0.# Vig", 		"0.# Vigintillion"),

					Tuple.Create(Math.Pow(10, 66),	Math.Pow(10, 66), 	"0.## UnVig", 	"0.## Unvigintillion"),
					Tuple.Create(Math.Pow(10, 68),	Math.Pow(10, 66), 	"0.# UnVig", 	"0.# Unvigintillion"),

					Tuple.Create(Math.Pow(10, 69),	Math.Pow(10, 69), 	"0.## DuVig", 	"0.## Duovigintillion"),
					Tuple.Create(Math.Pow(10, 71),	Math.Pow(10, 69), 	"0.# DuVig", 	"0.# Duovigintillion"),

					Tuple.Create(Math.Pow(10, 72),	Math.Pow(10, 72), 	"0.## TrVig", 	"0.## Trevigintillion"),
					Tuple.Create(Math.Pow(10, 74),	Math.Pow(10, 72), 	"0.# TrVig", 	"0.# Trevigintillion"),

					Tuple.Create(Math.Pow(10, 75),	Math.Pow(10, 75), 	"0.## QtVig", 	"0.## Quattuorvigintillion"),
					Tuple.Create(Math.Pow(10, 77),	Math.Pow(10, 75), 	"0.# QtVig", 	"0.# Quattuorvigintillion"),

					Tuple.Create(Math.Pow(10, 78),	Math.Pow(10, 78), 	"0.## QnVig", 	"0.## Quinvigintillion"),
					Tuple.Create(Math.Pow(10, 80),	Math.Pow(10, 78), 	"0.# QnVig", 	"0.# Quinvigintillion"),

					Tuple.Create(Math.Pow(10, 81),	Math.Pow(10, 81), 	"0.## SxVig", 	"0.## Sexvigintillion"),
					Tuple.Create(Math.Pow(10, 83),	Math.Pow(10, 81), 	"0.# SxVig", 	"0.# Sexvigintillion"),

					Tuple.Create(Math.Pow(10, 84),	Math.Pow(10, 84), 	"0.## SpVig", 	"0.## Septenvigintillion"),
					Tuple.Create(Math.Pow(10, 86),	Math.Pow(10, 84), 	"0.# SpVig", 	"0.# Septenvigintillion"),

					Tuple.Create(Math.Pow(10, 87),	Math.Pow(10, 87), 	"0.## OcVig", 	"0.## Octovigintillion"),
					Tuple.Create(Math.Pow(10, 89),	Math.Pow(10, 87), 	"0.# OcVig", 	"0.# Octovigintillion"),

					Tuple.Create(Math.Pow(10, 90),	Math.Pow(10, 90), 	"0.## NoVig", 	"0.## Novemvigintillion"),
					Tuple.Create(Math.Pow(10, 92),	Math.Pow(10, 90), 	"0.# NoVig", 	"0.# Novemvigintillion"),

					Tuple.Create(Math.Pow(10, 93),	Math.Pow(10, 93), 	"0.## Trig", 	"0.## Trigintillion"),
					Tuple.Create(Math.Pow(10, 95),	Math.Pow(10, 93), 	"0.# Trig", 	"0.# Trigintillion")
				};
			}

			public static string GetFormmatedNumberTextWithLongName (double number, bool ignoreLetters = false) {

				var formatter = NumberFormatters.LastOrDefault(x => number >= x.arg1);
				return (number / formatter.arg2).ToString(formatter.arg4);
			}

			public static string GetFormattedNumberText (double number, bool ignoreLetters = false) {
				if (!ignoreLetters) {
					var formatter = NumberFormatters.LastOrDefault(x => number >= x.arg1);
					if(formatter == null)
						return string.Empty;
					if(formatter.arg2 == 0d || string.IsNullOrEmpty(formatter.arg3) || string.IsNullOrEmpty(formatter.arg4))
						return string.Empty;

					return (number / formatter.arg2).ToString(formatter.arg3);
				}
				if (number < 1000)
					return number.ToString ();
				return number.ToString ("#,#", CultureInfo.InvariantCulture);
			}

			//http://stackoverflow.com/questions/2134161/format-number-like-stack-overflow-rounded-to-thousands-with-k-suffix
			//http://stackoverflow.com/questions/11731996/string-format-numbers-thousands-123k-millions-123m-billions-123b
			public static string GetFormattedNumberText (long number, bool ignoreLetters = false) {
				return GetFormattedNumberText((double)number, ignoreLetters);
			}

			public static string GetFormattedNumberText (int number, bool ignoreLetters = false) {
				return GetFormattedNumberText((double)number, ignoreLetters);
			}

			public static string GetFormattedNumberText (float number, bool ignoreLetters = false) {
				return GetFormattedNumberText((double)number, ignoreLetters);
			}

			//[InvokableDuringHookable]
			public static string GetFormattedNumberText (object number) {
				if(number == null)
					return string.Empty;
				double d = 0;
				var success = double.TryParse(number.ToString(), out d);
				return success ? GetFormattedNumberText(d, false) : number.ToString();
			}

			//[InvokableDuringHookable]
			public static string GetFormmatedNumberTextWithLongName (object number) {
				if(number == null)
					return string.Empty;
				double d = 0;
				var success = double.TryParse(number.ToString(), out d);
				return success ? GetFormmatedNumberTextWithLongName(d, false) : number.ToString();
			}

			//[InvokableDuringHookable]
			public static string GetTruncatedText (object obj) {
				if (obj == null)
					return string.Empty;
				if (obj as string == null)
					return string.Empty;
				var text = obj as string;
				if (text.Length < 10)
					return text;
				return text.Substring (0, 8) + "...";
			}

			public static string GetFormattedTextForMinorUnits (int number) {
				return (number * 0.01f).ToString ("G4");
			}
		}
	}
}
