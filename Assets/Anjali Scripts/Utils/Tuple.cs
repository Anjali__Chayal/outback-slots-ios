﻿using UnityEngine;
using System.Collections;
using MiniJSON;
using System.Collections.Generic;
using System.Linq;
using System;
using System.CodeDom;
using Bitware.Utils;

[Serializable]
public class Tuple<T1> {
	public T1 arg1;
	public Tuple (T1 arg1) {
		this.arg1 = arg1;
	}

	public Dictionary<string, object> Serialize () {
		return new Dictionary<string, object> () {
			{ "arg1", arg1 }
		};
	}

	public override string ToString () {
		return JSON.Serialize(Serialize());
	}
}

[Serializable]
public class Tuple<T1, T2> {
	public T1 arg1;
	public T2 arg2;

	public Tuple (T1 arg1, T2 arg2) {
		this.arg1 = arg1;
		this.arg2 = arg2;
	}

	public Dictionary<string, object> Serialize () {
		return new Dictionary<string, object> () {
			{ "arg1", arg1 },
			{ "arg2", arg2 }
		};
	}

	public override string ToString () {
		return JSON.Serialize(Serialize());
	}
}

[Serializable]
public class Tuple<T1, T2, T3> {
	public T1 arg1;
	public T2 arg2;
	public T3 arg3;

	public Tuple (T1 arg1, T2 arg2, T3 arg3) {
		this.arg1 = arg1;
		this.arg2 = arg2;
		this.arg3 = arg3;
	}

	public Dictionary<string, object> Serialize () {
		return new Dictionary<string, object> () {
			{ "arg1", arg1 },
			{ "arg2", arg2 },
			{ "arg3", arg3 }
		};
	}

	public override string ToString () {
		return JSON.Serialize(Serialize());
	}
}

[Serializable]
public class Tuple<T1, T2, T3, T4> {
	public T1 arg1;
	public T2 arg2;
	public T3 arg3;
	public T4 arg4;

	public Tuple (T1 arg1, T2 arg2, T3 arg3, T4 arg4) {
		this.arg1 = arg1;
		this.arg2 = arg2;
		this.arg3 = arg3;
		this.arg4 = arg4;
	}

	public Dictionary<string, object> Serialize () {
		return new Dictionary<string, object> () {
			{ "arg1", arg1 },
			{ "arg2", arg2 },
			{ "arg3", arg3 },
			{ "arg4", arg4 }
		};
	}

	public override string ToString () {
		return JSON.Serialize(Serialize());
	}
}

public static class Tuple {

	public static Tuple<T1> Create<T1> (T1 arg1) {
		return new Tuple<T1>(arg1);
	}

	public static Tuple<T1, T2> Create<T1, T2> (T1 arg1, T2 arg2) {
		return new Tuple<T1, T2>(arg1, arg2);
	}

	public static Tuple<T1, T2, T3> Create<T1, T2, T3> (T1 arg1, T2 arg2, T3 arg3) {
		return new Tuple<T1, T2, T3>(arg1, arg2, arg3);
	}

	public static Tuple<T1, T2, T3, T4> Create<T1, T2, T3, T4> (T1 arg1, T2 arg2, T3 arg3, T4 arg4) {
		return new Tuple<T1, T2, T3, T4>(arg1, arg2, arg3, arg4);
	}
}