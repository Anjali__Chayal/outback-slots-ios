﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using Bitware.Utils;
using Bitware.Wrappers;

namespace Bitware
{
	namespace Utils {
		public class UnityEditorPopup : MonoBehaviour {
			private List<Button> btns = new List<Button> ();
			private List<Text> texts = new List<Text> ();
			private Text t;
			private Text msg;
			public static UnityEditorPopup instance;

			void Awake(){
				DontDestroyOnLoad (transform.parent);
				instance = this;
				var i = 0;
				do{
					var b = transform.FindNested(i.ToString ()).GetComponent<Button>();
					btns.Add (b);
					b.image.sprite = SpriteManager.TransparentImage;
					texts.Add (transform.FindNested(i.ToString ()).FindNested ("Text").GetComponent<Text>());
					i++;
				}while(transform.FindNested ((i).ToString ()) != null);
				t = transform.FindNested ("Title").GetComponent<Text>();
				msg = transform.FindNested ("Message").GetComponent<Text>();
				gameObject.SetActive (false);
			}

			public static void ShowPopup(string title, string message, string[] buttons, System.Action<int> action){
				if (buttons.Length > 5)
					return;

				for (var i = 0; i < instance.btns.Count; i++)
					instance.btns [i].onClick.RemoveAllListeners ();
				for (var i = 0; i < instance.texts.Count; i++)
					instance.texts [i].Clear ();

				instance.gameObject.SetActive (true);
				instance.t.text = title;
				instance.msg.text = message;

				for(var i=0; i<buttons.Length;i++){
					instance.texts[i].GetComponent<Text> ().text = buttons[i];

					var a = i;
					instance.btns[i].onClick.AddListener(() => {
						if(action!=null)
							action(a);
						instance.gameObject.SetActive (false);
					});
				}
			}
		}
	}
}