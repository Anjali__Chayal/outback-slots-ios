﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Bitware.Utils;
using UnityEngine.UI;
using Outback.Managers;
using Outback.Core;
using TMPro.Examples;

public class SpinWheelController : MonoBehaviour
{
	public static SpinWheelController instance;

	public GameObject wheel;
	public GameObject spinWheel, pointer;
	public GameObject spinButton, collectButton;
	public TMPro.TextMeshProUGUI winningsText;
	public GameObject winningTextBG;
	public GameObject title;
	List<int> stoppingValues = new List<int> ();
	public List<int> winnings = new List<int> ();
	int segments = 20;
	public List<Sprite> numbers;

	void Awake ()
	{
		instance = this;
	}
	// Use this for initialization
	void Start ()
	{
		var eachDegree = 360 / segments;
		for (int i = 0; i < segments; i++) {
			stoppingValues.Add (eachDegree * i);
		}
		Debug.Log ("Stopping values " + stoppingValues.Print ());
	}
	
	// Update is called once per frame
	public void SpinButtonClicked ()
	{
		var ran = Random.Range (0, stoppingValues.Count);
		var stoppingAngle = (float)stoppingValues [ran];
		title.gameObject.SetActive (false);
		spinButton.GetComponent <Button> ().interactable = false;
		GetComponent<AudioSource>().enabled = true;
		var a = 0f;
		DOTween.To (x => MovePointer (), 0f, 3f, 3f).OnComplete (() => {
			pointer.transform.eulerAngles = new Vector3 (0, 0, 0);
		});
		DOTween.To (x => RotateWheel (), 0f, 3f, 2f).OnComplete (() => {
			Debug.Log("stoppingAngle "+stoppingAngle);
//			if (stoppingAngle <= 180)
			stoppingAngle = 360 + stoppingAngle;

			wheel.transform.DORotate (new Vector3 (0f, 0f, stoppingAngle), 2f,RotateMode.FastBeyond360).OnComplete(()=> {
				GetComponent<AudioSource>().enabled = false;
			});
		});

		DOTween.To (x => x = 0, 0f, 3f, 7f).OnComplete (() => {
			spinWheel.transform.DOScale (new Vector3 (0, 0, 0), 0.5f).OnComplete (() => {
				winningTextBG.gameObject.SetActive (true);

				winningsText = Instantiate(Resources.Load("WinningText", typeof(TMPro.TextMeshProUGUI)), winningTextBG.transform,false) as TMPro.TextMeshProUGUI;
				winningsText.transform.localPosition = new Vector3(winningsText.transform.localPosition.x,0,0);
//				winningsText.transform.parent = winningTextBG.transform;
				CoinsShower.instance.StartAnim ();

				winningsText.text = "0";
				var r = GetReward (stoppingValues [ran]);
				DailyReward.RewardToEarn = r;
				DOTween.To (x => GiveReward (x), 0, r, 2f);
				DOTween.To (x => x = 0, 0f, 3f, 4f).OnComplete (() => {
					collectButton.SetActive (true);
				});
			});

		});
	}

	int GetReward (int angle)
	{
		var index = stoppingValues.IndexOf (angle);
		var reward = winnings [index];
		return reward;
	}

	void GiveReward (float value)
	{
		winningsText.text = TextFormatter.GetFormattedNumberText (value, true);
	}

	void MovePointer ()
	{
		if (pointer.transform.eulerAngles.z >= 20f)
			pointer.transform.eulerAngles = new Vector3 (0, 0, 0);
		
		pointer.transform.eulerAngles = pointer.transform.eulerAngles + new Vector3 (0, 0, 5f);
	}

	void RotateWheel ()
	{
		if (wheel.transform.eulerAngles.z >= 360)
			wheel.transform.eulerAngles = new Vector3 (0, 0, 0);

		wheel.transform.eulerAngles = wheel.transform.eulerAngles + new Vector3 (0, 0, 5f);
	}

	public void OpenPopUp ()
	{
		transform.DOScale (new Vector3 (1, 1, 1), 0.5f).OnStart (() => {
			ScreenManager.instance.playerDetailsPanel.SetActive (false);

		});
	}

	public void ClosePopUp ()
	{
		title.gameObject.SetActive (true);
		ScreenManager.instance.playerDetailsPanel.SetActive (true);
		spinButton.GetComponent <Button> ().interactable = true;
		collectButton.SetActive (false);
		winningTextBG.gameObject.SetActive (false);
		spinWheel.transform.localScale = new Vector3 (1, 1, 1);
		gameObject.SetActive (false);
		if (winningsText != null)
		Destroy (winningsText.gameObject);
		winningsText = null;
	}
}
