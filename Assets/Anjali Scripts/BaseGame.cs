﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Outback.Core;
using LitJson;
using Outback.Managers;
using Outback.Views;
using DG.Tweening;
using Bitware.Utils;
using System.Linq;


[SerializeField]
public class BaseGame : MonoBehaviour
{
	public static int[,] gameArray = new int[3, 5];
	public static Image[,] gameImageArray = new Image[3, 5];
	public static int[,] ruleGameArray = new int[12, 4];
	public Image freeSpinPopUp;
	public GameObject rewardPopUp;
	public GameObject noRewardPopUp;
	public Button spinBtn;
	public Button stopBtn;
	public Button freeSpinBtn;
	public GameObject ruleLine;
	public GameObject freeSpinRewardPopUp;
	public GameObject bonusGamePanel;
	public static int finalWin;
	public ParticleSystem coinsAnim;
	public Sprite stopButtonSprite;
	public Sprite spinButtonSprite;
	public Sprite autoSpinStopButtonSprite;
	public Sprite autoSpinStartButtonSprite;

	public AudioSource musicSource;
	public AudioSource musicSourceFreeScatter;

	public long winAmount;
	public long betAmount;

	public Text betValue;
	public Text currentCoins;
	public Text winValue;
	public Text freeSpinRemainingText;
	public bool countingFreeSpins;
	public long minBet;

	public int numberOfFreeSpinsLeft;

	public long freeSpinWinnings = 0;

	public bool isBonusGame;

	public List<GameObject> rulePages;

	public List<Reward> rewards = new List<Reward> ();
	// Use this for initialization
	public virtual void Start ()
	{
		SoundManager.instance.gamesBackgroundMusic = GameObject.FindGameObjectsWithTag ("Background Music").ToList().ConvertAll(x=> x.GetComponent<AudioSource>()).ToList ();

		freeSpinPopUp.gameObject.SetActive (false);
		ScreenManager.instance.loadingScreenObj.SetActive (false);
		betAmount = minBet;
		betValue.text = betAmount.ToString ();
		currentCoins.text = TextFormatter.GetFormattedNumberText (Player.CurrentCoins, true);
	}


	public void CalculateStats ()
	{
//		Debug.Log ("Win Amount in calculate stats " + winAmount);
		if (winAmount == 0) {
			coinsAnim.gameObject.SetActive (false);
			if (!countingFreeSpins) {
				winValue.text = "0"; 

			}
		} else {
			if (!countingFreeSpins) {
				winValue.text = TextFormatter.GetFormattedNumberText (winAmount, true); 
				Player.CurrentCoins += winAmount;
				currentCoins.text = TextFormatter.GetFormattedNumberText (Player.CurrentCoins, true);
			} else {
				winValue.text = TextFormatter.GetFormattedNumberText (winAmount, true); 
			}
			coinsAnim.gameObject.SetActive (true);
		
		}
//		Debug.Log (Player.CurrentCoins);
		PlayerManager.ME.UpdateUsertable ("current_coins");
	}

	public void SpinSlots ()
	{

		if ((Player.CurrentCoins - int.Parse (betValue.text)) < 0) {
			ScreenManager.instance.OpenStorePanel ();
		} else {

//					if(!(GetComponent<AudioSource>().isPlaying))
//					{
//						Debug.Log ("Music Called");
//						musicSource.GetComponent<AudioSource>().Play();
//					}
//					else
//					{
//						//Debug.log("Something is wrong with Music.");
//					}
			foreach (Transform child in ruleLine.transform) {
				//child.gameObject.SetActive(false);
				Destroy (child.gameObject);
			}

			ruleLine.SetActive (false);

			SlotGameManager.instance.SpinSlots ();

		}

	}

	public virtual void DrawRuleLines ()
	{
	}



	public void StartFreeSpin ()
	{
		countingFreeSpins = true;

		freeSpinRemainingText.text = SlotGameManager.instance.numberOfFreeSpins.ToString ();
		freeSpinBtn.gameObject.SetActive (true);
		spinBtn.gameObject.SetActive (false);
		stopBtn.gameObject.SetActive (false);
//		Debug.Log ("Start free spins Sequence " + SlotGameManager.instance.numberOfFreeSpins);
		if (SlotGameManager.instance.gameName.Contains ("MovieNight")){
			var anim = freeSpinPopUp.GetComponent<Animator> ();
			anim.enabled = true;
			DOTween.To (x => x = 0f , 0f, 3f, 3f).OnComplete (() => {
				anim.enabled = false;
				freeSpinPopUp.transform.Find("Curtain").gameObject.SetActive(false);

				freeSpinPopUp.transform.Find("FreeSpinButton").gameObject.SetActive(true);
				freeSpinPopUp.transform.Find("CancelButton").gameObject.SetActive(true);
				freeSpinPopUp.transform.Find("Text").gameObject.SetActive(true);
				freeSpinPopUp.gameObject.SetActive (false);
				ScreenManager.instance.playerDetailsPanel.SetActive (true);

				StartCoroutine (StartFreeSpinSequence ());
			});
		}
			
		else {
			ScreenManager.instance.playerDetailsPanel.SetActive (true);

			freeSpinPopUp.gameObject.SetActive (false);
			StartCoroutine (StartFreeSpinSequence ());
		}

	}



	public IEnumerator StartFreeSpinSequence ()
	{
//		SlotGameManager.instance.maxScatter = 0;
		if (countingFreeSpins) {
			Debug.Log ("Start free spins " + numberOfFreeSpinsLeft);
			freeSpinRemainingText.text = numberOfFreeSpinsLeft.ToString ();
			SlotGameManager.instance.SpinSlots ();

//			SlotGameManager.instance.StopSlots ();
//			yield return new WaitForSeconds (4);	
			yield return new WaitForSeconds (8f);

			if (numberOfFreeSpinsLeft > 0) {
				numberOfFreeSpinsLeft--;
				countingFreeSpins = true;

//				numberOfFreeSpinsLeft = SlotGameManager.instance.numberOfFreeSpins;
				freeSpinWinnings += winAmount;
				Debug.Log ("freeSpinWinnings " + freeSpinWinnings);
				StartCoroutine (StartFreeSpinSequence ());
				yield return new WaitForSeconds (1);
			

			} else {
				freeSpinWinnings += winAmount;
				Debug.Log ("freeSpinWinnings " + freeSpinWinnings);
				if (freeSpinWinnings > 0) {
					countingFreeSpins = false;
					if (!isBonusGame) {
						OpenFreeSpinRewardPopUp ();
					} else {
						if (SlotGameManager.instance.gameName.Contains ("MovieNight"))
							freeSpinPopUp.GetComponent<Animator> ().enabled = false;
						var amntWon = 0L;
						var newAmount = SlotGameManager.baseGame.freeSpinWinnings;
						Player.CurrentCoins = Player.CurrentCoins + newAmount;
						SlotGameManager.baseGame.currentCoins.text = TextFormatter.GetFormattedNumberText (Player.CurrentCoins, true);
						PlayerManager.ME.UpdateUsertable ("current_coins");
						var a = 0;
						DOTween.To (x => x = a, a, 2f, 2f).OnComplete (() => {
							//						yield return new WaitForSeconds (2f);
							rewardPopUp.gameObject.SetActive (true);
							CoinsShower.instance.StartAnim ();
							RewardPanel.instance.ShowMessage (amntWon, newAmount);
							freeSpinBtn.gameObject.SetActive (false);
							spinBtn.gameObject.SetActive (true);
							stopBtn.gameObject.SetActive (true);
							SlotGameManager.baseGame.winValue.text = TextFormatter.GetFormattedNumberText ((newAmount),true);
					
						});
						SlotGameManager.baseGame.freeSpinWinnings = 0;
						foreach (var btn in SlotGameManager.instance.buttonsToDisableWhileSpinning) {
							btn.interactable = true;
						}
						SlotGameManager.baseGame.spinBtn.interactable = true;
						SlotGameManager.baseGame.stopBtn.interactable = true;

					}
				} else {
					freeSpinBtn.gameObject.SetActive (false);
					spinBtn.gameObject.SetActive (true);
					stopBtn.gameObject.SetActive (true);
					countingFreeSpins = false;
					ShowNoReward ();
					foreach (var btn in SlotGameManager.instance.buttonsToDisableWhileSpinning) {
//						Debug.Log("Turning on btns as no reward");
						btn.interactable = true;
					}
					SlotGameManager.baseGame.spinBtn.interactable = true;
					SlotGameManager.baseGame.stopBtn.interactable = true;

				}

			}
		} 

		//yield return new WaitForSeconds(interval);
	


		//			if ((GetComponent<AudioSource> ().isPlaying)) {
		//				musicSource.GetComponent<AudioSource> ().Stop ();
		//				musicSourceFreeScatter.GetComponent<AudioSource> ().Stop ();
		//			}

	}

	public void ShowNoReward ()
	{
		ScreenManager.instance.playerDetailsPanel.SetActive (false);
		noRewardPopUp.SetActive (true);
		Invoke ("HideNoReward", 2f);
	}

	public void HideNoReward ()
	{
		noRewardPopUp.SetActive (false);
		ScreenManager.instance.playerDetailsPanel.SetActive (true);
	}

	public void CancelFreeSpins ()
	{
		ScreenManager.instance.playerDetailsPanel.SetActive (true);
		freeSpinBtn.gameObject.SetActive (false);
		spinBtn.gameObject.SetActive (true);
		stopBtn.gameObject.SetActive (true);
		countingFreeSpins = false;
		freeSpinPopUp.gameObject.SetActive (false);
		foreach (var btn in SlotGameManager.instance.buttonsToDisableWhileSpinning) {
//			Debug.Log("Turning on btns as cancelling free spins");
			btn.interactable = true;
		}
	}

	public void OpenFreeSpinRewardPopUp ()
	{
		ScreenManager.instance.playerDetailsPanel.SetActive (false);
		freeSpinRewardPopUp.SetActive (true);
		countingFreeSpins = false;

		freeSpinBtn.gameObject.SetActive (false);
		spinBtn.gameObject.SetActive (true);
		stopBtn.gameObject.SetActive (true);
	}


	public void OpenRulePage (int index)
	{
		ScreenManager.instance.playerDetailsPanel.SetActive (false);
		foreach (var page in rulePages) {
			page.SetActive (false);
		}
		rulePages [index].SetActive (true);
	}

	public void ExitRulePages ()
	{
		ScreenManager.instance.playerDetailsPanel.SetActive (true);
		foreach (var page in rulePages) {
			page.SetActive (false);
		}
	}

	public void HideGameRewardsExcept (int rewardNumToshow)
	{
		Debug.Log (rewardNumToshow);
		foreach (var reward in rewards) {
			reward.gameObject.SetActive (false);
		}
		rewards.Find (x => x.gameObject.name.Contains ("Reward " + (rewardNumToshow + 1))).gameObject.SetActive (true);
	}

	public void UnHideGameRewards ()
	{

		foreach (var reward in rewards) {
			reward.gameObject.SetActive (true);
			reward.rewardAmount.transform.position = reward.rewardPos;
			reward.rewardAmount.transform.localScale = Vector3.zero;
			reward.GetComponent <Button> ().interactable = true;
			reward.bonusReward = 0;
		}

	}
}