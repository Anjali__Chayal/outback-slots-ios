﻿#define BROKENLINE_ROMOVED
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Outback.Core;
using System.Linq;
using LitJson;
using Bitware.Utils;
using System;
using Outback.Managers;

namespace Outback
{
	namespace Gameplay
	{
		[Serializable]
		public class SlotGameEgypt : BaseGame
		{
			public static SlotGameEgypt instance;
			public List<Sprite> lines;

			void Awake ()
			{
				instance = this;

			}
			// Use this for initialization
			public override void Start ()
			{
				base.Start ();
//				SlotGameManager.baseGame.betAmount = 1000;
//
//				betValue.text = SlotGameManager.baseGame.betAmount.ToString ();
			}

			public override void DrawRuleLines ()
			{
				StartCoroutine (StartOff ());
			}

			IEnumerator StartOff ()
			{    

				//10 Rule
				ruleGameArray [0, 0] = 0;
				ruleGameArray [0, 1] = 5;
				ruleGameArray [0, 2] = 20;
				ruleGameArray [0, 3] = 50;


				//A Rule
				ruleGameArray [1, 0] = 0;
				ruleGameArray [1, 1] = 12;
				ruleGameArray [1, 2] = 40;
				ruleGameArray [1, 3] = 90;

				//J Rule
				ruleGameArray [2, 0] = 0;
				ruleGameArray [2, 1] = 7;
				ruleGameArray [2, 2] = 30;
				ruleGameArray [2, 3] = 75;

				//K Rule
				ruleGameArray [3, 0] = 0;
				ruleGameArray [3, 1] = 12;
				ruleGameArray [3, 2] = 40;
				ruleGameArray [3, 3] = 90;

				//Q Rule

				ruleGameArray [4, 0] = 0;
				ruleGameArray [4, 1] = 7;
				ruleGameArray [4, 2] = 30;
				ruleGameArray [4, 3] = 75;

				//Camel Rule
				ruleGameArray [5, 0] = 0;
				ruleGameArray [5, 1] = 15;
				ruleGameArray [5, 2] = 55;
				ruleGameArray [5, 3] = 120;

				//Snake Rule
				ruleGameArray [6, 0] = 0;
				ruleGameArray [6, 1] = 20;
				ruleGameArray [6, 2] = 70;
				ruleGameArray [6, 3] = 150;

				//Pharoah Rule
				ruleGameArray [7, 0] = 0;
				ruleGameArray [7, 1] = 40;
				ruleGameArray [7, 2] = 90;
				ruleGameArray [7, 3] = 170;

				//woman Rule
				ruleGameArray [8, 0] = 10;
				ruleGameArray [8, 1] = 50;
				ruleGameArray [8, 2] = 110;
				ruleGameArray [8, 3] = 250;

				//lucky symbol Rule(Bonus) 
				ruleGameArray [9, 0] = 5;
				ruleGameArray [9, 1] = 15;
				ruleGameArray [9, 2] = 40;
				ruleGameArray [9, 3] = 90;

				//cat Rule (Scatter)
				ruleGameArray [10, 0] = 0;
				ruleGameArray [10, 1] = 20;
				ruleGameArray [10, 2] = 45;
				ruleGameArray [10, 3] = 100;

				//wild Rule 
				ruleGameArray [11, 0] = 0;
				ruleGameArray [11, 1] = 30;
				ruleGameArray [11, 2] = 0;
				ruleGameArray [11, 3] = 0;

				var arr1 = "";
				//i is column and j is row
				//gameArray 3 rows andd 5 columns 
				//				gameArray[5,3] = {{0,1,2,3,4} ,{0,1,2,3,4} ,{0,1,2,3,4} }
				for (int i = 0; i < 3; i++) {
					var arr = "";

					for (int j = 0; j < 5; j++) {
						//						gameArray [i, j] = int.Parse (SlotGameManager.instance.slotReels [j].visibleSlots [i].GetComponent <Image> ().sprite.name);

						gameImageArray [i, j] = SlotGameManager.instance.slotReels [j].visibleSlots [i].GetComponent <Image> ();
						arr += "(" + i + "," + j + ")" + gameImageArray [i, j].sprite.name + " , ";
					}	
					arr1 += "{" + arr + "}";
				}
				Debug.Log ("GAMEARRAY " + arr1);
//				spinBtn.interactable = false;

				yield return new WaitForSeconds (1); //used for total wait sec

				//	 Debug.Log ("Stopping Startoff"); 
				//allows the start button to be used
				//check Rule 1 row-2 -> all Same

				//countJ is number of elements with rules
				int[,] linesRules = new int[26, 5];
				linesRules [1, 0] = 1;
				linesRules [1, 1] = 1;
				linesRules [1, 2] = 1;
				linesRules [1, 3] = 1;
				linesRules [1, 4] = 1;


				linesRules [2, 0] = 2;
				linesRules [2, 1] = 2;
				linesRules [2, 2] = 2;
				linesRules [2, 3] = 2;
				linesRules [2, 4] = 2;

				linesRules [3, 0] = 0;
				linesRules [3, 1] = 0;
				linesRules [3, 2] = 0;
				linesRules [3, 3] = 0;
				linesRules [3, 4] = 0;

				linesRules [4, 0] = 2;
				linesRules [4, 1] = 1;
				linesRules [4, 2] = 0;
				linesRules [4, 3] = 1;
				linesRules [4, 4] = 2;

				linesRules [5, 0] = 0;
				linesRules [5, 1] = 1;
				linesRules [5, 2] = 2;
				linesRules [5, 3] = 1;
				linesRules [5, 4] = 0;

				linesRules [6, 0] = 2;
				linesRules [6, 1] = 2;
				linesRules [6, 2] = 1;
				linesRules [6, 3] = 2;
				linesRules [6, 4] = 2;

				linesRules [7, 0] = 0;
				linesRules [7, 1] = 0;
				linesRules [7, 2] = 1;
				linesRules [7, 3] = 0;
				linesRules [7, 4] = 0;

				linesRules [8, 0] = 1;
				linesRules [8, 1] = 0;
				linesRules [8, 2] = 0;
				linesRules [8, 3] = 0;
				linesRules [8, 4] = 1;

				linesRules [9, 0] = 1;
				linesRules [9, 1] = 0;
				linesRules [9, 2] = 0;
				linesRules [9, 3] = 0;
				linesRules [9, 4] = 1;

				linesRules [10, 0] = 2;
				linesRules [10, 1] = 1;
				linesRules [10, 2] = 1;
				linesRules [10, 3] = 1;
				linesRules [10, 4] = 2;


				linesRules [11, 0] = 0;
				linesRules [11, 1] = 1;
				linesRules [11, 2] = 1;
				linesRules [11, 3] = 1;
				linesRules [11, 4] = 0;

				linesRules [12, 0] = 1;
				linesRules [12, 1] = 2;
				linesRules [12, 2] = 1;
				linesRules [12, 3] = 2;
				linesRules [12, 4] = 1;

				linesRules [13, 0] = 0;
				linesRules [13, 1] = 1;
				linesRules [13, 2] = 0;
				linesRules [13, 3] = 1;
				linesRules [13, 4] = 0;

				linesRules [14, 0] = 1;
				linesRules [14, 1] = 2;
				linesRules [14, 2] = 1;
				linesRules [14, 3] = 2;
				linesRules [14, 4] = 1;

				linesRules [15, 0] = 1;
				linesRules [15, 1] = 0;
				linesRules [15, 2] = 1;
				linesRules [15, 3] = 0;
				linesRules [15, 4] = 1;

				linesRules [16, 0] = 1;
				linesRules [16, 1] = 1;
				linesRules [16, 2] = 2;
				linesRules [16, 3] = 1;
				linesRules [16, 4] = 1;

				linesRules [17, 0] = 1;
				linesRules [17, 1] = 1;
				linesRules [17, 2] = 0;
				linesRules [17, 3] = 1;
				linesRules [17, 4] = 1;

				linesRules [18, 0] = 2;
				linesRules [18, 1] = 0;
				linesRules [18, 2] = 2;
				linesRules [18, 3] = 0;
				linesRules [18, 4] = 2;

				linesRules [19, 0] = 0;
				linesRules [19, 1] = 2;
				linesRules [19, 2] = 0;
				linesRules [19, 3] = 2;
				linesRules [19, 4] = 0;

				linesRules [20, 0] = 1;
				linesRules [20, 1] = 2;
				linesRules [20, 2] = 0;
				linesRules [20, 3] = 2;
				linesRules [20, 4] = 1;

				linesRules [21, 0] = 1;
				linesRules [21, 1] = 2;
				linesRules [21, 2] = 1;
				linesRules [21, 3] = 1;
				linesRules [21, 4] = 1;

				linesRules [22, 0] = 2;
				linesRules [22, 1] = 2;
				linesRules [22, 2] = 0;
				linesRules [22, 3] = 2;
				linesRules [22, 4] = 2;

				linesRules [23, 0] = 0;
				linesRules [23, 1] = 0;
				linesRules [23, 2] = 2;
				linesRules [23, 3] = 0;
				linesRules [23, 4] = 0;

				linesRules [24, 0] = 1;
				linesRules [24, 1] = 1;
				linesRules [24, 2] = 1;
				linesRules [24, 3] = 0;
				linesRules [24, 4] = 1;

				linesRules [25, 0] = 2;
				linesRules [25, 1] = 1;
				linesRules [25, 2] = 1;
				linesRules [25, 3] = 2;
				linesRules [25, 4] = 1;
//
//				linesRules [26, 0] = 2;
//				linesRules [26, 1] = 1;
//				linesRules [26, 2] = 2;
//				linesRules [26, 3] = 2;
//				linesRules [26, 4] = 2;
//
//				linesRules [27, 0] = 1;
//				linesRules [27, 1] = 1;
//				linesRules [27, 2] = 1;
//				linesRules [27, 3] = 2;
//				linesRules [27, 4] = 1;
//
//				linesRules [28, 0] = 2;
//				linesRules [28, 1] = 2;
//				linesRules [28, 2] = 2;
//				linesRules [28, 3] = 1;
//				linesRules [28, 4] = 2;
//
//				linesRules [29, 0] = 2;
//				linesRules [29, 1] = 2;
//				linesRules [29, 2] = 1;
//				linesRules [29, 3] = 1;
//				linesRules [29, 4] = 2;
//
//				linesRules [30, 0] = 1;
//				linesRules [30, 1] = 0;
//				linesRules [30, 2] = 2;
//				linesRules [30, 3] = 1;
//				linesRules [30, 4] = 0;

				//		stopspinBtn.gameObject.SetActive (true);
				//		stopspinBtn.enabled = true;	
				//				spinBtn.interactable = true;
				long totalAmountWon = 0;

				var allMatchedImages = new List<Image> ();
				var numOfLines = linesRules.GetLength(0)-1;
				Debug.Log("number of lines "+numOfLines);
				for (int currentrule = 1; currentrule < linesRules.GetLength(0); currentrule++) {
					var imagesInLineMatch = new List<Image> ();

					for (int col = 0; col < 5; col++) {
						var consideringImage = gameImageArray [linesRules [currentrule, col], col];
						imagesInLineMatch.Add (consideringImage);
					}
					//					Debug.Log ("imagesInLineMatch " + imagesInLineMatch.ConvertAll (x => x.name).ToList ().Print () + " for current rule " + currentrule);

					//					var matches = new List<int> ();


					#if BROKENLINE_APPLIED
					var matchedImagesinLineMatch = new List<Image> ();
					var listofMatchedIndexes = new List<int> ();
					var matchedSpriteNames = new List<string> ();

					for (int i = 0; i < imagesInLineMatch.Count; i++) {
					var imageToSearch = imagesInLineMatch [i].sprite;
					if (i < imagesInLineMatch.Count - 1) {
					for (int a = i + 1; a < imagesInLineMatch.Count; a++) {

					if (imagesInLineMatch [a].sprite.name == imageToSearch.name) {
					listofMatchedIndexes.Add (i);
					listofMatchedIndexes.Add (a);
					matchedImagesinLineMatch.Add (imagesInLineMatch [i]);
					matchedImagesinLineMatch.Add (imagesInLineMatch [a]);
					matchedSpriteNames.Add (imagesInLineMatch [i].sprite.name);
					matchedSpriteNames.Add (imagesInLineMatch [a].sprite.name);
					}

					}
					}

					}
					matchedSpriteNames = matchedSpriteNames.Distinct ().ToList ();

					#endif

					#if BROKENLINE_ROMOVED

					var matchedImagesinLineMatch = new List<Image> ();
					var listofMatchedIndexes = new List<int> ();
					//					var matchedSpriteNames = new List<string> ();
					var imageToSearch = imagesInLineMatch [0].sprite.name;

					for (int i = 1; i < imagesInLineMatch.Count; i++) {
						if (imagesInLineMatch [i] == imagesInLineMatch [i - 1]) {
							if (i == 1) {
								listofMatchedIndexes.Add (0);
								matchedImagesinLineMatch.Add (imagesInLineMatch [0]);
								//								matchedSpriteNames.Add (imagesInLineMatch [0].sprite.name);
							}
							listofMatchedIndexes.Add (i);
							matchedImagesinLineMatch.Add (imagesInLineMatch [i]);
							//							matchedSpriteNames.Add (imagesInLineMatch [i].sprite.name);
							//							Debug.Log ("Adding " + imagesInLineMatch [i] + " as same at " + i + " and " + (i - 1));
						} else {
							if (imageToSearch == SlotGameManager.instance.wild) {
								if (imagesInLineMatch [i - 1].sprite.name == SlotGameManager.instance.wild) {
									if (i == 1) {
										listofMatchedIndexes.Add (0);
										matchedImagesinLineMatch.Add (imagesInLineMatch [0]);
										//										matchedSpriteNames.Add (imagesInLineMatch [0].sprite.name);
									}
									listofMatchedIndexes.Add (i);
									matchedImagesinLineMatch.Add (imagesInLineMatch [i]);
									//									matchedSpriteNames.Add (imagesInLineMatch [i].sprite.name);
									imageToSearch = imagesInLineMatch [i].sprite.name;
									//									Debug.Log ("spriteToMatch " + imageToSearch + " at " + i);
								} else {

									//									Debug.Log ("Breaking at " + imagesInLineMatch [i] + " as " + (i - 1) + " is not 10 and " + imagesInLineMatch [i - 1] + " is not equal to " + i);
									break;


								}
							} else {
								if (imagesInLineMatch [i].sprite.name == SlotGameManager.instance.wild) {
									if (i == 1) {
										listofMatchedIndexes.Add (0);
										matchedImagesinLineMatch.Add (imagesInLineMatch [0]);
										//										matchedSpriteNames.Add (imagesInLineMatch [0].sprite.name);
									}
									listofMatchedIndexes.Add (i);
									matchedImagesinLineMatch.Add (imagesInLineMatch [i]);
									//									matchedSpriteNames.Add (imagesInLineMatch [i].sprite.name);
								} else {
									if (imagesInLineMatch [i].sprite.name == imageToSearch) {
										if (i == 1) {
											listofMatchedIndexes.Add (0);
											matchedImagesinLineMatch.Add (imagesInLineMatch [0]);
											//											matchedSpriteNames.Add (imagesInLineMatch [0].sprite.name);
										}
										listofMatchedIndexes.Add (i);
										matchedImagesinLineMatch.Add (imagesInLineMatch [i]);
										//										matchedSpriteNames.Add (imagesInLineMatch [i].sprite.name);

									} else {
										//										Debug.Log ("Breaking at " + i);
										break;
									}

								}
							}
						}

					}

					#endif
					//					matchedSpriteNames = matchedSpriteNames.Distinct ().ToList ();
					listofMatchedIndexes = listofMatchedIndexes.Distinct ().ToList ();
					matchedImagesinLineMatch = matchedImagesinLineMatch.Distinct ().ToList ();


					//					Debug.Log ("Matched " + imageToSearch + " at indexes " + listofMatchedIndexes.Print () + " at " + matchedImagesinLineMatch.Print () + " for curent rule " + currentrule);
					//					Debug.Log ("same sprites " + listofMatchedIndexes.Print () + " at positions " + matchedImagesinLineMatch.ConvertAll (x => x.gameObject.name).ToList ().Distinct ().ToList ().Print () + " for current rule " + currentrule);

					if (listofMatchedIndexes.Count >= 3) {
						foreach (var match in matchedImagesinLineMatch) {
							allMatchedImages.Add (match);
						}
						Debug.Log ("matched sprites " + listofMatchedIndexes.Print () + " at positions " + matchedImagesinLineMatch.ConvertAll (x => x.GetComponent<Image> ().sprite.name).Print () + " for current rule " + currentrule + " image to search" + imageToSearch);

						//						Debug.Log ("matched sprites " + listofMatchedIndexes.Print () + " at positions " + matchedImagesinLineMatch.ConvertAll (x => x.gameObject.name).ToList ().Distinct ().ToList ().Print () + " for current rule " + currentrule + " image to search" + imageToSearch);
						var resultshare = int.Parse (betValue.text) /numOfLines;
						#if BROKENLINE_APPLIED
						if (matchedSpriteNames.Count > 1) {
						for (int j = 0; j < matchedSpriteNames.Count; j++) {
						var matchedspriteCount = matchedImagesinLineMatch.FindAll (x => x.sprite.name == matchedSpriteNames [j]).ToList ().Count ();
						Debug.Log (int.Parse (matchedSpriteNames [j]));
						Debug.Log (" count " + (matchedspriteCount - 2));
						Debug.Log (ruleGameArray [int.Parse (matchedSpriteNames [j]), (matchedspriteCount - 2)]);

						var ruleAmount = ruleGameArray [int.Parse (matchedSpriteNames [j]), (matchedspriteCount - 2)];
						var amntWon = long.Parse ((resultshare * ruleAmount).ToString ());
						//								var amntWon = long.Parse (((int.Parse (resultshare.ToString ())) * ruleGameArray [int.Parse (matchedSpriteNames [j]), matchedspriteCount - 1]).ToString ());
						Debug.Log ("Count for " + matchedSpriteNames [j] + " is " + (matchedspriteCount - 2)
						+ " and Amount Won is " + amntWon +
						" for ruleline " + currentrule);
						totalAmountWon += amntWon;
						}
						}
						#endif

						#if BROKENLINE_ROMOVED
						//						if (listofMatchedIndexes.Count > 3) {
						//							for (int j = 0; j < matchedSpriteNames.Count; j++) {
						//								var matchedspriteCount = matchedImagesinLineMatch.FindAll (x => x.sprite.name == matchedSpriteNames [j]).ToList ().Count ();
						//								Debug.Log (int.Parse (matchedSpriteNames [j]));
						//								Debug.Log (" count " + (matchedspriteCount - 2));
						//								Debug.Log (ruleGameArray [int.Parse (matchedSpriteNames [j]), (matchedspriteCount - 2)]);
						var ruleAmount = 0;

						var wildCountInMatch = matchedImagesinLineMatch.FindAll (x => x.GetComponent<Image> ().sprite.name == SlotGameManager.instance.wild).Count;
						if (wildCountInMatch != 0) {
							if ((imageToSearch != SlotGameManager.instance.bonus) && (imageToSearch != SlotGameManager.instance.scatter)) {
								// uncomment if you want to multiply the win with wild by 2.
								//								ruleAmount = ruleGameArray [int.Parse (imageToSearch), (listofMatchedIndexes.Count - 2)] * 2 * wildCountInMatch;
								ruleAmount = ruleGameArray [int.Parse (imageToSearch), (listofMatchedIndexes.Count - 2)] * wildCountInMatch;
							}
						}
						else
							ruleAmount = ruleGameArray [int.Parse (imageToSearch), (listofMatchedIndexes.Count - 2)];
						//						Debug.Log("WILD COUNT " + wildCountInMatch );





						var amntWon = long.Parse ((resultshare * ruleAmount).ToString ());
						//								var amntWon = long.Parse (((int.Parse (resultshare.ToString ())) * ruleGameArray [int.Parse (matchedSpriteNames [j]), matchedspriteCount - 1]).ToString ());
						Debug.Log (" for ruleline " + currentrule + "Count for " + imageToSearch + " is " + (listofMatchedIndexes.Count)
							+ " and Amount Won is " + resultshare +" x "+ruleAmount+" = "+amntWon );
						totalAmountWon += amntWon;
						//							}

						if (amntWon > 0) {
							//							yield return new WaitForSeconds (1f);
							ruleLine.SetActive (true);
							//						if (ruleLine.transform.childCount > 0) {
							//							var ruleLines = ruleLine.transform.GetComponentsInChildren <Image> ().ToList<Image> ();
							//							foreach (var ruleline in ruleLines) {
							//								if (ruleline.sprite.name == currentrule.ToString ()) {
							//									break;
							//								}
							//							}
							//						} 


							var go = new GameObject ();
							var image = go.AddComponent <Image> ();
//							image.sprite = Resources.Load ("GamePlay/" + SlotGameManager.instance.gameName + "RuleLine_" + SlotGameManager.instance.gameName + "/line" + currentrule, typeof(Sprite)) as Sprite;
							image.sprite = lines[currentrule-1];
							go.transform.SetParent (ruleLine.transform, false);
							image.SetNativeSize ();
							foreach (var match in allMatchedImages) {
								var spriteName = match.GetComponent<Image> ().sprite.name;
								//						Debug.Log (String.Format (" Playing animation for matched sprite {0} at {1} for currentrule {2} ", spriteName, matchedImagesinLineMatch [matchedImagesinLineMatch.IndexOf (match)].name, currentrule));

								var animator = match.GetComponent<AnimatableComponent> ();

								animator.Play (spriteName);
							}
						}
					}
					#endif

//					Debug.Log ("totalAmountWon : " + totalAmountWon.ToString ());





				}

				allMatchedImages = allMatchedImages.Distinct ().ToList (); 
			


				for (int i = 0; i < ruleLine.transform.childCount; i++) {
					if (ruleLine.transform.GetChild (i).GetComponent <Image> ().sprite == null)
						Destroy (ruleLine.transform.GetChild (i).gameObject);	
				}

//				if (base.isBonusGame && !SlotGameManager.baseGame.countingFreeSpins) {
//					if (base.isBonusGame) {
//					var bCount = SlotGameManager.instance.bonuses.Count;
//					if (bCount >= 3) {
//						winAmount += ruleGameArray [int.Parse (SlotGameManager.instance.bonus), (SlotGameManager.instance.bonuses.Count - 2)];
//					}
//
//				}
//				}

				winAmount += totalAmountWon;

//				Debug.Log ("Win :" + winAmount.ToString ());
				//				yield return new WaitForSeconds (1f);
				SlotGameManager.baseGame.CalculateStats ();

			}

		}
	}
}