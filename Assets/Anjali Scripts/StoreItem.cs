﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class StoreItem : MonoBehaviour {
	public Text nameText;
	public Text costText;
	public string nameOfProduct;
	public long costOfProduct;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	public void UpdateValues () {
		nameText.text = nameOfProduct;
		costText.text = costOfProduct.ToString();
	}
}
