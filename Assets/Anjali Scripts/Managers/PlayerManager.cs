﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Outback.Gameplay;
using Outback.Core;
using Outback.Views;

namespace Outback
{
	namespace Managers
	{

		public class PlayerManager : Player
		{
			public static PlayerManager ME;
			public BaseGame currentGame = null;
			public bool loggedInAsGuest = true;

			void Awake ()
			{
				ME = this;
				MachineId = SystemInfo.deviceUniqueIdentifier;
			}

			// Use this for initialization
			void Start ()
			{
		
			}
	
			// Update is called once per frame
			void Update ()
			{
		
			}

			public void UpdateUsertable (string toupdate)
			{
				StartCoroutine (UpdateUserDetails (toupdate));
			}

			IEnumerator UpdateUserDetails (string updatefield)
			{
				string url = FBHolder.BaseURL + "updatealluserrecords";
				WWWForm form = new WWWForm ();
				Debug.Log ("Checking Player for UserId : " + Player.UserId + " : " + updatefield + " = " + Player.CurrentCoins);
				if (updatefield == "current_coins") {
//					Debug.Log ("Updating coins");
					form.AddField ("current_coins", Player.CurrentCoins.ToString ());

				}
				form.AddField ("userid", Player.UserId);
				// Create a download object
				WWW download = new WWW (url, form);

				// Wait until the download is done
				yield return download;

				if (!string.IsNullOrEmpty (download.error)) {
					print ("Error downloading: " + download.error);
				} else {
					string json = MiniJSON.JSON.Serialize (download.text);
					var dict = MiniJSON.JSON.Deserialize (download.text) as Dictionary<string, object>;
					Debug.Log ("Player Details" + json);

					if (dict ["response_code"].ToString () == "fail" || dict ["response_code"].ToString () == "unsuccess") {
						Debug.Log ("Player Details" + json);
					} else {
						PlayerManager.ME.Serialize (dict);
						Debug.Log ("Updated PLAYER RECORD");
					}
				}
			}

			public void CheckIfPlayerExists ()
			{
				StartCoroutine ("CheckPlayer");
			}

			IEnumerator CheckPlayer ()
			{
				string url = FBHolder.BaseURL + "checkfacebookid";
				WWWForm form = new WWWForm ();
//		Debug.Log("Checking Player for UserId : " + Player.MachineId);
				// Assuming the perl script manages high scores for different games
				form.AddField ("facebook_id", Player.UserId);
				form.AddField ("machine_id", Player.MachineId);
				form.AddField ("userid", Player.UserId);
				form.AddField ("username", Player.UserName);
//		form.AddField("country", Player.country);
				form.AddField ("email", Player.Email);
//				form.AddField ("mobile_number", Player.mobileNumer.ToString ());
				form.AddField ("password", Player.password);
				// Create a download object
				WWW download = new WWW (url, form);

				// Wait until the download is done
				yield return download;

				if (!string.IsNullOrEmpty (download.error)) {
					print ("Error downloading: " + download.error);
					ErrorPanel.instance.ShowMessage ("There was some error downloading the content!! Please restart the app!!", true);

				} else {
					string json = MiniJSON.JSON.Serialize (download.text);
					var dict = MiniJSON.JSON.Deserialize (download.text) as Dictionary<string, object>;
					Debug.Log ("Player Details" + json);

					if (dict ["response_code"].ToString () == "unsuccess") { 
						LoginScreenController.instance.detailsPanel.SetActive (true);
						LoginScreenController.instance.UpdateDetailsPanel ();
					} else {
						PlayerManager.ME.Serialize (dict);
						AssetBundleHandler.instance.LoadAssetBundle  ("Lobby");
						loggedInAsGuest = false;
						SettingsPanelController.instance.ShowLogoutButton ();
					}
				}

			}


			public void PlayAsGuest ()
			{
				Debug.Log ("Play guest");
				StartCoroutine ("CreateNewGuest");
			}

			IEnumerator CreateNewGuest ()
			{
				if (Application.internetReachability == NetworkReachability.NotReachable) {
					ErrorPanel.instance.ShowMessage ("Please check your internet connection!", true);
				} else {
					string url = FBHolder.BaseURL + "guestregistration";
					//string highscore_url = "http://103.224.243.154/PHP/webservices/outblackslotlive/api/decreasecoin";
					// Create a form object for sending high score data to the server
					WWWForm form = new WWWForm ();
					// Assuming the perl script manages high scores for different games
					form.AddField ("machine_id", Player.MachineId);

					// Create a download object
					WWW download = new WWW (url, form);

					// Wait until the download is done
					yield return download;

					if (!string.IsNullOrEmpty (download.error)) {
						print ("Error downloading: " + download.error);
						ErrorPanel.instance.ShowMessage ("There was some error downloading the content!! Please restart the app!!", true);

					} else {
						var dict = MiniJSON.JSON.Deserialize (download.text) as Dictionary<string, object>;
						Debug.Log ("dict" + dict);
						PlayerManager.ME.Serialize (dict);
						FBHolder.instance.username.text = "Guest";
						AssetBundleHandler.instance.LoadAssetBundle ("Lobby");
						//anjali initialize store here
//						GameObject.FindObjectOfType <InAppPurchase> ().StartInitializing ();
//						ScreenManager.instance.LoadScreenMethod ("Lobby");
						loggedInAsGuest = true;
						SettingsPanelController.instance.HideLogoutButton ();
					}

				}

			}

			public void PlayAsFBPlayer ()
			{
				StartCoroutine ("CreateNewFBPlayer");
			}

			IEnumerator CreateNewFBPlayer ()
			{
				string url = FBHolder.BaseURL + "registration";

				// Create a form object for sending high score data to the server
				WWWForm form = new WWWForm ();
				// Assuming the perl script manages high scores for different games
				form.AddField ("facebook_id", Player.UserId);
				form.AddField ("machine_id", Player.MachineId);
				form.AddField ("userid", Player.UserId);
				form.AddField ("username", Player.UserName);
//        form.AddField("country", Player.country);
				form.AddField ("email", Player.Email);
//				form.AddField ("mobile_number", Player.mobileNumer.ToString ());
				form.AddField ("password", Player.password);


				// Create a download object
				WWW download = new WWW (url, form);
				Debug.Log (Player.UserId + " : " + Player.MachineId + " : " + Player.UserName
					//+" : " +Player.country" : " + Player.mobileNumer +
				+ " : " + Player.Email + " : " + Player.password);
				// Wait until the download is done
				yield return download;

				if (!string.IsNullOrEmpty (download.error)) {
					print ("Error downloading: " + download.error);
					ErrorPanel.instance.ShowMessage ("There was some error downloading the content!! Please restart the app!!", true);

				} else {
//					string json = MiniJSON.JSON.Serialize (download.text);
					var dict = MiniJSON.JSON.Deserialize (download.text) as Dictionary<string, object>;
					Debug.Log ("dict" + download.text);
					Serialize (dict);
					LoginScreenController.instance.detailsPanel.SetActive (false);
					SettingsPanelController.instance.HideLogoutButton ();
					AssetBundleHandler.instance.LoadAssetBundle  ("Lobby");
					loggedInAsGuest = false;
				}

			}

			public void Serialize (Dictionary <string,object> dict)
			{
				UserName = dict ["username"].ToString ();
				MachineId = dict ["machine_id"].ToString ();
				UserId = dict ["userid"].ToString ();
				Email = dict ["email"].ToString ();
				CurrentCoins = long.Parse (dict ["current_coins"].ToString ());
				Level = float.Parse (dict ["level"].ToString ());
				SpentCoins = float.Parse (dict ["spentcoin"].ToString ());
				Debug.Log (string.Format (
					"username: {0}" +
					" machine_id: {1}" +
					" userid: {2} " +
					"email: {3} " +
					"currentcoins: {4}" +
					" level: {5} " +
					"spentcoins{6}",
					UserName, MachineId, UserId, Email, CurrentCoins, Level, SpentCoins));

			}

		}
	}
}
