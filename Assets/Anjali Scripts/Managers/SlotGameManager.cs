﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Linq;
using Bitware.Utils;
using Outback.Core;
using DG.Tweening;
using Outback.Views;

namespace Outback
{
	namespace Managers
	{

		public class SlotGameManager : MonoBehaviour
		{
			public string scatter = "9";
			public string wild = "10";
			public string bonus;
			public Text credits;
			public string gameName = "Safari";
			public List<Sprite> sprites;
			public int spriteCountForThisGame = 11;
			public int totalWild;
			public int totalScatter;
			public int totalBonus;

			public List<SlotReel> slotReels;
			//			public int maxScatter;
			//			public int maxWild;
			public static BaseGame baseGame;
			public List<Image> scatters;
			public List<Image> wilds;
			public List<Image> bonuses;
			public List<int> scatterReels = new List<int> ();
			public List<int> wildReels = new List<int> ();
			public List<int> bonusReels = new List<int> ();

			public static SlotGameManager instance;
			public bool stopInstantly = false;
			public bool canStopReelsInstanty = false;
			public int numberOfFreeSpins;
			public List<Button> buttonsToDisableWhileSpinning;

			public bool spinBtnPressed;
			public bool stopBtnPressed;
			public List<long> baseBonusRewards;
			// Use this for initialization
			void Awake ()
			{
				instance = this;
			}

			void Start ()
			{
				baseGame = GetComponent <BaseGame> ();
//				for (int i = 0; i < spriteCountForThisGame; i++) {
//					sprites.Add (Resources.Load ("GamePlay/" + gameName + "/Slots/" + i, typeof(Sprite)) as Sprite);
//				}
//				Debug.Log ("Wild " + wild + " scatter " + scatter);
				foreach (var slotReel in slotReels) {
					slotReel.SetInitialSprites ();
				}
				if (baseGame.isBonusGame) {
					foreach (var r in baseGame.rewards) {
						for (int i = 0; i < baseBonusRewards.Count; i++) {
							Debug.Log ("at " + i +"for " +  r.name);
							r.bonusRewards [i] = baseBonusRewards [i];
						}
					}
				}
			
				baseGame.spinBtn.gameObject.SetActive (true);
				baseGame.stopBtn.gameObject.SetActive (false);
				baseGame.rewardPopUp.SetActive (false);
			}

			public void GoToLobby ()
			{
				foreach (var sound in SoundManager.instance.gamesBackgroundMusic) {
					sound.enabled = false;
				}
				SoundManager.instance.gamesBackgroundMusic.Clear ();

				AssetBundleHandler.instance.LoadAssetBundle  ("Lobby");
			}

			public void OpenSettings ()
			{
				SettingsPanelController.instance.OpenPanel ();
			}

			public void OpenStorePanel ()
			{
				ScreenManager.instance.OpenStorePanel ();
			}

			Tweener calculateStoppingTime;
			private bool isDown;
			private float downTime;
//			public bool startAutoSpin = false;
//			public bool stopAutoSpin = false;

			public bool autoSpinning;

			public void CheckForStartingAutoSpin ()
			{
				if (!autoSpinning) {
					spinBtnPressed = true;
					print ("CheckForStartingAutoSpin");
					isDown = true;
					downTime = Time.realtimeSinceStartup;
				}
					
					
			}

			public void CheckForStoppingAutoSpin ()
			{	if (autoSpinning) {
					stopBtnPressed = true;
					print ("CheckForStoppingAutoSpin");
					isDown = true;
					downTime = Time.realtimeSinceStartup;
				}

			}

			public void StartingAutoSpin ()
			{
				autoSpinning = true;
				SpinSlots ();
			}

			public void StopAutoSpin ()
			{
				autoSpinning = false;
			}

			public void CheckifSpinButtonUp ()
			{		spinBtnPressed = false;
				print ("CheckifSpinButtonUp");
				if (!autoSpinning) {
					SpinSlots ();
					baseGame.spinBtn.GetComponent <Image> ().sprite = baseGame.autoSpinStartButtonSprite;
					baseGame.stopBtn.GetComponent <Image> ().sprite = baseGame.stopButtonSprite;
				}
				isDown = false;
			}

			public void CheckifStopButtonUp ()
			{		stopBtnPressed = false;
				print ("CheckifStopButtonUp");
				if (!autoSpinning) {
					StopSlottImmediately ();
					baseGame.spinBtn.GetComponent <Image> ().sprite = baseGame.autoSpinStartButtonSprite;
					baseGame.stopBtn.GetComponent <Image> ().sprite = baseGame.stopButtonSprite;
				}
				isDown = false;
			}

			void Update ()
			{
				if (!isDown)
					return;
				if (Time.realtimeSinceStartup - downTime > 2f) {
//					print ("Handle Long Tap");
					// if startAutoSpin = false and spin button is held, Spin Slots for auto spin; 
					if (spinBtnPressed && !autoSpinning) {
						spinBtnPressed = false;
						print ("StartingAutoSpin");
						StartingAutoSpin ();
						baseGame.stopBtn.GetComponent <Image> ().sprite = baseGame.autoSpinStopButtonSprite;

					} else if (stopBtnPressed && autoSpinning) {
						stopBtnPressed = false;
						print ("StopAutoSpin");
						StopAutoSpin ();
						baseGame.spinBtn.GetComponent <Image> ().sprite = baseGame.autoSpinStartButtonSprite;
					}
					isDown = false;
				}
			}

			public void SpinSlots ()
			{
				baseGame.coinsAnim.gameObject.SetActive (false);
				if (Player.CurrentCoins < baseGame.betAmount) {
					autoSpinning = false;
					baseGame.spinBtn.GetComponent <Image> ().sprite = baseGame.spinButtonSprite;
					baseGame.stopBtn.GetComponent <Image> ().sprite = baseGame.stopButtonSprite;
					baseGame.spinBtn.gameObject.SetActive (true);
					baseGame.stopBtn.gameObject.SetActive (false);
					foreach (var btn in buttonsToDisableWhileSpinning) {
//						Debug.Log ("Too less coins");
						btn.interactable = true;
					}
					ErrorPanel.instance.ShowMessage ("Buy some coins or play with lower bet as you don't have sufficient coins to play this spin.", false);


				}
					
				else {
						
					foreach (var btn in buttonsToDisableWhileSpinning) {
//						Debug.Log ("Spinning as coins available");
						btn.interactable = false;
					}

					int a = 0;
					canStopReelsInstanty = true;
					calculateStoppingTime = DOTween.To (x => x = a, a, 2f, 1f).OnComplete (() => {
						canStopReelsInstanty = false;
						StopSlots ();
					});
					baseGame.winAmount = 0;
					baseGame.winValue.text = TextFormatter.GetFormattedNumberText (baseGame.winAmount, true); 
//				baseGame.spinBtn.interactable = false;
					//		baseGame.spinBtn.enabled = false;
					totalWild = 0;
					totalScatter = 0;
					totalBonus = 0;

					scatters.Clear ();
					wilds.Clear ();
					bonuses.Clear ();
//				foreach (var slotReel in slotReels) {
//					
//				}
					if (!baseGame.countingFreeSpins) {
						Player.CurrentCoins -= int.Parse (baseGame.betValue.text);
						baseGame.currentCoins.text = TextFormatter.GetFormattedNumberText (Player.CurrentCoins, true);
					}

					StartCoroutine (SpinReels ());
				}
			}

			public float speedOfSlots;

			IEnumerator SpinReels ()

			{
				baseGame.stopBtn.interactable = true;


				if (!baseGame.countingFreeSpins) {
					baseGame.spinBtn.gameObject.SetActive (false);
					baseGame.stopBtn.gameObject.SetActive (true);
				} else {
					baseGame.spinBtn.gameObject.SetActive (false);
					baseGame.stopBtn.gameObject.SetActive (false);
				}

			
				for (int i = 0; i < baseGame.ruleLine.transform.childCount; i++) {
					Destroy (baseGame.ruleLine.transform.GetChild (i).gameObject);	
				}

				foreach (var slotReel in slotReels) {
					slotReel.spinning = true;

					foreach (var animator in slotReel.GetComponentsInChildren <AnimatableComponent>().ToList ()) {
				
						animator.Stop ();
				
					}		
				
					yield return new WaitForSeconds (0.05f);
				}
			}

			public void StopSlottImmediately ()
			{
				if (canStopReelsInstanty) {
					Debug.Log ("Stopping Slots immediately");

					stopInstantly = true;
					calculateStoppingTime.Complete ();
				
//					StopCoroutine (StopReels ());
//					StartCoroutine (StopReels ());
				}
				baseGame.stopBtn.interactable = false;

			}

			public void StopSlots ()
			{
				StartCoroutine (StopReels ());
			}

			IEnumerator StopReels ()
			{
		

//				if (stopInstantly)
//					yield return new WaitForSeconds (0.05f);
//				else
				yield return new WaitForSeconds (0.05f);

			

				foreach (var slotReel in slotReels) {
				
					slotReel.SetRandomSprites ();
			
					slotReel.spinning = false;
////			int scatter = slotReel.CountScatterInThisReel ();
////			int wild = slotReel.CountWildInThisReel ();
					/// 
////			Debug.Log (string.Format ("wildCount :{0}, scatterCount :{1}",wild,scatter));
//					if (stopInstantly) {
//						if (slotReels.IndexOf (slotReel) == slotReels.Count - 1)
//							yield return new WaitForSeconds (0.01f);
//						else
//							yield return new WaitForSeconds (0.05f);
//					} else
					yield return new WaitForSeconds (0.5f);
//

				}

//				yield return new WaitForSeconds (0.05f);

//				if (stopInstantly)
//				yield return new WaitForSeconds (0.05f);

				var seq = DOTween.Sequence ();
				seq.AppendInterval (0.05f);

//				seq.AppendInterval (1f);
				seq.AppendCallback (() => {


					totalWild = wilds.Count;
					totalScatter = scatters.Count;
					totalBonus = bonuses.Count;


					//				CheckForScatterCount ();
					//				CheckForWildCount ();

					baseGame.DrawRuleLines ();
//					yield return new WaitForSeconds (0.5f);

				});


//				seq.AppendInterval (0f);
//				seq.AppendCallback (() => {
//					baseGame.CalculateStats ();
//
//				});



				seq.AppendInterval (1.5f);

				seq.AppendCallback (() => {
					if (!baseGame.countingFreeSpins) {
						CheckForFreeSpins ();
	
					}

					if (baseGame.isBonusGame) {
						CheckForBonusGame ();

					}
					Debug.Log (string.Format ("wildCount :{0}, scatterCount :{1} , bonusCount :{2}", totalWild, totalScatter, totalBonus));
				});

				seq.AppendInterval (1f);

				seq.AppendCallback (() => {
					if (!baseGame.countingFreeSpins){
						if (autoSpinning) {
							//						yield return new WaitForSeconds (3f);
							Debug.Log ("Restarting SPins as it is in AutoSpin");

							baseGame.spinBtn.GetComponent <Image> ().sprite = baseGame.spinButtonSprite;
							baseGame.stopBtn.GetComponent <Image> ().sprite = baseGame.autoSpinStopButtonSprite;
							foreach (var btn in buttonsToDisableWhileSpinning) {
//								Debug.Log("Turning off btns as autospinning");
								btn.interactable = false;
							}
							SpinSlots ();

						} else if (!autoSpinning) {
							Debug.Log ("No further Spins as  AutoSpin has been stopped");
							stopInstantly = false;
							autoSpinning = false;
							baseGame.spinBtn.GetComponent <Image> ().sprite = baseGame.autoSpinStartButtonSprite;
							baseGame.stopBtn.GetComponent <Image> ().sprite = baseGame.stopButtonSprite;
							foreach (var btn in buttonsToDisableWhileSpinning) {
//								Debug.Log("Turning on btns as autospinning");
								btn.interactable = true;
							}
							baseGame.spinBtn.gameObject.SetActive (true);
							baseGame.spinBtn.interactable = true; 
							baseGame.stopBtn.interactable = true;
							baseGame.stopBtn.gameObject.SetActive (false);
						} 
				
					}
					else{
//						Debug.Log ("counting free spins");
						foreach (var btn in buttonsToDisableWhileSpinning) {
							btn.interactable = false;
						}
						baseGame.spinBtn.gameObject.SetActive (false);
						baseGame.spinBtn.interactable = false; 
						baseGame.stopBtn.interactable = false;
						baseGame.stopBtn.gameObject.SetActive (false);
					}



				});
				seq.Play ();

			}


			public void CheckForFreeSpins ()
			{
				if (totalScatter >= 3) {
					baseGame.stopBtn.gameObject.SetActive (false);
				}
			
				switch (totalScatter) {
				case 3:
					{
						numberOfFreeSpins = 7;
						break;
					}
				case 4:
					{
						numberOfFreeSpins = 10;
						break;
					}
				case 5:
					{
						numberOfFreeSpins = 15;
						break;
					}
				default :
					{
						numberOfFreeSpins = 0;
						break;
					}
			
				}


				baseGame.numberOfFreeSpinsLeft = numberOfFreeSpins - 1;
//				Debug.Log ("numberOfFreeSpins" + numberOfFreeSpins);

				if (!baseGame.countingFreeSpins) {
					//Open Free Spins PopUp here
					if (numberOfFreeSpins > 0) {
						if (autoSpinning) {
							autoSpinning = false;
//							foreach (var btn in buttonsToDisableWhileSpinning) {
//								btn.interactable = false;
//							}
						}
					
						foreach (var reward in baseGame.rewards) {
							reward.SetValues ();
						}
						ScreenManager.instance.playerDetailsPanel.SetActive (false);
						baseGame.coinsAnim.gameObject.SetActive (false);
						baseGame.freeSpinPopUp.gameObject.SetActive (true);

					} else {

						baseGame.freeSpinPopUp.gameObject.SetActive (false);

					}
				}
			}


			void CheckForBonusGame ()
			{

				if (totalBonus >= 3) {
					if (!baseGame.countingFreeSpins) {
						if (autoSpinning) {
							autoSpinning = false;
							foreach (var btn in buttonsToDisableWhileSpinning) {
//								Debug.Log("Turning off btns as Checking for bonus game");
								btn.interactable = false;
							}
						}
						ScreenManager.instance.playerDetailsPanel.SetActive (false);

						baseGame.bonusGamePanel.SetActive (true);

						foreach (var reward in baseGame.rewards) {
							reward.SetValues ();
						}
					}

				}


			}
		}
	}
}
