﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using Outback.Managers;

namespace Outback
{
	namespace Managers
	{
		public class BetManager : MonoBehaviour
		{
			//	public Text totalBetText;

			#region Anjali

			//	long totalBet = 1000;
			public Button incrementButton;
			public Button decrementButton;

			#endregion

			public static long totalBet = 0;
			//	public int incrementValue;
			public int minBet = 10000;
			public int maxBet = 100000;

			public Button AddPlusButton;
			public static long xc;

			void Start ()
			{
				//AddPlusButton.gameObject.SetActive(true);
//		totalBetText.text = BetIncrementValue.ToString ();
//		incrementValue = minBet;
				maxBet = minBet * 16;
				decrementButton.gameObject.SetActive (false);
				totalBet = int.Parse (minBet.ToString ());
				Debug.Log ("Bet " + totalBet);
			}
	
			// Update is called once per frame
			void Update ()
			{
			}

			public void ChangeBet (string function)
			{
				if (function == "Increase") {
					var n = totalBet * 2;
					if (n < maxBet) {


						totalBet = n;
						incrementButton.gameObject.SetActive (true);
					} else {
						totalBet = maxBet;
						incrementButton.gameObject.SetActive (false);
					}
					if (SlotGameManager.baseGame.isBonusGame) {
						foreach (var reward in SlotGameManager.baseGame.rewards) {
							for (int i = 0; i < reward.bonusRewards.Count; i++) {
								reward.bonusRewards[i] = reward.bonusRewards[i] * 2;
							}

						}

					}
					decrementButton.gameObject.SetActive (true);
					SlotGameManager.baseGame.betValue.text = totalBet.ToString ();
					SlotGameManager.baseGame.betAmount = totalBet;
				} else if (function == "Decrease") {
					var n = (totalBet / 2);
					if (n > minBet) {
						totalBet = n;
						decrementButton.gameObject.SetActive (true);
					} else {
						totalBet = minBet;
						decrementButton.gameObject.SetActive (false);
					}
					if (SlotGameManager.baseGame.isBonusGame) {
						foreach (var reward in SlotGameManager.baseGame.rewards) {
							for (int i = 0; i < reward.bonusRewards.Count; i++) {
								reward.bonusRewards[i] = reward.bonusRewards[i] / 2;
							}

						}

					}

					incrementButton.gameObject.SetActive (true);
					SlotGameManager.baseGame.betAmount = totalBet;
					SlotGameManager.baseGame.betValue.text = totalBet.ToString ();
				}
				totalBet = int.Parse (SlotGameManager.baseGame.betValue.text);
			}

			//	public void AddTotalBet ()
			//	{
			//		if (Scroll_slot_machine.scatterWinStatus == false) {
			//			//gameObject.GetComponent<InputField>().placeholder.GetComponent<Text>().text = "Something";
			//			//totalBetV.text= BetIncrementValue.ToString() + BetIncrementValue;
			//			//totalBetV.text= (float) (BetIncrementValue) + BetIncrementValue;
			////			TotalPointC = txtCreadit.text;
			////			TotaluserPoint = TotalPointC.ToString ();
			//
			//			//Debug.Log(TotalPointC);
			//			//int xc = System.Int32.Parse(TotaluserPoint);
			//			xc = System.Int64.Parse (TotaluserPoint);
			//			int Score = StartMovementOfSlot.finalWin;
			//			Debug.Log ("Score" + Score);
			//
			//			if (xc > totalBet) {
			//
			//				// incValue = totalBetV.text.Tolong();
			//				incValue = incValue * 2;
			//				// Debug.Log("incValue :",+incValue);
			//
			//				Debug.Log ("Add to TotalBet :" + totalBet);
			//				Debug.Log ("creadit available :" + xc);
			//
			//				totalBet = incValue; // TotalBet * 2;
			//
			//				totalBetText.text = totalBet.ToString ();
			//
			//			} else {
			//				Debug.Log ("else");
			//				Debug.Log ("Total Bet is heigher than your total points");
			//				AddPlusButton.gameObject.SetActive (false);
			//			}
			//			Debug.Log ("Add Button Click");
			//		}
			//	}
			//
			//	public void MinusTotalBet ()
			//	{
			//		if (Scroll_slot_machine.scatterWinStatus == false) {
			//
			//			if (totalBet > BetMinimumValue) {
			//
			//				totalBet -= BetIncrementValue;
			//				incValue = totalBet;
			//				totalBetText.text = totalBet.ToString ();
			//				AddPlusButton.gameObject.SetActive (true);
			//				Debug.Log ("Button Minus Click");
			//			}
			//		}
			//	}
		}
	}
}