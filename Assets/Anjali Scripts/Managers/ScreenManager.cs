﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using LitJson;

namespace Outback
{
	namespace Managers
	{
		public class ScreenManager : MonoBehaviour
		{
	
			public static string SceneToJumpBack;
			public GameObject loadingScreenObj;
			public GameObject settingPanel;
			public GameObject iapPanel;
			public GameObject promocodePanel;
			public GameObject errorPanel;
			public GameObject playerDetailsPanel;
			public GameObject iapBackBtn;
			//	public GameObject bonusWinPanel;
			public Slider loadingSlider;

			public static ScreenManager instance;

			void Awake ()
			{
				instance = this;
				//var fbholder = new FBHolder();
				//GameObject.Find("Facebook").AddComponent<FBHolder>();
			}
			// Use this for initialization
			void Start ()
			{
		
			}
	
			// Update is called once per frame
			void Update ()
			{
		
			}

			public void LoadScreenMethod (string sceneName)
			{
				Debug.Log ("LOADING " + sceneName);
				StartCoroutine (LoadingNewScreen (sceneName));

			}

			IEnumerator LoadingNewScreen (string sceneName)
			{
				
				loadingSlider.value = 0f;
				AsyncOperation asyncc; 
				loadingScreenObj.SetActive (true);
				asyncc = SceneManager.LoadSceneAsync (sceneName);
				asyncc.allowSceneActivation = false;
			
				while (asyncc.isDone == false) {
					loadingSlider.value = asyncc.progress;

					if (asyncc.progress == 0.9f) {
						loadingSlider.value = 1.0f;
						yield return new WaitForSeconds (2f);
						asyncc.allowSceneActivation = true;

					}
					yield return null;
				}
			}

			public void OpenStorePanel ()
			{
				ScreenManager.instance.iapPanel.SetActive (true);
				ScreenManager.instance.iapBackBtn.SetActive (true);

				if (LobbyController.instance != null) {
					LobbyController.instance.lobbyPanel.SetActive (false);
//					LobbyController.instance.iapButton.SetActive (false);
				}
			}

			public void CloseStorePanel ()
			{
				ScreenManager.instance.iapPanel.SetActive (false);
				ScreenManager.instance.iapBackBtn.SetActive (false);
				if (LobbyController.instance != null) {
					LobbyController.instance.lobbyPanel.SetActive (true);
//					LobbyController.instance.iapButton.SetActive (true);
				}
			}

			public void JumpToLogin ()
			{
				string UserIDSaved = null;
				string strno = @"NO";
				string facebook_id = null;
				PlayerPrefs.SetString ("logflag", strno);
				PlayerPrefs.SetString ("facebook_ids", facebook_id);
				PlayerPrefs.SetString ("UserIDSaved", UserIDSaved);

				FBHolder.instance.LogoutFacebook ();
				SceneManager.LoadScene ("LoginScene");	
			}

			public void ShowPromoCodePanel ()
			{
				ScreenManager.instance.promocodePanel.SetActive (true);
				settingPanel.SetActive (false);
			}

			public void ClosePromoCodePanel ()
			{
				ScreenManager.instance.promocodePanel.SetActive (false);
				settingPanel.SetActive (true);
			}


		}
	}
}