﻿using UnityEngine;
using System.Collections.Generic;
using System;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;
using Bitware.Utils;
using MiniJSON;

namespace Outback {
	namespace Managers {

		public class MasterConfigsManager : MonoBehaviour {

			public static MasterConfigsManager instance;

			public AssetBundle lobbyAtlasAB;
			public Dictionary<string, object> lobbyLayoutJson;
			public Dictionary<string, object> levelUpRewardJson;		//Has reward details for leveling up and XP info
			private Dictionary<string, string> masterGameConfigJson;	//Has links to game config files of each game
			public Dictionary<string, object> storeInformationJson;		//Has store information
			public List<CrossPromoCreative> crossPromotions = new List<CrossPromoCreative>();

			public string configFileBaseUrl = "gameconfigs/";
			public string assetBundleBaseUrl = "bundles/";
			public string preloadersBaseUrl = "preloaders/";

			public static float playerSyncRepeatRate = 30;

			public List<Sprite> COMMONS_ATLAS = new List<Sprite>();
			public List<Sprite> LOBBY_ATLAS = new List<Sprite>();
			public List<Sprite> LOBBY_ATLAS_FROM_AB = new List<Sprite>();

			void Awake () {
				instance = this;
				COMMONS_ATLAS = Resources.LoadAll<Sprite>("Commons_Atlas").ToList();
				LOBBY_ATLAS = Resources.LoadAll<Sprite> ("Lobby_Atlas").ToList ();
			}

			public Sprite GetSpriteFromCommons (string name) {
				return COMMONS_ATLAS.Find(x => x.name == name);
			}

			public void SetupMasterConfigJson (Dictionary<string, object> dictionary) {
				masterGameConfigJson = new Dictionary<string, string>();
				var dict = dictionary["masterGameConfig"] as Dictionary<string, object>;
				foreach(var iterator in dict) {
					masterGameConfigJson.Add(iterator.Key, (string)iterator.Value);

					if(iterator.Key == "configFileBaseUrl")		configFileBaseUrl = (string)iterator.Value;
					if(iterator.Key == "assetBundleBaseUrl")	assetBundleBaseUrl = (string)iterator.Value;
					if(iterator.Key == "preloadersBaseUrl")		preloadersBaseUrl = (string)iterator.Value;
				}
			}
				
			//public string GetGameConfigLink (string gameName) {
			//	if(!masterGameConfigJson.ContainsKey(gameName))
			//		return string.Empty;
			//}

			public void LoadLobbyAtlasSprites () {
				LOBBY_ATLAS_FROM_AB = lobbyAtlasAB.LoadAllAssets<Sprite>().ToList();
				//TODO This is where we can load tex from bytes maybe?
				lobbyAtlasAB.Unload(false);
			}

			public Sprite GetSpriteForGameUnlock (string spriteName) {
//				return lobbyAtlasAB.LoadAllAssets<Sprite>().ToList().Find(x => x.name == spriteName);
				return LOBBY_ATLAS_FROM_AB.Find(x => x.name == spriteName);
			}

			public void SetLastPlayedGame (string gameName) {
				var iconLayout = lobbyLayoutJson["iconLayout"] as List<object>;
				foreach(var iterator in iconLayout) {
					var index = iconLayout.IndexOf(iterator);
					var info = iterator as Dictionary<string, object>;
					if((string)info["name"] == gameName) {
						info["ribbonToUse"] = "Last Played";
						iconLayout[index] = info;
					}
					else if((string)info["ribbonToUse"] == "Last Played")
						info["ribbonToUse"] = string.Empty;
				}
				lobbyLayoutJson["iconLayout"] = (object)iconLayout;
				PlayerPrefs.SetString ("lastPlayedGame", MiniJSON.JSON.Serialize (iconLayout));
			}

			/// <summary>
			/// Could have used 4 lines of code instead, but it would break in Update scenario.
			/// Call Before Setting up lobby icons
			/// </summary>
			public void SetLastPlayedGame(){
				var lastPlayedPP = PlayerPrefs.GetString ("lastPlayedGame", string.Empty);
				if (lastPlayedPP == string.Empty)
					return;
				var lastPlayed = MiniJSON.JSON.Deserialize (lastPlayedPP) as List<object>;

				var iconLayout = (lobbyLayoutJson ["iconLayout"] as List<object>);

				foreach(var iterator in iconLayout) {
					var ind = iconLayout.IndexOf (iterator);
					var info = iterator as Dictionary<string, object>;
					foreach(var item in lastPlayed){
						var i = item as Dictionary<string, object>;
						if((string)i["name"] == (string)info["name"]){
							info["ribbonToUse"] = i["ribbonToUse"];
							iconLayout [ind] = info;
						}
					}
				}
				lobbyLayoutJson["iconLayout"] = (object)iconLayout;
			}
		}

		[System.Serializable]
		public class CrossPromoCreative {
			public Sprite interstitial;
			public Sprite banner;
			public Sprite close;
			public string url = string.Empty;
		}
	}
}