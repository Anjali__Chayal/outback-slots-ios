﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Purchasing;
using Outback.Core;
using Outback.Managers;
using Bitware.Utils;
using System.Text;
using System.IO;
using System.Linq;

public class InAppPurchase : MonoBehaviour
{
	private Sdkbox.IAP _iap;

	public List<GameObject> inAppItems;
	#if UNITY_ANDROID
	public static string Product_10K = "com.outbackslots.android.coinpack10000";
	public static string Product_25K = "com.outbackslots.android.coinpack25000";
	public static string Product_50K = "com.outbackslots.android.coinpack50000";
	public static string Product_75K = "com.outbackslots.android.coinpack75000";
	public static string Product_100K = "com.outbackslots.android.coinpack100000";
	public static string Product_500K = "com.outbackslots.android.coinpack500000";
	public static string Product_1M = "com.outbackslots.android.coinpack1000000";
	public static string Product_5M = "com.outbackslots.android.coinpack5000000";
	public static string Product_10M = "com.outbackslots.android.coinpack10000000";
	#elif UNITY_IOS
	public static string Product100K = "com.outbackslots.ios.coinpack100000";
	public static string Product400K = "com.outbackslots.ios.coinpack400000";
	public static string Product2M = "com.outbackslots.ios.coinpack2000000";
	public static string Product8M = "com.outbackslots.ios.coinpack8000000";
	public static string Product25M = "com.outbackslots.ios.coinpack25000000";

	#endif

	// Use this for initialization
	void Start () {
		_iap = FindObjectOfType<Sdkbox.IAP>();
		if (_iap == null) {
			Debug.Log ("Failed to find IAP instance");
		} else {
//			for (int i = 0; i < _iap.iOSProducts.Count; i++) {
//				inAppItems[i].GetComponent<StoreItem>().costOfProduct 
//			}
			_iap.iOSProducts.ForEach	(x => {
				
				Debug.Log ("Product { ID :" + x.id + " ,NAME :" + x.name + " ,consumable: " + x.consumable + " }");
			});
		}

	
	}

	// Update is called once per frame
	void Update () {
	}

	public void Purchase(string product) {
		if (_iap != null) {
			Debug.Log("About to purchase " + product);
			_iap.getProducts ();
			if (product == "Product100K") {
				//Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
				_iap.purchase(Product100K);
				// The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
				//ScoreManager.score += 100;  //Commented this code to remove error
			}   else if (product == "Product400K") {
				//Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
				_iap.purchase(Product400K);

				// TODO: The non-consumable item has been successfully purchased, grant this item to the player.
			}   else if (product ==  "Product2M") {
				//Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
				_iap.purchase(Product2M);

				// TODO: The subscription item has been successfully purchased, grant this to the player.
			}   else if (product == "Product8M") {
				//Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
				_iap.purchase(Product8M);

				// TODO: The non-consumable item has been successfully purchased, grant this item to the player.
			}   else if (product == "Product25M") {
				//Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
				_iap.purchase(Product25M);
				// TODO: The subscription item has been successfully purchased, grant this to the player.
			}    else {


				Debug.Log ("Else part of the ProcessPurchase Class is  called PurchaseProcessingResult");
				//Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
			}

			// Return a flag indicating whether this product has completely been received, or if the application needs 
			// to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
			// saving purchased products to the cloud, and when that save is delayed. 
			if (LobbyController.instance == null) {
				var creditsInGame = GameObject.FindObjectOfType<SlotGameManager> ().credits;
				var newCredits = Player.CurrentCoins;
				creditsInGame.text = TextFormatter.GetFormattedNumberText (Player.CurrentCoins, true);

			}   else {
				LobbyController.instance.coinsText.text = TextFormatter.GetFormattedNumberText (Player.CurrentCoins, true);
			}
			PlayerManager.ME.UpdateUsertable ("current_coins");

		}
	}

	/*      * Event Handlers      */
	public void onInitialized(bool status) {
		Debug.Log("IAPScript.onInitialized " + status);
	}

	public void onSuccess(Sdkbox.Product product) {
		Debug.Log("IAPScript.onSuccess: " + product.name);
		if (product.name == "Product100K") {
			//Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			Debug.Log ("You just bought" + Product100K);
			Player.CurrentCoins += 100000;

			// The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
			//ScoreManager.score += 100;  //Commented this code to remove error
		}   else if (product.name == "Product400K") {
			//Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			Debug.Log ("You just bought" + Product400K);
			Player.CurrentCoins += 400000;

			// TODO: The non-consumable item has been successfully purchased, grant this item to the player.
		}   else if (product.name ==  "Product2M") {
			//Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			Debug.Log ("You just bought" + Product2M);
			Player.CurrentCoins += 2000000;
			// TODO: The subscription item has been successfully purchased, grant this to the player.
		}   else if (product.name == "Product8M") {
			//Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			Debug.Log ("You just bought" + Product8M);
			Player.CurrentCoins += 8000000;
			// TODO: The non-consumable item has been successfully purchased, grant this item to the player.
		}   else if (product.name == "Product25M") {
			//Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
			Debug.Log ("You just bought" + Product25M);
			Player.CurrentCoins += 25000000;
			// TODO: The subscription item has been successfully purchased, grant this to the player.
		}    else {


			Debug.Log ("Else part of the ProcessPurchase Class is  called PurchaseProcessingResult");
			//Debug.Log(string.Format("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
		}

		// Return a flag indicating whether this product has completely been received, or if the application needs 
		// to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
		// saving purchased products to the cloud, and when that save is delayed. 
		if (LobbyController.instance == null) {
			var creditsInGame = GameObject.FindObjectOfType<SlotGameManager> ().credits;
			var newCredits = Player.CurrentCoins;
			creditsInGame.text = TextFormatter.GetFormattedNumberText (Player.CurrentCoins, true);

		}   else {
			LobbyController.instance.coinsText.text = TextFormatter.GetFormattedNumberText (Player.CurrentCoins, true);
		}
		PlayerManager.ME.UpdateUsertable ("current_coins");
		_iap.finishTransaction (product.name);
	}

	public void onFailure(Sdkbox.Product product, string message) {
		Debug.Log("IAPScript.onFailure for product "+product.name +" with Product id : "+product.id + " with message "+ message);
	}

	public void onCanceled(Sdkbox.Product product) {
		Debug.Log("IAPScript.onCanceled product: " + product.name +" with Product id : "+product.id );
	}

	public void onRestored(Sdkbox.Product product) {
		Debug.Log("IAPScript.onRestored: " + product.name +" with Product id : "+product.id );
	}

	public void onProductRequestSuccess(Sdkbox.Product[] products) {
		foreach (var p in products) {
			Debug.Log("Product: " + p.name + " price: " + p.price);
		}
	}
	public void onProductRequestFailure(string message) {
		Debug.Log("IAPScript.onProductRequestFailure: " + message);
	}
	public void onRestoreComplete(string message) {
		Debug.Log("IAPScript.onRestoreComplete: " + message);
	}

}
