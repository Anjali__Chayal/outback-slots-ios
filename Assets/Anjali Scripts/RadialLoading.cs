﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class RadialLoading : MonoBehaviour {

	public Slider radialSlider;
	// Use this for initialization
	public void StartRadialLoad () {
		float a = 0;
		DOTween.To (x=>a = x, 0f, 1f, 1f).OnUpdate(()=> {
			radialSlider.value = a;
		}).OnComplete (() => {
			StartRadialLoad ();
		});
	}
	
	void Start(){
		StartRadialLoad ();
	}

}
