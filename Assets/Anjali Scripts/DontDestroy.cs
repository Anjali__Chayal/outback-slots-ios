﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class DontDestroy : MonoBehaviour
{
	public static DontDestroy instance;
	public List<GameObject> gos = new List<GameObject> ();

	void Awake ()
	{
		var dontDestroys = GameObject.FindObjectsOfType <GameObject> ().ToList ();
		foreach (var item in dontDestroys) {
			if (item.name == gameObject.name)
				gos.Add (item);
		}
		if (gos.Count > 1) {
			var go = gos [0];
			Destroy (go);
			gos.Remove (go);
		} 
		if (gos.Count == 1)
			DontDestroyOnLoad (gameObject);


//		if (instance == null) {
//			instance = this;
//		} else if (instance != this) {
//			Destroy (gameObject);  
//		}
//		DontDestroyOnLoad (gameObject);

	}
	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}
}
