﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Outback.Core;
using Facebook.Unity;
using Outback.Managers;
using Bitware.Utils;
using System;
using Outback.Views;

public class LobbyController : MonoBehaviour
{

	public Color colorfade = Color.white;
	public GameObject lobbyPanel;
	public Text coinsText;
	public static LobbyController instance;
	public GameObject iapButton;
	public GameObject friendsPanel, friendsToInvitePanel;
	public GameObject friendDetailsPrefab, inviteFriendDetailsPrefab;
	public GameObject friendsPanelParent, friendsToInvitePanelParent;
	public GameObject friendsDisplayText;

	void Awake ()
	{
		instance = this;
	}



	// Use this for initialization
	void Start ()
	{
		coinsText.text = TextFormatter.GetFormattedNumberText (Player.CurrentCoins, true);
		ScreenManager.instance.iapBackBtn.SetActive (false);
		ScreenManager.instance.loadingScreenObj.SetActive (false);
		ScreenManager.instance.promocodePanel.gameObject.SetActive (false); 
		ScreenManager.instance.playerDetailsPanel.gameObject.SetActive (true);
		PopulateFriendsPanel (FBHolder.instance.facebookFriends);
		//to invite friends #obselete
//		PopulateFriendsToInvitePanel (FBHolder.instance.allInvitableFriends);
		FindObjectOfType<DailyReward> ().CheckSpinWheel ();
		//		Screen.sleepTimeout = SleepTimeout.NeverSleep;
//		GameObject.Find ("CoinsText").GetComponent<Text> ().text = Player.instance.coins.ToString ();
	}
	
	// Update is called once per frame
	void Update ()
	{
		
	}

	public void Share(){
		if (!FB.IsLoggedIn) {
			if (Application.internetReachability == NetworkReachability.NotReachable ) {
				ErrorPanel.instance.ShowMessage ("Please check your internet connection!",true);
			} else {
				FBHolder.instance.FBlogin ();

			}

		} 
		if (FB.IsLoggedIn) {
			string shareLink = "";
			#if UNITY_ANDROID
			shareLink = "https://play.google.com/store/apps/details?id=com.bitware.outbackslotstest&hl=en";
			#elif UNITY_IOS
			shareLink = "";
			#endif

			System.Uri url = new Uri (shareLink);
			var message = "Come and play this game! Its Awesome!!";
			var title = "Outback Slots, Pokies and Games";
			System.Uri photourl = new Uri ("https://lh3.googleusercontent.com/9rNB5QFbZRz-PC5ysEMfi9RWdwvNooMUJAOKbX7gyOyOhiFm6w3sl_f1D7_2jwpPGMY=w300");
			FB.ShareLink (url, title, message, photourl, delegate (IShareResult result) {
				if (!result.Cancelled || (result.Error != null || result.Error != String.Empty)) {
					Player.CurrentCoins += 10000;
					LobbyController.instance.coinsText.text = TextFormatter.GetFormattedNumberText (Player.CurrentCoins, true);
					PlayerManager.ME.UpdateUsertable ("current_coins");
				} else {
					ErrorPanel.instance.ShowMessage ("There was some error Sharing the content!! Retry after some time!", true);
				}
			});
		}

	}

	public void OpenStore ()
	{
		ScreenManager.instance.OpenStorePanel ();

	}

	public void OpenLobby ()
	{
		ScreenManager.instance.CloseStorePanel ();
	}

	public void OpenSettingsPanel ()
	{
		SettingsPanelController.instance.OpenPanel ();
	}

    
	public void LoadScreen (string sceneName)
	{
		AssetBundleHandler.instance.LoadAssetBundle (sceneName);
	}

	public void ShowFriendsPanel ()
	{
		if (PlayerManager.ME.loggedInAsGuest)
			friendsDisplayText.SetActive (true);
		else
			friendsDisplayText.SetActive (false);
		
		friendsPanelParent.SetActive (true);
		lobbyPanel.SetActive (false);
	}



	public void HideFriendsPanel ()
	{
		friendsPanelParent.SetActive (false);
		lobbyPanel.SetActive (true);
	}
	#region Invite Panel

//	public void InviteFBFriends ()
//	{
//
//		LobbyController.instance.ShowFriendsForInvitePanel ();
//	}
//	public void ShowFriendsForInvitePanel ()
//	{
//		friendsToInvitePanelParent.SetActive (true);
//		lobbyPanel.SetActive (false);
//	}
//	public void HideFriendsForInvitePanel ()
//	{
//		friendsToInvitePanelParent.SetActive (false);
//		lobbyPanel.SetActive (true);
//	}
//
//	public void PopulateFriendsToInvitePanel (List <FacebookFriend> friendsList)
//	{
//		//		var panelRect = friendsPanel.GetComponent <RectTransform> ();
//		//		panelRect.size =new Vector2(panelRect.size.x,friendDetailsPrefab.GetComponent <RectTransform>().rect.size.y);
//		foreach (var friend in friendsList) {
//			var friendInfo = Instantiate (inviteFriendDetailsPrefab, friendsToInvitePanel.transform)as GameObject;
//			friendInfo.transform.Find ("Name").GetComponent <Text> ().text = friend.name;
//			friendInfo.transform.GetComponent <FBInviteManager> ().id = friend.id;
//			Debug.Log ("invitind friend with ID " + friend.id);
////			FB.API (FBHolder.instance.GetPictureURL (friend.id.ToString (), 128, 128), HttpMethod.GET, delegate (Facebook.Unity.IGraphResult pictureResult) {
////				if (pictureResult != null)
////					Debug.Log ("picture Result " + pictureResult.RawResult);
////					friend.imageUrl = FBHolder.instance.DeserializePictureURLString (pictureResult.RawResult.ToString ());
//
//			StartCoroutine (LoadPictureEnumerator (friend.imageUrl, pictureTexture => {
//				
////				var sprite = Sprite.Create (pictureTexture, new Rect (0, 0, 50, 50), new Vector2 (0, 0));
//				friendInfo.transform.Find ("Mask").Find ("Profile Pic").GetComponent <Image> ().sprite = Sprite.Create (pictureTexture, new Rect (0, 0, pictureTexture.width, pictureTexture.height), new Vector2 (0, 0));
////				var spriteTofill  =friendInfo.transform.Find ("Mask").Find ("Profile Pic").GetComponent <Image> ().sprite ;
////				spriteTofill = sprite;
////				spriteTofill.texture.EncodeToJPG (100);
//			}));
//
////			});
//
//		}
//		HideFriendsForInvitePanel ();
//
//		//		friendsPanel.GetComponent <RectTransform>().rect.size = new Vector2 (friendDetailsPrefab.);
//	}

	#endregion

	public void PopulateFriendsPanel (List <FacebookFriend> friendsList)
	{
//		var panelRect = friendsPanel.GetComponent <RectTransform> ();
//		panelRect.size =new Vector2(panelRect.size.x,friendDetailsPrefab.GetComponent <RectTransform>().rect.size.y);
		foreach (var friend in friendsList) {
			var friendInfo = Instantiate (friendDetailsPrefab, friendsPanel.transform)as GameObject;
			friendInfo.transform.Find ("Name").GetComponent <Text> ().text = friend.name;
			StartCoroutine (LoadPictureEnumerator (friend.imageUrl, pictureTexture => {
				friendInfo.transform.Find ("Mask").Find ("Profile Pic").GetComponent <Image> ().sprite = Sprite.Create (pictureTexture, new Rect (0, 0, 128, 128), new Vector2 (0, 0));
			}));
			//friendInfo.transform.Find ("Pic").GetComponent <Text>().text = friend.name;
		}
		HideFriendsPanel ();

//		friendsPanel.GetComponent <RectTransform>().rect.size = new Vector2 (friendDetailsPrefab.);
	}

	delegate void LoadPictureCallback (Texture2D texture);

	IEnumerator LoadPictureEnumerator (string url, LoadPictureCallback callback)
	{
		WWW www = new WWW (url);
		yield return www;
		callback (www.texture);
	}
	//
	//	IEnumerator LoadingNewScreen (string sceneName)
	//	{
	//		AsyncOperation asyncc;
	//		LoadingScreenObj.SetActive (true);
	//		asyncc = SceneManager.LoadSceneAsync (sceneName);
	//		asyncc.allowSceneActivation = false;
	//
	//		while (asyncc.isDone == false) {
	//			LoadingSlider.value = asyncc.progress;
	//
	//			if (asyncc.progress == 0.9f) {
	//				LoadingSlider.value = 1.0f;
	//				asyncc.allowSceneActivation = true;
	//			}
	//			yield return null;
	//		}
	//	}

}
