﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using Outback.Managers;
using Bitware.Utils;

public class SoundManager : MonoBehaviour
{
	
	public AudioSource backgroundMusic;
	public List<AudioSource> gamesBackgroundMusic = new List<AudioSource> ();
	public bool musicFlag = true;

	public static SoundManager instance;
 
	// Use this for initialization
	void Awake ()
	{
		instance = this;
	}


	void Start ()
	{
//		sounds = transform.GetComponents <AudioSource> ().ToList ();
	}

	public void Play (string gameName)
	{

	}
	//	 Update is called once per frame
	void Update ()
	{

		if (musicFlag) {
			foreach (var sound in gamesBackgroundMusic) {
				if (!sound.isPlaying) {
					sound.Play ();
				} 
//				else {
////					sound.Play ();
////					Debug.Log("Playing Music already");
//				}
			}

	
		} else {
			foreach (var sound in gamesBackgroundMusic) {
				sound.Stop ();
//				Debug.Log("Stopping Music.");
			}
		
			//musicSource.GetComponent<AudioSource>().Stop();
			//musicSourceFreeScatter.GetComponent<AudioSource>().Stop();
		}
	
	
	}
}
