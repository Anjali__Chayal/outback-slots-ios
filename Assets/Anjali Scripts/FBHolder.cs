﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Facebook.Unity;
using MiniJSON;
using System.Collections.Generic;
using System;
using System.Text;
using System.Text.RegularExpressions;
using System.IO;
using LitJson;
using UnityEngine.SceneManagement;
using System.Diagnostics;
using Debug = UnityEngine.Debug;
using Outback.Core;
using Outback.Managers;
using Outback.Views;
using System.Linq;
using Bitware.Utils;

public class FBHolder : MonoBehaviour
{
	public static FBHolder instance;
	//string url_link = "http://74.208.12.101/OutSlotGame/insert.php";
	public string applink = "https://fb.me/148666535902884";

	public string get_data;
	public string fbname;
	//public Texture2D profilePic;

	//public	GameObject dialogFBloggedIN;
	//public	GameObject dialogFBLoggedOut;
	//public	GameObject dialogUserName;
	public  Image displayprofilepic;
	//public  GameObject dic2;

	public  string userIDSaved;
	public  string userFacebookID;

	public Text emailID;
	public Text username;

    //JSON DATA
#pragma warning disable CS0436 // Type conflicts with imported type
    private LitJson.JsonData requiredData;
#pragma warning restore CS0436 // Type conflicts with imported type
	//private string jsonString;
//	public int level;
//	public float timeElapsed;
//	public string playerName;
	public string facebook_id;
	public string emailID_user;
//	public int FBID = 0;
	public static string BaseURL = "http://103.224.243.154/PHP/webservices/outblackslotlive/api/";
	private string gameDataFileName = "data.json";

	void Start ()
	{

	}

	void Awake ()
	{
		instance = this;
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		//	FB.Init(SetInt, OnHideUnity);  //OLd Code
		if (!FB.IsInitialized) {        //New Code
			// Initialize the Facebook SDK
			FB.Init (InitCallback, OnHideUnity);
		} else {
			// Already initialized, signal an app activation App Event
			FB.ActivateApp ();
		}
	}

	private void SetInt ()
	{
		if (Application.internetReachability == NetworkReachability.NotReachable ) {
			ErrorPanel.instance.ShowMessage ("Please check your internet connection!",true);
		} else {
			FBlogin ();
			LoginScreenController.instance.guestButton.SetActive(false);
			LoginScreenController.instance.fbLoginButton.SetActive(false);
		}

    }

	private void InitCallback ()
	{
		if (FB.IsInitialized) {
			// Signal an app activation App Event
			FB.ActivateApp ();
			// Continue with Facebook SDK
			// ...
		} else {
			Debug.Log ("Failed to Initialize the Facebook SDK");
//			username.text = "Failed to Initialize the Facebook SDK";
		}
	}


	private void OnHideUnity (bool isGameShown)
	{
		if (!isGameShown) {
			// Pause the game - we will need to hide
			Time.timeScale = 0;
		} else {
			// Resume the game - we're getting focus again
			Time.timeScale = 1;
		}
	}


	public void FBlogin ()
	{
		//var perms = new List<string>(){"public_profile", "email", "user_friends"};
		launchImage.constantvaluestored = @"called FB Login YES";
		List<string> perms = new List<string> ();
		perms.Add ("public_profile");
        perms.Add("email");
		perms.Add ("user_friends");
		FB.LogInWithReadPermissions (perms, AuthCallback);


        //SetInt();

        //ShareLink ();
    }

    public void LogoutFacebook ()
	{
		FB.LogOut ();
		SettingsPanelController.instance.ClosePanel ();
		AssetBundleHandler.instance.LoadAssetBundle ("LoginScene");

	}

	#region InvitePanel
//	public void PopulateFriendsForInvite(){
//		FB.API ("me/invitable_friends?fields=id,first_name,last_name,picture.width(128).height(128)&limit=10000", HttpMethod.GET,GetInvitableFriends);   
//	}
//
//	public List<FacebookFriend> allInvitableFriends = new List<FacebookFriend> ();
//	void GetInvitableFriends(IResult result){
//		if (result.Error == null) {
//			var dictionary = (Dictionary<string, object>)JSON.Deserialize (result.RawResult);
//			Debug.Log ("friend list " + JSON.Serialize (dictionary));
//			var friendsList = dictionary ["data"] as  List<object>;
//			//			string friend = string.Empty;
//
//			if (friendsList.Count != 0) {
//				foreach (var item in friendsList) {
//					var dict  = item as Dictionary<string,object>;
//					Debug.Log ("invitable friends : " + dict.Print ());
//					var fbFriend = new FacebookFriend ();
//					fbFriend.name = dict ["first_name"].ToString ()+" " + dict ["last_name"].ToString ();
//					fbFriend.id = dict ["id"].ToString ();
//
//					var pictDetails = dict["picture"]as Dictionary<string,object>;
//					var pictData = pictDetails["data"]as Dictionary<string,object>;
//					fbFriend.imageUrl = pictData ["url"].ToString ();
////					var s = fbFriend.imageUrl.Split('/');
////					var sublink = s.Last();
////					var id = sublink.Split ('_') [1];
////					Debug.Log ("fb id for " + fbFriend.name + " is " + fbFriend.id + " or " + id);
////					fbFriend.id = id;
////					FB.API(GetPictureURL(fbFriend.id.ToString(), 128, 128), HttpMethod.GET, delegate (Facebook.Unity.IGraphResult pictureResult)
////						{
////							fbFriend.imageUrl = DeserializePictureURLString(pictureResult.RawResult.ToString());
////							Debug.Log("IMAGE URL " + fbFriend.imageUrl);
////
////						});
//					allInvitableFriends.Add (fbFriend);
//					allInvitableFriends.Sort ((p1,p2)=>p1.name.CompareTo(p2.name));
//				}
//
//			} else {
//				Debug.Log ("NO FRIENDS to Invite");
//			}
//		}
//		else {
//			ErrorPanel.instance.ShowMessage ("There was some error downloading the content!! Please restart the app!!",true);
//			Debug.Log (result.Error);
//		}
//	}
//	void AuthCallbackPublish (ILoginResult result)
//	{
//
//		if (result.Error != null) {
//			//userNameTest.text = result.Error;
//			Debug.Log (result.Error);	
//			ErrorPanel.instance.ShowMessage ("There was some error downloading the content!! Please restart the app!!",true);
//		} else {
//			//dealwithMenus (FB.IsLoggedIn);
//			Debug.Log (result.ResultDictionary.ToList().Print());
//			DealwithMenus (FB.IsLoggedIn);
//
//		}
//
//	}
	#endregion
//	public void FBInvite ()
//	{
//
//		//  https://fb.me/1725666294158388
//		//"https://play.google.com/store/apps/details?id=com.bitware.outbackstest"
//
//		#if UNITY_IOS
//		FB.Mobile.AppInvite (new System.Uri (applink));
//		Debug.Log("called IOS Invite");
//		#endif
//
//		#if UNITY_ANDROID
////		FB.Mobile.AppInvite(new Uri("https://fb.me/148666535902884"),null,delegate (IAppInviteResult result){
////			InviteCallback (result);
////		});
//		Debug.Log ("called Android Invite");
//		#endif
//
//		//FB.Mobile.AppInvite (new System.Uri (applink));
//	}
//
//	private void InviteCallback(IResult result)
//	{
//		if (result != null)
//		{
//			var responseObject = JSON.Deserialize(result.RawResult) as Dictionary<string, object>;
//			IEnumerable<object> objectArray = (IEnumerable<object>)responseObject["to"];
//
//			Debug.Log (objectArray.ToList ().Print ());
//		}
//	}

//	protected void HandleResult(IResult result)
//	{
//		if (result == null)
//		{
//			Debug.Log ("result is null");
//			return;
//		}
//
//
//
//		// Some platforms return the empty string instead of null.
//		if (!string.IsNullOrEmpty(result.Error))
//		{
//			Debug.Log ("Error : " +result.Error );
//		}
//		else if (result.Cancelled)
//		{
//			Debug.Log ("cancelled .");
//		}
//		else if (!string.IsNullOrEmpty(result.RawResult))
//		{
//			Debug.Log ("result : "+result.RawResult);
//			InviteCallback (result);
//		}
//		else
//		{
//			Debug.Log ("other result .");
//		}
//
//
//	}

	
	void AuthCallback (ILoginResult result)
	{
		
		if (result.Error != null || result.Cancelled) {
			username.text = "Guest";
			Debug.Log (result.Error);	
			ErrorPanel.instance.ShowMessage ("There was some error downloading the content!! Please try again and check your internet connection!!",false);
			if (Application.loadedLevelName.Contains ("LoginScene")) {
				LoginScreenController.instance.fbLoginButton.SetActive (true);
				LoginScreenController.instance.guestButton.SetActive (true);
			}

		} else {
			if (FB.IsLoggedIn) {
				Debug.Log ("FB Login worked");
				#region invite friends (obselete)
//				List<string> perms = new List<string> ();
//				perms.Add ("publish_actions");
//				FB.LogInWithPublishPermissions (perms, AuthCallbackPublish);

				#endregion
				// FB.API ("me?fields=first_name",HttpMethod.GET,DisplayUserName);
				// ShareLink ();
				// Application.LoadLevel("MainScene");
				//DealwithMenus (FB.IsLoggedIn);

//                username.text = result.Error;
			} else {
				//dealwithMenus (FB.IsLoggedIn);
				Debug.Log ("FB Login failed");
			}
//            username.text = result.Error;
			DealwithMenus (FB.IsLoggedIn);

        }
    }

	// Get Facebook Details
	public void PopulateFriendsList(){
		FB.API ("me/friends?fields=id,first_name,last_name", HttpMethod.GET,GetFriendList);   
	}

	public void DealwithMenus (bool isloggedin)
	{
		if (isloggedin) {
			//dialogFBloggedIN.SetActive (true);
			//dialogFBLoggedOut.SetActive (false);

			FB.API ("me?fields=first_name,last_name", HttpMethod.GET, DisplayUserName);                                  // First   Firstname
			FB.API ("me?fields=id", HttpMethod.GET, GetUserID);                                               // Second   user Facebook ID
			//First Code To get the Facebook FriendList Working
			PopulateFriendsList();
			#region invite friends (obselete)
//			PopulateFriendsForInvite ();
			#endregion
			// Fourth Facebook Friend List 

		  
			//Second Code To get the Facebook FriendList Working
			//FB.API ("me?fields=friends.limit(10).fields(first_name,id)",HttpMethod.GET,FriendList);                           			     
		
			//Other Code to get Facebook Friendlist
			//FB.API ("me/friends.limit(10).fields(first_name,id)", HttpMethod.GET,FriendList);              // Fourth Facebook Friend List 
			//FB.API ("me?fields=friends{name,id}",HttpMethod.GET,FriendList);                           			     // Fourth Facebook Friend List 

			//	me?fields=friends.limit(10).fields(first_name,id)
			//  friends.limit(10).fields(first_name,id)   fields=friends{name,id}   me?fields=friends{name,id}  me?fields=friends{name,id}
			//  FB.API("/me/friends?fields=id,name", …)  ///Other Code      Facebook Friend List
			//  FB.API ("me?picture?type=square&height=128&width=128",HttpMethod.GET,DisplayProfilePicture); //Fourth
            FB.API("me?fields=email", HttpMethod.GET, UserEmail);
            //add extended permissions on developer console on facebook, to have this call
//           FB.API("me?fields=hometown", HttpMethod.GET, GetCountry);
        }
    }
	//call to get country
//    void GetCountry(IResult result)              // Second  user email 
//    {
//        Debug.Log("Country" + result.RawResult.ToString());
//        //Text emailtxt = dialogUserName.GetComponent<Text> ();
//
//        if (result.Error == null)
//        {
//			if (result.ResultDictionary.ContainsKey ("hometown")) {
//				var dict = result.ResultDictionary ["hometown"] as Dictionary<string, object>;
//				var place = dict ["name"].ToString ();
//				Player.country = place.Split (',') [1];
//				Debug.Log ("Country :"+Player.country);
//			} else {
//				Player.country = "";
//			}
//        }
//        else
//        {
//            Debug.Log(result.Error);
//        }
//    }
    void DisplayUserName (IResult result)   // First   Username of Facebook
	{
        Debug.Log("DisplayUserName" + result.RawResult.ToString());
		//Text UserName = dialogUserName.GetComponent<Text> ();

		if (result.Error == null) {
	         Player.UserName = result.ResultDictionary ["first_name"] + " " + result.ResultDictionary["last_name"];
            username.text = Player.UserName;
			Player.UserId = result.ResultDictionary["id"].ToString();
			
			PlayerPrefs.SetString ("facebook_id", Player.UserId);
            FB.API(GetPictureURL(Player.UserId.ToString(), 128, 128), HttpMethod.GET, delegate (Facebook.Unity.IGraphResult pictureResult)
            {
                string imageUrl = DeserializePictureURLString(pictureResult.RawResult.ToString());
                Debug.Log("IMAGE URL " + imageUrl);
                StartCoroutine(LoadPictureEnumerator(imageUrl, pictureTexture =>
                {
                    displayprofilepic.sprite = Sprite.Create(pictureTexture, new Rect(0, 0, 128, 128), new Vector2(0, 0));
                }));

                //Called here because this is the last data being loaded
                PlayerManager.ME.CheckIfPlayerExists();


            });
        } else {
//            username.text = result.Error;
			Debug.Log (result.Error);
		}
	}

	public List<FacebookFriend> facebookFriends;

	void GetFriendList(IResult result){

		if (result.Error == null) {
			var dictionary = (Dictionary<string, object>)JSON.Deserialize (result.RawResult);
			Debug.Log ("friend list " + JSON.Serialize (dictionary));
			var friendsList = dictionary ["data"] as  List<object>;
//			string friend = string.Empty;

			if (friendsList.Count != 0) {
				foreach (var item in friendsList) {
					var dict  = item as Dictionary<string,object>;
					var fbFriend = new FacebookFriend ();
					fbFriend.name = dict ["first_name"].ToString ()+" " + dict ["last_name"].ToString ();
					fbFriend.id = dict ["id"].ToString ();
					Debug.Log ("friend name : " + fbFriend.name + " id : " + fbFriend.id);
					FB.API (FBHolder.instance.GetPictureURL (fbFriend.id.ToString (), 128, 128), HttpMethod.GET, delegate (Facebook.Unity.IGraphResult pictureResult) {
						fbFriend.imageUrl = FBHolder.instance.DeserializePictureURLString (pictureResult.RawResult.ToString ());

						Debug.Log ("IMAGE URL " + fbFriend.imageUrl);
					

					});
					facebookFriends.Add (fbFriend);
				}

			} else {
				Debug.Log ("NO FRIENDS");
			}
		}
		else {
			ErrorPanel.instance.ShowMessage ("There was some error downloading the content!! Please restart the app!!",true);
			Debug.Log (result.Error);
		}

		//	friend += "name : "+((Dictionary<string, object>)dict)["name"]+" id :" +((Dictionary<string, object>)dict)["id"]+"\\n" ;

	}

	void UserEmail (IResult result)              // Second  user email 
	{
        Debug.Log("UserEmail" + result.RawResult.ToString());
        //Text emailtxt = dialogUserName.GetComponent<Text> ();
        if (result.Error == null) {
			if (result.ResultDictionary.ContainsKey ("email")) {
				Player.Email = result.ResultDictionary ["email"].ToString ();
				//emailID.text = Player.Email
			} else {
				Player.Email = "";
			}
			Debug.Log ("Email : " + Player.Email);
        } else {
            emailID.text = result.Error;
			Debug.Log (result.Error);
		}
	}

	
	void GetUserID (IResult result)  // Third   user Facebook ID
	{
        //		Text UserName = dialogUserName.GetComponent<Text> ();
//        Debug.Log("IDUserName" + result.RawResult.ToString());
        if (result.Error == null) {
			if (result.ResultDictionary.ContainsKey ("id")) {
				facebook_id = result.ResultDictionary ["id"].ToString ();

				//ON console
//			print (facebook_id);
				Player.UserId = facebook_id;
				// Debug.Log (UserName.text);
				//Debug.Log (facebook_id);
//			PlayerPrefs.SetString ("facebook_id", facebook_id);
				//saveFaceid.savefbid();

				//Anjali Commented this to check
				//StartCoroutine (Check_facebookID ()); 
				//StopCoroutine (Check_facebookID ()); 
			} else {
				Player.UserId  = "";
			}
			Debug.Log ("ID : " + Player.UserId);

		} else {
//            username.text = result.Error;
			Debug.Log (result.Error);
		}
	}


	void Check_emailID_Present_OR_NOTPresent ()   //Check for EmailID is Present for User ID or Not
	{
		Debug.Log ("********** Check Email ID Called ***************");
		Debug.Log (emailID_user);

		bool check = IsEmail (emailID_user);

		if (check) {

			print ("Email id present in facebook");
			// UserEnteredEmailId
			//StartCoroutine (Start1 ()); 
			//StopCoroutine (Start1 ()); 
		

		} else {

			//RectTransform myRectTransform = emailID.rectTransform;
			//myRectTransform.localPosition = new Vector3 (0, 3, 0);

			print ("email id not present in the facebook id");
		}
	}

	//Get the email Id Text from EmailId Screen
	//NEW http://103.224.243.154/PHP/webservices/outblackslotlive/api/checkfacebookid
	//OLD http://103.224.243.154/PHP/webservices/outbackslot/check_facebook_id.php



	///Email Validation
	/// <summary>
	/// Regular expression, which is used to validate an E-Mail address.
	/// </summary>
	public const string MatchEmailPattern = 
		@"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
		+ @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
		+ @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?
				[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
		+ @"([a-zA-Z0-9]+[\w-]+\.)+[a-zA-Z]{1}[a-zA-Z0-9-]{1,23})$";

	/// <summary>
	/// Checks whether the given Email-Parameter is a valid E-Mail address.
	/// </summary>
	/// <param name="email">Parameter-string that contains an E-Mail address.</param>
	/// <returns>True, when Parameter-string is not null and 
	/// contains a valid E-Mail address;
	/// otherwise false.</returns>
	public static bool IsEmail (string email)
	{
		if (email != null)
			return Regex.IsMatch (email, MatchEmailPattern);
		else
			return false;
	}
       

    // Use this way to avoid Unloaded Image
    delegate void LoadPictureCallback(Texture2D texture);

    IEnumerator LoadPictureEnumerator(string url, LoadPictureCallback callback)
    {
        WWW www = new WWW(url);
        yield return www;
        callback(www.texture);
    }

    public string DeserializePictureURLString(string response)
    {
        return DeserializePictureURLObject(MiniJSON.JSON.Deserialize(response));
    }

    public string DeserializePictureURLObject(object pictureObj)
    {
        var picture = (Dictionary<string, object>)(((Dictionary<string, object>)pictureObj)["data"]);
        object urlH = null;
        if (picture.TryGetValue("url", out urlH))
        {
            return (string)urlH;
        }
        return null;
    }

	public string GetPictureURL(string facebookID, int? width = null, int? height = null, string type = null)
    {
        string url = string.Format("/{0}/picture", facebookID);
        string query = width != null ? "&width=" + width.ToString() : "";
        query += height != null ? "&height=" + height.ToString() : "";
        query += type != null ? "&type=" + type : "";
        query += "&redirect=false";
       
        if (query != "")
            url += ("?g" + query);
        Debug.Log("URL " + url);
        return url;
      
    }

	public float sec = 50f;

#pragma warning disable CS0436 // Type conflicts with imported type
    public JsonData RequiredData
#pragma warning restore CS0436 // Type conflicts with imported type
    {
        get
        {
            return requiredData;
        }

        set
        {
            requiredData = value;
        }
    }

#pragma warning disable CS0436 // Type conflicts with imported type
    public JsonData RequiredData1
#pragma warning restore CS0436 // Type conflicts with imported type
    {
        get
        {
            return requiredData;
        }

        set
        {
            requiredData = value;
        }
    }

    IEnumerator LateCallRedirect ()
	{
 
		yield return new WaitForSeconds (sec);
		//  Application.LoadLevel("MainScene");
	}



}
