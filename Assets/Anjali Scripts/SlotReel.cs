﻿//#define TESTING
#define NOT_TESTING
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Outback.Managers;
using System.Linq;
using Bitware.Utils;
using Outback.Core;

public class SlotReel : MonoBehaviour
{
	public List<Sprite> slots;
	public List<Sprite> freeImages;
	public List<GameObject> visibleSlots;
	//	public int scatterCount = 0;
	//	public int wildCount = 0;
	//	public int bonusCount = 0;
	public bool spinning = false;
	float speed;
	float startingYposition;
	//	Vector3 endPos;
	public int reelNumber;
	//	int maxScatterCount = 1;
	//	int maxWildCount = 1;
	//	int maxBonusCount = 1;
	public float positionC = 0;

	public GameObject refGO;

	public void Start ()
	{

		speed = SlotGameManager.instance.speedOfSlots;
		startingYposition = refGO.transform.localPosition.y;
//		endPos = new Vector3 (refGO.transform.localPosition.x, 0, 0);
		foreach (var animator in transform.GetComponentsInChildren <Animator>().ToList ()) {
			animator.enabled = false;
		}
	}

	int[,] testRoll = new int[,] { { 10, 10, 10, 0, 7 }, {1 , 1, 1, 3, 3 }, { 4, 3, 0, 1, 2} };

	public void SetRandomSprites ()
	{
	
//		scatterCount = 0;
//		wildCount = 0;
//		bonusCount = 0;
		#if NOT_TESTING
		freeImages.Shuffle ();
		var r = new Sprite ();
//		List<Sprite> imagesToCheckFrom = new List<Sprite> ();
		for (int i = 0; i < transform.childCount; i++) {
			if (i < freeImages.Count) {
				r = freeImages [i];
			} else {
				var rand = Random.Range (0, freeImages.Count);
				r = freeImages [rand];
			}
			transform.GetChild (i).GetComponent <Image> ().sprite = r;
//			Debug.Log ("Setting Sprite " + r + " for " + i + " in reel " + reelNumber);
		}
		#endif

		//for testing change test roll and uncomment this for loop and comment the above for loop
		#if TESTING
		for (int i = 0; i < transform.childCount; i++) {
			if (i >= 1 && i < 4) {
				var r = testRoll [i - 1, reelNumber];
//				Debug.Log ("Setting at " + i + " in reel " + reelNumber + " sprite " + testRoll [i - 1, reelNumber]);
				transform.GetChild (i).GetComponent <Image> ().sprite = SlotGameManager.instance.sprites [r];
			} else {
				var r = Random.Range (0, transform.childCount);
//				Debug.Log ("Setting at " + i + " in reel " + reelNumber + " sprite " + r);

				transform.GetChild (i).GetComponent <Image> ().sprite = SlotGameManager.instance.sprites [r];
			}
		}
		#endif
	
		SetScatterInThisReel ();
		SetWildInThisReel ();
		if (SlotGameManager.baseGame.isBonusGame)
			SetBonusInThisReel ();
		

	}

	public void SetInitialSprites ()
	{
		for (int i = 0; i < transform.childCount; i++) {
			transform.GetChild (i).GetComponent <Image> ().sprite = SlotGameManager.instance.sprites [i];

		}


		//for testing call only SetRandomSprites and comment the rest
		for (int i = 0; i < transform.childCount; i++) {
			slots.Add (transform.GetChild (i).GetComponent <Image> ().sprite);
		}


		for (int x = 1; x < 4; x++) {
			visibleSlots.Add (transform.GetChild (x).gameObject);
		}

		for (int i = 0; i < SlotGameManager.instance.sprites.Count; i++) {
			freeImages.Add (SlotGameManager.instance.sprites [i]);
		}
		if (SlotGameManager.instance.wildReels.Count > 0) {
			if (!SlotGameManager.instance.wildReels.Contains (reelNumber)) {
				var toRemove = freeImages.FindIndex (x => x.name == SlotGameManager.instance.wild);
				freeImages.RemoveAt (toRemove);
			}
		}

		if (!SlotGameManager.instance.scatterReels.Contains (reelNumber)) {
			var toRemove = freeImages.FindIndex (x => x.name == SlotGameManager.instance.scatter);
			freeImages.RemoveAt (toRemove);
		}
		if (SlotGameManager.baseGame.isBonusGame) {
			if (!SlotGameManager.instance.bonusReels.Contains (reelNumber)) {
				var toRemove = freeImages.FindIndex (x => x.name == SlotGameManager.instance.bonus);
				freeImages.RemoveAt (toRemove);
			}
		}

		SetRandomSprites ();
	}


	public void SetScatterInThisReel ()
	{
		if (SlotGameManager.instance.scatterReels.Contains (reelNumber)) {
			foreach (var slot in visibleSlots) {
				//			Debug.Log ("scatter " + SlotGameManager.instance.scatter);

				if (slot.GetComponent <Image> ().sprite.name == SlotGameManager.instance.scatter) {
//					scatterCount++;
					//				Debug.Log ("scatter Count in reel " + reelNumber + " is " + scatterCount);

					SlotGameManager.instance.scatters.Add (slot.GetComponent <Image> ());

				} 
			}
		}
	
	}

	public void SetWildInThisReel ()
	{
		if (SlotGameManager.instance.wildReels.Contains (reelNumber)) {
			
			foreach (var slot in visibleSlots) {
				if (slot.GetComponent <Image> ().sprite.name == SlotGameManager.instance.wild) {
//					wildCount++;
					SlotGameManager.instance.wilds.Add (slot.GetComponent <Image> ());
				} 
			}
		}
	}

	public void SetBonusInThisReel ()
	{
		if (SlotGameManager.instance.bonusReels.Contains (reelNumber)) {
			
			foreach (var slot in visibleSlots) {
				if (slot.GetComponent <Image> ().sprite.name == SlotGameManager.instance.bonus) {
//					bonusCount++;
					SlotGameManager.instance.bonuses.Add (slot.GetComponent <Image> ());

					Debug.Log ("BONUS in " + reelNumber + " at " + visibleSlots.IndexOf (slot) + " and bonus count " + SlotGameManager.instance.bonuses.Count);
				} 
			}
		}
	}

	void Update ()
	{

		if (spinning) {
			if (refGO.transform.localPosition.y < positionC) {
				//			Debug.Log ("Test33");
				ResetPosition (refGO);
			} else {
				//			Debug.Log ("Test34");
				MoveFromAToC (refGO);
			}
		} else {
			StartCoroutine (StopThisReel ());

//			refGO.transform.localPosition = Vector3.Lerp (refGO.transform.localPosition, new Vector3 (refGO.transform.localPosition.x, startingYposition, 0), 0.1f);
		}
	}


	IEnumerator StopThisReel ()
	{
		if (SlotGameManager.instance.stopInstantly)
			yield return new WaitForSeconds (0.005f);
	
		refGO.transform.localPosition = Vector3.Lerp (refGO.transform.localPosition, new Vector3 (refGO.transform.localPosition.x, startingYposition, 0), 0.1f);

	}

	void ResetPosition (GameObject refGO)
	{
		transform.localPosition = new Vector3 (refGO.transform.localPosition.x, startingYposition * 70 / 100, 0);
	}

	void MoveFromAToC (GameObject refGO)
	{
		refGO.transform.localPosition = new Vector3 (refGO.transform.localPosition.x, refGO.transform.localPosition.y - (speed++), 0);
		if (speed > 75) {
			speed = 75;
		}
	}
}
