﻿//#define LOCAL_ASSETBUNDLES
#define SERVER_ASSETBUNDLES
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.IO;
using Bitware.Utils;
using Outback.Views;
using UnityEngine.Networking;
using Outback.Managers;

public class AssetBundleHandler : MonoBehaviour {

	//	public string url ;

	public static AssetBundleHandler instance;
	public string url;
	#if LOCAL_ASSETBUNDLES
	public string localhostLink = "file:///C:/xampp/htdocs/Outback/AssetBundles/" ;
	#elif SERVER_ASSETBUNDLES
	#if UNITY_ANDROID
	public string serverLink = "http://103.224.243.154/PHP/webservices/OutbackSlotsfiles/AssetBundles/Android/";
	#elif UNITY_IOS
	public string serverLink = "http://103.224.243.154/PHP/webservices/OutbackSlotsfiles/AssetBundles/iOS/";
	#endif
	#endif
	public List<string> gameNames;

	public List<AssetBundle> assetBundles;

	void Awake(){
		instance = this;
//		#if LOCAL_ASSETBUNDLES
//		var link =  localhostLink;
//		#elif SERVER_ASSETBUNDLES
//		var link = serverLink;
//		#endif
//		foreach (var gameName in gameNames) {
//			var url = link + name.ToLower();
//			Debug.Log (url);
//			urls.Add (url);
//		}
//		LoadAssetBundle ("Lobby");
	}
	void Start(){
	
	}


	IEnumerator progress(UnityWebRequest req)
	{
		while (isDownload)
		{
			if (Application.internetReachability == NetworkReachability.NotReachable) {
				ErrorPanel.instance.ShowMessage ("Please check your internet connection!", true);
			} else {
//				Debug.Log ("Downloading : " + req.downloadProgress * 100 + "%");
				ScreenManager.instance.loadingSlider.value = req.downloadProgress;
				yield return new WaitForSeconds (0.1f);
			}
		}
	}
	private bool isDownload = false;

	// Use this for initialization
	IEnumerator LoadAssets () {
		
		var names = "Scene Names : ";
//		foreach (var url in urls) {
		Debug.Log(url);
			UnityWebRequest www = UnityWebRequest.GetAssetBundle(url);

		Debug.Log("Trying the bundle download");
		isDownload = true;

		StartCoroutine(progress(www));

		yield return www.SendWebRequest();
		isDownload = false;

		if (www.isNetworkError || www.isHttpError) {
				ScreenManager.instance.errorPanel.SetActive (true);
				ErrorPanel.instance.ShowMessage ("Error downloading data. Please restart the app", true);
				Debug.LogError ("ERROR" + www.error);
				yield break;
			} 
//				else {
//					yield return www.isDone;
//				}
			else {
				AsyncOperation asyncc; 
			var assetBundle = DownloadHandlerAssetBundle.GetContent(www);

//				ScreenManager.instance.loadingSlider.value = 0f;
	
			

//				asyncc.allowSceneActivation = false;

				if (www.isDone ) {
					ScreenManager.instance.loadingSlider.value = 1f;
				StopCoroutine (progress (www));
				StopCoroutine (LoadAssets());
					yield return null;


				}




//				ScreenManager.instance.loadingSlider.value += 1.0f/(gameNames.Count);

			

				assetBundles.Add (assetBundle);
			

				string[] sceneNames = assetBundle.GetAllScenePaths ();
				//		Debug.Log("Scenes "  + sceneNames.ToList().ConvertAll(x=> Path.GetFileNameWithoutExtension(x)).ToArray().Print ());

				foreach (var sceneName in sceneNames) {
					names += Path.GetFileNameWithoutExtension (sceneName) + " , ";
				}
			}
//		}
//		if (assetBundles.Count == gameNames.Count) {
//			ScreenManager.instance.loadingSlider.value = 1.0f;
//			yield return new WaitForSeconds (2f);
//			//						asyncc.allowSceneActivation = true;
//			ScreenManager.instance.loadingScreenObj.SetActive (false);
//		}

		if (ScreenManager.instance.loadingSlider.value == 1.0f) {
			SceneManager.LoadScene (name);	

			yield return new WaitForSeconds (1f);
			//			//						asyncc.allowSceneActivation = true;
						ScreenManager.instance.loadingScreenObj.SetActive (false);

		}
		Debug.Log (names);

	}

	// Update is called once per frame
	public void LoadAssetBundle (string sceneName) {
				ScreenManager.instance.loadingScreenObj.SetActive (true);

		Debug.Log ("SceneNAme" + sceneName);
		#if LOCAL_ASSETBUNDLES
		var link =  localhostLink;
		#elif SERVER_ASSETBUNDLES
		var link = serverLink;
		#endif
		name = sceneName;
		url = link + sceneName.ToLower();
		StartCoroutine ("LoadAssets");
		//		assetBundles.Find(x=> x.name.Contains(sceneName.ToLower())).LoadAllAssets ();

	}
}

