﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Bitware.Utils;
using UnityEngine.UI;
using Outback.Managers;


namespace Outback
{
	namespace Core
	{
		public class AnimatableComponent : MonoBehaviour
		{

			Animator anim;
			Image image;
			public List<AnimatorControllerParameter> prmtrs = new List<AnimatorControllerParameter> ();
			public List<Sprite> sprites ;
			public Vector3 defaultSize;

//			public List<bool> triggers = new List<bool> ();
			// Use this for initialization
			void Start ()
			{
				image = GetComponent <Image> ();
				anim = GetComponent <Animator> ();
				prmtrs = anim.parameters.ToList ();
				defaultSize = transform.localScale;
//				triggers = anim.parameters.ToList ().ConvertAll (x => x.defaultBool).ToList ();
			}
	
			// Update is called once per frame
			public void Play (string parameter)
			{
		
				var index = prmtrs.FindIndex (x => x.name == parameter);
				anim.enabled = true;

				for (int i = 0; i < prmtrs.Count; i++) {
					anim.SetBool (prmtrs [i].name, false);
				}

				anim.SetBool (prmtrs [index].name, true);
			}


			public void Stop ()
			{
//				var index = prmtrs.FindIndex (x => x.name == parameter);
				for (int i = 0; i < prmtrs.Count; i++) {
					//			Debug.Log ("trigger " + prmtrs [i] + " is true");
//					triggers [i] = false;
					anim.SetBool (prmtrs [i].name, false);

				}
				//		Debug.Log (index);
				anim.enabled = false;
				var name = "";
				var image = anim.GetComponent <Image> ();
				if (image.sprite.name.Contains ("-")) {
					name = image.sprite.name.Split ('-') [0];
				} else {
					name = image.sprite.name;
				}
//				Debug.Log ("Stopping animation " + name + " for slot " + anim.gameObject.name);
//				image.sprite = sprites.Find (x => x.name == name);
				image.sprite = SlotGameManager.instance.sprites.Find (x => x.name == name);
				transform.localScale = defaultSize;
//				image.SetNativeSize ();
//				image.sprite = Resources.Load ("GamePlay/Safari/Slots/" + image, typeof(Sprite)) as Sprite;
		
			}
		}
	}
}