using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Outback.Gameplay;
using Bitware.Wrappers;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

namespace Bitware
{
	namespace Wrappers {
		public class SpriteManager : MonoBehaviour {

			public static SpriteManager instance;

			public static Sprite TransparentImage{
				get{
					if(_transparentImage == null) {
						var tex = new Texture2D (1, 1, TextureFormat.ARGB32, false);
						var p = tex.GetPixels ();
						for (int i = 0; i < p.Length; i++)
							p [i] = new Color (1, 1, 1, 0);
						tex.SetPixels (p);
						tex.Apply ();
						_transparentImage = Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), Vector2.one * 0.5f);
					}
					return _transparentImage;
				}
			}
			private static Sprite _transparentImage = null;
			public  List<NamedSprite> currencySprites = new List<NamedSprite>();
			public Sprite selectedtab,nonselectedtab;
			[Header ("Coins")]
			public Sprite spriteCoin1;
			public Sprite spriteCoin2;
			public Sprite spriteCoin5;
			public Sprite spriteCoin10;

			[Header ("Special Cards")]
			public Sprite cardBackSpriteRed;
			public Sprite cardBackSpriteBlue;
			public Sprite jokerSprite;
			public Dictionary<string, Sprite> cardImages = new Dictionary<string, Sprite>();


			[Header ("Connection Sprites")]
			public Sprite connectingSprite;					//The "Connecting...." Sprite
			public Sprite connectionLostSprite;				//The "Connection Lost" Sprite

			[Header("Named Sprites")]
			public List<NamedSprite> namedSprites = new List<NamedSprite>();

			void Awake () {
				instance = this;
			}

		}

	}
}

namespace Bitware{
	namespace Wrappers{
		[System.Serializable]
		public class NamedSprite{
			public string name;
			public Sprite sprite;
		}
	}
}