#define UNITY_BROKEN
//#define DEBUG_STORE
using UnityEngine.Purchasing;
using UnityEngine;
using System.Collections.Generic;
using MiniJSON;
using Outback.Core;
using Outback.Views;
using Bitware.Utils;
using Bitware.Wrappers;
using System;
using Debug = UnityEngine.Debug;
using Random = UnityEngine.Random;

namespace Bitware {
	namespace Wrappers {

		#if UNITY_BROKEN
		public class IABWrapper : MonoBehaviour {
			public static IABWrapper instance;

			public void PurchaseItemWithInGameCurrency(string id,string currency,string value){

			}

			public void SalvageItemWithInGameCurrency(string id,string currency,string value){

			}
		}
		#endif

		#if !UNITY_BROKEN
		public class IABWrapper : MonoBehaviour, IStoreListener {

			public static IABWrapper instance;

			private string storeSaleType = "standard";
			private bool videoAdsAllowed = false;
//			[SerializeField]
//			public List<StoreSKU> storeSkus = new List<StoreSKU> ();

			private static IStoreController m_StoreController; // Reference to the Purchasing system.
			private static IExtensionProvider m_StoreExtensionProvider; // Reference to store-specific Purchasing

			private static string playStoreAPIKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAsDIsAe+i43tBVGhDdg0HhWuSL/NGglsFcz6TSA9LWMxeJQ8Y9eEajy6yVqxTeMlQ9W4cl1o36ZhqDypHuml9T4L9K3ubASvcXS3WWbV82pzu/90URV9jN1hIl+YbTUrd2ZeLzwZ5/C9quMfP60ePlWgR0OtrE7JrAzVrncB2wrQ7DYqu3VrNz9OWcf5wtEr9mhKcHhPgn1f+hl0DssOb6r5iqFIQeOXXqvDyhAiSr4a3QHcX02+wYOB61HJ6TpF/0GhRIYNgWW/+ijwrnlRztTAGi61240yvT9CjYbI8yr4/FOCDW7aEVcrf1JfYsJ7uEe3QuYrf3HQq8Ab+0r//CwIDAQAB";

			#region SKUs
			private const string sku_coinpack_videoad = "coinpackvideoad";

			private const string sku_coinpack1_std = "coinpack1std";
			private const string sku_coinpack2_std = "coinpack2std";
			private const string sku_coinpack3_std = "coinpack3std";
			private const string sku_coinpack4_std = "coinpack4std";
			private const string sku_coinpack5_std = "coinpack5std";

			private const string sku_coinpack1_sale1 = "coinpack1sale1";
			private const string sku_coinpack2_sale1 = "coinpack2sale1";
			private const string sku_coinpack3_sale1 = "coinpack3sale1";
			private const string sku_coinpack4_sale1 = "coinpack4sale1";
			private const string sku_coinpack5_sale1 = "coinpack5sale1";

			private const string sku_coinpack1_sale2 = "coinpack1sale2";
			private const string sku_coinpack2_sale2 = "coinpack2sale2";
			private const string sku_coinpack3_sale2 = "coinpack3sale2";
			private const string sku_coinpack4_sale2 = "coinpack4sale2";
			private const string sku_coinpack5_sale2 = "coinpack5sale2";

			#if UNITY_ANDROID && !UNITY_EDITOR
			private static string STORE_NAME = GooglePlay.Name;
			private static string sku_name_coinpack1_std = "";
			private static string sku_name_coinpack2_std = "";
			private static string sku_name_coinpack3_std = "";
			private static string sku_name_coinpack4_std = "";
			private static string sku_name_coinpack5_std = "";

			#elif UNITY_IOS && !UNITY_EDITOR

			private static string STORE_NAME = AppleAppStore.Name;

			private static string sku_name_coinpack1_std = "";
			private static string sku_name_coinpack2_std = "";
			private static string sku_name_coinpack3_std = "";
			private static string sku_name_coinpack4_std = "";
			private static string sku_name_coinpack5_std = "";
			
			

			#else

			private static string STORE_NAME = GooglePlay.Name;

			private static string sku_name_coinpack1_std = "";
			private static string sku_name_coinpack2_std = "";
			private static string sku_name_coinpack3_std = "";
			private static string sku_name_coinpack4_std = "";
			private static string sku_name_coinpack5_std = "";

			
			#endif

			#endregion

			void Awake () {
				instance = this;
			}

//			public void QueryInventory () {
////				if(!IsInitialized())
//					return;
//		#if !DEBUG_STORE
//				var prods = m_StoreController.products.all;
//				foreach(var prod in prods) {
//					var index = storeSkus.FindIndex(x => x.sku == prod.definition.storeSpecificId);
//					if(index < 0)
//						continue;
//					storeSkus[index].localizedPrice = prod.metadata.localizedPriceString;
//				}
//		#endif
//			}

//			public void RestorePurchases () {
//
//				//IAB Not Initialized. Get out
////				if (!IsInitialized()) {
////					Debug.Log("RestorePurchases FAIL. Not initialized.");
////					return;
////				}
//
////				//Restore purchases work only on iOS
////				if (Application.platform == RuntimePlatform.IPhonePlayer || Application.platform == RuntimePlatform.OSXPlayer) {
////					Debug.Log("RestorePurchases started ...");
////
////					// Fetch the Apple store-specific subsystem.
////					var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
////					// Begin the asynchronous process of restoring purchases. Expect a confirmation response in the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
////					apple.RestoreTransactions((result) => {
////						// The first phase of restoration. If no more responses are received on ProcessPurchase then no purchases are available to be restored.
////						Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
////					});
////				}
////
////				else {
////					Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
////				}
//			}

			public void SalvageItemWithInGameCurrency(string id,string currency,string value){
				var item = InventoryManager.instance.config.Find (x => x.itemId.Contains (id));

				if (item.quantity > 0) {
					SalvageItem (id);
					StorePopup.instance.UpdateItem (item);
				} 
			}
			public void PurchaseItemWithInGameCurrency(string id,string currency,string value){
				var item = InventoryManager.instance.config.Find (x => x.itemId.Contains (id));
			
				switch (currency){
				case "Coins":
					PlayerManager.CurrentCoins -= long.Parse (value);
					break;
				case "Diamonds":
					PlayerManager.CurrentDiamonds -= long.Parse (value);
					break;
				}
				PurchaseItem (id);
				StorePopup.instance.UpdateItem (item);
			}

			public void PurchaseInGameItem (StoreSKU storeSku) {
		//		OnPurchaseSuccess(sku_coinpack_videoad);
		//		return;

				if(storeSku.itemId == sku_coinpack_videoad) {
					AdWrapper.instance.ShowAd(AdWrapper.REWARDED_AD_T1, delegate(string obj) {
						OnPurchaseSuccess(sku_coinpack_videoad);
					});
				}
			}

			public void PurchaseItemRealMoney (StoreSKU storeSku) {
				var errorMessage = "Unable to make this purchase. Please contact suppport";

				#if !DEBUG_STORE
				try {
					if(!IsInitialized()) {
						Debug.LogError("Purchasing has not been initialized.");
						OnPurchaseFailure(storeSku.sku, errorMessage);
						return;
					}

					//Grab the product
					var product = m_StoreController.products.all.ToList().Find(x => x.definition.storeSpecificId ==  storeSku.sku);

					if(product == null) {
						Debug.LogError("Product with SKU " + storeSku.sku + " was not found.");
						OnPurchaseFailure(storeSku.sku, errorMessage);
						return;
					}

					if(!product.availableToPurchase) {
						Debug.LogError("Product with SKU " + storeSku.sku + " is not available for purchase.");
						OnPurchaseFailure(storeSku.sku, errorMessage);
						return;
					}

					m_StoreController.InitiatePurchase(product);
				}
				catch (System.Exception e) {
					Debug.Log ("BuyProductID: FAIL. Exception during purchase. " + e);
				}

				#else
				OnPurchaseSuccess (storeSku.sku);
				#endif
			}
			public void SalvageItem (string productID){
				var skuItemId =  productID.Contains("com") ? storeSkus.Find (x => x.sku == productID).productID : string.Empty;
				var item = skuItemId != string.Empty ?   InventoryManager.instance.config.Find(x => x.itemId == skuItemId): InventoryManager.instance.config.Find(x => x.itemId == productID);
				if (PlayerManager.instance.player.inventory.items.Contains(item)) 
					item.quantity--;

				else {
					PlayerManager.instance.player.inventory.items.Add(item);
					item.quantity--;
				}
				if (item.salvage.ContainsKey("Coins")){
					PlayerManager.CurrentCoins += (long)item.salvage["Coins"];
					NetManager.instance.AskForCoinsAwarded(PlayerManager.ME, int.Parse(item.salvage["Coins"].ToString()), "Out of cash in game");
					PlayerManager.instance.currentGame.GotCash();
				}
				if (item.salvage.ContainsKey("Diamonds")){
					
					NetManager.instance.AskForDiamondsAwarded(PlayerManager.ME, int.Parse(item.salvage["Diamonds"].ToString()), "Out of cash in game");
					PlayerManager.CurrentDiamonds += (long)item.salvage["Diamonds"];
				}
			}
				
			public void OnPurchaseSuccess (string productID) {
				#if !UNITY_EDITOR
				Debug.Log ("Purchase Succeeded for " + productID);
				var storeSKu = storeSkus.Find (x => x.productID == productID);
//				Debug.Log ("Need to award so much to player " + storeSKu.award);
				//PlayerManager.instance.Coins += storeSKu.award;
//				AnalyticsWrapper.IAPPurchased (storeSKu.productID, storeSKu.basePrice, storeSKu.discounted);
		//		#if UNITY_ANDROID
//				CrossPlatformNative.ShowAlert ("Purchase successful", "Successfully added " + storeSKu.award + " coins", "OK");
		//		#endif
//				NetManager.instance.AskForCoinsAwarded (PlayerManager.ME, storeSKu.award, storeSKu.productID);
				if (PlayerManager.instance.currentGame != null){
					PlayerManager.instance.currentGame.GotCash ();
					PlayerManager.instance.currentGame.EnableGame (true);
				}
				#else
				var skuItemId =  productID.Contains("com") ? storeSkus.Find (x => x.sku == productID).productID : string.Empty;
				var item = skuItemId != string.Empty ?   InventoryManager.instance.config.Find(x => x.itemId == skuItemId): InventoryManager.instance.config.Find(x => x.itemId == productID);
				if (PlayerManager.instance.player.inventory.items.Contains(item)) 
				item.quantity++;

				else {
				PlayerManager.instance.player.inventory.items.Add(item);
				 item.quantity++;
				}

				if (item.rewards.ContainsKey("Coins")){
					PlayerManager.CurrentCoins += (long)item.rewards["Coins"];
					var toConsume = PlayerManager.instance.player.inventory.items.Find(x => x == item);
					NetManager.instance.AskForCoinsAwarded(PlayerManager.ME, int.Parse(item.rewards["Coins"].ToString()), "Out of cash in game");
					PlayerManager.instance.player.inventory.items.Remove(toConsume);
					PlayerManager.instance.currentGame.GotCash();
				}
				if (item.rewards.ContainsKey("Diamonds")){
					PlayerManager.CurrentDiamonds += (long)item.rewards["Diamonds"];
					var toConsume = PlayerManager.instance.player.inventory.items.Find(x => x == item);
					PlayerManager.instance.player.inventory.items.Remove(toConsume);
					NetManager.instance.AskForDiamondsAwarded(PlayerManager.ME, int.Parse(item.rewards["Diamonds"].ToString()), "Out of diamonds in game");
				}
//				if (item.rewards.ContainsKey("Tokens")){
//					PlayerManager.CurrentTokens += (long)item.rewards["Tokens"];
//					Debug.Log("contains tokens : " + (long)item.rewards["Tokens"]);
//					NetManager.instance.AskForTokensAwarded(PlayerManager.ME, int.Parse(item.rewards["Tokens"].ToString()), "Out of tokens in game");
//				}
				if(PlayerManager.instance.currentGame != null)
				PlayerManager.instance.currentGame.EnableGame (true);
				NetManager.instance.AskForItemPurchased(item,PlayerManager.ME.guid);
	
				#endif
			}

			public void OnPurchaseFailure (string sku, string error) {
				Debug.LogError(string.Format("Error is purchasing sku {0}. Error returned was {1}", new object[] {
					sku, error
				}));
		//		#if UNITY_ANDROID
				CrossPlatformNative.ShowAlert("Purchase failed", error, "OK");
		//		#endif
				AnalyticsWrapper.IAPCanceled(sku);
			}

			public List<StoreSKU> GetAvailableStoreItems () {
				//Lets only return what is applicable
				var validSkus = new List<StoreSKU> ();
				var subList = storeSkus.FindAll (x => x.sku.Contains (storeSaleType) || x.sku.Contains ("videoad"));
				foreach (var sku in subList) {
					if (!sku.isHidden)
						validSkus.Add (StoreSKU.DeepCopy (sku));
				}
				return validSkus;
			}

			/// <summary>
			/// Called when Unity IAP is ready to make purchases.
			/// </summary>
			public void OnInitialized (IStoreController controller, IExtensionProvider extensions) {
				// Purchasing has succeeded initializing. Collect our Purchasing references.
		//		Debug.Log("OnInitialized: PASS");

				// Overall Purchasing system, configured with products for this application.
				m_StoreController = controller;
				// Store specific subsystem, for accessing device-specific store features.
				m_StoreExtensionProvider = extensions;

				QueryInventory();

				if(LobbyController.instance != null)
					LobbyController.instance.OnIABInitialized(true);
			}


			public void OnInitializeFailed(InitializationFailureReason error) {
				Debug.Log("IAB was not initialized. Error :: " + error);

				if(LobbyController.instance != null)
					LobbyController.instance.OnIABInitialized(false);
			}



			public PurchaseProcessingResult ProcessPurchase (PurchaseEventArgs args) {

				OnPurchaseSuccess(args.purchasedProduct.definition.id);

				// Return a flag indicating wither this product has completely been received, or if the application needs to be reminded of this purchase at next app launch. Is useful when saving purchased products to the cloud, and when that save is delayed.
				return PurchaseProcessingResult.Complete;
			}
			
			public void OnPurchaseFailed (Product product, PurchaseFailureReason failureReason) {
				OnPurchaseFailure(product.definition.id, failureReason.ToString());
			}
		}
		#endif
	}
}