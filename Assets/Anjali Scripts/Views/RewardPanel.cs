﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Outback.Managers;
using Bitware.Utils;
using TMPro.Examples;


namespace Outback
{
	namespace Views
	{
		public class RewardPanel : GenericPanel
		{
			
			public TMPro.TextMeshProUGUI reward;

			public static RewardPanel instance;


			public override void Open (params object[] args)
			{
				gameObject.SetActive (true);
				ScreenManager.instance.playerDetailsPanel.SetActive (false);
			}

			public override void Close ()
			{
				gameObject.SetActive (false);
				ScreenManager.instance.playerDetailsPanel.SetActive (true);
				Destroy(reward.gameObject);
				reward = null;
			}

			public override void Populate (params object[] args)
			{
			}

			public override void OpenTab (int index = 0)
			{
			}

			public override void Awake ()
			{
				instance = this;
			}

			public void ShowMessage (params object[] args)
			{
				Open ();
				reward = Instantiate(Resources.Load("WinningText", typeof(TMPro.TextMeshProUGUI)), this.transform,false) as TMPro.TextMeshProUGUI;
				reward.text = TextFormatter.GetFormattedNumberText (long.Parse(args [0].ToString()), true);

				var startValue = float.Parse (args [0].ToString ());
				var a = 0f;
				var endValue = float.Parse (args [1].ToString ());
				DOTween.To (x => a = x, 0f, 10f, 2f).OnComplete (() => {
					IncrementValueAndClose (startValue, endValue);
				});
			

			}

			public void IncrementValueAndClose (float startVal, float endVal)
			{
				var a = startVal;
				DOTween.To (x => a = x, startVal, endVal, 3f).OnUpdate (() => {
					reward.text = TextFormatter.GetFormattedNumberText (a, true);
				}).OnComplete (() => {
					Invoke ("Close", 2f);
				
				});

			}
		}
	}
}
