﻿using UnityEngine;

namespace Outback {
	namespace Views {
		public abstract class GenericPanel : MonoBehaviour {

			public abstract void Open (params object[] args);

			public abstract void Close ();

			public abstract void Populate (params object[] args);

			public abstract void OpenTab (int index = 0);

			public abstract void Awake ();
		}
	}
}