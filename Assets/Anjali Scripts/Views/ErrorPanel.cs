﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Outback.Views;
using UnityEngine.UI;


namespace Outback
{
	namespace Views
	{
		public class ErrorPanel : GenericPanel
		{
			public Text heading;
			public Text message;
			bool exitApp;


			public static ErrorPanel instance;


			public override void Open (params object[] args)
			{
				gameObject.SetActive (true);
			}

			public override void Close ()
			{
				gameObject.SetActive (false);

			}

			public override void Populate (params object[] args)
			{
			}

			public override void OpenTab (int index = 0)
			{
			}

			public override void Awake ()
			{
				instance = this;
			}

			public void ShowMessage (params object[] args)
			{
				message.text = args [0].ToString ();
				exitApp = (bool)(args [1]);
				Open ();
				if (!exitApp)
					Invoke ("Close", 3f);
			}
		}
	}

}
