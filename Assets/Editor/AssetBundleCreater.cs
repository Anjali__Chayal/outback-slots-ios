﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class AssetBundleCreater : MonoBehaviour {

	[MenuItem ("Assets/Build AssetBundles")]
	static void BuildAllAssetBundles ()
	{
		#if UNITY_ANDROID
		BuildPipeline.BuildAssetBundles ("Assets/AssetBundles/Android", BuildAssetBundleOptions.None, BuildTarget.Android);
		#elif UNITY_IOS
		BuildPipeline.BuildAssetBundles ("Assets/AssetBundles/iOS", BuildAssetBundleOptions.None, BuildTarget.iOS);
		#endif

	}

	[MenuItem ("Assets/Get AssetBundle names")]
	static void GetNames ()
	{
		var names = AssetDatabase.GetAllAssetBundleNames();
		foreach (var name in names)
			Debug.Log ("AssetBundle: " + name);
	}
}
