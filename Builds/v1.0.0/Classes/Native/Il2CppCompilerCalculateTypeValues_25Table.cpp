﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Sdkbox.IAP/Callbacks/BoolEvent
struct BoolEvent_t3697584372;
// Sdkbox.IAP/Callbacks/ProductEvent
struct ProductEvent_t4069975844;
// Sdkbox.IAP/Callbacks/ProductStringEvent
struct ProductStringEvent_t801016626;
// Sdkbox.IAP/Callbacks/ProductArrayEvent
struct ProductArrayEvent_t1714183732;
// Sdkbox.IAP/Callbacks/StringEvent
struct StringEvent_t4181969553;
// Sdkbox.IAP/Callbacks/BoolStringEvent
struct BoolStringEvent_t1829059983;
// TMPro.Examples.ShaderPropAnimator
struct ShaderPropAnimator_t3617420994;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// System.Byte[]
struct ByteU5BU5D_t4116647657;
// UnityEngine.WWWForm
struct WWWForm_t4064702195;
// Facebook.Unity.Example.GraphRequest
struct GraphRequest_t4047451309;
// System.String
struct String_t;
// UnityEngine.Sprite
struct Sprite_t280657092;
// Outback.Views.RewardPanel
struct RewardPanel_t4063177467;
// TMPro.Examples.Benchmark01
struct Benchmark01_t1571072624;
// TMPro.Examples.Benchmark01_UGUI
struct Benchmark01_UGUI_t3264177817;
// UnityEngine.RectTransform
struct RectTransform_t3704657025;
// System.IO.StringReader
struct StringReader_t3465604688;
// System.Text.StringBuilder
struct StringBuilder_t;
// UnityEngine.Events.InvokableCallList
struct InvokableCallList_t2498835369;
// UnityEngine.Events.PersistentCallGroup
struct PersistentCallGroup_t3050769227;
// UnityEngine.WWW
struct WWW_t3688466362;
// TimeManager
struct TimeManager_t1960693005;
// Bitware.Utils.UnityEditorPopup/<ShowPopup>c__AnonStorey0
struct U3CShowPopupU3Ec__AnonStorey0_t938638323;
// System.Action`1<System.Int32>
struct Action_1_t3123413348;
// System.Collections.Generic.List`1<Tuple`4<System.Double,System.Double,System.String,System.String>>
struct List_1_t312090022;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Void
struct Void_t1185182177;
// System.Object[]
struct ObjectU5BU5D_t2843939325;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// EnvMapAnimator
struct EnvMapAnimator_t1140999784;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.AnimationCurve
struct AnimationCurve_t3046754366;
// UnityEngine.Texture
struct Texture_t3661962703;
// System.Collections.Generic.List`1<Sdkbox.Json>
struct List_1_t3150497957;
// System.Collections.Generic.Dictionary`2<System.String,Sdkbox.Json>
struct Dictionary_2_t1463679514;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// TMPro.TextMeshPro
struct TextMeshPro_t2393593166;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// UnityEngine.Renderer
struct Renderer_t2627027031;
// System.Collections.Generic.List`1<UnityEngine.UI.Button>
struct List_1_t1232139915;
// System.Collections.Generic.List`1<UnityEngine.UI.Text>
struct List_1_t3373957456;
// UnityEngine.UI.Text
struct Text_t1901882714;
// UnityEngine.Transform
struct Transform_t3600365921;
// Sdkbox.IAP
struct IAP_t1939458538;
// System.Collections.Generic.List`1<Bitware.Wrappers.NamedSprite>
struct List_1_t4170927959;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Sprite>
struct Dictionary_2_t65913391;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t2690840144;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// TMPro.TMP_FontAsset
struct TMP_FontAsset_t364381626;
// UnityEngine.Font
struct Font_t1956802104;
// TMPro.TextContainer
struct TextContainer_t97923372;
// UnityEngine.TextMesh
struct TextMesh_t1536577757;
// UnityEngine.Canvas
struct Canvas_t3310196443;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t529313277;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// TMPro.Examples.TextMeshProFloatingText
struct TextMeshProFloatingText_t845872552;
// UnityEngine.UI.Image
struct Image_t2670269651;
// ProgressBar.Utils.OnCompleteEvent
struct OnCompleteEvent_t1779196559;
// ProgressBar.Utils.FillerProperty
struct FillerProperty_t1637340060;
// ProgressBar.Utils.ProgressValue
struct ProgressValue_t1710479104;
// UnityEngine.Camera
struct Camera_t4157153871;
// System.Collections.Generic.List`1<MaterialEffect>
struct List_1_t1069258399;
// TMPro.TMP_InputField
struct TMP_InputField_t1099764886;
// TMPro.TMP_Text
struct TMP_Text_t2599618874;
// UnityEngine.UI.Scrollbar
struct Scrollbar_t1494447233;
// System.Collections.Generic.List`1<EffectData>
struct List_1_t3181922523;
// System.Collections.Generic.IList`1<System.String>
struct IList_1_t3662770472;
// System.Collections.Generic.List`1<Sdkbox.ProductDescription>
struct List_1_t3603741248;
// Sdkbox.IAP/Callbacks
struct Callbacks_t1762710508;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t2736202052;
// Sdkbox.IAP/CallbackIAPDelegate
struct CallbackIAPDelegate_t957877347;
// System.String[]
struct StringU5BU5D_t1281789340;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef CALLBACKS_T1762710508_H
#define CALLBACKS_T1762710508_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sdkbox.IAP/Callbacks
struct  Callbacks_t1762710508  : public RuntimeObject
{
public:
	// Sdkbox.IAP/Callbacks/BoolEvent Sdkbox.IAP/Callbacks::onInitialized
	BoolEvent_t3697584372 * ___onInitialized_0;
	// Sdkbox.IAP/Callbacks/ProductEvent Sdkbox.IAP/Callbacks::onSuccess
	ProductEvent_t4069975844 * ___onSuccess_1;
	// Sdkbox.IAP/Callbacks/ProductStringEvent Sdkbox.IAP/Callbacks::onFailure
	ProductStringEvent_t801016626 * ___onFailure_2;
	// Sdkbox.IAP/Callbacks/ProductEvent Sdkbox.IAP/Callbacks::onCanceled
	ProductEvent_t4069975844 * ___onCanceled_3;
	// Sdkbox.IAP/Callbacks/ProductEvent Sdkbox.IAP/Callbacks::onRestored
	ProductEvent_t4069975844 * ___onRestored_4;
	// Sdkbox.IAP/Callbacks/ProductArrayEvent Sdkbox.IAP/Callbacks::onProductRequestSuccess
	ProductArrayEvent_t1714183732 * ___onProductRequestSuccess_5;
	// Sdkbox.IAP/Callbacks/StringEvent Sdkbox.IAP/Callbacks::onProductRequestFailure
	StringEvent_t4181969553 * ___onProductRequestFailure_6;
	// Sdkbox.IAP/Callbacks/BoolStringEvent Sdkbox.IAP/Callbacks::onRestoreComplete
	BoolStringEvent_t1829059983 * ___onRestoreComplete_7;

public:
	inline static int32_t get_offset_of_onInitialized_0() { return static_cast<int32_t>(offsetof(Callbacks_t1762710508, ___onInitialized_0)); }
	inline BoolEvent_t3697584372 * get_onInitialized_0() const { return ___onInitialized_0; }
	inline BoolEvent_t3697584372 ** get_address_of_onInitialized_0() { return &___onInitialized_0; }
	inline void set_onInitialized_0(BoolEvent_t3697584372 * value)
	{
		___onInitialized_0 = value;
		Il2CppCodeGenWriteBarrier((&___onInitialized_0), value);
	}

	inline static int32_t get_offset_of_onSuccess_1() { return static_cast<int32_t>(offsetof(Callbacks_t1762710508, ___onSuccess_1)); }
	inline ProductEvent_t4069975844 * get_onSuccess_1() const { return ___onSuccess_1; }
	inline ProductEvent_t4069975844 ** get_address_of_onSuccess_1() { return &___onSuccess_1; }
	inline void set_onSuccess_1(ProductEvent_t4069975844 * value)
	{
		___onSuccess_1 = value;
		Il2CppCodeGenWriteBarrier((&___onSuccess_1), value);
	}

	inline static int32_t get_offset_of_onFailure_2() { return static_cast<int32_t>(offsetof(Callbacks_t1762710508, ___onFailure_2)); }
	inline ProductStringEvent_t801016626 * get_onFailure_2() const { return ___onFailure_2; }
	inline ProductStringEvent_t801016626 ** get_address_of_onFailure_2() { return &___onFailure_2; }
	inline void set_onFailure_2(ProductStringEvent_t801016626 * value)
	{
		___onFailure_2 = value;
		Il2CppCodeGenWriteBarrier((&___onFailure_2), value);
	}

	inline static int32_t get_offset_of_onCanceled_3() { return static_cast<int32_t>(offsetof(Callbacks_t1762710508, ___onCanceled_3)); }
	inline ProductEvent_t4069975844 * get_onCanceled_3() const { return ___onCanceled_3; }
	inline ProductEvent_t4069975844 ** get_address_of_onCanceled_3() { return &___onCanceled_3; }
	inline void set_onCanceled_3(ProductEvent_t4069975844 * value)
	{
		___onCanceled_3 = value;
		Il2CppCodeGenWriteBarrier((&___onCanceled_3), value);
	}

	inline static int32_t get_offset_of_onRestored_4() { return static_cast<int32_t>(offsetof(Callbacks_t1762710508, ___onRestored_4)); }
	inline ProductEvent_t4069975844 * get_onRestored_4() const { return ___onRestored_4; }
	inline ProductEvent_t4069975844 ** get_address_of_onRestored_4() { return &___onRestored_4; }
	inline void set_onRestored_4(ProductEvent_t4069975844 * value)
	{
		___onRestored_4 = value;
		Il2CppCodeGenWriteBarrier((&___onRestored_4), value);
	}

	inline static int32_t get_offset_of_onProductRequestSuccess_5() { return static_cast<int32_t>(offsetof(Callbacks_t1762710508, ___onProductRequestSuccess_5)); }
	inline ProductArrayEvent_t1714183732 * get_onProductRequestSuccess_5() const { return ___onProductRequestSuccess_5; }
	inline ProductArrayEvent_t1714183732 ** get_address_of_onProductRequestSuccess_5() { return &___onProductRequestSuccess_5; }
	inline void set_onProductRequestSuccess_5(ProductArrayEvent_t1714183732 * value)
	{
		___onProductRequestSuccess_5 = value;
		Il2CppCodeGenWriteBarrier((&___onProductRequestSuccess_5), value);
	}

	inline static int32_t get_offset_of_onProductRequestFailure_6() { return static_cast<int32_t>(offsetof(Callbacks_t1762710508, ___onProductRequestFailure_6)); }
	inline StringEvent_t4181969553 * get_onProductRequestFailure_6() const { return ___onProductRequestFailure_6; }
	inline StringEvent_t4181969553 ** get_address_of_onProductRequestFailure_6() { return &___onProductRequestFailure_6; }
	inline void set_onProductRequestFailure_6(StringEvent_t4181969553 * value)
	{
		___onProductRequestFailure_6 = value;
		Il2CppCodeGenWriteBarrier((&___onProductRequestFailure_6), value);
	}

	inline static int32_t get_offset_of_onRestoreComplete_7() { return static_cast<int32_t>(offsetof(Callbacks_t1762710508, ___onRestoreComplete_7)); }
	inline BoolStringEvent_t1829059983 * get_onRestoreComplete_7() const { return ___onRestoreComplete_7; }
	inline BoolStringEvent_t1829059983 ** get_address_of_onRestoreComplete_7() { return &___onRestoreComplete_7; }
	inline void set_onRestoreComplete_7(BoolStringEvent_t1829059983 * value)
	{
		___onRestoreComplete_7 = value;
		Il2CppCodeGenWriteBarrier((&___onRestoreComplete_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKS_T1762710508_H
#ifndef U3CANIMATEPROPERTIESU3EC__ITERATOR0_T4041402054_H
#define U3CANIMATEPROPERTIESU3EC__ITERATOR0_T4041402054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0
struct  U3CAnimatePropertiesU3Ec__Iterator0_t4041402054  : public RuntimeObject
{
public:
	// System.Single TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::<glowPower>__1
	float ___U3CglowPowerU3E__1_0;
	// TMPro.Examples.ShaderPropAnimator TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$this
	ShaderPropAnimator_t3617420994 * ___U24this_1;
	// System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.ShaderPropAnimator/<AnimateProperties>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CglowPowerU3E__1_0() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U3CglowPowerU3E__1_0)); }
	inline float get_U3CglowPowerU3E__1_0() const { return ___U3CglowPowerU3E__1_0; }
	inline float* get_address_of_U3CglowPowerU3E__1_0() { return &___U3CglowPowerU3E__1_0; }
	inline void set_U3CglowPowerU3E__1_0(float value)
	{
		___U3CglowPowerU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24this_1)); }
	inline ShaderPropAnimator_t3617420994 * get_U24this_1() const { return ___U24this_1; }
	inline ShaderPropAnimator_t3617420994 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(ShaderPropAnimator_t3617420994 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CAnimatePropertiesU3Ec__Iterator0_t4041402054, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CANIMATEPROPERTIESU3EC__ITERATOR0_T4041402054_H
#ifndef U3CTAKESCREENSHOTU3EC__ITERATOR0_T3544038915_H
#define U3CTAKESCREENSHOTU3EC__ITERATOR0_T3544038915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0
struct  U3CTakeScreenshotU3Ec__Iterator0_t3544038915  : public RuntimeObject
{
public:
	// System.Int32 Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::<width>__0
	int32_t ___U3CwidthU3E__0_0;
	// System.Int32 Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::<height>__0
	int32_t ___U3CheightU3E__0_1;
	// UnityEngine.Texture2D Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::<tex>__0
	Texture2D_t3840446185 * ___U3CtexU3E__0_2;
	// System.Byte[] Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::<screenshot>__0
	ByteU5BU5D_t4116647657* ___U3CscreenshotU3E__0_3;
	// UnityEngine.WWWForm Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::<wwwForm>__0
	WWWForm_t4064702195 * ___U3CwwwFormU3E__0_4;
	// Facebook.Unity.Example.GraphRequest Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::$this
	GraphRequest_t4047451309 * ___U24this_5;
	// System.Object Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 Facebook.Unity.Example.GraphRequest/<TakeScreenshot>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3CwidthU3E__0_0() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U3CwidthU3E__0_0)); }
	inline int32_t get_U3CwidthU3E__0_0() const { return ___U3CwidthU3E__0_0; }
	inline int32_t* get_address_of_U3CwidthU3E__0_0() { return &___U3CwidthU3E__0_0; }
	inline void set_U3CwidthU3E__0_0(int32_t value)
	{
		___U3CwidthU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U3CheightU3E__0_1() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U3CheightU3E__0_1)); }
	inline int32_t get_U3CheightU3E__0_1() const { return ___U3CheightU3E__0_1; }
	inline int32_t* get_address_of_U3CheightU3E__0_1() { return &___U3CheightU3E__0_1; }
	inline void set_U3CheightU3E__0_1(int32_t value)
	{
		___U3CheightU3E__0_1 = value;
	}

	inline static int32_t get_offset_of_U3CtexU3E__0_2() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U3CtexU3E__0_2)); }
	inline Texture2D_t3840446185 * get_U3CtexU3E__0_2() const { return ___U3CtexU3E__0_2; }
	inline Texture2D_t3840446185 ** get_address_of_U3CtexU3E__0_2() { return &___U3CtexU3E__0_2; }
	inline void set_U3CtexU3E__0_2(Texture2D_t3840446185 * value)
	{
		___U3CtexU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CtexU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CscreenshotU3E__0_3() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U3CscreenshotU3E__0_3)); }
	inline ByteU5BU5D_t4116647657* get_U3CscreenshotU3E__0_3() const { return ___U3CscreenshotU3E__0_3; }
	inline ByteU5BU5D_t4116647657** get_address_of_U3CscreenshotU3E__0_3() { return &___U3CscreenshotU3E__0_3; }
	inline void set_U3CscreenshotU3E__0_3(ByteU5BU5D_t4116647657* value)
	{
		___U3CscreenshotU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CscreenshotU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwFormU3E__0_4() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U3CwwwFormU3E__0_4)); }
	inline WWWForm_t4064702195 * get_U3CwwwFormU3E__0_4() const { return ___U3CwwwFormU3E__0_4; }
	inline WWWForm_t4064702195 ** get_address_of_U3CwwwFormU3E__0_4() { return &___U3CwwwFormU3E__0_4; }
	inline void set_U3CwwwFormU3E__0_4(WWWForm_t4064702195 * value)
	{
		___U3CwwwFormU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwFormU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U24this_5)); }
	inline GraphRequest_t4047451309 * get_U24this_5() const { return ___U24this_5; }
	inline GraphRequest_t4047451309 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(GraphRequest_t4047451309 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CTakeScreenshotU3Ec__Iterator0_t3544038915, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CTAKESCREENSHOTU3EC__ITERATOR0_T3544038915_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef NAMEDSPRITE_T2698853217_H
#define NAMEDSPRITE_T2698853217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bitware.Wrappers.NamedSprite
struct  NamedSprite_t2698853217  : public RuntimeObject
{
public:
	// System.String Bitware.Wrappers.NamedSprite::name
	String_t* ___name_0;
	// UnityEngine.Sprite Bitware.Wrappers.NamedSprite::sprite
	Sprite_t280657092 * ___sprite_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(NamedSprite_t2698853217, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_sprite_1() { return static_cast<int32_t>(offsetof(NamedSprite_t2698853217, ___sprite_1)); }
	inline Sprite_t280657092 * get_sprite_1() const { return ___sprite_1; }
	inline Sprite_t280657092 ** get_address_of_sprite_1() { return &___sprite_1; }
	inline void set_sprite_1(Sprite_t280657092 * value)
	{
		___sprite_1 = value;
		Il2CppCodeGenWriteBarrier((&___sprite_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEDSPRITE_T2698853217_H
#ifndef U3CINCREMENTVALUEANDCLOSEU3EC__ANONSTOREY1_T2550267275_H
#define U3CINCREMENTVALUEANDCLOSEU3EC__ANONSTOREY1_T2550267275_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Views.RewardPanel/<IncrementValueAndClose>c__AnonStorey1
struct  U3CIncrementValueAndCloseU3Ec__AnonStorey1_t2550267275  : public RuntimeObject
{
public:
	// System.Single Outback.Views.RewardPanel/<IncrementValueAndClose>c__AnonStorey1::a
	float ___a_0;
	// Outback.Views.RewardPanel Outback.Views.RewardPanel/<IncrementValueAndClose>c__AnonStorey1::$this
	RewardPanel_t4063177467 * ___U24this_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(U3CIncrementValueAndCloseU3Ec__AnonStorey1_t2550267275, ___a_0)); }
	inline float get_a_0() const { return ___a_0; }
	inline float* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(float value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CIncrementValueAndCloseU3Ec__AnonStorey1_t2550267275, ___U24this_1)); }
	inline RewardPanel_t4063177467 * get_U24this_1() const { return ___U24this_1; }
	inline RewardPanel_t4063177467 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(RewardPanel_t4063177467 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINCREMENTVALUEANDCLOSEU3EC__ANONSTOREY1_T2550267275_H
#ifndef U3CSHOWMESSAGEU3EC__ANONSTOREY0_T1415458573_H
#define U3CSHOWMESSAGEU3EC__ANONSTOREY0_T1415458573_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Views.RewardPanel/<ShowMessage>c__AnonStorey0
struct  U3CShowMessageU3Ec__AnonStorey0_t1415458573  : public RuntimeObject
{
public:
	// System.Single Outback.Views.RewardPanel/<ShowMessage>c__AnonStorey0::a
	float ___a_0;
	// System.Single Outback.Views.RewardPanel/<ShowMessage>c__AnonStorey0::startValue
	float ___startValue_1;
	// System.Single Outback.Views.RewardPanel/<ShowMessage>c__AnonStorey0::endValue
	float ___endValue_2;
	// Outback.Views.RewardPanel Outback.Views.RewardPanel/<ShowMessage>c__AnonStorey0::$this
	RewardPanel_t4063177467 * ___U24this_3;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(U3CShowMessageU3Ec__AnonStorey0_t1415458573, ___a_0)); }
	inline float get_a_0() const { return ___a_0; }
	inline float* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(float value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_startValue_1() { return static_cast<int32_t>(offsetof(U3CShowMessageU3Ec__AnonStorey0_t1415458573, ___startValue_1)); }
	inline float get_startValue_1() const { return ___startValue_1; }
	inline float* get_address_of_startValue_1() { return &___startValue_1; }
	inline void set_startValue_1(float value)
	{
		___startValue_1 = value;
	}

	inline static int32_t get_offset_of_endValue_2() { return static_cast<int32_t>(offsetof(U3CShowMessageU3Ec__AnonStorey0_t1415458573, ___endValue_2)); }
	inline float get_endValue_2() const { return ___endValue_2; }
	inline float* get_address_of_endValue_2() { return &___endValue_2; }
	inline void set_endValue_2(float value)
	{
		___endValue_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CShowMessageU3Ec__AnonStorey0_t1415458573, ___U24this_3)); }
	inline RewardPanel_t4063177467 * get_U24this_3() const { return ___U24this_3; }
	inline RewardPanel_t4063177467 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(RewardPanel_t4063177467 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWMESSAGEU3EC__ANONSTOREY0_T1415458573_H
#ifndef COMP_T2934735840_H
#define COMP_T2934735840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Comp
struct  Comp_t2934735840  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMP_T2934735840_H
#ifndef PROGRESSVALUE_T1710479104_H
#define PROGRESSVALUE_T1710479104_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProgressBar.Utils.ProgressValue
struct  ProgressValue_t1710479104  : public RuntimeObject
{
public:
	// System.Single ProgressBar.Utils.ProgressValue::m_Value
	float ___m_Value_0;
	// System.Single ProgressBar.Utils.ProgressValue::m_MaxValue
	float ___m_MaxValue_1;

public:
	inline static int32_t get_offset_of_m_Value_0() { return static_cast<int32_t>(offsetof(ProgressValue_t1710479104, ___m_Value_0)); }
	inline float get_m_Value_0() const { return ___m_Value_0; }
	inline float* get_address_of_m_Value_0() { return &___m_Value_0; }
	inline void set_m_Value_0(float value)
	{
		___m_Value_0 = value;
	}

	inline static int32_t get_offset_of_m_MaxValue_1() { return static_cast<int32_t>(offsetof(ProgressValue_t1710479104, ___m_MaxValue_1)); }
	inline float get_m_MaxValue_1() const { return ___m_MaxValue_1; }
	inline float* get_address_of_m_MaxValue_1() { return &___m_MaxValue_1; }
	inline void set_m_MaxValue_1(float value)
	{
		___m_MaxValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSVALUE_T1710479104_H
#ifndef FILLERPROPERTY_T1637340060_H
#define FILLERPROPERTY_T1637340060_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProgressBar.Utils.FillerProperty
struct  FillerProperty_t1637340060  : public RuntimeObject
{
public:
	// System.Single ProgressBar.Utils.FillerProperty::MaxWidth
	float ___MaxWidth_0;
	// System.Single ProgressBar.Utils.FillerProperty::MinWidth
	float ___MinWidth_1;

public:
	inline static int32_t get_offset_of_MaxWidth_0() { return static_cast<int32_t>(offsetof(FillerProperty_t1637340060, ___MaxWidth_0)); }
	inline float get_MaxWidth_0() const { return ___MaxWidth_0; }
	inline float* get_address_of_MaxWidth_0() { return &___MaxWidth_0; }
	inline void set_MaxWidth_0(float value)
	{
		___MaxWidth_0 = value;
	}

	inline static int32_t get_offset_of_MinWidth_1() { return static_cast<int32_t>(offsetof(FillerProperty_t1637340060, ___MinWidth_1)); }
	inline float get_MinWidth_1() const { return ___MinWidth_1; }
	inline float* get_address_of_MinWidth_1() { return &___MinWidth_1; }
	inline void set_MinWidth_1(float value)
	{
		___MinWidth_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FILLERPROPERTY_T1637340060_H
#ifndef TRANSFORMEXTENSION_T4238110091_H
#define TRANSFORMEXTENSION_T4238110091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TransformExtension
struct  TransformExtension_t4238110091  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMEXTENSION_T4238110091_H
#ifndef JSONPARSER_T1403359166_H
#define JSONPARSER_T1403359166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sdkbox.Json/JsonParser
struct  JsonParser_t1403359166  : public RuntimeObject
{
public:
	// System.Int32 Sdkbox.Json/JsonParser::MAX_DEPTH
	int32_t ___MAX_DEPTH_0;
	// System.Int32 Sdkbox.Json/JsonParser::MAX_DIGITS
	int32_t ___MAX_DIGITS_1;
	// System.String Sdkbox.Json/JsonParser::str
	String_t* ___str_2;
	// System.Boolean Sdkbox.Json/JsonParser::failed
	bool ___failed_3;
	// System.Int32 Sdkbox.Json/JsonParser::i
	int32_t ___i_4;
	// System.String Sdkbox.Json/JsonParser::err
	String_t* ___err_5;

public:
	inline static int32_t get_offset_of_MAX_DEPTH_0() { return static_cast<int32_t>(offsetof(JsonParser_t1403359166, ___MAX_DEPTH_0)); }
	inline int32_t get_MAX_DEPTH_0() const { return ___MAX_DEPTH_0; }
	inline int32_t* get_address_of_MAX_DEPTH_0() { return &___MAX_DEPTH_0; }
	inline void set_MAX_DEPTH_0(int32_t value)
	{
		___MAX_DEPTH_0 = value;
	}

	inline static int32_t get_offset_of_MAX_DIGITS_1() { return static_cast<int32_t>(offsetof(JsonParser_t1403359166, ___MAX_DIGITS_1)); }
	inline int32_t get_MAX_DIGITS_1() const { return ___MAX_DIGITS_1; }
	inline int32_t* get_address_of_MAX_DIGITS_1() { return &___MAX_DIGITS_1; }
	inline void set_MAX_DIGITS_1(int32_t value)
	{
		___MAX_DIGITS_1 = value;
	}

	inline static int32_t get_offset_of_str_2() { return static_cast<int32_t>(offsetof(JsonParser_t1403359166, ___str_2)); }
	inline String_t* get_str_2() const { return ___str_2; }
	inline String_t** get_address_of_str_2() { return &___str_2; }
	inline void set_str_2(String_t* value)
	{
		___str_2 = value;
		Il2CppCodeGenWriteBarrier((&___str_2), value);
	}

	inline static int32_t get_offset_of_failed_3() { return static_cast<int32_t>(offsetof(JsonParser_t1403359166, ___failed_3)); }
	inline bool get_failed_3() const { return ___failed_3; }
	inline bool* get_address_of_failed_3() { return &___failed_3; }
	inline void set_failed_3(bool value)
	{
		___failed_3 = value;
	}

	inline static int32_t get_offset_of_i_4() { return static_cast<int32_t>(offsetof(JsonParser_t1403359166, ___i_4)); }
	inline int32_t get_i_4() const { return ___i_4; }
	inline int32_t* get_address_of_i_4() { return &___i_4; }
	inline void set_i_4(int32_t value)
	{
		___i_4 = value;
	}

	inline static int32_t get_offset_of_err_5() { return static_cast<int32_t>(offsetof(JsonParser_t1403359166, ___err_5)); }
	inline String_t* get_err_5() const { return ___err_5; }
	inline String_t** get_address_of_err_5() { return &___err_5; }
	inline void set_err_5(String_t* value)
	{
		___err_5 = value;
		Il2CppCodeGenWriteBarrier((&___err_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPARSER_T1403359166_H
#ifndef INITIATE_T1514427591_H
#define INITIATE_T1514427591_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Initiate
struct  Initiate_t1514427591  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITIATE_T1514427591_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2216151886_H
#define U3CSTARTU3EC__ITERATOR0_T2216151886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t2216151886  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.Benchmark01/<Start>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// TMPro.Examples.Benchmark01 TMPro.Examples.Benchmark01/<Start>c__Iterator0::$this
	Benchmark01_t1571072624 * ___U24this_1;
	// System.Object TMPro.Examples.Benchmark01/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.Benchmark01/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.Benchmark01/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24this_1)); }
	inline Benchmark01_t1571072624 * get_U24this_1() const { return ___U24this_1; }
	inline Benchmark01_t1571072624 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Benchmark01_t1571072624 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2216151886, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2216151886_H
#ifndef EFFECTSHADERPROPERTYSTR_T3661917582_H
#define EFFECTSHADERPROPERTYSTR_T3661917582_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectShaderPropertyStr
struct  EffectShaderPropertyStr_t3661917582  : public RuntimeObject
{
public:

public:
};

struct EffectShaderPropertyStr_t3661917582_StaticFields
{
public:
	// System.Int32 EffectShaderPropertyStr::Material_Color
	int32_t ___Material_Color_34;
	// System.Int32 EffectShaderPropertyStr::Material_Color_Factor
	int32_t ___Material_Color_Factor_35;

public:
	inline static int32_t get_offset_of_Material_Color_34() { return static_cast<int32_t>(offsetof(EffectShaderPropertyStr_t3661917582_StaticFields, ___Material_Color_34)); }
	inline int32_t get_Material_Color_34() const { return ___Material_Color_34; }
	inline int32_t* get_address_of_Material_Color_34() { return &___Material_Color_34; }
	inline void set_Material_Color_34(int32_t value)
	{
		___Material_Color_34 = value;
	}

	inline static int32_t get_offset_of_Material_Color_Factor_35() { return static_cast<int32_t>(offsetof(EffectShaderPropertyStr_t3661917582_StaticFields, ___Material_Color_Factor_35)); }
	inline int32_t get_Material_Color_Factor_35() const { return ___Material_Color_Factor_35; }
	inline int32_t* get_address_of_Material_Color_Factor_35() { return &___Material_Color_Factor_35; }
	inline void set_Material_Color_Factor_35(int32_t value)
	{
		___Material_Color_Factor_35 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTSHADERPROPERTYSTR_T3661917582_H
#ifndef U3CSTARTU3EC__ITERATOR0_T2622988697_H
#define U3CSTARTU3EC__ITERATOR0_T2622988697_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t2622988697  : public RuntimeObject
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::<i>__1
	int32_t ___U3CiU3E__1_0;
	// TMPro.Examples.Benchmark01_UGUI TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$this
	Benchmark01_UGUI_t3264177817 * ___U24this_1;
	// System.Object TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TMPro.Examples.Benchmark01_UGUI/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CiU3E__1_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U3CiU3E__1_0)); }
	inline int32_t get_U3CiU3E__1_0() const { return ___U3CiU3E__1_0; }
	inline int32_t* get_address_of_U3CiU3E__1_0() { return &___U3CiU3E__1_0; }
	inline void set_U3CiU3E__1_0(int32_t value)
	{
		___U3CiU3E__1_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24this_1)); }
	inline Benchmark01_UGUI_t3264177817 * get_U24this_1() const { return ___U24this_1; }
	inline Benchmark01_UGUI_t3264177817 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(Benchmark01_UGUI_t3264177817 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t2622988697, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T2622988697_H
#ifndef MINIJSONXT_T2464555382_H
#define MINIJSONXT_T2464555382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniJSON.MiniJsonXT
struct  MiniJsonXT_t2464555382  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MINIJSONXT_T2464555382_H
#ifndef BETTERLIST_T163950454_H
#define BETTERLIST_T163950454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bitware.Utils.BetterList
struct  BetterList_t163950454  : public RuntimeObject
{
public:

public:
};

struct BetterList_t163950454_StaticFields
{
public:
	// UnityEngine.RectTransform Bitware.Utils.BetterList::scrollContent
	RectTransform_t3704657025 * ___scrollContent_0;

public:
	inline static int32_t get_offset_of_scrollContent_0() { return static_cast<int32_t>(offsetof(BetterList_t163950454_StaticFields, ___scrollContent_0)); }
	inline RectTransform_t3704657025 * get_scrollContent_0() const { return ___scrollContent_0; }
	inline RectTransform_t3704657025 ** get_address_of_scrollContent_0() { return &___scrollContent_0; }
	inline void set_scrollContent_0(RectTransform_t3704657025 * value)
	{
		___scrollContent_0 = value;
		Il2CppCodeGenWriteBarrier((&___scrollContent_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BETTERLIST_T163950454_H
#ifndef U3CGETFORMMATEDNUMBERTEXTWITHLONGNAMEU3EC__ANONSTOREY0_T4053947336_H
#define U3CGETFORMMATEDNUMBERTEXTWITHLONGNAMEU3EC__ANONSTOREY0_T4053947336_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bitware.Utils.TextFormatter/<GetFormmatedNumberTextWithLongName>c__AnonStorey0
struct  U3CGetFormmatedNumberTextWithLongNameU3Ec__AnonStorey0_t4053947336  : public RuntimeObject
{
public:
	// System.Double Bitware.Utils.TextFormatter/<GetFormmatedNumberTextWithLongName>c__AnonStorey0::number
	double ___number_0;

public:
	inline static int32_t get_offset_of_number_0() { return static_cast<int32_t>(offsetof(U3CGetFormmatedNumberTextWithLongNameU3Ec__AnonStorey0_t4053947336, ___number_0)); }
	inline double get_number_0() const { return ___number_0; }
	inline double* get_address_of_number_0() { return &___number_0; }
	inline void set_number_0(double value)
	{
		___number_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETFORMMATEDNUMBERTEXTWITHLONGNAMEU3EC__ANONSTOREY0_T4053947336_H
#ifndef JSON_T3924998764_H
#define JSON_T3924998764_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniJSON.JSON
struct  JSON_t3924998764  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSON_T3924998764_H
#ifndef PARSER_T3319173113_H
#define PARSER_T3319173113_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniJSON.JSON/Parser
struct  Parser_t3319173113  : public RuntimeObject
{
public:
	// System.IO.StringReader MiniJSON.JSON/Parser::json
	StringReader_t3465604688 * ___json_1;

public:
	inline static int32_t get_offset_of_json_1() { return static_cast<int32_t>(offsetof(Parser_t3319173113, ___json_1)); }
	inline StringReader_t3465604688 * get_json_1() const { return ___json_1; }
	inline StringReader_t3465604688 ** get_address_of_json_1() { return &___json_1; }
	inline void set_json_1(StringReader_t3465604688 * value)
	{
		___json_1 = value;
		Il2CppCodeGenWriteBarrier((&___json_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSER_T3319173113_H
#ifndef SERIALIZER_T3711094893_H
#define SERIALIZER_T3711094893_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniJSON.JSON/Serializer
struct  Serializer_t3711094893  : public RuntimeObject
{
public:
	// System.Text.StringBuilder MiniJSON.JSON/Serializer::builder
	StringBuilder_t * ___builder_0;

public:
	inline static int32_t get_offset_of_builder_0() { return static_cast<int32_t>(offsetof(Serializer_t3711094893, ___builder_0)); }
	inline StringBuilder_t * get_builder_0() const { return ___builder_0; }
	inline StringBuilder_t ** get_address_of_builder_0() { return &___builder_0; }
	inline void set_builder_0(StringBuilder_t * value)
	{
		___builder_0 = value;
		Il2CppCodeGenWriteBarrier((&___builder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_T3711094893_H
#ifndef TUPLE_T2651870793_H
#define TUPLE_T2651870793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Tuple
struct  Tuple_t2651870793  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TUPLE_T2651870793_H
#ifndef UNITYEVENTBASE_T3960448221_H
#define UNITYEVENTBASE_T3960448221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEventBase
struct  UnityEventBase_t3960448221  : public RuntimeObject
{
public:
	// UnityEngine.Events.InvokableCallList UnityEngine.Events.UnityEventBase::m_Calls
	InvokableCallList_t2498835369 * ___m_Calls_0;
	// UnityEngine.Events.PersistentCallGroup UnityEngine.Events.UnityEventBase::m_PersistentCalls
	PersistentCallGroup_t3050769227 * ___m_PersistentCalls_1;
	// System.String UnityEngine.Events.UnityEventBase::m_TypeName
	String_t* ___m_TypeName_2;
	// System.Boolean UnityEngine.Events.UnityEventBase::m_CallsDirty
	bool ___m_CallsDirty_3;

public:
	inline static int32_t get_offset_of_m_Calls_0() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_Calls_0)); }
	inline InvokableCallList_t2498835369 * get_m_Calls_0() const { return ___m_Calls_0; }
	inline InvokableCallList_t2498835369 ** get_address_of_m_Calls_0() { return &___m_Calls_0; }
	inline void set_m_Calls_0(InvokableCallList_t2498835369 * value)
	{
		___m_Calls_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Calls_0), value);
	}

	inline static int32_t get_offset_of_m_PersistentCalls_1() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_PersistentCalls_1)); }
	inline PersistentCallGroup_t3050769227 * get_m_PersistentCalls_1() const { return ___m_PersistentCalls_1; }
	inline PersistentCallGroup_t3050769227 ** get_address_of_m_PersistentCalls_1() { return &___m_PersistentCalls_1; }
	inline void set_m_PersistentCalls_1(PersistentCallGroup_t3050769227 * value)
	{
		___m_PersistentCalls_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_PersistentCalls_1), value);
	}

	inline static int32_t get_offset_of_m_TypeName_2() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_TypeName_2)); }
	inline String_t* get_m_TypeName_2() const { return ___m_TypeName_2; }
	inline String_t** get_address_of_m_TypeName_2() { return &___m_TypeName_2; }
	inline void set_m_TypeName_2(String_t* value)
	{
		___m_TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TypeName_2), value);
	}

	inline static int32_t get_offset_of_m_CallsDirty_3() { return static_cast<int32_t>(offsetof(UnityEventBase_t3960448221, ___m_CallsDirty_3)); }
	inline bool get_m_CallsDirty_3() const { return ___m_CallsDirty_3; }
	inline bool* get_address_of_m_CallsDirty_3() { return &___m_CallsDirty_3; }
	inline void set_m_CallsDirty_3(bool value)
	{
		___m_CallsDirty_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENTBASE_T3960448221_H
#ifndef U3CGETFORMATTEDNUMBERTEXTU3EC__ANONSTOREY1_T2026566151_H
#define U3CGETFORMATTEDNUMBERTEXTU3EC__ANONSTOREY1_T2026566151_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bitware.Utils.TextFormatter/<GetFormattedNumberText>c__AnonStorey1
struct  U3CGetFormattedNumberTextU3Ec__AnonStorey1_t2026566151  : public RuntimeObject
{
public:
	// System.Double Bitware.Utils.TextFormatter/<GetFormattedNumberText>c__AnonStorey1::number
	double ___number_0;

public:
	inline static int32_t get_offset_of_number_0() { return static_cast<int32_t>(offsetof(U3CGetFormattedNumberTextU3Ec__AnonStorey1_t2026566151, ___number_0)); }
	inline double get_number_0() const { return ___number_0; }
	inline double* get_address_of_number_0() { return &___number_0; }
	inline void set_number_0(double value)
	{
		___number_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETFORMATTEDNUMBERTEXTU3EC__ANONSTOREY1_T2026566151_H
#ifndef U3CGETTIMEU3EC__ITERATOR0_T4198639438_H
#define U3CGETTIMEU3EC__ITERATOR0_T4198639438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeManager/<GetTime>c__Iterator0
struct  U3CGetTimeU3Ec__Iterator0_t4198639438  : public RuntimeObject
{
public:
	// UnityEngine.WWW TimeManager/<GetTime>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_0;
	// TimeManager TimeManager/<GetTime>c__Iterator0::$this
	TimeManager_t1960693005 * ___U24this_1;
	// System.Object TimeManager/<GetTime>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean TimeManager/<GetTime>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 TimeManager/<GetTime>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CwwwU3E__0_0() { return static_cast<int32_t>(offsetof(U3CGetTimeU3Ec__Iterator0_t4198639438, ___U3CwwwU3E__0_0)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_0() const { return ___U3CwwwU3E__0_0; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_0() { return &___U3CwwwU3E__0_0; }
	inline void set_U3CwwwU3E__0_0(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CGetTimeU3Ec__Iterator0_t4198639438, ___U24this_1)); }
	inline TimeManager_t1960693005 * get_U24this_1() const { return ___U24this_1; }
	inline TimeManager_t1960693005 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(TimeManager_t1960693005 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CGetTimeU3Ec__Iterator0_t4198639438, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CGetTimeU3Ec__Iterator0_t4198639438, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CGetTimeU3Ec__Iterator0_t4198639438, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETTIMEU3EC__ITERATOR0_T4198639438_H
#ifndef U3CSHOWPOPUPU3EC__ANONSTOREY1_T3277290483_H
#define U3CSHOWPOPUPU3EC__ANONSTOREY1_T3277290483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bitware.Utils.UnityEditorPopup/<ShowPopup>c__AnonStorey1
struct  U3CShowPopupU3Ec__AnonStorey1_t3277290483  : public RuntimeObject
{
public:
	// System.Int32 Bitware.Utils.UnityEditorPopup/<ShowPopup>c__AnonStorey1::a
	int32_t ___a_0;
	// Bitware.Utils.UnityEditorPopup/<ShowPopup>c__AnonStorey0 Bitware.Utils.UnityEditorPopup/<ShowPopup>c__AnonStorey1::<>f__ref$0
	U3CShowPopupU3Ec__AnonStorey0_t938638323 * ___U3CU3Ef__refU240_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(U3CShowPopupU3Ec__AnonStorey1_t3277290483, ___a_0)); }
	inline int32_t get_a_0() const { return ___a_0; }
	inline int32_t* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(int32_t value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_1() { return static_cast<int32_t>(offsetof(U3CShowPopupU3Ec__AnonStorey1_t3277290483, ___U3CU3Ef__refU240_1)); }
	inline U3CShowPopupU3Ec__AnonStorey0_t938638323 * get_U3CU3Ef__refU240_1() const { return ___U3CU3Ef__refU240_1; }
	inline U3CShowPopupU3Ec__AnonStorey0_t938638323 ** get_address_of_U3CU3Ef__refU240_1() { return &___U3CU3Ef__refU240_1; }
	inline void set_U3CU3Ef__refU240_1(U3CShowPopupU3Ec__AnonStorey0_t938638323 * value)
	{
		___U3CU3Ef__refU240_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWPOPUPU3EC__ANONSTOREY1_T3277290483_H
#ifndef U3CSHOWPOPUPU3EC__ANONSTOREY0_T938638323_H
#define U3CSHOWPOPUPU3EC__ANONSTOREY0_T938638323_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bitware.Utils.UnityEditorPopup/<ShowPopup>c__AnonStorey0
struct  U3CShowPopupU3Ec__AnonStorey0_t938638323  : public RuntimeObject
{
public:
	// System.Action`1<System.Int32> Bitware.Utils.UnityEditorPopup/<ShowPopup>c__AnonStorey0::action
	Action_1_t3123413348 * ___action_0;

public:
	inline static int32_t get_offset_of_action_0() { return static_cast<int32_t>(offsetof(U3CShowPopupU3Ec__AnonStorey0_t938638323, ___action_0)); }
	inline Action_1_t3123413348 * get_action_0() const { return ___action_0; }
	inline Action_1_t3123413348 ** get_address_of_action_0() { return &___action_0; }
	inline void set_action_0(Action_1_t3123413348 * value)
	{
		___action_0 = value;
		Il2CppCodeGenWriteBarrier((&___action_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSHOWPOPUPU3EC__ANONSTOREY0_T938638323_H
#ifndef BETTERARRAY_T1983019263_H
#define BETTERARRAY_T1983019263_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bitware.Utils.BetterArray
struct  BetterArray_t1983019263  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BETTERARRAY_T1983019263_H
#ifndef TEXTFORMATTER_T3711854288_H
#define TEXTFORMATTER_T3711854288_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bitware.Utils.TextFormatter
struct  TextFormatter_t3711854288  : public RuntimeObject
{
public:

public:
};

struct TextFormatter_t3711854288_StaticFields
{
public:
	// System.Collections.Generic.List`1<Tuple`4<System.Double,System.Double,System.String,System.String>> Bitware.Utils.TextFormatter::NumberFormatters
	List_1_t312090022 * ___NumberFormatters_0;

public:
	inline static int32_t get_offset_of_NumberFormatters_0() { return static_cast<int32_t>(offsetof(TextFormatter_t3711854288_StaticFields, ___NumberFormatters_0)); }
	inline List_1_t312090022 * get_NumberFormatters_0() const { return ___NumberFormatters_0; }
	inline List_1_t312090022 ** get_address_of_NumberFormatters_0() { return &___NumberFormatters_0; }
	inline void set_NumberFormatters_0(List_1_t312090022 * value)
	{
		___NumberFormatters_0 = value;
		Il2CppCodeGenWriteBarrier((&___NumberFormatters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTFORMATTER_T3711854288_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef COLOR32_T2600501292_H
#define COLOR32_T2600501292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color32
struct  Color32_t2600501292 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 UnityEngine.Color32::rgba
			int32_t ___rgba_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___rgba_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Byte UnityEngine.Color32::r
			uint8_t ___r_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			uint8_t ___r_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___g_2_OffsetPadding[1];
			// System.Byte UnityEngine.Color32::g
			uint8_t ___g_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___g_2_OffsetPadding_forAlignmentOnly[1];
			uint8_t ___g_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___b_3_OffsetPadding[2];
			// System.Byte UnityEngine.Color32::b
			uint8_t ___b_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___b_3_OffsetPadding_forAlignmentOnly[2];
			uint8_t ___b_3_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			char ___a_4_OffsetPadding[3];
			// System.Byte UnityEngine.Color32::a
			uint8_t ___a_4;
		};
		#pragma pack(pop, tp)
		struct
		{
			char ___a_4_OffsetPadding_forAlignmentOnly[3];
			uint8_t ___a_4_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_rgba_0() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___rgba_0)); }
	inline int32_t get_rgba_0() const { return ___rgba_0; }
	inline int32_t* get_address_of_rgba_0() { return &___rgba_0; }
	inline void set_rgba_0(int32_t value)
	{
		___rgba_0 = value;
	}

	inline static int32_t get_offset_of_r_1() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___r_1)); }
	inline uint8_t get_r_1() const { return ___r_1; }
	inline uint8_t* get_address_of_r_1() { return &___r_1; }
	inline void set_r_1(uint8_t value)
	{
		___r_1 = value;
	}

	inline static int32_t get_offset_of_g_2() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___g_2)); }
	inline uint8_t get_g_2() const { return ___g_2; }
	inline uint8_t* get_address_of_g_2() { return &___g_2; }
	inline void set_g_2(uint8_t value)
	{
		___g_2 = value;
	}

	inline static int32_t get_offset_of_b_3() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___b_3)); }
	inline uint8_t get_b_3() const { return ___b_3; }
	inline uint8_t* get_address_of_b_3() { return &___b_3; }
	inline void set_b_3(uint8_t value)
	{
		___b_3 = value;
	}

	inline static int32_t get_offset_of_a_4() { return static_cast<int32_t>(offsetof(Color32_t2600501292, ___a_4)); }
	inline uint8_t get_a_4() const { return ___a_4; }
	inline uint8_t* get_address_of_a_4() { return &___a_4; }
	inline void set_a_4(uint8_t value)
	{
		___a_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32_T2600501292_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef PRODUCTDESCRIPTION_T2131666506_H
#define PRODUCTDESCRIPTION_T2131666506_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sdkbox.ProductDescription
struct  ProductDescription_t2131666506 
{
public:
	// System.String Sdkbox.ProductDescription::name
	String_t* ___name_0;
	// System.String Sdkbox.ProductDescription::id
	String_t* ___id_1;
	// System.Boolean Sdkbox.ProductDescription::consumable
	bool ___consumable_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(ProductDescription_t2131666506, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(ProductDescription_t2131666506, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_consumable_2() { return static_cast<int32_t>(offsetof(ProductDescription_t2131666506, ___consumable_2)); }
	inline bool get_consumable_2() const { return ___consumable_2; }
	inline bool* get_address_of_consumable_2() { return &___consumable_2; }
	inline void set_consumable_2(bool value)
	{
		___consumable_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Sdkbox.ProductDescription
struct ProductDescription_t2131666506_marshaled_pinvoke
{
	char* ___name_0;
	char* ___id_1;
	int32_t ___consumable_2;
};
// Native definition for COM marshalling of Sdkbox.ProductDescription
struct ProductDescription_t2131666506_marshaled_com
{
	Il2CppChar* ___name_0;
	Il2CppChar* ___id_1;
	int32_t ___consumable_2;
};
#endif // PRODUCTDESCRIPTION_T2131666506_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef UNITYEVENT_2_T3730059085_H
#define UNITYEVENT_2_T3730059085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<System.Boolean,System.String>
struct  UnityEvent_2_t3730059085  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t3730059085, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T3730059085_H
#ifndef UNITYEVENT_1_T1733074785_H
#define UNITYEVENT_1_T1733074785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Sdkbox.Product>
struct  UnityEvent_1_t1733074785  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t1733074785, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T1733074785_H
#ifndef UNITYEVENT_2_T3163854761_H
#define UNITYEVENT_2_T3163854761_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`2<Sdkbox.Product,System.String>
struct  UnityEvent_2_t3163854761  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`2::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_2_t3163854761, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_2_T3163854761_H
#ifndef UNITYEVENT_1_T3212873372_H
#define UNITYEVENT_1_T3212873372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<Sdkbox.Product[]>
struct  UnityEvent_1_t3212873372  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t3212873372, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T3212873372_H
#ifndef UNITYEVENT_1_T2729110193_H
#define UNITYEVENT_1_T2729110193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.String>
struct  UnityEvent_1_t2729110193  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t2729110193, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T2729110193_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef MATRIX4X4_T1817901843_H
#define MATRIX4X4_T1817901843_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Matrix4x4
struct  Matrix4x4_t1817901843 
{
public:
	// System.Single UnityEngine.Matrix4x4::m00
	float ___m00_0;
	// System.Single UnityEngine.Matrix4x4::m10
	float ___m10_1;
	// System.Single UnityEngine.Matrix4x4::m20
	float ___m20_2;
	// System.Single UnityEngine.Matrix4x4::m30
	float ___m30_3;
	// System.Single UnityEngine.Matrix4x4::m01
	float ___m01_4;
	// System.Single UnityEngine.Matrix4x4::m11
	float ___m11_5;
	// System.Single UnityEngine.Matrix4x4::m21
	float ___m21_6;
	// System.Single UnityEngine.Matrix4x4::m31
	float ___m31_7;
	// System.Single UnityEngine.Matrix4x4::m02
	float ___m02_8;
	// System.Single UnityEngine.Matrix4x4::m12
	float ___m12_9;
	// System.Single UnityEngine.Matrix4x4::m22
	float ___m22_10;
	// System.Single UnityEngine.Matrix4x4::m32
	float ___m32_11;
	// System.Single UnityEngine.Matrix4x4::m03
	float ___m03_12;
	// System.Single UnityEngine.Matrix4x4::m13
	float ___m13_13;
	// System.Single UnityEngine.Matrix4x4::m23
	float ___m23_14;
	// System.Single UnityEngine.Matrix4x4::m33
	float ___m33_15;

public:
	inline static int32_t get_offset_of_m00_0() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m00_0)); }
	inline float get_m00_0() const { return ___m00_0; }
	inline float* get_address_of_m00_0() { return &___m00_0; }
	inline void set_m00_0(float value)
	{
		___m00_0 = value;
	}

	inline static int32_t get_offset_of_m10_1() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m10_1)); }
	inline float get_m10_1() const { return ___m10_1; }
	inline float* get_address_of_m10_1() { return &___m10_1; }
	inline void set_m10_1(float value)
	{
		___m10_1 = value;
	}

	inline static int32_t get_offset_of_m20_2() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m20_2)); }
	inline float get_m20_2() const { return ___m20_2; }
	inline float* get_address_of_m20_2() { return &___m20_2; }
	inline void set_m20_2(float value)
	{
		___m20_2 = value;
	}

	inline static int32_t get_offset_of_m30_3() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m30_3)); }
	inline float get_m30_3() const { return ___m30_3; }
	inline float* get_address_of_m30_3() { return &___m30_3; }
	inline void set_m30_3(float value)
	{
		___m30_3 = value;
	}

	inline static int32_t get_offset_of_m01_4() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m01_4)); }
	inline float get_m01_4() const { return ___m01_4; }
	inline float* get_address_of_m01_4() { return &___m01_4; }
	inline void set_m01_4(float value)
	{
		___m01_4 = value;
	}

	inline static int32_t get_offset_of_m11_5() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m11_5)); }
	inline float get_m11_5() const { return ___m11_5; }
	inline float* get_address_of_m11_5() { return &___m11_5; }
	inline void set_m11_5(float value)
	{
		___m11_5 = value;
	}

	inline static int32_t get_offset_of_m21_6() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m21_6)); }
	inline float get_m21_6() const { return ___m21_6; }
	inline float* get_address_of_m21_6() { return &___m21_6; }
	inline void set_m21_6(float value)
	{
		___m21_6 = value;
	}

	inline static int32_t get_offset_of_m31_7() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m31_7)); }
	inline float get_m31_7() const { return ___m31_7; }
	inline float* get_address_of_m31_7() { return &___m31_7; }
	inline void set_m31_7(float value)
	{
		___m31_7 = value;
	}

	inline static int32_t get_offset_of_m02_8() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m02_8)); }
	inline float get_m02_8() const { return ___m02_8; }
	inline float* get_address_of_m02_8() { return &___m02_8; }
	inline void set_m02_8(float value)
	{
		___m02_8 = value;
	}

	inline static int32_t get_offset_of_m12_9() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m12_9)); }
	inline float get_m12_9() const { return ___m12_9; }
	inline float* get_address_of_m12_9() { return &___m12_9; }
	inline void set_m12_9(float value)
	{
		___m12_9 = value;
	}

	inline static int32_t get_offset_of_m22_10() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m22_10)); }
	inline float get_m22_10() const { return ___m22_10; }
	inline float* get_address_of_m22_10() { return &___m22_10; }
	inline void set_m22_10(float value)
	{
		___m22_10 = value;
	}

	inline static int32_t get_offset_of_m32_11() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m32_11)); }
	inline float get_m32_11() const { return ___m32_11; }
	inline float* get_address_of_m32_11() { return &___m32_11; }
	inline void set_m32_11(float value)
	{
		___m32_11 = value;
	}

	inline static int32_t get_offset_of_m03_12() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m03_12)); }
	inline float get_m03_12() const { return ___m03_12; }
	inline float* get_address_of_m03_12() { return &___m03_12; }
	inline void set_m03_12(float value)
	{
		___m03_12 = value;
	}

	inline static int32_t get_offset_of_m13_13() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m13_13)); }
	inline float get_m13_13() const { return ___m13_13; }
	inline float* get_address_of_m13_13() { return &___m13_13; }
	inline void set_m13_13(float value)
	{
		___m13_13 = value;
	}

	inline static int32_t get_offset_of_m23_14() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m23_14)); }
	inline float get_m23_14() const { return ___m23_14; }
	inline float* get_address_of_m23_14() { return &___m23_14; }
	inline void set_m23_14(float value)
	{
		___m23_14 = value;
	}

	inline static int32_t get_offset_of_m33_15() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843, ___m33_15)); }
	inline float get_m33_15() const { return ___m33_15; }
	inline float* get_address_of_m33_15() { return &___m33_15; }
	inline void set_m33_15(float value)
	{
		___m33_15 = value;
	}
};

struct Matrix4x4_t1817901843_StaticFields
{
public:
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::zeroMatrix
	Matrix4x4_t1817901843  ___zeroMatrix_16;
	// UnityEngine.Matrix4x4 UnityEngine.Matrix4x4::identityMatrix
	Matrix4x4_t1817901843  ___identityMatrix_17;

public:
	inline static int32_t get_offset_of_zeroMatrix_16() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___zeroMatrix_16)); }
	inline Matrix4x4_t1817901843  get_zeroMatrix_16() const { return ___zeroMatrix_16; }
	inline Matrix4x4_t1817901843 * get_address_of_zeroMatrix_16() { return &___zeroMatrix_16; }
	inline void set_zeroMatrix_16(Matrix4x4_t1817901843  value)
	{
		___zeroMatrix_16 = value;
	}

	inline static int32_t get_offset_of_identityMatrix_17() { return static_cast<int32_t>(offsetof(Matrix4x4_t1817901843_StaticFields, ___identityMatrix_17)); }
	inline Matrix4x4_t1817901843  get_identityMatrix_17() const { return ___identityMatrix_17; }
	inline Matrix4x4_t1817901843 * get_address_of_identityMatrix_17() { return &___identityMatrix_17; }
	inline void set_identityMatrix_17(Matrix4x4_t1817901843  value)
	{
		___identityMatrix_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIX4X4_T1817901843_H
#ifndef UNITYEVENT_1_T978947469_H
#define UNITYEVENT_1_T978947469_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent`1<System.Boolean>
struct  UnityEvent_1_t978947469  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent`1::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_1_t978947469, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_1_T978947469_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef UNITYEVENT_T2581268647_H
#define UNITYEVENT_T2581268647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Events.UnityEvent
struct  UnityEvent_t2581268647  : public UnityEventBase_t3960448221
{
public:
	// System.Object[] UnityEngine.Events.UnityEvent::m_InvokeArray
	ObjectU5BU5D_t2843939325* ___m_InvokeArray_4;

public:
	inline static int32_t get_offset_of_m_InvokeArray_4() { return static_cast<int32_t>(offsetof(UnityEvent_t2581268647, ___m_InvokeArray_4)); }
	inline ObjectU5BU5D_t2843939325* get_m_InvokeArray_4() const { return ___m_InvokeArray_4; }
	inline ObjectU5BU5D_t2843939325** get_address_of_m_InvokeArray_4() { return &___m_InvokeArray_4; }
	inline void set_m_InvokeArray_4(ObjectU5BU5D_t2843939325* value)
	{
		___m_InvokeArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_InvokeArray_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEVENT_T2581268647_H
#ifndef NULLABLE_1_T3119828856_H
#define NULLABLE_1_T3119828856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Single>
struct  Nullable_1_t3119828856 
{
public:
	// T System.Nullable`1::value
	float ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3119828856, ___value_0)); }
	inline float get_value_0() const { return ___value_0; }
	inline float* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(float value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3119828856, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3119828856_H
#ifndef PRODUCTSTRINGEVENT_T801016626_H
#define PRODUCTSTRINGEVENT_T801016626_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sdkbox.IAP/Callbacks/ProductStringEvent
struct  ProductStringEvent_t801016626  : public UnityEvent_2_t3163854761
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTSTRINGEVENT_T801016626_H
#ifndef PRODUCTEVENT_T4069975844_H
#define PRODUCTEVENT_T4069975844_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sdkbox.IAP/Callbacks/ProductEvent
struct  ProductEvent_t4069975844  : public UnityEvent_1_t1733074785
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTEVENT_T4069975844_H
#ifndef SHAREDIALOGMODE_T1679970535_H
#define SHAREDIALOGMODE_T1679970535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ShareDialogMode
struct  ShareDialogMode_t1679970535 
{
public:
	// System.Int32 Facebook.Unity.ShareDialogMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ShareDialogMode_t1679970535, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAREDIALOGMODE_T1679970535_H
#ifndef BOOLSTRINGEVENT_T1829059983_H
#define BOOLSTRINGEVENT_T1829059983_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sdkbox.IAP/Callbacks/BoolStringEvent
struct  BoolStringEvent_t1829059983  : public UnityEvent_2_t3730059085
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLSTRINGEVENT_T1829059983_H
#ifndef PRODUCTARRAYEVENT_T1714183732_H
#define PRODUCTARRAYEVENT_T1714183732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sdkbox.IAP/Callbacks/ProductArrayEvent
struct  ProductArrayEvent_t1714183732  : public UnityEvent_1_t3212873372
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTARRAYEVENT_T1714183732_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef CAMERAMODES_T3200559075_H
#define CAMERAMODES_T3200559075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.CameraController/CameraModes
struct  CameraModes_t3200559075 
{
public:
	// System.Int32 TMPro.Examples.CameraController/CameraModes::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(CameraModes_t3200559075, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAMODES_T3200559075_H
#ifndef U3CSTARTU3EC__ITERATOR0_T1520811813_H
#define U3CSTARTU3EC__ITERATOR0_T1520811813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvMapAnimator/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t1520811813  : public RuntimeObject
{
public:
	// UnityEngine.Matrix4x4 EnvMapAnimator/<Start>c__Iterator0::<matrix>__0
	Matrix4x4_t1817901843  ___U3CmatrixU3E__0_0;
	// EnvMapAnimator EnvMapAnimator/<Start>c__Iterator0::$this
	EnvMapAnimator_t1140999784 * ___U24this_1;
	// System.Object EnvMapAnimator/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean EnvMapAnimator/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 EnvMapAnimator/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CmatrixU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U3CmatrixU3E__0_0)); }
	inline Matrix4x4_t1817901843  get_U3CmatrixU3E__0_0() const { return ___U3CmatrixU3E__0_0; }
	inline Matrix4x4_t1817901843 * get_address_of_U3CmatrixU3E__0_0() { return &___U3CmatrixU3E__0_0; }
	inline void set_U3CmatrixU3E__0_0(Matrix4x4_t1817901843  value)
	{
		___U3CmatrixU3E__0_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24this_1)); }
	inline EnvMapAnimator_t1140999784 * get_U24this_1() const { return ___U24this_1; }
	inline EnvMapAnimator_t1140999784 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(EnvMapAnimator_t1140999784 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1520811813, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T1520811813_H
#ifndef MOTIONTYPE_T1905163921_H
#define MOTIONTYPE_T1905163921_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ObjectSpin/MotionType
struct  MotionType_t1905163921 
{
public:
	// System.Int32 TMPro.Examples.ObjectSpin/MotionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(MotionType_t1905163921, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOTIONTYPE_T1905163921_H
#ifndef TEXTUREWRAPMODE_T584250749_H
#define TEXTUREWRAPMODE_T584250749_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.TextureWrapMode
struct  TextureWrapMode_t584250749 
{
public:
	// System.Int32 UnityEngine.TextureWrapMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TextureWrapMode_t584250749, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TEXTUREWRAPMODE_T584250749_H
#ifndef TYPE_T216879902_H
#define TYPE_T216879902_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sdkbox.Json/Type
struct  Type_t216879902 
{
public:
	// System.Int32 Sdkbox.Json/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t216879902, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T216879902_H
#ifndef STRINGEVENT_T4181969553_H
#define STRINGEVENT_T4181969553_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sdkbox.IAP/Callbacks/StringEvent
struct  StringEvent_t4181969553  : public UnityEvent_1_t2729110193
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGEVENT_T4181969553_H
#ifndef BOOLEVENT_T3697584372_H
#define BOOLEVENT_T3697584372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sdkbox.IAP/Callbacks/BoolEvent
struct  BoolEvent_t3697584372  : public UnityEvent_1_t978947469
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEVENT_T3697584372_H
#ifndef TOKEN_T3312601886_H
#define TOKEN_T3312601886_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MiniJSON.JSON/Parser/TOKEN
struct  TOKEN_t3312601886 
{
public:
	// System.Int32 MiniJSON.JSON/Parser/TOKEN::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TOKEN_t3312601886, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T3312601886_H
#ifndef RENDERBILLBOARDTYPE_T3907887595_H
#define RENDERBILLBOARDTYPE_T3907887595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderBillBoardType
struct  RenderBillBoardType_t3907887595 
{
public:
	// System.Int32 RenderBillBoardType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(RenderBillBoardType_t3907887595, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDERBILLBOARDTYPE_T3907887595_H
#ifndef ONCOMPLETEEVENT_T1779196559_H
#define ONCOMPLETEEVENT_T1779196559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProgressBar.Utils.OnCompleteEvent
struct  OnCompleteEvent_t1779196559  : public UnityEvent_t2581268647
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCOMPLETEEVENT_T1779196559_H
#ifndef EFFECTDATA_T1709847781_H
#define EFFECTDATA_T1709847781_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectData
struct  EffectData_t1709847781  : public RuntimeObject
{
public:
	// System.Boolean EffectData::m_bFoldoutOpen
	bool ___m_bFoldoutOpen_0;
	// System.Single EffectData::m_fTimeSec
	float ___m_fTimeSec_1;
	// UnityEngine.GameObject EffectData::m_goEffect
	GameObject_t1113636619 * ___m_goEffect_2;
	// System.Boolean EffectData::m_bTransformFoldout
	bool ___m_bTransformFoldout_3;
	// UnityEngine.Vector3 EffectData::m_goPos
	Vector3_t3722313464  ___m_goPos_4;
	// UnityEngine.Vector3 EffectData::m_goRotation
	Vector3_t3722313464  ___m_goRotation_5;
	// UnityEngine.Vector3 EffectData::m_goScale
	Vector3_t3722313464  ___m_goScale_6;
	// System.Boolean EffectData::m_bSortingFoldout
	bool ___m_bSortingFoldout_7;
	// System.Int32 EffectData::m_SortingLayerID
	int32_t ___m_SortingLayerID_8;
	// System.Int32 EffectData::m_SortingOrder
	int32_t ___m_SortingOrder_9;

public:
	inline static int32_t get_offset_of_m_bFoldoutOpen_0() { return static_cast<int32_t>(offsetof(EffectData_t1709847781, ___m_bFoldoutOpen_0)); }
	inline bool get_m_bFoldoutOpen_0() const { return ___m_bFoldoutOpen_0; }
	inline bool* get_address_of_m_bFoldoutOpen_0() { return &___m_bFoldoutOpen_0; }
	inline void set_m_bFoldoutOpen_0(bool value)
	{
		___m_bFoldoutOpen_0 = value;
	}

	inline static int32_t get_offset_of_m_fTimeSec_1() { return static_cast<int32_t>(offsetof(EffectData_t1709847781, ___m_fTimeSec_1)); }
	inline float get_m_fTimeSec_1() const { return ___m_fTimeSec_1; }
	inline float* get_address_of_m_fTimeSec_1() { return &___m_fTimeSec_1; }
	inline void set_m_fTimeSec_1(float value)
	{
		___m_fTimeSec_1 = value;
	}

	inline static int32_t get_offset_of_m_goEffect_2() { return static_cast<int32_t>(offsetof(EffectData_t1709847781, ___m_goEffect_2)); }
	inline GameObject_t1113636619 * get_m_goEffect_2() const { return ___m_goEffect_2; }
	inline GameObject_t1113636619 ** get_address_of_m_goEffect_2() { return &___m_goEffect_2; }
	inline void set_m_goEffect_2(GameObject_t1113636619 * value)
	{
		___m_goEffect_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_goEffect_2), value);
	}

	inline static int32_t get_offset_of_m_bTransformFoldout_3() { return static_cast<int32_t>(offsetof(EffectData_t1709847781, ___m_bTransformFoldout_3)); }
	inline bool get_m_bTransformFoldout_3() const { return ___m_bTransformFoldout_3; }
	inline bool* get_address_of_m_bTransformFoldout_3() { return &___m_bTransformFoldout_3; }
	inline void set_m_bTransformFoldout_3(bool value)
	{
		___m_bTransformFoldout_3 = value;
	}

	inline static int32_t get_offset_of_m_goPos_4() { return static_cast<int32_t>(offsetof(EffectData_t1709847781, ___m_goPos_4)); }
	inline Vector3_t3722313464  get_m_goPos_4() const { return ___m_goPos_4; }
	inline Vector3_t3722313464 * get_address_of_m_goPos_4() { return &___m_goPos_4; }
	inline void set_m_goPos_4(Vector3_t3722313464  value)
	{
		___m_goPos_4 = value;
	}

	inline static int32_t get_offset_of_m_goRotation_5() { return static_cast<int32_t>(offsetof(EffectData_t1709847781, ___m_goRotation_5)); }
	inline Vector3_t3722313464  get_m_goRotation_5() const { return ___m_goRotation_5; }
	inline Vector3_t3722313464 * get_address_of_m_goRotation_5() { return &___m_goRotation_5; }
	inline void set_m_goRotation_5(Vector3_t3722313464  value)
	{
		___m_goRotation_5 = value;
	}

	inline static int32_t get_offset_of_m_goScale_6() { return static_cast<int32_t>(offsetof(EffectData_t1709847781, ___m_goScale_6)); }
	inline Vector3_t3722313464  get_m_goScale_6() const { return ___m_goScale_6; }
	inline Vector3_t3722313464 * get_address_of_m_goScale_6() { return &___m_goScale_6; }
	inline void set_m_goScale_6(Vector3_t3722313464  value)
	{
		___m_goScale_6 = value;
	}

	inline static int32_t get_offset_of_m_bSortingFoldout_7() { return static_cast<int32_t>(offsetof(EffectData_t1709847781, ___m_bSortingFoldout_7)); }
	inline bool get_m_bSortingFoldout_7() const { return ___m_bSortingFoldout_7; }
	inline bool* get_address_of_m_bSortingFoldout_7() { return &___m_bSortingFoldout_7; }
	inline void set_m_bSortingFoldout_7(bool value)
	{
		___m_bSortingFoldout_7 = value;
	}

	inline static int32_t get_offset_of_m_SortingLayerID_8() { return static_cast<int32_t>(offsetof(EffectData_t1709847781, ___m_SortingLayerID_8)); }
	inline int32_t get_m_SortingLayerID_8() const { return ___m_SortingLayerID_8; }
	inline int32_t* get_address_of_m_SortingLayerID_8() { return &___m_SortingLayerID_8; }
	inline void set_m_SortingLayerID_8(int32_t value)
	{
		___m_SortingLayerID_8 = value;
	}

	inline static int32_t get_offset_of_m_SortingOrder_9() { return static_cast<int32_t>(offsetof(EffectData_t1709847781, ___m_SortingOrder_9)); }
	inline int32_t get_m_SortingOrder_9() const { return ___m_SortingOrder_9; }
	inline int32_t* get_address_of_m_SortingOrder_9() { return &___m_SortingOrder_9; }
	inline void set_m_SortingOrder_9(int32_t value)
	{
		___m_SortingOrder_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTDATA_T1709847781_H
#ifndef TYPE_T309803420_H
#define TYPE_T309803420_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sdkbox.Product/Type
struct  Type_t309803420 
{
public:
	// System.Int32 Sdkbox.Product/Type::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Type_t309803420, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T309803420_H
#ifndef SCROLLTYPE_T1869506883_H
#define SCROLLTYPE_T1869506883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bitware.Utils.ScrollType
struct  ScrollType_t1869506883 
{
public:
	// System.Int32 Bitware.Utils.ScrollType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ScrollType_t1869506883, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCROLLTYPE_T1869506883_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef MATERIALEFFECT_T3892150953_H
#define MATERIALEFFECT_T3892150953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MaterialEffect
struct  MaterialEffect_t3892150953  : public RuntimeObject
{
public:
	// UnityEngine.Material MaterialEffect::m_EffectMaterial
	Material_t340375123 * ___m_EffectMaterial_0;
	// System.Boolean MaterialEffect::m_EnableAlphaAnimation
	bool ___m_EnableAlphaAnimation_1;
	// System.Single MaterialEffect::m_AlphaAnimationTimeScale
	float ___m_AlphaAnimationTimeScale_2;
	// UnityEngine.AnimationCurve MaterialEffect::m_AlphaCurve
	AnimationCurve_t3046754366 * ___m_AlphaCurve_3;
	// UnityEngine.Texture MaterialEffect::m_MainTexture
	Texture_t3661962703 * ___m_MainTexture_4;
	// UnityEngine.Texture MaterialEffect::m_MaskTexutre
	Texture_t3661962703 * ___m_MaskTexutre_5;
	// UnityEngine.TextureWrapMode MaterialEffect::m_MainTexWrapMode
	int32_t ___m_MainTexWrapMode_6;
	// UnityEngine.TextureWrapMode MaterialEffect::m_MaskTexWrapMode
	int32_t ___m_MaskTexWrapMode_7;
	// System.Boolean MaterialEffect::m_EnableUVScroll
	bool ___m_EnableUVScroll_8;
	// UnityEngine.Vector2 MaterialEffect::m_UVScrollMainTex
	Vector2_t2156229523  ___m_UVScrollMainTex_9;
	// UnityEngine.Vector2 MaterialEffect::m_UVScrollCutTex
	Vector2_t2156229523  ___m_UVScrollCutTex_10;

public:
	inline static int32_t get_offset_of_m_EffectMaterial_0() { return static_cast<int32_t>(offsetof(MaterialEffect_t3892150953, ___m_EffectMaterial_0)); }
	inline Material_t340375123 * get_m_EffectMaterial_0() const { return ___m_EffectMaterial_0; }
	inline Material_t340375123 ** get_address_of_m_EffectMaterial_0() { return &___m_EffectMaterial_0; }
	inline void set_m_EffectMaterial_0(Material_t340375123 * value)
	{
		___m_EffectMaterial_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_EffectMaterial_0), value);
	}

	inline static int32_t get_offset_of_m_EnableAlphaAnimation_1() { return static_cast<int32_t>(offsetof(MaterialEffect_t3892150953, ___m_EnableAlphaAnimation_1)); }
	inline bool get_m_EnableAlphaAnimation_1() const { return ___m_EnableAlphaAnimation_1; }
	inline bool* get_address_of_m_EnableAlphaAnimation_1() { return &___m_EnableAlphaAnimation_1; }
	inline void set_m_EnableAlphaAnimation_1(bool value)
	{
		___m_EnableAlphaAnimation_1 = value;
	}

	inline static int32_t get_offset_of_m_AlphaAnimationTimeScale_2() { return static_cast<int32_t>(offsetof(MaterialEffect_t3892150953, ___m_AlphaAnimationTimeScale_2)); }
	inline float get_m_AlphaAnimationTimeScale_2() const { return ___m_AlphaAnimationTimeScale_2; }
	inline float* get_address_of_m_AlphaAnimationTimeScale_2() { return &___m_AlphaAnimationTimeScale_2; }
	inline void set_m_AlphaAnimationTimeScale_2(float value)
	{
		___m_AlphaAnimationTimeScale_2 = value;
	}

	inline static int32_t get_offset_of_m_AlphaCurve_3() { return static_cast<int32_t>(offsetof(MaterialEffect_t3892150953, ___m_AlphaCurve_3)); }
	inline AnimationCurve_t3046754366 * get_m_AlphaCurve_3() const { return ___m_AlphaCurve_3; }
	inline AnimationCurve_t3046754366 ** get_address_of_m_AlphaCurve_3() { return &___m_AlphaCurve_3; }
	inline void set_m_AlphaCurve_3(AnimationCurve_t3046754366 * value)
	{
		___m_AlphaCurve_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_AlphaCurve_3), value);
	}

	inline static int32_t get_offset_of_m_MainTexture_4() { return static_cast<int32_t>(offsetof(MaterialEffect_t3892150953, ___m_MainTexture_4)); }
	inline Texture_t3661962703 * get_m_MainTexture_4() const { return ___m_MainTexture_4; }
	inline Texture_t3661962703 ** get_address_of_m_MainTexture_4() { return &___m_MainTexture_4; }
	inline void set_m_MainTexture_4(Texture_t3661962703 * value)
	{
		___m_MainTexture_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_MainTexture_4), value);
	}

	inline static int32_t get_offset_of_m_MaskTexutre_5() { return static_cast<int32_t>(offsetof(MaterialEffect_t3892150953, ___m_MaskTexutre_5)); }
	inline Texture_t3661962703 * get_m_MaskTexutre_5() const { return ___m_MaskTexutre_5; }
	inline Texture_t3661962703 ** get_address_of_m_MaskTexutre_5() { return &___m_MaskTexutre_5; }
	inline void set_m_MaskTexutre_5(Texture_t3661962703 * value)
	{
		___m_MaskTexutre_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaskTexutre_5), value);
	}

	inline static int32_t get_offset_of_m_MainTexWrapMode_6() { return static_cast<int32_t>(offsetof(MaterialEffect_t3892150953, ___m_MainTexWrapMode_6)); }
	inline int32_t get_m_MainTexWrapMode_6() const { return ___m_MainTexWrapMode_6; }
	inline int32_t* get_address_of_m_MainTexWrapMode_6() { return &___m_MainTexWrapMode_6; }
	inline void set_m_MainTexWrapMode_6(int32_t value)
	{
		___m_MainTexWrapMode_6 = value;
	}

	inline static int32_t get_offset_of_m_MaskTexWrapMode_7() { return static_cast<int32_t>(offsetof(MaterialEffect_t3892150953, ___m_MaskTexWrapMode_7)); }
	inline int32_t get_m_MaskTexWrapMode_7() const { return ___m_MaskTexWrapMode_7; }
	inline int32_t* get_address_of_m_MaskTexWrapMode_7() { return &___m_MaskTexWrapMode_7; }
	inline void set_m_MaskTexWrapMode_7(int32_t value)
	{
		___m_MaskTexWrapMode_7 = value;
	}

	inline static int32_t get_offset_of_m_EnableUVScroll_8() { return static_cast<int32_t>(offsetof(MaterialEffect_t3892150953, ___m_EnableUVScroll_8)); }
	inline bool get_m_EnableUVScroll_8() const { return ___m_EnableUVScroll_8; }
	inline bool* get_address_of_m_EnableUVScroll_8() { return &___m_EnableUVScroll_8; }
	inline void set_m_EnableUVScroll_8(bool value)
	{
		___m_EnableUVScroll_8 = value;
	}

	inline static int32_t get_offset_of_m_UVScrollMainTex_9() { return static_cast<int32_t>(offsetof(MaterialEffect_t3892150953, ___m_UVScrollMainTex_9)); }
	inline Vector2_t2156229523  get_m_UVScrollMainTex_9() const { return ___m_UVScrollMainTex_9; }
	inline Vector2_t2156229523 * get_address_of_m_UVScrollMainTex_9() { return &___m_UVScrollMainTex_9; }
	inline void set_m_UVScrollMainTex_9(Vector2_t2156229523  value)
	{
		___m_UVScrollMainTex_9 = value;
	}

	inline static int32_t get_offset_of_m_UVScrollCutTex_10() { return static_cast<int32_t>(offsetof(MaterialEffect_t3892150953, ___m_UVScrollCutTex_10)); }
	inline Vector2_t2156229523  get_m_UVScrollCutTex_10() const { return ___m_UVScrollCutTex_10; }
	inline Vector2_t2156229523 * get_address_of_m_UVScrollCutTex_10() { return &___m_UVScrollCutTex_10; }
	inline void set_m_UVScrollCutTex_10(Vector2_t2156229523  value)
	{
		___m_UVScrollCutTex_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALEFFECT_T3892150953_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef JSON_T1678423215_H
#define JSON_T1678423215_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sdkbox.Json
struct  Json_t1678423215  : public RuntimeObject
{
public:
	// Sdkbox.Json/Type Sdkbox.Json::_type
	int32_t ____type_0;
	// System.Double Sdkbox.Json::_d
	double ____d_1;
	// System.Boolean Sdkbox.Json::_b
	bool ____b_2;
	// System.String Sdkbox.Json::_s
	String_t* ____s_3;
	// System.Collections.Generic.List`1<Sdkbox.Json> Sdkbox.Json::_a
	List_1_t3150497957 * ____a_4;
	// System.Collections.Generic.Dictionary`2<System.String,Sdkbox.Json> Sdkbox.Json::_o
	Dictionary_2_t1463679514 * ____o_5;

public:
	inline static int32_t get_offset_of__type_0() { return static_cast<int32_t>(offsetof(Json_t1678423215, ____type_0)); }
	inline int32_t get__type_0() const { return ____type_0; }
	inline int32_t* get_address_of__type_0() { return &____type_0; }
	inline void set__type_0(int32_t value)
	{
		____type_0 = value;
	}

	inline static int32_t get_offset_of__d_1() { return static_cast<int32_t>(offsetof(Json_t1678423215, ____d_1)); }
	inline double get__d_1() const { return ____d_1; }
	inline double* get_address_of__d_1() { return &____d_1; }
	inline void set__d_1(double value)
	{
		____d_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Json_t1678423215, ____b_2)); }
	inline bool get__b_2() const { return ____b_2; }
	inline bool* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(bool value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__s_3() { return static_cast<int32_t>(offsetof(Json_t1678423215, ____s_3)); }
	inline String_t* get__s_3() const { return ____s_3; }
	inline String_t** get_address_of__s_3() { return &____s_3; }
	inline void set__s_3(String_t* value)
	{
		____s_3 = value;
		Il2CppCodeGenWriteBarrier((&____s_3), value);
	}

	inline static int32_t get_offset_of__a_4() { return static_cast<int32_t>(offsetof(Json_t1678423215, ____a_4)); }
	inline List_1_t3150497957 * get__a_4() const { return ____a_4; }
	inline List_1_t3150497957 ** get_address_of__a_4() { return &____a_4; }
	inline void set__a_4(List_1_t3150497957 * value)
	{
		____a_4 = value;
		Il2CppCodeGenWriteBarrier((&____a_4), value);
	}

	inline static int32_t get_offset_of__o_5() { return static_cast<int32_t>(offsetof(Json_t1678423215, ____o_5)); }
	inline Dictionary_2_t1463679514 * get__o_5() const { return ____o_5; }
	inline Dictionary_2_t1463679514 ** get_address_of__o_5() { return &____o_5; }
	inline void set__o_5(Dictionary_2_t1463679514 * value)
	{
		____o_5 = value;
		Il2CppCodeGenWriteBarrier((&____o_5), value);
	}
};

struct Json_t1678423215_StaticFields
{
public:
	// Sdkbox.Json Sdkbox.Json::null_json
	Json_t1678423215 * ___null_json_6;

public:
	inline static int32_t get_offset_of_null_json_6() { return static_cast<int32_t>(offsetof(Json_t1678423215_StaticFields, ___null_json_6)); }
	inline Json_t1678423215 * get_null_json_6() const { return ___null_json_6; }
	inline Json_t1678423215 ** get_address_of_null_json_6() { return &___null_json_6; }
	inline void set_null_json_6(Json_t1678423215 * value)
	{
		___null_json_6 = value;
		Il2CppCodeGenWriteBarrier((&___null_json_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSON_T1678423215_H
#ifndef PRODUCT_T851415281_H
#define PRODUCT_T851415281_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sdkbox.Product
struct  Product_t851415281 
{
public:
	// System.String Sdkbox.Product::name
	String_t* ___name_0;
	// System.String Sdkbox.Product::id
	String_t* ___id_1;
	// Sdkbox.Product/Type Sdkbox.Product::type
	int32_t ___type_2;
	// System.String Sdkbox.Product::title
	String_t* ___title_3;
	// System.String Sdkbox.Product::description
	String_t* ___description_4;
	// System.Single Sdkbox.Product::priceValue
	float ___priceValue_5;
	// System.String Sdkbox.Product::price
	String_t* ___price_6;
	// System.String Sdkbox.Product::currencyCode
	String_t* ___currencyCode_7;
	// System.String Sdkbox.Product::receiptCipheredPayload
	String_t* ___receiptCipheredPayload_8;
	// System.String Sdkbox.Product::receipt
	String_t* ___receipt_9;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(Product_t851415281, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(Product_t851415281, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(Product_t851415281, ___type_2)); }
	inline int32_t get_type_2() const { return ___type_2; }
	inline int32_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int32_t value)
	{
		___type_2 = value;
	}

	inline static int32_t get_offset_of_title_3() { return static_cast<int32_t>(offsetof(Product_t851415281, ___title_3)); }
	inline String_t* get_title_3() const { return ___title_3; }
	inline String_t** get_address_of_title_3() { return &___title_3; }
	inline void set_title_3(String_t* value)
	{
		___title_3 = value;
		Il2CppCodeGenWriteBarrier((&___title_3), value);
	}

	inline static int32_t get_offset_of_description_4() { return static_cast<int32_t>(offsetof(Product_t851415281, ___description_4)); }
	inline String_t* get_description_4() const { return ___description_4; }
	inline String_t** get_address_of_description_4() { return &___description_4; }
	inline void set_description_4(String_t* value)
	{
		___description_4 = value;
		Il2CppCodeGenWriteBarrier((&___description_4), value);
	}

	inline static int32_t get_offset_of_priceValue_5() { return static_cast<int32_t>(offsetof(Product_t851415281, ___priceValue_5)); }
	inline float get_priceValue_5() const { return ___priceValue_5; }
	inline float* get_address_of_priceValue_5() { return &___priceValue_5; }
	inline void set_priceValue_5(float value)
	{
		___priceValue_5 = value;
	}

	inline static int32_t get_offset_of_price_6() { return static_cast<int32_t>(offsetof(Product_t851415281, ___price_6)); }
	inline String_t* get_price_6() const { return ___price_6; }
	inline String_t** get_address_of_price_6() { return &___price_6; }
	inline void set_price_6(String_t* value)
	{
		___price_6 = value;
		Il2CppCodeGenWriteBarrier((&___price_6), value);
	}

	inline static int32_t get_offset_of_currencyCode_7() { return static_cast<int32_t>(offsetof(Product_t851415281, ___currencyCode_7)); }
	inline String_t* get_currencyCode_7() const { return ___currencyCode_7; }
	inline String_t** get_address_of_currencyCode_7() { return &___currencyCode_7; }
	inline void set_currencyCode_7(String_t* value)
	{
		___currencyCode_7 = value;
		Il2CppCodeGenWriteBarrier((&___currencyCode_7), value);
	}

	inline static int32_t get_offset_of_receiptCipheredPayload_8() { return static_cast<int32_t>(offsetof(Product_t851415281, ___receiptCipheredPayload_8)); }
	inline String_t* get_receiptCipheredPayload_8() const { return ___receiptCipheredPayload_8; }
	inline String_t** get_address_of_receiptCipheredPayload_8() { return &___receiptCipheredPayload_8; }
	inline void set_receiptCipheredPayload_8(String_t* value)
	{
		___receiptCipheredPayload_8 = value;
		Il2CppCodeGenWriteBarrier((&___receiptCipheredPayload_8), value);
	}

	inline static int32_t get_offset_of_receipt_9() { return static_cast<int32_t>(offsetof(Product_t851415281, ___receipt_9)); }
	inline String_t* get_receipt_9() const { return ___receipt_9; }
	inline String_t** get_address_of_receipt_9() { return &___receipt_9; }
	inline void set_receipt_9(String_t* value)
	{
		___receipt_9 = value;
		Il2CppCodeGenWriteBarrier((&___receipt_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Sdkbox.Product
struct Product_t851415281_marshaled_pinvoke
{
	char* ___name_0;
	char* ___id_1;
	int32_t ___type_2;
	char* ___title_3;
	char* ___description_4;
	float ___priceValue_5;
	char* ___price_6;
	char* ___currencyCode_7;
	char* ___receiptCipheredPayload_8;
	char* ___receipt_9;
};
// Native definition for COM marshalling of Sdkbox.Product
struct Product_t851415281_marshaled_com
{
	Il2CppChar* ___name_0;
	Il2CppChar* ___id_1;
	int32_t ___type_2;
	Il2CppChar* ___title_3;
	Il2CppChar* ___description_4;
	float ___priceValue_5;
	Il2CppChar* ___price_6;
	Il2CppChar* ___currencyCode_7;
	Il2CppChar* ___receiptCipheredPayload_8;
	Il2CppChar* ___receipt_9;
};
#endif // PRODUCT_T851415281_H
#ifndef CALLBACKIAPDELEGATE_T957877347_H
#define CALLBACKIAPDELEGATE_T957877347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sdkbox.IAP/CallbackIAPDelegate
struct  CallbackIAPDelegate_t957877347  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKIAPDELEGATE_T957877347_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef SIMPLESCRIPT_T3279312205_H
#define SIMPLESCRIPT_T3279312205_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.SimpleScript
struct  SimpleScript_t3279312205  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TextMeshPro TMPro.Examples.SimpleScript::m_textMeshPro
	TextMeshPro_t2393593166 * ___m_textMeshPro_2;
	// System.Single TMPro.Examples.SimpleScript::m_frame
	float ___m_frame_4;

public:
	inline static int32_t get_offset_of_m_textMeshPro_2() { return static_cast<int32_t>(offsetof(SimpleScript_t3279312205, ___m_textMeshPro_2)); }
	inline TextMeshPro_t2393593166 * get_m_textMeshPro_2() const { return ___m_textMeshPro_2; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_textMeshPro_2() { return &___m_textMeshPro_2; }
	inline void set_m_textMeshPro_2(TextMeshPro_t2393593166 * value)
	{
		___m_textMeshPro_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_2), value);
	}

	inline static int32_t get_offset_of_m_frame_4() { return static_cast<int32_t>(offsetof(SimpleScript_t3279312205, ___m_frame_4)); }
	inline float get_m_frame_4() const { return ___m_frame_4; }
	inline float* get_address_of_m_frame_4() { return &___m_frame_4; }
	inline void set_m_frame_4(float value)
	{
		___m_frame_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLESCRIPT_T3279312205_H
#ifndef TESTSCRIPT_T3771403385_H
#define TESTSCRIPT_T3771403385_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TestScript
struct  TestScript_t3771403385  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<System.Int32> TestScript::sprites
	List_1_t128053199 * ___sprites_2;

public:
	inline static int32_t get_offset_of_sprites_2() { return static_cast<int32_t>(offsetof(TestScript_t3771403385, ___sprites_2)); }
	inline List_1_t128053199 * get_sprites_2() const { return ___sprites_2; }
	inline List_1_t128053199 ** get_address_of_sprites_2() { return &___sprites_2; }
	inline void set_sprites_2(List_1_t128053199 * value)
	{
		___sprites_2 = value;
		Il2CppCodeGenWriteBarrier((&___sprites_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TESTSCRIPT_T3771403385_H
#ifndef SHADERPROPANIMATOR_T3617420994_H
#define SHADERPROPANIMATOR_T3617420994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ShaderPropAnimator
struct  ShaderPropAnimator_t3617420994  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Renderer TMPro.Examples.ShaderPropAnimator::m_Renderer
	Renderer_t2627027031 * ___m_Renderer_2;
	// UnityEngine.Material TMPro.Examples.ShaderPropAnimator::m_Material
	Material_t340375123 * ___m_Material_3;
	// UnityEngine.AnimationCurve TMPro.Examples.ShaderPropAnimator::GlowCurve
	AnimationCurve_t3046754366 * ___GlowCurve_4;
	// System.Single TMPro.Examples.ShaderPropAnimator::m_frame
	float ___m_frame_5;

public:
	inline static int32_t get_offset_of_m_Renderer_2() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___m_Renderer_2)); }
	inline Renderer_t2627027031 * get_m_Renderer_2() const { return ___m_Renderer_2; }
	inline Renderer_t2627027031 ** get_address_of_m_Renderer_2() { return &___m_Renderer_2; }
	inline void set_m_Renderer_2(Renderer_t2627027031 * value)
	{
		___m_Renderer_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Renderer_2), value);
	}

	inline static int32_t get_offset_of_m_Material_3() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___m_Material_3)); }
	inline Material_t340375123 * get_m_Material_3() const { return ___m_Material_3; }
	inline Material_t340375123 ** get_address_of_m_Material_3() { return &___m_Material_3; }
	inline void set_m_Material_3(Material_t340375123 * value)
	{
		___m_Material_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Material_3), value);
	}

	inline static int32_t get_offset_of_GlowCurve_4() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___GlowCurve_4)); }
	inline AnimationCurve_t3046754366 * get_GlowCurve_4() const { return ___GlowCurve_4; }
	inline AnimationCurve_t3046754366 ** get_address_of_GlowCurve_4() { return &___GlowCurve_4; }
	inline void set_GlowCurve_4(AnimationCurve_t3046754366 * value)
	{
		___GlowCurve_4 = value;
		Il2CppCodeGenWriteBarrier((&___GlowCurve_4), value);
	}

	inline static int32_t get_offset_of_m_frame_5() { return static_cast<int32_t>(offsetof(ShaderPropAnimator_t3617420994, ___m_frame_5)); }
	inline float get_m_frame_5() const { return ___m_frame_5; }
	inline float* get_address_of_m_frame_5() { return &___m_frame_5; }
	inline void set_m_frame_5(float value)
	{
		___m_frame_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHADERPROPANIMATOR_T3617420994_H
#ifndef UNITYEDITORPOPUP_T2304967859_H
#define UNITYEDITORPOPUP_T2304967859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bitware.Utils.UnityEditorPopup
struct  UnityEditorPopup_t2304967859  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.UI.Button> Bitware.Utils.UnityEditorPopup::btns
	List_1_t1232139915 * ___btns_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.Text> Bitware.Utils.UnityEditorPopup::texts
	List_1_t3373957456 * ___texts_3;
	// UnityEngine.UI.Text Bitware.Utils.UnityEditorPopup::t
	Text_t1901882714 * ___t_4;
	// UnityEngine.UI.Text Bitware.Utils.UnityEditorPopup::msg
	Text_t1901882714 * ___msg_5;

public:
	inline static int32_t get_offset_of_btns_2() { return static_cast<int32_t>(offsetof(UnityEditorPopup_t2304967859, ___btns_2)); }
	inline List_1_t1232139915 * get_btns_2() const { return ___btns_2; }
	inline List_1_t1232139915 ** get_address_of_btns_2() { return &___btns_2; }
	inline void set_btns_2(List_1_t1232139915 * value)
	{
		___btns_2 = value;
		Il2CppCodeGenWriteBarrier((&___btns_2), value);
	}

	inline static int32_t get_offset_of_texts_3() { return static_cast<int32_t>(offsetof(UnityEditorPopup_t2304967859, ___texts_3)); }
	inline List_1_t3373957456 * get_texts_3() const { return ___texts_3; }
	inline List_1_t3373957456 ** get_address_of_texts_3() { return &___texts_3; }
	inline void set_texts_3(List_1_t3373957456 * value)
	{
		___texts_3 = value;
		Il2CppCodeGenWriteBarrier((&___texts_3), value);
	}

	inline static int32_t get_offset_of_t_4() { return static_cast<int32_t>(offsetof(UnityEditorPopup_t2304967859, ___t_4)); }
	inline Text_t1901882714 * get_t_4() const { return ___t_4; }
	inline Text_t1901882714 ** get_address_of_t_4() { return &___t_4; }
	inline void set_t_4(Text_t1901882714 * value)
	{
		___t_4 = value;
		Il2CppCodeGenWriteBarrier((&___t_4), value);
	}

	inline static int32_t get_offset_of_msg_5() { return static_cast<int32_t>(offsetof(UnityEditorPopup_t2304967859, ___msg_5)); }
	inline Text_t1901882714 * get_msg_5() const { return ___msg_5; }
	inline Text_t1901882714 ** get_address_of_msg_5() { return &___msg_5; }
	inline void set_msg_5(Text_t1901882714 * value)
	{
		___msg_5 = value;
		Il2CppCodeGenWriteBarrier((&___msg_5), value);
	}
};

struct UnityEditorPopup_t2304967859_StaticFields
{
public:
	// Bitware.Utils.UnityEditorPopup Bitware.Utils.UnityEditorPopup::instance
	UnityEditorPopup_t2304967859 * ___instance_6;

public:
	inline static int32_t get_offset_of_instance_6() { return static_cast<int32_t>(offsetof(UnityEditorPopup_t2304967859_StaticFields, ___instance_6)); }
	inline UnityEditorPopup_t2304967859 * get_instance_6() const { return ___instance_6; }
	inline UnityEditorPopup_t2304967859 ** get_address_of_instance_6() { return &___instance_6; }
	inline void set_instance_6(UnityEditorPopup_t2304967859 * value)
	{
		___instance_6 = value;
		Il2CppCodeGenWriteBarrier((&___instance_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYEDITORPOPUP_T2304967859_H
#ifndef CAMERATARGET_T1265245093_H
#define CAMERATARGET_T1265245093_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraTarget
struct  CameraTarget_t1265245093  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform CameraTarget::m_TargetOffset
	Transform_t3600365921 * ___m_TargetOffset_2;

public:
	inline static int32_t get_offset_of_m_TargetOffset_2() { return static_cast<int32_t>(offsetof(CameraTarget_t1265245093, ___m_TargetOffset_2)); }
	inline Transform_t3600365921 * get_m_TargetOffset_2() const { return ___m_TargetOffset_2; }
	inline Transform_t3600365921 ** get_address_of_m_TargetOffset_2() { return &___m_TargetOffset_2; }
	inline void set_m_TargetOffset_2(Transform_t3600365921 * value)
	{
		___m_TargetOffset_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetOffset_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERATARGET_T1265245093_H
#ifndef PLUGINBASE_1_T2351609330_H
#define PLUGINBASE_1_T2351609330_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sdkbox.PluginBase`1<Sdkbox.IAP>
struct  PluginBase_1_t2351609330  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Sdkbox.PluginBase`1::_have_lazy_init
	bool ____have_lazy_init_3;

public:
	inline static int32_t get_offset_of__have_lazy_init_3() { return static_cast<int32_t>(offsetof(PluginBase_1_t2351609330, ____have_lazy_init_3)); }
	inline bool get__have_lazy_init_3() const { return ____have_lazy_init_3; }
	inline bool* get_address_of__have_lazy_init_3() { return &____have_lazy_init_3; }
	inline void set__have_lazy_init_3(bool value)
	{
		____have_lazy_init_3 = value;
	}
};

struct PluginBase_1_t2351609330_StaticFields
{
public:
	// T Sdkbox.PluginBase`1::_this
	IAP_t1939458538 * ____this_2;

public:
	inline static int32_t get_offset_of__this_2() { return static_cast<int32_t>(offsetof(PluginBase_1_t2351609330_StaticFields, ____this_2)); }
	inline IAP_t1939458538 * get__this_2() const { return ____this_2; }
	inline IAP_t1939458538 ** get_address_of__this_2() { return &____this_2; }
	inline void set__this_2(IAP_t1939458538 * value)
	{
		____this_2 = value;
		Il2CppCodeGenWriteBarrier((&____this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLUGINBASE_1_T2351609330_H
#ifndef GENERICPANEL_T306775760_H
#define GENERICPANEL_T306775760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Views.GenericPanel
struct  GenericPanel_t306775760  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GENERICPANEL_T306775760_H
#ifndef IABWRAPPER_T882725550_H
#define IABWRAPPER_T882725550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bitware.Wrappers.IABWrapper
struct  IABWrapper_t882725550  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct IABWrapper_t882725550_StaticFields
{
public:
	// Bitware.Wrappers.IABWrapper Bitware.Wrappers.IABWrapper::instance
	IABWrapper_t882725550 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(IABWrapper_t882725550_StaticFields, ___instance_2)); }
	inline IABWrapper_t882725550 * get_instance_2() const { return ___instance_2; }
	inline IABWrapper_t882725550 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(IABWrapper_t882725550 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IABWRAPPER_T882725550_H
#ifndef SPRITEMANAGER_T361410013_H
#define SPRITEMANAGER_T361410013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bitware.Wrappers.SpriteManager
struct  SpriteManager_t361410013  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<Bitware.Wrappers.NamedSprite> Bitware.Wrappers.SpriteManager::currencySprites
	List_1_t4170927959 * ___currencySprites_4;
	// UnityEngine.Sprite Bitware.Wrappers.SpriteManager::selectedtab
	Sprite_t280657092 * ___selectedtab_5;
	// UnityEngine.Sprite Bitware.Wrappers.SpriteManager::nonselectedtab
	Sprite_t280657092 * ___nonselectedtab_6;
	// UnityEngine.Sprite Bitware.Wrappers.SpriteManager::spriteCoin1
	Sprite_t280657092 * ___spriteCoin1_7;
	// UnityEngine.Sprite Bitware.Wrappers.SpriteManager::spriteCoin2
	Sprite_t280657092 * ___spriteCoin2_8;
	// UnityEngine.Sprite Bitware.Wrappers.SpriteManager::spriteCoin5
	Sprite_t280657092 * ___spriteCoin5_9;
	// UnityEngine.Sprite Bitware.Wrappers.SpriteManager::spriteCoin10
	Sprite_t280657092 * ___spriteCoin10_10;
	// UnityEngine.Sprite Bitware.Wrappers.SpriteManager::cardBackSpriteRed
	Sprite_t280657092 * ___cardBackSpriteRed_11;
	// UnityEngine.Sprite Bitware.Wrappers.SpriteManager::cardBackSpriteBlue
	Sprite_t280657092 * ___cardBackSpriteBlue_12;
	// UnityEngine.Sprite Bitware.Wrappers.SpriteManager::jokerSprite
	Sprite_t280657092 * ___jokerSprite_13;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Sprite> Bitware.Wrappers.SpriteManager::cardImages
	Dictionary_2_t65913391 * ___cardImages_14;
	// UnityEngine.Sprite Bitware.Wrappers.SpriteManager::connectingSprite
	Sprite_t280657092 * ___connectingSprite_15;
	// UnityEngine.Sprite Bitware.Wrappers.SpriteManager::connectionLostSprite
	Sprite_t280657092 * ___connectionLostSprite_16;
	// System.Collections.Generic.List`1<Bitware.Wrappers.NamedSprite> Bitware.Wrappers.SpriteManager::namedSprites
	List_1_t4170927959 * ___namedSprites_17;

public:
	inline static int32_t get_offset_of_currencySprites_4() { return static_cast<int32_t>(offsetof(SpriteManager_t361410013, ___currencySprites_4)); }
	inline List_1_t4170927959 * get_currencySprites_4() const { return ___currencySprites_4; }
	inline List_1_t4170927959 ** get_address_of_currencySprites_4() { return &___currencySprites_4; }
	inline void set_currencySprites_4(List_1_t4170927959 * value)
	{
		___currencySprites_4 = value;
		Il2CppCodeGenWriteBarrier((&___currencySprites_4), value);
	}

	inline static int32_t get_offset_of_selectedtab_5() { return static_cast<int32_t>(offsetof(SpriteManager_t361410013, ___selectedtab_5)); }
	inline Sprite_t280657092 * get_selectedtab_5() const { return ___selectedtab_5; }
	inline Sprite_t280657092 ** get_address_of_selectedtab_5() { return &___selectedtab_5; }
	inline void set_selectedtab_5(Sprite_t280657092 * value)
	{
		___selectedtab_5 = value;
		Il2CppCodeGenWriteBarrier((&___selectedtab_5), value);
	}

	inline static int32_t get_offset_of_nonselectedtab_6() { return static_cast<int32_t>(offsetof(SpriteManager_t361410013, ___nonselectedtab_6)); }
	inline Sprite_t280657092 * get_nonselectedtab_6() const { return ___nonselectedtab_6; }
	inline Sprite_t280657092 ** get_address_of_nonselectedtab_6() { return &___nonselectedtab_6; }
	inline void set_nonselectedtab_6(Sprite_t280657092 * value)
	{
		___nonselectedtab_6 = value;
		Il2CppCodeGenWriteBarrier((&___nonselectedtab_6), value);
	}

	inline static int32_t get_offset_of_spriteCoin1_7() { return static_cast<int32_t>(offsetof(SpriteManager_t361410013, ___spriteCoin1_7)); }
	inline Sprite_t280657092 * get_spriteCoin1_7() const { return ___spriteCoin1_7; }
	inline Sprite_t280657092 ** get_address_of_spriteCoin1_7() { return &___spriteCoin1_7; }
	inline void set_spriteCoin1_7(Sprite_t280657092 * value)
	{
		___spriteCoin1_7 = value;
		Il2CppCodeGenWriteBarrier((&___spriteCoin1_7), value);
	}

	inline static int32_t get_offset_of_spriteCoin2_8() { return static_cast<int32_t>(offsetof(SpriteManager_t361410013, ___spriteCoin2_8)); }
	inline Sprite_t280657092 * get_spriteCoin2_8() const { return ___spriteCoin2_8; }
	inline Sprite_t280657092 ** get_address_of_spriteCoin2_8() { return &___spriteCoin2_8; }
	inline void set_spriteCoin2_8(Sprite_t280657092 * value)
	{
		___spriteCoin2_8 = value;
		Il2CppCodeGenWriteBarrier((&___spriteCoin2_8), value);
	}

	inline static int32_t get_offset_of_spriteCoin5_9() { return static_cast<int32_t>(offsetof(SpriteManager_t361410013, ___spriteCoin5_9)); }
	inline Sprite_t280657092 * get_spriteCoin5_9() const { return ___spriteCoin5_9; }
	inline Sprite_t280657092 ** get_address_of_spriteCoin5_9() { return &___spriteCoin5_9; }
	inline void set_spriteCoin5_9(Sprite_t280657092 * value)
	{
		___spriteCoin5_9 = value;
		Il2CppCodeGenWriteBarrier((&___spriteCoin5_9), value);
	}

	inline static int32_t get_offset_of_spriteCoin10_10() { return static_cast<int32_t>(offsetof(SpriteManager_t361410013, ___spriteCoin10_10)); }
	inline Sprite_t280657092 * get_spriteCoin10_10() const { return ___spriteCoin10_10; }
	inline Sprite_t280657092 ** get_address_of_spriteCoin10_10() { return &___spriteCoin10_10; }
	inline void set_spriteCoin10_10(Sprite_t280657092 * value)
	{
		___spriteCoin10_10 = value;
		Il2CppCodeGenWriteBarrier((&___spriteCoin10_10), value);
	}

	inline static int32_t get_offset_of_cardBackSpriteRed_11() { return static_cast<int32_t>(offsetof(SpriteManager_t361410013, ___cardBackSpriteRed_11)); }
	inline Sprite_t280657092 * get_cardBackSpriteRed_11() const { return ___cardBackSpriteRed_11; }
	inline Sprite_t280657092 ** get_address_of_cardBackSpriteRed_11() { return &___cardBackSpriteRed_11; }
	inline void set_cardBackSpriteRed_11(Sprite_t280657092 * value)
	{
		___cardBackSpriteRed_11 = value;
		Il2CppCodeGenWriteBarrier((&___cardBackSpriteRed_11), value);
	}

	inline static int32_t get_offset_of_cardBackSpriteBlue_12() { return static_cast<int32_t>(offsetof(SpriteManager_t361410013, ___cardBackSpriteBlue_12)); }
	inline Sprite_t280657092 * get_cardBackSpriteBlue_12() const { return ___cardBackSpriteBlue_12; }
	inline Sprite_t280657092 ** get_address_of_cardBackSpriteBlue_12() { return &___cardBackSpriteBlue_12; }
	inline void set_cardBackSpriteBlue_12(Sprite_t280657092 * value)
	{
		___cardBackSpriteBlue_12 = value;
		Il2CppCodeGenWriteBarrier((&___cardBackSpriteBlue_12), value);
	}

	inline static int32_t get_offset_of_jokerSprite_13() { return static_cast<int32_t>(offsetof(SpriteManager_t361410013, ___jokerSprite_13)); }
	inline Sprite_t280657092 * get_jokerSprite_13() const { return ___jokerSprite_13; }
	inline Sprite_t280657092 ** get_address_of_jokerSprite_13() { return &___jokerSprite_13; }
	inline void set_jokerSprite_13(Sprite_t280657092 * value)
	{
		___jokerSprite_13 = value;
		Il2CppCodeGenWriteBarrier((&___jokerSprite_13), value);
	}

	inline static int32_t get_offset_of_cardImages_14() { return static_cast<int32_t>(offsetof(SpriteManager_t361410013, ___cardImages_14)); }
	inline Dictionary_2_t65913391 * get_cardImages_14() const { return ___cardImages_14; }
	inline Dictionary_2_t65913391 ** get_address_of_cardImages_14() { return &___cardImages_14; }
	inline void set_cardImages_14(Dictionary_2_t65913391 * value)
	{
		___cardImages_14 = value;
		Il2CppCodeGenWriteBarrier((&___cardImages_14), value);
	}

	inline static int32_t get_offset_of_connectingSprite_15() { return static_cast<int32_t>(offsetof(SpriteManager_t361410013, ___connectingSprite_15)); }
	inline Sprite_t280657092 * get_connectingSprite_15() const { return ___connectingSprite_15; }
	inline Sprite_t280657092 ** get_address_of_connectingSprite_15() { return &___connectingSprite_15; }
	inline void set_connectingSprite_15(Sprite_t280657092 * value)
	{
		___connectingSprite_15 = value;
		Il2CppCodeGenWriteBarrier((&___connectingSprite_15), value);
	}

	inline static int32_t get_offset_of_connectionLostSprite_16() { return static_cast<int32_t>(offsetof(SpriteManager_t361410013, ___connectionLostSprite_16)); }
	inline Sprite_t280657092 * get_connectionLostSprite_16() const { return ___connectionLostSprite_16; }
	inline Sprite_t280657092 ** get_address_of_connectionLostSprite_16() { return &___connectionLostSprite_16; }
	inline void set_connectionLostSprite_16(Sprite_t280657092 * value)
	{
		___connectionLostSprite_16 = value;
		Il2CppCodeGenWriteBarrier((&___connectionLostSprite_16), value);
	}

	inline static int32_t get_offset_of_namedSprites_17() { return static_cast<int32_t>(offsetof(SpriteManager_t361410013, ___namedSprites_17)); }
	inline List_1_t4170927959 * get_namedSprites_17() const { return ___namedSprites_17; }
	inline List_1_t4170927959 ** get_address_of_namedSprites_17() { return &___namedSprites_17; }
	inline void set_namedSprites_17(List_1_t4170927959 * value)
	{
		___namedSprites_17 = value;
		Il2CppCodeGenWriteBarrier((&___namedSprites_17), value);
	}
};

struct SpriteManager_t361410013_StaticFields
{
public:
	// Bitware.Wrappers.SpriteManager Bitware.Wrappers.SpriteManager::instance
	SpriteManager_t361410013 * ___instance_2;
	// UnityEngine.Sprite Bitware.Wrappers.SpriteManager::_transparentImage
	Sprite_t280657092 * ____transparentImage_3;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(SpriteManager_t361410013_StaticFields, ___instance_2)); }
	inline SpriteManager_t361410013 * get_instance_2() const { return ___instance_2; }
	inline SpriteManager_t361410013 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(SpriteManager_t361410013 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of__transparentImage_3() { return static_cast<int32_t>(offsetof(SpriteManager_t361410013_StaticFields, ____transparentImage_3)); }
	inline Sprite_t280657092 * get__transparentImage_3() const { return ____transparentImage_3; }
	inline Sprite_t280657092 ** get_address_of__transparentImage_3() { return &____transparentImage_3; }
	inline void set__transparentImage_3(Sprite_t280657092 * value)
	{
		____transparentImage_3 = value;
		Il2CppCodeGenWriteBarrier((&____transparentImage_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPRITEMANAGER_T361410013_H
#ifndef BASICS_T653963782_H
#define BASICS_T653963782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Basics
struct  Basics_t653963782  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform Basics::cubeA
	Transform_t3600365921 * ___cubeA_2;
	// UnityEngine.Transform Basics::cubeB
	Transform_t3600365921 * ___cubeB_3;

public:
	inline static int32_t get_offset_of_cubeA_2() { return static_cast<int32_t>(offsetof(Basics_t653963782, ___cubeA_2)); }
	inline Transform_t3600365921 * get_cubeA_2() const { return ___cubeA_2; }
	inline Transform_t3600365921 ** get_address_of_cubeA_2() { return &___cubeA_2; }
	inline void set_cubeA_2(Transform_t3600365921 * value)
	{
		___cubeA_2 = value;
		Il2CppCodeGenWriteBarrier((&___cubeA_2), value);
	}

	inline static int32_t get_offset_of_cubeB_3() { return static_cast<int32_t>(offsetof(Basics_t653963782, ___cubeB_3)); }
	inline Transform_t3600365921 * get_cubeB_3() const { return ___cubeB_3; }
	inline Transform_t3600365921 ** get_address_of_cubeB_3() { return &___cubeB_3; }
	inline void set_cubeB_3(Transform_t3600365921 * value)
	{
		___cubeB_3 = value;
		Il2CppCodeGenWriteBarrier((&___cubeB_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASICS_T653963782_H
#ifndef SEQUENCES_T3991711780_H
#define SEQUENCES_T3991711780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sequences
struct  Sequences_t3991711780  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform Sequences::target
	Transform_t3600365921 * ___target_2;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(Sequences_t3991711780, ___target_2)); }
	inline Transform_t3600365921 * get_target_2() const { return ___target_2; }
	inline Transform_t3600365921 ** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(Transform_t3600365921 * value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEQUENCES_T3991711780_H
#ifndef CONSOLEBASE_T1023975560_H
#define CONSOLEBASE_T1023975560_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.ConsoleBase
struct  ConsoleBase_t1023975560  : public MonoBehaviour_t3962482529
{
public:
	// System.String Facebook.Unity.Example.ConsoleBase::status
	String_t* ___status_4;
	// System.String Facebook.Unity.Example.ConsoleBase::lastResponse
	String_t* ___lastResponse_5;
	// UnityEngine.Vector2 Facebook.Unity.Example.ConsoleBase::scrollPosition
	Vector2_t2156229523  ___scrollPosition_6;
	// System.Nullable`1<System.Single> Facebook.Unity.Example.ConsoleBase::scaleFactor
	Nullable_1_t3119828856  ___scaleFactor_7;
	// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::textStyle
	GUIStyle_t3956901511 * ___textStyle_8;
	// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::buttonStyle
	GUIStyle_t3956901511 * ___buttonStyle_9;
	// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::textInputStyle
	GUIStyle_t3956901511 * ___textInputStyle_10;
	// UnityEngine.GUIStyle Facebook.Unity.Example.ConsoleBase::labelStyle
	GUIStyle_t3956901511 * ___labelStyle_11;
	// UnityEngine.Texture2D Facebook.Unity.Example.ConsoleBase::<LastResponseTexture>k__BackingField
	Texture2D_t3840446185 * ___U3CLastResponseTextureU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_status_4() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___status_4)); }
	inline String_t* get_status_4() const { return ___status_4; }
	inline String_t** get_address_of_status_4() { return &___status_4; }
	inline void set_status_4(String_t* value)
	{
		___status_4 = value;
		Il2CppCodeGenWriteBarrier((&___status_4), value);
	}

	inline static int32_t get_offset_of_lastResponse_5() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___lastResponse_5)); }
	inline String_t* get_lastResponse_5() const { return ___lastResponse_5; }
	inline String_t** get_address_of_lastResponse_5() { return &___lastResponse_5; }
	inline void set_lastResponse_5(String_t* value)
	{
		___lastResponse_5 = value;
		Il2CppCodeGenWriteBarrier((&___lastResponse_5), value);
	}

	inline static int32_t get_offset_of_scrollPosition_6() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___scrollPosition_6)); }
	inline Vector2_t2156229523  get_scrollPosition_6() const { return ___scrollPosition_6; }
	inline Vector2_t2156229523 * get_address_of_scrollPosition_6() { return &___scrollPosition_6; }
	inline void set_scrollPosition_6(Vector2_t2156229523  value)
	{
		___scrollPosition_6 = value;
	}

	inline static int32_t get_offset_of_scaleFactor_7() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___scaleFactor_7)); }
	inline Nullable_1_t3119828856  get_scaleFactor_7() const { return ___scaleFactor_7; }
	inline Nullable_1_t3119828856 * get_address_of_scaleFactor_7() { return &___scaleFactor_7; }
	inline void set_scaleFactor_7(Nullable_1_t3119828856  value)
	{
		___scaleFactor_7 = value;
	}

	inline static int32_t get_offset_of_textStyle_8() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___textStyle_8)); }
	inline GUIStyle_t3956901511 * get_textStyle_8() const { return ___textStyle_8; }
	inline GUIStyle_t3956901511 ** get_address_of_textStyle_8() { return &___textStyle_8; }
	inline void set_textStyle_8(GUIStyle_t3956901511 * value)
	{
		___textStyle_8 = value;
		Il2CppCodeGenWriteBarrier((&___textStyle_8), value);
	}

	inline static int32_t get_offset_of_buttonStyle_9() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___buttonStyle_9)); }
	inline GUIStyle_t3956901511 * get_buttonStyle_9() const { return ___buttonStyle_9; }
	inline GUIStyle_t3956901511 ** get_address_of_buttonStyle_9() { return &___buttonStyle_9; }
	inline void set_buttonStyle_9(GUIStyle_t3956901511 * value)
	{
		___buttonStyle_9 = value;
		Il2CppCodeGenWriteBarrier((&___buttonStyle_9), value);
	}

	inline static int32_t get_offset_of_textInputStyle_10() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___textInputStyle_10)); }
	inline GUIStyle_t3956901511 * get_textInputStyle_10() const { return ___textInputStyle_10; }
	inline GUIStyle_t3956901511 ** get_address_of_textInputStyle_10() { return &___textInputStyle_10; }
	inline void set_textInputStyle_10(GUIStyle_t3956901511 * value)
	{
		___textInputStyle_10 = value;
		Il2CppCodeGenWriteBarrier((&___textInputStyle_10), value);
	}

	inline static int32_t get_offset_of_labelStyle_11() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___labelStyle_11)); }
	inline GUIStyle_t3956901511 * get_labelStyle_11() const { return ___labelStyle_11; }
	inline GUIStyle_t3956901511 ** get_address_of_labelStyle_11() { return &___labelStyle_11; }
	inline void set_labelStyle_11(GUIStyle_t3956901511 * value)
	{
		___labelStyle_11 = value;
		Il2CppCodeGenWriteBarrier((&___labelStyle_11), value);
	}

	inline static int32_t get_offset_of_U3CLastResponseTextureU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560, ___U3CLastResponseTextureU3Ek__BackingField_12)); }
	inline Texture2D_t3840446185 * get_U3CLastResponseTextureU3Ek__BackingField_12() const { return ___U3CLastResponseTextureU3Ek__BackingField_12; }
	inline Texture2D_t3840446185 ** get_address_of_U3CLastResponseTextureU3Ek__BackingField_12() { return &___U3CLastResponseTextureU3Ek__BackingField_12; }
	inline void set_U3CLastResponseTextureU3Ek__BackingField_12(Texture2D_t3840446185 * value)
	{
		___U3CLastResponseTextureU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLastResponseTextureU3Ek__BackingField_12), value);
	}
};

struct ConsoleBase_t1023975560_StaticFields
{
public:
	// System.Collections.Generic.Stack`1<System.String> Facebook.Unity.Example.ConsoleBase::menuStack
	Stack_1_t2690840144 * ___menuStack_3;

public:
	inline static int32_t get_offset_of_menuStack_3() { return static_cast<int32_t>(offsetof(ConsoleBase_t1023975560_StaticFields, ___menuStack_3)); }
	inline Stack_1_t2690840144 * get_menuStack_3() const { return ___menuStack_3; }
	inline Stack_1_t2690840144 ** get_address_of_menuStack_3() { return &___menuStack_3; }
	inline void set_menuStack_3(Stack_1_t2690840144 * value)
	{
		___menuStack_3 = value;
		Il2CppCodeGenWriteBarrier((&___menuStack_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSOLEBASE_T1023975560_H
#ifndef OBJECTSPIN_T341713598_H
#define OBJECTSPIN_T341713598_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.ObjectSpin
struct  ObjectSpin_t341713598  : public MonoBehaviour_t3962482529
{
public:
	// System.Single TMPro.Examples.ObjectSpin::SpinSpeed
	float ___SpinSpeed_2;
	// System.Int32 TMPro.Examples.ObjectSpin::RotationRange
	int32_t ___RotationRange_3;
	// UnityEngine.Transform TMPro.Examples.ObjectSpin::m_transform
	Transform_t3600365921 * ___m_transform_4;
	// System.Single TMPro.Examples.ObjectSpin::m_time
	float ___m_time_5;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_prevPOS
	Vector3_t3722313464  ___m_prevPOS_6;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_initial_Rotation
	Vector3_t3722313464  ___m_initial_Rotation_7;
	// UnityEngine.Vector3 TMPro.Examples.ObjectSpin::m_initial_Position
	Vector3_t3722313464  ___m_initial_Position_8;
	// UnityEngine.Color32 TMPro.Examples.ObjectSpin::m_lightColor
	Color32_t2600501292  ___m_lightColor_9;
	// System.Int32 TMPro.Examples.ObjectSpin::frames
	int32_t ___frames_10;
	// TMPro.Examples.ObjectSpin/MotionType TMPro.Examples.ObjectSpin::Motion
	int32_t ___Motion_11;

public:
	inline static int32_t get_offset_of_SpinSpeed_2() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___SpinSpeed_2)); }
	inline float get_SpinSpeed_2() const { return ___SpinSpeed_2; }
	inline float* get_address_of_SpinSpeed_2() { return &___SpinSpeed_2; }
	inline void set_SpinSpeed_2(float value)
	{
		___SpinSpeed_2 = value;
	}

	inline static int32_t get_offset_of_RotationRange_3() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___RotationRange_3)); }
	inline int32_t get_RotationRange_3() const { return ___RotationRange_3; }
	inline int32_t* get_address_of_RotationRange_3() { return &___RotationRange_3; }
	inline void set_RotationRange_3(int32_t value)
	{
		___RotationRange_3 = value;
	}

	inline static int32_t get_offset_of_m_transform_4() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_transform_4)); }
	inline Transform_t3600365921 * get_m_transform_4() const { return ___m_transform_4; }
	inline Transform_t3600365921 ** get_address_of_m_transform_4() { return &___m_transform_4; }
	inline void set_m_transform_4(Transform_t3600365921 * value)
	{
		___m_transform_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_transform_4), value);
	}

	inline static int32_t get_offset_of_m_time_5() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_time_5)); }
	inline float get_m_time_5() const { return ___m_time_5; }
	inline float* get_address_of_m_time_5() { return &___m_time_5; }
	inline void set_m_time_5(float value)
	{
		___m_time_5 = value;
	}

	inline static int32_t get_offset_of_m_prevPOS_6() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_prevPOS_6)); }
	inline Vector3_t3722313464  get_m_prevPOS_6() const { return ___m_prevPOS_6; }
	inline Vector3_t3722313464 * get_address_of_m_prevPOS_6() { return &___m_prevPOS_6; }
	inline void set_m_prevPOS_6(Vector3_t3722313464  value)
	{
		___m_prevPOS_6 = value;
	}

	inline static int32_t get_offset_of_m_initial_Rotation_7() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_initial_Rotation_7)); }
	inline Vector3_t3722313464  get_m_initial_Rotation_7() const { return ___m_initial_Rotation_7; }
	inline Vector3_t3722313464 * get_address_of_m_initial_Rotation_7() { return &___m_initial_Rotation_7; }
	inline void set_m_initial_Rotation_7(Vector3_t3722313464  value)
	{
		___m_initial_Rotation_7 = value;
	}

	inline static int32_t get_offset_of_m_initial_Position_8() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_initial_Position_8)); }
	inline Vector3_t3722313464  get_m_initial_Position_8() const { return ___m_initial_Position_8; }
	inline Vector3_t3722313464 * get_address_of_m_initial_Position_8() { return &___m_initial_Position_8; }
	inline void set_m_initial_Position_8(Vector3_t3722313464  value)
	{
		___m_initial_Position_8 = value;
	}

	inline static int32_t get_offset_of_m_lightColor_9() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___m_lightColor_9)); }
	inline Color32_t2600501292  get_m_lightColor_9() const { return ___m_lightColor_9; }
	inline Color32_t2600501292 * get_address_of_m_lightColor_9() { return &___m_lightColor_9; }
	inline void set_m_lightColor_9(Color32_t2600501292  value)
	{
		___m_lightColor_9 = value;
	}

	inline static int32_t get_offset_of_frames_10() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___frames_10)); }
	inline int32_t get_frames_10() const { return ___frames_10; }
	inline int32_t* get_address_of_frames_10() { return &___frames_10; }
	inline void set_frames_10(int32_t value)
	{
		___frames_10 = value;
	}

	inline static int32_t get_offset_of_Motion_11() { return static_cast<int32_t>(offsetof(ObjectSpin_t341713598, ___Motion_11)); }
	inline int32_t get_Motion_11() const { return ___Motion_11; }
	inline int32_t* get_address_of_Motion_11() { return &___Motion_11; }
	inline void set_Motion_11(int32_t value)
	{
		___Motion_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTSPIN_T341713598_H
#ifndef BENCHMARK01_T1571072624_H
#define BENCHMARK01_T1571072624_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01
struct  Benchmark01_t1571072624  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark01::BenchmarkType
	int32_t ___BenchmarkType_2;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01::TMProFont
	TMP_FontAsset_t364381626 * ___TMProFont_3;
	// UnityEngine.Font TMPro.Examples.Benchmark01::TextMeshFont
	Font_t1956802104 * ___TextMeshFont_4;
	// TMPro.TextMeshPro TMPro.Examples.Benchmark01::m_textMeshPro
	TextMeshPro_t2393593166 * ___m_textMeshPro_5;
	// TMPro.TextContainer TMPro.Examples.Benchmark01::m_textContainer
	TextContainer_t97923372 * ___m_textContainer_6;
	// UnityEngine.TextMesh TMPro.Examples.Benchmark01::m_textMesh
	TextMesh_t1536577757 * ___m_textMesh_7;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material01
	Material_t340375123 * ___m_material01_10;
	// UnityEngine.Material TMPro.Examples.Benchmark01::m_material02
	Material_t340375123 * ___m_material02_11;

public:
	inline static int32_t get_offset_of_BenchmarkType_2() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___BenchmarkType_2)); }
	inline int32_t get_BenchmarkType_2() const { return ___BenchmarkType_2; }
	inline int32_t* get_address_of_BenchmarkType_2() { return &___BenchmarkType_2; }
	inline void set_BenchmarkType_2(int32_t value)
	{
		___BenchmarkType_2 = value;
	}

	inline static int32_t get_offset_of_TMProFont_3() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___TMProFont_3)); }
	inline TMP_FontAsset_t364381626 * get_TMProFont_3() const { return ___TMProFont_3; }
	inline TMP_FontAsset_t364381626 ** get_address_of_TMProFont_3() { return &___TMProFont_3; }
	inline void set_TMProFont_3(TMP_FontAsset_t364381626 * value)
	{
		___TMProFont_3 = value;
		Il2CppCodeGenWriteBarrier((&___TMProFont_3), value);
	}

	inline static int32_t get_offset_of_TextMeshFont_4() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___TextMeshFont_4)); }
	inline Font_t1956802104 * get_TextMeshFont_4() const { return ___TextMeshFont_4; }
	inline Font_t1956802104 ** get_address_of_TextMeshFont_4() { return &___TextMeshFont_4; }
	inline void set_TextMeshFont_4(Font_t1956802104 * value)
	{
		___TextMeshFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshFont_4), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_5() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_textMeshPro_5)); }
	inline TextMeshPro_t2393593166 * get_m_textMeshPro_5() const { return ___m_textMeshPro_5; }
	inline TextMeshPro_t2393593166 ** get_address_of_m_textMeshPro_5() { return &___m_textMeshPro_5; }
	inline void set_m_textMeshPro_5(TextMeshPro_t2393593166 * value)
	{
		___m_textMeshPro_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_5), value);
	}

	inline static int32_t get_offset_of_m_textContainer_6() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_textContainer_6)); }
	inline TextContainer_t97923372 * get_m_textContainer_6() const { return ___m_textContainer_6; }
	inline TextContainer_t97923372 ** get_address_of_m_textContainer_6() { return &___m_textContainer_6; }
	inline void set_m_textContainer_6(TextContainer_t97923372 * value)
	{
		___m_textContainer_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_textContainer_6), value);
	}

	inline static int32_t get_offset_of_m_textMesh_7() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_textMesh_7)); }
	inline TextMesh_t1536577757 * get_m_textMesh_7() const { return ___m_textMesh_7; }
	inline TextMesh_t1536577757 ** get_address_of_m_textMesh_7() { return &___m_textMesh_7; }
	inline void set_m_textMesh_7(TextMesh_t1536577757 * value)
	{
		___m_textMesh_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_7), value);
	}

	inline static int32_t get_offset_of_m_material01_10() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_material01_10)); }
	inline Material_t340375123 * get_m_material01_10() const { return ___m_material01_10; }
	inline Material_t340375123 ** get_address_of_m_material01_10() { return &___m_material01_10; }
	inline void set_m_material01_10(Material_t340375123 * value)
	{
		___m_material01_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_material01_10), value);
	}

	inline static int32_t get_offset_of_m_material02_11() { return static_cast<int32_t>(offsetof(Benchmark01_t1571072624, ___m_material02_11)); }
	inline Material_t340375123 * get_m_material02_11() const { return ___m_material02_11; }
	inline Material_t340375123 ** get_address_of_m_material02_11() { return &___m_material02_11; }
	inline void set_m_material02_11(Material_t340375123 * value)
	{
		___m_material02_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_material02_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK01_T1571072624_H
#ifndef BENCHMARK01_UGUI_T3264177817_H
#define BENCHMARK01_UGUI_T3264177817_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark01_UGUI
struct  Benchmark01_UGUI_t3264177817  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark01_UGUI::BenchmarkType
	int32_t ___BenchmarkType_2;
	// UnityEngine.Canvas TMPro.Examples.Benchmark01_UGUI::canvas
	Canvas_t3310196443 * ___canvas_3;
	// TMPro.TMP_FontAsset TMPro.Examples.Benchmark01_UGUI::TMProFont
	TMP_FontAsset_t364381626 * ___TMProFont_4;
	// UnityEngine.Font TMPro.Examples.Benchmark01_UGUI::TextMeshFont
	Font_t1956802104 * ___TextMeshFont_5;
	// TMPro.TextMeshProUGUI TMPro.Examples.Benchmark01_UGUI::m_textMeshPro
	TextMeshProUGUI_t529313277 * ___m_textMeshPro_6;
	// UnityEngine.UI.Text TMPro.Examples.Benchmark01_UGUI::m_textMesh
	Text_t1901882714 * ___m_textMesh_7;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material01
	Material_t340375123 * ___m_material01_10;
	// UnityEngine.Material TMPro.Examples.Benchmark01_UGUI::m_material02
	Material_t340375123 * ___m_material02_11;

public:
	inline static int32_t get_offset_of_BenchmarkType_2() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___BenchmarkType_2)); }
	inline int32_t get_BenchmarkType_2() const { return ___BenchmarkType_2; }
	inline int32_t* get_address_of_BenchmarkType_2() { return &___BenchmarkType_2; }
	inline void set_BenchmarkType_2(int32_t value)
	{
		___BenchmarkType_2 = value;
	}

	inline static int32_t get_offset_of_canvas_3() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___canvas_3)); }
	inline Canvas_t3310196443 * get_canvas_3() const { return ___canvas_3; }
	inline Canvas_t3310196443 ** get_address_of_canvas_3() { return &___canvas_3; }
	inline void set_canvas_3(Canvas_t3310196443 * value)
	{
		___canvas_3 = value;
		Il2CppCodeGenWriteBarrier((&___canvas_3), value);
	}

	inline static int32_t get_offset_of_TMProFont_4() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___TMProFont_4)); }
	inline TMP_FontAsset_t364381626 * get_TMProFont_4() const { return ___TMProFont_4; }
	inline TMP_FontAsset_t364381626 ** get_address_of_TMProFont_4() { return &___TMProFont_4; }
	inline void set_TMProFont_4(TMP_FontAsset_t364381626 * value)
	{
		___TMProFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TMProFont_4), value);
	}

	inline static int32_t get_offset_of_TextMeshFont_5() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___TextMeshFont_5)); }
	inline Font_t1956802104 * get_TextMeshFont_5() const { return ___TextMeshFont_5; }
	inline Font_t1956802104 ** get_address_of_TextMeshFont_5() { return &___TextMeshFont_5; }
	inline void set_TextMeshFont_5(Font_t1956802104 * value)
	{
		___TextMeshFont_5 = value;
		Il2CppCodeGenWriteBarrier((&___TextMeshFont_5), value);
	}

	inline static int32_t get_offset_of_m_textMeshPro_6() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_textMeshPro_6)); }
	inline TextMeshProUGUI_t529313277 * get_m_textMeshPro_6() const { return ___m_textMeshPro_6; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_m_textMeshPro_6() { return &___m_textMeshPro_6; }
	inline void set_m_textMeshPro_6(TextMeshProUGUI_t529313277 * value)
	{
		___m_textMeshPro_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_6), value);
	}

	inline static int32_t get_offset_of_m_textMesh_7() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_textMesh_7)); }
	inline Text_t1901882714 * get_m_textMesh_7() const { return ___m_textMesh_7; }
	inline Text_t1901882714 ** get_address_of_m_textMesh_7() { return &___m_textMesh_7; }
	inline void set_m_textMesh_7(Text_t1901882714 * value)
	{
		___m_textMesh_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMesh_7), value);
	}

	inline static int32_t get_offset_of_m_material01_10() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_material01_10)); }
	inline Material_t340375123 * get_m_material01_10() const { return ___m_material01_10; }
	inline Material_t340375123 ** get_address_of_m_material01_10() { return &___m_material01_10; }
	inline void set_m_material01_10(Material_t340375123 * value)
	{
		___m_material01_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_material01_10), value);
	}

	inline static int32_t get_offset_of_m_material02_11() { return static_cast<int32_t>(offsetof(Benchmark01_UGUI_t3264177817, ___m_material02_11)); }
	inline Material_t340375123 * get_m_material02_11() const { return ___m_material02_11; }
	inline Material_t340375123 ** get_address_of_m_material02_11() { return &___m_material02_11; }
	inline void set_m_material02_11(Material_t340375123 * value)
	{
		___m_material02_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_material02_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK01_UGUI_T3264177817_H
#ifndef EFFECTDEMO_T2474128590_H
#define EFFECTDEMO_T2474128590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectDemo
struct  EffectDemo_t2474128590  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> EffectDemo::m_EffectPrefabList
	List_1_t2585711361 * ___m_EffectPrefabList_3;
	// System.Boolean EffectDemo::m_LookAtEffect
	bool ___m_LookAtEffect_4;
	// UnityEngine.GameObject EffectDemo::m_NowShowEffect
	GameObject_t1113636619 * ___m_NowShowEffect_5;
	// System.Int32 EffectDemo::m_NowIndex
	int32_t ___m_NowIndex_6;
	// System.String EffectDemo::m_NowEffectName
	String_t* ___m_NowEffectName_7;

public:
	inline static int32_t get_offset_of_m_EffectPrefabList_3() { return static_cast<int32_t>(offsetof(EffectDemo_t2474128590, ___m_EffectPrefabList_3)); }
	inline List_1_t2585711361 * get_m_EffectPrefabList_3() const { return ___m_EffectPrefabList_3; }
	inline List_1_t2585711361 ** get_address_of_m_EffectPrefabList_3() { return &___m_EffectPrefabList_3; }
	inline void set_m_EffectPrefabList_3(List_1_t2585711361 * value)
	{
		___m_EffectPrefabList_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_EffectPrefabList_3), value);
	}

	inline static int32_t get_offset_of_m_LookAtEffect_4() { return static_cast<int32_t>(offsetof(EffectDemo_t2474128590, ___m_LookAtEffect_4)); }
	inline bool get_m_LookAtEffect_4() const { return ___m_LookAtEffect_4; }
	inline bool* get_address_of_m_LookAtEffect_4() { return &___m_LookAtEffect_4; }
	inline void set_m_LookAtEffect_4(bool value)
	{
		___m_LookAtEffect_4 = value;
	}

	inline static int32_t get_offset_of_m_NowShowEffect_5() { return static_cast<int32_t>(offsetof(EffectDemo_t2474128590, ___m_NowShowEffect_5)); }
	inline GameObject_t1113636619 * get_m_NowShowEffect_5() const { return ___m_NowShowEffect_5; }
	inline GameObject_t1113636619 ** get_address_of_m_NowShowEffect_5() { return &___m_NowShowEffect_5; }
	inline void set_m_NowShowEffect_5(GameObject_t1113636619 * value)
	{
		___m_NowShowEffect_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_NowShowEffect_5), value);
	}

	inline static int32_t get_offset_of_m_NowIndex_6() { return static_cast<int32_t>(offsetof(EffectDemo_t2474128590, ___m_NowIndex_6)); }
	inline int32_t get_m_NowIndex_6() const { return ___m_NowIndex_6; }
	inline int32_t* get_address_of_m_NowIndex_6() { return &___m_NowIndex_6; }
	inline void set_m_NowIndex_6(int32_t value)
	{
		___m_NowIndex_6 = value;
	}

	inline static int32_t get_offset_of_m_NowEffectName_7() { return static_cast<int32_t>(offsetof(EffectDemo_t2474128590, ___m_NowEffectName_7)); }
	inline String_t* get_m_NowEffectName_7() const { return ___m_NowEffectName_7; }
	inline String_t** get_address_of_m_NowEffectName_7() { return &___m_NowEffectName_7; }
	inline void set_m_NowEffectName_7(String_t* value)
	{
		___m_NowEffectName_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_NowEffectName_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTDEMO_T2474128590_H
#ifndef BENCHMARK02_T1571269232_H
#define BENCHMARK02_T1571269232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark02
struct  Benchmark02_t1571269232  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark02::SpawnType
	int32_t ___SpawnType_2;
	// System.Int32 TMPro.Examples.Benchmark02::NumberOfNPC
	int32_t ___NumberOfNPC_3;
	// TMPro.Examples.TextMeshProFloatingText TMPro.Examples.Benchmark02::floatingText_Script
	TextMeshProFloatingText_t845872552 * ___floatingText_Script_4;

public:
	inline static int32_t get_offset_of_SpawnType_2() { return static_cast<int32_t>(offsetof(Benchmark02_t1571269232, ___SpawnType_2)); }
	inline int32_t get_SpawnType_2() const { return ___SpawnType_2; }
	inline int32_t* get_address_of_SpawnType_2() { return &___SpawnType_2; }
	inline void set_SpawnType_2(int32_t value)
	{
		___SpawnType_2 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_3() { return static_cast<int32_t>(offsetof(Benchmark02_t1571269232, ___NumberOfNPC_3)); }
	inline int32_t get_NumberOfNPC_3() const { return ___NumberOfNPC_3; }
	inline int32_t* get_address_of_NumberOfNPC_3() { return &___NumberOfNPC_3; }
	inline void set_NumberOfNPC_3(int32_t value)
	{
		___NumberOfNPC_3 = value;
	}

	inline static int32_t get_offset_of_floatingText_Script_4() { return static_cast<int32_t>(offsetof(Benchmark02_t1571269232, ___floatingText_Script_4)); }
	inline TextMeshProFloatingText_t845872552 * get_floatingText_Script_4() const { return ___floatingText_Script_4; }
	inline TextMeshProFloatingText_t845872552 ** get_address_of_floatingText_Script_4() { return &___floatingText_Script_4; }
	inline void set_floatingText_Script_4(TextMeshProFloatingText_t845872552 * value)
	{
		___floatingText_Script_4 = value;
		Il2CppCodeGenWriteBarrier((&___floatingText_Script_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK02_T1571269232_H
#ifndef TIMEMANAGER_T1960693005_H
#define TIMEMANAGER_T1960693005_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TimeManager
struct  TimeManager_t1960693005  : public MonoBehaviour_t3962482529
{
public:
	// System.String TimeManager::_url
	String_t* ____url_3;
	// System.String TimeManager::_timeData
	String_t* ____timeData_4;
	// System.String TimeManager::_currentTime
	String_t* ____currentTime_5;
	// System.String TimeManager::_currentDate
	String_t* ____currentDate_6;

public:
	inline static int32_t get_offset_of__url_3() { return static_cast<int32_t>(offsetof(TimeManager_t1960693005, ____url_3)); }
	inline String_t* get__url_3() const { return ____url_3; }
	inline String_t** get_address_of__url_3() { return &____url_3; }
	inline void set__url_3(String_t* value)
	{
		____url_3 = value;
		Il2CppCodeGenWriteBarrier((&____url_3), value);
	}

	inline static int32_t get_offset_of__timeData_4() { return static_cast<int32_t>(offsetof(TimeManager_t1960693005, ____timeData_4)); }
	inline String_t* get__timeData_4() const { return ____timeData_4; }
	inline String_t** get_address_of__timeData_4() { return &____timeData_4; }
	inline void set__timeData_4(String_t* value)
	{
		____timeData_4 = value;
		Il2CppCodeGenWriteBarrier((&____timeData_4), value);
	}

	inline static int32_t get_offset_of__currentTime_5() { return static_cast<int32_t>(offsetof(TimeManager_t1960693005, ____currentTime_5)); }
	inline String_t* get__currentTime_5() const { return ____currentTime_5; }
	inline String_t** get_address_of__currentTime_5() { return &____currentTime_5; }
	inline void set__currentTime_5(String_t* value)
	{
		____currentTime_5 = value;
		Il2CppCodeGenWriteBarrier((&____currentTime_5), value);
	}

	inline static int32_t get_offset_of__currentDate_6() { return static_cast<int32_t>(offsetof(TimeManager_t1960693005, ____currentDate_6)); }
	inline String_t* get__currentDate_6() const { return ____currentDate_6; }
	inline String_t** get_address_of__currentDate_6() { return &____currentDate_6; }
	inline void set__currentDate_6(String_t* value)
	{
		____currentDate_6 = value;
		Il2CppCodeGenWriteBarrier((&____currentDate_6), value);
	}
};

struct TimeManager_t1960693005_StaticFields
{
public:
	// TimeManager TimeManager::sharedInstance
	TimeManager_t1960693005 * ___sharedInstance_2;

public:
	inline static int32_t get_offset_of_sharedInstance_2() { return static_cast<int32_t>(offsetof(TimeManager_t1960693005_StaticFields, ___sharedInstance_2)); }
	inline TimeManager_t1960693005 * get_sharedInstance_2() const { return ___sharedInstance_2; }
	inline TimeManager_t1960693005 ** get_address_of_sharedInstance_2() { return &___sharedInstance_2; }
	inline void set_sharedInstance_2(TimeManager_t1960693005 * value)
	{
		___sharedInstance_2 = value;
		Il2CppCodeGenWriteBarrier((&___sharedInstance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEMANAGER_T1960693005_H
#ifndef BENCHMARK03_T1571203696_H
#define BENCHMARK03_T1571203696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark03
struct  Benchmark03_t1571203696  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark03::SpawnType
	int32_t ___SpawnType_2;
	// System.Int32 TMPro.Examples.Benchmark03::NumberOfNPC
	int32_t ___NumberOfNPC_3;
	// UnityEngine.Font TMPro.Examples.Benchmark03::TheFont
	Font_t1956802104 * ___TheFont_4;

public:
	inline static int32_t get_offset_of_SpawnType_2() { return static_cast<int32_t>(offsetof(Benchmark03_t1571203696, ___SpawnType_2)); }
	inline int32_t get_SpawnType_2() const { return ___SpawnType_2; }
	inline int32_t* get_address_of_SpawnType_2() { return &___SpawnType_2; }
	inline void set_SpawnType_2(int32_t value)
	{
		___SpawnType_2 = value;
	}

	inline static int32_t get_offset_of_NumberOfNPC_3() { return static_cast<int32_t>(offsetof(Benchmark03_t1571203696, ___NumberOfNPC_3)); }
	inline int32_t get_NumberOfNPC_3() const { return ___NumberOfNPC_3; }
	inline int32_t* get_address_of_NumberOfNPC_3() { return &___NumberOfNPC_3; }
	inline void set_NumberOfNPC_3(int32_t value)
	{
		___NumberOfNPC_3 = value;
	}

	inline static int32_t get_offset_of_TheFont_4() { return static_cast<int32_t>(offsetof(Benchmark03_t1571203696, ___TheFont_4)); }
	inline Font_t1956802104 * get_TheFont_4() const { return ___TheFont_4; }
	inline Font_t1956802104 ** get_address_of_TheFont_4() { return &___TheFont_4; }
	inline void set_TheFont_4(Font_t1956802104 * value)
	{
		___TheFont_4 = value;
		Il2CppCodeGenWriteBarrier((&___TheFont_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK03_T1571203696_H
#ifndef FADER_T1795977150_H
#define FADER_T1795977150_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Fader
struct  Fader_t1795977150  : public MonoBehaviour_t3962482529
{
public:
	// System.Boolean Fader::start
	bool ___start_2;
	// System.Single Fader::fadeDamp
	float ___fadeDamp_3;
	// System.String Fader::fadeScene
	String_t* ___fadeScene_4;
	// System.Single Fader::alpha
	float ___alpha_5;
	// UnityEngine.Color Fader::fadeColor
	Color_t2555686324  ___fadeColor_6;
	// System.Boolean Fader::isFadeIn
	bool ___isFadeIn_7;

public:
	inline static int32_t get_offset_of_start_2() { return static_cast<int32_t>(offsetof(Fader_t1795977150, ___start_2)); }
	inline bool get_start_2() const { return ___start_2; }
	inline bool* get_address_of_start_2() { return &___start_2; }
	inline void set_start_2(bool value)
	{
		___start_2 = value;
	}

	inline static int32_t get_offset_of_fadeDamp_3() { return static_cast<int32_t>(offsetof(Fader_t1795977150, ___fadeDamp_3)); }
	inline float get_fadeDamp_3() const { return ___fadeDamp_3; }
	inline float* get_address_of_fadeDamp_3() { return &___fadeDamp_3; }
	inline void set_fadeDamp_3(float value)
	{
		___fadeDamp_3 = value;
	}

	inline static int32_t get_offset_of_fadeScene_4() { return static_cast<int32_t>(offsetof(Fader_t1795977150, ___fadeScene_4)); }
	inline String_t* get_fadeScene_4() const { return ___fadeScene_4; }
	inline String_t** get_address_of_fadeScene_4() { return &___fadeScene_4; }
	inline void set_fadeScene_4(String_t* value)
	{
		___fadeScene_4 = value;
		Il2CppCodeGenWriteBarrier((&___fadeScene_4), value);
	}

	inline static int32_t get_offset_of_alpha_5() { return static_cast<int32_t>(offsetof(Fader_t1795977150, ___alpha_5)); }
	inline float get_alpha_5() const { return ___alpha_5; }
	inline float* get_address_of_alpha_5() { return &___alpha_5; }
	inline void set_alpha_5(float value)
	{
		___alpha_5 = value;
	}

	inline static int32_t get_offset_of_fadeColor_6() { return static_cast<int32_t>(offsetof(Fader_t1795977150, ___fadeColor_6)); }
	inline Color_t2555686324  get_fadeColor_6() const { return ___fadeColor_6; }
	inline Color_t2555686324 * get_address_of_fadeColor_6() { return &___fadeColor_6; }
	inline void set_fadeColor_6(Color_t2555686324  value)
	{
		___fadeColor_6 = value;
	}

	inline static int32_t get_offset_of_isFadeIn_7() { return static_cast<int32_t>(offsetof(Fader_t1795977150, ___isFadeIn_7)); }
	inline bool get_isFadeIn_7() const { return ___isFadeIn_7; }
	inline bool* get_address_of_isFadeIn_7() { return &___isFadeIn_7; }
	inline void set_isFadeIn_7(bool value)
	{
		___isFadeIn_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FADER_T1795977150_H
#ifndef PROGRESSRADIALBEHAVIOUR_T4010907734_H
#define PROGRESSRADIALBEHAVIOUR_T4010907734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProgressBar.ProgressRadialBehaviour
struct  ProgressRadialBehaviour_t4010907734  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image ProgressBar.ProgressRadialBehaviour::m_Fill
	Image_t2670269651 * ___m_Fill_2;
	// System.Single ProgressBar.ProgressRadialBehaviour::m_Value
	float ___m_Value_3;
	// System.Single ProgressBar.ProgressRadialBehaviour::<TransitoryValue>k__BackingField
	float ___U3CTransitoryValueU3Ek__BackingField_4;
	// UnityEngine.UI.Text ProgressBar.ProgressRadialBehaviour::m_AttachedText
	Text_t1901882714 * ___m_AttachedText_5;
	// System.Single ProgressBar.ProgressRadialBehaviour::ProgressSpeed
	float ___ProgressSpeed_6;
	// System.Boolean ProgressBar.ProgressRadialBehaviour::TriggerOnComplete
	bool ___TriggerOnComplete_7;
	// ProgressBar.Utils.OnCompleteEvent ProgressBar.ProgressRadialBehaviour::OnCompleteMethods
	OnCompleteEvent_t1779196559 * ___OnCompleteMethods_8;

public:
	inline static int32_t get_offset_of_m_Fill_2() { return static_cast<int32_t>(offsetof(ProgressRadialBehaviour_t4010907734, ___m_Fill_2)); }
	inline Image_t2670269651 * get_m_Fill_2() const { return ___m_Fill_2; }
	inline Image_t2670269651 ** get_address_of_m_Fill_2() { return &___m_Fill_2; }
	inline void set_m_Fill_2(Image_t2670269651 * value)
	{
		___m_Fill_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Fill_2), value);
	}

	inline static int32_t get_offset_of_m_Value_3() { return static_cast<int32_t>(offsetof(ProgressRadialBehaviour_t4010907734, ___m_Value_3)); }
	inline float get_m_Value_3() const { return ___m_Value_3; }
	inline float* get_address_of_m_Value_3() { return &___m_Value_3; }
	inline void set_m_Value_3(float value)
	{
		___m_Value_3 = value;
	}

	inline static int32_t get_offset_of_U3CTransitoryValueU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ProgressRadialBehaviour_t4010907734, ___U3CTransitoryValueU3Ek__BackingField_4)); }
	inline float get_U3CTransitoryValueU3Ek__BackingField_4() const { return ___U3CTransitoryValueU3Ek__BackingField_4; }
	inline float* get_address_of_U3CTransitoryValueU3Ek__BackingField_4() { return &___U3CTransitoryValueU3Ek__BackingField_4; }
	inline void set_U3CTransitoryValueU3Ek__BackingField_4(float value)
	{
		___U3CTransitoryValueU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_m_AttachedText_5() { return static_cast<int32_t>(offsetof(ProgressRadialBehaviour_t4010907734, ___m_AttachedText_5)); }
	inline Text_t1901882714 * get_m_AttachedText_5() const { return ___m_AttachedText_5; }
	inline Text_t1901882714 ** get_address_of_m_AttachedText_5() { return &___m_AttachedText_5; }
	inline void set_m_AttachedText_5(Text_t1901882714 * value)
	{
		___m_AttachedText_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_AttachedText_5), value);
	}

	inline static int32_t get_offset_of_ProgressSpeed_6() { return static_cast<int32_t>(offsetof(ProgressRadialBehaviour_t4010907734, ___ProgressSpeed_6)); }
	inline float get_ProgressSpeed_6() const { return ___ProgressSpeed_6; }
	inline float* get_address_of_ProgressSpeed_6() { return &___ProgressSpeed_6; }
	inline void set_ProgressSpeed_6(float value)
	{
		___ProgressSpeed_6 = value;
	}

	inline static int32_t get_offset_of_TriggerOnComplete_7() { return static_cast<int32_t>(offsetof(ProgressRadialBehaviour_t4010907734, ___TriggerOnComplete_7)); }
	inline bool get_TriggerOnComplete_7() const { return ___TriggerOnComplete_7; }
	inline bool* get_address_of_TriggerOnComplete_7() { return &___TriggerOnComplete_7; }
	inline void set_TriggerOnComplete_7(bool value)
	{
		___TriggerOnComplete_7 = value;
	}

	inline static int32_t get_offset_of_OnCompleteMethods_8() { return static_cast<int32_t>(offsetof(ProgressRadialBehaviour_t4010907734, ___OnCompleteMethods_8)); }
	inline OnCompleteEvent_t1779196559 * get_OnCompleteMethods_8() const { return ___OnCompleteMethods_8; }
	inline OnCompleteEvent_t1779196559 ** get_address_of_OnCompleteMethods_8() { return &___OnCompleteMethods_8; }
	inline void set_OnCompleteMethods_8(OnCompleteEvent_t1779196559 * value)
	{
		___OnCompleteMethods_8 = value;
		Il2CppCodeGenWriteBarrier((&___OnCompleteMethods_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSRADIALBEHAVIOUR_T4010907734_H
#ifndef PURCHASEHANDLER_T2101448223_H
#define PURCHASEHANDLER_T2101448223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PurchaseHandler
struct  PurchaseHandler_t2101448223  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text PurchaseHandler::messageText
	Text_t1901882714 * ___messageText_2;
	// Sdkbox.IAP PurchaseHandler::_iap
	IAP_t1939458538 * ____iap_3;
	// System.String PurchaseHandler::mLogBuffer
	String_t* ___mLogBuffer_4;

public:
	inline static int32_t get_offset_of_messageText_2() { return static_cast<int32_t>(offsetof(PurchaseHandler_t2101448223, ___messageText_2)); }
	inline Text_t1901882714 * get_messageText_2() const { return ___messageText_2; }
	inline Text_t1901882714 ** get_address_of_messageText_2() { return &___messageText_2; }
	inline void set_messageText_2(Text_t1901882714 * value)
	{
		___messageText_2 = value;
		Il2CppCodeGenWriteBarrier((&___messageText_2), value);
	}

	inline static int32_t get_offset_of__iap_3() { return static_cast<int32_t>(offsetof(PurchaseHandler_t2101448223, ____iap_3)); }
	inline IAP_t1939458538 * get__iap_3() const { return ____iap_3; }
	inline IAP_t1939458538 ** get_address_of__iap_3() { return &____iap_3; }
	inline void set__iap_3(IAP_t1939458538 * value)
	{
		____iap_3 = value;
		Il2CppCodeGenWriteBarrier((&____iap_3), value);
	}

	inline static int32_t get_offset_of_mLogBuffer_4() { return static_cast<int32_t>(offsetof(PurchaseHandler_t2101448223, ___mLogBuffer_4)); }
	inline String_t* get_mLogBuffer_4() const { return ___mLogBuffer_4; }
	inline String_t** get_address_of_mLogBuffer_4() { return &___mLogBuffer_4; }
	inline void set_mLogBuffer_4(String_t* value)
	{
		___mLogBuffer_4 = value;
		Il2CppCodeGenWriteBarrier((&___mLogBuffer_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASEHANDLER_T2101448223_H
#ifndef PROGRESSBARBEHAVIOUR_T3447880169_H
#define PROGRESSBARBEHAVIOUR_T3447880169_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProgressBar.ProgressBarBehaviour
struct  ProgressBarBehaviour_t3447880169  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.RectTransform ProgressBar.ProgressBarBehaviour::m_FillRect
	RectTransform_t3704657025 * ___m_FillRect_2;
	// ProgressBar.Utils.FillerProperty ProgressBar.ProgressBarBehaviour::m_FillerInfo
	FillerProperty_t1637340060 * ___m_FillerInfo_3;
	// ProgressBar.Utils.ProgressValue ProgressBar.ProgressBarBehaviour::m_Value
	ProgressValue_t1710479104 * ___m_Value_4;
	// System.Single ProgressBar.ProgressBarBehaviour::<TransitoryValue>k__BackingField
	float ___U3CTransitoryValueU3Ek__BackingField_5;
	// UnityEngine.UI.Text ProgressBar.ProgressBarBehaviour::m_AttachedText
	Text_t1901882714 * ___m_AttachedText_6;
	// System.Int32 ProgressBar.ProgressBarBehaviour::ProgressSpeed
	int32_t ___ProgressSpeed_7;
	// System.Boolean ProgressBar.ProgressBarBehaviour::TriggerOnComplete
	bool ___TriggerOnComplete_8;
	// ProgressBar.Utils.OnCompleteEvent ProgressBar.ProgressBarBehaviour::OnCompleteMethods
	OnCompleteEvent_t1779196559 * ___OnCompleteMethods_9;
	// System.Single ProgressBar.ProgressBarBehaviour::m_XOffset
	float ___m_XOffset_10;

public:
	inline static int32_t get_offset_of_m_FillRect_2() { return static_cast<int32_t>(offsetof(ProgressBarBehaviour_t3447880169, ___m_FillRect_2)); }
	inline RectTransform_t3704657025 * get_m_FillRect_2() const { return ___m_FillRect_2; }
	inline RectTransform_t3704657025 ** get_address_of_m_FillRect_2() { return &___m_FillRect_2; }
	inline void set_m_FillRect_2(RectTransform_t3704657025 * value)
	{
		___m_FillRect_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillRect_2), value);
	}

	inline static int32_t get_offset_of_m_FillerInfo_3() { return static_cast<int32_t>(offsetof(ProgressBarBehaviour_t3447880169, ___m_FillerInfo_3)); }
	inline FillerProperty_t1637340060 * get_m_FillerInfo_3() const { return ___m_FillerInfo_3; }
	inline FillerProperty_t1637340060 ** get_address_of_m_FillerInfo_3() { return &___m_FillerInfo_3; }
	inline void set_m_FillerInfo_3(FillerProperty_t1637340060 * value)
	{
		___m_FillerInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_FillerInfo_3), value);
	}

	inline static int32_t get_offset_of_m_Value_4() { return static_cast<int32_t>(offsetof(ProgressBarBehaviour_t3447880169, ___m_Value_4)); }
	inline ProgressValue_t1710479104 * get_m_Value_4() const { return ___m_Value_4; }
	inline ProgressValue_t1710479104 ** get_address_of_m_Value_4() { return &___m_Value_4; }
	inline void set_m_Value_4(ProgressValue_t1710479104 * value)
	{
		___m_Value_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_4), value);
	}

	inline static int32_t get_offset_of_U3CTransitoryValueU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ProgressBarBehaviour_t3447880169, ___U3CTransitoryValueU3Ek__BackingField_5)); }
	inline float get_U3CTransitoryValueU3Ek__BackingField_5() const { return ___U3CTransitoryValueU3Ek__BackingField_5; }
	inline float* get_address_of_U3CTransitoryValueU3Ek__BackingField_5() { return &___U3CTransitoryValueU3Ek__BackingField_5; }
	inline void set_U3CTransitoryValueU3Ek__BackingField_5(float value)
	{
		___U3CTransitoryValueU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_m_AttachedText_6() { return static_cast<int32_t>(offsetof(ProgressBarBehaviour_t3447880169, ___m_AttachedText_6)); }
	inline Text_t1901882714 * get_m_AttachedText_6() const { return ___m_AttachedText_6; }
	inline Text_t1901882714 ** get_address_of_m_AttachedText_6() { return &___m_AttachedText_6; }
	inline void set_m_AttachedText_6(Text_t1901882714 * value)
	{
		___m_AttachedText_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_AttachedText_6), value);
	}

	inline static int32_t get_offset_of_ProgressSpeed_7() { return static_cast<int32_t>(offsetof(ProgressBarBehaviour_t3447880169, ___ProgressSpeed_7)); }
	inline int32_t get_ProgressSpeed_7() const { return ___ProgressSpeed_7; }
	inline int32_t* get_address_of_ProgressSpeed_7() { return &___ProgressSpeed_7; }
	inline void set_ProgressSpeed_7(int32_t value)
	{
		___ProgressSpeed_7 = value;
	}

	inline static int32_t get_offset_of_TriggerOnComplete_8() { return static_cast<int32_t>(offsetof(ProgressBarBehaviour_t3447880169, ___TriggerOnComplete_8)); }
	inline bool get_TriggerOnComplete_8() const { return ___TriggerOnComplete_8; }
	inline bool* get_address_of_TriggerOnComplete_8() { return &___TriggerOnComplete_8; }
	inline void set_TriggerOnComplete_8(bool value)
	{
		___TriggerOnComplete_8 = value;
	}

	inline static int32_t get_offset_of_OnCompleteMethods_9() { return static_cast<int32_t>(offsetof(ProgressBarBehaviour_t3447880169, ___OnCompleteMethods_9)); }
	inline OnCompleteEvent_t1779196559 * get_OnCompleteMethods_9() const { return ___OnCompleteMethods_9; }
	inline OnCompleteEvent_t1779196559 ** get_address_of_OnCompleteMethods_9() { return &___OnCompleteMethods_9; }
	inline void set_OnCompleteMethods_9(OnCompleteEvent_t1779196559 * value)
	{
		___OnCompleteMethods_9 = value;
		Il2CppCodeGenWriteBarrier((&___OnCompleteMethods_9), value);
	}

	inline static int32_t get_offset_of_m_XOffset_10() { return static_cast<int32_t>(offsetof(ProgressBarBehaviour_t3447880169, ___m_XOffset_10)); }
	inline float get_m_XOffset_10() const { return ___m_XOffset_10; }
	inline float* get_address_of_m_XOffset_10() { return &___m_XOffset_10; }
	inline void set_m_XOffset_10(float value)
	{
		___m_XOffset_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSBARBEHAVIOUR_T3447880169_H
#ifndef RENDEREFFECT_T2584084283_H
#define RENDEREFFECT_T2584084283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RenderEffect
struct  RenderEffect_t2584084283  : public MonoBehaviour_t3962482529
{
public:
	// RenderBillBoardType RenderEffect::m_BillBoardType
	int32_t ___m_BillBoardType_2;
	// UnityEngine.Camera RenderEffect::m_ReferenceCamera
	Camera_t4157153871 * ___m_ReferenceCamera_3;
	// System.Boolean RenderEffect::m_EnableBillBoard
	bool ___m_EnableBillBoard_4;
	// System.Boolean RenderEffect::m_EnableSetSortLayer
	bool ___m_EnableSetSortLayer_5;
	// UnityEngine.Renderer RenderEffect::m_Render
	Renderer_t2627027031 * ___m_Render_6;
	// System.Collections.Generic.List`1<MaterialEffect> RenderEffect::m_MaterialEffects
	List_1_t1069258399 * ___m_MaterialEffects_7;
	// System.Single RenderEffect::m_TimeLine
	float ___m_TimeLine_8;
	// System.Int32 RenderEffect::m_SortingLayerID
	int32_t ___m_SortingLayerID_9;
	// System.Int32 RenderEffect::m_SortingOrder
	int32_t ___m_SortingOrder_10;

public:
	inline static int32_t get_offset_of_m_BillBoardType_2() { return static_cast<int32_t>(offsetof(RenderEffect_t2584084283, ___m_BillBoardType_2)); }
	inline int32_t get_m_BillBoardType_2() const { return ___m_BillBoardType_2; }
	inline int32_t* get_address_of_m_BillBoardType_2() { return &___m_BillBoardType_2; }
	inline void set_m_BillBoardType_2(int32_t value)
	{
		___m_BillBoardType_2 = value;
	}

	inline static int32_t get_offset_of_m_ReferenceCamera_3() { return static_cast<int32_t>(offsetof(RenderEffect_t2584084283, ___m_ReferenceCamera_3)); }
	inline Camera_t4157153871 * get_m_ReferenceCamera_3() const { return ___m_ReferenceCamera_3; }
	inline Camera_t4157153871 ** get_address_of_m_ReferenceCamera_3() { return &___m_ReferenceCamera_3; }
	inline void set_m_ReferenceCamera_3(Camera_t4157153871 * value)
	{
		___m_ReferenceCamera_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReferenceCamera_3), value);
	}

	inline static int32_t get_offset_of_m_EnableBillBoard_4() { return static_cast<int32_t>(offsetof(RenderEffect_t2584084283, ___m_EnableBillBoard_4)); }
	inline bool get_m_EnableBillBoard_4() const { return ___m_EnableBillBoard_4; }
	inline bool* get_address_of_m_EnableBillBoard_4() { return &___m_EnableBillBoard_4; }
	inline void set_m_EnableBillBoard_4(bool value)
	{
		___m_EnableBillBoard_4 = value;
	}

	inline static int32_t get_offset_of_m_EnableSetSortLayer_5() { return static_cast<int32_t>(offsetof(RenderEffect_t2584084283, ___m_EnableSetSortLayer_5)); }
	inline bool get_m_EnableSetSortLayer_5() const { return ___m_EnableSetSortLayer_5; }
	inline bool* get_address_of_m_EnableSetSortLayer_5() { return &___m_EnableSetSortLayer_5; }
	inline void set_m_EnableSetSortLayer_5(bool value)
	{
		___m_EnableSetSortLayer_5 = value;
	}

	inline static int32_t get_offset_of_m_Render_6() { return static_cast<int32_t>(offsetof(RenderEffect_t2584084283, ___m_Render_6)); }
	inline Renderer_t2627027031 * get_m_Render_6() const { return ___m_Render_6; }
	inline Renderer_t2627027031 ** get_address_of_m_Render_6() { return &___m_Render_6; }
	inline void set_m_Render_6(Renderer_t2627027031 * value)
	{
		___m_Render_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Render_6), value);
	}

	inline static int32_t get_offset_of_m_MaterialEffects_7() { return static_cast<int32_t>(offsetof(RenderEffect_t2584084283, ___m_MaterialEffects_7)); }
	inline List_1_t1069258399 * get_m_MaterialEffects_7() const { return ___m_MaterialEffects_7; }
	inline List_1_t1069258399 ** get_address_of_m_MaterialEffects_7() { return &___m_MaterialEffects_7; }
	inline void set_m_MaterialEffects_7(List_1_t1069258399 * value)
	{
		___m_MaterialEffects_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_MaterialEffects_7), value);
	}

	inline static int32_t get_offset_of_m_TimeLine_8() { return static_cast<int32_t>(offsetof(RenderEffect_t2584084283, ___m_TimeLine_8)); }
	inline float get_m_TimeLine_8() const { return ___m_TimeLine_8; }
	inline float* get_address_of_m_TimeLine_8() { return &___m_TimeLine_8; }
	inline void set_m_TimeLine_8(float value)
	{
		___m_TimeLine_8 = value;
	}

	inline static int32_t get_offset_of_m_SortingLayerID_9() { return static_cast<int32_t>(offsetof(RenderEffect_t2584084283, ___m_SortingLayerID_9)); }
	inline int32_t get_m_SortingLayerID_9() const { return ___m_SortingLayerID_9; }
	inline int32_t* get_address_of_m_SortingLayerID_9() { return &___m_SortingLayerID_9; }
	inline void set_m_SortingLayerID_9(int32_t value)
	{
		___m_SortingLayerID_9 = value;
	}

	inline static int32_t get_offset_of_m_SortingOrder_10() { return static_cast<int32_t>(offsetof(RenderEffect_t2584084283, ___m_SortingOrder_10)); }
	inline int32_t get_m_SortingOrder_10() const { return ___m_SortingOrder_10; }
	inline int32_t* get_address_of_m_SortingOrder_10() { return &___m_SortingOrder_10; }
	inline void set_m_SortingOrder_10(int32_t value)
	{
		___m_SortingOrder_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RENDEREFFECT_T2584084283_H
#ifndef SDKBOX_T1939795462_H
#define SDKBOX_T1939795462_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sdkbox.SDKBOX
struct  SDKBOX_t1939795462  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct SDKBOX_t1939795462_StaticFields
{
public:
	// Sdkbox.SDKBOX Sdkbox.SDKBOX::_instance
	SDKBOX_t1939795462 * ____instance_2;

public:
	inline static int32_t get_offset_of__instance_2() { return static_cast<int32_t>(offsetof(SDKBOX_t1939795462_StaticFields, ____instance_2)); }
	inline SDKBOX_t1939795462 * get__instance_2() const { return ____instance_2; }
	inline SDKBOX_t1939795462 ** get_address_of__instance_2() { return &____instance_2; }
	inline void set__instance_2(SDKBOX_t1939795462 * value)
	{
		____instance_2 = value;
		Il2CppCodeGenWriteBarrier((&____instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SDKBOX_T1939795462_H
#ifndef DEMOSCRIPT_T4014867723_H
#define DEMOSCRIPT_T4014867723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DemoScript
struct  DemoScript_t4014867723  : public MonoBehaviour_t3962482529
{
public:
	// System.String DemoScript::scene
	String_t* ___scene_2;
	// UnityEngine.Color DemoScript::loadToColor
	Color_t2555686324  ___loadToColor_3;

public:
	inline static int32_t get_offset_of_scene_2() { return static_cast<int32_t>(offsetof(DemoScript_t4014867723, ___scene_2)); }
	inline String_t* get_scene_2() const { return ___scene_2; }
	inline String_t** get_address_of_scene_2() { return &___scene_2; }
	inline void set_scene_2(String_t* value)
	{
		___scene_2 = value;
		Il2CppCodeGenWriteBarrier((&___scene_2), value);
	}

	inline static int32_t get_offset_of_loadToColor_3() { return static_cast<int32_t>(offsetof(DemoScript_t4014867723, ___loadToColor_3)); }
	inline Color_t2555686324  get_loadToColor_3() const { return ___loadToColor_3; }
	inline Color_t2555686324 * get_address_of_loadToColor_3() { return &___loadToColor_3; }
	inline void set_loadToColor_3(Color_t2555686324  value)
	{
		___loadToColor_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEMOSCRIPT_T4014867723_H
#ifndef CHATCONTROLLER_T3486202795_H
#define CHATCONTROLLER_T3486202795_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ChatController
struct  ChatController_t3486202795  : public MonoBehaviour_t3962482529
{
public:
	// TMPro.TMP_InputField ChatController::TMP_ChatInput
	TMP_InputField_t1099764886 * ___TMP_ChatInput_2;
	// TMPro.TMP_Text ChatController::TMP_ChatOutput
	TMP_Text_t2599618874 * ___TMP_ChatOutput_3;
	// UnityEngine.UI.Scrollbar ChatController::ChatScrollbar
	Scrollbar_t1494447233 * ___ChatScrollbar_4;

public:
	inline static int32_t get_offset_of_TMP_ChatInput_2() { return static_cast<int32_t>(offsetof(ChatController_t3486202795, ___TMP_ChatInput_2)); }
	inline TMP_InputField_t1099764886 * get_TMP_ChatInput_2() const { return ___TMP_ChatInput_2; }
	inline TMP_InputField_t1099764886 ** get_address_of_TMP_ChatInput_2() { return &___TMP_ChatInput_2; }
	inline void set_TMP_ChatInput_2(TMP_InputField_t1099764886 * value)
	{
		___TMP_ChatInput_2 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_ChatInput_2), value);
	}

	inline static int32_t get_offset_of_TMP_ChatOutput_3() { return static_cast<int32_t>(offsetof(ChatController_t3486202795, ___TMP_ChatOutput_3)); }
	inline TMP_Text_t2599618874 * get_TMP_ChatOutput_3() const { return ___TMP_ChatOutput_3; }
	inline TMP_Text_t2599618874 ** get_address_of_TMP_ChatOutput_3() { return &___TMP_ChatOutput_3; }
	inline void set_TMP_ChatOutput_3(TMP_Text_t2599618874 * value)
	{
		___TMP_ChatOutput_3 = value;
		Il2CppCodeGenWriteBarrier((&___TMP_ChatOutput_3), value);
	}

	inline static int32_t get_offset_of_ChatScrollbar_4() { return static_cast<int32_t>(offsetof(ChatController_t3486202795, ___ChatScrollbar_4)); }
	inline Scrollbar_t1494447233 * get_ChatScrollbar_4() const { return ___ChatScrollbar_4; }
	inline Scrollbar_t1494447233 ** get_address_of_ChatScrollbar_4() { return &___ChatScrollbar_4; }
	inline void set_ChatScrollbar_4(Scrollbar_t1494447233 * value)
	{
		___ChatScrollbar_4 = value;
		Il2CppCodeGenWriteBarrier((&___ChatScrollbar_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CHATCONTROLLER_T3486202795_H
#ifndef BENCHMARK04_T1570876016_H
#define BENCHMARK04_T1570876016_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.Benchmark04
struct  Benchmark04_t1570876016  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 TMPro.Examples.Benchmark04::SpawnType
	int32_t ___SpawnType_2;
	// System.Int32 TMPro.Examples.Benchmark04::MinPointSize
	int32_t ___MinPointSize_3;
	// System.Int32 TMPro.Examples.Benchmark04::MaxPointSize
	int32_t ___MaxPointSize_4;
	// System.Int32 TMPro.Examples.Benchmark04::Steps
	int32_t ___Steps_5;
	// UnityEngine.Transform TMPro.Examples.Benchmark04::m_Transform
	Transform_t3600365921 * ___m_Transform_6;

public:
	inline static int32_t get_offset_of_SpawnType_2() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___SpawnType_2)); }
	inline int32_t get_SpawnType_2() const { return ___SpawnType_2; }
	inline int32_t* get_address_of_SpawnType_2() { return &___SpawnType_2; }
	inline void set_SpawnType_2(int32_t value)
	{
		___SpawnType_2 = value;
	}

	inline static int32_t get_offset_of_MinPointSize_3() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___MinPointSize_3)); }
	inline int32_t get_MinPointSize_3() const { return ___MinPointSize_3; }
	inline int32_t* get_address_of_MinPointSize_3() { return &___MinPointSize_3; }
	inline void set_MinPointSize_3(int32_t value)
	{
		___MinPointSize_3 = value;
	}

	inline static int32_t get_offset_of_MaxPointSize_4() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___MaxPointSize_4)); }
	inline int32_t get_MaxPointSize_4() const { return ___MaxPointSize_4; }
	inline int32_t* get_address_of_MaxPointSize_4() { return &___MaxPointSize_4; }
	inline void set_MaxPointSize_4(int32_t value)
	{
		___MaxPointSize_4 = value;
	}

	inline static int32_t get_offset_of_Steps_5() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___Steps_5)); }
	inline int32_t get_Steps_5() const { return ___Steps_5; }
	inline int32_t* get_address_of_Steps_5() { return &___Steps_5; }
	inline void set_Steps_5(int32_t value)
	{
		___Steps_5 = value;
	}

	inline static int32_t get_offset_of_m_Transform_6() { return static_cast<int32_t>(offsetof(Benchmark04_t1570876016, ___m_Transform_6)); }
	inline Transform_t3600365921 * get_m_Transform_6() const { return ___m_Transform_6; }
	inline Transform_t3600365921 ** get_address_of_m_Transform_6() { return &___m_Transform_6; }
	inline void set_m_Transform_6(Transform_t3600365921 * value)
	{
		___m_Transform_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_Transform_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BENCHMARK04_T1570876016_H
#ifndef ENVMAPANIMATOR_T1140999784_H
#define ENVMAPANIMATOR_T1140999784_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EnvMapAnimator
struct  EnvMapAnimator_t1140999784  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Vector3 EnvMapAnimator::RotationSpeeds
	Vector3_t3722313464  ___RotationSpeeds_2;
	// TMPro.TMP_Text EnvMapAnimator::m_textMeshPro
	TMP_Text_t2599618874 * ___m_textMeshPro_3;
	// UnityEngine.Material EnvMapAnimator::m_material
	Material_t340375123 * ___m_material_4;

public:
	inline static int32_t get_offset_of_RotationSpeeds_2() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1140999784, ___RotationSpeeds_2)); }
	inline Vector3_t3722313464  get_RotationSpeeds_2() const { return ___RotationSpeeds_2; }
	inline Vector3_t3722313464 * get_address_of_RotationSpeeds_2() { return &___RotationSpeeds_2; }
	inline void set_RotationSpeeds_2(Vector3_t3722313464  value)
	{
		___RotationSpeeds_2 = value;
	}

	inline static int32_t get_offset_of_m_textMeshPro_3() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1140999784, ___m_textMeshPro_3)); }
	inline TMP_Text_t2599618874 * get_m_textMeshPro_3() const { return ___m_textMeshPro_3; }
	inline TMP_Text_t2599618874 ** get_address_of_m_textMeshPro_3() { return &___m_textMeshPro_3; }
	inline void set_m_textMeshPro_3(TMP_Text_t2599618874 * value)
	{
		___m_textMeshPro_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_textMeshPro_3), value);
	}

	inline static int32_t get_offset_of_m_material_4() { return static_cast<int32_t>(offsetof(EnvMapAnimator_t1140999784, ___m_material_4)); }
	inline Material_t340375123 * get_m_material_4() const { return ___m_material_4; }
	inline Material_t340375123 ** get_address_of_m_material_4() { return &___m_material_4; }
	inline void set_m_material_4(Material_t340375123 * value)
	{
		___m_material_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_material_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENVMAPANIMATOR_T1140999784_H
#ifndef CAMERACONTROLLER_T2264742161_H
#define CAMERACONTROLLER_T2264742161_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TMPro.Examples.CameraController
struct  CameraController_t2264742161  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Transform TMPro.Examples.CameraController::cameraTransform
	Transform_t3600365921 * ___cameraTransform_2;
	// UnityEngine.Transform TMPro.Examples.CameraController::dummyTarget
	Transform_t3600365921 * ___dummyTarget_3;
	// UnityEngine.Transform TMPro.Examples.CameraController::CameraTarget
	Transform_t3600365921 * ___CameraTarget_4;
	// System.Single TMPro.Examples.CameraController::FollowDistance
	float ___FollowDistance_5;
	// System.Single TMPro.Examples.CameraController::MaxFollowDistance
	float ___MaxFollowDistance_6;
	// System.Single TMPro.Examples.CameraController::MinFollowDistance
	float ___MinFollowDistance_7;
	// System.Single TMPro.Examples.CameraController::ElevationAngle
	float ___ElevationAngle_8;
	// System.Single TMPro.Examples.CameraController::MaxElevationAngle
	float ___MaxElevationAngle_9;
	// System.Single TMPro.Examples.CameraController::MinElevationAngle
	float ___MinElevationAngle_10;
	// System.Single TMPro.Examples.CameraController::OrbitalAngle
	float ___OrbitalAngle_11;
	// TMPro.Examples.CameraController/CameraModes TMPro.Examples.CameraController::CameraMode
	int32_t ___CameraMode_12;
	// System.Boolean TMPro.Examples.CameraController::MovementSmoothing
	bool ___MovementSmoothing_13;
	// System.Boolean TMPro.Examples.CameraController::RotationSmoothing
	bool ___RotationSmoothing_14;
	// System.Boolean TMPro.Examples.CameraController::previousSmoothing
	bool ___previousSmoothing_15;
	// System.Single TMPro.Examples.CameraController::MovementSmoothingValue
	float ___MovementSmoothingValue_16;
	// System.Single TMPro.Examples.CameraController::RotationSmoothingValue
	float ___RotationSmoothingValue_17;
	// System.Single TMPro.Examples.CameraController::MoveSensitivity
	float ___MoveSensitivity_18;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::currentVelocity
	Vector3_t3722313464  ___currentVelocity_19;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::desiredPosition
	Vector3_t3722313464  ___desiredPosition_20;
	// System.Single TMPro.Examples.CameraController::mouseX
	float ___mouseX_21;
	// System.Single TMPro.Examples.CameraController::mouseY
	float ___mouseY_22;
	// UnityEngine.Vector3 TMPro.Examples.CameraController::moveVector
	Vector3_t3722313464  ___moveVector_23;
	// System.Single TMPro.Examples.CameraController::mouseWheel
	float ___mouseWheel_24;

public:
	inline static int32_t get_offset_of_cameraTransform_2() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___cameraTransform_2)); }
	inline Transform_t3600365921 * get_cameraTransform_2() const { return ___cameraTransform_2; }
	inline Transform_t3600365921 ** get_address_of_cameraTransform_2() { return &___cameraTransform_2; }
	inline void set_cameraTransform_2(Transform_t3600365921 * value)
	{
		___cameraTransform_2 = value;
		Il2CppCodeGenWriteBarrier((&___cameraTransform_2), value);
	}

	inline static int32_t get_offset_of_dummyTarget_3() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___dummyTarget_3)); }
	inline Transform_t3600365921 * get_dummyTarget_3() const { return ___dummyTarget_3; }
	inline Transform_t3600365921 ** get_address_of_dummyTarget_3() { return &___dummyTarget_3; }
	inline void set_dummyTarget_3(Transform_t3600365921 * value)
	{
		___dummyTarget_3 = value;
		Il2CppCodeGenWriteBarrier((&___dummyTarget_3), value);
	}

	inline static int32_t get_offset_of_CameraTarget_4() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___CameraTarget_4)); }
	inline Transform_t3600365921 * get_CameraTarget_4() const { return ___CameraTarget_4; }
	inline Transform_t3600365921 ** get_address_of_CameraTarget_4() { return &___CameraTarget_4; }
	inline void set_CameraTarget_4(Transform_t3600365921 * value)
	{
		___CameraTarget_4 = value;
		Il2CppCodeGenWriteBarrier((&___CameraTarget_4), value);
	}

	inline static int32_t get_offset_of_FollowDistance_5() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___FollowDistance_5)); }
	inline float get_FollowDistance_5() const { return ___FollowDistance_5; }
	inline float* get_address_of_FollowDistance_5() { return &___FollowDistance_5; }
	inline void set_FollowDistance_5(float value)
	{
		___FollowDistance_5 = value;
	}

	inline static int32_t get_offset_of_MaxFollowDistance_6() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MaxFollowDistance_6)); }
	inline float get_MaxFollowDistance_6() const { return ___MaxFollowDistance_6; }
	inline float* get_address_of_MaxFollowDistance_6() { return &___MaxFollowDistance_6; }
	inline void set_MaxFollowDistance_6(float value)
	{
		___MaxFollowDistance_6 = value;
	}

	inline static int32_t get_offset_of_MinFollowDistance_7() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MinFollowDistance_7)); }
	inline float get_MinFollowDistance_7() const { return ___MinFollowDistance_7; }
	inline float* get_address_of_MinFollowDistance_7() { return &___MinFollowDistance_7; }
	inline void set_MinFollowDistance_7(float value)
	{
		___MinFollowDistance_7 = value;
	}

	inline static int32_t get_offset_of_ElevationAngle_8() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___ElevationAngle_8)); }
	inline float get_ElevationAngle_8() const { return ___ElevationAngle_8; }
	inline float* get_address_of_ElevationAngle_8() { return &___ElevationAngle_8; }
	inline void set_ElevationAngle_8(float value)
	{
		___ElevationAngle_8 = value;
	}

	inline static int32_t get_offset_of_MaxElevationAngle_9() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MaxElevationAngle_9)); }
	inline float get_MaxElevationAngle_9() const { return ___MaxElevationAngle_9; }
	inline float* get_address_of_MaxElevationAngle_9() { return &___MaxElevationAngle_9; }
	inline void set_MaxElevationAngle_9(float value)
	{
		___MaxElevationAngle_9 = value;
	}

	inline static int32_t get_offset_of_MinElevationAngle_10() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MinElevationAngle_10)); }
	inline float get_MinElevationAngle_10() const { return ___MinElevationAngle_10; }
	inline float* get_address_of_MinElevationAngle_10() { return &___MinElevationAngle_10; }
	inline void set_MinElevationAngle_10(float value)
	{
		___MinElevationAngle_10 = value;
	}

	inline static int32_t get_offset_of_OrbitalAngle_11() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___OrbitalAngle_11)); }
	inline float get_OrbitalAngle_11() const { return ___OrbitalAngle_11; }
	inline float* get_address_of_OrbitalAngle_11() { return &___OrbitalAngle_11; }
	inline void set_OrbitalAngle_11(float value)
	{
		___OrbitalAngle_11 = value;
	}

	inline static int32_t get_offset_of_CameraMode_12() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___CameraMode_12)); }
	inline int32_t get_CameraMode_12() const { return ___CameraMode_12; }
	inline int32_t* get_address_of_CameraMode_12() { return &___CameraMode_12; }
	inline void set_CameraMode_12(int32_t value)
	{
		___CameraMode_12 = value;
	}

	inline static int32_t get_offset_of_MovementSmoothing_13() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MovementSmoothing_13)); }
	inline bool get_MovementSmoothing_13() const { return ___MovementSmoothing_13; }
	inline bool* get_address_of_MovementSmoothing_13() { return &___MovementSmoothing_13; }
	inline void set_MovementSmoothing_13(bool value)
	{
		___MovementSmoothing_13 = value;
	}

	inline static int32_t get_offset_of_RotationSmoothing_14() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___RotationSmoothing_14)); }
	inline bool get_RotationSmoothing_14() const { return ___RotationSmoothing_14; }
	inline bool* get_address_of_RotationSmoothing_14() { return &___RotationSmoothing_14; }
	inline void set_RotationSmoothing_14(bool value)
	{
		___RotationSmoothing_14 = value;
	}

	inline static int32_t get_offset_of_previousSmoothing_15() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___previousSmoothing_15)); }
	inline bool get_previousSmoothing_15() const { return ___previousSmoothing_15; }
	inline bool* get_address_of_previousSmoothing_15() { return &___previousSmoothing_15; }
	inline void set_previousSmoothing_15(bool value)
	{
		___previousSmoothing_15 = value;
	}

	inline static int32_t get_offset_of_MovementSmoothingValue_16() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MovementSmoothingValue_16)); }
	inline float get_MovementSmoothingValue_16() const { return ___MovementSmoothingValue_16; }
	inline float* get_address_of_MovementSmoothingValue_16() { return &___MovementSmoothingValue_16; }
	inline void set_MovementSmoothingValue_16(float value)
	{
		___MovementSmoothingValue_16 = value;
	}

	inline static int32_t get_offset_of_RotationSmoothingValue_17() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___RotationSmoothingValue_17)); }
	inline float get_RotationSmoothingValue_17() const { return ___RotationSmoothingValue_17; }
	inline float* get_address_of_RotationSmoothingValue_17() { return &___RotationSmoothingValue_17; }
	inline void set_RotationSmoothingValue_17(float value)
	{
		___RotationSmoothingValue_17 = value;
	}

	inline static int32_t get_offset_of_MoveSensitivity_18() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___MoveSensitivity_18)); }
	inline float get_MoveSensitivity_18() const { return ___MoveSensitivity_18; }
	inline float* get_address_of_MoveSensitivity_18() { return &___MoveSensitivity_18; }
	inline void set_MoveSensitivity_18(float value)
	{
		___MoveSensitivity_18 = value;
	}

	inline static int32_t get_offset_of_currentVelocity_19() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___currentVelocity_19)); }
	inline Vector3_t3722313464  get_currentVelocity_19() const { return ___currentVelocity_19; }
	inline Vector3_t3722313464 * get_address_of_currentVelocity_19() { return &___currentVelocity_19; }
	inline void set_currentVelocity_19(Vector3_t3722313464  value)
	{
		___currentVelocity_19 = value;
	}

	inline static int32_t get_offset_of_desiredPosition_20() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___desiredPosition_20)); }
	inline Vector3_t3722313464  get_desiredPosition_20() const { return ___desiredPosition_20; }
	inline Vector3_t3722313464 * get_address_of_desiredPosition_20() { return &___desiredPosition_20; }
	inline void set_desiredPosition_20(Vector3_t3722313464  value)
	{
		___desiredPosition_20 = value;
	}

	inline static int32_t get_offset_of_mouseX_21() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___mouseX_21)); }
	inline float get_mouseX_21() const { return ___mouseX_21; }
	inline float* get_address_of_mouseX_21() { return &___mouseX_21; }
	inline void set_mouseX_21(float value)
	{
		___mouseX_21 = value;
	}

	inline static int32_t get_offset_of_mouseY_22() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___mouseY_22)); }
	inline float get_mouseY_22() const { return ___mouseY_22; }
	inline float* get_address_of_mouseY_22() { return &___mouseY_22; }
	inline void set_mouseY_22(float value)
	{
		___mouseY_22 = value;
	}

	inline static int32_t get_offset_of_moveVector_23() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___moveVector_23)); }
	inline Vector3_t3722313464  get_moveVector_23() const { return ___moveVector_23; }
	inline Vector3_t3722313464 * get_address_of_moveVector_23() { return &___moveVector_23; }
	inline void set_moveVector_23(Vector3_t3722313464  value)
	{
		___moveVector_23 = value;
	}

	inline static int32_t get_offset_of_mouseWheel_24() { return static_cast<int32_t>(offsetof(CameraController_t2264742161, ___mouseWheel_24)); }
	inline float get_mouseWheel_24() const { return ___mouseWheel_24; }
	inline float* get_address_of_mouseWheel_24() { return &___mouseWheel_24; }
	inline void set_mouseWheel_24(float value)
	{
		___mouseWheel_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERACONTROLLER_T2264742161_H
#ifndef EFFECTCONTROLLER_T216029290_H
#define EFFECTCONTROLLER_T216029290_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EffectController
struct  EffectController_t216029290  : public MonoBehaviour_t3962482529
{
public:
	// System.Int32 EffectController::m_nNumOfEffects
	int32_t ___m_nNumOfEffects_2;
	// System.Boolean EffectController::m_bLockNums
	bool ___m_bLockNums_3;
	// System.Collections.Generic.List`1<EffectData> EffectController::m_kEffectGenList
	List_1_t3181922523 * ___m_kEffectGenList_4;
	// System.Int32 EffectController::m_nNowIndex
	int32_t ___m_nNowIndex_5;

public:
	inline static int32_t get_offset_of_m_nNumOfEffects_2() { return static_cast<int32_t>(offsetof(EffectController_t216029290, ___m_nNumOfEffects_2)); }
	inline int32_t get_m_nNumOfEffects_2() const { return ___m_nNumOfEffects_2; }
	inline int32_t* get_address_of_m_nNumOfEffects_2() { return &___m_nNumOfEffects_2; }
	inline void set_m_nNumOfEffects_2(int32_t value)
	{
		___m_nNumOfEffects_2 = value;
	}

	inline static int32_t get_offset_of_m_bLockNums_3() { return static_cast<int32_t>(offsetof(EffectController_t216029290, ___m_bLockNums_3)); }
	inline bool get_m_bLockNums_3() const { return ___m_bLockNums_3; }
	inline bool* get_address_of_m_bLockNums_3() { return &___m_bLockNums_3; }
	inline void set_m_bLockNums_3(bool value)
	{
		___m_bLockNums_3 = value;
	}

	inline static int32_t get_offset_of_m_kEffectGenList_4() { return static_cast<int32_t>(offsetof(EffectController_t216029290, ___m_kEffectGenList_4)); }
	inline List_1_t3181922523 * get_m_kEffectGenList_4() const { return ___m_kEffectGenList_4; }
	inline List_1_t3181922523 ** get_address_of_m_kEffectGenList_4() { return &___m_kEffectGenList_4; }
	inline void set_m_kEffectGenList_4(List_1_t3181922523 * value)
	{
		___m_kEffectGenList_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_kEffectGenList_4), value);
	}

	inline static int32_t get_offset_of_m_nNowIndex_5() { return static_cast<int32_t>(offsetof(EffectController_t216029290, ___m_nNowIndex_5)); }
	inline int32_t get_m_nNowIndex_5() const { return ___m_nNowIndex_5; }
	inline int32_t* get_address_of_m_nNowIndex_5() { return &___m_nNowIndex_5; }
	inline void set_m_nNowIndex_5(int32_t value)
	{
		___m_nNowIndex_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EFFECTCONTROLLER_T216029290_H
#ifndef ERRORPANEL_T2568969857_H
#define ERRORPANEL_T2568969857_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Views.ErrorPanel
struct  ErrorPanel_t2568969857  : public GenericPanel_t306775760
{
public:
	// UnityEngine.UI.Text Outback.Views.ErrorPanel::heading
	Text_t1901882714 * ___heading_2;
	// UnityEngine.UI.Text Outback.Views.ErrorPanel::message
	Text_t1901882714 * ___message_3;
	// System.Boolean Outback.Views.ErrorPanel::exitApp
	bool ___exitApp_4;

public:
	inline static int32_t get_offset_of_heading_2() { return static_cast<int32_t>(offsetof(ErrorPanel_t2568969857, ___heading_2)); }
	inline Text_t1901882714 * get_heading_2() const { return ___heading_2; }
	inline Text_t1901882714 ** get_address_of_heading_2() { return &___heading_2; }
	inline void set_heading_2(Text_t1901882714 * value)
	{
		___heading_2 = value;
		Il2CppCodeGenWriteBarrier((&___heading_2), value);
	}

	inline static int32_t get_offset_of_message_3() { return static_cast<int32_t>(offsetof(ErrorPanel_t2568969857, ___message_3)); }
	inline Text_t1901882714 * get_message_3() const { return ___message_3; }
	inline Text_t1901882714 ** get_address_of_message_3() { return &___message_3; }
	inline void set_message_3(Text_t1901882714 * value)
	{
		___message_3 = value;
		Il2CppCodeGenWriteBarrier((&___message_3), value);
	}

	inline static int32_t get_offset_of_exitApp_4() { return static_cast<int32_t>(offsetof(ErrorPanel_t2568969857, ___exitApp_4)); }
	inline bool get_exitApp_4() const { return ___exitApp_4; }
	inline bool* get_address_of_exitApp_4() { return &___exitApp_4; }
	inline void set_exitApp_4(bool value)
	{
		___exitApp_4 = value;
	}
};

struct ErrorPanel_t2568969857_StaticFields
{
public:
	// Outback.Views.ErrorPanel Outback.Views.ErrorPanel::instance
	ErrorPanel_t2568969857 * ___instance_5;

public:
	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(ErrorPanel_t2568969857_StaticFields, ___instance_5)); }
	inline ErrorPanel_t2568969857 * get_instance_5() const { return ___instance_5; }
	inline ErrorPanel_t2568969857 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(ErrorPanel_t2568969857 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORPANEL_T2568969857_H
#ifndef MENUBASE_T1079888489_H
#define MENUBASE_T1079888489_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.MenuBase
struct  MenuBase_t1079888489  : public ConsoleBase_t1023975560
{
public:

public:
};

struct MenuBase_t1079888489_StaticFields
{
public:
	// Facebook.Unity.ShareDialogMode Facebook.Unity.Example.MenuBase::shareDialogMode
	int32_t ___shareDialogMode_13;

public:
	inline static int32_t get_offset_of_shareDialogMode_13() { return static_cast<int32_t>(offsetof(MenuBase_t1079888489_StaticFields, ___shareDialogMode_13)); }
	inline int32_t get_shareDialogMode_13() const { return ___shareDialogMode_13; }
	inline int32_t* get_address_of_shareDialogMode_13() { return &___shareDialogMode_13; }
	inline void set_shareDialogMode_13(int32_t value)
	{
		___shareDialogMode_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUBASE_T1079888489_H
#ifndef LOGVIEW_T1067263371_H
#define LOGVIEW_T1067263371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.LogView
struct  LogView_t1067263371  : public ConsoleBase_t1023975560
{
public:

public:
};

struct LogView_t1067263371_StaticFields
{
public:
	// System.String Facebook.Unity.Example.LogView::datePatt
	String_t* ___datePatt_13;
	// System.Collections.Generic.IList`1<System.String> Facebook.Unity.Example.LogView::events
	RuntimeObject* ___events_14;

public:
	inline static int32_t get_offset_of_datePatt_13() { return static_cast<int32_t>(offsetof(LogView_t1067263371_StaticFields, ___datePatt_13)); }
	inline String_t* get_datePatt_13() const { return ___datePatt_13; }
	inline String_t** get_address_of_datePatt_13() { return &___datePatt_13; }
	inline void set_datePatt_13(String_t* value)
	{
		___datePatt_13 = value;
		Il2CppCodeGenWriteBarrier((&___datePatt_13), value);
	}

	inline static int32_t get_offset_of_events_14() { return static_cast<int32_t>(offsetof(LogView_t1067263371_StaticFields, ___events_14)); }
	inline RuntimeObject* get_events_14() const { return ___events_14; }
	inline RuntimeObject** get_address_of_events_14() { return &___events_14; }
	inline void set_events_14(RuntimeObject* value)
	{
		___events_14 = value;
		Il2CppCodeGenWriteBarrier((&___events_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGVIEW_T1067263371_H
#ifndef IAP_T1939458538_H
#define IAP_T1939458538_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Sdkbox.IAP
struct  IAP_t1939458538  : public PluginBase_1_t2351609330
{
public:
	// System.Collections.Generic.List`1<Sdkbox.ProductDescription> Sdkbox.IAP::iOSProducts
	List_1_t3603741248 * ___iOSProducts_4;
	// System.String Sdkbox.IAP::androidKey
	String_t* ___androidKey_5;
	// System.Collections.Generic.List`1<Sdkbox.ProductDescription> Sdkbox.IAP::androidProducts
	List_1_t3603741248 * ___androidProducts_6;
	// Sdkbox.IAP/Callbacks Sdkbox.IAP::callbacks
	Callbacks_t1762710508 * ___callbacks_7;

public:
	inline static int32_t get_offset_of_iOSProducts_4() { return static_cast<int32_t>(offsetof(IAP_t1939458538, ___iOSProducts_4)); }
	inline List_1_t3603741248 * get_iOSProducts_4() const { return ___iOSProducts_4; }
	inline List_1_t3603741248 ** get_address_of_iOSProducts_4() { return &___iOSProducts_4; }
	inline void set_iOSProducts_4(List_1_t3603741248 * value)
	{
		___iOSProducts_4 = value;
		Il2CppCodeGenWriteBarrier((&___iOSProducts_4), value);
	}

	inline static int32_t get_offset_of_androidKey_5() { return static_cast<int32_t>(offsetof(IAP_t1939458538, ___androidKey_5)); }
	inline String_t* get_androidKey_5() const { return ___androidKey_5; }
	inline String_t** get_address_of_androidKey_5() { return &___androidKey_5; }
	inline void set_androidKey_5(String_t* value)
	{
		___androidKey_5 = value;
		Il2CppCodeGenWriteBarrier((&___androidKey_5), value);
	}

	inline static int32_t get_offset_of_androidProducts_6() { return static_cast<int32_t>(offsetof(IAP_t1939458538, ___androidProducts_6)); }
	inline List_1_t3603741248 * get_androidProducts_6() const { return ___androidProducts_6; }
	inline List_1_t3603741248 ** get_address_of_androidProducts_6() { return &___androidProducts_6; }
	inline void set_androidProducts_6(List_1_t3603741248 * value)
	{
		___androidProducts_6 = value;
		Il2CppCodeGenWriteBarrier((&___androidProducts_6), value);
	}

	inline static int32_t get_offset_of_callbacks_7() { return static_cast<int32_t>(offsetof(IAP_t1939458538, ___callbacks_7)); }
	inline Callbacks_t1762710508 * get_callbacks_7() const { return ___callbacks_7; }
	inline Callbacks_t1762710508 ** get_address_of_callbacks_7() { return &___callbacks_7; }
	inline void set_callbacks_7(Callbacks_t1762710508 * value)
	{
		___callbacks_7 = value;
		Il2CppCodeGenWriteBarrier((&___callbacks_7), value);
	}
};

struct IAP_t1939458538_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> Sdkbox.IAP::<>f__switch$map0
	Dictionary_2_t2736202052 * ___U3CU3Ef__switchU24map0_8;
	// Sdkbox.IAP/CallbackIAPDelegate Sdkbox.IAP::<>f__mg$cache0
	CallbackIAPDelegate_t957877347 * ___U3CU3Ef__mgU24cache0_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__switchU24map0_8() { return static_cast<int32_t>(offsetof(IAP_t1939458538_StaticFields, ___U3CU3Ef__switchU24map0_8)); }
	inline Dictionary_2_t2736202052 * get_U3CU3Ef__switchU24map0_8() const { return ___U3CU3Ef__switchU24map0_8; }
	inline Dictionary_2_t2736202052 ** get_address_of_U3CU3Ef__switchU24map0_8() { return &___U3CU3Ef__switchU24map0_8; }
	inline void set_U3CU3Ef__switchU24map0_8(Dictionary_2_t2736202052 * value)
	{
		___U3CU3Ef__switchU24map0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__switchU24map0_8), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_9() { return static_cast<int32_t>(offsetof(IAP_t1939458538_StaticFields, ___U3CU3Ef__mgU24cache0_9)); }
	inline CallbackIAPDelegate_t957877347 * get_U3CU3Ef__mgU24cache0_9() const { return ___U3CU3Ef__mgU24cache0_9; }
	inline CallbackIAPDelegate_t957877347 ** get_address_of_U3CU3Ef__mgU24cache0_9() { return &___U3CU3Ef__mgU24cache0_9; }
	inline void set_U3CU3Ef__mgU24cache0_9(CallbackIAPDelegate_t957877347 * value)
	{
		___U3CU3Ef__mgU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IAP_T1939458538_H
#ifndef REWARDPANEL_T4063177467_H
#define REWARDPANEL_T4063177467_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Views.RewardPanel
struct  RewardPanel_t4063177467  : public GenericPanel_t306775760
{
public:
	// TMPro.TextMeshProUGUI Outback.Views.RewardPanel::reward
	TextMeshProUGUI_t529313277 * ___reward_2;

public:
	inline static int32_t get_offset_of_reward_2() { return static_cast<int32_t>(offsetof(RewardPanel_t4063177467, ___reward_2)); }
	inline TextMeshProUGUI_t529313277 * get_reward_2() const { return ___reward_2; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_reward_2() { return &___reward_2; }
	inline void set_reward_2(TextMeshProUGUI_t529313277 * value)
	{
		___reward_2 = value;
		Il2CppCodeGenWriteBarrier((&___reward_2), value);
	}
};

struct RewardPanel_t4063177467_StaticFields
{
public:
	// Outback.Views.RewardPanel Outback.Views.RewardPanel::instance
	RewardPanel_t4063177467 * ___instance_3;

public:
	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(RewardPanel_t4063177467_StaticFields, ___instance_3)); }
	inline RewardPanel_t4063177467 * get_instance_3() const { return ___instance_3; }
	inline RewardPanel_t4063177467 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(RewardPanel_t4063177467 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWARDPANEL_T4063177467_H
#ifndef APPEVENTS_T3645639549_H
#define APPEVENTS_T3645639549_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AppEvents
struct  AppEvents_t3645639549  : public MenuBase_t1079888489
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPEVENTS_T3645639549_H
#ifndef APPINVITES_T2439373346_H
#define APPINVITES_T2439373346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AppInvites
struct  AppInvites_t2439373346  : public MenuBase_t1079888489
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPINVITES_T2439373346_H
#ifndef APPLINKS_T2028121612_H
#define APPLINKS_T2028121612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AppLinks
struct  AppLinks_t2028121612  : public MenuBase_t1079888489
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLINKS_T2028121612_H
#ifndef APPREQUESTS_T2419817778_H
#define APPREQUESTS_T2419817778_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AppRequests
struct  AppRequests_t2419817778  : public MenuBase_t1079888489
{
public:
	// System.String Facebook.Unity.Example.AppRequests::requestMessage
	String_t* ___requestMessage_14;
	// System.String Facebook.Unity.Example.AppRequests::requestTo
	String_t* ___requestTo_15;
	// System.String Facebook.Unity.Example.AppRequests::requestFilter
	String_t* ___requestFilter_16;
	// System.String Facebook.Unity.Example.AppRequests::requestExcludes
	String_t* ___requestExcludes_17;
	// System.String Facebook.Unity.Example.AppRequests::requestMax
	String_t* ___requestMax_18;
	// System.String Facebook.Unity.Example.AppRequests::requestData
	String_t* ___requestData_19;
	// System.String Facebook.Unity.Example.AppRequests::requestTitle
	String_t* ___requestTitle_20;
	// System.String Facebook.Unity.Example.AppRequests::requestObjectID
	String_t* ___requestObjectID_21;
	// System.Int32 Facebook.Unity.Example.AppRequests::selectedAction
	int32_t ___selectedAction_22;
	// System.String[] Facebook.Unity.Example.AppRequests::actionTypeStrings
	StringU5BU5D_t1281789340* ___actionTypeStrings_23;

public:
	inline static int32_t get_offset_of_requestMessage_14() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestMessage_14)); }
	inline String_t* get_requestMessage_14() const { return ___requestMessage_14; }
	inline String_t** get_address_of_requestMessage_14() { return &___requestMessage_14; }
	inline void set_requestMessage_14(String_t* value)
	{
		___requestMessage_14 = value;
		Il2CppCodeGenWriteBarrier((&___requestMessage_14), value);
	}

	inline static int32_t get_offset_of_requestTo_15() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestTo_15)); }
	inline String_t* get_requestTo_15() const { return ___requestTo_15; }
	inline String_t** get_address_of_requestTo_15() { return &___requestTo_15; }
	inline void set_requestTo_15(String_t* value)
	{
		___requestTo_15 = value;
		Il2CppCodeGenWriteBarrier((&___requestTo_15), value);
	}

	inline static int32_t get_offset_of_requestFilter_16() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestFilter_16)); }
	inline String_t* get_requestFilter_16() const { return ___requestFilter_16; }
	inline String_t** get_address_of_requestFilter_16() { return &___requestFilter_16; }
	inline void set_requestFilter_16(String_t* value)
	{
		___requestFilter_16 = value;
		Il2CppCodeGenWriteBarrier((&___requestFilter_16), value);
	}

	inline static int32_t get_offset_of_requestExcludes_17() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestExcludes_17)); }
	inline String_t* get_requestExcludes_17() const { return ___requestExcludes_17; }
	inline String_t** get_address_of_requestExcludes_17() { return &___requestExcludes_17; }
	inline void set_requestExcludes_17(String_t* value)
	{
		___requestExcludes_17 = value;
		Il2CppCodeGenWriteBarrier((&___requestExcludes_17), value);
	}

	inline static int32_t get_offset_of_requestMax_18() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestMax_18)); }
	inline String_t* get_requestMax_18() const { return ___requestMax_18; }
	inline String_t** get_address_of_requestMax_18() { return &___requestMax_18; }
	inline void set_requestMax_18(String_t* value)
	{
		___requestMax_18 = value;
		Il2CppCodeGenWriteBarrier((&___requestMax_18), value);
	}

	inline static int32_t get_offset_of_requestData_19() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestData_19)); }
	inline String_t* get_requestData_19() const { return ___requestData_19; }
	inline String_t** get_address_of_requestData_19() { return &___requestData_19; }
	inline void set_requestData_19(String_t* value)
	{
		___requestData_19 = value;
		Il2CppCodeGenWriteBarrier((&___requestData_19), value);
	}

	inline static int32_t get_offset_of_requestTitle_20() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestTitle_20)); }
	inline String_t* get_requestTitle_20() const { return ___requestTitle_20; }
	inline String_t** get_address_of_requestTitle_20() { return &___requestTitle_20; }
	inline void set_requestTitle_20(String_t* value)
	{
		___requestTitle_20 = value;
		Il2CppCodeGenWriteBarrier((&___requestTitle_20), value);
	}

	inline static int32_t get_offset_of_requestObjectID_21() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___requestObjectID_21)); }
	inline String_t* get_requestObjectID_21() const { return ___requestObjectID_21; }
	inline String_t** get_address_of_requestObjectID_21() { return &___requestObjectID_21; }
	inline void set_requestObjectID_21(String_t* value)
	{
		___requestObjectID_21 = value;
		Il2CppCodeGenWriteBarrier((&___requestObjectID_21), value);
	}

	inline static int32_t get_offset_of_selectedAction_22() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___selectedAction_22)); }
	inline int32_t get_selectedAction_22() const { return ___selectedAction_22; }
	inline int32_t* get_address_of_selectedAction_22() { return &___selectedAction_22; }
	inline void set_selectedAction_22(int32_t value)
	{
		___selectedAction_22 = value;
	}

	inline static int32_t get_offset_of_actionTypeStrings_23() { return static_cast<int32_t>(offsetof(AppRequests_t2419817778, ___actionTypeStrings_23)); }
	inline StringU5BU5D_t1281789340* get_actionTypeStrings_23() const { return ___actionTypeStrings_23; }
	inline StringU5BU5D_t1281789340** get_address_of_actionTypeStrings_23() { return &___actionTypeStrings_23; }
	inline void set_actionTypeStrings_23(StringU5BU5D_t1281789340* value)
	{
		___actionTypeStrings_23 = value;
		Il2CppCodeGenWriteBarrier((&___actionTypeStrings_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPREQUESTS_T2419817778_H
#ifndef DIALOGSHARE_T2334043085_H
#define DIALOGSHARE_T2334043085_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.DialogShare
struct  DialogShare_t2334043085  : public MenuBase_t1079888489
{
public:
	// System.String Facebook.Unity.Example.DialogShare::shareLink
	String_t* ___shareLink_14;
	// System.String Facebook.Unity.Example.DialogShare::shareTitle
	String_t* ___shareTitle_15;
	// System.String Facebook.Unity.Example.DialogShare::shareDescription
	String_t* ___shareDescription_16;
	// System.String Facebook.Unity.Example.DialogShare::shareImage
	String_t* ___shareImage_17;
	// System.String Facebook.Unity.Example.DialogShare::feedTo
	String_t* ___feedTo_18;
	// System.String Facebook.Unity.Example.DialogShare::feedLink
	String_t* ___feedLink_19;
	// System.String Facebook.Unity.Example.DialogShare::feedTitle
	String_t* ___feedTitle_20;
	// System.String Facebook.Unity.Example.DialogShare::feedCaption
	String_t* ___feedCaption_21;
	// System.String Facebook.Unity.Example.DialogShare::feedDescription
	String_t* ___feedDescription_22;
	// System.String Facebook.Unity.Example.DialogShare::feedImage
	String_t* ___feedImage_23;
	// System.String Facebook.Unity.Example.DialogShare::feedMediaSource
	String_t* ___feedMediaSource_24;

public:
	inline static int32_t get_offset_of_shareLink_14() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___shareLink_14)); }
	inline String_t* get_shareLink_14() const { return ___shareLink_14; }
	inline String_t** get_address_of_shareLink_14() { return &___shareLink_14; }
	inline void set_shareLink_14(String_t* value)
	{
		___shareLink_14 = value;
		Il2CppCodeGenWriteBarrier((&___shareLink_14), value);
	}

	inline static int32_t get_offset_of_shareTitle_15() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___shareTitle_15)); }
	inline String_t* get_shareTitle_15() const { return ___shareTitle_15; }
	inline String_t** get_address_of_shareTitle_15() { return &___shareTitle_15; }
	inline void set_shareTitle_15(String_t* value)
	{
		___shareTitle_15 = value;
		Il2CppCodeGenWriteBarrier((&___shareTitle_15), value);
	}

	inline static int32_t get_offset_of_shareDescription_16() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___shareDescription_16)); }
	inline String_t* get_shareDescription_16() const { return ___shareDescription_16; }
	inline String_t** get_address_of_shareDescription_16() { return &___shareDescription_16; }
	inline void set_shareDescription_16(String_t* value)
	{
		___shareDescription_16 = value;
		Il2CppCodeGenWriteBarrier((&___shareDescription_16), value);
	}

	inline static int32_t get_offset_of_shareImage_17() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___shareImage_17)); }
	inline String_t* get_shareImage_17() const { return ___shareImage_17; }
	inline String_t** get_address_of_shareImage_17() { return &___shareImage_17; }
	inline void set_shareImage_17(String_t* value)
	{
		___shareImage_17 = value;
		Il2CppCodeGenWriteBarrier((&___shareImage_17), value);
	}

	inline static int32_t get_offset_of_feedTo_18() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___feedTo_18)); }
	inline String_t* get_feedTo_18() const { return ___feedTo_18; }
	inline String_t** get_address_of_feedTo_18() { return &___feedTo_18; }
	inline void set_feedTo_18(String_t* value)
	{
		___feedTo_18 = value;
		Il2CppCodeGenWriteBarrier((&___feedTo_18), value);
	}

	inline static int32_t get_offset_of_feedLink_19() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___feedLink_19)); }
	inline String_t* get_feedLink_19() const { return ___feedLink_19; }
	inline String_t** get_address_of_feedLink_19() { return &___feedLink_19; }
	inline void set_feedLink_19(String_t* value)
	{
		___feedLink_19 = value;
		Il2CppCodeGenWriteBarrier((&___feedLink_19), value);
	}

	inline static int32_t get_offset_of_feedTitle_20() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___feedTitle_20)); }
	inline String_t* get_feedTitle_20() const { return ___feedTitle_20; }
	inline String_t** get_address_of_feedTitle_20() { return &___feedTitle_20; }
	inline void set_feedTitle_20(String_t* value)
	{
		___feedTitle_20 = value;
		Il2CppCodeGenWriteBarrier((&___feedTitle_20), value);
	}

	inline static int32_t get_offset_of_feedCaption_21() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___feedCaption_21)); }
	inline String_t* get_feedCaption_21() const { return ___feedCaption_21; }
	inline String_t** get_address_of_feedCaption_21() { return &___feedCaption_21; }
	inline void set_feedCaption_21(String_t* value)
	{
		___feedCaption_21 = value;
		Il2CppCodeGenWriteBarrier((&___feedCaption_21), value);
	}

	inline static int32_t get_offset_of_feedDescription_22() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___feedDescription_22)); }
	inline String_t* get_feedDescription_22() const { return ___feedDescription_22; }
	inline String_t** get_address_of_feedDescription_22() { return &___feedDescription_22; }
	inline void set_feedDescription_22(String_t* value)
	{
		___feedDescription_22 = value;
		Il2CppCodeGenWriteBarrier((&___feedDescription_22), value);
	}

	inline static int32_t get_offset_of_feedImage_23() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___feedImage_23)); }
	inline String_t* get_feedImage_23() const { return ___feedImage_23; }
	inline String_t** get_address_of_feedImage_23() { return &___feedImage_23; }
	inline void set_feedImage_23(String_t* value)
	{
		___feedImage_23 = value;
		Il2CppCodeGenWriteBarrier((&___feedImage_23), value);
	}

	inline static int32_t get_offset_of_feedMediaSource_24() { return static_cast<int32_t>(offsetof(DialogShare_t2334043085, ___feedMediaSource_24)); }
	inline String_t* get_feedMediaSource_24() const { return ___feedMediaSource_24; }
	inline String_t** get_address_of_feedMediaSource_24() { return &___feedMediaSource_24; }
	inline void set_feedMediaSource_24(String_t* value)
	{
		___feedMediaSource_24 = value;
		Il2CppCodeGenWriteBarrier((&___feedMediaSource_24), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIALOGSHARE_T2334043085_H
#ifndef GRAPHREQUEST_T4047451309_H
#define GRAPHREQUEST_T4047451309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.GraphRequest
struct  GraphRequest_t4047451309  : public MenuBase_t1079888489
{
public:
	// System.String Facebook.Unity.Example.GraphRequest::apiQuery
	String_t* ___apiQuery_14;
	// UnityEngine.Texture2D Facebook.Unity.Example.GraphRequest::profilePic
	Texture2D_t3840446185 * ___profilePic_15;

public:
	inline static int32_t get_offset_of_apiQuery_14() { return static_cast<int32_t>(offsetof(GraphRequest_t4047451309, ___apiQuery_14)); }
	inline String_t* get_apiQuery_14() const { return ___apiQuery_14; }
	inline String_t** get_address_of_apiQuery_14() { return &___apiQuery_14; }
	inline void set_apiQuery_14(String_t* value)
	{
		___apiQuery_14 = value;
		Il2CppCodeGenWriteBarrier((&___apiQuery_14), value);
	}

	inline static int32_t get_offset_of_profilePic_15() { return static_cast<int32_t>(offsetof(GraphRequest_t4047451309, ___profilePic_15)); }
	inline Texture2D_t3840446185 * get_profilePic_15() const { return ___profilePic_15; }
	inline Texture2D_t3840446185 ** get_address_of_profilePic_15() { return &___profilePic_15; }
	inline void set_profilePic_15(Texture2D_t3840446185 * value)
	{
		___profilePic_15 = value;
		Il2CppCodeGenWriteBarrier((&___profilePic_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHREQUEST_T4047451309_H
#ifndef MAINMENU_T1823058806_H
#define MAINMENU_T1823058806_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.MainMenu
struct  MainMenu_t1823058806  : public MenuBase_t1079888489
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MAINMENU_T1823058806_H
#ifndef ACCESSTOKENMENU_T4028641200_H
#define ACCESSTOKENMENU_T4028641200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.AccessTokenMenu
struct  AccessTokenMenu_t4028641200  : public MenuBase_t1079888489
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCESSTOKENMENU_T4028641200_H
#ifndef PAY_T1264260185_H
#define PAY_T1264260185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Example.Pay
struct  Pay_t1264260185  : public MenuBase_t1079888489
{
public:
	// System.String Facebook.Unity.Example.Pay::payProduct
	String_t* ___payProduct_14;

public:
	inline static int32_t get_offset_of_payProduct_14() { return static_cast<int32_t>(offsetof(Pay_t1264260185, ___payProduct_14)); }
	inline String_t* get_payProduct_14() const { return ___payProduct_14; }
	inline String_t** get_address_of_payProduct_14() { return &___payProduct_14; }
	inline void set_payProduct_14(String_t* value)
	{
		___payProduct_14 = value;
		Il2CppCodeGenWriteBarrier((&___payProduct_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAY_T1264260185_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2500 = { sizeof (TimeManager_t1960693005), -1, sizeof(TimeManager_t1960693005_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2500[5] = 
{
	TimeManager_t1960693005_StaticFields::get_offset_of_sharedInstance_2(),
	TimeManager_t1960693005::get_offset_of__url_3(),
	TimeManager_t1960693005::get_offset_of__timeData_4(),
	TimeManager_t1960693005::get_offset_of__currentTime_5(),
	TimeManager_t1960693005::get_offset_of__currentDate_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2501 = { sizeof (U3CGetTimeU3Ec__Iterator0_t4198639438), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2501[5] = 
{
	U3CGetTimeU3Ec__Iterator0_t4198639438::get_offset_of_U3CwwwU3E__0_0(),
	U3CGetTimeU3Ec__Iterator0_t4198639438::get_offset_of_U24this_1(),
	U3CGetTimeU3Ec__Iterator0_t4198639438::get_offset_of_U24current_2(),
	U3CGetTimeU3Ec__Iterator0_t4198639438::get_offset_of_U24disposing_3(),
	U3CGetTimeU3Ec__Iterator0_t4198639438::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2502 = { sizeof (BetterArray_t1983019263), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2503 = { sizeof (ScrollType_t1869506883)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2503[3] = 
{
	ScrollType_t1869506883::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2504 = { sizeof (BetterList_t163950454), -1, sizeof(BetterList_t163950454_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2504[1] = 
{
	BetterList_t163950454_StaticFields::get_offset_of_scrollContent_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2505 = { sizeof (MiniJsonXT_t2464555382), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2506 = { sizeof (JSON_t3924998764), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2507 = { sizeof (Parser_t3319173113), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2507[2] = 
{
	0,
	Parser_t3319173113::get_offset_of_json_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2508 = { sizeof (TOKEN_t3312601886)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2508[13] = 
{
	TOKEN_t3312601886::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2509 = { sizeof (Serializer_t3711094893), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2509[1] = 
{
	Serializer_t3711094893::get_offset_of_builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2510 = { sizeof (TextFormatter_t3711854288), -1, sizeof(TextFormatter_t3711854288_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2510[1] = 
{
	TextFormatter_t3711854288_StaticFields::get_offset_of_NumberFormatters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2511 = { sizeof (U3CGetFormmatedNumberTextWithLongNameU3Ec__AnonStorey0_t4053947336), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2511[1] = 
{
	U3CGetFormmatedNumberTextWithLongNameU3Ec__AnonStorey0_t4053947336::get_offset_of_number_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2512 = { sizeof (U3CGetFormattedNumberTextU3Ec__AnonStorey1_t2026566151), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2512[1] = 
{
	U3CGetFormattedNumberTextU3Ec__AnonStorey1_t2026566151::get_offset_of_number_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2513 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2513[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2514 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2514[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2515 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2515[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2516 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2516[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2517 = { sizeof (Tuple_t2651870793), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2518 = { sizeof (UnityEditorPopup_t2304967859), -1, sizeof(UnityEditorPopup_t2304967859_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2518[5] = 
{
	UnityEditorPopup_t2304967859::get_offset_of_btns_2(),
	UnityEditorPopup_t2304967859::get_offset_of_texts_3(),
	UnityEditorPopup_t2304967859::get_offset_of_t_4(),
	UnityEditorPopup_t2304967859::get_offset_of_msg_5(),
	UnityEditorPopup_t2304967859_StaticFields::get_offset_of_instance_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2519 = { sizeof (U3CShowPopupU3Ec__AnonStorey0_t938638323), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2519[1] = 
{
	U3CShowPopupU3Ec__AnonStorey0_t938638323::get_offset_of_action_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2520 = { sizeof (U3CShowPopupU3Ec__AnonStorey1_t3277290483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2520[2] = 
{
	U3CShowPopupU3Ec__AnonStorey1_t3277290483::get_offset_of_a_0(),
	U3CShowPopupU3Ec__AnonStorey1_t3277290483::get_offset_of_U3CU3Ef__refU240_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2521 = { sizeof (ErrorPanel_t2568969857), -1, sizeof(ErrorPanel_t2568969857_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2521[4] = 
{
	ErrorPanel_t2568969857::get_offset_of_heading_2(),
	ErrorPanel_t2568969857::get_offset_of_message_3(),
	ErrorPanel_t2568969857::get_offset_of_exitApp_4(),
	ErrorPanel_t2568969857_StaticFields::get_offset_of_instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2522 = { sizeof (GenericPanel_t306775760), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2523 = { sizeof (RewardPanel_t4063177467), -1, sizeof(RewardPanel_t4063177467_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2523[2] = 
{
	RewardPanel_t4063177467::get_offset_of_reward_2(),
	RewardPanel_t4063177467_StaticFields::get_offset_of_instance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2524 = { sizeof (U3CShowMessageU3Ec__AnonStorey0_t1415458573), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2524[4] = 
{
	U3CShowMessageU3Ec__AnonStorey0_t1415458573::get_offset_of_a_0(),
	U3CShowMessageU3Ec__AnonStorey0_t1415458573::get_offset_of_startValue_1(),
	U3CShowMessageU3Ec__AnonStorey0_t1415458573::get_offset_of_endValue_2(),
	U3CShowMessageU3Ec__AnonStorey0_t1415458573::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2525 = { sizeof (U3CIncrementValueAndCloseU3Ec__AnonStorey1_t2550267275), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2525[2] = 
{
	U3CIncrementValueAndCloseU3Ec__AnonStorey1_t2550267275::get_offset_of_a_0(),
	U3CIncrementValueAndCloseU3Ec__AnonStorey1_t2550267275::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2526 = { sizeof (IABWrapper_t882725550), -1, sizeof(IABWrapper_t882725550_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2526[1] = 
{
	IABWrapper_t882725550_StaticFields::get_offset_of_instance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2527 = { sizeof (SpriteManager_t361410013), -1, sizeof(SpriteManager_t361410013_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2527[16] = 
{
	SpriteManager_t361410013_StaticFields::get_offset_of_instance_2(),
	SpriteManager_t361410013_StaticFields::get_offset_of__transparentImage_3(),
	SpriteManager_t361410013::get_offset_of_currencySprites_4(),
	SpriteManager_t361410013::get_offset_of_selectedtab_5(),
	SpriteManager_t361410013::get_offset_of_nonselectedtab_6(),
	SpriteManager_t361410013::get_offset_of_spriteCoin1_7(),
	SpriteManager_t361410013::get_offset_of_spriteCoin2_8(),
	SpriteManager_t361410013::get_offset_of_spriteCoin5_9(),
	SpriteManager_t361410013::get_offset_of_spriteCoin10_10(),
	SpriteManager_t361410013::get_offset_of_cardBackSpriteRed_11(),
	SpriteManager_t361410013::get_offset_of_cardBackSpriteBlue_12(),
	SpriteManager_t361410013::get_offset_of_jokerSprite_13(),
	SpriteManager_t361410013::get_offset_of_cardImages_14(),
	SpriteManager_t361410013::get_offset_of_connectingSprite_15(),
	SpriteManager_t361410013::get_offset_of_connectionLostSprite_16(),
	SpriteManager_t361410013::get_offset_of_namedSprites_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2528 = { sizeof (NamedSprite_t2698853217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2528[2] = 
{
	NamedSprite_t2698853217::get_offset_of_name_0(),
	NamedSprite_t2698853217::get_offset_of_sprite_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2529 = { sizeof (Basics_t653963782), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2529[2] = 
{
	Basics_t653963782::get_offset_of_cubeA_2(),
	Basics_t653963782::get_offset_of_cubeB_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2530 = { sizeof (Sequences_t3991711780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2530[1] = 
{
	Sequences_t3991711780::get_offset_of_target_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2531 = { sizeof (ConsoleBase_t1023975560), -1, sizeof(ConsoleBase_t1023975560_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2531[11] = 
{
	0,
	ConsoleBase_t1023975560_StaticFields::get_offset_of_menuStack_3(),
	ConsoleBase_t1023975560::get_offset_of_status_4(),
	ConsoleBase_t1023975560::get_offset_of_lastResponse_5(),
	ConsoleBase_t1023975560::get_offset_of_scrollPosition_6(),
	ConsoleBase_t1023975560::get_offset_of_scaleFactor_7(),
	ConsoleBase_t1023975560::get_offset_of_textStyle_8(),
	ConsoleBase_t1023975560::get_offset_of_buttonStyle_9(),
	ConsoleBase_t1023975560::get_offset_of_textInputStyle_10(),
	ConsoleBase_t1023975560::get_offset_of_labelStyle_11(),
	ConsoleBase_t1023975560::get_offset_of_U3CLastResponseTextureU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2532 = { sizeof (LogView_t1067263371), -1, sizeof(LogView_t1067263371_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2532[2] = 
{
	LogView_t1067263371_StaticFields::get_offset_of_datePatt_13(),
	LogView_t1067263371_StaticFields::get_offset_of_events_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2533 = { sizeof (MenuBase_t1079888489), -1, sizeof(MenuBase_t1079888489_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2533[1] = 
{
	MenuBase_t1079888489_StaticFields::get_offset_of_shareDialogMode_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2534 = { sizeof (AccessTokenMenu_t4028641200), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2535 = { sizeof (AppEvents_t3645639549), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2536 = { sizeof (AppInvites_t2439373346), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2537 = { sizeof (AppLinks_t2028121612), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2538 = { sizeof (AppRequests_t2419817778), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2538[10] = 
{
	AppRequests_t2419817778::get_offset_of_requestMessage_14(),
	AppRequests_t2419817778::get_offset_of_requestTo_15(),
	AppRequests_t2419817778::get_offset_of_requestFilter_16(),
	AppRequests_t2419817778::get_offset_of_requestExcludes_17(),
	AppRequests_t2419817778::get_offset_of_requestMax_18(),
	AppRequests_t2419817778::get_offset_of_requestData_19(),
	AppRequests_t2419817778::get_offset_of_requestTitle_20(),
	AppRequests_t2419817778::get_offset_of_requestObjectID_21(),
	AppRequests_t2419817778::get_offset_of_selectedAction_22(),
	AppRequests_t2419817778::get_offset_of_actionTypeStrings_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2539 = { sizeof (DialogShare_t2334043085), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2539[11] = 
{
	DialogShare_t2334043085::get_offset_of_shareLink_14(),
	DialogShare_t2334043085::get_offset_of_shareTitle_15(),
	DialogShare_t2334043085::get_offset_of_shareDescription_16(),
	DialogShare_t2334043085::get_offset_of_shareImage_17(),
	DialogShare_t2334043085::get_offset_of_feedTo_18(),
	DialogShare_t2334043085::get_offset_of_feedLink_19(),
	DialogShare_t2334043085::get_offset_of_feedTitle_20(),
	DialogShare_t2334043085::get_offset_of_feedCaption_21(),
	DialogShare_t2334043085::get_offset_of_feedDescription_22(),
	DialogShare_t2334043085::get_offset_of_feedImage_23(),
	DialogShare_t2334043085::get_offset_of_feedMediaSource_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2540 = { sizeof (GraphRequest_t4047451309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2540[2] = 
{
	GraphRequest_t4047451309::get_offset_of_apiQuery_14(),
	GraphRequest_t4047451309::get_offset_of_profilePic_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2541 = { sizeof (U3CTakeScreenshotU3Ec__Iterator0_t3544038915), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2541[9] = 
{
	U3CTakeScreenshotU3Ec__Iterator0_t3544038915::get_offset_of_U3CwidthU3E__0_0(),
	U3CTakeScreenshotU3Ec__Iterator0_t3544038915::get_offset_of_U3CheightU3E__0_1(),
	U3CTakeScreenshotU3Ec__Iterator0_t3544038915::get_offset_of_U3CtexU3E__0_2(),
	U3CTakeScreenshotU3Ec__Iterator0_t3544038915::get_offset_of_U3CscreenshotU3E__0_3(),
	U3CTakeScreenshotU3Ec__Iterator0_t3544038915::get_offset_of_U3CwwwFormU3E__0_4(),
	U3CTakeScreenshotU3Ec__Iterator0_t3544038915::get_offset_of_U24this_5(),
	U3CTakeScreenshotU3Ec__Iterator0_t3544038915::get_offset_of_U24current_6(),
	U3CTakeScreenshotU3Ec__Iterator0_t3544038915::get_offset_of_U24disposing_7(),
	U3CTakeScreenshotU3Ec__Iterator0_t3544038915::get_offset_of_U24PC_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2542 = { sizeof (MainMenu_t1823058806), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2543 = { sizeof (Pay_t1264260185), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2543[1] = 
{
	Pay_t1264260185::get_offset_of_payProduct_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2544 = { sizeof (TestScript_t3771403385), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2544[1] = 
{
	TestScript_t3771403385::get_offset_of_sprites_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2545 = { sizeof (CameraTarget_t1265245093), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2545[1] = 
{
	CameraTarget_t1265245093::get_offset_of_m_TargetOffset_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2546 = { sizeof (EffectData_t1709847781), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2546[10] = 
{
	EffectData_t1709847781::get_offset_of_m_bFoldoutOpen_0(),
	EffectData_t1709847781::get_offset_of_m_fTimeSec_1(),
	EffectData_t1709847781::get_offset_of_m_goEffect_2(),
	EffectData_t1709847781::get_offset_of_m_bTransformFoldout_3(),
	EffectData_t1709847781::get_offset_of_m_goPos_4(),
	EffectData_t1709847781::get_offset_of_m_goRotation_5(),
	EffectData_t1709847781::get_offset_of_m_goScale_6(),
	EffectData_t1709847781::get_offset_of_m_bSortingFoldout_7(),
	EffectData_t1709847781::get_offset_of_m_SortingLayerID_8(),
	EffectData_t1709847781::get_offset_of_m_SortingOrder_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2547 = { sizeof (EffectController_t216029290), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2547[4] = 
{
	EffectController_t216029290::get_offset_of_m_nNumOfEffects_2(),
	EffectController_t216029290::get_offset_of_m_bLockNums_3(),
	EffectController_t216029290::get_offset_of_m_kEffectGenList_4(),
	EffectController_t216029290::get_offset_of_m_nNowIndex_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2548 = { sizeof (Comp_t2934735840), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2549 = { sizeof (EffectDemo_t2474128590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2549[6] = 
{
	0,
	EffectDemo_t2474128590::get_offset_of_m_EffectPrefabList_3(),
	EffectDemo_t2474128590::get_offset_of_m_LookAtEffect_4(),
	EffectDemo_t2474128590::get_offset_of_m_NowShowEffect_5(),
	EffectDemo_t2474128590::get_offset_of_m_NowIndex_6(),
	EffectDemo_t2474128590::get_offset_of_m_NowEffectName_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2550 = { sizeof (EffectShaderPropertyStr_t3661917582), -1, sizeof(EffectShaderPropertyStr_t3661917582_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2550[36] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	EffectShaderPropertyStr_t3661917582_StaticFields::get_offset_of_Material_Color_34(),
	EffectShaderPropertyStr_t3661917582_StaticFields::get_offset_of_Material_Color_Factor_35(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2551 = { sizeof (RenderBillBoardType_t3907887595)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2551[4] = 
{
	RenderBillBoardType_t3907887595::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2552 = { sizeof (MaterialEffect_t3892150953), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2552[11] = 
{
	MaterialEffect_t3892150953::get_offset_of_m_EffectMaterial_0(),
	MaterialEffect_t3892150953::get_offset_of_m_EnableAlphaAnimation_1(),
	MaterialEffect_t3892150953::get_offset_of_m_AlphaAnimationTimeScale_2(),
	MaterialEffect_t3892150953::get_offset_of_m_AlphaCurve_3(),
	MaterialEffect_t3892150953::get_offset_of_m_MainTexture_4(),
	MaterialEffect_t3892150953::get_offset_of_m_MaskTexutre_5(),
	MaterialEffect_t3892150953::get_offset_of_m_MainTexWrapMode_6(),
	MaterialEffect_t3892150953::get_offset_of_m_MaskTexWrapMode_7(),
	MaterialEffect_t3892150953::get_offset_of_m_EnableUVScroll_8(),
	MaterialEffect_t3892150953::get_offset_of_m_UVScrollMainTex_9(),
	MaterialEffect_t3892150953::get_offset_of_m_UVScrollCutTex_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2553 = { sizeof (RenderEffect_t2584084283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2553[9] = 
{
	RenderEffect_t2584084283::get_offset_of_m_BillBoardType_2(),
	RenderEffect_t2584084283::get_offset_of_m_ReferenceCamera_3(),
	RenderEffect_t2584084283::get_offset_of_m_EnableBillBoard_4(),
	RenderEffect_t2584084283::get_offset_of_m_EnableSetSortLayer_5(),
	RenderEffect_t2584084283::get_offset_of_m_Render_6(),
	RenderEffect_t2584084283::get_offset_of_m_MaterialEffects_7(),
	RenderEffect_t2584084283::get_offset_of_m_TimeLine_8(),
	RenderEffect_t2584084283::get_offset_of_m_SortingLayerID_9(),
	RenderEffect_t2584084283::get_offset_of_m_SortingOrder_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2554 = { sizeof (TransformExtension_t4238110091), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2555 = { sizeof (ProgressBarBehaviour_t3447880169), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2555[9] = 
{
	ProgressBarBehaviour_t3447880169::get_offset_of_m_FillRect_2(),
	ProgressBarBehaviour_t3447880169::get_offset_of_m_FillerInfo_3(),
	ProgressBarBehaviour_t3447880169::get_offset_of_m_Value_4(),
	ProgressBarBehaviour_t3447880169::get_offset_of_U3CTransitoryValueU3Ek__BackingField_5(),
	ProgressBarBehaviour_t3447880169::get_offset_of_m_AttachedText_6(),
	ProgressBarBehaviour_t3447880169::get_offset_of_ProgressSpeed_7(),
	ProgressBarBehaviour_t3447880169::get_offset_of_TriggerOnComplete_8(),
	ProgressBarBehaviour_t3447880169::get_offset_of_OnCompleteMethods_9(),
	ProgressBarBehaviour_t3447880169::get_offset_of_m_XOffset_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2556 = { sizeof (ProgressRadialBehaviour_t4010907734), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2556[7] = 
{
	ProgressRadialBehaviour_t4010907734::get_offset_of_m_Fill_2(),
	ProgressRadialBehaviour_t4010907734::get_offset_of_m_Value_3(),
	ProgressRadialBehaviour_t4010907734::get_offset_of_U3CTransitoryValueU3Ek__BackingField_4(),
	ProgressRadialBehaviour_t4010907734::get_offset_of_m_AttachedText_5(),
	ProgressRadialBehaviour_t4010907734::get_offset_of_ProgressSpeed_6(),
	ProgressRadialBehaviour_t4010907734::get_offset_of_TriggerOnComplete_7(),
	ProgressRadialBehaviour_t4010907734::get_offset_of_OnCompleteMethods_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2557 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2558 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2559 = { sizeof (OnCompleteEvent_t1779196559), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2560 = { sizeof (FillerProperty_t1637340060), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2560[2] = 
{
	FillerProperty_t1637340060::get_offset_of_MaxWidth_0(),
	FillerProperty_t1637340060::get_offset_of_MinWidth_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2561 = { sizeof (ProgressValue_t1710479104), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2561[2] = 
{
	ProgressValue_t1710479104::get_offset_of_m_Value_0(),
	ProgressValue_t1710479104::get_offset_of_m_MaxValue_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2562 = { sizeof (ProductDescription_t2131666506)+ sizeof (RuntimeObject), sizeof(ProductDescription_t2131666506_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2562[3] = 
{
	ProductDescription_t2131666506::get_offset_of_name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProductDescription_t2131666506::get_offset_of_id_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ProductDescription_t2131666506::get_offset_of_consumable_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2563 = { sizeof (Product_t851415281)+ sizeof (RuntimeObject), sizeof(Product_t851415281_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2563[10] = 
{
	Product_t851415281::get_offset_of_name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Product_t851415281::get_offset_of_id_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Product_t851415281::get_offset_of_type_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Product_t851415281::get_offset_of_title_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Product_t851415281::get_offset_of_description_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Product_t851415281::get_offset_of_priceValue_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Product_t851415281::get_offset_of_price_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Product_t851415281::get_offset_of_currencyCode_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Product_t851415281::get_offset_of_receiptCipheredPayload_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Product_t851415281::get_offset_of_receipt_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2564 = { sizeof (Type_t309803420)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2564[3] = 
{
	Type_t309803420::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2565 = { sizeof (IAP_t1939458538), -1, sizeof(IAP_t1939458538_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2565[6] = 
{
	IAP_t1939458538::get_offset_of_iOSProducts_4(),
	IAP_t1939458538::get_offset_of_androidKey_5(),
	IAP_t1939458538::get_offset_of_androidProducts_6(),
	IAP_t1939458538::get_offset_of_callbacks_7(),
	IAP_t1939458538_StaticFields::get_offset_of_U3CU3Ef__switchU24map0_8(),
	IAP_t1939458538_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2566 = { sizeof (Callbacks_t1762710508), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2566[8] = 
{
	Callbacks_t1762710508::get_offset_of_onInitialized_0(),
	Callbacks_t1762710508::get_offset_of_onSuccess_1(),
	Callbacks_t1762710508::get_offset_of_onFailure_2(),
	Callbacks_t1762710508::get_offset_of_onCanceled_3(),
	Callbacks_t1762710508::get_offset_of_onRestored_4(),
	Callbacks_t1762710508::get_offset_of_onProductRequestSuccess_5(),
	Callbacks_t1762710508::get_offset_of_onProductRequestFailure_6(),
	Callbacks_t1762710508::get_offset_of_onRestoreComplete_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2567 = { sizeof (BoolEvent_t3697584372), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2568 = { sizeof (BoolStringEvent_t1829059983), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2569 = { sizeof (ProductEvent_t4069975844), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2570 = { sizeof (ProductStringEvent_t801016626), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2571 = { sizeof (ProductArrayEvent_t1714183732), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2572 = { sizeof (StringEvent_t4181969553), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2573 = { sizeof (CallbackIAPDelegate_t957877347), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2574 = { sizeof (PurchaseHandler_t2101448223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2574[4] = 
{
	PurchaseHandler_t2101448223::get_offset_of_messageText_2(),
	PurchaseHandler_t2101448223::get_offset_of__iap_3(),
	PurchaseHandler_t2101448223::get_offset_of_mLogBuffer_4(),
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2575 = { sizeof (Json_t1678423215), -1, sizeof(Json_t1678423215_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2575[7] = 
{
	Json_t1678423215::get_offset_of__type_0(),
	Json_t1678423215::get_offset_of__d_1(),
	Json_t1678423215::get_offset_of__b_2(),
	Json_t1678423215::get_offset_of__s_3(),
	Json_t1678423215::get_offset_of__a_4(),
	Json_t1678423215::get_offset_of__o_5(),
	Json_t1678423215_StaticFields::get_offset_of_null_json_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2576 = { sizeof (Type_t216879902)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2576[7] = 
{
	Type_t216879902::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2577 = { sizeof (JsonParser_t1403359166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2577[6] = 
{
	JsonParser_t1403359166::get_offset_of_MAX_DEPTH_0(),
	JsonParser_t1403359166::get_offset_of_MAX_DIGITS_1(),
	JsonParser_t1403359166::get_offset_of_str_2(),
	JsonParser_t1403359166::get_offset_of_failed_3(),
	JsonParser_t1403359166::get_offset_of_i_4(),
	JsonParser_t1403359166::get_offset_of_err_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2578 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2578[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2579 = { sizeof (SDKBOX_t1939795462), -1, sizeof(SDKBOX_t1939795462_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2579[1] = 
{
	SDKBOX_t1939795462_StaticFields::get_offset_of__instance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2580 = { sizeof (DemoScript_t4014867723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2580[2] = 
{
	DemoScript_t4014867723::get_offset_of_scene_2(),
	DemoScript_t4014867723::get_offset_of_loadToColor_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2581 = { sizeof (Fader_t1795977150), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2581[6] = 
{
	Fader_t1795977150::get_offset_of_start_2(),
	Fader_t1795977150::get_offset_of_fadeDamp_3(),
	Fader_t1795977150::get_offset_of_fadeScene_4(),
	Fader_t1795977150::get_offset_of_alpha_5(),
	Fader_t1795977150::get_offset_of_fadeColor_6(),
	Fader_t1795977150::get_offset_of_isFadeIn_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2582 = { sizeof (Initiate_t1514427591), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2583 = { sizeof (Benchmark01_t1571072624), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2583[10] = 
{
	Benchmark01_t1571072624::get_offset_of_BenchmarkType_2(),
	Benchmark01_t1571072624::get_offset_of_TMProFont_3(),
	Benchmark01_t1571072624::get_offset_of_TextMeshFont_4(),
	Benchmark01_t1571072624::get_offset_of_m_textMeshPro_5(),
	Benchmark01_t1571072624::get_offset_of_m_textContainer_6(),
	Benchmark01_t1571072624::get_offset_of_m_textMesh_7(),
	0,
	0,
	Benchmark01_t1571072624::get_offset_of_m_material01_10(),
	Benchmark01_t1571072624::get_offset_of_m_material02_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2584 = { sizeof (U3CStartU3Ec__Iterator0_t2216151886), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2584[5] = 
{
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U3CiU3E__1_0(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2216151886::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2585 = { sizeof (Benchmark01_UGUI_t3264177817), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2585[10] = 
{
	Benchmark01_UGUI_t3264177817::get_offset_of_BenchmarkType_2(),
	Benchmark01_UGUI_t3264177817::get_offset_of_canvas_3(),
	Benchmark01_UGUI_t3264177817::get_offset_of_TMProFont_4(),
	Benchmark01_UGUI_t3264177817::get_offset_of_TextMeshFont_5(),
	Benchmark01_UGUI_t3264177817::get_offset_of_m_textMeshPro_6(),
	Benchmark01_UGUI_t3264177817::get_offset_of_m_textMesh_7(),
	0,
	0,
	Benchmark01_UGUI_t3264177817::get_offset_of_m_material01_10(),
	Benchmark01_UGUI_t3264177817::get_offset_of_m_material02_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2586 = { sizeof (U3CStartU3Ec__Iterator0_t2622988697), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2586[5] = 
{
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U3CiU3E__1_0(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t2622988697::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2587 = { sizeof (Benchmark02_t1571269232), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2587[3] = 
{
	Benchmark02_t1571269232::get_offset_of_SpawnType_2(),
	Benchmark02_t1571269232::get_offset_of_NumberOfNPC_3(),
	Benchmark02_t1571269232::get_offset_of_floatingText_Script_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2588 = { sizeof (Benchmark03_t1571203696), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2588[3] = 
{
	Benchmark03_t1571203696::get_offset_of_SpawnType_2(),
	Benchmark03_t1571203696::get_offset_of_NumberOfNPC_3(),
	Benchmark03_t1571203696::get_offset_of_TheFont_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2589 = { sizeof (Benchmark04_t1570876016), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2589[5] = 
{
	Benchmark04_t1570876016::get_offset_of_SpawnType_2(),
	Benchmark04_t1570876016::get_offset_of_MinPointSize_3(),
	Benchmark04_t1570876016::get_offset_of_MaxPointSize_4(),
	Benchmark04_t1570876016::get_offset_of_Steps_5(),
	Benchmark04_t1570876016::get_offset_of_m_Transform_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2590 = { sizeof (CameraController_t2264742161), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2590[25] = 
{
	CameraController_t2264742161::get_offset_of_cameraTransform_2(),
	CameraController_t2264742161::get_offset_of_dummyTarget_3(),
	CameraController_t2264742161::get_offset_of_CameraTarget_4(),
	CameraController_t2264742161::get_offset_of_FollowDistance_5(),
	CameraController_t2264742161::get_offset_of_MaxFollowDistance_6(),
	CameraController_t2264742161::get_offset_of_MinFollowDistance_7(),
	CameraController_t2264742161::get_offset_of_ElevationAngle_8(),
	CameraController_t2264742161::get_offset_of_MaxElevationAngle_9(),
	CameraController_t2264742161::get_offset_of_MinElevationAngle_10(),
	CameraController_t2264742161::get_offset_of_OrbitalAngle_11(),
	CameraController_t2264742161::get_offset_of_CameraMode_12(),
	CameraController_t2264742161::get_offset_of_MovementSmoothing_13(),
	CameraController_t2264742161::get_offset_of_RotationSmoothing_14(),
	CameraController_t2264742161::get_offset_of_previousSmoothing_15(),
	CameraController_t2264742161::get_offset_of_MovementSmoothingValue_16(),
	CameraController_t2264742161::get_offset_of_RotationSmoothingValue_17(),
	CameraController_t2264742161::get_offset_of_MoveSensitivity_18(),
	CameraController_t2264742161::get_offset_of_currentVelocity_19(),
	CameraController_t2264742161::get_offset_of_desiredPosition_20(),
	CameraController_t2264742161::get_offset_of_mouseX_21(),
	CameraController_t2264742161::get_offset_of_mouseY_22(),
	CameraController_t2264742161::get_offset_of_moveVector_23(),
	CameraController_t2264742161::get_offset_of_mouseWheel_24(),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2591 = { sizeof (CameraModes_t3200559075)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2591[4] = 
{
	CameraModes_t3200559075::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2592 = { sizeof (ChatController_t3486202795), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2592[3] = 
{
	ChatController_t3486202795::get_offset_of_TMP_ChatInput_2(),
	ChatController_t3486202795::get_offset_of_TMP_ChatOutput_3(),
	ChatController_t3486202795::get_offset_of_ChatScrollbar_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2593 = { sizeof (EnvMapAnimator_t1140999784), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2593[3] = 
{
	EnvMapAnimator_t1140999784::get_offset_of_RotationSpeeds_2(),
	EnvMapAnimator_t1140999784::get_offset_of_m_textMeshPro_3(),
	EnvMapAnimator_t1140999784::get_offset_of_m_material_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2594 = { sizeof (U3CStartU3Ec__Iterator0_t1520811813), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2594[5] = 
{
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U3CmatrixU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t1520811813::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2595 = { sizeof (ObjectSpin_t341713598), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2595[10] = 
{
	ObjectSpin_t341713598::get_offset_of_SpinSpeed_2(),
	ObjectSpin_t341713598::get_offset_of_RotationRange_3(),
	ObjectSpin_t341713598::get_offset_of_m_transform_4(),
	ObjectSpin_t341713598::get_offset_of_m_time_5(),
	ObjectSpin_t341713598::get_offset_of_m_prevPOS_6(),
	ObjectSpin_t341713598::get_offset_of_m_initial_Rotation_7(),
	ObjectSpin_t341713598::get_offset_of_m_initial_Position_8(),
	ObjectSpin_t341713598::get_offset_of_m_lightColor_9(),
	ObjectSpin_t341713598::get_offset_of_frames_10(),
	ObjectSpin_t341713598::get_offset_of_Motion_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2596 = { sizeof (MotionType_t1905163921)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2596[4] = 
{
	MotionType_t1905163921::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2597 = { sizeof (ShaderPropAnimator_t3617420994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2597[4] = 
{
	ShaderPropAnimator_t3617420994::get_offset_of_m_Renderer_2(),
	ShaderPropAnimator_t3617420994::get_offset_of_m_Material_3(),
	ShaderPropAnimator_t3617420994::get_offset_of_GlowCurve_4(),
	ShaderPropAnimator_t3617420994::get_offset_of_m_frame_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2598 = { sizeof (U3CAnimatePropertiesU3Ec__Iterator0_t4041402054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2598[5] = 
{
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U3CglowPowerU3E__1_0(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24this_1(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24current_2(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24disposing_3(),
	U3CAnimatePropertiesU3Ec__Iterator0_t4041402054::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2599 = { sizeof (SimpleScript_t3279312205), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2599[3] = 
{
	SimpleScript_t3279312205::get_offset_of_m_textMeshPro_2(),
	0,
	SimpleScript_t3279312205::get_offset_of_m_frame_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
