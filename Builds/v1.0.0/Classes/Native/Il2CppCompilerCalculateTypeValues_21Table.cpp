﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Facebook.Unity.IFacebookLogger
struct IFacebookLogger_t2421194823;
// Facebook.Unity.InitDelegate
struct InitDelegate_t3081360126;
// Facebook.Unity.CallbackManager
struct CallbackManager_t2446104503;
// System.String
struct String_t;
// Facebook.Unity.HideUnityDelegate
struct HideUnityDelegate_t1353799728;
// System.Action
struct Action_t1264377477;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t2197129486;
// Facebook.Unity.Gameroom.GameroomFacebookGameObject
struct GameroomFacebookGameObject_t2310521816;
// Facebook.Unity.Gameroom.GameroomFacebook/OnComplete
struct OnComplete_t3786498944;
// Facebook.Unity.IFacebookCallbackHandler
struct IFacebookCallbackHandler_t762977247;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t1329213854;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.IO.StringReader
struct StringReader_t3465604688;
// System.Globalization.NumberFormatInfo
struct NumberFormatInfo_t435877138;
// Facebook.Unity.AsyncRequestString
struct AsyncRequestString_t3669574124;
// UnityEngine.WWW
struct WWW_t3688466362;
// Facebook.Unity.Editor.Dialogs.MockLoginDialog
struct MockLoginDialog_t1686232399;
// Facebook.Unity.IAsyncRequestStringWrapper
struct IAsyncRequestStringWrapper_t1063345135;
// System.Func`2<System.Object,System.String>
struct Func_2_t1214474899;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// Facebook.Unity.Gameroom.IGameroomWrapper
struct IGameroomWrapper_t1796962761;
// System.Void
struct Void_t1185182177;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// Facebook.Unity.AccessToken
struct AccessToken_t2431487013;
// System.String[]
struct StringU5BU5D_t1281789340;
// System.Int32[]
struct Int32U5BU5D_t385246372;
// Facebook.Unity.Editor.IEditorWrapper
struct IEditorWrapper_t1620787942;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t827303578;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t600458651;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// Facebook.Unity.Mobile.IOS.IIOSWrapper
struct IIOSWrapper_t1975150683;
// Facebook.Unity.IFacebook
struct IFacebook_t588365452;
// Facebook.Unity.FB/OnDLLLoaded
struct OnDLLLoaded_t2963647862;
// Facebook.Unity.ResultContainer
struct ResultContainer_t4150301447;
// System.Uri
struct Uri_t100236324;
// System.Collections.Generic.IDictionary`2<System.String,System.String>
struct IDictionary_2_t96558379;
// UnityEngine.WWWForm
struct WWWForm_t4064702195;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult>
struct FacebookDelegate_1_t138390958;
// Facebook.Unity.IFacebookImplementation
struct IFacebookImplementation_t2218504286;
// UnityEngine.GUIStyle
struct GUIStyle_t3956901511;
// Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer>
struct Callback_1_t3507265931;




#ifndef U3CMODULEU3E_T692745557_H
#define U3CMODULEU3E_T692745557_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745557 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745557_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef FACEBOOKLOGGER_T1173398901_H
#define FACEBOOKLOGGER_T1173398901_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookLogger
struct  FacebookLogger_t1173398901  : public RuntimeObject
{
public:

public:
};

struct FacebookLogger_t1173398901_StaticFields
{
public:
	// Facebook.Unity.IFacebookLogger Facebook.Unity.FacebookLogger::<Instance>k__BackingField
	RuntimeObject* ___U3CInstanceU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FacebookLogger_t1173398901_StaticFields, ___U3CInstanceU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CInstanceU3Ek__BackingField_0() const { return ___U3CInstanceU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CInstanceU3Ek__BackingField_0() { return &___U3CInstanceU3Ek__BackingField_0; }
	inline void set_U3CInstanceU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CInstanceU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKLOGGER_T1173398901_H
#ifndef FACEBOOKBASE_T1615169142_H
#define FACEBOOKBASE_T1615169142_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookBase
struct  FacebookBase_t1615169142  : public RuntimeObject
{
public:
	// Facebook.Unity.InitDelegate Facebook.Unity.FacebookBase::onInitCompleteDelegate
	InitDelegate_t3081360126 * ___onInitCompleteDelegate_0;
	// System.Boolean Facebook.Unity.FacebookBase::<Initialized>k__BackingField
	bool ___U3CInitializedU3Ek__BackingField_1;
	// Facebook.Unity.CallbackManager Facebook.Unity.FacebookBase::<CallbackManager>k__BackingField
	CallbackManager_t2446104503 * ___U3CCallbackManagerU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_onInitCompleteDelegate_0() { return static_cast<int32_t>(offsetof(FacebookBase_t1615169142, ___onInitCompleteDelegate_0)); }
	inline InitDelegate_t3081360126 * get_onInitCompleteDelegate_0() const { return ___onInitCompleteDelegate_0; }
	inline InitDelegate_t3081360126 ** get_address_of_onInitCompleteDelegate_0() { return &___onInitCompleteDelegate_0; }
	inline void set_onInitCompleteDelegate_0(InitDelegate_t3081360126 * value)
	{
		___onInitCompleteDelegate_0 = value;
		Il2CppCodeGenWriteBarrier((&___onInitCompleteDelegate_0), value);
	}

	inline static int32_t get_offset_of_U3CInitializedU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FacebookBase_t1615169142, ___U3CInitializedU3Ek__BackingField_1)); }
	inline bool get_U3CInitializedU3Ek__BackingField_1() const { return ___U3CInitializedU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CInitializedU3Ek__BackingField_1() { return &___U3CInitializedU3Ek__BackingField_1; }
	inline void set_U3CInitializedU3Ek__BackingField_1(bool value)
	{
		___U3CInitializedU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CCallbackManagerU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FacebookBase_t1615169142, ___U3CCallbackManagerU3Ek__BackingField_2)); }
	inline CallbackManager_t2446104503 * get_U3CCallbackManagerU3Ek__BackingField_2() const { return ___U3CCallbackManagerU3Ek__BackingField_2; }
	inline CallbackManager_t2446104503 ** get_address_of_U3CCallbackManagerU3Ek__BackingField_2() { return &___U3CCallbackManagerU3Ek__BackingField_2; }
	inline void set_U3CCallbackManagerU3Ek__BackingField_2(CallbackManager_t2446104503 * value)
	{
		___U3CCallbackManagerU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackManagerU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKBASE_T1615169142_H
#ifndef U3CU3EC__DISPLAYCLASS35_0_T171294370_H
#define U3CU3EC__DISPLAYCLASS35_0_T171294370_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FB/<>c__DisplayClass35_0
struct  U3CU3Ec__DisplayClass35_0_t171294370  : public RuntimeObject
{
public:
	// Facebook.Unity.InitDelegate Facebook.Unity.FB/<>c__DisplayClass35_0::onInitComplete
	InitDelegate_t3081360126 * ___onInitComplete_0;
	// System.String Facebook.Unity.FB/<>c__DisplayClass35_0::appId
	String_t* ___appId_1;
	// System.Boolean Facebook.Unity.FB/<>c__DisplayClass35_0::cookie
	bool ___cookie_2;
	// System.Boolean Facebook.Unity.FB/<>c__DisplayClass35_0::logging
	bool ___logging_3;
	// System.Boolean Facebook.Unity.FB/<>c__DisplayClass35_0::status
	bool ___status_4;
	// System.Boolean Facebook.Unity.FB/<>c__DisplayClass35_0::xfbml
	bool ___xfbml_5;
	// System.String Facebook.Unity.FB/<>c__DisplayClass35_0::authResponse
	String_t* ___authResponse_6;
	// System.Boolean Facebook.Unity.FB/<>c__DisplayClass35_0::frictionlessRequests
	bool ___frictionlessRequests_7;
	// System.String Facebook.Unity.FB/<>c__DisplayClass35_0::javascriptSDKLocale
	String_t* ___javascriptSDKLocale_8;
	// Facebook.Unity.HideUnityDelegate Facebook.Unity.FB/<>c__DisplayClass35_0::onHideUnity
	HideUnityDelegate_t1353799728 * ___onHideUnity_9;

public:
	inline static int32_t get_offset_of_onInitComplete_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t171294370, ___onInitComplete_0)); }
	inline InitDelegate_t3081360126 * get_onInitComplete_0() const { return ___onInitComplete_0; }
	inline InitDelegate_t3081360126 ** get_address_of_onInitComplete_0() { return &___onInitComplete_0; }
	inline void set_onInitComplete_0(InitDelegate_t3081360126 * value)
	{
		___onInitComplete_0 = value;
		Il2CppCodeGenWriteBarrier((&___onInitComplete_0), value);
	}

	inline static int32_t get_offset_of_appId_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t171294370, ___appId_1)); }
	inline String_t* get_appId_1() const { return ___appId_1; }
	inline String_t** get_address_of_appId_1() { return &___appId_1; }
	inline void set_appId_1(String_t* value)
	{
		___appId_1 = value;
		Il2CppCodeGenWriteBarrier((&___appId_1), value);
	}

	inline static int32_t get_offset_of_cookie_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t171294370, ___cookie_2)); }
	inline bool get_cookie_2() const { return ___cookie_2; }
	inline bool* get_address_of_cookie_2() { return &___cookie_2; }
	inline void set_cookie_2(bool value)
	{
		___cookie_2 = value;
	}

	inline static int32_t get_offset_of_logging_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t171294370, ___logging_3)); }
	inline bool get_logging_3() const { return ___logging_3; }
	inline bool* get_address_of_logging_3() { return &___logging_3; }
	inline void set_logging_3(bool value)
	{
		___logging_3 = value;
	}

	inline static int32_t get_offset_of_status_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t171294370, ___status_4)); }
	inline bool get_status_4() const { return ___status_4; }
	inline bool* get_address_of_status_4() { return &___status_4; }
	inline void set_status_4(bool value)
	{
		___status_4 = value;
	}

	inline static int32_t get_offset_of_xfbml_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t171294370, ___xfbml_5)); }
	inline bool get_xfbml_5() const { return ___xfbml_5; }
	inline bool* get_address_of_xfbml_5() { return &___xfbml_5; }
	inline void set_xfbml_5(bool value)
	{
		___xfbml_5 = value;
	}

	inline static int32_t get_offset_of_authResponse_6() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t171294370, ___authResponse_6)); }
	inline String_t* get_authResponse_6() const { return ___authResponse_6; }
	inline String_t** get_address_of_authResponse_6() { return &___authResponse_6; }
	inline void set_authResponse_6(String_t* value)
	{
		___authResponse_6 = value;
		Il2CppCodeGenWriteBarrier((&___authResponse_6), value);
	}

	inline static int32_t get_offset_of_frictionlessRequests_7() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t171294370, ___frictionlessRequests_7)); }
	inline bool get_frictionlessRequests_7() const { return ___frictionlessRequests_7; }
	inline bool* get_address_of_frictionlessRequests_7() { return &___frictionlessRequests_7; }
	inline void set_frictionlessRequests_7(bool value)
	{
		___frictionlessRequests_7 = value;
	}

	inline static int32_t get_offset_of_javascriptSDKLocale_8() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t171294370, ___javascriptSDKLocale_8)); }
	inline String_t* get_javascriptSDKLocale_8() const { return ___javascriptSDKLocale_8; }
	inline String_t** get_address_of_javascriptSDKLocale_8() { return &___javascriptSDKLocale_8; }
	inline void set_javascriptSDKLocale_8(String_t* value)
	{
		___javascriptSDKLocale_8 = value;
		Il2CppCodeGenWriteBarrier((&___javascriptSDKLocale_8), value);
	}

	inline static int32_t get_offset_of_onHideUnity_9() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t171294370, ___onHideUnity_9)); }
	inline HideUnityDelegate_t1353799728 * get_onHideUnity_9() const { return ___onHideUnity_9; }
	inline HideUnityDelegate_t1353799728 ** get_address_of_onHideUnity_9() { return &___onHideUnity_9; }
	inline void set_onHideUnity_9(HideUnityDelegate_t1353799728 * value)
	{
		___onHideUnity_9 = value;
		Il2CppCodeGenWriteBarrier((&___onHideUnity_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_0_T171294370_H
#ifndef U3CDELAYEVENTU3ED__1_T2601426355_H
#define U3CDELAYEVENTU3ED__1_T2601426355_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookScheduler/<DelayEvent>d__1
struct  U3CDelayEventU3Ed__1_t2601426355  : public RuntimeObject
{
public:
	// System.Int32 Facebook.Unity.FacebookScheduler/<DelayEvent>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Facebook.Unity.FacebookScheduler/<DelayEvent>d__1::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Int64 Facebook.Unity.FacebookScheduler/<DelayEvent>d__1::delay
	int64_t ___delay_2;
	// System.Action Facebook.Unity.FacebookScheduler/<DelayEvent>d__1::action
	Action_t1264377477 * ___action_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDelayEventU3Ed__1_t2601426355, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDelayEventU3Ed__1_t2601426355, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CDelayEventU3Ed__1_t2601426355, ___delay_2)); }
	inline int64_t get_delay_2() const { return ___delay_2; }
	inline int64_t* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(int64_t value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_action_3() { return static_cast<int32_t>(offsetof(U3CDelayEventU3Ed__1_t2601426355, ___action_3)); }
	inline Action_t1264377477 * get_action_3() const { return ___action_3; }
	inline Action_t1264377477 ** get_address_of_action_3() { return &___action_3; }
	inline void set_action_3(Action_t1264377477 * value)
	{
		___action_3 = value;
		Il2CppCodeGenWriteBarrier((&___action_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDELAYEVENTU3ED__1_T2601426355_H
#ifndef MOBILE_T77204530_H
#define MOBILE_T77204530_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FB/Mobile
struct  Mobile_t77204530  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILE_T77204530_H
#ifndef U3CU3EC_T3332642991_H
#define U3CU3EC_T3332642991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookBase/<>c
struct  U3CU3Ec_t3332642991  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3332642991_StaticFields
{
public:
	// Facebook.Unity.FacebookBase/<>c Facebook.Unity.FacebookBase/<>c::<>9
	U3CU3Ec_t3332642991 * ___U3CU3E9_0;
	// System.Func`2<System.String,System.Boolean> Facebook.Unity.FacebookBase/<>c::<>9__41_0
	Func_2_t2197129486 * ___U3CU3E9__41_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3332642991_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3332642991 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3332642991 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3332642991 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__41_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3332642991_StaticFields, ___U3CU3E9__41_0_1)); }
	inline Func_2_t2197129486 * get_U3CU3E9__41_0_1() const { return ___U3CU3E9__41_0_1; }
	inline Func_2_t2197129486 ** get_address_of_U3CU3E9__41_0_1() { return &___U3CU3E9__41_0_1; }
	inline void set_U3CU3E9__41_0_1(Func_2_t2197129486 * value)
	{
		___U3CU3E9__41_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__41_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T3332642991_H
#ifndef U3CWAITFORPIPERESPONSEU3ED__4_T3567704217_H
#define U3CWAITFORPIPERESPONSEU3ED__4_T3567704217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Gameroom.GameroomFacebookGameObject/<WaitForPipeResponse>d__4
struct  U3CWaitForPipeResponseU3Ed__4_t3567704217  : public RuntimeObject
{
public:
	// System.Int32 Facebook.Unity.Gameroom.GameroomFacebookGameObject/<WaitForPipeResponse>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Facebook.Unity.Gameroom.GameroomFacebookGameObject/<WaitForPipeResponse>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Facebook.Unity.Gameroom.GameroomFacebookGameObject Facebook.Unity.Gameroom.GameroomFacebookGameObject/<WaitForPipeResponse>d__4::<>4__this
	GameroomFacebookGameObject_t2310521816 * ___U3CU3E4__this_2;
	// Facebook.Unity.Gameroom.GameroomFacebook/OnComplete Facebook.Unity.Gameroom.GameroomFacebookGameObject/<WaitForPipeResponse>d__4::onCompleteDelegate
	OnComplete_t3786498944 * ___onCompleteDelegate_3;
	// System.String Facebook.Unity.Gameroom.GameroomFacebookGameObject/<WaitForPipeResponse>d__4::callbackId
	String_t* ___callbackId_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForPipeResponseU3Ed__4_t3567704217, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForPipeResponseU3Ed__4_t3567704217, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CWaitForPipeResponseU3Ed__4_t3567704217, ___U3CU3E4__this_2)); }
	inline GameroomFacebookGameObject_t2310521816 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline GameroomFacebookGameObject_t2310521816 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(GameroomFacebookGameObject_t2310521816 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_onCompleteDelegate_3() { return static_cast<int32_t>(offsetof(U3CWaitForPipeResponseU3Ed__4_t3567704217, ___onCompleteDelegate_3)); }
	inline OnComplete_t3786498944 * get_onCompleteDelegate_3() const { return ___onCompleteDelegate_3; }
	inline OnComplete_t3786498944 ** get_address_of_onCompleteDelegate_3() { return &___onCompleteDelegate_3; }
	inline void set_onCompleteDelegate_3(OnComplete_t3786498944 * value)
	{
		___onCompleteDelegate_3 = value;
		Il2CppCodeGenWriteBarrier((&___onCompleteDelegate_3), value);
	}

	inline static int32_t get_offset_of_callbackId_4() { return static_cast<int32_t>(offsetof(U3CWaitForPipeResponseU3Ed__4_t3567704217, ___callbackId_4)); }
	inline String_t* get_callbackId_4() const { return ___callbackId_4; }
	inline String_t** get_address_of_callbackId_4() { return &___callbackId_4; }
	inline void set_callbackId_4(String_t* value)
	{
		___callbackId_4 = value;
		Il2CppCodeGenWriteBarrier((&___callbackId_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORPIPERESPONSEU3ED__4_T3567704217_H
#ifndef EDITORWRAPPER_T3420512207_H
#define EDITORWRAPPER_T3420512207_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.EditorWrapper
struct  EditorWrapper_t3420512207  : public RuntimeObject
{
public:
	// Facebook.Unity.IFacebookCallbackHandler Facebook.Unity.Editor.EditorWrapper::callbackHandler
	RuntimeObject* ___callbackHandler_0;

public:
	inline static int32_t get_offset_of_callbackHandler_0() { return static_cast<int32_t>(offsetof(EditorWrapper_t3420512207, ___callbackHandler_0)); }
	inline RuntimeObject* get_callbackHandler_0() const { return ___callbackHandler_0; }
	inline RuntimeObject** get_address_of_callbackHandler_0() { return &___callbackHandler_0; }
	inline void set_callbackHandler_0(RuntimeObject* value)
	{
		___callbackHandler_0 = value;
		Il2CppCodeGenWriteBarrier((&___callbackHandler_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORWRAPPER_T3420512207_H
#ifndef FACEBOOKSDKVERSION_T4164812230_H
#define FACEBOOKSDKVERSION_T4164812230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookSdkVersion
struct  FacebookSdkVersion_t4164812230  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKSDKVERSION_T4164812230_H
#ifndef METHODARGUMENTS_T1563002313_H
#define METHODARGUMENTS_T1563002313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.MethodArguments
struct  MethodArguments_t1563002313  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.MethodArguments::arguments
	RuntimeObject* ___arguments_0;

public:
	inline static int32_t get_offset_of_arguments_0() { return static_cast<int32_t>(offsetof(MethodArguments_t1563002313, ___arguments_0)); }
	inline RuntimeObject* get_arguments_0() const { return ___arguments_0; }
	inline RuntimeObject** get_address_of_arguments_0() { return &___arguments_0; }
	inline void set_arguments_0(RuntimeObject* value)
	{
		___arguments_0 = value;
		Il2CppCodeGenWriteBarrier((&___arguments_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODARGUMENTS_T1563002313_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef CANVAS_T3146753929_H
#define CANVAS_T3146753929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FB/Canvas
struct  Canvas_t3146753929  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVAS_T3146753929_H
#ifndef RESULTCONTAINER_T4150301447_H
#define RESULTCONTAINER_T4150301447_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ResultContainer
struct  ResultContainer_t4150301447  : public RuntimeObject
{
public:
	// System.String Facebook.Unity.ResultContainer::<RawResult>k__BackingField
	String_t* ___U3CRawResultU3Ek__BackingField_1;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.ResultContainer::<ResultDictionary>k__BackingField
	RuntimeObject* ___U3CResultDictionaryU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CRawResultU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ResultContainer_t4150301447, ___U3CRawResultU3Ek__BackingField_1)); }
	inline String_t* get_U3CRawResultU3Ek__BackingField_1() const { return ___U3CRawResultU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CRawResultU3Ek__BackingField_1() { return &___U3CRawResultU3Ek__BackingField_1; }
	inline void set_U3CRawResultU3Ek__BackingField_1(String_t* value)
	{
		___U3CRawResultU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRawResultU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CResultDictionaryU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ResultContainer_t4150301447, ___U3CResultDictionaryU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CResultDictionaryU3Ek__BackingField_2() const { return ___U3CResultDictionaryU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CResultDictionaryU3Ek__BackingField_2() { return &___U3CResultDictionaryU3Ek__BackingField_2; }
	inline void set_U3CResultDictionaryU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CResultDictionaryU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResultDictionaryU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTCONTAINER_T4150301447_H
#ifndef SERIALIZER_T3368339501_H
#define SERIALIZER_T3368339501_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.MiniJSON.Json/Serializer
struct  Serializer_t3368339501  : public RuntimeObject
{
public:
	// System.Text.StringBuilder Facebook.MiniJSON.Json/Serializer::builder
	StringBuilder_t * ___builder_0;

public:
	inline static int32_t get_offset_of_builder_0() { return static_cast<int32_t>(offsetof(Serializer_t3368339501, ___builder_0)); }
	inline StringBuilder_t * get_builder_0() const { return ___builder_0; }
	inline StringBuilder_t ** get_address_of_builder_0() { return &___builder_0; }
	inline void set_builder_0(StringBuilder_t * value)
	{
		___builder_0 = value;
		Il2CppCodeGenWriteBarrier((&___builder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZER_T3368339501_H
#ifndef DEBUGLOGGER_T2300716689_H
#define DEBUGLOGGER_T2300716689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookLogger/DebugLogger
struct  DebugLogger_t2300716689  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGLOGGER_T2300716689_H
#ifndef PARSER_T2196815292_H
#define PARSER_T2196815292_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.MiniJSON.Json/Parser
struct  Parser_t2196815292  : public RuntimeObject
{
public:
	// System.IO.StringReader Facebook.MiniJSON.Json/Parser::json
	StringReader_t3465604688 * ___json_0;

public:
	inline static int32_t get_offset_of_json_0() { return static_cast<int32_t>(offsetof(Parser_t2196815292, ___json_0)); }
	inline StringReader_t3465604688 * get_json_0() const { return ___json_0; }
	inline StringReader_t3465604688 ** get_address_of_json_0() { return &___json_0; }
	inline void set_json_0(StringReader_t3465604688 * value)
	{
		___json_0 = value;
		Il2CppCodeGenWriteBarrier((&___json_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSER_T2196815292_H
#ifndef JSON_T3131101706_H
#define JSON_T3131101706_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.MiniJSON.Json
struct  Json_t3131101706  : public RuntimeObject
{
public:

public:
};

struct Json_t3131101706_StaticFields
{
public:
	// System.Globalization.NumberFormatInfo Facebook.MiniJSON.Json::numberFormat
	NumberFormatInfo_t435877138 * ___numberFormat_0;

public:
	inline static int32_t get_offset_of_numberFormat_0() { return static_cast<int32_t>(offsetof(Json_t3131101706_StaticFields, ___numberFormat_0)); }
	inline NumberFormatInfo_t435877138 * get_numberFormat_0() const { return ___numberFormat_0; }
	inline NumberFormatInfo_t435877138 ** get_address_of_numberFormat_0() { return &___numberFormat_0; }
	inline void set_numberFormat_0(NumberFormatInfo_t435877138 * value)
	{
		___numberFormat_0 = value;
		Il2CppCodeGenWriteBarrier((&___numberFormat_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSON_T3131101706_H
#ifndef U3CSTARTU3ED__9_T2855687532_H
#define U3CSTARTU3ED__9_T2855687532_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AsyncRequestString/<Start>d__9
struct  U3CStartU3Ed__9_t2855687532  : public RuntimeObject
{
public:
	// System.Int32 Facebook.Unity.AsyncRequestString/<Start>d__9::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Facebook.Unity.AsyncRequestString/<Start>d__9::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Facebook.Unity.AsyncRequestString Facebook.Unity.AsyncRequestString/<Start>d__9::<>4__this
	AsyncRequestString_t3669574124 * ___U3CU3E4__this_2;
	// UnityEngine.WWW Facebook.Unity.AsyncRequestString/<Start>d__9::<www>5__1
	WWW_t3688466362 * ___U3CwwwU3E5__1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__9_t2855687532, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__9_t2855687532, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__9_t2855687532, ___U3CU3E4__this_2)); }
	inline AsyncRequestString_t3669574124 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline AsyncRequestString_t3669574124 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(AsyncRequestString_t3669574124 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__1_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__9_t2855687532, ___U3CwwwU3E5__1_3)); }
	inline WWW_t3688466362 * get_U3CwwwU3E5__1_3() const { return ___U3CwwwU3E5__1_3; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E5__1_3() { return &___U3CwwwU3E5__1_3; }
	inline void set_U3CwwwU3E5__1_3(WWW_t3688466362 * value)
	{
		___U3CwwwU3E5__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__9_T2855687532_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T95552448_H
#define U3CU3EC__DISPLAYCLASS4_0_T95552448_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.Dialogs.MockLoginDialog/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t95552448  : public RuntimeObject
{
public:
	// System.String Facebook.Unity.Editor.Dialogs.MockLoginDialog/<>c__DisplayClass4_0::facebookID
	String_t* ___facebookID_0;
	// Facebook.Unity.Editor.Dialogs.MockLoginDialog Facebook.Unity.Editor.Dialogs.MockLoginDialog/<>c__DisplayClass4_0::<>4__this
	MockLoginDialog_t1686232399 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_facebookID_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t95552448, ___facebookID_0)); }
	inline String_t* get_facebookID_0() const { return ___facebookID_0; }
	inline String_t** get_address_of_facebookID_0() { return &___facebookID_0; }
	inline void set_facebookID_0(String_t* value)
	{
		___facebookID_0 = value;
		Il2CppCodeGenWriteBarrier((&___facebookID_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t95552448, ___U3CU3E4__this_1)); }
	inline MockLoginDialog_t1686232399 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline MockLoginDialog_t1686232399 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(MockLoginDialog_t1686232399 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T95552448_H
#ifndef ASYNCREQUESTSTRINGWRAPPER_T707311955_H
#define ASYNCREQUESTSTRINGWRAPPER_T707311955_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AsyncRequestStringWrapper
struct  AsyncRequestStringWrapper_t707311955  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCREQUESTSTRINGWRAPPER_T707311955_H
#ifndef FBUNITYUTILITY_T2214217213_H
#define FBUNITYUTILITY_T2214217213_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FBUnityUtility
struct  FBUnityUtility_t2214217213  : public RuntimeObject
{
public:

public:
};

struct FBUnityUtility_t2214217213_StaticFields
{
public:
	// Facebook.Unity.IAsyncRequestStringWrapper Facebook.Unity.FBUnityUtility::asyncRequestStringWrapper
	RuntimeObject* ___asyncRequestStringWrapper_0;

public:
	inline static int32_t get_offset_of_asyncRequestStringWrapper_0() { return static_cast<int32_t>(offsetof(FBUnityUtility_t2214217213_StaticFields, ___asyncRequestStringWrapper_0)); }
	inline RuntimeObject* get_asyncRequestStringWrapper_0() const { return ___asyncRequestStringWrapper_0; }
	inline RuntimeObject** get_address_of_asyncRequestStringWrapper_0() { return &___asyncRequestStringWrapper_0; }
	inline void set_asyncRequestStringWrapper_0(RuntimeObject* value)
	{
		___asyncRequestStringWrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&___asyncRequestStringWrapper_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FBUNITYUTILITY_T2214217213_H
#ifndef U3CU3EC_T3449414030_H
#define U3CU3EC_T3449414030_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Utilities/<>c
struct  U3CU3Ec_t3449414030  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3449414030_StaticFields
{
public:
	// Facebook.Unity.Utilities/<>c Facebook.Unity.Utilities/<>c::<>9
	U3CU3Ec_t3449414030 * ___U3CU3E9_0;
	// System.Func`2<System.Object,System.String> Facebook.Unity.Utilities/<>c::<>9__18_0
	Func_2_t1214474899 * ___U3CU3E9__18_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3449414030_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3449414030 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3449414030 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3449414030 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__18_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3449414030_StaticFields, ___U3CU3E9__18_0_1)); }
	inline Func_2_t1214474899 * get_U3CU3E9__18_0_1() const { return ___U3CU3E9__18_0_1; }
	inline Func_2_t1214474899 ** get_address_of_U3CU3E9__18_0_1() { return &___U3CU3E9__18_0_1; }
	inline void set_U3CU3E9__18_0_1(Func_2_t1214474899 * value)
	{
		___U3CU3E9__18_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__18_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T3449414030_H
#ifndef UTILITIES_T2011615223_H
#define UTILITIES_T2011615223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Utilities
struct  Utilities_t2011615223  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILITIES_T2011615223_H
#ifndef COMPONENTFACTORY_T141932586_H
#define COMPONENTFACTORY_T141932586_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ComponentFactory
struct  ComponentFactory_t141932586  : public RuntimeObject
{
public:

public:
};

struct ComponentFactory_t141932586_StaticFields
{
public:
	// UnityEngine.GameObject Facebook.Unity.ComponentFactory::facebookGameObject
	GameObject_t1113636619 * ___facebookGameObject_0;

public:
	inline static int32_t get_offset_of_facebookGameObject_0() { return static_cast<int32_t>(offsetof(ComponentFactory_t141932586_StaticFields, ___facebookGameObject_0)); }
	inline GameObject_t1113636619 * get_facebookGameObject_0() const { return ___facebookGameObject_0; }
	inline GameObject_t1113636619 ** get_address_of_facebookGameObject_0() { return &___facebookGameObject_0; }
	inline void set_facebookGameObject_0(GameObject_t1113636619 * value)
	{
		___facebookGameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___facebookGameObject_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENTFACTORY_T141932586_H
#ifndef CALLBACKMANAGER_T2446104503_H
#define CALLBACKMANAGER_T2446104503_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.CallbackManager
struct  CallbackManager_t2446104503  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.CallbackManager::facebookDelegates
	RuntimeObject* ___facebookDelegates_0;
	// System.Int32 Facebook.Unity.CallbackManager::nextAsyncId
	int32_t ___nextAsyncId_1;

public:
	inline static int32_t get_offset_of_facebookDelegates_0() { return static_cast<int32_t>(offsetof(CallbackManager_t2446104503, ___facebookDelegates_0)); }
	inline RuntimeObject* get_facebookDelegates_0() const { return ___facebookDelegates_0; }
	inline RuntimeObject** get_address_of_facebookDelegates_0() { return &___facebookDelegates_0; }
	inline void set_facebookDelegates_0(RuntimeObject* value)
	{
		___facebookDelegates_0 = value;
		Il2CppCodeGenWriteBarrier((&___facebookDelegates_0), value);
	}

	inline static int32_t get_offset_of_nextAsyncId_1() { return static_cast<int32_t>(offsetof(CallbackManager_t2446104503, ___nextAsyncId_1)); }
	inline int32_t get_nextAsyncId_1() const { return ___nextAsyncId_1; }
	inline int32_t* get_address_of_nextAsyncId_1() { return &___nextAsyncId_1; }
	inline void set_nextAsyncId_1(int32_t value)
	{
		___nextAsyncId_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLBACKMANAGER_T2446104503_H
#ifndef GAMEROOMFACEBOOK_T453300599_H
#define GAMEROOMFACEBOOK_T453300599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Gameroom.GameroomFacebook
struct  GameroomFacebook_t453300599  : public FacebookBase_t1615169142
{
public:
	// System.String Facebook.Unity.Gameroom.GameroomFacebook::appId
	String_t* ___appId_3;
	// Facebook.Unity.Gameroom.IGameroomWrapper Facebook.Unity.Gameroom.GameroomFacebook::gameroomWrapper
	RuntimeObject* ___gameroomWrapper_4;
	// System.Boolean Facebook.Unity.Gameroom.GameroomFacebook::<LimitEventUsage>k__BackingField
	bool ___U3CLimitEventUsageU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_appId_3() { return static_cast<int32_t>(offsetof(GameroomFacebook_t453300599, ___appId_3)); }
	inline String_t* get_appId_3() const { return ___appId_3; }
	inline String_t** get_address_of_appId_3() { return &___appId_3; }
	inline void set_appId_3(String_t* value)
	{
		___appId_3 = value;
		Il2CppCodeGenWriteBarrier((&___appId_3), value);
	}

	inline static int32_t get_offset_of_gameroomWrapper_4() { return static_cast<int32_t>(offsetof(GameroomFacebook_t453300599, ___gameroomWrapper_4)); }
	inline RuntimeObject* get_gameroomWrapper_4() const { return ___gameroomWrapper_4; }
	inline RuntimeObject** get_address_of_gameroomWrapper_4() { return &___gameroomWrapper_4; }
	inline void set_gameroomWrapper_4(RuntimeObject* value)
	{
		___gameroomWrapper_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameroomWrapper_4), value);
	}

	inline static int32_t get_offset_of_U3CLimitEventUsageU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GameroomFacebook_t453300599, ___U3CLimitEventUsageU3Ek__BackingField_5)); }
	inline bool get_U3CLimitEventUsageU3Ek__BackingField_5() const { return ___U3CLimitEventUsageU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CLimitEventUsageU3Ek__BackingField_5() { return &___U3CLimitEventUsageU3Ek__BackingField_5; }
	inline void set_U3CLimitEventUsageU3Ek__BackingField_5(bool value)
	{
		___U3CLimitEventUsageU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEROOMFACEBOOK_T453300599_H
#ifndef __STATICARRAYINITTYPESIZEU3D120_T3297148301_H
#define __STATICARRAYINITTYPESIZEU3D120_T3297148301_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=120
struct  __StaticArrayInitTypeSizeU3D120_t3297148301 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D120_t3297148301__padding[120];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D120_T3297148301_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef RECT_T2360479859_H
#define RECT_T2360479859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t2360479859 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t2360479859, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T2360479859_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef BOOLEAN_T97287965_H
#define BOOLEAN_T97287965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_t97287965 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_2;

public:
	inline static int32_t get_offset_of_m_value_2() { return static_cast<int32_t>(offsetof(Boolean_t97287965, ___m_value_2)); }
	inline bool get_m_value_2() const { return ___m_value_2; }
	inline bool* get_address_of_m_value_2() { return &___m_value_2; }
	inline void set_m_value_2(bool value)
	{
		___m_value_2 = value;
	}
};

struct Boolean_t97287965_StaticFields
{
public:
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_0;
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_1;

public:
	inline static int32_t get_offset_of_FalseString_0() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___FalseString_0)); }
	inline String_t* get_FalseString_0() const { return ___FalseString_0; }
	inline String_t** get_address_of_FalseString_0() { return &___FalseString_0; }
	inline void set_FalseString_0(String_t* value)
	{
		___FalseString_0 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_0), value);
	}

	inline static int32_t get_offset_of_TrueString_1() { return static_cast<int32_t>(offsetof(Boolean_t97287965_StaticFields, ___TrueString_1)); }
	inline String_t* get_TrueString_1() const { return ___TrueString_1; }
	inline String_t** get_address_of_TrueString_1() { return &___TrueString_1; }
	inline void set_TrueString_1(String_t* value)
	{
		___TrueString_1 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_T97287965_H
#ifndef __STATICARRAYINITTYPESIZEU3D50_T1547932759_H
#define __STATICARRAYINITTYPESIZEU3D50_T1547932759_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=50
struct  __StaticArrayInitTypeSizeU3D50_t1547932759 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D50_t1547932759__padding[50];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // __STATICARRAYINITTYPESIZEU3D50_T1547932759_H
#ifndef NULLABLE_1_T1164162090_H
#define NULLABLE_1_T1164162090_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int64>
struct  Nullable_1_t1164162090 
{
public:
	// T System.Nullable`1::value
	int64_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1164162090, ___value_0)); }
	inline int64_t get_value_0() const { return ___value_0; }
	inline int64_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int64_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1164162090, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1164162090_H
#ifndef DATETIMEKIND_T3468814247_H
#define DATETIMEKIND_T3468814247_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeKind
struct  DateTimeKind_t3468814247 
{
public:
	// System.Int32 System.DateTimeKind::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(DateTimeKind_t3468814247, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEKIND_T3468814247_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef HTTPMETHOD_T313594167_H
#define HTTPMETHOD_T313594167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.HttpMethod
struct  HttpMethod_t313594167 
{
public:
	// System.Int32 Facebook.Unity.HttpMethod::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(HttpMethod_t313594167, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPMETHOD_T313594167_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef FACEBOOKUNITYPLATFORM_T2098070734_H
#define FACEBOOKUNITYPLATFORM_T2098070734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookUnityPlatform
struct  FacebookUnityPlatform_t2098070734 
{
public:
	// System.Int32 Facebook.Unity.FacebookUnityPlatform::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(FacebookUnityPlatform_t2098070734, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKUNITYPLATFORM_T2098070734_H
#ifndef OGACTIONTYPE_T397354381_H
#define OGACTIONTYPE_T397354381_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.OGActionType
struct  OGActionType_t397354381 
{
public:
	// System.Int32 Facebook.Unity.OGActionType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(OGActionType_t397354381, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OGACTIONTYPE_T397354381_H
#ifndef SHAREDIALOGMODE_T1679970535_H
#define SHAREDIALOGMODE_T1679970535_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ShareDialogMode
struct  ShareDialogMode_t1679970535 
{
public:
	// System.Int32 Facebook.Unity.ShareDialogMode::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(ShareDialogMode_t1679970535, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHAREDIALOGMODE_T1679970535_H
#ifndef RESULTBASE_T777682874_H
#define RESULTBASE_T777682874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ResultBase
struct  ResultBase_t777682874  : public RuntimeObject
{
public:
	// System.String Facebook.Unity.ResultBase::<Error>k__BackingField
	String_t* ___U3CErrorU3Ek__BackingField_0;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.ResultBase::<ResultDictionary>k__BackingField
	RuntimeObject* ___U3CResultDictionaryU3Ek__BackingField_1;
	// System.String Facebook.Unity.ResultBase::<RawResult>k__BackingField
	String_t* ___U3CRawResultU3Ek__BackingField_2;
	// System.Boolean Facebook.Unity.ResultBase::<Cancelled>k__BackingField
	bool ___U3CCancelledU3Ek__BackingField_3;
	// System.String Facebook.Unity.ResultBase::<CallbackId>k__BackingField
	String_t* ___U3CCallbackIdU3Ek__BackingField_4;
	// System.Nullable`1<System.Int64> Facebook.Unity.ResultBase::<CanvasErrorCode>k__BackingField
	Nullable_1_t1164162090  ___U3CCanvasErrorCodeU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CErrorU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ResultBase_t777682874, ___U3CErrorU3Ek__BackingField_0)); }
	inline String_t* get_U3CErrorU3Ek__BackingField_0() const { return ___U3CErrorU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CErrorU3Ek__BackingField_0() { return &___U3CErrorU3Ek__BackingField_0; }
	inline void set_U3CErrorU3Ek__BackingField_0(String_t* value)
	{
		___U3CErrorU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CResultDictionaryU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ResultBase_t777682874, ___U3CResultDictionaryU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CResultDictionaryU3Ek__BackingField_1() const { return ___U3CResultDictionaryU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CResultDictionaryU3Ek__BackingField_1() { return &___U3CResultDictionaryU3Ek__BackingField_1; }
	inline void set_U3CResultDictionaryU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CResultDictionaryU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResultDictionaryU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CRawResultU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ResultBase_t777682874, ___U3CRawResultU3Ek__BackingField_2)); }
	inline String_t* get_U3CRawResultU3Ek__BackingField_2() const { return ___U3CRawResultU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CRawResultU3Ek__BackingField_2() { return &___U3CRawResultU3Ek__BackingField_2; }
	inline void set_U3CRawResultU3Ek__BackingField_2(String_t* value)
	{
		___U3CRawResultU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRawResultU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CCancelledU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ResultBase_t777682874, ___U3CCancelledU3Ek__BackingField_3)); }
	inline bool get_U3CCancelledU3Ek__BackingField_3() const { return ___U3CCancelledU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CCancelledU3Ek__BackingField_3() { return &___U3CCancelledU3Ek__BackingField_3; }
	inline void set_U3CCancelledU3Ek__BackingField_3(bool value)
	{
		___U3CCancelledU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CCallbackIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ResultBase_t777682874, ___U3CCallbackIdU3Ek__BackingField_4)); }
	inline String_t* get_U3CCallbackIdU3Ek__BackingField_4() const { return ___U3CCallbackIdU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CCallbackIdU3Ek__BackingField_4() { return &___U3CCallbackIdU3Ek__BackingField_4; }
	inline void set_U3CCallbackIdU3Ek__BackingField_4(String_t* value)
	{
		___U3CCallbackIdU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackIdU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CCanvasErrorCodeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ResultBase_t777682874, ___U3CCanvasErrorCodeU3Ek__BackingField_5)); }
	inline Nullable_1_t1164162090  get_U3CCanvasErrorCodeU3Ek__BackingField_5() const { return ___U3CCanvasErrorCodeU3Ek__BackingField_5; }
	inline Nullable_1_t1164162090 * get_address_of_U3CCanvasErrorCodeU3Ek__BackingField_5() { return &___U3CCanvasErrorCodeU3Ek__BackingField_5; }
	inline void set_U3CCanvasErrorCodeU3Ek__BackingField_5(Nullable_1_t1164162090  value)
	{
		___U3CCanvasErrorCodeU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTBASE_T777682874_H
#ifndef IFNOTEXIST_T4145194463_H
#define IFNOTEXIST_T4145194463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ComponentFactory/IfNotExist
struct  IfNotExist_t4145194463 
{
public:
	// System.Int32 Facebook.Unity.ComponentFactory/IfNotExist::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(IfNotExist_t4145194463, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IFNOTEXIST_T4145194463_H
#ifndef TOKEN_T215992027_H
#define TOKEN_T215992027_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.MiniJSON.Json/Parser/TOKEN
struct  TOKEN_t215992027 
{
public:
	// System.Int32 Facebook.MiniJSON.Json/Parser/TOKEN::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TOKEN_t215992027, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T215992027_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MOBILEFACEBOOK_T4202726836_H
#define MOBILEFACEBOOK_T4202726836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.MobileFacebook
struct  MobileFacebook_t4202726836  : public FacebookBase_t1615169142
{
public:
	// Facebook.Unity.ShareDialogMode Facebook.Unity.Mobile.MobileFacebook::shareDialogMode
	int32_t ___shareDialogMode_3;

public:
	inline static int32_t get_offset_of_shareDialogMode_3() { return static_cast<int32_t>(offsetof(MobileFacebook_t4202726836, ___shareDialogMode_3)); }
	inline int32_t get_shareDialogMode_3() const { return ___shareDialogMode_3; }
	inline int32_t* get_address_of_shareDialogMode_3() { return &___shareDialogMode_3; }
	inline void set_shareDialogMode_3(int32_t value)
	{
		___shareDialogMode_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEFACEBOOK_T4202726836_H
#ifndef ACCESSTOKENREFRESHRESULT_T1866521153_H
#define ACCESSTOKENREFRESHRESULT_T1866521153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AccessTokenRefreshResult
struct  AccessTokenRefreshResult_t1866521153  : public ResultBase_t777682874
{
public:
	// Facebook.Unity.AccessToken Facebook.Unity.AccessTokenRefreshResult::<AccessToken>k__BackingField
	AccessToken_t2431487013 * ___U3CAccessTokenU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CAccessTokenU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AccessTokenRefreshResult_t1866521153, ___U3CAccessTokenU3Ek__BackingField_6)); }
	inline AccessToken_t2431487013 * get_U3CAccessTokenU3Ek__BackingField_6() const { return ___U3CAccessTokenU3Ek__BackingField_6; }
	inline AccessToken_t2431487013 ** get_address_of_U3CAccessTokenU3Ek__BackingField_6() { return &___U3CAccessTokenU3Ek__BackingField_6; }
	inline void set_U3CAccessTokenU3Ek__BackingField_6(AccessToken_t2431487013 * value)
	{
		___U3CAccessTokenU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAccessTokenU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCESSTOKENREFRESHRESULT_T1866521153_H
#ifndef DATETIME_T3738529785_H
#define DATETIME_T3738529785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t3738529785 
{
public:
	// System.TimeSpan System.DateTime::ticks
	TimeSpan_t881159249  ___ticks_0;
	// System.DateTimeKind System.DateTime::kind
	int32_t ___kind_1;

public:
	inline static int32_t get_offset_of_ticks_0() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___ticks_0)); }
	inline TimeSpan_t881159249  get_ticks_0() const { return ___ticks_0; }
	inline TimeSpan_t881159249 * get_address_of_ticks_0() { return &___ticks_0; }
	inline void set_ticks_0(TimeSpan_t881159249  value)
	{
		___ticks_0 = value;
	}

	inline static int32_t get_offset_of_kind_1() { return static_cast<int32_t>(offsetof(DateTime_t3738529785, ___kind_1)); }
	inline int32_t get_kind_1() const { return ___kind_1; }
	inline int32_t* get_address_of_kind_1() { return &___kind_1; }
	inline void set_kind_1(int32_t value)
	{
		___kind_1 = value;
	}
};

struct DateTime_t3738529785_StaticFields
{
public:
	// System.DateTime System.DateTime::MaxValue
	DateTime_t3738529785  ___MaxValue_2;
	// System.DateTime System.DateTime::MinValue
	DateTime_t3738529785  ___MinValue_3;
	// System.String[] System.DateTime::ParseTimeFormats
	StringU5BU5D_t1281789340* ___ParseTimeFormats_4;
	// System.String[] System.DateTime::ParseYearDayMonthFormats
	StringU5BU5D_t1281789340* ___ParseYearDayMonthFormats_5;
	// System.String[] System.DateTime::ParseYearMonthDayFormats
	StringU5BU5D_t1281789340* ___ParseYearMonthDayFormats_6;
	// System.String[] System.DateTime::ParseDayMonthYearFormats
	StringU5BU5D_t1281789340* ___ParseDayMonthYearFormats_7;
	// System.String[] System.DateTime::ParseMonthDayYearFormats
	StringU5BU5D_t1281789340* ___ParseMonthDayYearFormats_8;
	// System.String[] System.DateTime::MonthDayShortFormats
	StringU5BU5D_t1281789340* ___MonthDayShortFormats_9;
	// System.String[] System.DateTime::DayMonthShortFormats
	StringU5BU5D_t1281789340* ___DayMonthShortFormats_10;
	// System.Int32[] System.DateTime::daysmonth
	Int32U5BU5D_t385246372* ___daysmonth_11;
	// System.Int32[] System.DateTime::daysmonthleap
	Int32U5BU5D_t385246372* ___daysmonthleap_12;
	// System.Object System.DateTime::to_local_time_span_object
	RuntimeObject * ___to_local_time_span_object_13;
	// System.Int64 System.DateTime::last_now
	int64_t ___last_now_14;

public:
	inline static int32_t get_offset_of_MaxValue_2() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MaxValue_2)); }
	inline DateTime_t3738529785  get_MaxValue_2() const { return ___MaxValue_2; }
	inline DateTime_t3738529785 * get_address_of_MaxValue_2() { return &___MaxValue_2; }
	inline void set_MaxValue_2(DateTime_t3738529785  value)
	{
		___MaxValue_2 = value;
	}

	inline static int32_t get_offset_of_MinValue_3() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MinValue_3)); }
	inline DateTime_t3738529785  get_MinValue_3() const { return ___MinValue_3; }
	inline DateTime_t3738529785 * get_address_of_MinValue_3() { return &___MinValue_3; }
	inline void set_MinValue_3(DateTime_t3738529785  value)
	{
		___MinValue_3 = value;
	}

	inline static int32_t get_offset_of_ParseTimeFormats_4() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseTimeFormats_4)); }
	inline StringU5BU5D_t1281789340* get_ParseTimeFormats_4() const { return ___ParseTimeFormats_4; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseTimeFormats_4() { return &___ParseTimeFormats_4; }
	inline void set_ParseTimeFormats_4(StringU5BU5D_t1281789340* value)
	{
		___ParseTimeFormats_4 = value;
		Il2CppCodeGenWriteBarrier((&___ParseTimeFormats_4), value);
	}

	inline static int32_t get_offset_of_ParseYearDayMonthFormats_5() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearDayMonthFormats_5)); }
	inline StringU5BU5D_t1281789340* get_ParseYearDayMonthFormats_5() const { return ___ParseYearDayMonthFormats_5; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearDayMonthFormats_5() { return &___ParseYearDayMonthFormats_5; }
	inline void set_ParseYearDayMonthFormats_5(StringU5BU5D_t1281789340* value)
	{
		___ParseYearDayMonthFormats_5 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearDayMonthFormats_5), value);
	}

	inline static int32_t get_offset_of_ParseYearMonthDayFormats_6() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseYearMonthDayFormats_6)); }
	inline StringU5BU5D_t1281789340* get_ParseYearMonthDayFormats_6() const { return ___ParseYearMonthDayFormats_6; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseYearMonthDayFormats_6() { return &___ParseYearMonthDayFormats_6; }
	inline void set_ParseYearMonthDayFormats_6(StringU5BU5D_t1281789340* value)
	{
		___ParseYearMonthDayFormats_6 = value;
		Il2CppCodeGenWriteBarrier((&___ParseYearMonthDayFormats_6), value);
	}

	inline static int32_t get_offset_of_ParseDayMonthYearFormats_7() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseDayMonthYearFormats_7)); }
	inline StringU5BU5D_t1281789340* get_ParseDayMonthYearFormats_7() const { return ___ParseDayMonthYearFormats_7; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseDayMonthYearFormats_7() { return &___ParseDayMonthYearFormats_7; }
	inline void set_ParseDayMonthYearFormats_7(StringU5BU5D_t1281789340* value)
	{
		___ParseDayMonthYearFormats_7 = value;
		Il2CppCodeGenWriteBarrier((&___ParseDayMonthYearFormats_7), value);
	}

	inline static int32_t get_offset_of_ParseMonthDayYearFormats_8() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___ParseMonthDayYearFormats_8)); }
	inline StringU5BU5D_t1281789340* get_ParseMonthDayYearFormats_8() const { return ___ParseMonthDayYearFormats_8; }
	inline StringU5BU5D_t1281789340** get_address_of_ParseMonthDayYearFormats_8() { return &___ParseMonthDayYearFormats_8; }
	inline void set_ParseMonthDayYearFormats_8(StringU5BU5D_t1281789340* value)
	{
		___ParseMonthDayYearFormats_8 = value;
		Il2CppCodeGenWriteBarrier((&___ParseMonthDayYearFormats_8), value);
	}

	inline static int32_t get_offset_of_MonthDayShortFormats_9() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___MonthDayShortFormats_9)); }
	inline StringU5BU5D_t1281789340* get_MonthDayShortFormats_9() const { return ___MonthDayShortFormats_9; }
	inline StringU5BU5D_t1281789340** get_address_of_MonthDayShortFormats_9() { return &___MonthDayShortFormats_9; }
	inline void set_MonthDayShortFormats_9(StringU5BU5D_t1281789340* value)
	{
		___MonthDayShortFormats_9 = value;
		Il2CppCodeGenWriteBarrier((&___MonthDayShortFormats_9), value);
	}

	inline static int32_t get_offset_of_DayMonthShortFormats_10() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___DayMonthShortFormats_10)); }
	inline StringU5BU5D_t1281789340* get_DayMonthShortFormats_10() const { return ___DayMonthShortFormats_10; }
	inline StringU5BU5D_t1281789340** get_address_of_DayMonthShortFormats_10() { return &___DayMonthShortFormats_10; }
	inline void set_DayMonthShortFormats_10(StringU5BU5D_t1281789340* value)
	{
		___DayMonthShortFormats_10 = value;
		Il2CppCodeGenWriteBarrier((&___DayMonthShortFormats_10), value);
	}

	inline static int32_t get_offset_of_daysmonth_11() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonth_11)); }
	inline Int32U5BU5D_t385246372* get_daysmonth_11() const { return ___daysmonth_11; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonth_11() { return &___daysmonth_11; }
	inline void set_daysmonth_11(Int32U5BU5D_t385246372* value)
	{
		___daysmonth_11 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonth_11), value);
	}

	inline static int32_t get_offset_of_daysmonthleap_12() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___daysmonthleap_12)); }
	inline Int32U5BU5D_t385246372* get_daysmonthleap_12() const { return ___daysmonthleap_12; }
	inline Int32U5BU5D_t385246372** get_address_of_daysmonthleap_12() { return &___daysmonthleap_12; }
	inline void set_daysmonthleap_12(Int32U5BU5D_t385246372* value)
	{
		___daysmonthleap_12 = value;
		Il2CppCodeGenWriteBarrier((&___daysmonthleap_12), value);
	}

	inline static int32_t get_offset_of_to_local_time_span_object_13() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___to_local_time_span_object_13)); }
	inline RuntimeObject * get_to_local_time_span_object_13() const { return ___to_local_time_span_object_13; }
	inline RuntimeObject ** get_address_of_to_local_time_span_object_13() { return &___to_local_time_span_object_13; }
	inline void set_to_local_time_span_object_13(RuntimeObject * value)
	{
		___to_local_time_span_object_13 = value;
		Il2CppCodeGenWriteBarrier((&___to_local_time_span_object_13), value);
	}

	inline static int32_t get_offset_of_last_now_14() { return static_cast<int32_t>(offsetof(DateTime_t3738529785_StaticFields, ___last_now_14)); }
	inline int64_t get_last_now_14() const { return ___last_now_14; }
	inline int64_t* get_address_of_last_now_14() { return &___last_now_14; }
	inline void set_last_now_14(int64_t value)
	{
		___last_now_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T3738529785_H
#ifndef NULLABLE_1_T3820632816_H
#define NULLABLE_1_T3820632816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Facebook.Unity.FacebookUnityPlatform>
struct  Nullable_1_t3820632816 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t3820632816, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t3820632816, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T3820632816_H
#ifndef SCRIPTABLEOBJECT_T2528358522_H
#define SCRIPTABLEOBJECT_T2528358522_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_t2528358522  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_pinvoke : public Object_t631007953_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_t2528358522_marshaled_com : public Object_t631007953_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_T2528358522_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef EDITORFACEBOOK_T2350935743_H
#define EDITORFACEBOOK_T2350935743_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.EditorFacebook
struct  EditorFacebook_t2350935743  : public FacebookBase_t1615169142
{
public:
	// Facebook.Unity.Editor.IEditorWrapper Facebook.Unity.Editor.EditorFacebook::editorWrapper
	RuntimeObject* ___editorWrapper_3;
	// System.Boolean Facebook.Unity.Editor.EditorFacebook::<LimitEventUsage>k__BackingField
	bool ___U3CLimitEventUsageU3Ek__BackingField_4;
	// Facebook.Unity.ShareDialogMode Facebook.Unity.Editor.EditorFacebook::<ShareDialogMode>k__BackingField
	int32_t ___U3CShareDialogModeU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_editorWrapper_3() { return static_cast<int32_t>(offsetof(EditorFacebook_t2350935743, ___editorWrapper_3)); }
	inline RuntimeObject* get_editorWrapper_3() const { return ___editorWrapper_3; }
	inline RuntimeObject** get_address_of_editorWrapper_3() { return &___editorWrapper_3; }
	inline void set_editorWrapper_3(RuntimeObject* value)
	{
		___editorWrapper_3 = value;
		Il2CppCodeGenWriteBarrier((&___editorWrapper_3), value);
	}

	inline static int32_t get_offset_of_U3CLimitEventUsageU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(EditorFacebook_t2350935743, ___U3CLimitEventUsageU3Ek__BackingField_4)); }
	inline bool get_U3CLimitEventUsageU3Ek__BackingField_4() const { return ___U3CLimitEventUsageU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CLimitEventUsageU3Ek__BackingField_4() { return &___U3CLimitEventUsageU3Ek__BackingField_4; }
	inline void set_U3CLimitEventUsageU3Ek__BackingField_4(bool value)
	{
		___U3CLimitEventUsageU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CShareDialogModeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(EditorFacebook_t2350935743, ___U3CShareDialogModeU3Ek__BackingField_5)); }
	inline int32_t get_U3CShareDialogModeU3Ek__BackingField_5() const { return ___U3CShareDialogModeU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CShareDialogModeU3Ek__BackingField_5() { return &___U3CShareDialogModeU3Ek__BackingField_5; }
	inline void set_U3CShareDialogModeU3Ek__BackingField_5(int32_t value)
	{
		___U3CShareDialogModeU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORFACEBOOK_T2350935743_H
#ifndef APPREQUESTRESULT_T635749912_H
#define APPREQUESTRESULT_T635749912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AppRequestResult
struct  AppRequestResult_t635749912  : public ResultBase_t777682874
{
public:
	// System.String Facebook.Unity.AppRequestResult::<RequestID>k__BackingField
	String_t* ___U3CRequestIDU3Ek__BackingField_6;
	// System.Collections.Generic.IEnumerable`1<System.String> Facebook.Unity.AppRequestResult::<To>k__BackingField
	RuntimeObject* ___U3CToU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CRequestIDU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AppRequestResult_t635749912, ___U3CRequestIDU3Ek__BackingField_6)); }
	inline String_t* get_U3CRequestIDU3Ek__BackingField_6() const { return ___U3CRequestIDU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CRequestIDU3Ek__BackingField_6() { return &___U3CRequestIDU3Ek__BackingField_6; }
	inline void set_U3CRequestIDU3Ek__BackingField_6(String_t* value)
	{
		___U3CRequestIDU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRequestIDU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CToU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AppRequestResult_t635749912, ___U3CToU3Ek__BackingField_7)); }
	inline RuntimeObject* get_U3CToU3Ek__BackingField_7() const { return ___U3CToU3Ek__BackingField_7; }
	inline RuntimeObject** get_address_of_U3CToU3Ek__BackingField_7() { return &___U3CToU3Ek__BackingField_7; }
	inline void set_U3CToU3Ek__BackingField_7(RuntimeObject* value)
	{
		___U3CToU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CToU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPREQUESTRESULT_T635749912_H
#ifndef APPLINKRESULT_T495655548_H
#define APPLINKRESULT_T495655548_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AppLinkResult
struct  AppLinkResult_t495655548  : public ResultBase_t777682874
{
public:
	// System.String Facebook.Unity.AppLinkResult::<Url>k__BackingField
	String_t* ___U3CUrlU3Ek__BackingField_6;
	// System.String Facebook.Unity.AppLinkResult::<TargetUrl>k__BackingField
	String_t* ___U3CTargetUrlU3Ek__BackingField_7;
	// System.String Facebook.Unity.AppLinkResult::<Ref>k__BackingField
	String_t* ___U3CRefU3Ek__BackingField_8;
	// System.Collections.Generic.IDictionary`2<System.String,System.Object> Facebook.Unity.AppLinkResult::<Extras>k__BackingField
	RuntimeObject* ___U3CExtrasU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_U3CUrlU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AppLinkResult_t495655548, ___U3CUrlU3Ek__BackingField_6)); }
	inline String_t* get_U3CUrlU3Ek__BackingField_6() const { return ___U3CUrlU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CUrlU3Ek__BackingField_6() { return &___U3CUrlU3Ek__BackingField_6; }
	inline void set_U3CUrlU3Ek__BackingField_6(String_t* value)
	{
		___U3CUrlU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUrlU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CTargetUrlU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(AppLinkResult_t495655548, ___U3CTargetUrlU3Ek__BackingField_7)); }
	inline String_t* get_U3CTargetUrlU3Ek__BackingField_7() const { return ___U3CTargetUrlU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CTargetUrlU3Ek__BackingField_7() { return &___U3CTargetUrlU3Ek__BackingField_7; }
	inline void set_U3CTargetUrlU3Ek__BackingField_7(String_t* value)
	{
		___U3CTargetUrlU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTargetUrlU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CRefU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(AppLinkResult_t495655548, ___U3CRefU3Ek__BackingField_8)); }
	inline String_t* get_U3CRefU3Ek__BackingField_8() const { return ___U3CRefU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CRefU3Ek__BackingField_8() { return &___U3CRefU3Ek__BackingField_8; }
	inline void set_U3CRefU3Ek__BackingField_8(String_t* value)
	{
		___U3CRefU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRefU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CExtrasU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(AppLinkResult_t495655548, ___U3CExtrasU3Ek__BackingField_9)); }
	inline RuntimeObject* get_U3CExtrasU3Ek__BackingField_9() const { return ___U3CExtrasU3Ek__BackingField_9; }
	inline RuntimeObject** get_address_of_U3CExtrasU3Ek__BackingField_9() { return &___U3CExtrasU3Ek__BackingField_9; }
	inline void set_U3CExtrasU3Ek__BackingField_9(RuntimeObject* value)
	{
		___U3CExtrasU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtrasU3Ek__BackingField_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPLINKRESULT_T495655548_H
#ifndef SHARERESULT_T3324833527_H
#define SHARERESULT_T3324833527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.ShareResult
struct  ShareResult_t3324833527  : public ResultBase_t777682874
{
public:
	// System.String Facebook.Unity.ShareResult::<PostId>k__BackingField
	String_t* ___U3CPostIdU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CPostIdU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ShareResult_t3324833527, ___U3CPostIdU3Ek__BackingField_6)); }
	inline String_t* get_U3CPostIdU3Ek__BackingField_6() const { return ___U3CPostIdU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CPostIdU3Ek__BackingField_6() { return &___U3CPostIdU3Ek__BackingField_6; }
	inline void set_U3CPostIdU3Ek__BackingField_6(String_t* value)
	{
		___U3CPostIdU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPostIdU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHARERESULT_T3324833527_H
#ifndef GRAPHRESULT_T1918224463_H
#define GRAPHRESULT_T1918224463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.GraphResult
struct  GraphResult_t1918224463  : public ResultBase_t777682874
{
public:
	// System.Collections.Generic.IList`1<System.Object> Facebook.Unity.GraphResult::<ResultList>k__BackingField
	RuntimeObject* ___U3CResultListU3Ek__BackingField_6;
	// UnityEngine.Texture2D Facebook.Unity.GraphResult::<Texture>k__BackingField
	Texture2D_t3840446185 * ___U3CTextureU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CResultListU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(GraphResult_t1918224463, ___U3CResultListU3Ek__BackingField_6)); }
	inline RuntimeObject* get_U3CResultListU3Ek__BackingField_6() const { return ___U3CResultListU3Ek__BackingField_6; }
	inline RuntimeObject** get_address_of_U3CResultListU3Ek__BackingField_6() { return &___U3CResultListU3Ek__BackingField_6; }
	inline void set_U3CResultListU3Ek__BackingField_6(RuntimeObject* value)
	{
		___U3CResultListU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResultListU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CTextureU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(GraphResult_t1918224463, ___U3CTextureU3Ek__BackingField_7)); }
	inline Texture2D_t3840446185 * get_U3CTextureU3Ek__BackingField_7() const { return ___U3CTextureU3Ek__BackingField_7; }
	inline Texture2D_t3840446185 ** get_address_of_U3CTextureU3Ek__BackingField_7() { return &___U3CTextureU3Ek__BackingField_7; }
	inline void set_U3CTextureU3Ek__BackingField_7(Texture2D_t3840446185 * value)
	{
		___U3CTextureU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextureU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GRAPHRESULT_T1918224463_H
#ifndef LOGINRESULT_T3585129065_H
#define LOGINRESULT_T3585129065_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.LoginResult
struct  LoginResult_t3585129065  : public ResultBase_t777682874
{
public:
	// Facebook.Unity.AccessToken Facebook.Unity.LoginResult::<AccessToken>k__BackingField
	AccessToken_t2431487013 * ___U3CAccessTokenU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CAccessTokenU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(LoginResult_t3585129065, ___U3CAccessTokenU3Ek__BackingField_10)); }
	inline AccessToken_t2431487013 * get_U3CAccessTokenU3Ek__BackingField_10() const { return ___U3CAccessTokenU3Ek__BackingField_10; }
	inline AccessToken_t2431487013 ** get_address_of_U3CAccessTokenU3Ek__BackingField_10() { return &___U3CAccessTokenU3Ek__BackingField_10; }
	inline void set_U3CAccessTokenU3Ek__BackingField_10(AccessToken_t2431487013 * value)
	{
		___U3CAccessTokenU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAccessTokenU3Ek__BackingField_10), value);
	}
};

struct LoginResult_t3585129065_StaticFields
{
public:
	// System.String Facebook.Unity.LoginResult::UserIdKey
	String_t* ___UserIdKey_6;
	// System.String Facebook.Unity.LoginResult::ExpirationTimestampKey
	String_t* ___ExpirationTimestampKey_7;
	// System.String Facebook.Unity.LoginResult::PermissionsKey
	String_t* ___PermissionsKey_8;
	// System.String Facebook.Unity.LoginResult::AccessTokenKey
	String_t* ___AccessTokenKey_9;

public:
	inline static int32_t get_offset_of_UserIdKey_6() { return static_cast<int32_t>(offsetof(LoginResult_t3585129065_StaticFields, ___UserIdKey_6)); }
	inline String_t* get_UserIdKey_6() const { return ___UserIdKey_6; }
	inline String_t** get_address_of_UserIdKey_6() { return &___UserIdKey_6; }
	inline void set_UserIdKey_6(String_t* value)
	{
		___UserIdKey_6 = value;
		Il2CppCodeGenWriteBarrier((&___UserIdKey_6), value);
	}

	inline static int32_t get_offset_of_ExpirationTimestampKey_7() { return static_cast<int32_t>(offsetof(LoginResult_t3585129065_StaticFields, ___ExpirationTimestampKey_7)); }
	inline String_t* get_ExpirationTimestampKey_7() const { return ___ExpirationTimestampKey_7; }
	inline String_t** get_address_of_ExpirationTimestampKey_7() { return &___ExpirationTimestampKey_7; }
	inline void set_ExpirationTimestampKey_7(String_t* value)
	{
		___ExpirationTimestampKey_7 = value;
		Il2CppCodeGenWriteBarrier((&___ExpirationTimestampKey_7), value);
	}

	inline static int32_t get_offset_of_PermissionsKey_8() { return static_cast<int32_t>(offsetof(LoginResult_t3585129065_StaticFields, ___PermissionsKey_8)); }
	inline String_t* get_PermissionsKey_8() const { return ___PermissionsKey_8; }
	inline String_t** get_address_of_PermissionsKey_8() { return &___PermissionsKey_8; }
	inline void set_PermissionsKey_8(String_t* value)
	{
		___PermissionsKey_8 = value;
		Il2CppCodeGenWriteBarrier((&___PermissionsKey_8), value);
	}

	inline static int32_t get_offset_of_AccessTokenKey_9() { return static_cast<int32_t>(offsetof(LoginResult_t3585129065_StaticFields, ___AccessTokenKey_9)); }
	inline String_t* get_AccessTokenKey_9() const { return ___AccessTokenKey_9; }
	inline String_t** get_address_of_AccessTokenKey_9() { return &___AccessTokenKey_9; }
	inline void set_AccessTokenKey_9(String_t* value)
	{
		___AccessTokenKey_9 = value;
		Il2CppCodeGenWriteBarrier((&___AccessTokenKey_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINRESULT_T3585129065_H
#ifndef PAYRESULT_T2637031204_H
#define PAYRESULT_T2637031204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.PayResult
struct  PayResult_t2637031204  : public ResultBase_t777682874
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAYRESULT_T2637031204_H
#ifndef APPINVITERESULT_T1043157067_H
#define APPINVITERESULT_T1043157067_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AppInviteResult
struct  AppInviteResult_t1043157067  : public ResultBase_t777682874
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // APPINVITERESULT_T1043157067_H
#ifndef INITDELEGATE_T3081360126_H
#define INITDELEGATE_T3081360126_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.InitDelegate
struct  InitDelegate_t3081360126  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITDELEGATE_T3081360126_H
#ifndef NULLABLE_1_T1166124571_H
#define NULLABLE_1_T1166124571_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.DateTime>
struct  Nullable_1_t1166124571 
{
public:
	// T System.Nullable`1::value
	DateTime_t3738529785  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1166124571, ___value_0)); }
	inline DateTime_t3738529785  get_value_0() const { return ___value_0; }
	inline DateTime_t3738529785 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(DateTime_t3738529785  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1166124571, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1166124571_H
#ifndef IOSFACEBOOK_T793364667_H
#define IOSFACEBOOK_T793364667_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.IOS.IOSFacebook
struct  IOSFacebook_t793364667  : public MobileFacebook_t4202726836
{
public:
	// System.Boolean Facebook.Unity.Mobile.IOS.IOSFacebook::limitEventUsage
	bool ___limitEventUsage_4;
	// Facebook.Unity.Mobile.IOS.IIOSWrapper Facebook.Unity.Mobile.IOS.IOSFacebook::iosWrapper
	RuntimeObject* ___iosWrapper_5;

public:
	inline static int32_t get_offset_of_limitEventUsage_4() { return static_cast<int32_t>(offsetof(IOSFacebook_t793364667, ___limitEventUsage_4)); }
	inline bool get_limitEventUsage_4() const { return ___limitEventUsage_4; }
	inline bool* get_address_of_limitEventUsage_4() { return &___limitEventUsage_4; }
	inline void set_limitEventUsage_4(bool value)
	{
		___limitEventUsage_4 = value;
	}

	inline static int32_t get_offset_of_iosWrapper_5() { return static_cast<int32_t>(offsetof(IOSFacebook_t793364667, ___iosWrapper_5)); }
	inline RuntimeObject* get_iosWrapper_5() const { return ___iosWrapper_5; }
	inline RuntimeObject** get_address_of_iosWrapper_5() { return &___iosWrapper_5; }
	inline void set_iosWrapper_5(RuntimeObject* value)
	{
		___iosWrapper_5 = value;
		Il2CppCodeGenWriteBarrier((&___iosWrapper_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSFACEBOOK_T793364667_H
#ifndef CONSTANTS_T1121581803_H
#define CONSTANTS_T1121581803_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Constants
struct  Constants_t1121581803  : public RuntimeObject
{
public:

public:
};

struct Constants_t1121581803_StaticFields
{
public:
	// System.Nullable`1<Facebook.Unity.FacebookUnityPlatform> Facebook.Unity.Constants::currentPlatform
	Nullable_1_t3820632816  ___currentPlatform_0;

public:
	inline static int32_t get_offset_of_currentPlatform_0() { return static_cast<int32_t>(offsetof(Constants_t1121581803_StaticFields, ___currentPlatform_0)); }
	inline Nullable_1_t3820632816  get_currentPlatform_0() const { return ___currentPlatform_0; }
	inline Nullable_1_t3820632816 * get_address_of_currentPlatform_0() { return &___currentPlatform_0; }
	inline void set_currentPlatform_0(Nullable_1_t3820632816  value)
	{
		___currentPlatform_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTANTS_T1121581803_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef FB_T2178373596_H
#define FB_T2178373596_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FB
struct  FB_t2178373596  : public ScriptableObject_t2528358522
{
public:

public:
};

struct FB_t2178373596_StaticFields
{
public:
	// Facebook.Unity.IFacebook Facebook.Unity.FB::facebook
	RuntimeObject* ___facebook_3;
	// System.Boolean Facebook.Unity.FB::isInitCalled
	bool ___isInitCalled_4;
	// System.String Facebook.Unity.FB::facebookDomain
	String_t* ___facebookDomain_5;
	// System.String Facebook.Unity.FB::graphApiVersion
	String_t* ___graphApiVersion_6;
	// System.String Facebook.Unity.FB::<AppId>k__BackingField
	String_t* ___U3CAppIdU3Ek__BackingField_7;
	// System.String Facebook.Unity.FB::<ClientToken>k__BackingField
	String_t* ___U3CClientTokenU3Ek__BackingField_8;
	// Facebook.Unity.FB/OnDLLLoaded Facebook.Unity.FB::<OnDLLLoadedDelegate>k__BackingField
	OnDLLLoaded_t2963647862 * ___U3COnDLLLoadedDelegateU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_facebook_3() { return static_cast<int32_t>(offsetof(FB_t2178373596_StaticFields, ___facebook_3)); }
	inline RuntimeObject* get_facebook_3() const { return ___facebook_3; }
	inline RuntimeObject** get_address_of_facebook_3() { return &___facebook_3; }
	inline void set_facebook_3(RuntimeObject* value)
	{
		___facebook_3 = value;
		Il2CppCodeGenWriteBarrier((&___facebook_3), value);
	}

	inline static int32_t get_offset_of_isInitCalled_4() { return static_cast<int32_t>(offsetof(FB_t2178373596_StaticFields, ___isInitCalled_4)); }
	inline bool get_isInitCalled_4() const { return ___isInitCalled_4; }
	inline bool* get_address_of_isInitCalled_4() { return &___isInitCalled_4; }
	inline void set_isInitCalled_4(bool value)
	{
		___isInitCalled_4 = value;
	}

	inline static int32_t get_offset_of_facebookDomain_5() { return static_cast<int32_t>(offsetof(FB_t2178373596_StaticFields, ___facebookDomain_5)); }
	inline String_t* get_facebookDomain_5() const { return ___facebookDomain_5; }
	inline String_t** get_address_of_facebookDomain_5() { return &___facebookDomain_5; }
	inline void set_facebookDomain_5(String_t* value)
	{
		___facebookDomain_5 = value;
		Il2CppCodeGenWriteBarrier((&___facebookDomain_5), value);
	}

	inline static int32_t get_offset_of_graphApiVersion_6() { return static_cast<int32_t>(offsetof(FB_t2178373596_StaticFields, ___graphApiVersion_6)); }
	inline String_t* get_graphApiVersion_6() const { return ___graphApiVersion_6; }
	inline String_t** get_address_of_graphApiVersion_6() { return &___graphApiVersion_6; }
	inline void set_graphApiVersion_6(String_t* value)
	{
		___graphApiVersion_6 = value;
		Il2CppCodeGenWriteBarrier((&___graphApiVersion_6), value);
	}

	inline static int32_t get_offset_of_U3CAppIdU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FB_t2178373596_StaticFields, ___U3CAppIdU3Ek__BackingField_7)); }
	inline String_t* get_U3CAppIdU3Ek__BackingField_7() const { return ___U3CAppIdU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CAppIdU3Ek__BackingField_7() { return &___U3CAppIdU3Ek__BackingField_7; }
	inline void set_U3CAppIdU3Ek__BackingField_7(String_t* value)
	{
		___U3CAppIdU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAppIdU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CClientTokenU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FB_t2178373596_StaticFields, ___U3CClientTokenU3Ek__BackingField_8)); }
	inline String_t* get_U3CClientTokenU3Ek__BackingField_8() const { return ___U3CClientTokenU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CClientTokenU3Ek__BackingField_8() { return &___U3CClientTokenU3Ek__BackingField_8; }
	inline void set_U3CClientTokenU3Ek__BackingField_8(String_t* value)
	{
		___U3CClientTokenU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClientTokenU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3COnDLLLoadedDelegateU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(FB_t2178373596_StaticFields, ___U3COnDLLLoadedDelegateU3Ek__BackingField_9)); }
	inline OnDLLLoaded_t2963647862 * get_U3COnDLLLoadedDelegateU3Ek__BackingField_9() const { return ___U3COnDLLLoadedDelegateU3Ek__BackingField_9; }
	inline OnDLLLoaded_t2963647862 ** get_address_of_U3COnDLLLoadedDelegateU3Ek__BackingField_9() { return &___U3COnDLLLoadedDelegateU3Ek__BackingField_9; }
	inline void set_U3COnDLLLoadedDelegateU3Ek__BackingField_9(OnDLLLoaded_t2963647862 * value)
	{
		___U3COnDLLLoadedDelegateU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnDLLLoadedDelegateU3Ek__BackingField_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FB_T2178373596_H
#ifndef ONDLLLOADED_T2963647862_H
#define ONDLLLOADED_T2963647862_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FB/OnDLLLoaded
struct  OnDLLLoaded_t2963647862  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDLLLOADED_T2963647862_H
#ifndef ONCOMPLETE_T3786498944_H
#define ONCOMPLETE_T3786498944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Gameroom.GameroomFacebook/OnComplete
struct  OnComplete_t3786498944  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCOMPLETE_T3786498944_H
#ifndef HIDEUNITYDELEGATE_T1353799728_H
#define HIDEUNITYDELEGATE_T1353799728_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.HideUnityDelegate
struct  HideUnityDelegate_t1353799728  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HIDEUNITYDELEGATE_T1353799728_H
#ifndef ACCESSTOKEN_T2431487013_H
#define ACCESSTOKEN_T2431487013_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AccessToken
struct  AccessToken_t2431487013  : public RuntimeObject
{
public:
	// System.String Facebook.Unity.AccessToken::<TokenString>k__BackingField
	String_t* ___U3CTokenStringU3Ek__BackingField_1;
	// System.DateTime Facebook.Unity.AccessToken::<ExpirationTime>k__BackingField
	DateTime_t3738529785  ___U3CExpirationTimeU3Ek__BackingField_2;
	// System.Collections.Generic.IEnumerable`1<System.String> Facebook.Unity.AccessToken::<Permissions>k__BackingField
	RuntimeObject* ___U3CPermissionsU3Ek__BackingField_3;
	// System.String Facebook.Unity.AccessToken::<UserId>k__BackingField
	String_t* ___U3CUserIdU3Ek__BackingField_4;
	// System.Nullable`1<System.DateTime> Facebook.Unity.AccessToken::<LastRefresh>k__BackingField
	Nullable_1_t1166124571  ___U3CLastRefreshU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CTokenStringU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AccessToken_t2431487013, ___U3CTokenStringU3Ek__BackingField_1)); }
	inline String_t* get_U3CTokenStringU3Ek__BackingField_1() const { return ___U3CTokenStringU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CTokenStringU3Ek__BackingField_1() { return &___U3CTokenStringU3Ek__BackingField_1; }
	inline void set_U3CTokenStringU3Ek__BackingField_1(String_t* value)
	{
		___U3CTokenStringU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTokenStringU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CExpirationTimeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(AccessToken_t2431487013, ___U3CExpirationTimeU3Ek__BackingField_2)); }
	inline DateTime_t3738529785  get_U3CExpirationTimeU3Ek__BackingField_2() const { return ___U3CExpirationTimeU3Ek__BackingField_2; }
	inline DateTime_t3738529785 * get_address_of_U3CExpirationTimeU3Ek__BackingField_2() { return &___U3CExpirationTimeU3Ek__BackingField_2; }
	inline void set_U3CExpirationTimeU3Ek__BackingField_2(DateTime_t3738529785  value)
	{
		___U3CExpirationTimeU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CPermissionsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(AccessToken_t2431487013, ___U3CPermissionsU3Ek__BackingField_3)); }
	inline RuntimeObject* get_U3CPermissionsU3Ek__BackingField_3() const { return ___U3CPermissionsU3Ek__BackingField_3; }
	inline RuntimeObject** get_address_of_U3CPermissionsU3Ek__BackingField_3() { return &___U3CPermissionsU3Ek__BackingField_3; }
	inline void set_U3CPermissionsU3Ek__BackingField_3(RuntimeObject* value)
	{
		___U3CPermissionsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPermissionsU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CUserIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AccessToken_t2431487013, ___U3CUserIdU3Ek__BackingField_4)); }
	inline String_t* get_U3CUserIdU3Ek__BackingField_4() const { return ___U3CUserIdU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CUserIdU3Ek__BackingField_4() { return &___U3CUserIdU3Ek__BackingField_4; }
	inline void set_U3CUserIdU3Ek__BackingField_4(String_t* value)
	{
		___U3CUserIdU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserIdU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CLastRefreshU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AccessToken_t2431487013, ___U3CLastRefreshU3Ek__BackingField_5)); }
	inline Nullable_1_t1166124571  get_U3CLastRefreshU3Ek__BackingField_5() const { return ___U3CLastRefreshU3Ek__BackingField_5; }
	inline Nullable_1_t1166124571 * get_address_of_U3CLastRefreshU3Ek__BackingField_5() { return &___U3CLastRefreshU3Ek__BackingField_5; }
	inline void set_U3CLastRefreshU3Ek__BackingField_5(Nullable_1_t1166124571  value)
	{
		___U3CLastRefreshU3Ek__BackingField_5 = value;
	}
};

struct AccessToken_t2431487013_StaticFields
{
public:
	// Facebook.Unity.AccessToken Facebook.Unity.AccessToken::<CurrentAccessToken>k__BackingField
	AccessToken_t2431487013 * ___U3CCurrentAccessTokenU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CCurrentAccessTokenU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AccessToken_t2431487013_StaticFields, ___U3CCurrentAccessTokenU3Ek__BackingField_0)); }
	inline AccessToken_t2431487013 * get_U3CCurrentAccessTokenU3Ek__BackingField_0() const { return ___U3CCurrentAccessTokenU3Ek__BackingField_0; }
	inline AccessToken_t2431487013 ** get_address_of_U3CCurrentAccessTokenU3Ek__BackingField_0() { return &___U3CCurrentAccessTokenU3Ek__BackingField_0; }
	inline void set_U3CCurrentAccessTokenU3Ek__BackingField_0(AccessToken_t2431487013 * value)
	{
		___U3CCurrentAccessTokenU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentAccessTokenU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACCESSTOKEN_T2431487013_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef ASYNCREQUESTSTRING_T3669574124_H
#define ASYNCREQUESTSTRING_T3669574124_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.AsyncRequestString
struct  AsyncRequestString_t3669574124  : public MonoBehaviour_t3962482529
{
public:
	// System.Uri Facebook.Unity.AsyncRequestString::url
	Uri_t100236324 * ___url_2;
	// Facebook.Unity.HttpMethod Facebook.Unity.AsyncRequestString::method
	int32_t ___method_3;
	// System.Collections.Generic.IDictionary`2<System.String,System.String> Facebook.Unity.AsyncRequestString::formData
	RuntimeObject* ___formData_4;
	// UnityEngine.WWWForm Facebook.Unity.AsyncRequestString::query
	WWWForm_t4064702195 * ___query_5;
	// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IGraphResult> Facebook.Unity.AsyncRequestString::callback
	FacebookDelegate_1_t138390958 * ___callback_6;

public:
	inline static int32_t get_offset_of_url_2() { return static_cast<int32_t>(offsetof(AsyncRequestString_t3669574124, ___url_2)); }
	inline Uri_t100236324 * get_url_2() const { return ___url_2; }
	inline Uri_t100236324 ** get_address_of_url_2() { return &___url_2; }
	inline void set_url_2(Uri_t100236324 * value)
	{
		___url_2 = value;
		Il2CppCodeGenWriteBarrier((&___url_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(AsyncRequestString_t3669574124, ___method_3)); }
	inline int32_t get_method_3() const { return ___method_3; }
	inline int32_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(int32_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_formData_4() { return static_cast<int32_t>(offsetof(AsyncRequestString_t3669574124, ___formData_4)); }
	inline RuntimeObject* get_formData_4() const { return ___formData_4; }
	inline RuntimeObject** get_address_of_formData_4() { return &___formData_4; }
	inline void set_formData_4(RuntimeObject* value)
	{
		___formData_4 = value;
		Il2CppCodeGenWriteBarrier((&___formData_4), value);
	}

	inline static int32_t get_offset_of_query_5() { return static_cast<int32_t>(offsetof(AsyncRequestString_t3669574124, ___query_5)); }
	inline WWWForm_t4064702195 * get_query_5() const { return ___query_5; }
	inline WWWForm_t4064702195 ** get_address_of_query_5() { return &___query_5; }
	inline void set_query_5(WWWForm_t4064702195 * value)
	{
		___query_5 = value;
		Il2CppCodeGenWriteBarrier((&___query_5), value);
	}

	inline static int32_t get_offset_of_callback_6() { return static_cast<int32_t>(offsetof(AsyncRequestString_t3669574124, ___callback_6)); }
	inline FacebookDelegate_1_t138390958 * get_callback_6() const { return ___callback_6; }
	inline FacebookDelegate_1_t138390958 ** get_address_of_callback_6() { return &___callback_6; }
	inline void set_callback_6(FacebookDelegate_1_t138390958 * value)
	{
		___callback_6 = value;
		Il2CppCodeGenWriteBarrier((&___callback_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCREQUESTSTRING_T3669574124_H
#ifndef COMPILEDFACEBOOKLOADER_T350999866_H
#define COMPILEDFACEBOOKLOADER_T350999866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FB/CompiledFacebookLoader
struct  CompiledFacebookLoader_t350999866  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPILEDFACEBOOKLOADER_T350999866_H
#ifndef FACEBOOKSCHEDULER_T1007614953_H
#define FACEBOOKSCHEDULER_T1007614953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookScheduler
struct  FacebookScheduler_t1007614953  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKSCHEDULER_T1007614953_H
#ifndef FACEBOOKGAMEOBJECT_T3770693708_H
#define FACEBOOKGAMEOBJECT_T3770693708_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.FacebookGameObject
struct  FacebookGameObject_t3770693708  : public MonoBehaviour_t3962482529
{
public:
	// Facebook.Unity.IFacebookImplementation Facebook.Unity.FacebookGameObject::<Facebook>k__BackingField
	RuntimeObject* ___U3CFacebookU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CFacebookU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(FacebookGameObject_t3770693708, ___U3CFacebookU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CFacebookU3Ek__BackingField_2() const { return ___U3CFacebookU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CFacebookU3Ek__BackingField_2() { return &___U3CFacebookU3Ek__BackingField_2; }
	inline void set_U3CFacebookU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CFacebookU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFacebookU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKGAMEOBJECT_T3770693708_H
#ifndef EDITORFACEBOOKMOCKDIALOG_T3691766720_H
#define EDITORFACEBOOKMOCKDIALOG_T3691766720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.EditorFacebookMockDialog
struct  EditorFacebookMockDialog_t3691766720  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Rect Facebook.Unity.Editor.EditorFacebookMockDialog::modalRect
	Rect_t2360479859  ___modalRect_2;
	// UnityEngine.GUIStyle Facebook.Unity.Editor.EditorFacebookMockDialog::modalStyle
	GUIStyle_t3956901511 * ___modalStyle_3;
	// Facebook.Unity.Utilities/Callback`1<Facebook.Unity.ResultContainer> Facebook.Unity.Editor.EditorFacebookMockDialog::<Callback>k__BackingField
	Callback_1_t3507265931 * ___U3CCallbackU3Ek__BackingField_4;
	// System.String Facebook.Unity.Editor.EditorFacebookMockDialog::<CallbackID>k__BackingField
	String_t* ___U3CCallbackIDU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_modalRect_2() { return static_cast<int32_t>(offsetof(EditorFacebookMockDialog_t3691766720, ___modalRect_2)); }
	inline Rect_t2360479859  get_modalRect_2() const { return ___modalRect_2; }
	inline Rect_t2360479859 * get_address_of_modalRect_2() { return &___modalRect_2; }
	inline void set_modalRect_2(Rect_t2360479859  value)
	{
		___modalRect_2 = value;
	}

	inline static int32_t get_offset_of_modalStyle_3() { return static_cast<int32_t>(offsetof(EditorFacebookMockDialog_t3691766720, ___modalStyle_3)); }
	inline GUIStyle_t3956901511 * get_modalStyle_3() const { return ___modalStyle_3; }
	inline GUIStyle_t3956901511 ** get_address_of_modalStyle_3() { return &___modalStyle_3; }
	inline void set_modalStyle_3(GUIStyle_t3956901511 * value)
	{
		___modalStyle_3 = value;
		Il2CppCodeGenWriteBarrier((&___modalStyle_3), value);
	}

	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(EditorFacebookMockDialog_t3691766720, ___U3CCallbackU3Ek__BackingField_4)); }
	inline Callback_1_t3507265931 * get_U3CCallbackU3Ek__BackingField_4() const { return ___U3CCallbackU3Ek__BackingField_4; }
	inline Callback_1_t3507265931 ** get_address_of_U3CCallbackU3Ek__BackingField_4() { return &___U3CCallbackU3Ek__BackingField_4; }
	inline void set_U3CCallbackU3Ek__BackingField_4(Callback_1_t3507265931 * value)
	{
		___U3CCallbackU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CCallbackIDU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(EditorFacebookMockDialog_t3691766720, ___U3CCallbackIDU3Ek__BackingField_5)); }
	inline String_t* get_U3CCallbackIDU3Ek__BackingField_5() const { return ___U3CCallbackIDU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CCallbackIDU3Ek__BackingField_5() { return &___U3CCallbackIDU3Ek__BackingField_5; }
	inline void set_U3CCallbackIDU3Ek__BackingField_5(String_t* value)
	{
		___U3CCallbackIDU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackIDU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORFACEBOOKMOCKDIALOG_T3691766720_H
#ifndef EDITORFACEBOOKGAMEOBJECT_T4122771246_H
#define EDITORFACEBOOKGAMEOBJECT_T4122771246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.EditorFacebookGameObject
struct  EditorFacebookGameObject_t4122771246  : public FacebookGameObject_t3770693708
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORFACEBOOKGAMEOBJECT_T4122771246_H
#ifndef EDITORFACEBOOKLOADER_T147050272_H
#define EDITORFACEBOOKLOADER_T147050272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.EditorFacebookLoader
struct  EditorFacebookLoader_t147050272  : public CompiledFacebookLoader_t350999866
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITORFACEBOOKLOADER_T147050272_H
#ifndef GAMEROOMFACEBOOKLOADER_T1800644555_H
#define GAMEROOMFACEBOOKLOADER_T1800644555_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Gameroom.GameroomFacebookLoader
struct  GameroomFacebookLoader_t1800644555  : public CompiledFacebookLoader_t350999866
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEROOMFACEBOOKLOADER_T1800644555_H
#ifndef EMPTYMOCKDIALOG_T3184315303_H
#define EMPTYMOCKDIALOG_T3184315303_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.Dialogs.EmptyMockDialog
struct  EmptyMockDialog_t3184315303  : public EditorFacebookMockDialog_t3691766720
{
public:
	// System.String Facebook.Unity.Editor.Dialogs.EmptyMockDialog::<EmptyDialogTitle>k__BackingField
	String_t* ___U3CEmptyDialogTitleU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CEmptyDialogTitleU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(EmptyMockDialog_t3184315303, ___U3CEmptyDialogTitleU3Ek__BackingField_6)); }
	inline String_t* get_U3CEmptyDialogTitleU3Ek__BackingField_6() const { return ___U3CEmptyDialogTitleU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CEmptyDialogTitleU3Ek__BackingField_6() { return &___U3CEmptyDialogTitleU3Ek__BackingField_6; }
	inline void set_U3CEmptyDialogTitleU3Ek__BackingField_6(String_t* value)
	{
		___U3CEmptyDialogTitleU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEmptyDialogTitleU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYMOCKDIALOG_T3184315303_H
#ifndef MOCKLOGINDIALOG_T1686232399_H
#define MOCKLOGINDIALOG_T1686232399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.Dialogs.MockLoginDialog
struct  MockLoginDialog_t1686232399  : public EditorFacebookMockDialog_t3691766720
{
public:
	// System.String Facebook.Unity.Editor.Dialogs.MockLoginDialog::accessToken
	String_t* ___accessToken_6;

public:
	inline static int32_t get_offset_of_accessToken_6() { return static_cast<int32_t>(offsetof(MockLoginDialog_t1686232399, ___accessToken_6)); }
	inline String_t* get_accessToken_6() const { return ___accessToken_6; }
	inline String_t** get_address_of_accessToken_6() { return &___accessToken_6; }
	inline void set_accessToken_6(String_t* value)
	{
		___accessToken_6 = value;
		Il2CppCodeGenWriteBarrier((&___accessToken_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOCKLOGINDIALOG_T1686232399_H
#ifndef GAMEROOMFACEBOOKGAMEOBJECT_T2310521816_H
#define GAMEROOMFACEBOOKGAMEOBJECT_T2310521816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Gameroom.GameroomFacebookGameObject
struct  GameroomFacebookGameObject_t2310521816  : public FacebookGameObject_t3770693708
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEROOMFACEBOOKGAMEOBJECT_T2310521816_H
#ifndef MOBILEFACEBOOKGAMEOBJECT_T508240675_H
#define MOBILEFACEBOOKGAMEOBJECT_T508240675_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Mobile.MobileFacebookGameObject
struct  MobileFacebookGameObject_t508240675  : public FacebookGameObject_t3770693708
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEFACEBOOKGAMEOBJECT_T508240675_H
#ifndef MOCKSHAREDIALOG_T1183999029_H
#define MOCKSHAREDIALOG_T1183999029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Facebook.Unity.Editor.Dialogs.MockShareDialog
struct  MockShareDialog_t1183999029  : public EditorFacebookMockDialog_t3691766720
{
public:
	// System.String Facebook.Unity.Editor.Dialogs.MockShareDialog::<SubTitle>k__BackingField
	String_t* ___U3CSubTitleU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CSubTitleU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(MockShareDialog_t1183999029, ___U3CSubTitleU3Ek__BackingField_6)); }
	inline String_t* get_U3CSubTitleU3Ek__BackingField_6() const { return ___U3CSubTitleU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CSubTitleU3Ek__BackingField_6() { return &___U3CSubTitleU3Ek__BackingField_6; }
	inline void set_U3CSubTitleU3Ek__BackingField_6(String_t* value)
	{
		___U3CSubTitleU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSubTitleU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOCKSHAREDIALOG_T1183999029_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2100 = { sizeof (__StaticArrayInitTypeSizeU3D50_t1547932759)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D50_t1547932759 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2101 = { sizeof (__StaticArrayInitTypeSizeU3D120_t3297148301)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D120_t3297148301 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2102 = { sizeof (U3CModuleU3E_t692745557), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2103 = { sizeof (Json_t3131101706), -1, sizeof(Json_t3131101706_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2103[1] = 
{
	Json_t3131101706_StaticFields::get_offset_of_numberFormat_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2104 = { sizeof (Parser_t2196815292), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2104[1] = 
{
	Parser_t2196815292::get_offset_of_json_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2105 = { sizeof (TOKEN_t215992027)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2105[13] = 
{
	TOKEN_t215992027::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2106 = { sizeof (Serializer_t3368339501), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2106[1] = 
{
	Serializer_t3368339501::get_offset_of_builder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2107 = { sizeof (AccessToken_t2431487013), -1, sizeof(AccessToken_t2431487013_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2107[6] = 
{
	AccessToken_t2431487013_StaticFields::get_offset_of_U3CCurrentAccessTokenU3Ek__BackingField_0(),
	AccessToken_t2431487013::get_offset_of_U3CTokenStringU3Ek__BackingField_1(),
	AccessToken_t2431487013::get_offset_of_U3CExpirationTimeU3Ek__BackingField_2(),
	AccessToken_t2431487013::get_offset_of_U3CPermissionsU3Ek__BackingField_3(),
	AccessToken_t2431487013::get_offset_of_U3CUserIdU3Ek__BackingField_4(),
	AccessToken_t2431487013::get_offset_of_U3CLastRefreshU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2108 = { sizeof (CallbackManager_t2446104503), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2108[2] = 
{
	CallbackManager_t2446104503::get_offset_of_facebookDelegates_0(),
	CallbackManager_t2446104503::get_offset_of_nextAsyncId_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2109 = { sizeof (ComponentFactory_t141932586), -1, sizeof(ComponentFactory_t141932586_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2109[1] = 
{
	ComponentFactory_t141932586_StaticFields::get_offset_of_facebookGameObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2110 = { sizeof (IfNotExist_t4145194463)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2110[3] = 
{
	IfNotExist_t4145194463::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2111 = { sizeof (Constants_t1121581803), -1, sizeof(Constants_t1121581803_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2111[1] = 
{
	Constants_t1121581803_StaticFields::get_offset_of_currentPlatform_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2112 = { sizeof (FB_t2178373596), -1, sizeof(FB_t2178373596_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2112[8] = 
{
	0,
	FB_t2178373596_StaticFields::get_offset_of_facebook_3(),
	FB_t2178373596_StaticFields::get_offset_of_isInitCalled_4(),
	FB_t2178373596_StaticFields::get_offset_of_facebookDomain_5(),
	FB_t2178373596_StaticFields::get_offset_of_graphApiVersion_6(),
	FB_t2178373596_StaticFields::get_offset_of_U3CAppIdU3Ek__BackingField_7(),
	FB_t2178373596_StaticFields::get_offset_of_U3CClientTokenU3Ek__BackingField_8(),
	FB_t2178373596_StaticFields::get_offset_of_U3COnDLLLoadedDelegateU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2113 = { sizeof (OnDLLLoaded_t2963647862), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2114 = { sizeof (Canvas_t3146753929), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2115 = { sizeof (Mobile_t77204530), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2116 = { sizeof (CompiledFacebookLoader_t350999866), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2117 = { sizeof (U3CU3Ec__DisplayClass35_0_t171294370), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2117[10] = 
{
	U3CU3Ec__DisplayClass35_0_t171294370::get_offset_of_onInitComplete_0(),
	U3CU3Ec__DisplayClass35_0_t171294370::get_offset_of_appId_1(),
	U3CU3Ec__DisplayClass35_0_t171294370::get_offset_of_cookie_2(),
	U3CU3Ec__DisplayClass35_0_t171294370::get_offset_of_logging_3(),
	U3CU3Ec__DisplayClass35_0_t171294370::get_offset_of_status_4(),
	U3CU3Ec__DisplayClass35_0_t171294370::get_offset_of_xfbml_5(),
	U3CU3Ec__DisplayClass35_0_t171294370::get_offset_of_authResponse_6(),
	U3CU3Ec__DisplayClass35_0_t171294370::get_offset_of_frictionlessRequests_7(),
	U3CU3Ec__DisplayClass35_0_t171294370::get_offset_of_javascriptSDKLocale_8(),
	U3CU3Ec__DisplayClass35_0_t171294370::get_offset_of_onHideUnity_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2118 = { sizeof (FacebookBase_t1615169142), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2118[3] = 
{
	FacebookBase_t1615169142::get_offset_of_onInitCompleteDelegate_0(),
	FacebookBase_t1615169142::get_offset_of_U3CInitializedU3Ek__BackingField_1(),
	FacebookBase_t1615169142::get_offset_of_U3CCallbackManagerU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2119 = { sizeof (U3CU3Ec_t3332642991), -1, sizeof(U3CU3Ec_t3332642991_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2119[2] = 
{
	U3CU3Ec_t3332642991_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3332642991_StaticFields::get_offset_of_U3CU3E9__41_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2120 = { sizeof (InitDelegate_t3081360126), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2121 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2122 = { sizeof (HideUnityDelegate_t1353799728), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2123 = { sizeof (FacebookGameObject_t3770693708), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2123[1] = 
{
	FacebookGameObject_t3770693708::get_offset_of_U3CFacebookU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2124 = { sizeof (FacebookSdkVersion_t4164812230), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2125 = { sizeof (FacebookUnityPlatform_t2098070734)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2125[6] = 
{
	FacebookUnityPlatform_t2098070734::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2126 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2127 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2128 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2129 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2130 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2131 = { sizeof (MethodArguments_t1563002313), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2131[1] = 
{
	MethodArguments_t1563002313::get_offset_of_arguments_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2132 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable2132[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2133 = { sizeof (ShareDialogMode_t1679970535)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2133[5] = 
{
	ShareDialogMode_t1679970535::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2134 = { sizeof (OGActionType_t397354381)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2134[4] = 
{
	OGActionType_t397354381::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2135 = { sizeof (AccessTokenRefreshResult_t1866521153), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2135[1] = 
{
	AccessTokenRefreshResult_t1866521153::get_offset_of_U3CAccessTokenU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2136 = { sizeof (AppInviteResult_t1043157067), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2137 = { sizeof (AppLinkResult_t495655548), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2137[4] = 
{
	AppLinkResult_t495655548::get_offset_of_U3CUrlU3Ek__BackingField_6(),
	AppLinkResult_t495655548::get_offset_of_U3CTargetUrlU3Ek__BackingField_7(),
	AppLinkResult_t495655548::get_offset_of_U3CRefU3Ek__BackingField_8(),
	AppLinkResult_t495655548::get_offset_of_U3CExtrasU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2138 = { sizeof (AppRequestResult_t635749912), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2138[2] = 
{
	AppRequestResult_t635749912::get_offset_of_U3CRequestIDU3Ek__BackingField_6(),
	AppRequestResult_t635749912::get_offset_of_U3CToU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2139 = { sizeof (GraphResult_t1918224463), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2139[2] = 
{
	GraphResult_t1918224463::get_offset_of_U3CResultListU3Ek__BackingField_6(),
	GraphResult_t1918224463::get_offset_of_U3CTextureU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2140 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2141 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2142 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2143 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2144 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2145 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2146 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2147 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2148 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2149 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2150 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2151 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2152 = { sizeof (LoginResult_t3585129065), -1, sizeof(LoginResult_t3585129065_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2152[5] = 
{
	LoginResult_t3585129065_StaticFields::get_offset_of_UserIdKey_6(),
	LoginResult_t3585129065_StaticFields::get_offset_of_ExpirationTimestampKey_7(),
	LoginResult_t3585129065_StaticFields::get_offset_of_PermissionsKey_8(),
	LoginResult_t3585129065_StaticFields::get_offset_of_AccessTokenKey_9(),
	LoginResult_t3585129065::get_offset_of_U3CAccessTokenU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2153 = { sizeof (PayResult_t2637031204), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2154 = { sizeof (ResultBase_t777682874), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2154[6] = 
{
	ResultBase_t777682874::get_offset_of_U3CErrorU3Ek__BackingField_0(),
	ResultBase_t777682874::get_offset_of_U3CResultDictionaryU3Ek__BackingField_1(),
	ResultBase_t777682874::get_offset_of_U3CRawResultU3Ek__BackingField_2(),
	ResultBase_t777682874::get_offset_of_U3CCancelledU3Ek__BackingField_3(),
	ResultBase_t777682874::get_offset_of_U3CCallbackIdU3Ek__BackingField_4(),
	ResultBase_t777682874::get_offset_of_U3CCanvasErrorCodeU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2155 = { sizeof (ResultContainer_t4150301447), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2155[3] = 
{
	0,
	ResultContainer_t4150301447::get_offset_of_U3CRawResultU3Ek__BackingField_1(),
	ResultContainer_t4150301447::get_offset_of_U3CResultDictionaryU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2156 = { sizeof (ShareResult_t3324833527), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2156[1] = 
{
	ShareResult_t3324833527::get_offset_of_U3CPostIdU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2157 = { sizeof (AsyncRequestString_t3669574124), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2157[5] = 
{
	AsyncRequestString_t3669574124::get_offset_of_url_2(),
	AsyncRequestString_t3669574124::get_offset_of_method_3(),
	AsyncRequestString_t3669574124::get_offset_of_formData_4(),
	AsyncRequestString_t3669574124::get_offset_of_query_5(),
	AsyncRequestString_t3669574124::get_offset_of_callback_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2158 = { sizeof (U3CStartU3Ed__9_t2855687532), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2158[4] = 
{
	U3CStartU3Ed__9_t2855687532::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__9_t2855687532::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__9_t2855687532::get_offset_of_U3CU3E4__this_2(),
	U3CStartU3Ed__9_t2855687532::get_offset_of_U3CwwwU3E5__1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2159 = { sizeof (FacebookLogger_t1173398901), -1, sizeof(FacebookLogger_t1173398901_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2159[1] = 
{
	FacebookLogger_t1173398901_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2160 = { sizeof (DebugLogger_t2300716689), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2161 = { sizeof (HttpMethod_t313594167)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2161[4] = 
{
	HttpMethod_t313594167::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2162 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2163 = { sizeof (Utilities_t2011615223), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2164 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2165 = { sizeof (U3CU3Ec_t3449414030), -1, sizeof(U3CU3Ec_t3449414030_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2165[2] = 
{
	U3CU3Ec_t3449414030_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3449414030_StaticFields::get_offset_of_U3CU3E9__18_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2166 = { sizeof (FBUnityUtility_t2214217213), -1, sizeof(FBUnityUtility_t2214217213_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2166[1] = 
{
	FBUnityUtility_t2214217213_StaticFields::get_offset_of_asyncRequestStringWrapper_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2167 = { sizeof (AsyncRequestStringWrapper_t707311955), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2168 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2169 = { sizeof (FacebookScheduler_t1007614953), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2170 = { sizeof (U3CDelayEventU3Ed__1_t2601426355), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2170[4] = 
{
	U3CDelayEventU3Ed__1_t2601426355::get_offset_of_U3CU3E1__state_0(),
	U3CDelayEventU3Ed__1_t2601426355::get_offset_of_U3CU3E2__current_1(),
	U3CDelayEventU3Ed__1_t2601426355::get_offset_of_delay_2(),
	U3CDelayEventU3Ed__1_t2601426355::get_offset_of_action_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2171 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2172 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2173 = { sizeof (GameroomFacebook_t453300599), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2173[3] = 
{
	GameroomFacebook_t453300599::get_offset_of_appId_3(),
	GameroomFacebook_t453300599::get_offset_of_gameroomWrapper_4(),
	GameroomFacebook_t453300599::get_offset_of_U3CLimitEventUsageU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2174 = { sizeof (OnComplete_t3786498944), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2175 = { sizeof (GameroomFacebookGameObject_t2310521816), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2176 = { sizeof (U3CWaitForPipeResponseU3Ed__4_t3567704217), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2176[5] = 
{
	U3CWaitForPipeResponseU3Ed__4_t3567704217::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForPipeResponseU3Ed__4_t3567704217::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForPipeResponseU3Ed__4_t3567704217::get_offset_of_U3CU3E4__this_2(),
	U3CWaitForPipeResponseU3Ed__4_t3567704217::get_offset_of_onCompleteDelegate_3(),
	U3CWaitForPipeResponseU3Ed__4_t3567704217::get_offset_of_callbackId_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2177 = { sizeof (GameroomFacebookLoader_t1800644555), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2178 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2179 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2180 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2181 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2182 = { sizeof (EditorFacebook_t2350935743), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2182[3] = 
{
	EditorFacebook_t2350935743::get_offset_of_editorWrapper_3(),
	EditorFacebook_t2350935743::get_offset_of_U3CLimitEventUsageU3Ek__BackingField_4(),
	EditorFacebook_t2350935743::get_offset_of_U3CShareDialogModeU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2183 = { sizeof (EditorFacebookGameObject_t4122771246), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2184 = { sizeof (EditorFacebookLoader_t147050272), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2185 = { sizeof (EditorFacebookMockDialog_t3691766720), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2185[4] = 
{
	EditorFacebookMockDialog_t3691766720::get_offset_of_modalRect_2(),
	EditorFacebookMockDialog_t3691766720::get_offset_of_modalStyle_3(),
	EditorFacebookMockDialog_t3691766720::get_offset_of_U3CCallbackU3Ek__BackingField_4(),
	EditorFacebookMockDialog_t3691766720::get_offset_of_U3CCallbackIDU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2186 = { sizeof (EditorWrapper_t3420512207), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2186[1] = 
{
	EditorWrapper_t3420512207::get_offset_of_callbackHandler_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2187 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2188 = { sizeof (EmptyMockDialog_t3184315303), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2188[1] = 
{
	EmptyMockDialog_t3184315303::get_offset_of_U3CEmptyDialogTitleU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2189 = { sizeof (MockLoginDialog_t1686232399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2189[1] = 
{
	MockLoginDialog_t1686232399::get_offset_of_accessToken_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2190 = { sizeof (U3CU3Ec__DisplayClass4_0_t95552448), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2190[2] = 
{
	U3CU3Ec__DisplayClass4_0_t95552448::get_offset_of_facebookID_0(),
	U3CU3Ec__DisplayClass4_0_t95552448::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2191 = { sizeof (MockShareDialog_t1183999029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2191[1] = 
{
	MockShareDialog_t1183999029::get_offset_of_U3CSubTitleU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2192 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2193 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2194 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2195 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2196 = { sizeof (MobileFacebook_t4202726836), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2196[1] = 
{
	MobileFacebook_t4202726836::get_offset_of_shareDialogMode_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2197 = { sizeof (MobileFacebookGameObject_t508240675), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2198 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2199 = { sizeof (IOSFacebook_t793364667), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2199[2] = 
{
	IOSFacebook_t793364667::get_offset_of_limitEventUsage_4(),
	IOSFacebook_t793364667::get_offset_of_iosWrapper_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
