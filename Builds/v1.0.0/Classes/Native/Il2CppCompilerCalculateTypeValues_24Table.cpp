﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.String
struct String_t;
// System.Int32[0...,0...]
struct Int32U5B0___U2C0___U5D_t385246373;
// System.Collections.Generic.List`1<UnityEngine.UI.Image>
struct List_1_t4142344393;
// Outback.Gameplay.SlotGameBoxing
struct SlotGameBoxing_t1523269123;
// System.Converter`2<UnityEngine.UI.Image,System.String>
struct Converter_2_t804895609;
// System.Predicate`1<UnityEngine.UI.Image>
struct Predicate_1_t3495563775;
// UnityEngine.WWWForm
struct WWWForm_t4064702195;
// UnityEngine.WWW
struct WWW_t3688466362;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t2865362463;
// PromoCodePanelController
struct PromoCodePanelController_t729267270;
// Outback.Gameplay.SlotGameCircus
struct SlotGameCircus_t1905421599;
// LobbyController/LoadPictureCallback
struct LoadPictureCallback_t1320020716;
// Outback.Gameplay.SlotGameEgypt
struct SlotGameEgypt_t1252059770;
// RadialLoading
struct RadialLoading_t2588181753;
// UnityEngine.GameObject
struct GameObject_t1113636619;
// FacebookFriend
struct FacebookFriend_t1860979812;
// FBHolder/LoadPictureCallback
struct LoadPictureCallback_t2056340374;
// FBHolder
struct FBHolder_t1712543493;
// Outback.Core.Reward
struct Reward_t2838753371;
// Outback.Gameplay.SlotGameSafari
struct SlotGameSafari_t3944626951;
// Outback.Gameplay.SlotGameNewYork
struct SlotGameNewYork_t345457032;
// Outback.Managers.PlayerManager
struct PlayerManager_t1841934776;
// Outback.Gameplay.SlotGameOuterSpace
struct SlotGameOuterSpace_t3776195372;
// Outback.Gameplay.SlotGameRetro
struct SlotGameRetro_t96010562;
// UnityEngine.Sprite
struct Sprite_t280657092;
// Outback.Gameplay.SlotGameFarm
struct SlotGameFarm_t876359397;
// Outback.Managers.SlotGameManager
struct SlotGameManager_t584614006;
// Outback.Gameplay.SlotGameIreland
struct SlotGameIreland_t2086646524;
// UnityEngine.AsyncOperation
struct AsyncOperation_t1445031843;
// Outback.Managers.ScreenManager
struct ScreenManager_t3517964220;
// Outback.Gameplay.SlotGameMarineWorld
struct SlotGameMarineWorld_t1002224217;
// Outback.Gameplay.SlotGameMovieNight
struct SlotGameMovieNight_t2457517516;
// UnityEngine.Networking.UnityWebRequest
struct UnityWebRequest_t463507806;
// UnityEngine.AssetBundle
struct AssetBundle_t1153907252;
// System.String[]
struct StringU5BU5D_t1281789340;
// AssetBundleHandler
struct AssetBundleHandler_t3701416431;
// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule>
struct List_1_t3418373063;
// launchImage
struct launchImage_t1920275599;
// DG.Tweening.Core.DOSetter`1<System.Single>
struct DOSetter_1_t2447375106;
// DG.Tweening.TweenCallback
struct TweenCallback_t3727756325;
// UnityEngine.Animator
struct Animator_t434523843;
// BaseGame
struct BaseGame_t2336638441;
// SlotReel
struct SlotReel_t1924038889;
// DailyReward
struct DailyReward_t2847634760;
// SpinWheelController
struct SpinWheelController_t1228200680;
// CoinsShower/<StartAnim>c__AnonStorey1
struct U3CStartAnimU3Ec__AnonStorey1_t4682627;
// UnityEngine.UI.Image
struct Image_t2670269651;
// BaseGame/<StartFreeSpinSequence>c__Iterator0
struct U3CStartFreeSpinSequenceU3Ec__Iterator0_t2638780399;
// System.Void
struct Void_t1185182177;
// System.Collections.Generic.List`1<Outback.Core.AnimatableComponent>
struct List_1_t1804807908;
// Outback.Core.AnimatableComponent
struct AnimatableComponent_t332733166;
// System.Char[]
struct CharU5BU5D_t3528271667;
// System.Collections.Generic.List`1<SlotReel>
struct List_1_t3396113631;
// DG.Tweening.Sequence
struct Sequence_t2050373119;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.DelegateData
struct DelegateData_t1677132599;
// UnityEngine.Analytics.TriggerListContainer
struct TriggerListContainer_t2032715483;
// UnityEngine.Analytics.EventTrigger/OnTrigger
struct OnTrigger_t4184125570;
// UnityEngine.Analytics.TriggerMethod
struct TriggerMethod_t582536534;
// UnityEngine.Analytics.TrackableField
struct TrackableField_t1772682203;
// UnityEngine.Analytics.ValueProperty
struct ValueProperty_t1868393739;
// UnityEngine.Texture2D
struct Texture2D_t3840446185;
// System.IAsyncResult
struct IAsyncResult_t767004451;
// System.AsyncCallback
struct AsyncCallback_t3962456242;
// UnityEngine.UI.Button
struct Button_t4055032469;
// System.Collections.Generic.List`1<UnityEngine.Sprite>
struct List_1_t1752731834;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t2585711361;
// System.Predicate`1<UnityEngine.Sprite>
struct Predicate_1_t1105951216;
// UnityEngine.UI.Text
struct Text_t1901882714;
// LitJson.JsonData
struct JsonData_t1524858407;
// System.Collections.Generic.List`1<FacebookFriend>
struct List_1_t3333054554;
// UnityEngine.AudioSource
struct AudioSource_t3935305588;
// System.Collections.Generic.List`1<UnityEngine.AudioSource>
struct List_1_t1112413034;
// TMPro.TextMeshProUGUI
struct TextMeshProUGUI_t529313277;
// System.Collections.Generic.List`1<System.Int32>
struct List_1_t128053199;
// System.Collections.Generic.List`1<UnityEngine.AnimatorControllerParameter>
struct List_1_t3230334784;
// System.Collections.Generic.List`1<System.String>
struct List_1_t3319525431;
// System.Collections.Generic.List`1<UnityEngine.AssetBundle>
struct List_1_t2625981994;
// UnityEngine.UI.Image[0...,0...]
struct ImageU5B0___U2C0___U5D_t2439009923;
// UnityEngine.ParticleSystem
struct ParticleSystem_t1800779281;
// System.Collections.Generic.List`1<Outback.Core.Reward>
struct List_1_t15860817;
// System.Converter`2<UnityEngine.GameObject,UnityEngine.AudioSource>
struct Converter_2_t3782339732;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult>
struct FacebookDelegate_1_t1976622014;
// UnityEngine.UI.Slider
struct Slider_t3903728902;
// UnityEngine.Purchasing.IStoreController
struct IStoreController_t2579314702;
// UnityEngine.Purchasing.IExtensionProvider
struct IExtensionProvider_t3180538779;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t1632706988;
// System.Collections.Generic.List`1<Outback.Managers.CrossPromoCreative>
struct List_1_t2205672511;
// UnityEngine.UI.InputField
struct InputField_t3762917431;
// Sdkbox.IAP
struct IAP_t1939458538;
// System.Collections.Generic.List`1<UnityEngine.UI.Button>
struct List_1_t1232139915;
// System.Collections.Generic.List`1<System.Int64>
struct List_1_t913674750;
// DG.Tweening.Tweener
struct Tweener_t436044680;
// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult>
struct FacebookDelegate_1_t602110559;
// UnityEngine.Material
struct Material_t340375123;
// UnityEngine.Shader
struct Shader_t4151988712;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CMODULEU3E_T692745565_H
#define U3CMODULEU3E_T692745565_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t692745565 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T692745565_H
#ifndef U3CSTARTOFFU3EC__ITERATOR0_T309367319_H
#define U3CSTARTOFFU3EC__ITERATOR0_T309367319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Gameplay.SlotGameBoxing/<StartOff>c__Iterator0
struct  U3CStartOffU3Ec__Iterator0_t309367319  : public RuntimeObject
{
public:
	// System.String Outback.Gameplay.SlotGameBoxing/<StartOff>c__Iterator0::<arr1>__0
	String_t* ___U3Carr1U3E__0_0;
	// System.Int32[0...,0...] Outback.Gameplay.SlotGameBoxing/<StartOff>c__Iterator0::<linesRules>__0
	Int32U5B0___U2C0___U5D_t385246373* ___U3ClinesRulesU3E__0_1;
	// System.Int64 Outback.Gameplay.SlotGameBoxing/<StartOff>c__Iterator0::<totalAmountWon>__0
	int64_t ___U3CtotalAmountWonU3E__0_2;
	// System.Int32 Outback.Gameplay.SlotGameBoxing/<StartOff>c__Iterator0::<numOfLines>__0
	int32_t ___U3CnumOfLinesU3E__0_3;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> Outback.Gameplay.SlotGameBoxing/<StartOff>c__Iterator0::<allMatchedImages>__0
	List_1_t4142344393 * ___U3CallMatchedImagesU3E__0_4;
	// Outback.Gameplay.SlotGameBoxing Outback.Gameplay.SlotGameBoxing/<StartOff>c__Iterator0::$this
	SlotGameBoxing_t1523269123 * ___U24this_5;
	// System.Object Outback.Gameplay.SlotGameBoxing/<StartOff>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean Outback.Gameplay.SlotGameBoxing/<StartOff>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 Outback.Gameplay.SlotGameBoxing/<StartOff>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3Carr1U3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t309367319, ___U3Carr1U3E__0_0)); }
	inline String_t* get_U3Carr1U3E__0_0() const { return ___U3Carr1U3E__0_0; }
	inline String_t** get_address_of_U3Carr1U3E__0_0() { return &___U3Carr1U3E__0_0; }
	inline void set_U3Carr1U3E__0_0(String_t* value)
	{
		___U3Carr1U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Carr1U3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3ClinesRulesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t309367319, ___U3ClinesRulesU3E__0_1)); }
	inline Int32U5B0___U2C0___U5D_t385246373* get_U3ClinesRulesU3E__0_1() const { return ___U3ClinesRulesU3E__0_1; }
	inline Int32U5B0___U2C0___U5D_t385246373** get_address_of_U3ClinesRulesU3E__0_1() { return &___U3ClinesRulesU3E__0_1; }
	inline void set_U3ClinesRulesU3E__0_1(Int32U5B0___U2C0___U5D_t385246373* value)
	{
		___U3ClinesRulesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClinesRulesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtotalAmountWonU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t309367319, ___U3CtotalAmountWonU3E__0_2)); }
	inline int64_t get_U3CtotalAmountWonU3E__0_2() const { return ___U3CtotalAmountWonU3E__0_2; }
	inline int64_t* get_address_of_U3CtotalAmountWonU3E__0_2() { return &___U3CtotalAmountWonU3E__0_2; }
	inline void set_U3CtotalAmountWonU3E__0_2(int64_t value)
	{
		___U3CtotalAmountWonU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CnumOfLinesU3E__0_3() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t309367319, ___U3CnumOfLinesU3E__0_3)); }
	inline int32_t get_U3CnumOfLinesU3E__0_3() const { return ___U3CnumOfLinesU3E__0_3; }
	inline int32_t* get_address_of_U3CnumOfLinesU3E__0_3() { return &___U3CnumOfLinesU3E__0_3; }
	inline void set_U3CnumOfLinesU3E__0_3(int32_t value)
	{
		___U3CnumOfLinesU3E__0_3 = value;
	}

	inline static int32_t get_offset_of_U3CallMatchedImagesU3E__0_4() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t309367319, ___U3CallMatchedImagesU3E__0_4)); }
	inline List_1_t4142344393 * get_U3CallMatchedImagesU3E__0_4() const { return ___U3CallMatchedImagesU3E__0_4; }
	inline List_1_t4142344393 ** get_address_of_U3CallMatchedImagesU3E__0_4() { return &___U3CallMatchedImagesU3E__0_4; }
	inline void set_U3CallMatchedImagesU3E__0_4(List_1_t4142344393 * value)
	{
		___U3CallMatchedImagesU3E__0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CallMatchedImagesU3E__0_4), value);
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t309367319, ___U24this_5)); }
	inline SlotGameBoxing_t1523269123 * get_U24this_5() const { return ___U24this_5; }
	inline SlotGameBoxing_t1523269123 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(SlotGameBoxing_t1523269123 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t309367319, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t309367319, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t309367319, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

struct U3CStartOffU3Ec__Iterator0_t309367319_StaticFields
{
public:
	// System.Converter`2<UnityEngine.UI.Image,System.String> Outback.Gameplay.SlotGameBoxing/<StartOff>c__Iterator0::<>f__am$cache0
	Converter_2_t804895609 * ___U3CU3Ef__amU24cache0_9;
	// System.Predicate`1<UnityEngine.UI.Image> Outback.Gameplay.SlotGameBoxing/<StartOff>c__Iterator0::<>f__am$cache1
	Predicate_1_t3495563775 * ___U3CU3Ef__amU24cache1_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t309367319_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Converter_2_t804895609 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Converter_2_t804895609 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Converter_2_t804895609 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_10() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t309367319_StaticFields, ___U3CU3Ef__amU24cache1_10)); }
	inline Predicate_1_t3495563775 * get_U3CU3Ef__amU24cache1_10() const { return ___U3CU3Ef__amU24cache1_10; }
	inline Predicate_1_t3495563775 ** get_address_of_U3CU3Ef__amU24cache1_10() { return &___U3CU3Ef__amU24cache1_10; }
	inline void set_U3CU3Ef__amU24cache1_10(Predicate_1_t3495563775 * value)
	{
		___U3CU3Ef__amU24cache1_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTOFFU3EC__ITERATOR0_T309367319_H
#ifndef U3CCALLPROMOCODESERVICEU3EC__ITERATOR0_T2382672223_H
#define U3CCALLPROMOCODESERVICEU3EC__ITERATOR0_T2382672223_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PromoCodePanelController/<CallPromoCodeService>c__Iterator0
struct  U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223  : public RuntimeObject
{
public:
	// System.String PromoCodePanelController/<CallPromoCodeService>c__Iterator0::<highscore_url>__0
	String_t* ___U3Chighscore_urlU3E__0_0;
	// System.String PromoCodePanelController/<CallPromoCodeService>c__Iterator0::<promocode>__0
	String_t* ___U3CpromocodeU3E__0_1;
	// UnityEngine.WWWForm PromoCodePanelController/<CallPromoCodeService>c__Iterator0::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_2;
	// UnityEngine.WWW PromoCodePanelController/<CallPromoCodeService>c__Iterator0::<download>__0
	WWW_t3688466362 * ___U3CdownloadU3E__0_3;
	// System.String PromoCodePanelController/<CallPromoCodeService>c__Iterator0::<json>__1
	String_t* ___U3CjsonU3E__1_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> PromoCodePanelController/<CallPromoCodeService>c__Iterator0::<data>__1
	Dictionary_2_t2865362463 * ___U3CdataU3E__1_5;
	// System.String PromoCodePanelController/<CallPromoCodeService>c__Iterator0::<responsecode>__1
	String_t* ___U3CresponsecodeU3E__1_6;
	// System.String PromoCodePanelController/<CallPromoCodeService>c__Iterator0::<responsetext>__1
	String_t* ___U3CresponsetextU3E__1_7;
	// System.String PromoCodePanelController/<CallPromoCodeService>c__Iterator0::<str_message>__2
	String_t* ___U3Cstr_messageU3E__2_8;
	// System.Single PromoCodePanelController/<CallPromoCodeService>c__Iterator0::<delay>__2
	float ___U3CdelayU3E__2_9;
	// PromoCodePanelController PromoCodePanelController/<CallPromoCodeService>c__Iterator0::$this
	PromoCodePanelController_t729267270 * ___U24this_10;
	// System.Object PromoCodePanelController/<CallPromoCodeService>c__Iterator0::$current
	RuntimeObject * ___U24current_11;
	// System.Boolean PromoCodePanelController/<CallPromoCodeService>c__Iterator0::$disposing
	bool ___U24disposing_12;
	// System.Int32 PromoCodePanelController/<CallPromoCodeService>c__Iterator0::$PC
	int32_t ___U24PC_13;

public:
	inline static int32_t get_offset_of_U3Chighscore_urlU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223, ___U3Chighscore_urlU3E__0_0)); }
	inline String_t* get_U3Chighscore_urlU3E__0_0() const { return ___U3Chighscore_urlU3E__0_0; }
	inline String_t** get_address_of_U3Chighscore_urlU3E__0_0() { return &___U3Chighscore_urlU3E__0_0; }
	inline void set_U3Chighscore_urlU3E__0_0(String_t* value)
	{
		___U3Chighscore_urlU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Chighscore_urlU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CpromocodeU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223, ___U3CpromocodeU3E__0_1)); }
	inline String_t* get_U3CpromocodeU3E__0_1() const { return ___U3CpromocodeU3E__0_1; }
	inline String_t** get_address_of_U3CpromocodeU3E__0_1() { return &___U3CpromocodeU3E__0_1; }
	inline void set_U3CpromocodeU3E__0_1(String_t* value)
	{
		___U3CpromocodeU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpromocodeU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CformU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223, ___U3CformU3E__0_2)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_2() const { return ___U3CformU3E__0_2; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_2() { return &___U3CformU3E__0_2; }
	inline void set_U3CformU3E__0_2(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U3CdownloadU3E__0_3() { return static_cast<int32_t>(offsetof(U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223, ___U3CdownloadU3E__0_3)); }
	inline WWW_t3688466362 * get_U3CdownloadU3E__0_3() const { return ___U3CdownloadU3E__0_3; }
	inline WWW_t3688466362 ** get_address_of_U3CdownloadU3E__0_3() { return &___U3CdownloadU3E__0_3; }
	inline void set_U3CdownloadU3E__0_3(WWW_t3688466362 * value)
	{
		___U3CdownloadU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdownloadU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CjsonU3E__1_4() { return static_cast<int32_t>(offsetof(U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223, ___U3CjsonU3E__1_4)); }
	inline String_t* get_U3CjsonU3E__1_4() const { return ___U3CjsonU3E__1_4; }
	inline String_t** get_address_of_U3CjsonU3E__1_4() { return &___U3CjsonU3E__1_4; }
	inline void set_U3CjsonU3E__1_4(String_t* value)
	{
		___U3CjsonU3E__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CjsonU3E__1_4), value);
	}

	inline static int32_t get_offset_of_U3CdataU3E__1_5() { return static_cast<int32_t>(offsetof(U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223, ___U3CdataU3E__1_5)); }
	inline Dictionary_2_t2865362463 * get_U3CdataU3E__1_5() const { return ___U3CdataU3E__1_5; }
	inline Dictionary_2_t2865362463 ** get_address_of_U3CdataU3E__1_5() { return &___U3CdataU3E__1_5; }
	inline void set_U3CdataU3E__1_5(Dictionary_2_t2865362463 * value)
	{
		___U3CdataU3E__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdataU3E__1_5), value);
	}

	inline static int32_t get_offset_of_U3CresponsecodeU3E__1_6() { return static_cast<int32_t>(offsetof(U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223, ___U3CresponsecodeU3E__1_6)); }
	inline String_t* get_U3CresponsecodeU3E__1_6() const { return ___U3CresponsecodeU3E__1_6; }
	inline String_t** get_address_of_U3CresponsecodeU3E__1_6() { return &___U3CresponsecodeU3E__1_6; }
	inline void set_U3CresponsecodeU3E__1_6(String_t* value)
	{
		___U3CresponsecodeU3E__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresponsecodeU3E__1_6), value);
	}

	inline static int32_t get_offset_of_U3CresponsetextU3E__1_7() { return static_cast<int32_t>(offsetof(U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223, ___U3CresponsetextU3E__1_7)); }
	inline String_t* get_U3CresponsetextU3E__1_7() const { return ___U3CresponsetextU3E__1_7; }
	inline String_t** get_address_of_U3CresponsetextU3E__1_7() { return &___U3CresponsetextU3E__1_7; }
	inline void set_U3CresponsetextU3E__1_7(String_t* value)
	{
		___U3CresponsetextU3E__1_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresponsetextU3E__1_7), value);
	}

	inline static int32_t get_offset_of_U3Cstr_messageU3E__2_8() { return static_cast<int32_t>(offsetof(U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223, ___U3Cstr_messageU3E__2_8)); }
	inline String_t* get_U3Cstr_messageU3E__2_8() const { return ___U3Cstr_messageU3E__2_8; }
	inline String_t** get_address_of_U3Cstr_messageU3E__2_8() { return &___U3Cstr_messageU3E__2_8; }
	inline void set_U3Cstr_messageU3E__2_8(String_t* value)
	{
		___U3Cstr_messageU3E__2_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3Cstr_messageU3E__2_8), value);
	}

	inline static int32_t get_offset_of_U3CdelayU3E__2_9() { return static_cast<int32_t>(offsetof(U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223, ___U3CdelayU3E__2_9)); }
	inline float get_U3CdelayU3E__2_9() const { return ___U3CdelayU3E__2_9; }
	inline float* get_address_of_U3CdelayU3E__2_9() { return &___U3CdelayU3E__2_9; }
	inline void set_U3CdelayU3E__2_9(float value)
	{
		___U3CdelayU3E__2_9 = value;
	}

	inline static int32_t get_offset_of_U24this_10() { return static_cast<int32_t>(offsetof(U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223, ___U24this_10)); }
	inline PromoCodePanelController_t729267270 * get_U24this_10() const { return ___U24this_10; }
	inline PromoCodePanelController_t729267270 ** get_address_of_U24this_10() { return &___U24this_10; }
	inline void set_U24this_10(PromoCodePanelController_t729267270 * value)
	{
		___U24this_10 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_10), value);
	}

	inline static int32_t get_offset_of_U24current_11() { return static_cast<int32_t>(offsetof(U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223, ___U24current_11)); }
	inline RuntimeObject * get_U24current_11() const { return ___U24current_11; }
	inline RuntimeObject ** get_address_of_U24current_11() { return &___U24current_11; }
	inline void set_U24current_11(RuntimeObject * value)
	{
		___U24current_11 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_11), value);
	}

	inline static int32_t get_offset_of_U24disposing_12() { return static_cast<int32_t>(offsetof(U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223, ___U24disposing_12)); }
	inline bool get_U24disposing_12() const { return ___U24disposing_12; }
	inline bool* get_address_of_U24disposing_12() { return &___U24disposing_12; }
	inline void set_U24disposing_12(bool value)
	{
		___U24disposing_12 = value;
	}

	inline static int32_t get_offset_of_U24PC_13() { return static_cast<int32_t>(offsetof(U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223, ___U24PC_13)); }
	inline int32_t get_U24PC_13() const { return ___U24PC_13; }
	inline int32_t* get_address_of_U24PC_13() { return &___U24PC_13; }
	inline void set_U24PC_13(int32_t value)
	{
		___U24PC_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCALLPROMOCODESERVICEU3EC__ITERATOR0_T2382672223_H
#ifndef U3CSTARTOFFU3EC__ITERATOR0_T1690707056_H
#define U3CSTARTOFFU3EC__ITERATOR0_T1690707056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Gameplay.SlotGameCircus/<StartOff>c__Iterator0
struct  U3CStartOffU3Ec__Iterator0_t1690707056  : public RuntimeObject
{
public:
	// System.String Outback.Gameplay.SlotGameCircus/<StartOff>c__Iterator0::<arr1>__0
	String_t* ___U3Carr1U3E__0_0;
	// System.Int32[0...,0...] Outback.Gameplay.SlotGameCircus/<StartOff>c__Iterator0::<linesRules>__0
	Int32U5B0___U2C0___U5D_t385246373* ___U3ClinesRulesU3E__0_1;
	// System.Int64 Outback.Gameplay.SlotGameCircus/<StartOff>c__Iterator0::<totalAmountWon>__0
	int64_t ___U3CtotalAmountWonU3E__0_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> Outback.Gameplay.SlotGameCircus/<StartOff>c__Iterator0::<allMatchedImages>__0
	List_1_t4142344393 * ___U3CallMatchedImagesU3E__0_3;
	// System.Int32 Outback.Gameplay.SlotGameCircus/<StartOff>c__Iterator0::<numOfLines>__0
	int32_t ___U3CnumOfLinesU3E__0_4;
	// Outback.Gameplay.SlotGameCircus Outback.Gameplay.SlotGameCircus/<StartOff>c__Iterator0::$this
	SlotGameCircus_t1905421599 * ___U24this_5;
	// System.Object Outback.Gameplay.SlotGameCircus/<StartOff>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean Outback.Gameplay.SlotGameCircus/<StartOff>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 Outback.Gameplay.SlotGameCircus/<StartOff>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3Carr1U3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1690707056, ___U3Carr1U3E__0_0)); }
	inline String_t* get_U3Carr1U3E__0_0() const { return ___U3Carr1U3E__0_0; }
	inline String_t** get_address_of_U3Carr1U3E__0_0() { return &___U3Carr1U3E__0_0; }
	inline void set_U3Carr1U3E__0_0(String_t* value)
	{
		___U3Carr1U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Carr1U3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3ClinesRulesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1690707056, ___U3ClinesRulesU3E__0_1)); }
	inline Int32U5B0___U2C0___U5D_t385246373* get_U3ClinesRulesU3E__0_1() const { return ___U3ClinesRulesU3E__0_1; }
	inline Int32U5B0___U2C0___U5D_t385246373** get_address_of_U3ClinesRulesU3E__0_1() { return &___U3ClinesRulesU3E__0_1; }
	inline void set_U3ClinesRulesU3E__0_1(Int32U5B0___U2C0___U5D_t385246373* value)
	{
		___U3ClinesRulesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClinesRulesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtotalAmountWonU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1690707056, ___U3CtotalAmountWonU3E__0_2)); }
	inline int64_t get_U3CtotalAmountWonU3E__0_2() const { return ___U3CtotalAmountWonU3E__0_2; }
	inline int64_t* get_address_of_U3CtotalAmountWonU3E__0_2() { return &___U3CtotalAmountWonU3E__0_2; }
	inline void set_U3CtotalAmountWonU3E__0_2(int64_t value)
	{
		___U3CtotalAmountWonU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CallMatchedImagesU3E__0_3() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1690707056, ___U3CallMatchedImagesU3E__0_3)); }
	inline List_1_t4142344393 * get_U3CallMatchedImagesU3E__0_3() const { return ___U3CallMatchedImagesU3E__0_3; }
	inline List_1_t4142344393 ** get_address_of_U3CallMatchedImagesU3E__0_3() { return &___U3CallMatchedImagesU3E__0_3; }
	inline void set_U3CallMatchedImagesU3E__0_3(List_1_t4142344393 * value)
	{
		___U3CallMatchedImagesU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CallMatchedImagesU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CnumOfLinesU3E__0_4() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1690707056, ___U3CnumOfLinesU3E__0_4)); }
	inline int32_t get_U3CnumOfLinesU3E__0_4() const { return ___U3CnumOfLinesU3E__0_4; }
	inline int32_t* get_address_of_U3CnumOfLinesU3E__0_4() { return &___U3CnumOfLinesU3E__0_4; }
	inline void set_U3CnumOfLinesU3E__0_4(int32_t value)
	{
		___U3CnumOfLinesU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1690707056, ___U24this_5)); }
	inline SlotGameCircus_t1905421599 * get_U24this_5() const { return ___U24this_5; }
	inline SlotGameCircus_t1905421599 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(SlotGameCircus_t1905421599 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1690707056, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1690707056, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1690707056, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

struct U3CStartOffU3Ec__Iterator0_t1690707056_StaticFields
{
public:
	// System.Converter`2<UnityEngine.UI.Image,System.String> Outback.Gameplay.SlotGameCircus/<StartOff>c__Iterator0::<>f__am$cache0
	Converter_2_t804895609 * ___U3CU3Ef__amU24cache0_9;
	// System.Predicate`1<UnityEngine.UI.Image> Outback.Gameplay.SlotGameCircus/<StartOff>c__Iterator0::<>f__am$cache1
	Predicate_1_t3495563775 * ___U3CU3Ef__amU24cache1_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1690707056_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Converter_2_t804895609 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Converter_2_t804895609 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Converter_2_t804895609 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_10() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1690707056_StaticFields, ___U3CU3Ef__amU24cache1_10)); }
	inline Predicate_1_t3495563775 * get_U3CU3Ef__amU24cache1_10() const { return ___U3CU3Ef__amU24cache1_10; }
	inline Predicate_1_t3495563775 ** get_address_of_U3CU3Ef__amU24cache1_10() { return &___U3CU3Ef__amU24cache1_10; }
	inline void set_U3CU3Ef__amU24cache1_10(Predicate_1_t3495563775 * value)
	{
		___U3CU3Ef__amU24cache1_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTOFFU3EC__ITERATOR0_T1690707056_H
#ifndef U3CLOADPICTUREENUMERATORU3EC__ITERATOR0_T2007900168_H
#define U3CLOADPICTUREENUMERATORU3EC__ITERATOR0_T2007900168_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LobbyController/<LoadPictureEnumerator>c__Iterator0
struct  U3CLoadPictureEnumeratorU3Ec__Iterator0_t2007900168  : public RuntimeObject
{
public:
	// System.String LobbyController/<LoadPictureEnumerator>c__Iterator0::url
	String_t* ___url_0;
	// UnityEngine.WWW LobbyController/<LoadPictureEnumerator>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_1;
	// LobbyController/LoadPictureCallback LobbyController/<LoadPictureEnumerator>c__Iterator0::callback
	LoadPictureCallback_t1320020716 * ___callback_2;
	// System.Object LobbyController/<LoadPictureEnumerator>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean LobbyController/<LoadPictureEnumerator>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 LobbyController/<LoadPictureEnumerator>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CLoadPictureEnumeratorU3Ec__Iterator0_t2007900168, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadPictureEnumeratorU3Ec__Iterator0_t2007900168, ___U3CwwwU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CLoadPictureEnumeratorU3Ec__Iterator0_t2007900168, ___callback_2)); }
	inline LoadPictureCallback_t1320020716 * get_callback_2() const { return ___callback_2; }
	inline LoadPictureCallback_t1320020716 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(LoadPictureCallback_t1320020716 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLoadPictureEnumeratorU3Ec__Iterator0_t2007900168, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CLoadPictureEnumeratorU3Ec__Iterator0_t2007900168, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CLoadPictureEnumeratorU3Ec__Iterator0_t2007900168, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADPICTUREENUMERATORU3EC__ITERATOR0_T2007900168_H
#ifndef U3CSTARTOFFU3EC__ITERATOR0_T1448251964_H
#define U3CSTARTOFFU3EC__ITERATOR0_T1448251964_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Gameplay.SlotGameEgypt/<StartOff>c__Iterator0
struct  U3CStartOffU3Ec__Iterator0_t1448251964  : public RuntimeObject
{
public:
	// System.String Outback.Gameplay.SlotGameEgypt/<StartOff>c__Iterator0::<arr1>__0
	String_t* ___U3Carr1U3E__0_0;
	// System.Int32[0...,0...] Outback.Gameplay.SlotGameEgypt/<StartOff>c__Iterator0::<linesRules>__0
	Int32U5B0___U2C0___U5D_t385246373* ___U3ClinesRulesU3E__0_1;
	// System.Int64 Outback.Gameplay.SlotGameEgypt/<StartOff>c__Iterator0::<totalAmountWon>__0
	int64_t ___U3CtotalAmountWonU3E__0_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> Outback.Gameplay.SlotGameEgypt/<StartOff>c__Iterator0::<allMatchedImages>__0
	List_1_t4142344393 * ___U3CallMatchedImagesU3E__0_3;
	// System.Int32 Outback.Gameplay.SlotGameEgypt/<StartOff>c__Iterator0::<numOfLines>__0
	int32_t ___U3CnumOfLinesU3E__0_4;
	// Outback.Gameplay.SlotGameEgypt Outback.Gameplay.SlotGameEgypt/<StartOff>c__Iterator0::$this
	SlotGameEgypt_t1252059770 * ___U24this_5;
	// System.Object Outback.Gameplay.SlotGameEgypt/<StartOff>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean Outback.Gameplay.SlotGameEgypt/<StartOff>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 Outback.Gameplay.SlotGameEgypt/<StartOff>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3Carr1U3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1448251964, ___U3Carr1U3E__0_0)); }
	inline String_t* get_U3Carr1U3E__0_0() const { return ___U3Carr1U3E__0_0; }
	inline String_t** get_address_of_U3Carr1U3E__0_0() { return &___U3Carr1U3E__0_0; }
	inline void set_U3Carr1U3E__0_0(String_t* value)
	{
		___U3Carr1U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Carr1U3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3ClinesRulesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1448251964, ___U3ClinesRulesU3E__0_1)); }
	inline Int32U5B0___U2C0___U5D_t385246373* get_U3ClinesRulesU3E__0_1() const { return ___U3ClinesRulesU3E__0_1; }
	inline Int32U5B0___U2C0___U5D_t385246373** get_address_of_U3ClinesRulesU3E__0_1() { return &___U3ClinesRulesU3E__0_1; }
	inline void set_U3ClinesRulesU3E__0_1(Int32U5B0___U2C0___U5D_t385246373* value)
	{
		___U3ClinesRulesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClinesRulesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtotalAmountWonU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1448251964, ___U3CtotalAmountWonU3E__0_2)); }
	inline int64_t get_U3CtotalAmountWonU3E__0_2() const { return ___U3CtotalAmountWonU3E__0_2; }
	inline int64_t* get_address_of_U3CtotalAmountWonU3E__0_2() { return &___U3CtotalAmountWonU3E__0_2; }
	inline void set_U3CtotalAmountWonU3E__0_2(int64_t value)
	{
		___U3CtotalAmountWonU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CallMatchedImagesU3E__0_3() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1448251964, ___U3CallMatchedImagesU3E__0_3)); }
	inline List_1_t4142344393 * get_U3CallMatchedImagesU3E__0_3() const { return ___U3CallMatchedImagesU3E__0_3; }
	inline List_1_t4142344393 ** get_address_of_U3CallMatchedImagesU3E__0_3() { return &___U3CallMatchedImagesU3E__0_3; }
	inline void set_U3CallMatchedImagesU3E__0_3(List_1_t4142344393 * value)
	{
		___U3CallMatchedImagesU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CallMatchedImagesU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CnumOfLinesU3E__0_4() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1448251964, ___U3CnumOfLinesU3E__0_4)); }
	inline int32_t get_U3CnumOfLinesU3E__0_4() const { return ___U3CnumOfLinesU3E__0_4; }
	inline int32_t* get_address_of_U3CnumOfLinesU3E__0_4() { return &___U3CnumOfLinesU3E__0_4; }
	inline void set_U3CnumOfLinesU3E__0_4(int32_t value)
	{
		___U3CnumOfLinesU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1448251964, ___U24this_5)); }
	inline SlotGameEgypt_t1252059770 * get_U24this_5() const { return ___U24this_5; }
	inline SlotGameEgypt_t1252059770 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(SlotGameEgypt_t1252059770 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1448251964, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1448251964, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1448251964, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

struct U3CStartOffU3Ec__Iterator0_t1448251964_StaticFields
{
public:
	// System.Converter`2<UnityEngine.UI.Image,System.String> Outback.Gameplay.SlotGameEgypt/<StartOff>c__Iterator0::<>f__am$cache0
	Converter_2_t804895609 * ___U3CU3Ef__amU24cache0_9;
	// System.Predicate`1<UnityEngine.UI.Image> Outback.Gameplay.SlotGameEgypt/<StartOff>c__Iterator0::<>f__am$cache1
	Predicate_1_t3495563775 * ___U3CU3Ef__amU24cache1_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1448251964_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Converter_2_t804895609 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Converter_2_t804895609 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Converter_2_t804895609 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_10() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1448251964_StaticFields, ___U3CU3Ef__amU24cache1_10)); }
	inline Predicate_1_t3495563775 * get_U3CU3Ef__amU24cache1_10() const { return ___U3CU3Ef__amU24cache1_10; }
	inline Predicate_1_t3495563775 ** get_address_of_U3CU3Ef__amU24cache1_10() { return &___U3CU3Ef__amU24cache1_10; }
	inline void set_U3CU3Ef__amU24cache1_10(Predicate_1_t3495563775 * value)
	{
		___U3CU3Ef__amU24cache1_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTOFFU3EC__ITERATOR0_T1448251964_H
#ifndef U3CSTARTRADIALLOADU3EC__ANONSTOREY0_T2579740159_H
#define U3CSTARTRADIALLOADU3EC__ANONSTOREY0_T2579740159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RadialLoading/<StartRadialLoad>c__AnonStorey0
struct  U3CStartRadialLoadU3Ec__AnonStorey0_t2579740159  : public RuntimeObject
{
public:
	// System.Single RadialLoading/<StartRadialLoad>c__AnonStorey0::a
	float ___a_0;
	// RadialLoading RadialLoading/<StartRadialLoad>c__AnonStorey0::$this
	RadialLoading_t2588181753 * ___U24this_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(U3CStartRadialLoadU3Ec__AnonStorey0_t2579740159, ___a_0)); }
	inline float get_a_0() const { return ___a_0; }
	inline float* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(float value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartRadialLoadU3Ec__AnonStorey0_t2579740159, ___U24this_1)); }
	inline RadialLoading_t2588181753 * get_U24this_1() const { return ___U24this_1; }
	inline RadialLoading_t2588181753 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(RadialLoading_t2588181753 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTRADIALLOADU3EC__ANONSTOREY0_T2579740159_H
#ifndef U3CPOPULATEFRIENDSPANELU3EC__ANONSTOREY1_T1616196176_H
#define U3CPOPULATEFRIENDSPANELU3EC__ANONSTOREY1_T1616196176_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LobbyController/<PopulateFriendsPanel>c__AnonStorey1
struct  U3CPopulateFriendsPanelU3Ec__AnonStorey1_t1616196176  : public RuntimeObject
{
public:
	// UnityEngine.GameObject LobbyController/<PopulateFriendsPanel>c__AnonStorey1::friendInfo
	GameObject_t1113636619 * ___friendInfo_0;

public:
	inline static int32_t get_offset_of_friendInfo_0() { return static_cast<int32_t>(offsetof(U3CPopulateFriendsPanelU3Ec__AnonStorey1_t1616196176, ___friendInfo_0)); }
	inline GameObject_t1113636619 * get_friendInfo_0() const { return ___friendInfo_0; }
	inline GameObject_t1113636619 ** get_address_of_friendInfo_0() { return &___friendInfo_0; }
	inline void set_friendInfo_0(GameObject_t1113636619 * value)
	{
		___friendInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___friendInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPOPULATEFRIENDSPANELU3EC__ANONSTOREY1_T1616196176_H
#ifndef U3CGIVEREWARDSU3EC__ANONSTOREY1_T4164667808_H
#define U3CGIVEREWARDSU3EC__ANONSTOREY1_T4164667808_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Core.Reward/<GiveRewards>c__AnonStorey1
struct  U3CGiveRewardsU3Ec__AnonStorey1_t4164667808  : public RuntimeObject
{
public:
	// System.Int32 Outback.Core.Reward/<GiveRewards>c__AnonStorey1::a
	int32_t ___a_0;
	// System.Int64 Outback.Core.Reward/<GiveRewards>c__AnonStorey1::amntWon
	int64_t ___amntWon_1;
	// System.Int64 Outback.Core.Reward/<GiveRewards>c__AnonStorey1::newAmount
	int64_t ___newAmount_2;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(U3CGiveRewardsU3Ec__AnonStorey1_t4164667808, ___a_0)); }
	inline int32_t get_a_0() const { return ___a_0; }
	inline int32_t* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(int32_t value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_amntWon_1() { return static_cast<int32_t>(offsetof(U3CGiveRewardsU3Ec__AnonStorey1_t4164667808, ___amntWon_1)); }
	inline int64_t get_amntWon_1() const { return ___amntWon_1; }
	inline int64_t* get_address_of_amntWon_1() { return &___amntWon_1; }
	inline void set_amntWon_1(int64_t value)
	{
		___amntWon_1 = value;
	}

	inline static int32_t get_offset_of_newAmount_2() { return static_cast<int32_t>(offsetof(U3CGiveRewardsU3Ec__AnonStorey1_t4164667808, ___newAmount_2)); }
	inline int64_t get_newAmount_2() const { return ___newAmount_2; }
	inline int64_t* get_address_of_newAmount_2() { return &___newAmount_2; }
	inline void set_newAmount_2(int64_t value)
	{
		___newAmount_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGIVEREWARDSU3EC__ANONSTOREY1_T4164667808_H
#ifndef U3CGETFRIENDLISTU3EC__ANONSTOREY2_T3832777403_H
#define U3CGETFRIENDLISTU3EC__ANONSTOREY2_T3832777403_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FBHolder/<GetFriendList>c__AnonStorey2
struct  U3CGetFriendListU3Ec__AnonStorey2_t3832777403  : public RuntimeObject
{
public:
	// FacebookFriend FBHolder/<GetFriendList>c__AnonStorey2::fbFriend
	FacebookFriend_t1860979812 * ___fbFriend_0;

public:
	inline static int32_t get_offset_of_fbFriend_0() { return static_cast<int32_t>(offsetof(U3CGetFriendListU3Ec__AnonStorey2_t3832777403, ___fbFriend_0)); }
	inline FacebookFriend_t1860979812 * get_fbFriend_0() const { return ___fbFriend_0; }
	inline FacebookFriend_t1860979812 ** get_address_of_fbFriend_0() { return &___fbFriend_0; }
	inline void set_fbFriend_0(FacebookFriend_t1860979812 * value)
	{
		___fbFriend_0 = value;
		Il2CppCodeGenWriteBarrier((&___fbFriend_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETFRIENDLISTU3EC__ANONSTOREY2_T3832777403_H
#ifndef U3CLOADPICTUREENUMERATORU3EC__ITERATOR0_T1346235464_H
#define U3CLOADPICTUREENUMERATORU3EC__ITERATOR0_T1346235464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FBHolder/<LoadPictureEnumerator>c__Iterator0
struct  U3CLoadPictureEnumeratorU3Ec__Iterator0_t1346235464  : public RuntimeObject
{
public:
	// System.String FBHolder/<LoadPictureEnumerator>c__Iterator0::url
	String_t* ___url_0;
	// UnityEngine.WWW FBHolder/<LoadPictureEnumerator>c__Iterator0::<www>__0
	WWW_t3688466362 * ___U3CwwwU3E__0_1;
	// FBHolder/LoadPictureCallback FBHolder/<LoadPictureEnumerator>c__Iterator0::callback
	LoadPictureCallback_t2056340374 * ___callback_2;
	// System.Object FBHolder/<LoadPictureEnumerator>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean FBHolder/<LoadPictureEnumerator>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 FBHolder/<LoadPictureEnumerator>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_url_0() { return static_cast<int32_t>(offsetof(U3CLoadPictureEnumeratorU3Ec__Iterator0_t1346235464, ___url_0)); }
	inline String_t* get_url_0() const { return ___url_0; }
	inline String_t** get_address_of_url_0() { return &___url_0; }
	inline void set_url_0(String_t* value)
	{
		___url_0 = value;
		Il2CppCodeGenWriteBarrier((&___url_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadPictureEnumeratorU3Ec__Iterator0_t1346235464, ___U3CwwwU3E__0_1)); }
	inline WWW_t3688466362 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline WWW_t3688466362 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(WWW_t3688466362 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CLoadPictureEnumeratorU3Ec__Iterator0_t1346235464, ___callback_2)); }
	inline LoadPictureCallback_t2056340374 * get_callback_2() const { return ___callback_2; }
	inline LoadPictureCallback_t2056340374 ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(LoadPictureCallback_t2056340374 * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLoadPictureEnumeratorU3Ec__Iterator0_t1346235464, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CLoadPictureEnumeratorU3Ec__Iterator0_t1346235464, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CLoadPictureEnumeratorU3Ec__Iterator0_t1346235464, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADPICTUREENUMERATORU3EC__ITERATOR0_T1346235464_H
#ifndef U3CLATECALLREDIRECTU3EC__ITERATOR1_T1650181486_H
#define U3CLATECALLREDIRECTU3EC__ITERATOR1_T1650181486_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FBHolder/<LateCallRedirect>c__Iterator1
struct  U3CLateCallRedirectU3Ec__Iterator1_t1650181486  : public RuntimeObject
{
public:
	// FBHolder FBHolder/<LateCallRedirect>c__Iterator1::$this
	FBHolder_t1712543493 * ___U24this_0;
	// System.Object FBHolder/<LateCallRedirect>c__Iterator1::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean FBHolder/<LateCallRedirect>c__Iterator1::$disposing
	bool ___U24disposing_2;
	// System.Int32 FBHolder/<LateCallRedirect>c__Iterator1::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CLateCallRedirectU3Ec__Iterator1_t1650181486, ___U24this_0)); }
	inline FBHolder_t1712543493 * get_U24this_0() const { return ___U24this_0; }
	inline FBHolder_t1712543493 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(FBHolder_t1712543493 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CLateCallRedirectU3Ec__Iterator1_t1650181486, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CLateCallRedirectU3Ec__Iterator1_t1650181486, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CLateCallRedirectU3Ec__Iterator1_t1650181486, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLATECALLREDIRECTU3EC__ITERATOR1_T1650181486_H
#ifndef U3CGIVEREWARDSU3EC__ANONSTOREY0_T2598583867_H
#define U3CGIVEREWARDSU3EC__ANONSTOREY0_T2598583867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Core.Reward/<GiveRewards>c__AnonStorey0
struct  U3CGiveRewardsU3Ec__AnonStorey0_t2598583867  : public RuntimeObject
{
public:
	// System.Int32 Outback.Core.Reward/<GiveRewards>c__AnonStorey0::a
	int32_t ___a_0;
	// System.Int64 Outback.Core.Reward/<GiveRewards>c__AnonStorey0::amntWon
	int64_t ___amntWon_1;
	// System.Int64 Outback.Core.Reward/<GiveRewards>c__AnonStorey0::newAmount
	int64_t ___newAmount_2;
	// Outback.Core.Reward Outback.Core.Reward/<GiveRewards>c__AnonStorey0::$this
	Reward_t2838753371 * ___U24this_3;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(U3CGiveRewardsU3Ec__AnonStorey0_t2598583867, ___a_0)); }
	inline int32_t get_a_0() const { return ___a_0; }
	inline int32_t* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(int32_t value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_amntWon_1() { return static_cast<int32_t>(offsetof(U3CGiveRewardsU3Ec__AnonStorey0_t2598583867, ___amntWon_1)); }
	inline int64_t get_amntWon_1() const { return ___amntWon_1; }
	inline int64_t* get_address_of_amntWon_1() { return &___amntWon_1; }
	inline void set_amntWon_1(int64_t value)
	{
		___amntWon_1 = value;
	}

	inline static int32_t get_offset_of_newAmount_2() { return static_cast<int32_t>(offsetof(U3CGiveRewardsU3Ec__AnonStorey0_t2598583867, ___newAmount_2)); }
	inline int64_t get_newAmount_2() const { return ___newAmount_2; }
	inline int64_t* get_address_of_newAmount_2() { return &___newAmount_2; }
	inline void set_newAmount_2(int64_t value)
	{
		___newAmount_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CGiveRewardsU3Ec__AnonStorey0_t2598583867, ___U24this_3)); }
	inline Reward_t2838753371 * get_U24this_3() const { return ___U24this_3; }
	inline Reward_t2838753371 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(Reward_t2838753371 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGIVEREWARDSU3EC__ANONSTOREY0_T2598583867_H
#ifndef U3CSTARTOFFU3EC__ITERATOR0_T528495272_H
#define U3CSTARTOFFU3EC__ITERATOR0_T528495272_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Gameplay.SlotGameSafari/<StartOff>c__Iterator0
struct  U3CStartOffU3Ec__Iterator0_t528495272  : public RuntimeObject
{
public:
	// System.String Outback.Gameplay.SlotGameSafari/<StartOff>c__Iterator0::<arr1>__0
	String_t* ___U3Carr1U3E__0_0;
	// System.Int32[0...,0...] Outback.Gameplay.SlotGameSafari/<StartOff>c__Iterator0::<linesRules>__0
	Int32U5B0___U2C0___U5D_t385246373* ___U3ClinesRulesU3E__0_1;
	// System.Int64 Outback.Gameplay.SlotGameSafari/<StartOff>c__Iterator0::<totalAmountWon>__0
	int64_t ___U3CtotalAmountWonU3E__0_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> Outback.Gameplay.SlotGameSafari/<StartOff>c__Iterator0::<allMatchedImages>__0
	List_1_t4142344393 * ___U3CallMatchedImagesU3E__0_3;
	// System.Int32 Outback.Gameplay.SlotGameSafari/<StartOff>c__Iterator0::<numOfLines>__0
	int32_t ___U3CnumOfLinesU3E__0_4;
	// Outback.Gameplay.SlotGameSafari Outback.Gameplay.SlotGameSafari/<StartOff>c__Iterator0::$this
	SlotGameSafari_t3944626951 * ___U24this_5;
	// System.Object Outback.Gameplay.SlotGameSafari/<StartOff>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean Outback.Gameplay.SlotGameSafari/<StartOff>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 Outback.Gameplay.SlotGameSafari/<StartOff>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3Carr1U3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t528495272, ___U3Carr1U3E__0_0)); }
	inline String_t* get_U3Carr1U3E__0_0() const { return ___U3Carr1U3E__0_0; }
	inline String_t** get_address_of_U3Carr1U3E__0_0() { return &___U3Carr1U3E__0_0; }
	inline void set_U3Carr1U3E__0_0(String_t* value)
	{
		___U3Carr1U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Carr1U3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3ClinesRulesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t528495272, ___U3ClinesRulesU3E__0_1)); }
	inline Int32U5B0___U2C0___U5D_t385246373* get_U3ClinesRulesU3E__0_1() const { return ___U3ClinesRulesU3E__0_1; }
	inline Int32U5B0___U2C0___U5D_t385246373** get_address_of_U3ClinesRulesU3E__0_1() { return &___U3ClinesRulesU3E__0_1; }
	inline void set_U3ClinesRulesU3E__0_1(Int32U5B0___U2C0___U5D_t385246373* value)
	{
		___U3ClinesRulesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClinesRulesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtotalAmountWonU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t528495272, ___U3CtotalAmountWonU3E__0_2)); }
	inline int64_t get_U3CtotalAmountWonU3E__0_2() const { return ___U3CtotalAmountWonU3E__0_2; }
	inline int64_t* get_address_of_U3CtotalAmountWonU3E__0_2() { return &___U3CtotalAmountWonU3E__0_2; }
	inline void set_U3CtotalAmountWonU3E__0_2(int64_t value)
	{
		___U3CtotalAmountWonU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CallMatchedImagesU3E__0_3() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t528495272, ___U3CallMatchedImagesU3E__0_3)); }
	inline List_1_t4142344393 * get_U3CallMatchedImagesU3E__0_3() const { return ___U3CallMatchedImagesU3E__0_3; }
	inline List_1_t4142344393 ** get_address_of_U3CallMatchedImagesU3E__0_3() { return &___U3CallMatchedImagesU3E__0_3; }
	inline void set_U3CallMatchedImagesU3E__0_3(List_1_t4142344393 * value)
	{
		___U3CallMatchedImagesU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CallMatchedImagesU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CnumOfLinesU3E__0_4() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t528495272, ___U3CnumOfLinesU3E__0_4)); }
	inline int32_t get_U3CnumOfLinesU3E__0_4() const { return ___U3CnumOfLinesU3E__0_4; }
	inline int32_t* get_address_of_U3CnumOfLinesU3E__0_4() { return &___U3CnumOfLinesU3E__0_4; }
	inline void set_U3CnumOfLinesU3E__0_4(int32_t value)
	{
		___U3CnumOfLinesU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t528495272, ___U24this_5)); }
	inline SlotGameSafari_t3944626951 * get_U24this_5() const { return ___U24this_5; }
	inline SlotGameSafari_t3944626951 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(SlotGameSafari_t3944626951 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t528495272, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t528495272, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t528495272, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

struct U3CStartOffU3Ec__Iterator0_t528495272_StaticFields
{
public:
	// System.Converter`2<UnityEngine.UI.Image,System.String> Outback.Gameplay.SlotGameSafari/<StartOff>c__Iterator0::<>f__am$cache0
	Converter_2_t804895609 * ___U3CU3Ef__amU24cache0_9;
	// System.Predicate`1<UnityEngine.UI.Image> Outback.Gameplay.SlotGameSafari/<StartOff>c__Iterator0::<>f__am$cache1
	Predicate_1_t3495563775 * ___U3CU3Ef__amU24cache1_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t528495272_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Converter_2_t804895609 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Converter_2_t804895609 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Converter_2_t804895609 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_10() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t528495272_StaticFields, ___U3CU3Ef__amU24cache1_10)); }
	inline Predicate_1_t3495563775 * get_U3CU3Ef__amU24cache1_10() const { return ___U3CU3Ef__amU24cache1_10; }
	inline Predicate_1_t3495563775 ** get_address_of_U3CU3Ef__amU24cache1_10() { return &___U3CU3Ef__amU24cache1_10; }
	inline void set_U3CU3Ef__amU24cache1_10(Predicate_1_t3495563775 * value)
	{
		___U3CU3Ef__amU24cache1_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTOFFU3EC__ITERATOR0_T528495272_H
#ifndef U3CGETSPRITEFROMCOMMONSU3EC__ANONSTOREY0_T1183225816_H
#define U3CGETSPRITEFROMCOMMONSU3EC__ANONSTOREY0_T1183225816_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Managers.MasterConfigsManager/<GetSpriteFromCommons>c__AnonStorey0
struct  U3CGetSpriteFromCommonsU3Ec__AnonStorey0_t1183225816  : public RuntimeObject
{
public:
	// System.String Outback.Managers.MasterConfigsManager/<GetSpriteFromCommons>c__AnonStorey0::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CGetSpriteFromCommonsU3Ec__AnonStorey0_t1183225816, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETSPRITEFROMCOMMONSU3EC__ANONSTOREY0_T1183225816_H
#ifndef U3CSTARTOFFU3EC__ITERATOR0_T278533614_H
#define U3CSTARTOFFU3EC__ITERATOR0_T278533614_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Gameplay.SlotGameNewYork/<StartOff>c__Iterator0
struct  U3CStartOffU3Ec__Iterator0_t278533614  : public RuntimeObject
{
public:
	// System.String Outback.Gameplay.SlotGameNewYork/<StartOff>c__Iterator0::<arr1>__0
	String_t* ___U3Carr1U3E__0_0;
	// System.Int32[0...,0...] Outback.Gameplay.SlotGameNewYork/<StartOff>c__Iterator0::<linesRules>__0
	Int32U5B0___U2C0___U5D_t385246373* ___U3ClinesRulesU3E__0_1;
	// System.Int64 Outback.Gameplay.SlotGameNewYork/<StartOff>c__Iterator0::<totalAmountWon>__0
	int64_t ___U3CtotalAmountWonU3E__0_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> Outback.Gameplay.SlotGameNewYork/<StartOff>c__Iterator0::<allMatchedImages>__0
	List_1_t4142344393 * ___U3CallMatchedImagesU3E__0_3;
	// System.Int32 Outback.Gameplay.SlotGameNewYork/<StartOff>c__Iterator0::<numOfLines>__0
	int32_t ___U3CnumOfLinesU3E__0_4;
	// Outback.Gameplay.SlotGameNewYork Outback.Gameplay.SlotGameNewYork/<StartOff>c__Iterator0::$this
	SlotGameNewYork_t345457032 * ___U24this_5;
	// System.Object Outback.Gameplay.SlotGameNewYork/<StartOff>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean Outback.Gameplay.SlotGameNewYork/<StartOff>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 Outback.Gameplay.SlotGameNewYork/<StartOff>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3Carr1U3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t278533614, ___U3Carr1U3E__0_0)); }
	inline String_t* get_U3Carr1U3E__0_0() const { return ___U3Carr1U3E__0_0; }
	inline String_t** get_address_of_U3Carr1U3E__0_0() { return &___U3Carr1U3E__0_0; }
	inline void set_U3Carr1U3E__0_0(String_t* value)
	{
		___U3Carr1U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Carr1U3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3ClinesRulesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t278533614, ___U3ClinesRulesU3E__0_1)); }
	inline Int32U5B0___U2C0___U5D_t385246373* get_U3ClinesRulesU3E__0_1() const { return ___U3ClinesRulesU3E__0_1; }
	inline Int32U5B0___U2C0___U5D_t385246373** get_address_of_U3ClinesRulesU3E__0_1() { return &___U3ClinesRulesU3E__0_1; }
	inline void set_U3ClinesRulesU3E__0_1(Int32U5B0___U2C0___U5D_t385246373* value)
	{
		___U3ClinesRulesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClinesRulesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtotalAmountWonU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t278533614, ___U3CtotalAmountWonU3E__0_2)); }
	inline int64_t get_U3CtotalAmountWonU3E__0_2() const { return ___U3CtotalAmountWonU3E__0_2; }
	inline int64_t* get_address_of_U3CtotalAmountWonU3E__0_2() { return &___U3CtotalAmountWonU3E__0_2; }
	inline void set_U3CtotalAmountWonU3E__0_2(int64_t value)
	{
		___U3CtotalAmountWonU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CallMatchedImagesU3E__0_3() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t278533614, ___U3CallMatchedImagesU3E__0_3)); }
	inline List_1_t4142344393 * get_U3CallMatchedImagesU3E__0_3() const { return ___U3CallMatchedImagesU3E__0_3; }
	inline List_1_t4142344393 ** get_address_of_U3CallMatchedImagesU3E__0_3() { return &___U3CallMatchedImagesU3E__0_3; }
	inline void set_U3CallMatchedImagesU3E__0_3(List_1_t4142344393 * value)
	{
		___U3CallMatchedImagesU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CallMatchedImagesU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CnumOfLinesU3E__0_4() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t278533614, ___U3CnumOfLinesU3E__0_4)); }
	inline int32_t get_U3CnumOfLinesU3E__0_4() const { return ___U3CnumOfLinesU3E__0_4; }
	inline int32_t* get_address_of_U3CnumOfLinesU3E__0_4() { return &___U3CnumOfLinesU3E__0_4; }
	inline void set_U3CnumOfLinesU3E__0_4(int32_t value)
	{
		___U3CnumOfLinesU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t278533614, ___U24this_5)); }
	inline SlotGameNewYork_t345457032 * get_U24this_5() const { return ___U24this_5; }
	inline SlotGameNewYork_t345457032 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(SlotGameNewYork_t345457032 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t278533614, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t278533614, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t278533614, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

struct U3CStartOffU3Ec__Iterator0_t278533614_StaticFields
{
public:
	// System.Converter`2<UnityEngine.UI.Image,System.String> Outback.Gameplay.SlotGameNewYork/<StartOff>c__Iterator0::<>f__am$cache0
	Converter_2_t804895609 * ___U3CU3Ef__amU24cache0_9;
	// System.Predicate`1<UnityEngine.UI.Image> Outback.Gameplay.SlotGameNewYork/<StartOff>c__Iterator0::<>f__am$cache1
	Predicate_1_t3495563775 * ___U3CU3Ef__amU24cache1_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t278533614_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Converter_2_t804895609 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Converter_2_t804895609 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Converter_2_t804895609 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_10() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t278533614_StaticFields, ___U3CU3Ef__amU24cache1_10)); }
	inline Predicate_1_t3495563775 * get_U3CU3Ef__amU24cache1_10() const { return ___U3CU3Ef__amU24cache1_10; }
	inline Predicate_1_t3495563775 ** get_address_of_U3CU3Ef__amU24cache1_10() { return &___U3CU3Ef__amU24cache1_10; }
	inline void set_U3CU3Ef__amU24cache1_10(Predicate_1_t3495563775 * value)
	{
		___U3CU3Ef__amU24cache1_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTOFFU3EC__ITERATOR0_T278533614_H
#ifndef U3CCHECKPLAYERU3EC__ITERATOR1_T2156344797_H
#define U3CCHECKPLAYERU3EC__ITERATOR1_T2156344797_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Managers.PlayerManager/<CheckPlayer>c__Iterator1
struct  U3CCheckPlayerU3Ec__Iterator1_t2156344797  : public RuntimeObject
{
public:
	// System.String Outback.Managers.PlayerManager/<CheckPlayer>c__Iterator1::<url>__0
	String_t* ___U3CurlU3E__0_0;
	// UnityEngine.WWWForm Outback.Managers.PlayerManager/<CheckPlayer>c__Iterator1::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_1;
	// UnityEngine.WWW Outback.Managers.PlayerManager/<CheckPlayer>c__Iterator1::<download>__0
	WWW_t3688466362 * ___U3CdownloadU3E__0_2;
	// Outback.Managers.PlayerManager Outback.Managers.PlayerManager/<CheckPlayer>c__Iterator1::$this
	PlayerManager_t1841934776 * ___U24this_3;
	// System.Object Outback.Managers.PlayerManager/<CheckPlayer>c__Iterator1::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Outback.Managers.PlayerManager/<CheckPlayer>c__Iterator1::$disposing
	bool ___U24disposing_5;
	// System.Int32 Outback.Managers.PlayerManager/<CheckPlayer>c__Iterator1::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CurlU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCheckPlayerU3Ec__Iterator1_t2156344797, ___U3CurlU3E__0_0)); }
	inline String_t* get_U3CurlU3E__0_0() const { return ___U3CurlU3E__0_0; }
	inline String_t** get_address_of_U3CurlU3E__0_0() { return &___U3CurlU3E__0_0; }
	inline void set_U3CurlU3E__0_0(String_t* value)
	{
		___U3CurlU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CformU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCheckPlayerU3Ec__Iterator1_t2156344797, ___U3CformU3E__0_1)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_1() const { return ___U3CformU3E__0_1; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_1() { return &___U3CformU3E__0_1; }
	inline void set_U3CformU3E__0_1(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CdownloadU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCheckPlayerU3Ec__Iterator1_t2156344797, ___U3CdownloadU3E__0_2)); }
	inline WWW_t3688466362 * get_U3CdownloadU3E__0_2() const { return ___U3CdownloadU3E__0_2; }
	inline WWW_t3688466362 ** get_address_of_U3CdownloadU3E__0_2() { return &___U3CdownloadU3E__0_2; }
	inline void set_U3CdownloadU3E__0_2(WWW_t3688466362 * value)
	{
		___U3CdownloadU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdownloadU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CCheckPlayerU3Ec__Iterator1_t2156344797, ___U24this_3)); }
	inline PlayerManager_t1841934776 * get_U24this_3() const { return ___U24this_3; }
	inline PlayerManager_t1841934776 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(PlayerManager_t1841934776 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCheckPlayerU3Ec__Iterator1_t2156344797, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CCheckPlayerU3Ec__Iterator1_t2156344797, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CCheckPlayerU3Ec__Iterator1_t2156344797, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKPLAYERU3EC__ITERATOR1_T2156344797_H
#ifndef U3CSTARTOFFU3EC__ITERATOR0_T282260692_H
#define U3CSTARTOFFU3EC__ITERATOR0_T282260692_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Gameplay.SlotGameOuterSpace/<StartOff>c__Iterator0
struct  U3CStartOffU3Ec__Iterator0_t282260692  : public RuntimeObject
{
public:
	// System.String Outback.Gameplay.SlotGameOuterSpace/<StartOff>c__Iterator0::<arr1>__0
	String_t* ___U3Carr1U3E__0_0;
	// System.Int32[0...,0...] Outback.Gameplay.SlotGameOuterSpace/<StartOff>c__Iterator0::<linesRules>__0
	Int32U5B0___U2C0___U5D_t385246373* ___U3ClinesRulesU3E__0_1;
	// System.Int64 Outback.Gameplay.SlotGameOuterSpace/<StartOff>c__Iterator0::<totalAmountWon>__0
	int64_t ___U3CtotalAmountWonU3E__0_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> Outback.Gameplay.SlotGameOuterSpace/<StartOff>c__Iterator0::<allMatchedImages>__0
	List_1_t4142344393 * ___U3CallMatchedImagesU3E__0_3;
	// System.Int32 Outback.Gameplay.SlotGameOuterSpace/<StartOff>c__Iterator0::<numOfLines>__0
	int32_t ___U3CnumOfLinesU3E__0_4;
	// Outback.Gameplay.SlotGameOuterSpace Outback.Gameplay.SlotGameOuterSpace/<StartOff>c__Iterator0::$this
	SlotGameOuterSpace_t3776195372 * ___U24this_5;
	// System.Object Outback.Gameplay.SlotGameOuterSpace/<StartOff>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean Outback.Gameplay.SlotGameOuterSpace/<StartOff>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 Outback.Gameplay.SlotGameOuterSpace/<StartOff>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3Carr1U3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t282260692, ___U3Carr1U3E__0_0)); }
	inline String_t* get_U3Carr1U3E__0_0() const { return ___U3Carr1U3E__0_0; }
	inline String_t** get_address_of_U3Carr1U3E__0_0() { return &___U3Carr1U3E__0_0; }
	inline void set_U3Carr1U3E__0_0(String_t* value)
	{
		___U3Carr1U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Carr1U3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3ClinesRulesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t282260692, ___U3ClinesRulesU3E__0_1)); }
	inline Int32U5B0___U2C0___U5D_t385246373* get_U3ClinesRulesU3E__0_1() const { return ___U3ClinesRulesU3E__0_1; }
	inline Int32U5B0___U2C0___U5D_t385246373** get_address_of_U3ClinesRulesU3E__0_1() { return &___U3ClinesRulesU3E__0_1; }
	inline void set_U3ClinesRulesU3E__0_1(Int32U5B0___U2C0___U5D_t385246373* value)
	{
		___U3ClinesRulesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClinesRulesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtotalAmountWonU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t282260692, ___U3CtotalAmountWonU3E__0_2)); }
	inline int64_t get_U3CtotalAmountWonU3E__0_2() const { return ___U3CtotalAmountWonU3E__0_2; }
	inline int64_t* get_address_of_U3CtotalAmountWonU3E__0_2() { return &___U3CtotalAmountWonU3E__0_2; }
	inline void set_U3CtotalAmountWonU3E__0_2(int64_t value)
	{
		___U3CtotalAmountWonU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CallMatchedImagesU3E__0_3() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t282260692, ___U3CallMatchedImagesU3E__0_3)); }
	inline List_1_t4142344393 * get_U3CallMatchedImagesU3E__0_3() const { return ___U3CallMatchedImagesU3E__0_3; }
	inline List_1_t4142344393 ** get_address_of_U3CallMatchedImagesU3E__0_3() { return &___U3CallMatchedImagesU3E__0_3; }
	inline void set_U3CallMatchedImagesU3E__0_3(List_1_t4142344393 * value)
	{
		___U3CallMatchedImagesU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CallMatchedImagesU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CnumOfLinesU3E__0_4() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t282260692, ___U3CnumOfLinesU3E__0_4)); }
	inline int32_t get_U3CnumOfLinesU3E__0_4() const { return ___U3CnumOfLinesU3E__0_4; }
	inline int32_t* get_address_of_U3CnumOfLinesU3E__0_4() { return &___U3CnumOfLinesU3E__0_4; }
	inline void set_U3CnumOfLinesU3E__0_4(int32_t value)
	{
		___U3CnumOfLinesU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t282260692, ___U24this_5)); }
	inline SlotGameOuterSpace_t3776195372 * get_U24this_5() const { return ___U24this_5; }
	inline SlotGameOuterSpace_t3776195372 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(SlotGameOuterSpace_t3776195372 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t282260692, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t282260692, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t282260692, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

struct U3CStartOffU3Ec__Iterator0_t282260692_StaticFields
{
public:
	// System.Converter`2<UnityEngine.UI.Image,System.String> Outback.Gameplay.SlotGameOuterSpace/<StartOff>c__Iterator0::<>f__am$cache0
	Converter_2_t804895609 * ___U3CU3Ef__amU24cache0_9;
	// System.Predicate`1<UnityEngine.UI.Image> Outback.Gameplay.SlotGameOuterSpace/<StartOff>c__Iterator0::<>f__am$cache1
	Predicate_1_t3495563775 * ___U3CU3Ef__amU24cache1_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t282260692_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Converter_2_t804895609 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Converter_2_t804895609 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Converter_2_t804895609 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_10() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t282260692_StaticFields, ___U3CU3Ef__amU24cache1_10)); }
	inline Predicate_1_t3495563775 * get_U3CU3Ef__amU24cache1_10() const { return ___U3CU3Ef__amU24cache1_10; }
	inline Predicate_1_t3495563775 ** get_address_of_U3CU3Ef__amU24cache1_10() { return &___U3CU3Ef__amU24cache1_10; }
	inline void set_U3CU3Ef__amU24cache1_10(Predicate_1_t3495563775 * value)
	{
		___U3CU3Ef__amU24cache1_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTOFFU3EC__ITERATOR0_T282260692_H
#ifndef U3CUPDATEUSERDETAILSU3EC__ITERATOR0_T3148111241_H
#define U3CUPDATEUSERDETAILSU3EC__ITERATOR0_T3148111241_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Managers.PlayerManager/<UpdateUserDetails>c__Iterator0
struct  U3CUpdateUserDetailsU3Ec__Iterator0_t3148111241  : public RuntimeObject
{
public:
	// System.String Outback.Managers.PlayerManager/<UpdateUserDetails>c__Iterator0::<url>__0
	String_t* ___U3CurlU3E__0_0;
	// UnityEngine.WWWForm Outback.Managers.PlayerManager/<UpdateUserDetails>c__Iterator0::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_1;
	// System.String Outback.Managers.PlayerManager/<UpdateUserDetails>c__Iterator0::updatefield
	String_t* ___updatefield_2;
	// UnityEngine.WWW Outback.Managers.PlayerManager/<UpdateUserDetails>c__Iterator0::<download>__0
	WWW_t3688466362 * ___U3CdownloadU3E__0_3;
	// System.Object Outback.Managers.PlayerManager/<UpdateUserDetails>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Outback.Managers.PlayerManager/<UpdateUserDetails>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Outback.Managers.PlayerManager/<UpdateUserDetails>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CurlU3E__0_0() { return static_cast<int32_t>(offsetof(U3CUpdateUserDetailsU3Ec__Iterator0_t3148111241, ___U3CurlU3E__0_0)); }
	inline String_t* get_U3CurlU3E__0_0() const { return ___U3CurlU3E__0_0; }
	inline String_t** get_address_of_U3CurlU3E__0_0() { return &___U3CurlU3E__0_0; }
	inline void set_U3CurlU3E__0_0(String_t* value)
	{
		___U3CurlU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CformU3E__0_1() { return static_cast<int32_t>(offsetof(U3CUpdateUserDetailsU3Ec__Iterator0_t3148111241, ___U3CformU3E__0_1)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_1() const { return ___U3CformU3E__0_1; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_1() { return &___U3CformU3E__0_1; }
	inline void set_U3CformU3E__0_1(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_1), value);
	}

	inline static int32_t get_offset_of_updatefield_2() { return static_cast<int32_t>(offsetof(U3CUpdateUserDetailsU3Ec__Iterator0_t3148111241, ___updatefield_2)); }
	inline String_t* get_updatefield_2() const { return ___updatefield_2; }
	inline String_t** get_address_of_updatefield_2() { return &___updatefield_2; }
	inline void set_updatefield_2(String_t* value)
	{
		___updatefield_2 = value;
		Il2CppCodeGenWriteBarrier((&___updatefield_2), value);
	}

	inline static int32_t get_offset_of_U3CdownloadU3E__0_3() { return static_cast<int32_t>(offsetof(U3CUpdateUserDetailsU3Ec__Iterator0_t3148111241, ___U3CdownloadU3E__0_3)); }
	inline WWW_t3688466362 * get_U3CdownloadU3E__0_3() const { return ___U3CdownloadU3E__0_3; }
	inline WWW_t3688466362 ** get_address_of_U3CdownloadU3E__0_3() { return &___U3CdownloadU3E__0_3; }
	inline void set_U3CdownloadU3E__0_3(WWW_t3688466362 * value)
	{
		___U3CdownloadU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdownloadU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CUpdateUserDetailsU3Ec__Iterator0_t3148111241, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CUpdateUserDetailsU3Ec__Iterator0_t3148111241, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CUpdateUserDetailsU3Ec__Iterator0_t3148111241, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CUPDATEUSERDETAILSU3EC__ITERATOR0_T3148111241_H
#ifndef U3CSTARTOFFU3EC__ITERATOR0_T2261323095_H
#define U3CSTARTOFFU3EC__ITERATOR0_T2261323095_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Gameplay.SlotGameRetro/<StartOff>c__Iterator0
struct  U3CStartOffU3Ec__Iterator0_t2261323095  : public RuntimeObject
{
public:
	// System.String Outback.Gameplay.SlotGameRetro/<StartOff>c__Iterator0::<arr1>__0
	String_t* ___U3Carr1U3E__0_0;
	// System.Int32[0...,0...] Outback.Gameplay.SlotGameRetro/<StartOff>c__Iterator0::<linesRules>__0
	Int32U5B0___U2C0___U5D_t385246373* ___U3ClinesRulesU3E__0_1;
	// System.Int64 Outback.Gameplay.SlotGameRetro/<StartOff>c__Iterator0::<totalAmountWon>__0
	int64_t ___U3CtotalAmountWonU3E__0_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> Outback.Gameplay.SlotGameRetro/<StartOff>c__Iterator0::<allMatchedImages>__0
	List_1_t4142344393 * ___U3CallMatchedImagesU3E__0_3;
	// System.Int32 Outback.Gameplay.SlotGameRetro/<StartOff>c__Iterator0::<numOfLines>__0
	int32_t ___U3CnumOfLinesU3E__0_4;
	// Outback.Gameplay.SlotGameRetro Outback.Gameplay.SlotGameRetro/<StartOff>c__Iterator0::$this
	SlotGameRetro_t96010562 * ___U24this_5;
	// System.Object Outback.Gameplay.SlotGameRetro/<StartOff>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean Outback.Gameplay.SlotGameRetro/<StartOff>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 Outback.Gameplay.SlotGameRetro/<StartOff>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3Carr1U3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t2261323095, ___U3Carr1U3E__0_0)); }
	inline String_t* get_U3Carr1U3E__0_0() const { return ___U3Carr1U3E__0_0; }
	inline String_t** get_address_of_U3Carr1U3E__0_0() { return &___U3Carr1U3E__0_0; }
	inline void set_U3Carr1U3E__0_0(String_t* value)
	{
		___U3Carr1U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Carr1U3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3ClinesRulesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t2261323095, ___U3ClinesRulesU3E__0_1)); }
	inline Int32U5B0___U2C0___U5D_t385246373* get_U3ClinesRulesU3E__0_1() const { return ___U3ClinesRulesU3E__0_1; }
	inline Int32U5B0___U2C0___U5D_t385246373** get_address_of_U3ClinesRulesU3E__0_1() { return &___U3ClinesRulesU3E__0_1; }
	inline void set_U3ClinesRulesU3E__0_1(Int32U5B0___U2C0___U5D_t385246373* value)
	{
		___U3ClinesRulesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClinesRulesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtotalAmountWonU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t2261323095, ___U3CtotalAmountWonU3E__0_2)); }
	inline int64_t get_U3CtotalAmountWonU3E__0_2() const { return ___U3CtotalAmountWonU3E__0_2; }
	inline int64_t* get_address_of_U3CtotalAmountWonU3E__0_2() { return &___U3CtotalAmountWonU3E__0_2; }
	inline void set_U3CtotalAmountWonU3E__0_2(int64_t value)
	{
		___U3CtotalAmountWonU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CallMatchedImagesU3E__0_3() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t2261323095, ___U3CallMatchedImagesU3E__0_3)); }
	inline List_1_t4142344393 * get_U3CallMatchedImagesU3E__0_3() const { return ___U3CallMatchedImagesU3E__0_3; }
	inline List_1_t4142344393 ** get_address_of_U3CallMatchedImagesU3E__0_3() { return &___U3CallMatchedImagesU3E__0_3; }
	inline void set_U3CallMatchedImagesU3E__0_3(List_1_t4142344393 * value)
	{
		___U3CallMatchedImagesU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CallMatchedImagesU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CnumOfLinesU3E__0_4() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t2261323095, ___U3CnumOfLinesU3E__0_4)); }
	inline int32_t get_U3CnumOfLinesU3E__0_4() const { return ___U3CnumOfLinesU3E__0_4; }
	inline int32_t* get_address_of_U3CnumOfLinesU3E__0_4() { return &___U3CnumOfLinesU3E__0_4; }
	inline void set_U3CnumOfLinesU3E__0_4(int32_t value)
	{
		___U3CnumOfLinesU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t2261323095, ___U24this_5)); }
	inline SlotGameRetro_t96010562 * get_U24this_5() const { return ___U24this_5; }
	inline SlotGameRetro_t96010562 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(SlotGameRetro_t96010562 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t2261323095, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t2261323095, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t2261323095, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

struct U3CStartOffU3Ec__Iterator0_t2261323095_StaticFields
{
public:
	// System.Converter`2<UnityEngine.UI.Image,System.String> Outback.Gameplay.SlotGameRetro/<StartOff>c__Iterator0::<>f__am$cache0
	Converter_2_t804895609 * ___U3CU3Ef__amU24cache0_9;
	// System.Predicate`1<UnityEngine.UI.Image> Outback.Gameplay.SlotGameRetro/<StartOff>c__Iterator0::<>f__am$cache1
	Predicate_1_t3495563775 * ___U3CU3Ef__amU24cache1_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t2261323095_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Converter_2_t804895609 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Converter_2_t804895609 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Converter_2_t804895609 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_10() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t2261323095_StaticFields, ___U3CU3Ef__amU24cache1_10)); }
	inline Predicate_1_t3495563775 * get_U3CU3Ef__amU24cache1_10() const { return ___U3CU3Ef__amU24cache1_10; }
	inline Predicate_1_t3495563775 ** get_address_of_U3CU3Ef__amU24cache1_10() { return &___U3CU3Ef__amU24cache1_10; }
	inline void set_U3CU3Ef__amU24cache1_10(Predicate_1_t3495563775 * value)
	{
		___U3CU3Ef__amU24cache1_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTOFFU3EC__ITERATOR0_T2261323095_H
#ifndef CROSSPROMOCREATIVE_T733597769_H
#define CROSSPROMOCREATIVE_T733597769_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Managers.CrossPromoCreative
struct  CrossPromoCreative_t733597769  : public RuntimeObject
{
public:
	// UnityEngine.Sprite Outback.Managers.CrossPromoCreative::interstitial
	Sprite_t280657092 * ___interstitial_0;
	// UnityEngine.Sprite Outback.Managers.CrossPromoCreative::banner
	Sprite_t280657092 * ___banner_1;
	// UnityEngine.Sprite Outback.Managers.CrossPromoCreative::close
	Sprite_t280657092 * ___close_2;
	// System.String Outback.Managers.CrossPromoCreative::url
	String_t* ___url_3;

public:
	inline static int32_t get_offset_of_interstitial_0() { return static_cast<int32_t>(offsetof(CrossPromoCreative_t733597769, ___interstitial_0)); }
	inline Sprite_t280657092 * get_interstitial_0() const { return ___interstitial_0; }
	inline Sprite_t280657092 ** get_address_of_interstitial_0() { return &___interstitial_0; }
	inline void set_interstitial_0(Sprite_t280657092 * value)
	{
		___interstitial_0 = value;
		Il2CppCodeGenWriteBarrier((&___interstitial_0), value);
	}

	inline static int32_t get_offset_of_banner_1() { return static_cast<int32_t>(offsetof(CrossPromoCreative_t733597769, ___banner_1)); }
	inline Sprite_t280657092 * get_banner_1() const { return ___banner_1; }
	inline Sprite_t280657092 ** get_address_of_banner_1() { return &___banner_1; }
	inline void set_banner_1(Sprite_t280657092 * value)
	{
		___banner_1 = value;
		Il2CppCodeGenWriteBarrier((&___banner_1), value);
	}

	inline static int32_t get_offset_of_close_2() { return static_cast<int32_t>(offsetof(CrossPromoCreative_t733597769, ___close_2)); }
	inline Sprite_t280657092 * get_close_2() const { return ___close_2; }
	inline Sprite_t280657092 ** get_address_of_close_2() { return &___close_2; }
	inline void set_close_2(Sprite_t280657092 * value)
	{
		___close_2 = value;
		Il2CppCodeGenWriteBarrier((&___close_2), value);
	}

	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(CrossPromoCreative_t733597769, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier((&___url_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CROSSPROMOCREATIVE_T733597769_H
#ifndef U3CCREATENEWGUESTU3EC__ITERATOR2_T4264403073_H
#define U3CCREATENEWGUESTU3EC__ITERATOR2_T4264403073_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Managers.PlayerManager/<CreateNewGuest>c__Iterator2
struct  U3CCreateNewGuestU3Ec__Iterator2_t4264403073  : public RuntimeObject
{
public:
	// System.String Outback.Managers.PlayerManager/<CreateNewGuest>c__Iterator2::<url>__1
	String_t* ___U3CurlU3E__1_0;
	// UnityEngine.WWWForm Outback.Managers.PlayerManager/<CreateNewGuest>c__Iterator2::<form>__1
	WWWForm_t4064702195 * ___U3CformU3E__1_1;
	// UnityEngine.WWW Outback.Managers.PlayerManager/<CreateNewGuest>c__Iterator2::<download>__1
	WWW_t3688466362 * ___U3CdownloadU3E__1_2;
	// Outback.Managers.PlayerManager Outback.Managers.PlayerManager/<CreateNewGuest>c__Iterator2::$this
	PlayerManager_t1841934776 * ___U24this_3;
	// System.Object Outback.Managers.PlayerManager/<CreateNewGuest>c__Iterator2::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Outback.Managers.PlayerManager/<CreateNewGuest>c__Iterator2::$disposing
	bool ___U24disposing_5;
	// System.Int32 Outback.Managers.PlayerManager/<CreateNewGuest>c__Iterator2::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CurlU3E__1_0() { return static_cast<int32_t>(offsetof(U3CCreateNewGuestU3Ec__Iterator2_t4264403073, ___U3CurlU3E__1_0)); }
	inline String_t* get_U3CurlU3E__1_0() const { return ___U3CurlU3E__1_0; }
	inline String_t** get_address_of_U3CurlU3E__1_0() { return &___U3CurlU3E__1_0; }
	inline void set_U3CurlU3E__1_0(String_t* value)
	{
		___U3CurlU3E__1_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__1_0), value);
	}

	inline static int32_t get_offset_of_U3CformU3E__1_1() { return static_cast<int32_t>(offsetof(U3CCreateNewGuestU3Ec__Iterator2_t4264403073, ___U3CformU3E__1_1)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__1_1() const { return ___U3CformU3E__1_1; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__1_1() { return &___U3CformU3E__1_1; }
	inline void set_U3CformU3E__1_1(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U3CdownloadU3E__1_2() { return static_cast<int32_t>(offsetof(U3CCreateNewGuestU3Ec__Iterator2_t4264403073, ___U3CdownloadU3E__1_2)); }
	inline WWW_t3688466362 * get_U3CdownloadU3E__1_2() const { return ___U3CdownloadU3E__1_2; }
	inline WWW_t3688466362 ** get_address_of_U3CdownloadU3E__1_2() { return &___U3CdownloadU3E__1_2; }
	inline void set_U3CdownloadU3E__1_2(WWW_t3688466362 * value)
	{
		___U3CdownloadU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdownloadU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CCreateNewGuestU3Ec__Iterator2_t4264403073, ___U24this_3)); }
	inline PlayerManager_t1841934776 * get_U24this_3() const { return ___U24this_3; }
	inline PlayerManager_t1841934776 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(PlayerManager_t1841934776 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCreateNewGuestU3Ec__Iterator2_t4264403073, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CCreateNewGuestU3Ec__Iterator2_t4264403073, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CCreateNewGuestU3Ec__Iterator2_t4264403073, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATENEWGUESTU3EC__ITERATOR2_T4264403073_H
#ifndef U3CSTARTOFFU3EC__ITERATOR0_T4243828766_H
#define U3CSTARTOFFU3EC__ITERATOR0_T4243828766_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Gameplay.SlotGameFarm/<StartOff>c__Iterator0
struct  U3CStartOffU3Ec__Iterator0_t4243828766  : public RuntimeObject
{
public:
	// System.String Outback.Gameplay.SlotGameFarm/<StartOff>c__Iterator0::<arr1>__0
	String_t* ___U3Carr1U3E__0_0;
	// System.Int32[0...,0...] Outback.Gameplay.SlotGameFarm/<StartOff>c__Iterator0::<linesRules>__0
	Int32U5B0___U2C0___U5D_t385246373* ___U3ClinesRulesU3E__0_1;
	// System.Int64 Outback.Gameplay.SlotGameFarm/<StartOff>c__Iterator0::<totalAmountWon>__0
	int64_t ___U3CtotalAmountWonU3E__0_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> Outback.Gameplay.SlotGameFarm/<StartOff>c__Iterator0::<allMatchedImages>__0
	List_1_t4142344393 * ___U3CallMatchedImagesU3E__0_3;
	// System.Int32 Outback.Gameplay.SlotGameFarm/<StartOff>c__Iterator0::<numOfLines>__0
	int32_t ___U3CnumOfLinesU3E__0_4;
	// Outback.Gameplay.SlotGameFarm Outback.Gameplay.SlotGameFarm/<StartOff>c__Iterator0::$this
	SlotGameFarm_t876359397 * ___U24this_5;
	// System.Object Outback.Gameplay.SlotGameFarm/<StartOff>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean Outback.Gameplay.SlotGameFarm/<StartOff>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 Outback.Gameplay.SlotGameFarm/<StartOff>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3Carr1U3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t4243828766, ___U3Carr1U3E__0_0)); }
	inline String_t* get_U3Carr1U3E__0_0() const { return ___U3Carr1U3E__0_0; }
	inline String_t** get_address_of_U3Carr1U3E__0_0() { return &___U3Carr1U3E__0_0; }
	inline void set_U3Carr1U3E__0_0(String_t* value)
	{
		___U3Carr1U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Carr1U3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3ClinesRulesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t4243828766, ___U3ClinesRulesU3E__0_1)); }
	inline Int32U5B0___U2C0___U5D_t385246373* get_U3ClinesRulesU3E__0_1() const { return ___U3ClinesRulesU3E__0_1; }
	inline Int32U5B0___U2C0___U5D_t385246373** get_address_of_U3ClinesRulesU3E__0_1() { return &___U3ClinesRulesU3E__0_1; }
	inline void set_U3ClinesRulesU3E__0_1(Int32U5B0___U2C0___U5D_t385246373* value)
	{
		___U3ClinesRulesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClinesRulesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtotalAmountWonU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t4243828766, ___U3CtotalAmountWonU3E__0_2)); }
	inline int64_t get_U3CtotalAmountWonU3E__0_2() const { return ___U3CtotalAmountWonU3E__0_2; }
	inline int64_t* get_address_of_U3CtotalAmountWonU3E__0_2() { return &___U3CtotalAmountWonU3E__0_2; }
	inline void set_U3CtotalAmountWonU3E__0_2(int64_t value)
	{
		___U3CtotalAmountWonU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CallMatchedImagesU3E__0_3() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t4243828766, ___U3CallMatchedImagesU3E__0_3)); }
	inline List_1_t4142344393 * get_U3CallMatchedImagesU3E__0_3() const { return ___U3CallMatchedImagesU3E__0_3; }
	inline List_1_t4142344393 ** get_address_of_U3CallMatchedImagesU3E__0_3() { return &___U3CallMatchedImagesU3E__0_3; }
	inline void set_U3CallMatchedImagesU3E__0_3(List_1_t4142344393 * value)
	{
		___U3CallMatchedImagesU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CallMatchedImagesU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CnumOfLinesU3E__0_4() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t4243828766, ___U3CnumOfLinesU3E__0_4)); }
	inline int32_t get_U3CnumOfLinesU3E__0_4() const { return ___U3CnumOfLinesU3E__0_4; }
	inline int32_t* get_address_of_U3CnumOfLinesU3E__0_4() { return &___U3CnumOfLinesU3E__0_4; }
	inline void set_U3CnumOfLinesU3E__0_4(int32_t value)
	{
		___U3CnumOfLinesU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t4243828766, ___U24this_5)); }
	inline SlotGameFarm_t876359397 * get_U24this_5() const { return ___U24this_5; }
	inline SlotGameFarm_t876359397 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(SlotGameFarm_t876359397 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t4243828766, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t4243828766, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t4243828766, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

struct U3CStartOffU3Ec__Iterator0_t4243828766_StaticFields
{
public:
	// System.Predicate`1<UnityEngine.UI.Image> Outback.Gameplay.SlotGameFarm/<StartOff>c__Iterator0::<>f__am$cache0
	Predicate_1_t3495563775 * ___U3CU3Ef__amU24cache0_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t4243828766_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Predicate_1_t3495563775 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Predicate_1_t3495563775 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Predicate_1_t3495563775 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTOFFU3EC__ITERATOR0_T4243828766_H
#ifndef U3CSPINSLOTSU3EC__ANONSTOREY2_T3788656927_H
#define U3CSPINSLOTSU3EC__ANONSTOREY2_T3788656927_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Managers.SlotGameManager/<SpinSlots>c__AnonStorey2
struct  U3CSpinSlotsU3Ec__AnonStorey2_t3788656927  : public RuntimeObject
{
public:
	// System.Int32 Outback.Managers.SlotGameManager/<SpinSlots>c__AnonStorey2::a
	int32_t ___a_0;
	// Outback.Managers.SlotGameManager Outback.Managers.SlotGameManager/<SpinSlots>c__AnonStorey2::$this
	SlotGameManager_t584614006 * ___U24this_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(U3CSpinSlotsU3Ec__AnonStorey2_t3788656927, ___a_0)); }
	inline int32_t get_a_0() const { return ___a_0; }
	inline int32_t* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(int32_t value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CSpinSlotsU3Ec__AnonStorey2_t3788656927, ___U24this_1)); }
	inline SlotGameManager_t584614006 * get_U24this_1() const { return ___U24this_1; }
	inline SlotGameManager_t584614006 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(SlotGameManager_t584614006 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPINSLOTSU3EC__ANONSTOREY2_T3788656927_H
#ifndef U3CSTARTOFFU3EC__ITERATOR0_T1891839980_H
#define U3CSTARTOFFU3EC__ITERATOR0_T1891839980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Gameplay.SlotGameIreland/<StartOff>c__Iterator0
struct  U3CStartOffU3Ec__Iterator0_t1891839980  : public RuntimeObject
{
public:
	// System.String Outback.Gameplay.SlotGameIreland/<StartOff>c__Iterator0::<arr1>__0
	String_t* ___U3Carr1U3E__0_0;
	// System.Int32[0...,0...] Outback.Gameplay.SlotGameIreland/<StartOff>c__Iterator0::<linesRules>__0
	Int32U5B0___U2C0___U5D_t385246373* ___U3ClinesRulesU3E__0_1;
	// System.Int64 Outback.Gameplay.SlotGameIreland/<StartOff>c__Iterator0::<totalAmountWon>__0
	int64_t ___U3CtotalAmountWonU3E__0_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> Outback.Gameplay.SlotGameIreland/<StartOff>c__Iterator0::<allMatchedImages>__0
	List_1_t4142344393 * ___U3CallMatchedImagesU3E__0_3;
	// System.Int32 Outback.Gameplay.SlotGameIreland/<StartOff>c__Iterator0::<numOfLines>__0
	int32_t ___U3CnumOfLinesU3E__0_4;
	// Outback.Gameplay.SlotGameIreland Outback.Gameplay.SlotGameIreland/<StartOff>c__Iterator0::$this
	SlotGameIreland_t2086646524 * ___U24this_5;
	// System.Object Outback.Gameplay.SlotGameIreland/<StartOff>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean Outback.Gameplay.SlotGameIreland/<StartOff>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 Outback.Gameplay.SlotGameIreland/<StartOff>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3Carr1U3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1891839980, ___U3Carr1U3E__0_0)); }
	inline String_t* get_U3Carr1U3E__0_0() const { return ___U3Carr1U3E__0_0; }
	inline String_t** get_address_of_U3Carr1U3E__0_0() { return &___U3Carr1U3E__0_0; }
	inline void set_U3Carr1U3E__0_0(String_t* value)
	{
		___U3Carr1U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Carr1U3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3ClinesRulesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1891839980, ___U3ClinesRulesU3E__0_1)); }
	inline Int32U5B0___U2C0___U5D_t385246373* get_U3ClinesRulesU3E__0_1() const { return ___U3ClinesRulesU3E__0_1; }
	inline Int32U5B0___U2C0___U5D_t385246373** get_address_of_U3ClinesRulesU3E__0_1() { return &___U3ClinesRulesU3E__0_1; }
	inline void set_U3ClinesRulesU3E__0_1(Int32U5B0___U2C0___U5D_t385246373* value)
	{
		___U3ClinesRulesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClinesRulesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtotalAmountWonU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1891839980, ___U3CtotalAmountWonU3E__0_2)); }
	inline int64_t get_U3CtotalAmountWonU3E__0_2() const { return ___U3CtotalAmountWonU3E__0_2; }
	inline int64_t* get_address_of_U3CtotalAmountWonU3E__0_2() { return &___U3CtotalAmountWonU3E__0_2; }
	inline void set_U3CtotalAmountWonU3E__0_2(int64_t value)
	{
		___U3CtotalAmountWonU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CallMatchedImagesU3E__0_3() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1891839980, ___U3CallMatchedImagesU3E__0_3)); }
	inline List_1_t4142344393 * get_U3CallMatchedImagesU3E__0_3() const { return ___U3CallMatchedImagesU3E__0_3; }
	inline List_1_t4142344393 ** get_address_of_U3CallMatchedImagesU3E__0_3() { return &___U3CallMatchedImagesU3E__0_3; }
	inline void set_U3CallMatchedImagesU3E__0_3(List_1_t4142344393 * value)
	{
		___U3CallMatchedImagesU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CallMatchedImagesU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CnumOfLinesU3E__0_4() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1891839980, ___U3CnumOfLinesU3E__0_4)); }
	inline int32_t get_U3CnumOfLinesU3E__0_4() const { return ___U3CnumOfLinesU3E__0_4; }
	inline int32_t* get_address_of_U3CnumOfLinesU3E__0_4() { return &___U3CnumOfLinesU3E__0_4; }
	inline void set_U3CnumOfLinesU3E__0_4(int32_t value)
	{
		___U3CnumOfLinesU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1891839980, ___U24this_5)); }
	inline SlotGameIreland_t2086646524 * get_U24this_5() const { return ___U24this_5; }
	inline SlotGameIreland_t2086646524 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(SlotGameIreland_t2086646524 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1891839980, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1891839980, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1891839980, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

struct U3CStartOffU3Ec__Iterator0_t1891839980_StaticFields
{
public:
	// System.Converter`2<UnityEngine.UI.Image,System.String> Outback.Gameplay.SlotGameIreland/<StartOff>c__Iterator0::<>f__am$cache0
	Converter_2_t804895609 * ___U3CU3Ef__amU24cache0_9;
	// System.Predicate`1<UnityEngine.UI.Image> Outback.Gameplay.SlotGameIreland/<StartOff>c__Iterator0::<>f__am$cache1
	Predicate_1_t3495563775 * ___U3CU3Ef__amU24cache1_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1891839980_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Converter_2_t804895609 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Converter_2_t804895609 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Converter_2_t804895609 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_10() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t1891839980_StaticFields, ___U3CU3Ef__amU24cache1_10)); }
	inline Predicate_1_t3495563775 * get_U3CU3Ef__amU24cache1_10() const { return ___U3CU3Ef__amU24cache1_10; }
	inline Predicate_1_t3495563775 ** get_address_of_U3CU3Ef__amU24cache1_10() { return &___U3CU3Ef__amU24cache1_10; }
	inline void set_U3CU3Ef__amU24cache1_10(Predicate_1_t3495563775 * value)
	{
		___U3CU3Ef__amU24cache1_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTOFFU3EC__ITERATOR0_T1891839980_H
#ifndef U3CLOADINGNEWSCREENU3EC__ITERATOR0_T3188382973_H
#define U3CLOADINGNEWSCREENU3EC__ITERATOR0_T3188382973_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Managers.ScreenManager/<LoadingNewScreen>c__Iterator0
struct  U3CLoadingNewScreenU3Ec__Iterator0_t3188382973  : public RuntimeObject
{
public:
	// System.String Outback.Managers.ScreenManager/<LoadingNewScreen>c__Iterator0::sceneName
	String_t* ___sceneName_0;
	// UnityEngine.AsyncOperation Outback.Managers.ScreenManager/<LoadingNewScreen>c__Iterator0::<asyncc>__0
	AsyncOperation_t1445031843 * ___U3CasynccU3E__0_1;
	// Outback.Managers.ScreenManager Outback.Managers.ScreenManager/<LoadingNewScreen>c__Iterator0::$this
	ScreenManager_t3517964220 * ___U24this_2;
	// System.Object Outback.Managers.ScreenManager/<LoadingNewScreen>c__Iterator0::$current
	RuntimeObject * ___U24current_3;
	// System.Boolean Outback.Managers.ScreenManager/<LoadingNewScreen>c__Iterator0::$disposing
	bool ___U24disposing_4;
	// System.Int32 Outback.Managers.ScreenManager/<LoadingNewScreen>c__Iterator0::$PC
	int32_t ___U24PC_5;

public:
	inline static int32_t get_offset_of_sceneName_0() { return static_cast<int32_t>(offsetof(U3CLoadingNewScreenU3Ec__Iterator0_t3188382973, ___sceneName_0)); }
	inline String_t* get_sceneName_0() const { return ___sceneName_0; }
	inline String_t** get_address_of_sceneName_0() { return &___sceneName_0; }
	inline void set_sceneName_0(String_t* value)
	{
		___sceneName_0 = value;
		Il2CppCodeGenWriteBarrier((&___sceneName_0), value);
	}

	inline static int32_t get_offset_of_U3CasynccU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadingNewScreenU3Ec__Iterator0_t3188382973, ___U3CasynccU3E__0_1)); }
	inline AsyncOperation_t1445031843 * get_U3CasynccU3E__0_1() const { return ___U3CasynccU3E__0_1; }
	inline AsyncOperation_t1445031843 ** get_address_of_U3CasynccU3E__0_1() { return &___U3CasynccU3E__0_1; }
	inline void set_U3CasynccU3E__0_1(AsyncOperation_t1445031843 * value)
	{
		___U3CasynccU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CasynccU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CLoadingNewScreenU3Ec__Iterator0_t3188382973, ___U24this_2)); }
	inline ScreenManager_t3517964220 * get_U24this_2() const { return ___U24this_2; }
	inline ScreenManager_t3517964220 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(ScreenManager_t3517964220 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}

	inline static int32_t get_offset_of_U24current_3() { return static_cast<int32_t>(offsetof(U3CLoadingNewScreenU3Ec__Iterator0_t3188382973, ___U24current_3)); }
	inline RuntimeObject * get_U24current_3() const { return ___U24current_3; }
	inline RuntimeObject ** get_address_of_U24current_3() { return &___U24current_3; }
	inline void set_U24current_3(RuntimeObject * value)
	{
		___U24current_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_3), value);
	}

	inline static int32_t get_offset_of_U24disposing_4() { return static_cast<int32_t>(offsetof(U3CLoadingNewScreenU3Ec__Iterator0_t3188382973, ___U24disposing_4)); }
	inline bool get_U24disposing_4() const { return ___U24disposing_4; }
	inline bool* get_address_of_U24disposing_4() { return &___U24disposing_4; }
	inline void set_U24disposing_4(bool value)
	{
		___U24disposing_4 = value;
	}

	inline static int32_t get_offset_of_U24PC_5() { return static_cast<int32_t>(offsetof(U3CLoadingNewScreenU3Ec__Iterator0_t3188382973, ___U24PC_5)); }
	inline int32_t get_U24PC_5() const { return ___U24PC_5; }
	inline int32_t* get_address_of_U24PC_5() { return &___U24PC_5; }
	inline void set_U24PC_5(int32_t value)
	{
		___U24PC_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADINGNEWSCREENU3EC__ITERATOR0_T3188382973_H
#ifndef U3CSTARTOFFU3EC__ITERATOR0_T608072064_H
#define U3CSTARTOFFU3EC__ITERATOR0_T608072064_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Gameplay.SlotGameMarineWorld/<StartOff>c__Iterator0
struct  U3CStartOffU3Ec__Iterator0_t608072064  : public RuntimeObject
{
public:
	// System.String Outback.Gameplay.SlotGameMarineWorld/<StartOff>c__Iterator0::<arr1>__0
	String_t* ___U3Carr1U3E__0_0;
	// System.Int32[0...,0...] Outback.Gameplay.SlotGameMarineWorld/<StartOff>c__Iterator0::<linesRules>__0
	Int32U5B0___U2C0___U5D_t385246373* ___U3ClinesRulesU3E__0_1;
	// System.Int64 Outback.Gameplay.SlotGameMarineWorld/<StartOff>c__Iterator0::<totalAmountWon>__0
	int64_t ___U3CtotalAmountWonU3E__0_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> Outback.Gameplay.SlotGameMarineWorld/<StartOff>c__Iterator0::<allMatchedImages>__0
	List_1_t4142344393 * ___U3CallMatchedImagesU3E__0_3;
	// System.Int32 Outback.Gameplay.SlotGameMarineWorld/<StartOff>c__Iterator0::<numOfLines>__0
	int32_t ___U3CnumOfLinesU3E__0_4;
	// Outback.Gameplay.SlotGameMarineWorld Outback.Gameplay.SlotGameMarineWorld/<StartOff>c__Iterator0::$this
	SlotGameMarineWorld_t1002224217 * ___U24this_5;
	// System.Object Outback.Gameplay.SlotGameMarineWorld/<StartOff>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean Outback.Gameplay.SlotGameMarineWorld/<StartOff>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 Outback.Gameplay.SlotGameMarineWorld/<StartOff>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3Carr1U3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t608072064, ___U3Carr1U3E__0_0)); }
	inline String_t* get_U3Carr1U3E__0_0() const { return ___U3Carr1U3E__0_0; }
	inline String_t** get_address_of_U3Carr1U3E__0_0() { return &___U3Carr1U3E__0_0; }
	inline void set_U3Carr1U3E__0_0(String_t* value)
	{
		___U3Carr1U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Carr1U3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3ClinesRulesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t608072064, ___U3ClinesRulesU3E__0_1)); }
	inline Int32U5B0___U2C0___U5D_t385246373* get_U3ClinesRulesU3E__0_1() const { return ___U3ClinesRulesU3E__0_1; }
	inline Int32U5B0___U2C0___U5D_t385246373** get_address_of_U3ClinesRulesU3E__0_1() { return &___U3ClinesRulesU3E__0_1; }
	inline void set_U3ClinesRulesU3E__0_1(Int32U5B0___U2C0___U5D_t385246373* value)
	{
		___U3ClinesRulesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClinesRulesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtotalAmountWonU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t608072064, ___U3CtotalAmountWonU3E__0_2)); }
	inline int64_t get_U3CtotalAmountWonU3E__0_2() const { return ___U3CtotalAmountWonU3E__0_2; }
	inline int64_t* get_address_of_U3CtotalAmountWonU3E__0_2() { return &___U3CtotalAmountWonU3E__0_2; }
	inline void set_U3CtotalAmountWonU3E__0_2(int64_t value)
	{
		___U3CtotalAmountWonU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CallMatchedImagesU3E__0_3() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t608072064, ___U3CallMatchedImagesU3E__0_3)); }
	inline List_1_t4142344393 * get_U3CallMatchedImagesU3E__0_3() const { return ___U3CallMatchedImagesU3E__0_3; }
	inline List_1_t4142344393 ** get_address_of_U3CallMatchedImagesU3E__0_3() { return &___U3CallMatchedImagesU3E__0_3; }
	inline void set_U3CallMatchedImagesU3E__0_3(List_1_t4142344393 * value)
	{
		___U3CallMatchedImagesU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CallMatchedImagesU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CnumOfLinesU3E__0_4() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t608072064, ___U3CnumOfLinesU3E__0_4)); }
	inline int32_t get_U3CnumOfLinesU3E__0_4() const { return ___U3CnumOfLinesU3E__0_4; }
	inline int32_t* get_address_of_U3CnumOfLinesU3E__0_4() { return &___U3CnumOfLinesU3E__0_4; }
	inline void set_U3CnumOfLinesU3E__0_4(int32_t value)
	{
		___U3CnumOfLinesU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t608072064, ___U24this_5)); }
	inline SlotGameMarineWorld_t1002224217 * get_U24this_5() const { return ___U24this_5; }
	inline SlotGameMarineWorld_t1002224217 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(SlotGameMarineWorld_t1002224217 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t608072064, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t608072064, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t608072064, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

struct U3CStartOffU3Ec__Iterator0_t608072064_StaticFields
{
public:
	// System.Predicate`1<UnityEngine.UI.Image> Outback.Gameplay.SlotGameMarineWorld/<StartOff>c__Iterator0::<>f__am$cache0
	Predicate_1_t3495563775 * ___U3CU3Ef__amU24cache0_9;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t608072064_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Predicate_1_t3495563775 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Predicate_1_t3495563775 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Predicate_1_t3495563775 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTOFFU3EC__ITERATOR0_T608072064_H
#ifndef U3CCREATENEWFBPLAYERU3EC__ITERATOR3_T3275366866_H
#define U3CCREATENEWFBPLAYERU3EC__ITERATOR3_T3275366866_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Managers.PlayerManager/<CreateNewFBPlayer>c__Iterator3
struct  U3CCreateNewFBPlayerU3Ec__Iterator3_t3275366866  : public RuntimeObject
{
public:
	// System.String Outback.Managers.PlayerManager/<CreateNewFBPlayer>c__Iterator3::<url>__0
	String_t* ___U3CurlU3E__0_0;
	// UnityEngine.WWWForm Outback.Managers.PlayerManager/<CreateNewFBPlayer>c__Iterator3::<form>__0
	WWWForm_t4064702195 * ___U3CformU3E__0_1;
	// UnityEngine.WWW Outback.Managers.PlayerManager/<CreateNewFBPlayer>c__Iterator3::<download>__0
	WWW_t3688466362 * ___U3CdownloadU3E__0_2;
	// Outback.Managers.PlayerManager Outback.Managers.PlayerManager/<CreateNewFBPlayer>c__Iterator3::$this
	PlayerManager_t1841934776 * ___U24this_3;
	// System.Object Outback.Managers.PlayerManager/<CreateNewFBPlayer>c__Iterator3::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Outback.Managers.PlayerManager/<CreateNewFBPlayer>c__Iterator3::$disposing
	bool ___U24disposing_5;
	// System.Int32 Outback.Managers.PlayerManager/<CreateNewFBPlayer>c__Iterator3::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U3CurlU3E__0_0() { return static_cast<int32_t>(offsetof(U3CCreateNewFBPlayerU3Ec__Iterator3_t3275366866, ___U3CurlU3E__0_0)); }
	inline String_t* get_U3CurlU3E__0_0() const { return ___U3CurlU3E__0_0; }
	inline String_t** get_address_of_U3CurlU3E__0_0() { return &___U3CurlU3E__0_0; }
	inline void set_U3CurlU3E__0_0(String_t* value)
	{
		___U3CurlU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CurlU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CformU3E__0_1() { return static_cast<int32_t>(offsetof(U3CCreateNewFBPlayerU3Ec__Iterator3_t3275366866, ___U3CformU3E__0_1)); }
	inline WWWForm_t4064702195 * get_U3CformU3E__0_1() const { return ___U3CformU3E__0_1; }
	inline WWWForm_t4064702195 ** get_address_of_U3CformU3E__0_1() { return &___U3CformU3E__0_1; }
	inline void set_U3CformU3E__0_1(WWWForm_t4064702195 * value)
	{
		___U3CformU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CformU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CdownloadU3E__0_2() { return static_cast<int32_t>(offsetof(U3CCreateNewFBPlayerU3Ec__Iterator3_t3275366866, ___U3CdownloadU3E__0_2)); }
	inline WWW_t3688466362 * get_U3CdownloadU3E__0_2() const { return ___U3CdownloadU3E__0_2; }
	inline WWW_t3688466362 ** get_address_of_U3CdownloadU3E__0_2() { return &___U3CdownloadU3E__0_2; }
	inline void set_U3CdownloadU3E__0_2(WWW_t3688466362 * value)
	{
		___U3CdownloadU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdownloadU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CCreateNewFBPlayerU3Ec__Iterator3_t3275366866, ___U24this_3)); }
	inline PlayerManager_t1841934776 * get_U24this_3() const { return ___U24this_3; }
	inline PlayerManager_t1841934776 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(PlayerManager_t1841934776 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CCreateNewFBPlayerU3Ec__Iterator3_t3275366866, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CCreateNewFBPlayerU3Ec__Iterator3_t3275366866, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CCreateNewFBPlayerU3Ec__Iterator3_t3275366866, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATENEWFBPLAYERU3EC__ITERATOR3_T3275366866_H
#ifndef U3CSTARTOFFU3EC__ITERATOR0_T4188771159_H
#define U3CSTARTOFFU3EC__ITERATOR0_T4188771159_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Gameplay.SlotGameMovieNight/<StartOff>c__Iterator0
struct  U3CStartOffU3Ec__Iterator0_t4188771159  : public RuntimeObject
{
public:
	// System.String Outback.Gameplay.SlotGameMovieNight/<StartOff>c__Iterator0::<arr1>__0
	String_t* ___U3Carr1U3E__0_0;
	// System.Int32[0...,0...] Outback.Gameplay.SlotGameMovieNight/<StartOff>c__Iterator0::<linesRules>__0
	Int32U5B0___U2C0___U5D_t385246373* ___U3ClinesRulesU3E__0_1;
	// System.Int64 Outback.Gameplay.SlotGameMovieNight/<StartOff>c__Iterator0::<totalAmountWon>__0
	int64_t ___U3CtotalAmountWonU3E__0_2;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> Outback.Gameplay.SlotGameMovieNight/<StartOff>c__Iterator0::<allMatchedImages>__0
	List_1_t4142344393 * ___U3CallMatchedImagesU3E__0_3;
	// System.Int32 Outback.Gameplay.SlotGameMovieNight/<StartOff>c__Iterator0::<numOfLines>__0
	int32_t ___U3CnumOfLinesU3E__0_4;
	// Outback.Gameplay.SlotGameMovieNight Outback.Gameplay.SlotGameMovieNight/<StartOff>c__Iterator0::$this
	SlotGameMovieNight_t2457517516 * ___U24this_5;
	// System.Object Outback.Gameplay.SlotGameMovieNight/<StartOff>c__Iterator0::$current
	RuntimeObject * ___U24current_6;
	// System.Boolean Outback.Gameplay.SlotGameMovieNight/<StartOff>c__Iterator0::$disposing
	bool ___U24disposing_7;
	// System.Int32 Outback.Gameplay.SlotGameMovieNight/<StartOff>c__Iterator0::$PC
	int32_t ___U24PC_8;

public:
	inline static int32_t get_offset_of_U3Carr1U3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t4188771159, ___U3Carr1U3E__0_0)); }
	inline String_t* get_U3Carr1U3E__0_0() const { return ___U3Carr1U3E__0_0; }
	inline String_t** get_address_of_U3Carr1U3E__0_0() { return &___U3Carr1U3E__0_0; }
	inline void set_U3Carr1U3E__0_0(String_t* value)
	{
		___U3Carr1U3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3Carr1U3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3ClinesRulesU3E__0_1() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t4188771159, ___U3ClinesRulesU3E__0_1)); }
	inline Int32U5B0___U2C0___U5D_t385246373* get_U3ClinesRulesU3E__0_1() const { return ___U3ClinesRulesU3E__0_1; }
	inline Int32U5B0___U2C0___U5D_t385246373** get_address_of_U3ClinesRulesU3E__0_1() { return &___U3ClinesRulesU3E__0_1; }
	inline void set_U3ClinesRulesU3E__0_1(Int32U5B0___U2C0___U5D_t385246373* value)
	{
		___U3ClinesRulesU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClinesRulesU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CtotalAmountWonU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t4188771159, ___U3CtotalAmountWonU3E__0_2)); }
	inline int64_t get_U3CtotalAmountWonU3E__0_2() const { return ___U3CtotalAmountWonU3E__0_2; }
	inline int64_t* get_address_of_U3CtotalAmountWonU3E__0_2() { return &___U3CtotalAmountWonU3E__0_2; }
	inline void set_U3CtotalAmountWonU3E__0_2(int64_t value)
	{
		___U3CtotalAmountWonU3E__0_2 = value;
	}

	inline static int32_t get_offset_of_U3CallMatchedImagesU3E__0_3() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t4188771159, ___U3CallMatchedImagesU3E__0_3)); }
	inline List_1_t4142344393 * get_U3CallMatchedImagesU3E__0_3() const { return ___U3CallMatchedImagesU3E__0_3; }
	inline List_1_t4142344393 ** get_address_of_U3CallMatchedImagesU3E__0_3() { return &___U3CallMatchedImagesU3E__0_3; }
	inline void set_U3CallMatchedImagesU3E__0_3(List_1_t4142344393 * value)
	{
		___U3CallMatchedImagesU3E__0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CallMatchedImagesU3E__0_3), value);
	}

	inline static int32_t get_offset_of_U3CnumOfLinesU3E__0_4() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t4188771159, ___U3CnumOfLinesU3E__0_4)); }
	inline int32_t get_U3CnumOfLinesU3E__0_4() const { return ___U3CnumOfLinesU3E__0_4; }
	inline int32_t* get_address_of_U3CnumOfLinesU3E__0_4() { return &___U3CnumOfLinesU3E__0_4; }
	inline void set_U3CnumOfLinesU3E__0_4(int32_t value)
	{
		___U3CnumOfLinesU3E__0_4 = value;
	}

	inline static int32_t get_offset_of_U24this_5() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t4188771159, ___U24this_5)); }
	inline SlotGameMovieNight_t2457517516 * get_U24this_5() const { return ___U24this_5; }
	inline SlotGameMovieNight_t2457517516 ** get_address_of_U24this_5() { return &___U24this_5; }
	inline void set_U24this_5(SlotGameMovieNight_t2457517516 * value)
	{
		___U24this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_5), value);
	}

	inline static int32_t get_offset_of_U24current_6() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t4188771159, ___U24current_6)); }
	inline RuntimeObject * get_U24current_6() const { return ___U24current_6; }
	inline RuntimeObject ** get_address_of_U24current_6() { return &___U24current_6; }
	inline void set_U24current_6(RuntimeObject * value)
	{
		___U24current_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_6), value);
	}

	inline static int32_t get_offset_of_U24disposing_7() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t4188771159, ___U24disposing_7)); }
	inline bool get_U24disposing_7() const { return ___U24disposing_7; }
	inline bool* get_address_of_U24disposing_7() { return &___U24disposing_7; }
	inline void set_U24disposing_7(bool value)
	{
		___U24disposing_7 = value;
	}

	inline static int32_t get_offset_of_U24PC_8() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t4188771159, ___U24PC_8)); }
	inline int32_t get_U24PC_8() const { return ___U24PC_8; }
	inline int32_t* get_address_of_U24PC_8() { return &___U24PC_8; }
	inline void set_U24PC_8(int32_t value)
	{
		___U24PC_8 = value;
	}
};

struct U3CStartOffU3Ec__Iterator0_t4188771159_StaticFields
{
public:
	// System.Converter`2<UnityEngine.UI.Image,System.String> Outback.Gameplay.SlotGameMovieNight/<StartOff>c__Iterator0::<>f__am$cache0
	Converter_2_t804895609 * ___U3CU3Ef__amU24cache0_9;
	// System.Predicate`1<UnityEngine.UI.Image> Outback.Gameplay.SlotGameMovieNight/<StartOff>c__Iterator0::<>f__am$cache1
	Predicate_1_t3495563775 * ___U3CU3Ef__amU24cache1_10;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_9() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t4188771159_StaticFields, ___U3CU3Ef__amU24cache0_9)); }
	inline Converter_2_t804895609 * get_U3CU3Ef__amU24cache0_9() const { return ___U3CU3Ef__amU24cache0_9; }
	inline Converter_2_t804895609 ** get_address_of_U3CU3Ef__amU24cache0_9() { return &___U3CU3Ef__amU24cache0_9; }
	inline void set_U3CU3Ef__amU24cache0_9(Converter_2_t804895609 * value)
	{
		___U3CU3Ef__amU24cache0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_9), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_10() { return static_cast<int32_t>(offsetof(U3CStartOffU3Ec__Iterator0_t4188771159_StaticFields, ___U3CU3Ef__amU24cache1_10)); }
	inline Predicate_1_t3495563775 * get_U3CU3Ef__amU24cache1_10() const { return ___U3CU3Ef__amU24cache1_10; }
	inline Predicate_1_t3495563775 ** get_address_of_U3CU3Ef__amU24cache1_10() { return &___U3CU3Ef__amU24cache1_10; }
	inline void set_U3CU3Ef__amU24cache1_10(Predicate_1_t3495563775 * value)
	{
		___U3CU3Ef__amU24cache1_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTOFFU3EC__ITERATOR0_T4188771159_H
#ifndef FACEBOOKFRIEND_T1860979812_H
#define FACEBOOKFRIEND_T1860979812_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FacebookFriend
struct  FacebookFriend_t1860979812  : public RuntimeObject
{
public:
	// System.String FacebookFriend::name
	String_t* ___name_0;
	// System.String FacebookFriend::id
	String_t* ___id_1;
	// System.String FacebookFriend::imageUrl
	String_t* ___imageUrl_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(FacebookFriend_t1860979812, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_id_1() { return static_cast<int32_t>(offsetof(FacebookFriend_t1860979812, ___id_1)); }
	inline String_t* get_id_1() const { return ___id_1; }
	inline String_t** get_address_of_id_1() { return &___id_1; }
	inline void set_id_1(String_t* value)
	{
		___id_1 = value;
		Il2CppCodeGenWriteBarrier((&___id_1), value);
	}

	inline static int32_t get_offset_of_imageUrl_2() { return static_cast<int32_t>(offsetof(FacebookFriend_t1860979812, ___imageUrl_2)); }
	inline String_t* get_imageUrl_2() const { return ___imageUrl_2; }
	inline String_t** get_address_of_imageUrl_2() { return &___imageUrl_2; }
	inline void set_imageUrl_2(String_t* value)
	{
		___imageUrl_2 = value;
		Il2CppCodeGenWriteBarrier((&___imageUrl_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACEBOOKFRIEND_T1860979812_H
#ifndef U3CLOADASSETSU3EC__ITERATOR1_T1305909268_H
#define U3CLOADASSETSU3EC__ITERATOR1_T1305909268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundleHandler/<LoadAssets>c__Iterator1
struct  U3CLoadAssetsU3Ec__Iterator1_t1305909268  : public RuntimeObject
{
public:
	// System.String AssetBundleHandler/<LoadAssets>c__Iterator1::<names>__0
	String_t* ___U3CnamesU3E__0_0;
	// UnityEngine.Networking.UnityWebRequest AssetBundleHandler/<LoadAssets>c__Iterator1::<www>__0
	UnityWebRequest_t463507806 * ___U3CwwwU3E__0_1;
	// UnityEngine.AssetBundle AssetBundleHandler/<LoadAssets>c__Iterator1::<assetBundle>__1
	AssetBundle_t1153907252 * ___U3CassetBundleU3E__1_2;
	// System.String[] AssetBundleHandler/<LoadAssets>c__Iterator1::<sceneNames>__1
	StringU5BU5D_t1281789340* ___U3CsceneNamesU3E__1_3;
	// System.String[] AssetBundleHandler/<LoadAssets>c__Iterator1::$locvar0
	StringU5BU5D_t1281789340* ___U24locvar0_4;
	// System.Int32 AssetBundleHandler/<LoadAssets>c__Iterator1::$locvar1
	int32_t ___U24locvar1_5;
	// AssetBundleHandler AssetBundleHandler/<LoadAssets>c__Iterator1::$this
	AssetBundleHandler_t3701416431 * ___U24this_6;
	// System.Object AssetBundleHandler/<LoadAssets>c__Iterator1::$current
	RuntimeObject * ___U24current_7;
	// System.Boolean AssetBundleHandler/<LoadAssets>c__Iterator1::$disposing
	bool ___U24disposing_8;
	// System.Int32 AssetBundleHandler/<LoadAssets>c__Iterator1::$PC
	int32_t ___U24PC_9;

public:
	inline static int32_t get_offset_of_U3CnamesU3E__0_0() { return static_cast<int32_t>(offsetof(U3CLoadAssetsU3Ec__Iterator1_t1305909268, ___U3CnamesU3E__0_0)); }
	inline String_t* get_U3CnamesU3E__0_0() const { return ___U3CnamesU3E__0_0; }
	inline String_t** get_address_of_U3CnamesU3E__0_0() { return &___U3CnamesU3E__0_0; }
	inline void set_U3CnamesU3E__0_0(String_t* value)
	{
		___U3CnamesU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CnamesU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E__0_1() { return static_cast<int32_t>(offsetof(U3CLoadAssetsU3Ec__Iterator1_t1305909268, ___U3CwwwU3E__0_1)); }
	inline UnityWebRequest_t463507806 * get_U3CwwwU3E__0_1() const { return ___U3CwwwU3E__0_1; }
	inline UnityWebRequest_t463507806 ** get_address_of_U3CwwwU3E__0_1() { return &___U3CwwwU3E__0_1; }
	inline void set_U3CwwwU3E__0_1(UnityWebRequest_t463507806 * value)
	{
		___U3CwwwU3E__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E__0_1), value);
	}

	inline static int32_t get_offset_of_U3CassetBundleU3E__1_2() { return static_cast<int32_t>(offsetof(U3CLoadAssetsU3Ec__Iterator1_t1305909268, ___U3CassetBundleU3E__1_2)); }
	inline AssetBundle_t1153907252 * get_U3CassetBundleU3E__1_2() const { return ___U3CassetBundleU3E__1_2; }
	inline AssetBundle_t1153907252 ** get_address_of_U3CassetBundleU3E__1_2() { return &___U3CassetBundleU3E__1_2; }
	inline void set_U3CassetBundleU3E__1_2(AssetBundle_t1153907252 * value)
	{
		___U3CassetBundleU3E__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CassetBundleU3E__1_2), value);
	}

	inline static int32_t get_offset_of_U3CsceneNamesU3E__1_3() { return static_cast<int32_t>(offsetof(U3CLoadAssetsU3Ec__Iterator1_t1305909268, ___U3CsceneNamesU3E__1_3)); }
	inline StringU5BU5D_t1281789340* get_U3CsceneNamesU3E__1_3() const { return ___U3CsceneNamesU3E__1_3; }
	inline StringU5BU5D_t1281789340** get_address_of_U3CsceneNamesU3E__1_3() { return &___U3CsceneNamesU3E__1_3; }
	inline void set_U3CsceneNamesU3E__1_3(StringU5BU5D_t1281789340* value)
	{
		___U3CsceneNamesU3E__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsceneNamesU3E__1_3), value);
	}

	inline static int32_t get_offset_of_U24locvar0_4() { return static_cast<int32_t>(offsetof(U3CLoadAssetsU3Ec__Iterator1_t1305909268, ___U24locvar0_4)); }
	inline StringU5BU5D_t1281789340* get_U24locvar0_4() const { return ___U24locvar0_4; }
	inline StringU5BU5D_t1281789340** get_address_of_U24locvar0_4() { return &___U24locvar0_4; }
	inline void set_U24locvar0_4(StringU5BU5D_t1281789340* value)
	{
		___U24locvar0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24locvar0_4), value);
	}

	inline static int32_t get_offset_of_U24locvar1_5() { return static_cast<int32_t>(offsetof(U3CLoadAssetsU3Ec__Iterator1_t1305909268, ___U24locvar1_5)); }
	inline int32_t get_U24locvar1_5() const { return ___U24locvar1_5; }
	inline int32_t* get_address_of_U24locvar1_5() { return &___U24locvar1_5; }
	inline void set_U24locvar1_5(int32_t value)
	{
		___U24locvar1_5 = value;
	}

	inline static int32_t get_offset_of_U24this_6() { return static_cast<int32_t>(offsetof(U3CLoadAssetsU3Ec__Iterator1_t1305909268, ___U24this_6)); }
	inline AssetBundleHandler_t3701416431 * get_U24this_6() const { return ___U24this_6; }
	inline AssetBundleHandler_t3701416431 ** get_address_of_U24this_6() { return &___U24this_6; }
	inline void set_U24this_6(AssetBundleHandler_t3701416431 * value)
	{
		___U24this_6 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_6), value);
	}

	inline static int32_t get_offset_of_U24current_7() { return static_cast<int32_t>(offsetof(U3CLoadAssetsU3Ec__Iterator1_t1305909268, ___U24current_7)); }
	inline RuntimeObject * get_U24current_7() const { return ___U24current_7; }
	inline RuntimeObject ** get_address_of_U24current_7() { return &___U24current_7; }
	inline void set_U24current_7(RuntimeObject * value)
	{
		___U24current_7 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_7), value);
	}

	inline static int32_t get_offset_of_U24disposing_8() { return static_cast<int32_t>(offsetof(U3CLoadAssetsU3Ec__Iterator1_t1305909268, ___U24disposing_8)); }
	inline bool get_U24disposing_8() const { return ___U24disposing_8; }
	inline bool* get_address_of_U24disposing_8() { return &___U24disposing_8; }
	inline void set_U24disposing_8(bool value)
	{
		___U24disposing_8 = value;
	}

	inline static int32_t get_offset_of_U24PC_9() { return static_cast<int32_t>(offsetof(U3CLoadAssetsU3Ec__Iterator1_t1305909268, ___U24PC_9)); }
	inline int32_t get_U24PC_9() const { return ___U24PC_9; }
	inline int32_t* get_address_of_U24PC_9() { return &___U24PC_9; }
	inline void set_U24PC_9(int32_t value)
	{
		___U24PC_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADASSETSU3EC__ITERATOR1_T1305909268_H
#ifndef U3CPROGRESSU3EC__ITERATOR0_T2955259158_H
#define U3CPROGRESSU3EC__ITERATOR0_T2955259158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundleHandler/<progress>c__Iterator0
struct  U3CprogressU3Ec__Iterator0_t2955259158  : public RuntimeObject
{
public:
	// UnityEngine.Networking.UnityWebRequest AssetBundleHandler/<progress>c__Iterator0::req
	UnityWebRequest_t463507806 * ___req_0;
	// AssetBundleHandler AssetBundleHandler/<progress>c__Iterator0::$this
	AssetBundleHandler_t3701416431 * ___U24this_1;
	// System.Object AssetBundleHandler/<progress>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean AssetBundleHandler/<progress>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 AssetBundleHandler/<progress>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_req_0() { return static_cast<int32_t>(offsetof(U3CprogressU3Ec__Iterator0_t2955259158, ___req_0)); }
	inline UnityWebRequest_t463507806 * get_req_0() const { return ___req_0; }
	inline UnityWebRequest_t463507806 ** get_address_of_req_0() { return &___req_0; }
	inline void set_req_0(UnityWebRequest_t463507806 * value)
	{
		___req_0 = value;
		Il2CppCodeGenWriteBarrier((&___req_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CprogressU3Ec__Iterator0_t2955259158, ___U24this_1)); }
	inline AssetBundleHandler_t3701416431 * get_U24this_1() const { return ___U24this_1; }
	inline AssetBundleHandler_t3701416431 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(AssetBundleHandler_t3701416431 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CprogressU3Ec__Iterator0_t2955259158, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CprogressU3Ec__Iterator0_t2955259158, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CprogressU3Ec__Iterator0_t2955259158, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPROGRESSU3EC__ITERATOR0_T2955259158_H
#ifndef U3CSTOPU3EC__ANONSTOREY1_T1647417446_H
#define U3CSTOPU3EC__ANONSTOREY1_T1647417446_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Core.AnimatableComponent/<Stop>c__AnonStorey1
struct  U3CStopU3Ec__AnonStorey1_t1647417446  : public RuntimeObject
{
public:
	// System.String Outback.Core.AnimatableComponent/<Stop>c__AnonStorey1::name
	String_t* ___name_0;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(U3CStopU3Ec__AnonStorey1_t1647417446, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTOPU3EC__ANONSTOREY1_T1647417446_H
#ifndef U3CPLAYU3EC__ANONSTOREY0_T4221027112_H
#define U3CPLAYU3EC__ANONSTOREY0_T4221027112_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Core.AnimatableComponent/<Play>c__AnonStorey0
struct  U3CPlayU3Ec__AnonStorey0_t4221027112  : public RuntimeObject
{
public:
	// System.String Outback.Core.AnimatableComponent/<Play>c__AnonStorey0::parameter
	String_t* ___parameter_0;

public:
	inline static int32_t get_offset_of_parameter_0() { return static_cast<int32_t>(offsetof(U3CPlayU3Ec__AnonStorey0_t4221027112, ___parameter_0)); }
	inline String_t* get_parameter_0() const { return ___parameter_0; }
	inline String_t** get_address_of_parameter_0() { return &___parameter_0; }
	inline void set_parameter_0(String_t* value)
	{
		___parameter_0 = value;
		Il2CppCodeGenWriteBarrier((&___parameter_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CPLAYU3EC__ANONSTOREY0_T4221027112_H
#ifndef TRIGGERMETHOD_T582536534_H
#define TRIGGERMETHOD_T582536534_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerMethod
struct  TriggerMethod_t582536534  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERMETHOD_T582536534_H
#ifndef TRACKABLETRIGGER_T621205209_H
#define TRACKABLETRIGGER_T621205209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TrackableTrigger
struct  TrackableTrigger_t621205209  : public RuntimeObject
{
public:
	// UnityEngine.GameObject UnityEngine.Analytics.TrackableTrigger::m_Target
	GameObject_t1113636619 * ___m_Target_0;
	// System.String UnityEngine.Analytics.TrackableTrigger::m_MethodPath
	String_t* ___m_MethodPath_1;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_Target_0)); }
	inline GameObject_t1113636619 * get_m_Target_0() const { return ___m_Target_0; }
	inline GameObject_t1113636619 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(GameObject_t1113636619 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_MethodPath_1() { return static_cast<int32_t>(offsetof(TrackableTrigger_t621205209, ___m_MethodPath_1)); }
	inline String_t* get_m_MethodPath_1() const { return ___m_MethodPath_1; }
	inline String_t** get_address_of_m_MethodPath_1() { return &___m_MethodPath_1; }
	inline void set_m_MethodPath_1(String_t* value)
	{
		___m_MethodPath_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_MethodPath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLETRIGGER_T621205209_H
#ifndef TRIGGERLISTCONTAINER_T2032715483_H
#define TRIGGERLISTCONTAINER_T2032715483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerListContainer
struct  TriggerListContainer_t2032715483  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Analytics.TriggerRule> UnityEngine.Analytics.TriggerListContainer::m_Rules
	List_1_t3418373063 * ___m_Rules_0;

public:
	inline static int32_t get_offset_of_m_Rules_0() { return static_cast<int32_t>(offsetof(TriggerListContainer_t2032715483, ___m_Rules_0)); }
	inline List_1_t3418373063 * get_m_Rules_0() const { return ___m_Rules_0; }
	inline List_1_t3418373063 ** get_address_of_m_Rules_0() { return &___m_Rules_0; }
	inline void set_m_Rules_0(List_1_t3418373063 * value)
	{
		___m_Rules_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLISTCONTAINER_T2032715483_H
#ifndef VALUETYPE_T3640485471_H
#define VALUETYPE_T3640485471_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t3640485471  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t3640485471_marshaled_com
{
};
#endif // VALUETYPE_T3640485471_H
#ifndef U3CSTARTU3EC__ITERATOR0_T1180820131_H
#define U3CSTARTU3EC__ITERATOR0_T1180820131_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// launchImage/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t1180820131  : public RuntimeObject
{
public:
	// System.String launchImage/<Start>c__Iterator0::<strlog>__0
	String_t* ___U3CstrlogU3E__0_0;
	// launchImage launchImage/<Start>c__Iterator0::$this
	launchImage_t1920275599 * ___U24this_1;
	// System.Object launchImage/<Start>c__Iterator0::$current
	RuntimeObject * ___U24current_2;
	// System.Boolean launchImage/<Start>c__Iterator0::$disposing
	bool ___U24disposing_3;
	// System.Int32 launchImage/<Start>c__Iterator0::$PC
	int32_t ___U24PC_4;

public:
	inline static int32_t get_offset_of_U3CstrlogU3E__0_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1180820131, ___U3CstrlogU3E__0_0)); }
	inline String_t* get_U3CstrlogU3E__0_0() const { return ___U3CstrlogU3E__0_0; }
	inline String_t** get_address_of_U3CstrlogU3E__0_0() { return &___U3CstrlogU3E__0_0; }
	inline void set_U3CstrlogU3E__0_0(String_t* value)
	{
		___U3CstrlogU3E__0_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstrlogU3E__0_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1180820131, ___U24this_1)); }
	inline launchImage_t1920275599 * get_U24this_1() const { return ___U24this_1; }
	inline launchImage_t1920275599 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(launchImage_t1920275599 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}

	inline static int32_t get_offset_of_U24current_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1180820131, ___U24current_2)); }
	inline RuntimeObject * get_U24current_2() const { return ___U24current_2; }
	inline RuntimeObject ** get_address_of_U24current_2() { return &___U24current_2; }
	inline void set_U24current_2(RuntimeObject * value)
	{
		___U24current_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_2), value);
	}

	inline static int32_t get_offset_of_U24disposing_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1180820131, ___U24disposing_3)); }
	inline bool get_U24disposing_3() const { return ___U24disposing_3; }
	inline bool* get_address_of_U24disposing_3() { return &___U24disposing_3; }
	inline void set_U24disposing_3(bool value)
	{
		___U24disposing_3 = value;
	}

	inline static int32_t get_offset_of_U24PC_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1180820131, ___U24PC_4)); }
	inline int32_t get_U24PC_4() const { return ___U24PC_4; }
	inline int32_t* get_address_of_U24PC_4() { return &___U24PC_4; }
	inline void set_U24PC_4(int32_t value)
	{
		___U24PC_4 = value;
	}
};

struct U3CStartU3Ec__Iterator0_t1180820131_StaticFields
{
public:
	// DG.Tweening.Core.DOSetter`1<System.Single> launchImage/<Start>c__Iterator0::<>f__am$cache0
	DOSetter_1_t2447375106 * ___U3CU3Ef__amU24cache0_5;
	// DG.Tweening.TweenCallback launchImage/<Start>c__Iterator0::<>f__am$cache1
	TweenCallback_t3727756325 * ___U3CU3Ef__amU24cache1_6;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_5() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1180820131_StaticFields, ___U3CU3Ef__amU24cache0_5)); }
	inline DOSetter_1_t2447375106 * get_U3CU3Ef__amU24cache0_5() const { return ___U3CU3Ef__amU24cache0_5; }
	inline DOSetter_1_t2447375106 ** get_address_of_U3CU3Ef__amU24cache0_5() { return &___U3CU3Ef__amU24cache0_5; }
	inline void set_U3CU3Ef__amU24cache0_5(DOSetter_1_t2447375106 * value)
	{
		___U3CU3Ef__amU24cache0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_6() { return static_cast<int32_t>(offsetof(U3CStartU3Ec__Iterator0_t1180820131_StaticFields, ___U3CU3Ef__amU24cache1_6)); }
	inline TweenCallback_t3727756325 * get_U3CU3Ef__amU24cache1_6() const { return ___U3CU3Ef__amU24cache1_6; }
	inline TweenCallback_t3727756325 ** get_address_of_U3CU3Ef__amU24cache1_6() { return &___U3CU3Ef__amU24cache1_6; }
	inline void set_U3CU3Ef__amU24cache1_6(TweenCallback_t3727756325 * value)
	{
		___U3CU3Ef__amU24cache1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3EC__ITERATOR0_T1180820131_H
#ifndef U3CSTARTFREESPINU3EC__ANONSTOREY1_T680187965_H
#define U3CSTARTFREESPINU3EC__ANONSTOREY1_T680187965_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseGame/<StartFreeSpin>c__AnonStorey1
struct  U3CStartFreeSpinU3Ec__AnonStorey1_t680187965  : public RuntimeObject
{
public:
	// UnityEngine.Animator BaseGame/<StartFreeSpin>c__AnonStorey1::anim
	Animator_t434523843 * ___anim_0;
	// BaseGame BaseGame/<StartFreeSpin>c__AnonStorey1::$this
	BaseGame_t2336638441 * ___U24this_1;

public:
	inline static int32_t get_offset_of_anim_0() { return static_cast<int32_t>(offsetof(U3CStartFreeSpinU3Ec__AnonStorey1_t680187965, ___anim_0)); }
	inline Animator_t434523843 * get_anim_0() const { return ___anim_0; }
	inline Animator_t434523843 ** get_address_of_anim_0() { return &___anim_0; }
	inline void set_anim_0(Animator_t434523843 * value)
	{
		___anim_0 = value;
		Il2CppCodeGenWriteBarrier((&___anim_0), value);
	}

	inline static int32_t get_offset_of_U24this_1() { return static_cast<int32_t>(offsetof(U3CStartFreeSpinU3Ec__AnonStorey1_t680187965, ___U24this_1)); }
	inline BaseGame_t2336638441 * get_U24this_1() const { return ___U24this_1; }
	inline BaseGame_t2336638441 ** get_address_of_U24this_1() { return &___U24this_1; }
	inline void set_U24this_1(BaseGame_t2336638441 * value)
	{
		___U24this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTFREESPINU3EC__ANONSTOREY1_T680187965_H
#ifndef U3CSTOPTHISREELU3EC__ITERATOR0_T4106801625_H
#define U3CSTOPTHISREELU3EC__ITERATOR0_T4106801625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SlotReel/<StopThisReel>c__Iterator0
struct  U3CStopThisReelU3Ec__Iterator0_t4106801625  : public RuntimeObject
{
public:
	// SlotReel SlotReel/<StopThisReel>c__Iterator0::$this
	SlotReel_t1924038889 * ___U24this_0;
	// System.Object SlotReel/<StopThisReel>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean SlotReel/<StopThisReel>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 SlotReel/<StopThisReel>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStopThisReelU3Ec__Iterator0_t4106801625, ___U24this_0)); }
	inline SlotReel_t1924038889 * get_U24this_0() const { return ___U24this_0; }
	inline SlotReel_t1924038889 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(SlotReel_t1924038889 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStopThisReelU3Ec__Iterator0_t4106801625, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStopThisReelU3Ec__Iterator0_t4106801625, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStopThisReelU3Ec__Iterator0_t4106801625, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTOPTHISREELU3EC__ITERATOR0_T4106801625_H
#ifndef U3CCHECKTIMEU3EC__ITERATOR0_T3434459525_H
#define U3CCHECKTIMEU3EC__ITERATOR0_T3434459525_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DailyReward/<CheckTime>c__Iterator0
struct  U3CCheckTimeU3Ec__Iterator0_t3434459525  : public RuntimeObject
{
public:
	// DailyReward DailyReward/<CheckTime>c__Iterator0::$this
	DailyReward_t2847634760 * ___U24this_0;
	// System.Object DailyReward/<CheckTime>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean DailyReward/<CheckTime>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 DailyReward/<CheckTime>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CCheckTimeU3Ec__Iterator0_t3434459525, ___U24this_0)); }
	inline DailyReward_t2847634760 * get_U24this_0() const { return ___U24this_0; }
	inline DailyReward_t2847634760 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(DailyReward_t2847634760 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CCheckTimeU3Ec__Iterator0_t3434459525, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CCheckTimeU3Ec__Iterator0_t3434459525, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CCheckTimeU3Ec__Iterator0_t3434459525, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCHECKTIMEU3EC__ITERATOR0_T3434459525_H
#ifndef U3CSPINBUTTONCLICKEDU3EC__ANONSTOREY0_T1647695004_H
#define U3CSPINBUTTONCLICKEDU3EC__ANONSTOREY0_T1647695004_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpinWheelController/<SpinButtonClicked>c__AnonStorey0
struct  U3CSpinButtonClickedU3Ec__AnonStorey0_t1647695004  : public RuntimeObject
{
public:
	// System.Single SpinWheelController/<SpinButtonClicked>c__AnonStorey0::stoppingAngle
	float ___stoppingAngle_0;
	// System.Int32 SpinWheelController/<SpinButtonClicked>c__AnonStorey0::ran
	int32_t ___ran_1;
	// SpinWheelController SpinWheelController/<SpinButtonClicked>c__AnonStorey0::$this
	SpinWheelController_t1228200680 * ___U24this_2;

public:
	inline static int32_t get_offset_of_stoppingAngle_0() { return static_cast<int32_t>(offsetof(U3CSpinButtonClickedU3Ec__AnonStorey0_t1647695004, ___stoppingAngle_0)); }
	inline float get_stoppingAngle_0() const { return ___stoppingAngle_0; }
	inline float* get_address_of_stoppingAngle_0() { return &___stoppingAngle_0; }
	inline void set_stoppingAngle_0(float value)
	{
		___stoppingAngle_0 = value;
	}

	inline static int32_t get_offset_of_ran_1() { return static_cast<int32_t>(offsetof(U3CSpinButtonClickedU3Ec__AnonStorey0_t1647695004, ___ran_1)); }
	inline int32_t get_ran_1() const { return ___ran_1; }
	inline int32_t* get_address_of_ran_1() { return &___ran_1; }
	inline void set_ran_1(int32_t value)
	{
		___ran_1 = value;
	}

	inline static int32_t get_offset_of_U24this_2() { return static_cast<int32_t>(offsetof(U3CSpinButtonClickedU3Ec__AnonStorey0_t1647695004, ___U24this_2)); }
	inline SpinWheelController_t1228200680 * get_U24this_2() const { return ___U24this_2; }
	inline SpinWheelController_t1228200680 ** get_address_of_U24this_2() { return &___U24this_2; }
	inline void set_U24this_2(SpinWheelController_t1228200680 * value)
	{
		___U24this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_2), value);
	}
};

struct U3CSpinButtonClickedU3Ec__AnonStorey0_t1647695004_StaticFields
{
public:
	// DG.Tweening.Core.DOSetter`1<System.Single> SpinWheelController/<SpinButtonClicked>c__AnonStorey0::<>f__am$cache0
	DOSetter_1_t2447375106 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(U3CSpinButtonClickedU3Ec__AnonStorey0_t1647695004_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline DOSetter_1_t2447375106 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline DOSetter_1_t2447375106 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(DOSetter_1_t2447375106 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPINBUTTONCLICKEDU3EC__ANONSTOREY0_T1647695004_H
#ifndef U3CSTARTANIMU3EC__ANONSTOREY0_T4682628_H
#define U3CSTARTANIMU3EC__ANONSTOREY0_T4682628_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CoinsShower/<StartAnim>c__AnonStorey0
struct  U3CStartAnimU3Ec__AnonStorey0_t4682628  : public RuntimeObject
{
public:
	// System.Single CoinsShower/<StartAnim>c__AnonStorey0::a
	float ___a_0;
	// CoinsShower/<StartAnim>c__AnonStorey1 CoinsShower/<StartAnim>c__AnonStorey0::<>f__ref$1
	U3CStartAnimU3Ec__AnonStorey1_t4682627 * ___U3CU3Ef__refU241_1;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(U3CStartAnimU3Ec__AnonStorey0_t4682628, ___a_0)); }
	inline float get_a_0() const { return ___a_0; }
	inline float* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(float value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU241_1() { return static_cast<int32_t>(offsetof(U3CStartAnimU3Ec__AnonStorey0_t4682628, ___U3CU3Ef__refU241_1)); }
	inline U3CStartAnimU3Ec__AnonStorey1_t4682627 * get_U3CU3Ef__refU241_1() const { return ___U3CU3Ef__refU241_1; }
	inline U3CStartAnimU3Ec__AnonStorey1_t4682627 ** get_address_of_U3CU3Ef__refU241_1() { return &___U3CU3Ef__refU241_1; }
	inline void set_U3CU3Ef__refU241_1(U3CStartAnimU3Ec__AnonStorey1_t4682627 * value)
	{
		___U3CU3Ef__refU241_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU241_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTANIMU3EC__ANONSTOREY0_T4682628_H
#ifndef U3CGETSPRITEFORGAMEUNLOCKU3EC__ANONSTOREY1_T3737134032_H
#define U3CGETSPRITEFORGAMEUNLOCKU3EC__ANONSTOREY1_T3737134032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Managers.MasterConfigsManager/<GetSpriteForGameUnlock>c__AnonStorey1
struct  U3CGetSpriteForGameUnlockU3Ec__AnonStorey1_t3737134032  : public RuntimeObject
{
public:
	// System.String Outback.Managers.MasterConfigsManager/<GetSpriteForGameUnlock>c__AnonStorey1::spriteName
	String_t* ___spriteName_0;

public:
	inline static int32_t get_offset_of_spriteName_0() { return static_cast<int32_t>(offsetof(U3CGetSpriteForGameUnlockU3Ec__AnonStorey1_t3737134032, ___spriteName_0)); }
	inline String_t* get_spriteName_0() const { return ___spriteName_0; }
	inline String_t** get_address_of_spriteName_0() { return &___spriteName_0; }
	inline void set_spriteName_0(String_t* value)
	{
		___spriteName_0 = value;
		Il2CppCodeGenWriteBarrier((&___spriteName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETSPRITEFORGAMEUNLOCKU3EC__ANONSTOREY1_T3737134032_H
#ifndef U3CSTARTANIMU3EC__ANONSTOREY1_T4682627_H
#define U3CSTARTANIMU3EC__ANONSTOREY1_T4682627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CoinsShower/<StartAnim>c__AnonStorey1
struct  U3CStartAnimU3Ec__AnonStorey1_t4682627  : public RuntimeObject
{
public:
	// UnityEngine.UI.Image CoinsShower/<StartAnim>c__AnonStorey1::coin
	Image_t2670269651 * ___coin_0;

public:
	inline static int32_t get_offset_of_coin_0() { return static_cast<int32_t>(offsetof(U3CStartAnimU3Ec__AnonStorey1_t4682627, ___coin_0)); }
	inline Image_t2670269651 * get_coin_0() const { return ___coin_0; }
	inline Image_t2670269651 ** get_address_of_coin_0() { return &___coin_0; }
	inline void set_coin_0(Image_t2670269651 * value)
	{
		___coin_0 = value;
		Il2CppCodeGenWriteBarrier((&___coin_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTANIMU3EC__ANONSTOREY1_T4682627_H
#ifndef U3CSTARTFREESPINSEQUENCEU3EC__ITERATOR0_T2638780399_H
#define U3CSTARTFREESPINSEQUENCEU3EC__ITERATOR0_T2638780399_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseGame/<StartFreeSpinSequence>c__Iterator0
struct  U3CStartFreeSpinSequenceU3Ec__Iterator0_t2638780399  : public RuntimeObject
{
public:
	// BaseGame BaseGame/<StartFreeSpinSequence>c__Iterator0::$this
	BaseGame_t2336638441 * ___U24this_0;
	// System.Object BaseGame/<StartFreeSpinSequence>c__Iterator0::$current
	RuntimeObject * ___U24current_1;
	// System.Boolean BaseGame/<StartFreeSpinSequence>c__Iterator0::$disposing
	bool ___U24disposing_2;
	// System.Int32 BaseGame/<StartFreeSpinSequence>c__Iterator0::$PC
	int32_t ___U24PC_3;

public:
	inline static int32_t get_offset_of_U24this_0() { return static_cast<int32_t>(offsetof(U3CStartFreeSpinSequenceU3Ec__Iterator0_t2638780399, ___U24this_0)); }
	inline BaseGame_t2336638441 * get_U24this_0() const { return ___U24this_0; }
	inline BaseGame_t2336638441 ** get_address_of_U24this_0() { return &___U24this_0; }
	inline void set_U24this_0(BaseGame_t2336638441 * value)
	{
		___U24this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_0), value);
	}

	inline static int32_t get_offset_of_U24current_1() { return static_cast<int32_t>(offsetof(U3CStartFreeSpinSequenceU3Ec__Iterator0_t2638780399, ___U24current_1)); }
	inline RuntimeObject * get_U24current_1() const { return ___U24current_1; }
	inline RuntimeObject ** get_address_of_U24current_1() { return &___U24current_1; }
	inline void set_U24current_1(RuntimeObject * value)
	{
		___U24current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_1), value);
	}

	inline static int32_t get_offset_of_U24disposing_2() { return static_cast<int32_t>(offsetof(U3CStartFreeSpinSequenceU3Ec__Iterator0_t2638780399, ___U24disposing_2)); }
	inline bool get_U24disposing_2() const { return ___U24disposing_2; }
	inline bool* get_address_of_U24disposing_2() { return &___U24disposing_2; }
	inline void set_U24disposing_2(bool value)
	{
		___U24disposing_2 = value;
	}

	inline static int32_t get_offset_of_U24PC_3() { return static_cast<int32_t>(offsetof(U3CStartFreeSpinSequenceU3Ec__Iterator0_t2638780399, ___U24PC_3)); }
	inline int32_t get_U24PC_3() const { return ___U24PC_3; }
	inline int32_t* get_address_of_U24PC_3() { return &___U24PC_3; }
	inline void set_U24PC_3(int32_t value)
	{
		___U24PC_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTFREESPINSEQUENCEU3EC__ITERATOR0_T2638780399_H
#ifndef U3CHIDEGAMEREWARDSEXCEPTU3EC__ANONSTOREY3_T4084852398_H
#define U3CHIDEGAMEREWARDSEXCEPTU3EC__ANONSTOREY3_T4084852398_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseGame/<HideGameRewardsExcept>c__AnonStorey3
struct  U3CHideGameRewardsExceptU3Ec__AnonStorey3_t4084852398  : public RuntimeObject
{
public:
	// System.Int32 BaseGame/<HideGameRewardsExcept>c__AnonStorey3::rewardNumToshow
	int32_t ___rewardNumToshow_0;

public:
	inline static int32_t get_offset_of_rewardNumToshow_0() { return static_cast<int32_t>(offsetof(U3CHideGameRewardsExceptU3Ec__AnonStorey3_t4084852398, ___rewardNumToshow_0)); }
	inline int32_t get_rewardNumToshow_0() const { return ___rewardNumToshow_0; }
	inline int32_t* get_address_of_rewardNumToshow_0() { return &___rewardNumToshow_0; }
	inline void set_rewardNumToshow_0(int32_t value)
	{
		___rewardNumToshow_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CHIDEGAMEREWARDSEXCEPTU3EC__ANONSTOREY3_T4084852398_H
#ifndef U3CSTARTFREESPINSEQUENCEU3EC__ANONSTOREY2_T2566881606_H
#define U3CSTARTFREESPINSEQUENCEU3EC__ANONSTOREY2_T2566881606_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseGame/<StartFreeSpinSequence>c__Iterator0/<StartFreeSpinSequence>c__AnonStorey2
struct  U3CStartFreeSpinSequenceU3Ec__AnonStorey2_t2566881606  : public RuntimeObject
{
public:
	// System.Int32 BaseGame/<StartFreeSpinSequence>c__Iterator0/<StartFreeSpinSequence>c__AnonStorey2::a
	int32_t ___a_0;
	// System.Int64 BaseGame/<StartFreeSpinSequence>c__Iterator0/<StartFreeSpinSequence>c__AnonStorey2::amntWon
	int64_t ___amntWon_1;
	// System.Int64 BaseGame/<StartFreeSpinSequence>c__Iterator0/<StartFreeSpinSequence>c__AnonStorey2::newAmount
	int64_t ___newAmount_2;
	// BaseGame/<StartFreeSpinSequence>c__Iterator0 BaseGame/<StartFreeSpinSequence>c__Iterator0/<StartFreeSpinSequence>c__AnonStorey2::<>f__ref$0
	U3CStartFreeSpinSequenceU3Ec__Iterator0_t2638780399 * ___U3CU3Ef__refU240_3;

public:
	inline static int32_t get_offset_of_a_0() { return static_cast<int32_t>(offsetof(U3CStartFreeSpinSequenceU3Ec__AnonStorey2_t2566881606, ___a_0)); }
	inline int32_t get_a_0() const { return ___a_0; }
	inline int32_t* get_address_of_a_0() { return &___a_0; }
	inline void set_a_0(int32_t value)
	{
		___a_0 = value;
	}

	inline static int32_t get_offset_of_amntWon_1() { return static_cast<int32_t>(offsetof(U3CStartFreeSpinSequenceU3Ec__AnonStorey2_t2566881606, ___amntWon_1)); }
	inline int64_t get_amntWon_1() const { return ___amntWon_1; }
	inline int64_t* get_address_of_amntWon_1() { return &___amntWon_1; }
	inline void set_amntWon_1(int64_t value)
	{
		___amntWon_1 = value;
	}

	inline static int32_t get_offset_of_newAmount_2() { return static_cast<int32_t>(offsetof(U3CStartFreeSpinSequenceU3Ec__AnonStorey2_t2566881606, ___newAmount_2)); }
	inline int64_t get_newAmount_2() const { return ___newAmount_2; }
	inline int64_t* get_address_of_newAmount_2() { return &___newAmount_2; }
	inline void set_newAmount_2(int64_t value)
	{
		___newAmount_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU240_3() { return static_cast<int32_t>(offsetof(U3CStartFreeSpinSequenceU3Ec__AnonStorey2_t2566881606, ___U3CU3Ef__refU240_3)); }
	inline U3CStartFreeSpinSequenceU3Ec__Iterator0_t2638780399 * get_U3CU3Ef__refU240_3() const { return ___U3CU3Ef__refU240_3; }
	inline U3CStartFreeSpinSequenceU3Ec__Iterator0_t2638780399 ** get_address_of_U3CU3Ef__refU240_3() { return &___U3CU3Ef__refU240_3; }
	inline void set_U3CU3Ef__refU240_3(U3CStartFreeSpinSequenceU3Ec__Iterator0_t2638780399 * value)
	{
		___U3CU3Ef__refU240_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU240_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTFREESPINSEQUENCEU3EC__ANONSTOREY2_T2566881606_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VECTOR3_T3722313464_H
#define VECTOR3_T3722313464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_t3722313464 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_1;
	// System.Single UnityEngine.Vector3::y
	float ___y_2;
	// System.Single UnityEngine.Vector3::z
	float ___z_3;

public:
	inline static int32_t get_offset_of_x_1() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___x_1)); }
	inline float get_x_1() const { return ___x_1; }
	inline float* get_address_of_x_1() { return &___x_1; }
	inline void set_x_1(float value)
	{
		___x_1 = value;
	}

	inline static int32_t get_offset_of_y_2() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___y_2)); }
	inline float get_y_2() const { return ___y_2; }
	inline float* get_address_of_y_2() { return &___y_2; }
	inline void set_y_2(float value)
	{
		___y_2 = value;
	}

	inline static int32_t get_offset_of_z_3() { return static_cast<int32_t>(offsetof(Vector3_t3722313464, ___z_3)); }
	inline float get_z_3() const { return ___z_3; }
	inline float* get_address_of_z_3() { return &___z_3; }
	inline void set_z_3(float value)
	{
		___z_3 = value;
	}
};

struct Vector3_t3722313464_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_t3722313464  ___zeroVector_4;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_t3722313464  ___oneVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_t3722313464  ___upVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_t3722313464  ___downVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_t3722313464  ___leftVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_t3722313464  ___rightVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_t3722313464  ___forwardVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_t3722313464  ___backVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_t3722313464  ___positiveInfinityVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_t3722313464  ___negativeInfinityVector_13;

public:
	inline static int32_t get_offset_of_zeroVector_4() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___zeroVector_4)); }
	inline Vector3_t3722313464  get_zeroVector_4() const { return ___zeroVector_4; }
	inline Vector3_t3722313464 * get_address_of_zeroVector_4() { return &___zeroVector_4; }
	inline void set_zeroVector_4(Vector3_t3722313464  value)
	{
		___zeroVector_4 = value;
	}

	inline static int32_t get_offset_of_oneVector_5() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___oneVector_5)); }
	inline Vector3_t3722313464  get_oneVector_5() const { return ___oneVector_5; }
	inline Vector3_t3722313464 * get_address_of_oneVector_5() { return &___oneVector_5; }
	inline void set_oneVector_5(Vector3_t3722313464  value)
	{
		___oneVector_5 = value;
	}

	inline static int32_t get_offset_of_upVector_6() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___upVector_6)); }
	inline Vector3_t3722313464  get_upVector_6() const { return ___upVector_6; }
	inline Vector3_t3722313464 * get_address_of_upVector_6() { return &___upVector_6; }
	inline void set_upVector_6(Vector3_t3722313464  value)
	{
		___upVector_6 = value;
	}

	inline static int32_t get_offset_of_downVector_7() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___downVector_7)); }
	inline Vector3_t3722313464  get_downVector_7() const { return ___downVector_7; }
	inline Vector3_t3722313464 * get_address_of_downVector_7() { return &___downVector_7; }
	inline void set_downVector_7(Vector3_t3722313464  value)
	{
		___downVector_7 = value;
	}

	inline static int32_t get_offset_of_leftVector_8() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___leftVector_8)); }
	inline Vector3_t3722313464  get_leftVector_8() const { return ___leftVector_8; }
	inline Vector3_t3722313464 * get_address_of_leftVector_8() { return &___leftVector_8; }
	inline void set_leftVector_8(Vector3_t3722313464  value)
	{
		___leftVector_8 = value;
	}

	inline static int32_t get_offset_of_rightVector_9() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___rightVector_9)); }
	inline Vector3_t3722313464  get_rightVector_9() const { return ___rightVector_9; }
	inline Vector3_t3722313464 * get_address_of_rightVector_9() { return &___rightVector_9; }
	inline void set_rightVector_9(Vector3_t3722313464  value)
	{
		___rightVector_9 = value;
	}

	inline static int32_t get_offset_of_forwardVector_10() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___forwardVector_10)); }
	inline Vector3_t3722313464  get_forwardVector_10() const { return ___forwardVector_10; }
	inline Vector3_t3722313464 * get_address_of_forwardVector_10() { return &___forwardVector_10; }
	inline void set_forwardVector_10(Vector3_t3722313464  value)
	{
		___forwardVector_10 = value;
	}

	inline static int32_t get_offset_of_backVector_11() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___backVector_11)); }
	inline Vector3_t3722313464  get_backVector_11() const { return ___backVector_11; }
	inline Vector3_t3722313464 * get_address_of_backVector_11() { return &___backVector_11; }
	inline void set_backVector_11(Vector3_t3722313464  value)
	{
		___backVector_11 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_12() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___positiveInfinityVector_12)); }
	inline Vector3_t3722313464  get_positiveInfinityVector_12() const { return ___positiveInfinityVector_12; }
	inline Vector3_t3722313464 * get_address_of_positiveInfinityVector_12() { return &___positiveInfinityVector_12; }
	inline void set_positiveInfinityVector_12(Vector3_t3722313464  value)
	{
		___positiveInfinityVector_12 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_t3722313464_StaticFields, ___negativeInfinityVector_13)); }
	inline Vector3_t3722313464  get_negativeInfinityVector_13() const { return ___negativeInfinityVector_13; }
	inline Vector3_t3722313464 * get_address_of_negativeInfinityVector_13() { return &___negativeInfinityVector_13; }
	inline void set_negativeInfinityVector_13(Vector3_t3722313464  value)
	{
		___negativeInfinityVector_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_T3722313464_H
#ifndef VECTOR2_T2156229523_H
#define VECTOR2_T2156229523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_t2156229523 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_t2156229523, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_t2156229523_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_t2156229523  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_t2156229523  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_t2156229523  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_t2156229523  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_t2156229523  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_t2156229523  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_t2156229523  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_t2156229523  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___zeroVector_2)); }
	inline Vector2_t2156229523  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_t2156229523 * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_t2156229523  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___oneVector_3)); }
	inline Vector2_t2156229523  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_t2156229523 * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_t2156229523  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___upVector_4)); }
	inline Vector2_t2156229523  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_t2156229523 * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_t2156229523  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___downVector_5)); }
	inline Vector2_t2156229523  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_t2156229523 * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_t2156229523  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___leftVector_6)); }
	inline Vector2_t2156229523  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_t2156229523 * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_t2156229523  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___rightVector_7)); }
	inline Vector2_t2156229523  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_t2156229523 * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_t2156229523  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_t2156229523  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_t2156229523 * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_t2156229523  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_t2156229523_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_t2156229523  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_t2156229523 * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_t2156229523  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_T2156229523_H
#ifndef ENUMERATOR_T3694051785_H
#define ENUMERATOR_T3694051785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<Outback.Core.AnimatableComponent>
struct  Enumerator_t3694051785 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t1804807908 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	AnimatableComponent_t332733166 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t3694051785, ___l_0)); }
	inline List_1_t1804807908 * get_l_0() const { return ___l_0; }
	inline List_1_t1804807908 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t1804807908 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t3694051785, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t3694051785, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t3694051785, ___current_3)); }
	inline AnimatableComponent_t332733166 * get_current_3() const { return ___current_3; }
	inline AnimatableComponent_t332733166 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(AnimatableComponent_t332733166 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T3694051785_H
#ifndef ENUM_T4135868527_H
#define ENUM_T4135868527_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t4135868527  : public ValueType_t3640485471
{
public:

public:
};

struct Enum_t4135868527_StaticFields
{
public:
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t3528271667* ___split_char_0;

public:
	inline static int32_t get_offset_of_split_char_0() { return static_cast<int32_t>(offsetof(Enum_t4135868527_StaticFields, ___split_char_0)); }
	inline CharU5BU5D_t3528271667* get_split_char_0() const { return ___split_char_0; }
	inline CharU5BU5D_t3528271667** get_address_of_split_char_0() { return &___split_char_0; }
	inline void set_split_char_0(CharU5BU5D_t3528271667* value)
	{
		___split_char_0 = value;
		Il2CppCodeGenWriteBarrier((&___split_char_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t4135868527_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t4135868527_marshaled_com
{
};
#endif // ENUM_T4135868527_H
#ifndef TIMESPAN_T881159249_H
#define TIMESPAN_T881159249_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_t881159249 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_8;

public:
	inline static int32_t get_offset_of__ticks_8() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249, ____ticks_8)); }
	inline int64_t get__ticks_8() const { return ____ticks_8; }
	inline int64_t* get_address_of__ticks_8() { return &____ticks_8; }
	inline void set__ticks_8(int64_t value)
	{
		____ticks_8 = value;
	}
};

struct TimeSpan_t881159249_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_t881159249  ___MaxValue_5;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_t881159249  ___MinValue_6;
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_t881159249  ___Zero_7;

public:
	inline static int32_t get_offset_of_MaxValue_5() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MaxValue_5)); }
	inline TimeSpan_t881159249  get_MaxValue_5() const { return ___MaxValue_5; }
	inline TimeSpan_t881159249 * get_address_of_MaxValue_5() { return &___MaxValue_5; }
	inline void set_MaxValue_5(TimeSpan_t881159249  value)
	{
		___MaxValue_5 = value;
	}

	inline static int32_t get_offset_of_MinValue_6() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___MinValue_6)); }
	inline TimeSpan_t881159249  get_MinValue_6() const { return ___MinValue_6; }
	inline TimeSpan_t881159249 * get_address_of_MinValue_6() { return &___MinValue_6; }
	inline void set_MinValue_6(TimeSpan_t881159249  value)
	{
		___MinValue_6 = value;
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(TimeSpan_t881159249_StaticFields, ___Zero_7)); }
	inline TimeSpan_t881159249  get_Zero_7() const { return ___Zero_7; }
	inline TimeSpan_t881159249 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(TimeSpan_t881159249  value)
	{
		___Zero_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_T881159249_H
#ifndef COLOR_T2555686324_H
#define COLOR_T2555686324_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t2555686324 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t2555686324, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T2555686324_H
#ifndef VOID_T1185182177_H
#define VOID_T1185182177_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t1185182177 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T1185182177_H
#ifndef ENUMERATOR_T990390212_H
#define ENUMERATOR_T990390212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<SlotReel>
struct  Enumerator_t990390212 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::l
	List_1_t3396113631 * ___l_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::next
	int32_t ___next_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::ver
	int32_t ___ver_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	SlotReel_t1924038889 * ___current_3;

public:
	inline static int32_t get_offset_of_l_0() { return static_cast<int32_t>(offsetof(Enumerator_t990390212, ___l_0)); }
	inline List_1_t3396113631 * get_l_0() const { return ___l_0; }
	inline List_1_t3396113631 ** get_address_of_l_0() { return &___l_0; }
	inline void set_l_0(List_1_t3396113631 * value)
	{
		___l_0 = value;
		Il2CppCodeGenWriteBarrier((&___l_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(Enumerator_t990390212, ___next_1)); }
	inline int32_t get_next_1() const { return ___next_1; }
	inline int32_t* get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(int32_t value)
	{
		___next_1 = value;
	}

	inline static int32_t get_offset_of_ver_2() { return static_cast<int32_t>(offsetof(Enumerator_t990390212, ___ver_2)); }
	inline int32_t get_ver_2() const { return ___ver_2; }
	inline int32_t* get_address_of_ver_2() { return &___ver_2; }
	inline void set_ver_2(int32_t value)
	{
		___ver_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t990390212, ___current_3)); }
	inline SlotReel_t1924038889 * get_current_3() const { return ___current_3; }
	inline SlotReel_t1924038889 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(SlotReel_t1924038889 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T990390212_H
#ifndef U3CSPINREELSU3EC__ITERATOR0_T73513122_H
#define U3CSPINREELSU3EC__ITERATOR0_T73513122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Managers.SlotGameManager/<SpinReels>c__Iterator0
struct  U3CSpinReelsU3Ec__Iterator0_t73513122  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<SlotReel> Outback.Managers.SlotGameManager/<SpinReels>c__Iterator0::$locvar0
	Enumerator_t990390212  ___U24locvar0_0;
	// SlotReel Outback.Managers.SlotGameManager/<SpinReels>c__Iterator0::<slotReel>__1
	SlotReel_t1924038889 * ___U3CslotReelU3E__1_1;
	// System.Collections.Generic.List`1/Enumerator<Outback.Core.AnimatableComponent> Outback.Managers.SlotGameManager/<SpinReels>c__Iterator0::$locvar1
	Enumerator_t3694051785  ___U24locvar1_2;
	// Outback.Managers.SlotGameManager Outback.Managers.SlotGameManager/<SpinReels>c__Iterator0::$this
	SlotGameManager_t584614006 * ___U24this_3;
	// System.Object Outback.Managers.SlotGameManager/<SpinReels>c__Iterator0::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Outback.Managers.SlotGameManager/<SpinReels>c__Iterator0::$disposing
	bool ___U24disposing_5;
	// System.Int32 Outback.Managers.SlotGameManager/<SpinReels>c__Iterator0::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CSpinReelsU3Ec__Iterator0_t73513122, ___U24locvar0_0)); }
	inline Enumerator_t990390212  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t990390212 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t990390212  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CslotReelU3E__1_1() { return static_cast<int32_t>(offsetof(U3CSpinReelsU3Ec__Iterator0_t73513122, ___U3CslotReelU3E__1_1)); }
	inline SlotReel_t1924038889 * get_U3CslotReelU3E__1_1() const { return ___U3CslotReelU3E__1_1; }
	inline SlotReel_t1924038889 ** get_address_of_U3CslotReelU3E__1_1() { return &___U3CslotReelU3E__1_1; }
	inline void set_U3CslotReelU3E__1_1(SlotReel_t1924038889 * value)
	{
		___U3CslotReelU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CslotReelU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U24locvar1_2() { return static_cast<int32_t>(offsetof(U3CSpinReelsU3Ec__Iterator0_t73513122, ___U24locvar1_2)); }
	inline Enumerator_t3694051785  get_U24locvar1_2() const { return ___U24locvar1_2; }
	inline Enumerator_t3694051785 * get_address_of_U24locvar1_2() { return &___U24locvar1_2; }
	inline void set_U24locvar1_2(Enumerator_t3694051785  value)
	{
		___U24locvar1_2 = value;
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CSpinReelsU3Ec__Iterator0_t73513122, ___U24this_3)); }
	inline SlotGameManager_t584614006 * get_U24this_3() const { return ___U24this_3; }
	inline SlotGameManager_t584614006 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(SlotGameManager_t584614006 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CSpinReelsU3Ec__Iterator0_t73513122, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CSpinReelsU3Ec__Iterator0_t73513122, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CSpinReelsU3Ec__Iterator0_t73513122, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSPINREELSU3EC__ITERATOR0_T73513122_H
#ifndef U3CSTOPREELSU3EC__ITERATOR1_T984865831_H
#define U3CSTOPREELSU3EC__ITERATOR1_T984865831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Managers.SlotGameManager/<StopReels>c__Iterator1
struct  U3CStopReelsU3Ec__Iterator1_t984865831  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1/Enumerator<SlotReel> Outback.Managers.SlotGameManager/<StopReels>c__Iterator1::$locvar0
	Enumerator_t990390212  ___U24locvar0_0;
	// SlotReel Outback.Managers.SlotGameManager/<StopReels>c__Iterator1::<slotReel>__1
	SlotReel_t1924038889 * ___U3CslotReelU3E__1_1;
	// DG.Tweening.Sequence Outback.Managers.SlotGameManager/<StopReels>c__Iterator1::<seq>__0
	Sequence_t2050373119 * ___U3CseqU3E__0_2;
	// Outback.Managers.SlotGameManager Outback.Managers.SlotGameManager/<StopReels>c__Iterator1::$this
	SlotGameManager_t584614006 * ___U24this_3;
	// System.Object Outback.Managers.SlotGameManager/<StopReels>c__Iterator1::$current
	RuntimeObject * ___U24current_4;
	// System.Boolean Outback.Managers.SlotGameManager/<StopReels>c__Iterator1::$disposing
	bool ___U24disposing_5;
	// System.Int32 Outback.Managers.SlotGameManager/<StopReels>c__Iterator1::$PC
	int32_t ___U24PC_6;

public:
	inline static int32_t get_offset_of_U24locvar0_0() { return static_cast<int32_t>(offsetof(U3CStopReelsU3Ec__Iterator1_t984865831, ___U24locvar0_0)); }
	inline Enumerator_t990390212  get_U24locvar0_0() const { return ___U24locvar0_0; }
	inline Enumerator_t990390212 * get_address_of_U24locvar0_0() { return &___U24locvar0_0; }
	inline void set_U24locvar0_0(Enumerator_t990390212  value)
	{
		___U24locvar0_0 = value;
	}

	inline static int32_t get_offset_of_U3CslotReelU3E__1_1() { return static_cast<int32_t>(offsetof(U3CStopReelsU3Ec__Iterator1_t984865831, ___U3CslotReelU3E__1_1)); }
	inline SlotReel_t1924038889 * get_U3CslotReelU3E__1_1() const { return ___U3CslotReelU3E__1_1; }
	inline SlotReel_t1924038889 ** get_address_of_U3CslotReelU3E__1_1() { return &___U3CslotReelU3E__1_1; }
	inline void set_U3CslotReelU3E__1_1(SlotReel_t1924038889 * value)
	{
		___U3CslotReelU3E__1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CslotReelU3E__1_1), value);
	}

	inline static int32_t get_offset_of_U3CseqU3E__0_2() { return static_cast<int32_t>(offsetof(U3CStopReelsU3Ec__Iterator1_t984865831, ___U3CseqU3E__0_2)); }
	inline Sequence_t2050373119 * get_U3CseqU3E__0_2() const { return ___U3CseqU3E__0_2; }
	inline Sequence_t2050373119 ** get_address_of_U3CseqU3E__0_2() { return &___U3CseqU3E__0_2; }
	inline void set_U3CseqU3E__0_2(Sequence_t2050373119 * value)
	{
		___U3CseqU3E__0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CseqU3E__0_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CStopReelsU3Ec__Iterator1_t984865831, ___U24this_3)); }
	inline SlotGameManager_t584614006 * get_U24this_3() const { return ___U24this_3; }
	inline SlotGameManager_t584614006 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(SlotGameManager_t584614006 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}

	inline static int32_t get_offset_of_U24current_4() { return static_cast<int32_t>(offsetof(U3CStopReelsU3Ec__Iterator1_t984865831, ___U24current_4)); }
	inline RuntimeObject * get_U24current_4() const { return ___U24current_4; }
	inline RuntimeObject ** get_address_of_U24current_4() { return &___U24current_4; }
	inline void set_U24current_4(RuntimeObject * value)
	{
		___U24current_4 = value;
		Il2CppCodeGenWriteBarrier((&___U24current_4), value);
	}

	inline static int32_t get_offset_of_U24disposing_5() { return static_cast<int32_t>(offsetof(U3CStopReelsU3Ec__Iterator1_t984865831, ___U24disposing_5)); }
	inline bool get_U24disposing_5() const { return ___U24disposing_5; }
	inline bool* get_address_of_U24disposing_5() { return &___U24disposing_5; }
	inline void set_U24disposing_5(bool value)
	{
		___U24disposing_5 = value;
	}

	inline static int32_t get_offset_of_U24PC_6() { return static_cast<int32_t>(offsetof(U3CStopReelsU3Ec__Iterator1_t984865831, ___U24PC_6)); }
	inline int32_t get_U24PC_6() const { return ___U24PC_6; }
	inline int32_t* get_address_of_U24PC_6() { return &___U24PC_6; }
	inline void set_U24PC_6(int32_t value)
	{
		___U24PC_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTOPREELSU3EC__ITERATOR1_T984865831_H
#ifndef REWARDS_T564064347_H
#define REWARDS_T564064347_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Core.Rewards
struct  Rewards_t564064347 
{
public:
	// System.Int32 Outback.Core.Rewards::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(Rewards_t564064347, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWARDS_T564064347_H
#ifndef DELEGATE_T1188392813_H
#define DELEGATE_T1188392813_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t1188392813  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_5;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_6;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_7;
	// System.DelegateData System.Delegate::data
	DelegateData_t1677132599 * ___data_8;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_method_code_5() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_code_5)); }
	inline intptr_t get_method_code_5() const { return ___method_code_5; }
	inline intptr_t* get_address_of_method_code_5() { return &___method_code_5; }
	inline void set_method_code_5(intptr_t value)
	{
		___method_code_5 = value;
	}

	inline static int32_t get_offset_of_method_info_6() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___method_info_6)); }
	inline MethodInfo_t * get_method_info_6() const { return ___method_info_6; }
	inline MethodInfo_t ** get_address_of_method_info_6() { return &___method_info_6; }
	inline void set_method_info_6(MethodInfo_t * value)
	{
		___method_info_6 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_6), value);
	}

	inline static int32_t get_offset_of_original_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___original_method_info_7)); }
	inline MethodInfo_t * get_original_method_info_7() const { return ___original_method_info_7; }
	inline MethodInfo_t ** get_address_of_original_method_info_7() { return &___original_method_info_7; }
	inline void set_original_method_info_7(MethodInfo_t * value)
	{
		___original_method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_7), value);
	}

	inline static int32_t get_offset_of_data_8() { return static_cast<int32_t>(offsetof(Delegate_t1188392813, ___data_8)); }
	inline DelegateData_t1677132599 * get_data_8() const { return ___data_8; }
	inline DelegateData_t1677132599 ** get_address_of_data_8() { return &___data_8; }
	inline void set_data_8(DelegateData_t1677132599 * value)
	{
		___data_8 = value;
		Il2CppCodeGenWriteBarrier((&___data_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATE_T1188392813_H
#ifndef OBJECT_T631007953_H
#define OBJECT_T631007953_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_t631007953  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_t631007953, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_t631007953_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_t631007953_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_t631007953_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_T631007953_H
#ifndef TRIGGERBOOL_T501031542_H
#define TRIGGERBOOL_T501031542_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerBool
struct  TriggerBool_t501031542 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerBool::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerBool_t501031542, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERBOOL_T501031542_H
#ifndef TRIGGERLIFECYCLEEVENT_T3193146760_H
#define TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerLifecycleEvent
struct  TriggerLifecycleEvent_t3193146760 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerLifecycleEvent::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerLifecycleEvent_t3193146760, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERLIFECYCLEEVENT_T3193146760_H
#ifndef TRIGGERTYPE_T105272677_H
#define TRIGGERTYPE_T105272677_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerType
struct  TriggerType_t105272677 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerType::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerType_t105272677, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERTYPE_T105272677_H
#ifndef TRIGGEROPERATOR_T3611898925_H
#define TRIGGEROPERATOR_T3611898925_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerOperator
struct  TriggerOperator_t3611898925 
{
public:
	// System.Int32 UnityEngine.Analytics.TriggerOperator::value__
	int32_t ___value___1;

public:
	inline static int32_t get_offset_of_value___1() { return static_cast<int32_t>(offsetof(TriggerOperator_t3611898925, ___value___1)); }
	inline int32_t get_value___1() const { return ___value___1; }
	inline int32_t* get_address_of_value___1() { return &___value___1; }
	inline void set_value___1(int32_t value)
	{
		___value___1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGEROPERATOR_T3611898925_H
#ifndef EVENTTRIGGER_T2527451695_H
#define EVENTTRIGGER_T2527451695_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger
struct  EventTrigger_t2527451695  : public RuntimeObject
{
public:
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_IsTriggerExpanded
	bool ___m_IsTriggerExpanded_0;
	// UnityEngine.Analytics.TriggerType UnityEngine.Analytics.EventTrigger::m_Type
	int32_t ___m_Type_1;
	// UnityEngine.Analytics.TriggerLifecycleEvent UnityEngine.Analytics.EventTrigger::m_LifecycleEvent
	int32_t ___m_LifecycleEvent_2;
	// System.Boolean UnityEngine.Analytics.EventTrigger::m_ApplyRules
	bool ___m_ApplyRules_3;
	// UnityEngine.Analytics.TriggerListContainer UnityEngine.Analytics.EventTrigger::m_Rules
	TriggerListContainer_t2032715483 * ___m_Rules_4;
	// UnityEngine.Analytics.TriggerBool UnityEngine.Analytics.EventTrigger::m_TriggerBool
	int32_t ___m_TriggerBool_5;
	// System.Single UnityEngine.Analytics.EventTrigger::m_InitTime
	float ___m_InitTime_6;
	// System.Single UnityEngine.Analytics.EventTrigger::m_RepeatTime
	float ___m_RepeatTime_7;
	// System.Int32 UnityEngine.Analytics.EventTrigger::m_Repetitions
	int32_t ___m_Repetitions_8;
	// System.Int32 UnityEngine.Analytics.EventTrigger::repetitionCount
	int32_t ___repetitionCount_9;
	// UnityEngine.Analytics.EventTrigger/OnTrigger UnityEngine.Analytics.EventTrigger::m_TriggerFunction
	OnTrigger_t4184125570 * ___m_TriggerFunction_10;
	// UnityEngine.Analytics.TriggerMethod UnityEngine.Analytics.EventTrigger::m_Method
	TriggerMethod_t582536534 * ___m_Method_11;

public:
	inline static int32_t get_offset_of_m_IsTriggerExpanded_0() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_IsTriggerExpanded_0)); }
	inline bool get_m_IsTriggerExpanded_0() const { return ___m_IsTriggerExpanded_0; }
	inline bool* get_address_of_m_IsTriggerExpanded_0() { return &___m_IsTriggerExpanded_0; }
	inline void set_m_IsTriggerExpanded_0(bool value)
	{
		___m_IsTriggerExpanded_0 = value;
	}

	inline static int32_t get_offset_of_m_Type_1() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Type_1)); }
	inline int32_t get_m_Type_1() const { return ___m_Type_1; }
	inline int32_t* get_address_of_m_Type_1() { return &___m_Type_1; }
	inline void set_m_Type_1(int32_t value)
	{
		___m_Type_1 = value;
	}

	inline static int32_t get_offset_of_m_LifecycleEvent_2() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_LifecycleEvent_2)); }
	inline int32_t get_m_LifecycleEvent_2() const { return ___m_LifecycleEvent_2; }
	inline int32_t* get_address_of_m_LifecycleEvent_2() { return &___m_LifecycleEvent_2; }
	inline void set_m_LifecycleEvent_2(int32_t value)
	{
		___m_LifecycleEvent_2 = value;
	}

	inline static int32_t get_offset_of_m_ApplyRules_3() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_ApplyRules_3)); }
	inline bool get_m_ApplyRules_3() const { return ___m_ApplyRules_3; }
	inline bool* get_address_of_m_ApplyRules_3() { return &___m_ApplyRules_3; }
	inline void set_m_ApplyRules_3(bool value)
	{
		___m_ApplyRules_3 = value;
	}

	inline static int32_t get_offset_of_m_Rules_4() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Rules_4)); }
	inline TriggerListContainer_t2032715483 * get_m_Rules_4() const { return ___m_Rules_4; }
	inline TriggerListContainer_t2032715483 ** get_address_of_m_Rules_4() { return &___m_Rules_4; }
	inline void set_m_Rules_4(TriggerListContainer_t2032715483 * value)
	{
		___m_Rules_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Rules_4), value);
	}

	inline static int32_t get_offset_of_m_TriggerBool_5() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerBool_5)); }
	inline int32_t get_m_TriggerBool_5() const { return ___m_TriggerBool_5; }
	inline int32_t* get_address_of_m_TriggerBool_5() { return &___m_TriggerBool_5; }
	inline void set_m_TriggerBool_5(int32_t value)
	{
		___m_TriggerBool_5 = value;
	}

	inline static int32_t get_offset_of_m_InitTime_6() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_InitTime_6)); }
	inline float get_m_InitTime_6() const { return ___m_InitTime_6; }
	inline float* get_address_of_m_InitTime_6() { return &___m_InitTime_6; }
	inline void set_m_InitTime_6(float value)
	{
		___m_InitTime_6 = value;
	}

	inline static int32_t get_offset_of_m_RepeatTime_7() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_RepeatTime_7)); }
	inline float get_m_RepeatTime_7() const { return ___m_RepeatTime_7; }
	inline float* get_address_of_m_RepeatTime_7() { return &___m_RepeatTime_7; }
	inline void set_m_RepeatTime_7(float value)
	{
		___m_RepeatTime_7 = value;
	}

	inline static int32_t get_offset_of_m_Repetitions_8() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Repetitions_8)); }
	inline int32_t get_m_Repetitions_8() const { return ___m_Repetitions_8; }
	inline int32_t* get_address_of_m_Repetitions_8() { return &___m_Repetitions_8; }
	inline void set_m_Repetitions_8(int32_t value)
	{
		___m_Repetitions_8 = value;
	}

	inline static int32_t get_offset_of_repetitionCount_9() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___repetitionCount_9)); }
	inline int32_t get_repetitionCount_9() const { return ___repetitionCount_9; }
	inline int32_t* get_address_of_repetitionCount_9() { return &___repetitionCount_9; }
	inline void set_repetitionCount_9(int32_t value)
	{
		___repetitionCount_9 = value;
	}

	inline static int32_t get_offset_of_m_TriggerFunction_10() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_TriggerFunction_10)); }
	inline OnTrigger_t4184125570 * get_m_TriggerFunction_10() const { return ___m_TriggerFunction_10; }
	inline OnTrigger_t4184125570 ** get_address_of_m_TriggerFunction_10() { return &___m_TriggerFunction_10; }
	inline void set_m_TriggerFunction_10(OnTrigger_t4184125570 * value)
	{
		___m_TriggerFunction_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_TriggerFunction_10), value);
	}

	inline static int32_t get_offset_of_m_Method_11() { return static_cast<int32_t>(offsetof(EventTrigger_t2527451695, ___m_Method_11)); }
	inline TriggerMethod_t582536534 * get_m_Method_11() const { return ___m_Method_11; }
	inline TriggerMethod_t582536534 ** get_address_of_m_Method_11() { return &___m_Method_11; }
	inline void set_m_Method_11(TriggerMethod_t582536534 * value)
	{
		___m_Method_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_Method_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTTRIGGER_T2527451695_H
#ifndef TRIGGERRULE_T1946298321_H
#define TRIGGERRULE_T1946298321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.TriggerRule
struct  TriggerRule_t1946298321  : public RuntimeObject
{
public:
	// UnityEngine.Analytics.TrackableField UnityEngine.Analytics.TriggerRule::m_Target
	TrackableField_t1772682203 * ___m_Target_0;
	// UnityEngine.Analytics.TriggerOperator UnityEngine.Analytics.TriggerRule::m_Operator
	int32_t ___m_Operator_1;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value
	ValueProperty_t1868393739 * ___m_Value_2;
	// UnityEngine.Analytics.ValueProperty UnityEngine.Analytics.TriggerRule::m_Value2
	ValueProperty_t1868393739 * ___m_Value2_3;

public:
	inline static int32_t get_offset_of_m_Target_0() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Target_0)); }
	inline TrackableField_t1772682203 * get_m_Target_0() const { return ___m_Target_0; }
	inline TrackableField_t1772682203 ** get_address_of_m_Target_0() { return &___m_Target_0; }
	inline void set_m_Target_0(TrackableField_t1772682203 * value)
	{
		___m_Target_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_Target_0), value);
	}

	inline static int32_t get_offset_of_m_Operator_1() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Operator_1)); }
	inline int32_t get_m_Operator_1() const { return ___m_Operator_1; }
	inline int32_t* get_address_of_m_Operator_1() { return &___m_Operator_1; }
	inline void set_m_Operator_1(int32_t value)
	{
		___m_Operator_1 = value;
	}

	inline static int32_t get_offset_of_m_Value_2() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value_2)); }
	inline ValueProperty_t1868393739 * get_m_Value_2() const { return ___m_Value_2; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value_2() { return &___m_Value_2; }
	inline void set_m_Value_2(ValueProperty_t1868393739 * value)
	{
		___m_Value_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value_2), value);
	}

	inline static int32_t get_offset_of_m_Value2_3() { return static_cast<int32_t>(offsetof(TriggerRule_t1946298321, ___m_Value2_3)); }
	inline ValueProperty_t1868393739 * get_m_Value2_3() const { return ___m_Value2_3; }
	inline ValueProperty_t1868393739 ** get_address_of_m_Value2_3() { return &___m_Value2_3; }
	inline void set_m_Value2_3(ValueProperty_t1868393739 * value)
	{
		___m_Value2_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Value2_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRIGGERRULE_T1946298321_H
#ifndef COMPONENT_T1923634451_H
#define COMPONENT_T1923634451_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t1923634451  : public Object_t631007953
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T1923634451_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t1188392813
{
public:
	// System.MulticastDelegate System.MulticastDelegate::prev
	MulticastDelegate_t * ___prev_9;
	// System.MulticastDelegate System.MulticastDelegate::kpm_next
	MulticastDelegate_t * ___kpm_next_10;

public:
	inline static int32_t get_offset_of_prev_9() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___prev_9)); }
	inline MulticastDelegate_t * get_prev_9() const { return ___prev_9; }
	inline MulticastDelegate_t ** get_address_of_prev_9() { return &___prev_9; }
	inline void set_prev_9(MulticastDelegate_t * value)
	{
		___prev_9 = value;
		Il2CppCodeGenWriteBarrier((&___prev_9), value);
	}

	inline static int32_t get_offset_of_kpm_next_10() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___kpm_next_10)); }
	inline MulticastDelegate_t * get_kpm_next_10() const { return ___kpm_next_10; }
	inline MulticastDelegate_t ** get_address_of_kpm_next_10() { return &___kpm_next_10; }
	inline void set_kpm_next_10(MulticastDelegate_t * value)
	{
		___kpm_next_10 = value;
		Il2CppCodeGenWriteBarrier((&___kpm_next_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTICASTDELEGATE_T_H
#ifndef LOADPICTURECALLBACK_T2056340374_H
#define LOADPICTURECALLBACK_T2056340374_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FBHolder/LoadPictureCallback
struct  LoadPictureCallback_t2056340374  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADPICTURECALLBACK_T2056340374_H
#ifndef ONTRIGGER_T4184125570_H
#define ONTRIGGER_T4184125570_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Analytics.EventTrigger/OnTrigger
struct  OnTrigger_t4184125570  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONTRIGGER_T4184125570_H
#ifndef BEHAVIOUR_T1437897464_H
#define BEHAVIOUR_T1437897464_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_t1437897464  : public Component_t1923634451
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_T1437897464_H
#ifndef LOADPICTURECALLBACK_T1320020716_H
#define LOADPICTURECALLBACK_T1320020716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LobbyController/LoadPictureCallback
struct  LoadPictureCallback_t1320020716  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADPICTURECALLBACK_T1320020716_H
#ifndef MONOBEHAVIOUR_T3962482529_H
#define MONOBEHAVIOUR_T3962482529_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t3962482529  : public Behaviour_t1437897464
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T3962482529_H
#ifndef SETTINGSPANELCONTROLLER_T3191827294_H
#define SETTINGSPANELCONTROLLER_T3191827294_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SettingsPanelController
struct  SettingsPanelController_t3191827294  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button SettingsPanelController::musicbutton
	Button_t4055032469 * ___musicbutton_2;
	// UnityEngine.Sprite SettingsPanelController::soundOffImage
	Sprite_t280657092 * ___soundOffImage_3;
	// UnityEngine.Sprite SettingsPanelController::soundOnImage
	Sprite_t280657092 * ___soundOnImage_4;
	// UnityEngine.Color SettingsPanelController::colorfade
	Color_t2555686324  ___colorfade_5;
	// UnityEngine.UI.Button SettingsPanelController::logoutButton
	Button_t4055032469 * ___logoutButton_6;
	// UnityEngine.UI.Button SettingsPanelController::promocodeButton
	Button_t4055032469 * ___promocodeButton_7;
	// UnityEngine.Vector2 SettingsPanelController::promobuttonInitPos
	Vector2_t2156229523  ___promobuttonInitPos_8;

public:
	inline static int32_t get_offset_of_musicbutton_2() { return static_cast<int32_t>(offsetof(SettingsPanelController_t3191827294, ___musicbutton_2)); }
	inline Button_t4055032469 * get_musicbutton_2() const { return ___musicbutton_2; }
	inline Button_t4055032469 ** get_address_of_musicbutton_2() { return &___musicbutton_2; }
	inline void set_musicbutton_2(Button_t4055032469 * value)
	{
		___musicbutton_2 = value;
		Il2CppCodeGenWriteBarrier((&___musicbutton_2), value);
	}

	inline static int32_t get_offset_of_soundOffImage_3() { return static_cast<int32_t>(offsetof(SettingsPanelController_t3191827294, ___soundOffImage_3)); }
	inline Sprite_t280657092 * get_soundOffImage_3() const { return ___soundOffImage_3; }
	inline Sprite_t280657092 ** get_address_of_soundOffImage_3() { return &___soundOffImage_3; }
	inline void set_soundOffImage_3(Sprite_t280657092 * value)
	{
		___soundOffImage_3 = value;
		Il2CppCodeGenWriteBarrier((&___soundOffImage_3), value);
	}

	inline static int32_t get_offset_of_soundOnImage_4() { return static_cast<int32_t>(offsetof(SettingsPanelController_t3191827294, ___soundOnImage_4)); }
	inline Sprite_t280657092 * get_soundOnImage_4() const { return ___soundOnImage_4; }
	inline Sprite_t280657092 ** get_address_of_soundOnImage_4() { return &___soundOnImage_4; }
	inline void set_soundOnImage_4(Sprite_t280657092 * value)
	{
		___soundOnImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___soundOnImage_4), value);
	}

	inline static int32_t get_offset_of_colorfade_5() { return static_cast<int32_t>(offsetof(SettingsPanelController_t3191827294, ___colorfade_5)); }
	inline Color_t2555686324  get_colorfade_5() const { return ___colorfade_5; }
	inline Color_t2555686324 * get_address_of_colorfade_5() { return &___colorfade_5; }
	inline void set_colorfade_5(Color_t2555686324  value)
	{
		___colorfade_5 = value;
	}

	inline static int32_t get_offset_of_logoutButton_6() { return static_cast<int32_t>(offsetof(SettingsPanelController_t3191827294, ___logoutButton_6)); }
	inline Button_t4055032469 * get_logoutButton_6() const { return ___logoutButton_6; }
	inline Button_t4055032469 ** get_address_of_logoutButton_6() { return &___logoutButton_6; }
	inline void set_logoutButton_6(Button_t4055032469 * value)
	{
		___logoutButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___logoutButton_6), value);
	}

	inline static int32_t get_offset_of_promocodeButton_7() { return static_cast<int32_t>(offsetof(SettingsPanelController_t3191827294, ___promocodeButton_7)); }
	inline Button_t4055032469 * get_promocodeButton_7() const { return ___promocodeButton_7; }
	inline Button_t4055032469 ** get_address_of_promocodeButton_7() { return &___promocodeButton_7; }
	inline void set_promocodeButton_7(Button_t4055032469 * value)
	{
		___promocodeButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___promocodeButton_7), value);
	}

	inline static int32_t get_offset_of_promobuttonInitPos_8() { return static_cast<int32_t>(offsetof(SettingsPanelController_t3191827294, ___promobuttonInitPos_8)); }
	inline Vector2_t2156229523  get_promobuttonInitPos_8() const { return ___promobuttonInitPos_8; }
	inline Vector2_t2156229523 * get_address_of_promobuttonInitPos_8() { return &___promobuttonInitPos_8; }
	inline void set_promobuttonInitPos_8(Vector2_t2156229523  value)
	{
		___promobuttonInitPos_8 = value;
	}
};

struct SettingsPanelController_t3191827294_StaticFields
{
public:
	// SettingsPanelController SettingsPanelController::instance
	SettingsPanelController_t3191827294 * ___instance_9;

public:
	inline static int32_t get_offset_of_instance_9() { return static_cast<int32_t>(offsetof(SettingsPanelController_t3191827294_StaticFields, ___instance_9)); }
	inline SettingsPanelController_t3191827294 * get_instance_9() const { return ___instance_9; }
	inline SettingsPanelController_t3191827294 ** get_address_of_instance_9() { return &___instance_9; }
	inline void set_instance_9(SettingsPanelController_t3191827294 * value)
	{
		___instance_9 = value;
		Il2CppCodeGenWriteBarrier((&___instance_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTINGSPANELCONTROLLER_T3191827294_H
#ifndef SLOTREEL_T1924038889_H
#define SLOTREEL_T1924038889_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SlotReel
struct  SlotReel_t1924038889  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Sprite> SlotReel::slots
	List_1_t1752731834 * ___slots_2;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> SlotReel::freeImages
	List_1_t1752731834 * ___freeImages_3;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> SlotReel::visibleSlots
	List_1_t2585711361 * ___visibleSlots_4;
	// System.Boolean SlotReel::spinning
	bool ___spinning_5;
	// System.Single SlotReel::speed
	float ___speed_6;
	// System.Single SlotReel::startingYposition
	float ___startingYposition_7;
	// System.Int32 SlotReel::reelNumber
	int32_t ___reelNumber_8;
	// System.Single SlotReel::positionC
	float ___positionC_9;
	// UnityEngine.GameObject SlotReel::refGO
	GameObject_t1113636619 * ___refGO_10;
	// System.Int32[0...,0...] SlotReel::testRoll
	Int32U5B0___U2C0___U5D_t385246373* ___testRoll_11;

public:
	inline static int32_t get_offset_of_slots_2() { return static_cast<int32_t>(offsetof(SlotReel_t1924038889, ___slots_2)); }
	inline List_1_t1752731834 * get_slots_2() const { return ___slots_2; }
	inline List_1_t1752731834 ** get_address_of_slots_2() { return &___slots_2; }
	inline void set_slots_2(List_1_t1752731834 * value)
	{
		___slots_2 = value;
		Il2CppCodeGenWriteBarrier((&___slots_2), value);
	}

	inline static int32_t get_offset_of_freeImages_3() { return static_cast<int32_t>(offsetof(SlotReel_t1924038889, ___freeImages_3)); }
	inline List_1_t1752731834 * get_freeImages_3() const { return ___freeImages_3; }
	inline List_1_t1752731834 ** get_address_of_freeImages_3() { return &___freeImages_3; }
	inline void set_freeImages_3(List_1_t1752731834 * value)
	{
		___freeImages_3 = value;
		Il2CppCodeGenWriteBarrier((&___freeImages_3), value);
	}

	inline static int32_t get_offset_of_visibleSlots_4() { return static_cast<int32_t>(offsetof(SlotReel_t1924038889, ___visibleSlots_4)); }
	inline List_1_t2585711361 * get_visibleSlots_4() const { return ___visibleSlots_4; }
	inline List_1_t2585711361 ** get_address_of_visibleSlots_4() { return &___visibleSlots_4; }
	inline void set_visibleSlots_4(List_1_t2585711361 * value)
	{
		___visibleSlots_4 = value;
		Il2CppCodeGenWriteBarrier((&___visibleSlots_4), value);
	}

	inline static int32_t get_offset_of_spinning_5() { return static_cast<int32_t>(offsetof(SlotReel_t1924038889, ___spinning_5)); }
	inline bool get_spinning_5() const { return ___spinning_5; }
	inline bool* get_address_of_spinning_5() { return &___spinning_5; }
	inline void set_spinning_5(bool value)
	{
		___spinning_5 = value;
	}

	inline static int32_t get_offset_of_speed_6() { return static_cast<int32_t>(offsetof(SlotReel_t1924038889, ___speed_6)); }
	inline float get_speed_6() const { return ___speed_6; }
	inline float* get_address_of_speed_6() { return &___speed_6; }
	inline void set_speed_6(float value)
	{
		___speed_6 = value;
	}

	inline static int32_t get_offset_of_startingYposition_7() { return static_cast<int32_t>(offsetof(SlotReel_t1924038889, ___startingYposition_7)); }
	inline float get_startingYposition_7() const { return ___startingYposition_7; }
	inline float* get_address_of_startingYposition_7() { return &___startingYposition_7; }
	inline void set_startingYposition_7(float value)
	{
		___startingYposition_7 = value;
	}

	inline static int32_t get_offset_of_reelNumber_8() { return static_cast<int32_t>(offsetof(SlotReel_t1924038889, ___reelNumber_8)); }
	inline int32_t get_reelNumber_8() const { return ___reelNumber_8; }
	inline int32_t* get_address_of_reelNumber_8() { return &___reelNumber_8; }
	inline void set_reelNumber_8(int32_t value)
	{
		___reelNumber_8 = value;
	}

	inline static int32_t get_offset_of_positionC_9() { return static_cast<int32_t>(offsetof(SlotReel_t1924038889, ___positionC_9)); }
	inline float get_positionC_9() const { return ___positionC_9; }
	inline float* get_address_of_positionC_9() { return &___positionC_9; }
	inline void set_positionC_9(float value)
	{
		___positionC_9 = value;
	}

	inline static int32_t get_offset_of_refGO_10() { return static_cast<int32_t>(offsetof(SlotReel_t1924038889, ___refGO_10)); }
	inline GameObject_t1113636619 * get_refGO_10() const { return ___refGO_10; }
	inline GameObject_t1113636619 ** get_address_of_refGO_10() { return &___refGO_10; }
	inline void set_refGO_10(GameObject_t1113636619 * value)
	{
		___refGO_10 = value;
		Il2CppCodeGenWriteBarrier((&___refGO_10), value);
	}

	inline static int32_t get_offset_of_testRoll_11() { return static_cast<int32_t>(offsetof(SlotReel_t1924038889, ___testRoll_11)); }
	inline Int32U5B0___U2C0___U5D_t385246373* get_testRoll_11() const { return ___testRoll_11; }
	inline Int32U5B0___U2C0___U5D_t385246373** get_address_of_testRoll_11() { return &___testRoll_11; }
	inline void set_testRoll_11(Int32U5B0___U2C0___U5D_t385246373* value)
	{
		___testRoll_11 = value;
		Il2CppCodeGenWriteBarrier((&___testRoll_11), value);
	}
};

struct SlotReel_t1924038889_StaticFields
{
public:
	// System.Predicate`1<UnityEngine.Sprite> SlotReel::<>f__am$cache0
	Predicate_1_t1105951216 * ___U3CU3Ef__amU24cache0_12;
	// System.Predicate`1<UnityEngine.Sprite> SlotReel::<>f__am$cache1
	Predicate_1_t1105951216 * ___U3CU3Ef__amU24cache1_13;
	// System.Predicate`1<UnityEngine.Sprite> SlotReel::<>f__am$cache2
	Predicate_1_t1105951216 * ___U3CU3Ef__amU24cache2_14;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_12() { return static_cast<int32_t>(offsetof(SlotReel_t1924038889_StaticFields, ___U3CU3Ef__amU24cache0_12)); }
	inline Predicate_1_t1105951216 * get_U3CU3Ef__amU24cache0_12() const { return ___U3CU3Ef__amU24cache0_12; }
	inline Predicate_1_t1105951216 ** get_address_of_U3CU3Ef__amU24cache0_12() { return &___U3CU3Ef__amU24cache0_12; }
	inline void set_U3CU3Ef__amU24cache0_12(Predicate_1_t1105951216 * value)
	{
		___U3CU3Ef__amU24cache0_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_12), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_13() { return static_cast<int32_t>(offsetof(SlotReel_t1924038889_StaticFields, ___U3CU3Ef__amU24cache1_13)); }
	inline Predicate_1_t1105951216 * get_U3CU3Ef__amU24cache1_13() const { return ___U3CU3Ef__amU24cache1_13; }
	inline Predicate_1_t1105951216 ** get_address_of_U3CU3Ef__amU24cache1_13() { return &___U3CU3Ef__amU24cache1_13; }
	inline void set_U3CU3Ef__amU24cache1_13(Predicate_1_t1105951216 * value)
	{
		___U3CU3Ef__amU24cache1_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_13), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_14() { return static_cast<int32_t>(offsetof(SlotReel_t1924038889_StaticFields, ___U3CU3Ef__amU24cache2_14)); }
	inline Predicate_1_t1105951216 * get_U3CU3Ef__amU24cache2_14() const { return ___U3CU3Ef__amU24cache2_14; }
	inline Predicate_1_t1105951216 ** get_address_of_U3CU3Ef__amU24cache2_14() { return &___U3CU3Ef__amU24cache2_14; }
	inline void set_U3CU3Ef__amU24cache2_14(Predicate_1_t1105951216 * value)
	{
		___U3CU3Ef__amU24cache2_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTREEL_T1924038889_H
#ifndef FBHOLDER_T1712543493_H
#define FBHOLDER_T1712543493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FBHolder
struct  FBHolder_t1712543493  : public MonoBehaviour_t3962482529
{
public:
	// System.String FBHolder::applink
	String_t* ___applink_3;
	// System.String FBHolder::get_data
	String_t* ___get_data_4;
	// System.String FBHolder::fbname
	String_t* ___fbname_5;
	// UnityEngine.UI.Image FBHolder::displayprofilepic
	Image_t2670269651 * ___displayprofilepic_6;
	// System.String FBHolder::userIDSaved
	String_t* ___userIDSaved_7;
	// System.String FBHolder::userFacebookID
	String_t* ___userFacebookID_8;
	// UnityEngine.UI.Text FBHolder::emailID
	Text_t1901882714 * ___emailID_9;
	// UnityEngine.UI.Text FBHolder::username
	Text_t1901882714 * ___username_10;
	// LitJson.JsonData FBHolder::requiredData
	JsonData_t1524858407 * ___requiredData_11;
	// System.String FBHolder::facebook_id
	String_t* ___facebook_id_12;
	// System.String FBHolder::emailID_user
	String_t* ___emailID_user_13;
	// System.String FBHolder::gameDataFileName
	String_t* ___gameDataFileName_15;
	// System.Collections.Generic.List`1<FacebookFriend> FBHolder::facebookFriends
	List_1_t3333054554 * ___facebookFriends_16;
	// System.Single FBHolder::sec
	float ___sec_18;

public:
	inline static int32_t get_offset_of_applink_3() { return static_cast<int32_t>(offsetof(FBHolder_t1712543493, ___applink_3)); }
	inline String_t* get_applink_3() const { return ___applink_3; }
	inline String_t** get_address_of_applink_3() { return &___applink_3; }
	inline void set_applink_3(String_t* value)
	{
		___applink_3 = value;
		Il2CppCodeGenWriteBarrier((&___applink_3), value);
	}

	inline static int32_t get_offset_of_get_data_4() { return static_cast<int32_t>(offsetof(FBHolder_t1712543493, ___get_data_4)); }
	inline String_t* get_get_data_4() const { return ___get_data_4; }
	inline String_t** get_address_of_get_data_4() { return &___get_data_4; }
	inline void set_get_data_4(String_t* value)
	{
		___get_data_4 = value;
		Il2CppCodeGenWriteBarrier((&___get_data_4), value);
	}

	inline static int32_t get_offset_of_fbname_5() { return static_cast<int32_t>(offsetof(FBHolder_t1712543493, ___fbname_5)); }
	inline String_t* get_fbname_5() const { return ___fbname_5; }
	inline String_t** get_address_of_fbname_5() { return &___fbname_5; }
	inline void set_fbname_5(String_t* value)
	{
		___fbname_5 = value;
		Il2CppCodeGenWriteBarrier((&___fbname_5), value);
	}

	inline static int32_t get_offset_of_displayprofilepic_6() { return static_cast<int32_t>(offsetof(FBHolder_t1712543493, ___displayprofilepic_6)); }
	inline Image_t2670269651 * get_displayprofilepic_6() const { return ___displayprofilepic_6; }
	inline Image_t2670269651 ** get_address_of_displayprofilepic_6() { return &___displayprofilepic_6; }
	inline void set_displayprofilepic_6(Image_t2670269651 * value)
	{
		___displayprofilepic_6 = value;
		Il2CppCodeGenWriteBarrier((&___displayprofilepic_6), value);
	}

	inline static int32_t get_offset_of_userIDSaved_7() { return static_cast<int32_t>(offsetof(FBHolder_t1712543493, ___userIDSaved_7)); }
	inline String_t* get_userIDSaved_7() const { return ___userIDSaved_7; }
	inline String_t** get_address_of_userIDSaved_7() { return &___userIDSaved_7; }
	inline void set_userIDSaved_7(String_t* value)
	{
		___userIDSaved_7 = value;
		Il2CppCodeGenWriteBarrier((&___userIDSaved_7), value);
	}

	inline static int32_t get_offset_of_userFacebookID_8() { return static_cast<int32_t>(offsetof(FBHolder_t1712543493, ___userFacebookID_8)); }
	inline String_t* get_userFacebookID_8() const { return ___userFacebookID_8; }
	inline String_t** get_address_of_userFacebookID_8() { return &___userFacebookID_8; }
	inline void set_userFacebookID_8(String_t* value)
	{
		___userFacebookID_8 = value;
		Il2CppCodeGenWriteBarrier((&___userFacebookID_8), value);
	}

	inline static int32_t get_offset_of_emailID_9() { return static_cast<int32_t>(offsetof(FBHolder_t1712543493, ___emailID_9)); }
	inline Text_t1901882714 * get_emailID_9() const { return ___emailID_9; }
	inline Text_t1901882714 ** get_address_of_emailID_9() { return &___emailID_9; }
	inline void set_emailID_9(Text_t1901882714 * value)
	{
		___emailID_9 = value;
		Il2CppCodeGenWriteBarrier((&___emailID_9), value);
	}

	inline static int32_t get_offset_of_username_10() { return static_cast<int32_t>(offsetof(FBHolder_t1712543493, ___username_10)); }
	inline Text_t1901882714 * get_username_10() const { return ___username_10; }
	inline Text_t1901882714 ** get_address_of_username_10() { return &___username_10; }
	inline void set_username_10(Text_t1901882714 * value)
	{
		___username_10 = value;
		Il2CppCodeGenWriteBarrier((&___username_10), value);
	}

	inline static int32_t get_offset_of_requiredData_11() { return static_cast<int32_t>(offsetof(FBHolder_t1712543493, ___requiredData_11)); }
	inline JsonData_t1524858407 * get_requiredData_11() const { return ___requiredData_11; }
	inline JsonData_t1524858407 ** get_address_of_requiredData_11() { return &___requiredData_11; }
	inline void set_requiredData_11(JsonData_t1524858407 * value)
	{
		___requiredData_11 = value;
		Il2CppCodeGenWriteBarrier((&___requiredData_11), value);
	}

	inline static int32_t get_offset_of_facebook_id_12() { return static_cast<int32_t>(offsetof(FBHolder_t1712543493, ___facebook_id_12)); }
	inline String_t* get_facebook_id_12() const { return ___facebook_id_12; }
	inline String_t** get_address_of_facebook_id_12() { return &___facebook_id_12; }
	inline void set_facebook_id_12(String_t* value)
	{
		___facebook_id_12 = value;
		Il2CppCodeGenWriteBarrier((&___facebook_id_12), value);
	}

	inline static int32_t get_offset_of_emailID_user_13() { return static_cast<int32_t>(offsetof(FBHolder_t1712543493, ___emailID_user_13)); }
	inline String_t* get_emailID_user_13() const { return ___emailID_user_13; }
	inline String_t** get_address_of_emailID_user_13() { return &___emailID_user_13; }
	inline void set_emailID_user_13(String_t* value)
	{
		___emailID_user_13 = value;
		Il2CppCodeGenWriteBarrier((&___emailID_user_13), value);
	}

	inline static int32_t get_offset_of_gameDataFileName_15() { return static_cast<int32_t>(offsetof(FBHolder_t1712543493, ___gameDataFileName_15)); }
	inline String_t* get_gameDataFileName_15() const { return ___gameDataFileName_15; }
	inline String_t** get_address_of_gameDataFileName_15() { return &___gameDataFileName_15; }
	inline void set_gameDataFileName_15(String_t* value)
	{
		___gameDataFileName_15 = value;
		Il2CppCodeGenWriteBarrier((&___gameDataFileName_15), value);
	}

	inline static int32_t get_offset_of_facebookFriends_16() { return static_cast<int32_t>(offsetof(FBHolder_t1712543493, ___facebookFriends_16)); }
	inline List_1_t3333054554 * get_facebookFriends_16() const { return ___facebookFriends_16; }
	inline List_1_t3333054554 ** get_address_of_facebookFriends_16() { return &___facebookFriends_16; }
	inline void set_facebookFriends_16(List_1_t3333054554 * value)
	{
		___facebookFriends_16 = value;
		Il2CppCodeGenWriteBarrier((&___facebookFriends_16), value);
	}

	inline static int32_t get_offset_of_sec_18() { return static_cast<int32_t>(offsetof(FBHolder_t1712543493, ___sec_18)); }
	inline float get_sec_18() const { return ___sec_18; }
	inline float* get_address_of_sec_18() { return &___sec_18; }
	inline void set_sec_18(float value)
	{
		___sec_18 = value;
	}
};

struct FBHolder_t1712543493_StaticFields
{
public:
	// FBHolder FBHolder::instance
	FBHolder_t1712543493 * ___instance_2;
	// System.String FBHolder::BaseURL
	String_t* ___BaseURL_14;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(FBHolder_t1712543493_StaticFields, ___instance_2)); }
	inline FBHolder_t1712543493 * get_instance_2() const { return ___instance_2; }
	inline FBHolder_t1712543493 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(FBHolder_t1712543493 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_BaseURL_14() { return static_cast<int32_t>(offsetof(FBHolder_t1712543493_StaticFields, ___BaseURL_14)); }
	inline String_t* get_BaseURL_14() const { return ___BaseURL_14; }
	inline String_t** get_address_of_BaseURL_14() { return &___BaseURL_14; }
	inline void set_BaseURL_14(String_t* value)
	{
		___BaseURL_14 = value;
		Il2CppCodeGenWriteBarrier((&___BaseURL_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FBHOLDER_T1712543493_H
#ifndef DONTDESTROY_T1446738193_H
#define DONTDESTROY_T1446738193_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DontDestroy
struct  DontDestroy_t1446738193  : public MonoBehaviour_t3962482529
{
public:
	// System.Collections.Generic.List`1<UnityEngine.GameObject> DontDestroy::gos
	List_1_t2585711361 * ___gos_3;

public:
	inline static int32_t get_offset_of_gos_3() { return static_cast<int32_t>(offsetof(DontDestroy_t1446738193, ___gos_3)); }
	inline List_1_t2585711361 * get_gos_3() const { return ___gos_3; }
	inline List_1_t2585711361 ** get_address_of_gos_3() { return &___gos_3; }
	inline void set_gos_3(List_1_t2585711361 * value)
	{
		___gos_3 = value;
		Il2CppCodeGenWriteBarrier((&___gos_3), value);
	}
};

struct DontDestroy_t1446738193_StaticFields
{
public:
	// DontDestroy DontDestroy::instance
	DontDestroy_t1446738193 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(DontDestroy_t1446738193_StaticFields, ___instance_2)); }
	inline DontDestroy_t1446738193 * get_instance_2() const { return ___instance_2; }
	inline DontDestroy_t1446738193 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(DontDestroy_t1446738193 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DONTDESTROY_T1446738193_H
#ifndef SOUNDMANAGER_T2102329059_H
#define SOUNDMANAGER_T2102329059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SoundManager
struct  SoundManager_t2102329059  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AudioSource SoundManager::backgroundMusic
	AudioSource_t3935305588 * ___backgroundMusic_2;
	// System.Collections.Generic.List`1<UnityEngine.AudioSource> SoundManager::gamesBackgroundMusic
	List_1_t1112413034 * ___gamesBackgroundMusic_3;
	// System.Boolean SoundManager::musicFlag
	bool ___musicFlag_4;

public:
	inline static int32_t get_offset_of_backgroundMusic_2() { return static_cast<int32_t>(offsetof(SoundManager_t2102329059, ___backgroundMusic_2)); }
	inline AudioSource_t3935305588 * get_backgroundMusic_2() const { return ___backgroundMusic_2; }
	inline AudioSource_t3935305588 ** get_address_of_backgroundMusic_2() { return &___backgroundMusic_2; }
	inline void set_backgroundMusic_2(AudioSource_t3935305588 * value)
	{
		___backgroundMusic_2 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundMusic_2), value);
	}

	inline static int32_t get_offset_of_gamesBackgroundMusic_3() { return static_cast<int32_t>(offsetof(SoundManager_t2102329059, ___gamesBackgroundMusic_3)); }
	inline List_1_t1112413034 * get_gamesBackgroundMusic_3() const { return ___gamesBackgroundMusic_3; }
	inline List_1_t1112413034 ** get_address_of_gamesBackgroundMusic_3() { return &___gamesBackgroundMusic_3; }
	inline void set_gamesBackgroundMusic_3(List_1_t1112413034 * value)
	{
		___gamesBackgroundMusic_3 = value;
		Il2CppCodeGenWriteBarrier((&___gamesBackgroundMusic_3), value);
	}

	inline static int32_t get_offset_of_musicFlag_4() { return static_cast<int32_t>(offsetof(SoundManager_t2102329059, ___musicFlag_4)); }
	inline bool get_musicFlag_4() const { return ___musicFlag_4; }
	inline bool* get_address_of_musicFlag_4() { return &___musicFlag_4; }
	inline void set_musicFlag_4(bool value)
	{
		___musicFlag_4 = value;
	}
};

struct SoundManager_t2102329059_StaticFields
{
public:
	// SoundManager SoundManager::instance
	SoundManager_t2102329059 * ___instance_5;

public:
	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(SoundManager_t2102329059_StaticFields, ___instance_5)); }
	inline SoundManager_t2102329059 * get_instance_5() const { return ___instance_5; }
	inline SoundManager_t2102329059 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(SoundManager_t2102329059 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___instance_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDMANAGER_T2102329059_H
#ifndef SPINWHEELCONTROLLER_T1228200680_H
#define SPINWHEELCONTROLLER_T1228200680_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpinWheelController
struct  SpinWheelController_t1228200680  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject SpinWheelController::wheel
	GameObject_t1113636619 * ___wheel_3;
	// UnityEngine.GameObject SpinWheelController::spinWheel
	GameObject_t1113636619 * ___spinWheel_4;
	// UnityEngine.GameObject SpinWheelController::pointer
	GameObject_t1113636619 * ___pointer_5;
	// UnityEngine.GameObject SpinWheelController::spinButton
	GameObject_t1113636619 * ___spinButton_6;
	// UnityEngine.GameObject SpinWheelController::collectButton
	GameObject_t1113636619 * ___collectButton_7;
	// TMPro.TextMeshProUGUI SpinWheelController::winningsText
	TextMeshProUGUI_t529313277 * ___winningsText_8;
	// UnityEngine.GameObject SpinWheelController::winningTextBG
	GameObject_t1113636619 * ___winningTextBG_9;
	// UnityEngine.GameObject SpinWheelController::title
	GameObject_t1113636619 * ___title_10;
	// System.Collections.Generic.List`1<System.Int32> SpinWheelController::stoppingValues
	List_1_t128053199 * ___stoppingValues_11;
	// System.Collections.Generic.List`1<System.Int32> SpinWheelController::winnings
	List_1_t128053199 * ___winnings_12;
	// System.Int32 SpinWheelController::segments
	int32_t ___segments_13;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> SpinWheelController::numbers
	List_1_t1752731834 * ___numbers_14;

public:
	inline static int32_t get_offset_of_wheel_3() { return static_cast<int32_t>(offsetof(SpinWheelController_t1228200680, ___wheel_3)); }
	inline GameObject_t1113636619 * get_wheel_3() const { return ___wheel_3; }
	inline GameObject_t1113636619 ** get_address_of_wheel_3() { return &___wheel_3; }
	inline void set_wheel_3(GameObject_t1113636619 * value)
	{
		___wheel_3 = value;
		Il2CppCodeGenWriteBarrier((&___wheel_3), value);
	}

	inline static int32_t get_offset_of_spinWheel_4() { return static_cast<int32_t>(offsetof(SpinWheelController_t1228200680, ___spinWheel_4)); }
	inline GameObject_t1113636619 * get_spinWheel_4() const { return ___spinWheel_4; }
	inline GameObject_t1113636619 ** get_address_of_spinWheel_4() { return &___spinWheel_4; }
	inline void set_spinWheel_4(GameObject_t1113636619 * value)
	{
		___spinWheel_4 = value;
		Il2CppCodeGenWriteBarrier((&___spinWheel_4), value);
	}

	inline static int32_t get_offset_of_pointer_5() { return static_cast<int32_t>(offsetof(SpinWheelController_t1228200680, ___pointer_5)); }
	inline GameObject_t1113636619 * get_pointer_5() const { return ___pointer_5; }
	inline GameObject_t1113636619 ** get_address_of_pointer_5() { return &___pointer_5; }
	inline void set_pointer_5(GameObject_t1113636619 * value)
	{
		___pointer_5 = value;
		Il2CppCodeGenWriteBarrier((&___pointer_5), value);
	}

	inline static int32_t get_offset_of_spinButton_6() { return static_cast<int32_t>(offsetof(SpinWheelController_t1228200680, ___spinButton_6)); }
	inline GameObject_t1113636619 * get_spinButton_6() const { return ___spinButton_6; }
	inline GameObject_t1113636619 ** get_address_of_spinButton_6() { return &___spinButton_6; }
	inline void set_spinButton_6(GameObject_t1113636619 * value)
	{
		___spinButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___spinButton_6), value);
	}

	inline static int32_t get_offset_of_collectButton_7() { return static_cast<int32_t>(offsetof(SpinWheelController_t1228200680, ___collectButton_7)); }
	inline GameObject_t1113636619 * get_collectButton_7() const { return ___collectButton_7; }
	inline GameObject_t1113636619 ** get_address_of_collectButton_7() { return &___collectButton_7; }
	inline void set_collectButton_7(GameObject_t1113636619 * value)
	{
		___collectButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___collectButton_7), value);
	}

	inline static int32_t get_offset_of_winningsText_8() { return static_cast<int32_t>(offsetof(SpinWheelController_t1228200680, ___winningsText_8)); }
	inline TextMeshProUGUI_t529313277 * get_winningsText_8() const { return ___winningsText_8; }
	inline TextMeshProUGUI_t529313277 ** get_address_of_winningsText_8() { return &___winningsText_8; }
	inline void set_winningsText_8(TextMeshProUGUI_t529313277 * value)
	{
		___winningsText_8 = value;
		Il2CppCodeGenWriteBarrier((&___winningsText_8), value);
	}

	inline static int32_t get_offset_of_winningTextBG_9() { return static_cast<int32_t>(offsetof(SpinWheelController_t1228200680, ___winningTextBG_9)); }
	inline GameObject_t1113636619 * get_winningTextBG_9() const { return ___winningTextBG_9; }
	inline GameObject_t1113636619 ** get_address_of_winningTextBG_9() { return &___winningTextBG_9; }
	inline void set_winningTextBG_9(GameObject_t1113636619 * value)
	{
		___winningTextBG_9 = value;
		Il2CppCodeGenWriteBarrier((&___winningTextBG_9), value);
	}

	inline static int32_t get_offset_of_title_10() { return static_cast<int32_t>(offsetof(SpinWheelController_t1228200680, ___title_10)); }
	inline GameObject_t1113636619 * get_title_10() const { return ___title_10; }
	inline GameObject_t1113636619 ** get_address_of_title_10() { return &___title_10; }
	inline void set_title_10(GameObject_t1113636619 * value)
	{
		___title_10 = value;
		Il2CppCodeGenWriteBarrier((&___title_10), value);
	}

	inline static int32_t get_offset_of_stoppingValues_11() { return static_cast<int32_t>(offsetof(SpinWheelController_t1228200680, ___stoppingValues_11)); }
	inline List_1_t128053199 * get_stoppingValues_11() const { return ___stoppingValues_11; }
	inline List_1_t128053199 ** get_address_of_stoppingValues_11() { return &___stoppingValues_11; }
	inline void set_stoppingValues_11(List_1_t128053199 * value)
	{
		___stoppingValues_11 = value;
		Il2CppCodeGenWriteBarrier((&___stoppingValues_11), value);
	}

	inline static int32_t get_offset_of_winnings_12() { return static_cast<int32_t>(offsetof(SpinWheelController_t1228200680, ___winnings_12)); }
	inline List_1_t128053199 * get_winnings_12() const { return ___winnings_12; }
	inline List_1_t128053199 ** get_address_of_winnings_12() { return &___winnings_12; }
	inline void set_winnings_12(List_1_t128053199 * value)
	{
		___winnings_12 = value;
		Il2CppCodeGenWriteBarrier((&___winnings_12), value);
	}

	inline static int32_t get_offset_of_segments_13() { return static_cast<int32_t>(offsetof(SpinWheelController_t1228200680, ___segments_13)); }
	inline int32_t get_segments_13() const { return ___segments_13; }
	inline int32_t* get_address_of_segments_13() { return &___segments_13; }
	inline void set_segments_13(int32_t value)
	{
		___segments_13 = value;
	}

	inline static int32_t get_offset_of_numbers_14() { return static_cast<int32_t>(offsetof(SpinWheelController_t1228200680, ___numbers_14)); }
	inline List_1_t1752731834 * get_numbers_14() const { return ___numbers_14; }
	inline List_1_t1752731834 ** get_address_of_numbers_14() { return &___numbers_14; }
	inline void set_numbers_14(List_1_t1752731834 * value)
	{
		___numbers_14 = value;
		Il2CppCodeGenWriteBarrier((&___numbers_14), value);
	}
};

struct SpinWheelController_t1228200680_StaticFields
{
public:
	// SpinWheelController SpinWheelController::instance
	SpinWheelController_t1228200680 * ___instance_2;
	// DG.Tweening.Core.DOSetter`1<System.Single> SpinWheelController::<>f__am$cache0
	DOSetter_1_t2447375106 * ___U3CU3Ef__amU24cache0_15;
	// DG.Tweening.TweenCallback SpinWheelController::<>f__am$cache1
	TweenCallback_t3727756325 * ___U3CU3Ef__amU24cache1_16;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(SpinWheelController_t1228200680_StaticFields, ___instance_2)); }
	inline SpinWheelController_t1228200680 * get_instance_2() const { return ___instance_2; }
	inline SpinWheelController_t1228200680 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(SpinWheelController_t1228200680 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_15() { return static_cast<int32_t>(offsetof(SpinWheelController_t1228200680_StaticFields, ___U3CU3Ef__amU24cache0_15)); }
	inline DOSetter_1_t2447375106 * get_U3CU3Ef__amU24cache0_15() const { return ___U3CU3Ef__amU24cache0_15; }
	inline DOSetter_1_t2447375106 ** get_address_of_U3CU3Ef__amU24cache0_15() { return &___U3CU3Ef__amU24cache0_15; }
	inline void set_U3CU3Ef__amU24cache0_15(DOSetter_1_t2447375106 * value)
	{
		___U3CU3Ef__amU24cache0_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_16() { return static_cast<int32_t>(offsetof(SpinWheelController_t1228200680_StaticFields, ___U3CU3Ef__amU24cache1_16)); }
	inline TweenCallback_t3727756325 * get_U3CU3Ef__amU24cache1_16() const { return ___U3CU3Ef__amU24cache1_16; }
	inline TweenCallback_t3727756325 ** get_address_of_U3CU3Ef__amU24cache1_16() { return &___U3CU3Ef__amU24cache1_16; }
	inline void set_U3CU3Ef__amU24cache1_16(TweenCallback_t3727756325 * value)
	{
		___U3CU3Ef__amU24cache1_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPINWHEELCONTROLLER_T1228200680_H
#ifndef ANIMATABLECOMPONENT_T332733166_H
#define ANIMATABLECOMPONENT_T332733166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Core.AnimatableComponent
struct  AnimatableComponent_t332733166  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Animator Outback.Core.AnimatableComponent::anim
	Animator_t434523843 * ___anim_2;
	// UnityEngine.UI.Image Outback.Core.AnimatableComponent::image
	Image_t2670269651 * ___image_3;
	// System.Collections.Generic.List`1<UnityEngine.AnimatorControllerParameter> Outback.Core.AnimatableComponent::prmtrs
	List_1_t3230334784 * ___prmtrs_4;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> Outback.Core.AnimatableComponent::sprites
	List_1_t1752731834 * ___sprites_5;
	// UnityEngine.Vector3 Outback.Core.AnimatableComponent::defaultSize
	Vector3_t3722313464  ___defaultSize_6;

public:
	inline static int32_t get_offset_of_anim_2() { return static_cast<int32_t>(offsetof(AnimatableComponent_t332733166, ___anim_2)); }
	inline Animator_t434523843 * get_anim_2() const { return ___anim_2; }
	inline Animator_t434523843 ** get_address_of_anim_2() { return &___anim_2; }
	inline void set_anim_2(Animator_t434523843 * value)
	{
		___anim_2 = value;
		Il2CppCodeGenWriteBarrier((&___anim_2), value);
	}

	inline static int32_t get_offset_of_image_3() { return static_cast<int32_t>(offsetof(AnimatableComponent_t332733166, ___image_3)); }
	inline Image_t2670269651 * get_image_3() const { return ___image_3; }
	inline Image_t2670269651 ** get_address_of_image_3() { return &___image_3; }
	inline void set_image_3(Image_t2670269651 * value)
	{
		___image_3 = value;
		Il2CppCodeGenWriteBarrier((&___image_3), value);
	}

	inline static int32_t get_offset_of_prmtrs_4() { return static_cast<int32_t>(offsetof(AnimatableComponent_t332733166, ___prmtrs_4)); }
	inline List_1_t3230334784 * get_prmtrs_4() const { return ___prmtrs_4; }
	inline List_1_t3230334784 ** get_address_of_prmtrs_4() { return &___prmtrs_4; }
	inline void set_prmtrs_4(List_1_t3230334784 * value)
	{
		___prmtrs_4 = value;
		Il2CppCodeGenWriteBarrier((&___prmtrs_4), value);
	}

	inline static int32_t get_offset_of_sprites_5() { return static_cast<int32_t>(offsetof(AnimatableComponent_t332733166, ___sprites_5)); }
	inline List_1_t1752731834 * get_sprites_5() const { return ___sprites_5; }
	inline List_1_t1752731834 ** get_address_of_sprites_5() { return &___sprites_5; }
	inline void set_sprites_5(List_1_t1752731834 * value)
	{
		___sprites_5 = value;
		Il2CppCodeGenWriteBarrier((&___sprites_5), value);
	}

	inline static int32_t get_offset_of_defaultSize_6() { return static_cast<int32_t>(offsetof(AnimatableComponent_t332733166, ___defaultSize_6)); }
	inline Vector3_t3722313464  get_defaultSize_6() const { return ___defaultSize_6; }
	inline Vector3_t3722313464 * get_address_of_defaultSize_6() { return &___defaultSize_6; }
	inline void set_defaultSize_6(Vector3_t3722313464  value)
	{
		___defaultSize_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANIMATABLECOMPONENT_T332733166_H
#ifndef ASSETBUNDLEHANDLER_T3701416431_H
#define ASSETBUNDLEHANDLER_T3701416431_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundleHandler
struct  AssetBundleHandler_t3701416431  : public MonoBehaviour_t3962482529
{
public:
	// System.String AssetBundleHandler::url
	String_t* ___url_3;
	// System.String AssetBundleHandler::serverLink
	String_t* ___serverLink_4;
	// System.Collections.Generic.List`1<System.String> AssetBundleHandler::gameNames
	List_1_t3319525431 * ___gameNames_5;
	// System.Collections.Generic.List`1<UnityEngine.AssetBundle> AssetBundleHandler::assetBundles
	List_1_t2625981994 * ___assetBundles_6;
	// System.Boolean AssetBundleHandler::isDownload
	bool ___isDownload_7;

public:
	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(AssetBundleHandler_t3701416431, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier((&___url_3), value);
	}

	inline static int32_t get_offset_of_serverLink_4() { return static_cast<int32_t>(offsetof(AssetBundleHandler_t3701416431, ___serverLink_4)); }
	inline String_t* get_serverLink_4() const { return ___serverLink_4; }
	inline String_t** get_address_of_serverLink_4() { return &___serverLink_4; }
	inline void set_serverLink_4(String_t* value)
	{
		___serverLink_4 = value;
		Il2CppCodeGenWriteBarrier((&___serverLink_4), value);
	}

	inline static int32_t get_offset_of_gameNames_5() { return static_cast<int32_t>(offsetof(AssetBundleHandler_t3701416431, ___gameNames_5)); }
	inline List_1_t3319525431 * get_gameNames_5() const { return ___gameNames_5; }
	inline List_1_t3319525431 ** get_address_of_gameNames_5() { return &___gameNames_5; }
	inline void set_gameNames_5(List_1_t3319525431 * value)
	{
		___gameNames_5 = value;
		Il2CppCodeGenWriteBarrier((&___gameNames_5), value);
	}

	inline static int32_t get_offset_of_assetBundles_6() { return static_cast<int32_t>(offsetof(AssetBundleHandler_t3701416431, ___assetBundles_6)); }
	inline List_1_t2625981994 * get_assetBundles_6() const { return ___assetBundles_6; }
	inline List_1_t2625981994 ** get_address_of_assetBundles_6() { return &___assetBundles_6; }
	inline void set_assetBundles_6(List_1_t2625981994 * value)
	{
		___assetBundles_6 = value;
		Il2CppCodeGenWriteBarrier((&___assetBundles_6), value);
	}

	inline static int32_t get_offset_of_isDownload_7() { return static_cast<int32_t>(offsetof(AssetBundleHandler_t3701416431, ___isDownload_7)); }
	inline bool get_isDownload_7() const { return ___isDownload_7; }
	inline bool* get_address_of_isDownload_7() { return &___isDownload_7; }
	inline void set_isDownload_7(bool value)
	{
		___isDownload_7 = value;
	}
};

struct AssetBundleHandler_t3701416431_StaticFields
{
public:
	// AssetBundleHandler AssetBundleHandler::instance
	AssetBundleHandler_t3701416431 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(AssetBundleHandler_t3701416431_StaticFields, ___instance_2)); }
	inline AssetBundleHandler_t3701416431 * get_instance_2() const { return ___instance_2; }
	inline AssetBundleHandler_t3701416431 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(AssetBundleHandler_t3701416431 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETBUNDLEHANDLER_T3701416431_H
#ifndef BASEGAME_T2336638441_H
#define BASEGAME_T2336638441_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseGame
struct  BaseGame_t2336638441  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Image BaseGame::freeSpinPopUp
	Image_t2670269651 * ___freeSpinPopUp_5;
	// UnityEngine.GameObject BaseGame::rewardPopUp
	GameObject_t1113636619 * ___rewardPopUp_6;
	// UnityEngine.GameObject BaseGame::noRewardPopUp
	GameObject_t1113636619 * ___noRewardPopUp_7;
	// UnityEngine.UI.Button BaseGame::spinBtn
	Button_t4055032469 * ___spinBtn_8;
	// UnityEngine.UI.Button BaseGame::stopBtn
	Button_t4055032469 * ___stopBtn_9;
	// UnityEngine.UI.Button BaseGame::freeSpinBtn
	Button_t4055032469 * ___freeSpinBtn_10;
	// UnityEngine.GameObject BaseGame::ruleLine
	GameObject_t1113636619 * ___ruleLine_11;
	// UnityEngine.GameObject BaseGame::freeSpinRewardPopUp
	GameObject_t1113636619 * ___freeSpinRewardPopUp_12;
	// UnityEngine.GameObject BaseGame::bonusGamePanel
	GameObject_t1113636619 * ___bonusGamePanel_13;
	// UnityEngine.ParticleSystem BaseGame::coinsAnim
	ParticleSystem_t1800779281 * ___coinsAnim_15;
	// UnityEngine.Sprite BaseGame::stopButtonSprite
	Sprite_t280657092 * ___stopButtonSprite_16;
	// UnityEngine.Sprite BaseGame::spinButtonSprite
	Sprite_t280657092 * ___spinButtonSprite_17;
	// UnityEngine.Sprite BaseGame::autoSpinStopButtonSprite
	Sprite_t280657092 * ___autoSpinStopButtonSprite_18;
	// UnityEngine.Sprite BaseGame::autoSpinStartButtonSprite
	Sprite_t280657092 * ___autoSpinStartButtonSprite_19;
	// UnityEngine.AudioSource BaseGame::musicSource
	AudioSource_t3935305588 * ___musicSource_20;
	// UnityEngine.AudioSource BaseGame::musicSourceFreeScatter
	AudioSource_t3935305588 * ___musicSourceFreeScatter_21;
	// System.Int64 BaseGame::winAmount
	int64_t ___winAmount_22;
	// System.Int64 BaseGame::betAmount
	int64_t ___betAmount_23;
	// UnityEngine.UI.Text BaseGame::betValue
	Text_t1901882714 * ___betValue_24;
	// UnityEngine.UI.Text BaseGame::currentCoins
	Text_t1901882714 * ___currentCoins_25;
	// UnityEngine.UI.Text BaseGame::winValue
	Text_t1901882714 * ___winValue_26;
	// UnityEngine.UI.Text BaseGame::freeSpinRemainingText
	Text_t1901882714 * ___freeSpinRemainingText_27;
	// System.Boolean BaseGame::countingFreeSpins
	bool ___countingFreeSpins_28;
	// System.Int64 BaseGame::minBet
	int64_t ___minBet_29;
	// System.Int32 BaseGame::numberOfFreeSpinsLeft
	int32_t ___numberOfFreeSpinsLeft_30;
	// System.Int64 BaseGame::freeSpinWinnings
	int64_t ___freeSpinWinnings_31;
	// System.Boolean BaseGame::isBonusGame
	bool ___isBonusGame_32;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> BaseGame::rulePages
	List_1_t2585711361 * ___rulePages_33;
	// System.Collections.Generic.List`1<Outback.Core.Reward> BaseGame::rewards
	List_1_t15860817 * ___rewards_34;

public:
	inline static int32_t get_offset_of_freeSpinPopUp_5() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___freeSpinPopUp_5)); }
	inline Image_t2670269651 * get_freeSpinPopUp_5() const { return ___freeSpinPopUp_5; }
	inline Image_t2670269651 ** get_address_of_freeSpinPopUp_5() { return &___freeSpinPopUp_5; }
	inline void set_freeSpinPopUp_5(Image_t2670269651 * value)
	{
		___freeSpinPopUp_5 = value;
		Il2CppCodeGenWriteBarrier((&___freeSpinPopUp_5), value);
	}

	inline static int32_t get_offset_of_rewardPopUp_6() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___rewardPopUp_6)); }
	inline GameObject_t1113636619 * get_rewardPopUp_6() const { return ___rewardPopUp_6; }
	inline GameObject_t1113636619 ** get_address_of_rewardPopUp_6() { return &___rewardPopUp_6; }
	inline void set_rewardPopUp_6(GameObject_t1113636619 * value)
	{
		___rewardPopUp_6 = value;
		Il2CppCodeGenWriteBarrier((&___rewardPopUp_6), value);
	}

	inline static int32_t get_offset_of_noRewardPopUp_7() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___noRewardPopUp_7)); }
	inline GameObject_t1113636619 * get_noRewardPopUp_7() const { return ___noRewardPopUp_7; }
	inline GameObject_t1113636619 ** get_address_of_noRewardPopUp_7() { return &___noRewardPopUp_7; }
	inline void set_noRewardPopUp_7(GameObject_t1113636619 * value)
	{
		___noRewardPopUp_7 = value;
		Il2CppCodeGenWriteBarrier((&___noRewardPopUp_7), value);
	}

	inline static int32_t get_offset_of_spinBtn_8() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___spinBtn_8)); }
	inline Button_t4055032469 * get_spinBtn_8() const { return ___spinBtn_8; }
	inline Button_t4055032469 ** get_address_of_spinBtn_8() { return &___spinBtn_8; }
	inline void set_spinBtn_8(Button_t4055032469 * value)
	{
		___spinBtn_8 = value;
		Il2CppCodeGenWriteBarrier((&___spinBtn_8), value);
	}

	inline static int32_t get_offset_of_stopBtn_9() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___stopBtn_9)); }
	inline Button_t4055032469 * get_stopBtn_9() const { return ___stopBtn_9; }
	inline Button_t4055032469 ** get_address_of_stopBtn_9() { return &___stopBtn_9; }
	inline void set_stopBtn_9(Button_t4055032469 * value)
	{
		___stopBtn_9 = value;
		Il2CppCodeGenWriteBarrier((&___stopBtn_9), value);
	}

	inline static int32_t get_offset_of_freeSpinBtn_10() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___freeSpinBtn_10)); }
	inline Button_t4055032469 * get_freeSpinBtn_10() const { return ___freeSpinBtn_10; }
	inline Button_t4055032469 ** get_address_of_freeSpinBtn_10() { return &___freeSpinBtn_10; }
	inline void set_freeSpinBtn_10(Button_t4055032469 * value)
	{
		___freeSpinBtn_10 = value;
		Il2CppCodeGenWriteBarrier((&___freeSpinBtn_10), value);
	}

	inline static int32_t get_offset_of_ruleLine_11() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___ruleLine_11)); }
	inline GameObject_t1113636619 * get_ruleLine_11() const { return ___ruleLine_11; }
	inline GameObject_t1113636619 ** get_address_of_ruleLine_11() { return &___ruleLine_11; }
	inline void set_ruleLine_11(GameObject_t1113636619 * value)
	{
		___ruleLine_11 = value;
		Il2CppCodeGenWriteBarrier((&___ruleLine_11), value);
	}

	inline static int32_t get_offset_of_freeSpinRewardPopUp_12() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___freeSpinRewardPopUp_12)); }
	inline GameObject_t1113636619 * get_freeSpinRewardPopUp_12() const { return ___freeSpinRewardPopUp_12; }
	inline GameObject_t1113636619 ** get_address_of_freeSpinRewardPopUp_12() { return &___freeSpinRewardPopUp_12; }
	inline void set_freeSpinRewardPopUp_12(GameObject_t1113636619 * value)
	{
		___freeSpinRewardPopUp_12 = value;
		Il2CppCodeGenWriteBarrier((&___freeSpinRewardPopUp_12), value);
	}

	inline static int32_t get_offset_of_bonusGamePanel_13() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___bonusGamePanel_13)); }
	inline GameObject_t1113636619 * get_bonusGamePanel_13() const { return ___bonusGamePanel_13; }
	inline GameObject_t1113636619 ** get_address_of_bonusGamePanel_13() { return &___bonusGamePanel_13; }
	inline void set_bonusGamePanel_13(GameObject_t1113636619 * value)
	{
		___bonusGamePanel_13 = value;
		Il2CppCodeGenWriteBarrier((&___bonusGamePanel_13), value);
	}

	inline static int32_t get_offset_of_coinsAnim_15() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___coinsAnim_15)); }
	inline ParticleSystem_t1800779281 * get_coinsAnim_15() const { return ___coinsAnim_15; }
	inline ParticleSystem_t1800779281 ** get_address_of_coinsAnim_15() { return &___coinsAnim_15; }
	inline void set_coinsAnim_15(ParticleSystem_t1800779281 * value)
	{
		___coinsAnim_15 = value;
		Il2CppCodeGenWriteBarrier((&___coinsAnim_15), value);
	}

	inline static int32_t get_offset_of_stopButtonSprite_16() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___stopButtonSprite_16)); }
	inline Sprite_t280657092 * get_stopButtonSprite_16() const { return ___stopButtonSprite_16; }
	inline Sprite_t280657092 ** get_address_of_stopButtonSprite_16() { return &___stopButtonSprite_16; }
	inline void set_stopButtonSprite_16(Sprite_t280657092 * value)
	{
		___stopButtonSprite_16 = value;
		Il2CppCodeGenWriteBarrier((&___stopButtonSprite_16), value);
	}

	inline static int32_t get_offset_of_spinButtonSprite_17() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___spinButtonSprite_17)); }
	inline Sprite_t280657092 * get_spinButtonSprite_17() const { return ___spinButtonSprite_17; }
	inline Sprite_t280657092 ** get_address_of_spinButtonSprite_17() { return &___spinButtonSprite_17; }
	inline void set_spinButtonSprite_17(Sprite_t280657092 * value)
	{
		___spinButtonSprite_17 = value;
		Il2CppCodeGenWriteBarrier((&___spinButtonSprite_17), value);
	}

	inline static int32_t get_offset_of_autoSpinStopButtonSprite_18() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___autoSpinStopButtonSprite_18)); }
	inline Sprite_t280657092 * get_autoSpinStopButtonSprite_18() const { return ___autoSpinStopButtonSprite_18; }
	inline Sprite_t280657092 ** get_address_of_autoSpinStopButtonSprite_18() { return &___autoSpinStopButtonSprite_18; }
	inline void set_autoSpinStopButtonSprite_18(Sprite_t280657092 * value)
	{
		___autoSpinStopButtonSprite_18 = value;
		Il2CppCodeGenWriteBarrier((&___autoSpinStopButtonSprite_18), value);
	}

	inline static int32_t get_offset_of_autoSpinStartButtonSprite_19() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___autoSpinStartButtonSprite_19)); }
	inline Sprite_t280657092 * get_autoSpinStartButtonSprite_19() const { return ___autoSpinStartButtonSprite_19; }
	inline Sprite_t280657092 ** get_address_of_autoSpinStartButtonSprite_19() { return &___autoSpinStartButtonSprite_19; }
	inline void set_autoSpinStartButtonSprite_19(Sprite_t280657092 * value)
	{
		___autoSpinStartButtonSprite_19 = value;
		Il2CppCodeGenWriteBarrier((&___autoSpinStartButtonSprite_19), value);
	}

	inline static int32_t get_offset_of_musicSource_20() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___musicSource_20)); }
	inline AudioSource_t3935305588 * get_musicSource_20() const { return ___musicSource_20; }
	inline AudioSource_t3935305588 ** get_address_of_musicSource_20() { return &___musicSource_20; }
	inline void set_musicSource_20(AudioSource_t3935305588 * value)
	{
		___musicSource_20 = value;
		Il2CppCodeGenWriteBarrier((&___musicSource_20), value);
	}

	inline static int32_t get_offset_of_musicSourceFreeScatter_21() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___musicSourceFreeScatter_21)); }
	inline AudioSource_t3935305588 * get_musicSourceFreeScatter_21() const { return ___musicSourceFreeScatter_21; }
	inline AudioSource_t3935305588 ** get_address_of_musicSourceFreeScatter_21() { return &___musicSourceFreeScatter_21; }
	inline void set_musicSourceFreeScatter_21(AudioSource_t3935305588 * value)
	{
		___musicSourceFreeScatter_21 = value;
		Il2CppCodeGenWriteBarrier((&___musicSourceFreeScatter_21), value);
	}

	inline static int32_t get_offset_of_winAmount_22() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___winAmount_22)); }
	inline int64_t get_winAmount_22() const { return ___winAmount_22; }
	inline int64_t* get_address_of_winAmount_22() { return &___winAmount_22; }
	inline void set_winAmount_22(int64_t value)
	{
		___winAmount_22 = value;
	}

	inline static int32_t get_offset_of_betAmount_23() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___betAmount_23)); }
	inline int64_t get_betAmount_23() const { return ___betAmount_23; }
	inline int64_t* get_address_of_betAmount_23() { return &___betAmount_23; }
	inline void set_betAmount_23(int64_t value)
	{
		___betAmount_23 = value;
	}

	inline static int32_t get_offset_of_betValue_24() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___betValue_24)); }
	inline Text_t1901882714 * get_betValue_24() const { return ___betValue_24; }
	inline Text_t1901882714 ** get_address_of_betValue_24() { return &___betValue_24; }
	inline void set_betValue_24(Text_t1901882714 * value)
	{
		___betValue_24 = value;
		Il2CppCodeGenWriteBarrier((&___betValue_24), value);
	}

	inline static int32_t get_offset_of_currentCoins_25() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___currentCoins_25)); }
	inline Text_t1901882714 * get_currentCoins_25() const { return ___currentCoins_25; }
	inline Text_t1901882714 ** get_address_of_currentCoins_25() { return &___currentCoins_25; }
	inline void set_currentCoins_25(Text_t1901882714 * value)
	{
		___currentCoins_25 = value;
		Il2CppCodeGenWriteBarrier((&___currentCoins_25), value);
	}

	inline static int32_t get_offset_of_winValue_26() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___winValue_26)); }
	inline Text_t1901882714 * get_winValue_26() const { return ___winValue_26; }
	inline Text_t1901882714 ** get_address_of_winValue_26() { return &___winValue_26; }
	inline void set_winValue_26(Text_t1901882714 * value)
	{
		___winValue_26 = value;
		Il2CppCodeGenWriteBarrier((&___winValue_26), value);
	}

	inline static int32_t get_offset_of_freeSpinRemainingText_27() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___freeSpinRemainingText_27)); }
	inline Text_t1901882714 * get_freeSpinRemainingText_27() const { return ___freeSpinRemainingText_27; }
	inline Text_t1901882714 ** get_address_of_freeSpinRemainingText_27() { return &___freeSpinRemainingText_27; }
	inline void set_freeSpinRemainingText_27(Text_t1901882714 * value)
	{
		___freeSpinRemainingText_27 = value;
		Il2CppCodeGenWriteBarrier((&___freeSpinRemainingText_27), value);
	}

	inline static int32_t get_offset_of_countingFreeSpins_28() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___countingFreeSpins_28)); }
	inline bool get_countingFreeSpins_28() const { return ___countingFreeSpins_28; }
	inline bool* get_address_of_countingFreeSpins_28() { return &___countingFreeSpins_28; }
	inline void set_countingFreeSpins_28(bool value)
	{
		___countingFreeSpins_28 = value;
	}

	inline static int32_t get_offset_of_minBet_29() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___minBet_29)); }
	inline int64_t get_minBet_29() const { return ___minBet_29; }
	inline int64_t* get_address_of_minBet_29() { return &___minBet_29; }
	inline void set_minBet_29(int64_t value)
	{
		___minBet_29 = value;
	}

	inline static int32_t get_offset_of_numberOfFreeSpinsLeft_30() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___numberOfFreeSpinsLeft_30)); }
	inline int32_t get_numberOfFreeSpinsLeft_30() const { return ___numberOfFreeSpinsLeft_30; }
	inline int32_t* get_address_of_numberOfFreeSpinsLeft_30() { return &___numberOfFreeSpinsLeft_30; }
	inline void set_numberOfFreeSpinsLeft_30(int32_t value)
	{
		___numberOfFreeSpinsLeft_30 = value;
	}

	inline static int32_t get_offset_of_freeSpinWinnings_31() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___freeSpinWinnings_31)); }
	inline int64_t get_freeSpinWinnings_31() const { return ___freeSpinWinnings_31; }
	inline int64_t* get_address_of_freeSpinWinnings_31() { return &___freeSpinWinnings_31; }
	inline void set_freeSpinWinnings_31(int64_t value)
	{
		___freeSpinWinnings_31 = value;
	}

	inline static int32_t get_offset_of_isBonusGame_32() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___isBonusGame_32)); }
	inline bool get_isBonusGame_32() const { return ___isBonusGame_32; }
	inline bool* get_address_of_isBonusGame_32() { return &___isBonusGame_32; }
	inline void set_isBonusGame_32(bool value)
	{
		___isBonusGame_32 = value;
	}

	inline static int32_t get_offset_of_rulePages_33() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___rulePages_33)); }
	inline List_1_t2585711361 * get_rulePages_33() const { return ___rulePages_33; }
	inline List_1_t2585711361 ** get_address_of_rulePages_33() { return &___rulePages_33; }
	inline void set_rulePages_33(List_1_t2585711361 * value)
	{
		___rulePages_33 = value;
		Il2CppCodeGenWriteBarrier((&___rulePages_33), value);
	}

	inline static int32_t get_offset_of_rewards_34() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441, ___rewards_34)); }
	inline List_1_t15860817 * get_rewards_34() const { return ___rewards_34; }
	inline List_1_t15860817 ** get_address_of_rewards_34() { return &___rewards_34; }
	inline void set_rewards_34(List_1_t15860817 * value)
	{
		___rewards_34 = value;
		Il2CppCodeGenWriteBarrier((&___rewards_34), value);
	}
};

struct BaseGame_t2336638441_StaticFields
{
public:
	// System.Int32[0...,0...] BaseGame::gameArray
	Int32U5B0___U2C0___U5D_t385246373* ___gameArray_2;
	// UnityEngine.UI.Image[0...,0...] BaseGame::gameImageArray
	ImageU5B0___U2C0___U5D_t2439009923* ___gameImageArray_3;
	// System.Int32[0...,0...] BaseGame::ruleGameArray
	Int32U5B0___U2C0___U5D_t385246373* ___ruleGameArray_4;
	// System.Int32 BaseGame::finalWin
	int32_t ___finalWin_14;
	// System.Converter`2<UnityEngine.GameObject,UnityEngine.AudioSource> BaseGame::<>f__am$cache0
	Converter_2_t3782339732 * ___U3CU3Ef__amU24cache0_35;
	// DG.Tweening.Core.DOSetter`1<System.Single> BaseGame::<>f__am$cache1
	DOSetter_1_t2447375106 * ___U3CU3Ef__amU24cache1_36;

public:
	inline static int32_t get_offset_of_gameArray_2() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441_StaticFields, ___gameArray_2)); }
	inline Int32U5B0___U2C0___U5D_t385246373* get_gameArray_2() const { return ___gameArray_2; }
	inline Int32U5B0___U2C0___U5D_t385246373** get_address_of_gameArray_2() { return &___gameArray_2; }
	inline void set_gameArray_2(Int32U5B0___U2C0___U5D_t385246373* value)
	{
		___gameArray_2 = value;
		Il2CppCodeGenWriteBarrier((&___gameArray_2), value);
	}

	inline static int32_t get_offset_of_gameImageArray_3() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441_StaticFields, ___gameImageArray_3)); }
	inline ImageU5B0___U2C0___U5D_t2439009923* get_gameImageArray_3() const { return ___gameImageArray_3; }
	inline ImageU5B0___U2C0___U5D_t2439009923** get_address_of_gameImageArray_3() { return &___gameImageArray_3; }
	inline void set_gameImageArray_3(ImageU5B0___U2C0___U5D_t2439009923* value)
	{
		___gameImageArray_3 = value;
		Il2CppCodeGenWriteBarrier((&___gameImageArray_3), value);
	}

	inline static int32_t get_offset_of_ruleGameArray_4() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441_StaticFields, ___ruleGameArray_4)); }
	inline Int32U5B0___U2C0___U5D_t385246373* get_ruleGameArray_4() const { return ___ruleGameArray_4; }
	inline Int32U5B0___U2C0___U5D_t385246373** get_address_of_ruleGameArray_4() { return &___ruleGameArray_4; }
	inline void set_ruleGameArray_4(Int32U5B0___U2C0___U5D_t385246373* value)
	{
		___ruleGameArray_4 = value;
		Il2CppCodeGenWriteBarrier((&___ruleGameArray_4), value);
	}

	inline static int32_t get_offset_of_finalWin_14() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441_StaticFields, ___finalWin_14)); }
	inline int32_t get_finalWin_14() const { return ___finalWin_14; }
	inline int32_t* get_address_of_finalWin_14() { return &___finalWin_14; }
	inline void set_finalWin_14(int32_t value)
	{
		___finalWin_14 = value;
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_35() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441_StaticFields, ___U3CU3Ef__amU24cache0_35)); }
	inline Converter_2_t3782339732 * get_U3CU3Ef__amU24cache0_35() const { return ___U3CU3Ef__amU24cache0_35; }
	inline Converter_2_t3782339732 ** get_address_of_U3CU3Ef__amU24cache0_35() { return &___U3CU3Ef__amU24cache0_35; }
	inline void set_U3CU3Ef__amU24cache0_35(Converter_2_t3782339732 * value)
	{
		___U3CU3Ef__amU24cache0_35 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_35), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_36() { return static_cast<int32_t>(offsetof(BaseGame_t2336638441_StaticFields, ___U3CU3Ef__amU24cache1_36)); }
	inline DOSetter_1_t2447375106 * get_U3CU3Ef__amU24cache1_36() const { return ___U3CU3Ef__amU24cache1_36; }
	inline DOSetter_1_t2447375106 ** get_address_of_U3CU3Ef__amU24cache1_36() { return &___U3CU3Ef__amU24cache1_36; }
	inline void set_U3CU3Ef__amU24cache1_36(DOSetter_1_t2447375106 * value)
	{
		___U3CU3Ef__amU24cache1_36 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_36), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEGAME_T2336638441_H
#ifndef CLICKHANDLER_T1871395319_H
#define CLICKHANDLER_T1871395319_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ClickHandler
struct  ClickHandler_t1871395319  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLICKHANDLER_T1871395319_H
#ifndef COINSSHOWER_T2268929218_H
#define COINSSHOWER_T2268929218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CoinsShower
struct  CoinsShower_t2268929218  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject CoinsShower::coinPrefab
	GameObject_t1113636619 * ___coinPrefab_2;

public:
	inline static int32_t get_offset_of_coinPrefab_2() { return static_cast<int32_t>(offsetof(CoinsShower_t2268929218, ___coinPrefab_2)); }
	inline GameObject_t1113636619 * get_coinPrefab_2() const { return ___coinPrefab_2; }
	inline GameObject_t1113636619 ** get_address_of_coinPrefab_2() { return &___coinPrefab_2; }
	inline void set_coinPrefab_2(GameObject_t1113636619 * value)
	{
		___coinPrefab_2 = value;
		Il2CppCodeGenWriteBarrier((&___coinPrefab_2), value);
	}
};

struct CoinsShower_t2268929218_StaticFields
{
public:
	// CoinsShower CoinsShower::instance
	CoinsShower_t2268929218 * ___instance_3;

public:
	inline static int32_t get_offset_of_instance_3() { return static_cast<int32_t>(offsetof(CoinsShower_t2268929218_StaticFields, ___instance_3)); }
	inline CoinsShower_t2268929218 * get_instance_3() const { return ___instance_3; }
	inline CoinsShower_t2268929218 ** get_address_of_instance_3() { return &___instance_3; }
	inline void set_instance_3(CoinsShower_t2268929218 * value)
	{
		___instance_3 = value;
		Il2CppCodeGenWriteBarrier((&___instance_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COINSSHOWER_T2268929218_H
#ifndef PLAYER_T2753795864_H
#define PLAYER_T2753795864_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Core.Player
struct  Player_t2753795864  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Player_t2753795864_StaticFields
{
public:
	// System.String Outback.Core.Player::userId
	String_t* ___userId_2;
	// System.String Outback.Core.Player::machineId
	String_t* ___machineId_3;
	// System.String Outback.Core.Player::userName
	String_t* ___userName_4;
	// System.String Outback.Core.Player::email
	String_t* ___email_5;
	// System.Int64 Outback.Core.Player::currentCoins
	int64_t ___currentCoins_6;
	// System.Single Outback.Core.Player::spentCoins
	float ___spentCoins_7;
	// System.Single Outback.Core.Player::level
	float ___level_8;
	// System.String Outback.Core.Player::password
	String_t* ___password_9;

public:
	inline static int32_t get_offset_of_userId_2() { return static_cast<int32_t>(offsetof(Player_t2753795864_StaticFields, ___userId_2)); }
	inline String_t* get_userId_2() const { return ___userId_2; }
	inline String_t** get_address_of_userId_2() { return &___userId_2; }
	inline void set_userId_2(String_t* value)
	{
		___userId_2 = value;
		Il2CppCodeGenWriteBarrier((&___userId_2), value);
	}

	inline static int32_t get_offset_of_machineId_3() { return static_cast<int32_t>(offsetof(Player_t2753795864_StaticFields, ___machineId_3)); }
	inline String_t* get_machineId_3() const { return ___machineId_3; }
	inline String_t** get_address_of_machineId_3() { return &___machineId_3; }
	inline void set_machineId_3(String_t* value)
	{
		___machineId_3 = value;
		Il2CppCodeGenWriteBarrier((&___machineId_3), value);
	}

	inline static int32_t get_offset_of_userName_4() { return static_cast<int32_t>(offsetof(Player_t2753795864_StaticFields, ___userName_4)); }
	inline String_t* get_userName_4() const { return ___userName_4; }
	inline String_t** get_address_of_userName_4() { return &___userName_4; }
	inline void set_userName_4(String_t* value)
	{
		___userName_4 = value;
		Il2CppCodeGenWriteBarrier((&___userName_4), value);
	}

	inline static int32_t get_offset_of_email_5() { return static_cast<int32_t>(offsetof(Player_t2753795864_StaticFields, ___email_5)); }
	inline String_t* get_email_5() const { return ___email_5; }
	inline String_t** get_address_of_email_5() { return &___email_5; }
	inline void set_email_5(String_t* value)
	{
		___email_5 = value;
		Il2CppCodeGenWriteBarrier((&___email_5), value);
	}

	inline static int32_t get_offset_of_currentCoins_6() { return static_cast<int32_t>(offsetof(Player_t2753795864_StaticFields, ___currentCoins_6)); }
	inline int64_t get_currentCoins_6() const { return ___currentCoins_6; }
	inline int64_t* get_address_of_currentCoins_6() { return &___currentCoins_6; }
	inline void set_currentCoins_6(int64_t value)
	{
		___currentCoins_6 = value;
	}

	inline static int32_t get_offset_of_spentCoins_7() { return static_cast<int32_t>(offsetof(Player_t2753795864_StaticFields, ___spentCoins_7)); }
	inline float get_spentCoins_7() const { return ___spentCoins_7; }
	inline float* get_address_of_spentCoins_7() { return &___spentCoins_7; }
	inline void set_spentCoins_7(float value)
	{
		___spentCoins_7 = value;
	}

	inline static int32_t get_offset_of_level_8() { return static_cast<int32_t>(offsetof(Player_t2753795864_StaticFields, ___level_8)); }
	inline float get_level_8() const { return ___level_8; }
	inline float* get_address_of_level_8() { return &___level_8; }
	inline void set_level_8(float value)
	{
		___level_8 = value;
	}

	inline static int32_t get_offset_of_password_9() { return static_cast<int32_t>(offsetof(Player_t2753795864_StaticFields, ___password_9)); }
	inline String_t* get_password_9() const { return ___password_9; }
	inline String_t** get_address_of_password_9() { return &___password_9; }
	inline void set_password_9(String_t* value)
	{
		___password_9 = value;
		Il2CppCodeGenWriteBarrier((&___password_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYER_T2753795864_H
#ifndef DAILYREWARD_T2847634760_H
#define DAILYREWARD_T2847634760_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DailyReward
struct  DailyReward_t2847634760  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text DailyReward::timeLabel
	Text_t1901882714 * ___timeLabel_2;
	// UnityEngine.UI.Button DailyReward::timerButton
	Button_t4055032469 * ___timerButton_3;
	// System.Int32 DailyReward::hours
	int32_t ___hours_4;
	// System.Int32 DailyReward::minutes
	int32_t ___minutes_5;
	// System.Int32 DailyReward::seconds
	int32_t ___seconds_6;
	// System.Boolean DailyReward::_timerComplete
	bool ____timerComplete_7;
	// System.Boolean DailyReward::_timerIsReady
	bool ____timerIsReady_8;
	// System.TimeSpan DailyReward::startTime
	TimeSpan_t881159249  ___startTime_9;
	// System.TimeSpan DailyReward::endTime
	TimeSpan_t881159249  ___endTime_10;
	// System.TimeSpan DailyReward::remainingTime
	TimeSpan_t881159249  ___remainingTime_11;
	// System.Single DailyReward::_value
	float ____value_12;
	// UnityEngine.GameObject DailyReward::spinWheel
	GameObject_t1113636619 * ___spinWheel_14;

public:
	inline static int32_t get_offset_of_timeLabel_2() { return static_cast<int32_t>(offsetof(DailyReward_t2847634760, ___timeLabel_2)); }
	inline Text_t1901882714 * get_timeLabel_2() const { return ___timeLabel_2; }
	inline Text_t1901882714 ** get_address_of_timeLabel_2() { return &___timeLabel_2; }
	inline void set_timeLabel_2(Text_t1901882714 * value)
	{
		___timeLabel_2 = value;
		Il2CppCodeGenWriteBarrier((&___timeLabel_2), value);
	}

	inline static int32_t get_offset_of_timerButton_3() { return static_cast<int32_t>(offsetof(DailyReward_t2847634760, ___timerButton_3)); }
	inline Button_t4055032469 * get_timerButton_3() const { return ___timerButton_3; }
	inline Button_t4055032469 ** get_address_of_timerButton_3() { return &___timerButton_3; }
	inline void set_timerButton_3(Button_t4055032469 * value)
	{
		___timerButton_3 = value;
		Il2CppCodeGenWriteBarrier((&___timerButton_3), value);
	}

	inline static int32_t get_offset_of_hours_4() { return static_cast<int32_t>(offsetof(DailyReward_t2847634760, ___hours_4)); }
	inline int32_t get_hours_4() const { return ___hours_4; }
	inline int32_t* get_address_of_hours_4() { return &___hours_4; }
	inline void set_hours_4(int32_t value)
	{
		___hours_4 = value;
	}

	inline static int32_t get_offset_of_minutes_5() { return static_cast<int32_t>(offsetof(DailyReward_t2847634760, ___minutes_5)); }
	inline int32_t get_minutes_5() const { return ___minutes_5; }
	inline int32_t* get_address_of_minutes_5() { return &___minutes_5; }
	inline void set_minutes_5(int32_t value)
	{
		___minutes_5 = value;
	}

	inline static int32_t get_offset_of_seconds_6() { return static_cast<int32_t>(offsetof(DailyReward_t2847634760, ___seconds_6)); }
	inline int32_t get_seconds_6() const { return ___seconds_6; }
	inline int32_t* get_address_of_seconds_6() { return &___seconds_6; }
	inline void set_seconds_6(int32_t value)
	{
		___seconds_6 = value;
	}

	inline static int32_t get_offset_of__timerComplete_7() { return static_cast<int32_t>(offsetof(DailyReward_t2847634760, ____timerComplete_7)); }
	inline bool get__timerComplete_7() const { return ____timerComplete_7; }
	inline bool* get_address_of__timerComplete_7() { return &____timerComplete_7; }
	inline void set__timerComplete_7(bool value)
	{
		____timerComplete_7 = value;
	}

	inline static int32_t get_offset_of__timerIsReady_8() { return static_cast<int32_t>(offsetof(DailyReward_t2847634760, ____timerIsReady_8)); }
	inline bool get__timerIsReady_8() const { return ____timerIsReady_8; }
	inline bool* get_address_of__timerIsReady_8() { return &____timerIsReady_8; }
	inline void set__timerIsReady_8(bool value)
	{
		____timerIsReady_8 = value;
	}

	inline static int32_t get_offset_of_startTime_9() { return static_cast<int32_t>(offsetof(DailyReward_t2847634760, ___startTime_9)); }
	inline TimeSpan_t881159249  get_startTime_9() const { return ___startTime_9; }
	inline TimeSpan_t881159249 * get_address_of_startTime_9() { return &___startTime_9; }
	inline void set_startTime_9(TimeSpan_t881159249  value)
	{
		___startTime_9 = value;
	}

	inline static int32_t get_offset_of_endTime_10() { return static_cast<int32_t>(offsetof(DailyReward_t2847634760, ___endTime_10)); }
	inline TimeSpan_t881159249  get_endTime_10() const { return ___endTime_10; }
	inline TimeSpan_t881159249 * get_address_of_endTime_10() { return &___endTime_10; }
	inline void set_endTime_10(TimeSpan_t881159249  value)
	{
		___endTime_10 = value;
	}

	inline static int32_t get_offset_of_remainingTime_11() { return static_cast<int32_t>(offsetof(DailyReward_t2847634760, ___remainingTime_11)); }
	inline TimeSpan_t881159249  get_remainingTime_11() const { return ___remainingTime_11; }
	inline TimeSpan_t881159249 * get_address_of_remainingTime_11() { return &___remainingTime_11; }
	inline void set_remainingTime_11(TimeSpan_t881159249  value)
	{
		___remainingTime_11 = value;
	}

	inline static int32_t get_offset_of__value_12() { return static_cast<int32_t>(offsetof(DailyReward_t2847634760, ____value_12)); }
	inline float get__value_12() const { return ____value_12; }
	inline float* get_address_of__value_12() { return &____value_12; }
	inline void set__value_12(float value)
	{
		____value_12 = value;
	}

	inline static int32_t get_offset_of_spinWheel_14() { return static_cast<int32_t>(offsetof(DailyReward_t2847634760, ___spinWheel_14)); }
	inline GameObject_t1113636619 * get_spinWheel_14() const { return ___spinWheel_14; }
	inline GameObject_t1113636619 ** get_address_of_spinWheel_14() { return &___spinWheel_14; }
	inline void set_spinWheel_14(GameObject_t1113636619 * value)
	{
		___spinWheel_14 = value;
		Il2CppCodeGenWriteBarrier((&___spinWheel_14), value);
	}
};

struct DailyReward_t2847634760_StaticFields
{
public:
	// System.Int32 DailyReward::RewardToEarn
	int32_t ___RewardToEarn_13;

public:
	inline static int32_t get_offset_of_RewardToEarn_13() { return static_cast<int32_t>(offsetof(DailyReward_t2847634760_StaticFields, ___RewardToEarn_13)); }
	inline int32_t get_RewardToEarn_13() const { return ___RewardToEarn_13; }
	inline int32_t* get_address_of_RewardToEarn_13() { return &___RewardToEarn_13; }
	inline void set_RewardToEarn_13(int32_t value)
	{
		___RewardToEarn_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DAILYREWARD_T2847634760_H
#ifndef FBINVITEMANAGER_T4111452257_H
#define FBINVITEMANAGER_T4111452257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// FBInviteManager
struct  FBInviteManager_t4111452257  : public MonoBehaviour_t3962482529
{
public:
	// System.String FBInviteManager::id
	String_t* ___id_2;

public:
	inline static int32_t get_offset_of_id_2() { return static_cast<int32_t>(offsetof(FBInviteManager_t4111452257, ___id_2)); }
	inline String_t* get_id_2() const { return ___id_2; }
	inline String_t** get_address_of_id_2() { return &___id_2; }
	inline void set_id_2(String_t* value)
	{
		___id_2 = value;
		Il2CppCodeGenWriteBarrier((&___id_2), value);
	}
};

struct FBInviteManager_t4111452257_StaticFields
{
public:
	// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IAppRequestResult> FBInviteManager::<>f__am$cache0
	FacebookDelegate_1_t1976622014 * ___U3CU3Ef__amU24cache0_3;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_3() { return static_cast<int32_t>(offsetof(FBInviteManager_t4111452257_StaticFields, ___U3CU3Ef__amU24cache0_3)); }
	inline FacebookDelegate_1_t1976622014 * get_U3CU3Ef__amU24cache0_3() const { return ___U3CU3Ef__amU24cache0_3; }
	inline FacebookDelegate_1_t1976622014 ** get_address_of_U3CU3Ef__amU24cache0_3() { return &___U3CU3Ef__amU24cache0_3; }
	inline void set_U3CU3Ef__amU24cache0_3(FacebookDelegate_1_t1976622014 * value)
	{
		___U3CU3Ef__amU24cache0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FBINVITEMANAGER_T4111452257_H
#ifndef SCREENMANAGER_T3517964220_H
#define SCREENMANAGER_T3517964220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Managers.ScreenManager
struct  ScreenManager_t3517964220  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject Outback.Managers.ScreenManager::loadingScreenObj
	GameObject_t1113636619 * ___loadingScreenObj_3;
	// UnityEngine.GameObject Outback.Managers.ScreenManager::settingPanel
	GameObject_t1113636619 * ___settingPanel_4;
	// UnityEngine.GameObject Outback.Managers.ScreenManager::iapPanel
	GameObject_t1113636619 * ___iapPanel_5;
	// UnityEngine.GameObject Outback.Managers.ScreenManager::promocodePanel
	GameObject_t1113636619 * ___promocodePanel_6;
	// UnityEngine.GameObject Outback.Managers.ScreenManager::errorPanel
	GameObject_t1113636619 * ___errorPanel_7;
	// UnityEngine.GameObject Outback.Managers.ScreenManager::playerDetailsPanel
	GameObject_t1113636619 * ___playerDetailsPanel_8;
	// UnityEngine.GameObject Outback.Managers.ScreenManager::iapBackBtn
	GameObject_t1113636619 * ___iapBackBtn_9;
	// UnityEngine.UI.Slider Outback.Managers.ScreenManager::loadingSlider
	Slider_t3903728902 * ___loadingSlider_10;

public:
	inline static int32_t get_offset_of_loadingScreenObj_3() { return static_cast<int32_t>(offsetof(ScreenManager_t3517964220, ___loadingScreenObj_3)); }
	inline GameObject_t1113636619 * get_loadingScreenObj_3() const { return ___loadingScreenObj_3; }
	inline GameObject_t1113636619 ** get_address_of_loadingScreenObj_3() { return &___loadingScreenObj_3; }
	inline void set_loadingScreenObj_3(GameObject_t1113636619 * value)
	{
		___loadingScreenObj_3 = value;
		Il2CppCodeGenWriteBarrier((&___loadingScreenObj_3), value);
	}

	inline static int32_t get_offset_of_settingPanel_4() { return static_cast<int32_t>(offsetof(ScreenManager_t3517964220, ___settingPanel_4)); }
	inline GameObject_t1113636619 * get_settingPanel_4() const { return ___settingPanel_4; }
	inline GameObject_t1113636619 ** get_address_of_settingPanel_4() { return &___settingPanel_4; }
	inline void set_settingPanel_4(GameObject_t1113636619 * value)
	{
		___settingPanel_4 = value;
		Il2CppCodeGenWriteBarrier((&___settingPanel_4), value);
	}

	inline static int32_t get_offset_of_iapPanel_5() { return static_cast<int32_t>(offsetof(ScreenManager_t3517964220, ___iapPanel_5)); }
	inline GameObject_t1113636619 * get_iapPanel_5() const { return ___iapPanel_5; }
	inline GameObject_t1113636619 ** get_address_of_iapPanel_5() { return &___iapPanel_5; }
	inline void set_iapPanel_5(GameObject_t1113636619 * value)
	{
		___iapPanel_5 = value;
		Il2CppCodeGenWriteBarrier((&___iapPanel_5), value);
	}

	inline static int32_t get_offset_of_promocodePanel_6() { return static_cast<int32_t>(offsetof(ScreenManager_t3517964220, ___promocodePanel_6)); }
	inline GameObject_t1113636619 * get_promocodePanel_6() const { return ___promocodePanel_6; }
	inline GameObject_t1113636619 ** get_address_of_promocodePanel_6() { return &___promocodePanel_6; }
	inline void set_promocodePanel_6(GameObject_t1113636619 * value)
	{
		___promocodePanel_6 = value;
		Il2CppCodeGenWriteBarrier((&___promocodePanel_6), value);
	}

	inline static int32_t get_offset_of_errorPanel_7() { return static_cast<int32_t>(offsetof(ScreenManager_t3517964220, ___errorPanel_7)); }
	inline GameObject_t1113636619 * get_errorPanel_7() const { return ___errorPanel_7; }
	inline GameObject_t1113636619 ** get_address_of_errorPanel_7() { return &___errorPanel_7; }
	inline void set_errorPanel_7(GameObject_t1113636619 * value)
	{
		___errorPanel_7 = value;
		Il2CppCodeGenWriteBarrier((&___errorPanel_7), value);
	}

	inline static int32_t get_offset_of_playerDetailsPanel_8() { return static_cast<int32_t>(offsetof(ScreenManager_t3517964220, ___playerDetailsPanel_8)); }
	inline GameObject_t1113636619 * get_playerDetailsPanel_8() const { return ___playerDetailsPanel_8; }
	inline GameObject_t1113636619 ** get_address_of_playerDetailsPanel_8() { return &___playerDetailsPanel_8; }
	inline void set_playerDetailsPanel_8(GameObject_t1113636619 * value)
	{
		___playerDetailsPanel_8 = value;
		Il2CppCodeGenWriteBarrier((&___playerDetailsPanel_8), value);
	}

	inline static int32_t get_offset_of_iapBackBtn_9() { return static_cast<int32_t>(offsetof(ScreenManager_t3517964220, ___iapBackBtn_9)); }
	inline GameObject_t1113636619 * get_iapBackBtn_9() const { return ___iapBackBtn_9; }
	inline GameObject_t1113636619 ** get_address_of_iapBackBtn_9() { return &___iapBackBtn_9; }
	inline void set_iapBackBtn_9(GameObject_t1113636619 * value)
	{
		___iapBackBtn_9 = value;
		Il2CppCodeGenWriteBarrier((&___iapBackBtn_9), value);
	}

	inline static int32_t get_offset_of_loadingSlider_10() { return static_cast<int32_t>(offsetof(ScreenManager_t3517964220, ___loadingSlider_10)); }
	inline Slider_t3903728902 * get_loadingSlider_10() const { return ___loadingSlider_10; }
	inline Slider_t3903728902 ** get_address_of_loadingSlider_10() { return &___loadingSlider_10; }
	inline void set_loadingSlider_10(Slider_t3903728902 * value)
	{
		___loadingSlider_10 = value;
		Il2CppCodeGenWriteBarrier((&___loadingSlider_10), value);
	}
};

struct ScreenManager_t3517964220_StaticFields
{
public:
	// System.String Outback.Managers.ScreenManager::SceneToJumpBack
	String_t* ___SceneToJumpBack_2;
	// Outback.Managers.ScreenManager Outback.Managers.ScreenManager::instance
	ScreenManager_t3517964220 * ___instance_11;

public:
	inline static int32_t get_offset_of_SceneToJumpBack_2() { return static_cast<int32_t>(offsetof(ScreenManager_t3517964220_StaticFields, ___SceneToJumpBack_2)); }
	inline String_t* get_SceneToJumpBack_2() const { return ___SceneToJumpBack_2; }
	inline String_t** get_address_of_SceneToJumpBack_2() { return &___SceneToJumpBack_2; }
	inline void set_SceneToJumpBack_2(String_t* value)
	{
		___SceneToJumpBack_2 = value;
		Il2CppCodeGenWriteBarrier((&___SceneToJumpBack_2), value);
	}

	inline static int32_t get_offset_of_instance_11() { return static_cast<int32_t>(offsetof(ScreenManager_t3517964220_StaticFields, ___instance_11)); }
	inline ScreenManager_t3517964220 * get_instance_11() const { return ___instance_11; }
	inline ScreenManager_t3517964220 ** get_address_of_instance_11() { return &___instance_11; }
	inline void set_instance_11(ScreenManager_t3517964220 * value)
	{
		___instance_11 = value;
		Il2CppCodeGenWriteBarrier((&___instance_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENMANAGER_T3517964220_H
#ifndef PURCHASER_T807278424_H
#define PURCHASER_T807278424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Managers.Purchaser
struct  Purchaser_t807278424  : public MonoBehaviour_t3962482529
{
public:

public:
};

struct Purchaser_t807278424_StaticFields
{
public:
	// UnityEngine.Purchasing.IStoreController Outback.Managers.Purchaser::m_StoreController
	RuntimeObject* ___m_StoreController_2;
	// UnityEngine.Purchasing.IExtensionProvider Outback.Managers.Purchaser::m_StoreExtensionProvider
	RuntimeObject* ___m_StoreExtensionProvider_3;
	// System.String Outback.Managers.Purchaser::kProductIDConsumable
	String_t* ___kProductIDConsumable_4;
	// System.String Outback.Managers.Purchaser::kProductIDNonConsumable
	String_t* ___kProductIDNonConsumable_5;
	// System.String Outback.Managers.Purchaser::kProductIDSubscription
	String_t* ___kProductIDSubscription_6;
	// System.String Outback.Managers.Purchaser::coins10000
	String_t* ___coins10000_7;
	// System.String Outback.Managers.Purchaser::coins25000
	String_t* ___coins25000_8;
	// System.String Outback.Managers.Purchaser::coins50000
	String_t* ___coins50000_9;
	// System.String Outback.Managers.Purchaser::coins75000
	String_t* ___coins75000_10;
	// System.String Outback.Managers.Purchaser::coins100000
	String_t* ___coins100000_11;
	// System.String Outback.Managers.Purchaser::coins500000
	String_t* ___coins500000_12;
	// System.String Outback.Managers.Purchaser::coins1000000
	String_t* ___coins1000000_13;
	// System.String Outback.Managers.Purchaser::coins5000000
	String_t* ___coins5000000_14;
	// System.String Outback.Managers.Purchaser::coins10000000
	String_t* ___coins10000000_15;
	// System.String Outback.Managers.Purchaser::kProductNameAppleSubscription
	String_t* ___kProductNameAppleSubscription_16;
	// System.String Outback.Managers.Purchaser::kProductNameGooglePlaySubscription
	String_t* ___kProductNameGooglePlaySubscription_17;

public:
	inline static int32_t get_offset_of_m_StoreController_2() { return static_cast<int32_t>(offsetof(Purchaser_t807278424_StaticFields, ___m_StoreController_2)); }
	inline RuntimeObject* get_m_StoreController_2() const { return ___m_StoreController_2; }
	inline RuntimeObject** get_address_of_m_StoreController_2() { return &___m_StoreController_2; }
	inline void set_m_StoreController_2(RuntimeObject* value)
	{
		___m_StoreController_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_StoreController_2), value);
	}

	inline static int32_t get_offset_of_m_StoreExtensionProvider_3() { return static_cast<int32_t>(offsetof(Purchaser_t807278424_StaticFields, ___m_StoreExtensionProvider_3)); }
	inline RuntimeObject* get_m_StoreExtensionProvider_3() const { return ___m_StoreExtensionProvider_3; }
	inline RuntimeObject** get_address_of_m_StoreExtensionProvider_3() { return &___m_StoreExtensionProvider_3; }
	inline void set_m_StoreExtensionProvider_3(RuntimeObject* value)
	{
		___m_StoreExtensionProvider_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_StoreExtensionProvider_3), value);
	}

	inline static int32_t get_offset_of_kProductIDConsumable_4() { return static_cast<int32_t>(offsetof(Purchaser_t807278424_StaticFields, ___kProductIDConsumable_4)); }
	inline String_t* get_kProductIDConsumable_4() const { return ___kProductIDConsumable_4; }
	inline String_t** get_address_of_kProductIDConsumable_4() { return &___kProductIDConsumable_4; }
	inline void set_kProductIDConsumable_4(String_t* value)
	{
		___kProductIDConsumable_4 = value;
		Il2CppCodeGenWriteBarrier((&___kProductIDConsumable_4), value);
	}

	inline static int32_t get_offset_of_kProductIDNonConsumable_5() { return static_cast<int32_t>(offsetof(Purchaser_t807278424_StaticFields, ___kProductIDNonConsumable_5)); }
	inline String_t* get_kProductIDNonConsumable_5() const { return ___kProductIDNonConsumable_5; }
	inline String_t** get_address_of_kProductIDNonConsumable_5() { return &___kProductIDNonConsumable_5; }
	inline void set_kProductIDNonConsumable_5(String_t* value)
	{
		___kProductIDNonConsumable_5 = value;
		Il2CppCodeGenWriteBarrier((&___kProductIDNonConsumable_5), value);
	}

	inline static int32_t get_offset_of_kProductIDSubscription_6() { return static_cast<int32_t>(offsetof(Purchaser_t807278424_StaticFields, ___kProductIDSubscription_6)); }
	inline String_t* get_kProductIDSubscription_6() const { return ___kProductIDSubscription_6; }
	inline String_t** get_address_of_kProductIDSubscription_6() { return &___kProductIDSubscription_6; }
	inline void set_kProductIDSubscription_6(String_t* value)
	{
		___kProductIDSubscription_6 = value;
		Il2CppCodeGenWriteBarrier((&___kProductIDSubscription_6), value);
	}

	inline static int32_t get_offset_of_coins10000_7() { return static_cast<int32_t>(offsetof(Purchaser_t807278424_StaticFields, ___coins10000_7)); }
	inline String_t* get_coins10000_7() const { return ___coins10000_7; }
	inline String_t** get_address_of_coins10000_7() { return &___coins10000_7; }
	inline void set_coins10000_7(String_t* value)
	{
		___coins10000_7 = value;
		Il2CppCodeGenWriteBarrier((&___coins10000_7), value);
	}

	inline static int32_t get_offset_of_coins25000_8() { return static_cast<int32_t>(offsetof(Purchaser_t807278424_StaticFields, ___coins25000_8)); }
	inline String_t* get_coins25000_8() const { return ___coins25000_8; }
	inline String_t** get_address_of_coins25000_8() { return &___coins25000_8; }
	inline void set_coins25000_8(String_t* value)
	{
		___coins25000_8 = value;
		Il2CppCodeGenWriteBarrier((&___coins25000_8), value);
	}

	inline static int32_t get_offset_of_coins50000_9() { return static_cast<int32_t>(offsetof(Purchaser_t807278424_StaticFields, ___coins50000_9)); }
	inline String_t* get_coins50000_9() const { return ___coins50000_9; }
	inline String_t** get_address_of_coins50000_9() { return &___coins50000_9; }
	inline void set_coins50000_9(String_t* value)
	{
		___coins50000_9 = value;
		Il2CppCodeGenWriteBarrier((&___coins50000_9), value);
	}

	inline static int32_t get_offset_of_coins75000_10() { return static_cast<int32_t>(offsetof(Purchaser_t807278424_StaticFields, ___coins75000_10)); }
	inline String_t* get_coins75000_10() const { return ___coins75000_10; }
	inline String_t** get_address_of_coins75000_10() { return &___coins75000_10; }
	inline void set_coins75000_10(String_t* value)
	{
		___coins75000_10 = value;
		Il2CppCodeGenWriteBarrier((&___coins75000_10), value);
	}

	inline static int32_t get_offset_of_coins100000_11() { return static_cast<int32_t>(offsetof(Purchaser_t807278424_StaticFields, ___coins100000_11)); }
	inline String_t* get_coins100000_11() const { return ___coins100000_11; }
	inline String_t** get_address_of_coins100000_11() { return &___coins100000_11; }
	inline void set_coins100000_11(String_t* value)
	{
		___coins100000_11 = value;
		Il2CppCodeGenWriteBarrier((&___coins100000_11), value);
	}

	inline static int32_t get_offset_of_coins500000_12() { return static_cast<int32_t>(offsetof(Purchaser_t807278424_StaticFields, ___coins500000_12)); }
	inline String_t* get_coins500000_12() const { return ___coins500000_12; }
	inline String_t** get_address_of_coins500000_12() { return &___coins500000_12; }
	inline void set_coins500000_12(String_t* value)
	{
		___coins500000_12 = value;
		Il2CppCodeGenWriteBarrier((&___coins500000_12), value);
	}

	inline static int32_t get_offset_of_coins1000000_13() { return static_cast<int32_t>(offsetof(Purchaser_t807278424_StaticFields, ___coins1000000_13)); }
	inline String_t* get_coins1000000_13() const { return ___coins1000000_13; }
	inline String_t** get_address_of_coins1000000_13() { return &___coins1000000_13; }
	inline void set_coins1000000_13(String_t* value)
	{
		___coins1000000_13 = value;
		Il2CppCodeGenWriteBarrier((&___coins1000000_13), value);
	}

	inline static int32_t get_offset_of_coins5000000_14() { return static_cast<int32_t>(offsetof(Purchaser_t807278424_StaticFields, ___coins5000000_14)); }
	inline String_t* get_coins5000000_14() const { return ___coins5000000_14; }
	inline String_t** get_address_of_coins5000000_14() { return &___coins5000000_14; }
	inline void set_coins5000000_14(String_t* value)
	{
		___coins5000000_14 = value;
		Il2CppCodeGenWriteBarrier((&___coins5000000_14), value);
	}

	inline static int32_t get_offset_of_coins10000000_15() { return static_cast<int32_t>(offsetof(Purchaser_t807278424_StaticFields, ___coins10000000_15)); }
	inline String_t* get_coins10000000_15() const { return ___coins10000000_15; }
	inline String_t** get_address_of_coins10000000_15() { return &___coins10000000_15; }
	inline void set_coins10000000_15(String_t* value)
	{
		___coins10000000_15 = value;
		Il2CppCodeGenWriteBarrier((&___coins10000000_15), value);
	}

	inline static int32_t get_offset_of_kProductNameAppleSubscription_16() { return static_cast<int32_t>(offsetof(Purchaser_t807278424_StaticFields, ___kProductNameAppleSubscription_16)); }
	inline String_t* get_kProductNameAppleSubscription_16() const { return ___kProductNameAppleSubscription_16; }
	inline String_t** get_address_of_kProductNameAppleSubscription_16() { return &___kProductNameAppleSubscription_16; }
	inline void set_kProductNameAppleSubscription_16(String_t* value)
	{
		___kProductNameAppleSubscription_16 = value;
		Il2CppCodeGenWriteBarrier((&___kProductNameAppleSubscription_16), value);
	}

	inline static int32_t get_offset_of_kProductNameGooglePlaySubscription_17() { return static_cast<int32_t>(offsetof(Purchaser_t807278424_StaticFields, ___kProductNameGooglePlaySubscription_17)); }
	inline String_t* get_kProductNameGooglePlaySubscription_17() const { return ___kProductNameGooglePlaySubscription_17; }
	inline String_t** get_address_of_kProductNameGooglePlaySubscription_17() { return &___kProductNameGooglePlaySubscription_17; }
	inline void set_kProductNameGooglePlaySubscription_17(String_t* value)
	{
		___kProductNameGooglePlaySubscription_17 = value;
		Il2CppCodeGenWriteBarrier((&___kProductNameGooglePlaySubscription_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PURCHASER_T807278424_H
#ifndef GAMESOUND_T3372255689_H
#define GAMESOUND_T3372255689_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameSound
struct  GameSound_t3372255689  : public MonoBehaviour_t3962482529
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESOUND_T3372255689_H
#ifndef MASTERCONFIGSMANAGER_T2615534321_H
#define MASTERCONFIGSMANAGER_T2615534321_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Managers.MasterConfigsManager
struct  MasterConfigsManager_t2615534321  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.AssetBundle Outback.Managers.MasterConfigsManager::lobbyAtlasAB
	AssetBundle_t1153907252 * ___lobbyAtlasAB_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> Outback.Managers.MasterConfigsManager::lobbyLayoutJson
	Dictionary_2_t2865362463 * ___lobbyLayoutJson_4;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> Outback.Managers.MasterConfigsManager::levelUpRewardJson
	Dictionary_2_t2865362463 * ___levelUpRewardJson_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Outback.Managers.MasterConfigsManager::masterGameConfigJson
	Dictionary_2_t1632706988 * ___masterGameConfigJson_6;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> Outback.Managers.MasterConfigsManager::storeInformationJson
	Dictionary_2_t2865362463 * ___storeInformationJson_7;
	// System.Collections.Generic.List`1<Outback.Managers.CrossPromoCreative> Outback.Managers.MasterConfigsManager::crossPromotions
	List_1_t2205672511 * ___crossPromotions_8;
	// System.String Outback.Managers.MasterConfigsManager::configFileBaseUrl
	String_t* ___configFileBaseUrl_9;
	// System.String Outback.Managers.MasterConfigsManager::assetBundleBaseUrl
	String_t* ___assetBundleBaseUrl_10;
	// System.String Outback.Managers.MasterConfigsManager::preloadersBaseUrl
	String_t* ___preloadersBaseUrl_11;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> Outback.Managers.MasterConfigsManager::COMMONS_ATLAS
	List_1_t1752731834 * ___COMMONS_ATLAS_13;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> Outback.Managers.MasterConfigsManager::LOBBY_ATLAS
	List_1_t1752731834 * ___LOBBY_ATLAS_14;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> Outback.Managers.MasterConfigsManager::LOBBY_ATLAS_FROM_AB
	List_1_t1752731834 * ___LOBBY_ATLAS_FROM_AB_15;

public:
	inline static int32_t get_offset_of_lobbyAtlasAB_3() { return static_cast<int32_t>(offsetof(MasterConfigsManager_t2615534321, ___lobbyAtlasAB_3)); }
	inline AssetBundle_t1153907252 * get_lobbyAtlasAB_3() const { return ___lobbyAtlasAB_3; }
	inline AssetBundle_t1153907252 ** get_address_of_lobbyAtlasAB_3() { return &___lobbyAtlasAB_3; }
	inline void set_lobbyAtlasAB_3(AssetBundle_t1153907252 * value)
	{
		___lobbyAtlasAB_3 = value;
		Il2CppCodeGenWriteBarrier((&___lobbyAtlasAB_3), value);
	}

	inline static int32_t get_offset_of_lobbyLayoutJson_4() { return static_cast<int32_t>(offsetof(MasterConfigsManager_t2615534321, ___lobbyLayoutJson_4)); }
	inline Dictionary_2_t2865362463 * get_lobbyLayoutJson_4() const { return ___lobbyLayoutJson_4; }
	inline Dictionary_2_t2865362463 ** get_address_of_lobbyLayoutJson_4() { return &___lobbyLayoutJson_4; }
	inline void set_lobbyLayoutJson_4(Dictionary_2_t2865362463 * value)
	{
		___lobbyLayoutJson_4 = value;
		Il2CppCodeGenWriteBarrier((&___lobbyLayoutJson_4), value);
	}

	inline static int32_t get_offset_of_levelUpRewardJson_5() { return static_cast<int32_t>(offsetof(MasterConfigsManager_t2615534321, ___levelUpRewardJson_5)); }
	inline Dictionary_2_t2865362463 * get_levelUpRewardJson_5() const { return ___levelUpRewardJson_5; }
	inline Dictionary_2_t2865362463 ** get_address_of_levelUpRewardJson_5() { return &___levelUpRewardJson_5; }
	inline void set_levelUpRewardJson_5(Dictionary_2_t2865362463 * value)
	{
		___levelUpRewardJson_5 = value;
		Il2CppCodeGenWriteBarrier((&___levelUpRewardJson_5), value);
	}

	inline static int32_t get_offset_of_masterGameConfigJson_6() { return static_cast<int32_t>(offsetof(MasterConfigsManager_t2615534321, ___masterGameConfigJson_6)); }
	inline Dictionary_2_t1632706988 * get_masterGameConfigJson_6() const { return ___masterGameConfigJson_6; }
	inline Dictionary_2_t1632706988 ** get_address_of_masterGameConfigJson_6() { return &___masterGameConfigJson_6; }
	inline void set_masterGameConfigJson_6(Dictionary_2_t1632706988 * value)
	{
		___masterGameConfigJson_6 = value;
		Il2CppCodeGenWriteBarrier((&___masterGameConfigJson_6), value);
	}

	inline static int32_t get_offset_of_storeInformationJson_7() { return static_cast<int32_t>(offsetof(MasterConfigsManager_t2615534321, ___storeInformationJson_7)); }
	inline Dictionary_2_t2865362463 * get_storeInformationJson_7() const { return ___storeInformationJson_7; }
	inline Dictionary_2_t2865362463 ** get_address_of_storeInformationJson_7() { return &___storeInformationJson_7; }
	inline void set_storeInformationJson_7(Dictionary_2_t2865362463 * value)
	{
		___storeInformationJson_7 = value;
		Il2CppCodeGenWriteBarrier((&___storeInformationJson_7), value);
	}

	inline static int32_t get_offset_of_crossPromotions_8() { return static_cast<int32_t>(offsetof(MasterConfigsManager_t2615534321, ___crossPromotions_8)); }
	inline List_1_t2205672511 * get_crossPromotions_8() const { return ___crossPromotions_8; }
	inline List_1_t2205672511 ** get_address_of_crossPromotions_8() { return &___crossPromotions_8; }
	inline void set_crossPromotions_8(List_1_t2205672511 * value)
	{
		___crossPromotions_8 = value;
		Il2CppCodeGenWriteBarrier((&___crossPromotions_8), value);
	}

	inline static int32_t get_offset_of_configFileBaseUrl_9() { return static_cast<int32_t>(offsetof(MasterConfigsManager_t2615534321, ___configFileBaseUrl_9)); }
	inline String_t* get_configFileBaseUrl_9() const { return ___configFileBaseUrl_9; }
	inline String_t** get_address_of_configFileBaseUrl_9() { return &___configFileBaseUrl_9; }
	inline void set_configFileBaseUrl_9(String_t* value)
	{
		___configFileBaseUrl_9 = value;
		Il2CppCodeGenWriteBarrier((&___configFileBaseUrl_9), value);
	}

	inline static int32_t get_offset_of_assetBundleBaseUrl_10() { return static_cast<int32_t>(offsetof(MasterConfigsManager_t2615534321, ___assetBundleBaseUrl_10)); }
	inline String_t* get_assetBundleBaseUrl_10() const { return ___assetBundleBaseUrl_10; }
	inline String_t** get_address_of_assetBundleBaseUrl_10() { return &___assetBundleBaseUrl_10; }
	inline void set_assetBundleBaseUrl_10(String_t* value)
	{
		___assetBundleBaseUrl_10 = value;
		Il2CppCodeGenWriteBarrier((&___assetBundleBaseUrl_10), value);
	}

	inline static int32_t get_offset_of_preloadersBaseUrl_11() { return static_cast<int32_t>(offsetof(MasterConfigsManager_t2615534321, ___preloadersBaseUrl_11)); }
	inline String_t* get_preloadersBaseUrl_11() const { return ___preloadersBaseUrl_11; }
	inline String_t** get_address_of_preloadersBaseUrl_11() { return &___preloadersBaseUrl_11; }
	inline void set_preloadersBaseUrl_11(String_t* value)
	{
		___preloadersBaseUrl_11 = value;
		Il2CppCodeGenWriteBarrier((&___preloadersBaseUrl_11), value);
	}

	inline static int32_t get_offset_of_COMMONS_ATLAS_13() { return static_cast<int32_t>(offsetof(MasterConfigsManager_t2615534321, ___COMMONS_ATLAS_13)); }
	inline List_1_t1752731834 * get_COMMONS_ATLAS_13() const { return ___COMMONS_ATLAS_13; }
	inline List_1_t1752731834 ** get_address_of_COMMONS_ATLAS_13() { return &___COMMONS_ATLAS_13; }
	inline void set_COMMONS_ATLAS_13(List_1_t1752731834 * value)
	{
		___COMMONS_ATLAS_13 = value;
		Il2CppCodeGenWriteBarrier((&___COMMONS_ATLAS_13), value);
	}

	inline static int32_t get_offset_of_LOBBY_ATLAS_14() { return static_cast<int32_t>(offsetof(MasterConfigsManager_t2615534321, ___LOBBY_ATLAS_14)); }
	inline List_1_t1752731834 * get_LOBBY_ATLAS_14() const { return ___LOBBY_ATLAS_14; }
	inline List_1_t1752731834 ** get_address_of_LOBBY_ATLAS_14() { return &___LOBBY_ATLAS_14; }
	inline void set_LOBBY_ATLAS_14(List_1_t1752731834 * value)
	{
		___LOBBY_ATLAS_14 = value;
		Il2CppCodeGenWriteBarrier((&___LOBBY_ATLAS_14), value);
	}

	inline static int32_t get_offset_of_LOBBY_ATLAS_FROM_AB_15() { return static_cast<int32_t>(offsetof(MasterConfigsManager_t2615534321, ___LOBBY_ATLAS_FROM_AB_15)); }
	inline List_1_t1752731834 * get_LOBBY_ATLAS_FROM_AB_15() const { return ___LOBBY_ATLAS_FROM_AB_15; }
	inline List_1_t1752731834 ** get_address_of_LOBBY_ATLAS_FROM_AB_15() { return &___LOBBY_ATLAS_FROM_AB_15; }
	inline void set_LOBBY_ATLAS_FROM_AB_15(List_1_t1752731834 * value)
	{
		___LOBBY_ATLAS_FROM_AB_15 = value;
		Il2CppCodeGenWriteBarrier((&___LOBBY_ATLAS_FROM_AB_15), value);
	}
};

struct MasterConfigsManager_t2615534321_StaticFields
{
public:
	// Outback.Managers.MasterConfigsManager Outback.Managers.MasterConfigsManager::instance
	MasterConfigsManager_t2615534321 * ___instance_2;
	// System.Single Outback.Managers.MasterConfigsManager::playerSyncRepeatRate
	float ___playerSyncRepeatRate_12;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(MasterConfigsManager_t2615534321_StaticFields, ___instance_2)); }
	inline MasterConfigsManager_t2615534321 * get_instance_2() const { return ___instance_2; }
	inline MasterConfigsManager_t2615534321 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(MasterConfigsManager_t2615534321 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}

	inline static int32_t get_offset_of_playerSyncRepeatRate_12() { return static_cast<int32_t>(offsetof(MasterConfigsManager_t2615534321_StaticFields, ___playerSyncRepeatRate_12)); }
	inline float get_playerSyncRepeatRate_12() const { return ___playerSyncRepeatRate_12; }
	inline float* get_address_of_playerSyncRepeatRate_12() { return &___playerSyncRepeatRate_12; }
	inline void set_playerSyncRepeatRate_12(float value)
	{
		___playerSyncRepeatRate_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASTERCONFIGSMANAGER_T2615534321_H
#ifndef BETMANAGER_T1302012615_H
#define BETMANAGER_T1302012615_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Managers.BetManager
struct  BetManager_t1302012615  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Button Outback.Managers.BetManager::incrementButton
	Button_t4055032469 * ___incrementButton_2;
	// UnityEngine.UI.Button Outback.Managers.BetManager::decrementButton
	Button_t4055032469 * ___decrementButton_3;
	// System.Int32 Outback.Managers.BetManager::minBet
	int32_t ___minBet_5;
	// System.Int32 Outback.Managers.BetManager::maxBet
	int32_t ___maxBet_6;
	// UnityEngine.UI.Button Outback.Managers.BetManager::AddPlusButton
	Button_t4055032469 * ___AddPlusButton_7;

public:
	inline static int32_t get_offset_of_incrementButton_2() { return static_cast<int32_t>(offsetof(BetManager_t1302012615, ___incrementButton_2)); }
	inline Button_t4055032469 * get_incrementButton_2() const { return ___incrementButton_2; }
	inline Button_t4055032469 ** get_address_of_incrementButton_2() { return &___incrementButton_2; }
	inline void set_incrementButton_2(Button_t4055032469 * value)
	{
		___incrementButton_2 = value;
		Il2CppCodeGenWriteBarrier((&___incrementButton_2), value);
	}

	inline static int32_t get_offset_of_decrementButton_3() { return static_cast<int32_t>(offsetof(BetManager_t1302012615, ___decrementButton_3)); }
	inline Button_t4055032469 * get_decrementButton_3() const { return ___decrementButton_3; }
	inline Button_t4055032469 ** get_address_of_decrementButton_3() { return &___decrementButton_3; }
	inline void set_decrementButton_3(Button_t4055032469 * value)
	{
		___decrementButton_3 = value;
		Il2CppCodeGenWriteBarrier((&___decrementButton_3), value);
	}

	inline static int32_t get_offset_of_minBet_5() { return static_cast<int32_t>(offsetof(BetManager_t1302012615, ___minBet_5)); }
	inline int32_t get_minBet_5() const { return ___minBet_5; }
	inline int32_t* get_address_of_minBet_5() { return &___minBet_5; }
	inline void set_minBet_5(int32_t value)
	{
		___minBet_5 = value;
	}

	inline static int32_t get_offset_of_maxBet_6() { return static_cast<int32_t>(offsetof(BetManager_t1302012615, ___maxBet_6)); }
	inline int32_t get_maxBet_6() const { return ___maxBet_6; }
	inline int32_t* get_address_of_maxBet_6() { return &___maxBet_6; }
	inline void set_maxBet_6(int32_t value)
	{
		___maxBet_6 = value;
	}

	inline static int32_t get_offset_of_AddPlusButton_7() { return static_cast<int32_t>(offsetof(BetManager_t1302012615, ___AddPlusButton_7)); }
	inline Button_t4055032469 * get_AddPlusButton_7() const { return ___AddPlusButton_7; }
	inline Button_t4055032469 ** get_address_of_AddPlusButton_7() { return &___AddPlusButton_7; }
	inline void set_AddPlusButton_7(Button_t4055032469 * value)
	{
		___AddPlusButton_7 = value;
		Il2CppCodeGenWriteBarrier((&___AddPlusButton_7), value);
	}
};

struct BetManager_t1302012615_StaticFields
{
public:
	// System.Int64 Outback.Managers.BetManager::totalBet
	int64_t ___totalBet_4;
	// System.Int64 Outback.Managers.BetManager::xc
	int64_t ___xc_8;

public:
	inline static int32_t get_offset_of_totalBet_4() { return static_cast<int32_t>(offsetof(BetManager_t1302012615_StaticFields, ___totalBet_4)); }
	inline int64_t get_totalBet_4() const { return ___totalBet_4; }
	inline int64_t* get_address_of_totalBet_4() { return &___totalBet_4; }
	inline void set_totalBet_4(int64_t value)
	{
		___totalBet_4 = value;
	}

	inline static int32_t get_offset_of_xc_8() { return static_cast<int32_t>(offsetof(BetManager_t1302012615_StaticFields, ___xc_8)); }
	inline int64_t get_xc_8() const { return ___xc_8; }
	inline int64_t* get_address_of_xc_8() { return &___xc_8; }
	inline void set_xc_8(int64_t value)
	{
		___xc_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BETMANAGER_T1302012615_H
#ifndef LOGINSCREENCONTROLLER_T2093126341_H
#define LOGINSCREENCONTROLLER_T2093126341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoginScreenController
struct  LoginScreenController_t2093126341  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.GameObject LoginScreenController::detailsPanel
	GameObject_t1113636619 * ___detailsPanel_3;
	// UnityEngine.GameObject LoginScreenController::guestButton
	GameObject_t1113636619 * ___guestButton_4;
	// UnityEngine.GameObject LoginScreenController::fbLoginButton
	GameObject_t1113636619 * ___fbLoginButton_5;
	// UnityEngine.UI.InputField LoginScreenController::email
	InputField_t3762917431 * ___email_6;
	// UnityEngine.UI.InputField LoginScreenController::password
	InputField_t3762917431 * ___password_7;

public:
	inline static int32_t get_offset_of_detailsPanel_3() { return static_cast<int32_t>(offsetof(LoginScreenController_t2093126341, ___detailsPanel_3)); }
	inline GameObject_t1113636619 * get_detailsPanel_3() const { return ___detailsPanel_3; }
	inline GameObject_t1113636619 ** get_address_of_detailsPanel_3() { return &___detailsPanel_3; }
	inline void set_detailsPanel_3(GameObject_t1113636619 * value)
	{
		___detailsPanel_3 = value;
		Il2CppCodeGenWriteBarrier((&___detailsPanel_3), value);
	}

	inline static int32_t get_offset_of_guestButton_4() { return static_cast<int32_t>(offsetof(LoginScreenController_t2093126341, ___guestButton_4)); }
	inline GameObject_t1113636619 * get_guestButton_4() const { return ___guestButton_4; }
	inline GameObject_t1113636619 ** get_address_of_guestButton_4() { return &___guestButton_4; }
	inline void set_guestButton_4(GameObject_t1113636619 * value)
	{
		___guestButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___guestButton_4), value);
	}

	inline static int32_t get_offset_of_fbLoginButton_5() { return static_cast<int32_t>(offsetof(LoginScreenController_t2093126341, ___fbLoginButton_5)); }
	inline GameObject_t1113636619 * get_fbLoginButton_5() const { return ___fbLoginButton_5; }
	inline GameObject_t1113636619 ** get_address_of_fbLoginButton_5() { return &___fbLoginButton_5; }
	inline void set_fbLoginButton_5(GameObject_t1113636619 * value)
	{
		___fbLoginButton_5 = value;
		Il2CppCodeGenWriteBarrier((&___fbLoginButton_5), value);
	}

	inline static int32_t get_offset_of_email_6() { return static_cast<int32_t>(offsetof(LoginScreenController_t2093126341, ___email_6)); }
	inline InputField_t3762917431 * get_email_6() const { return ___email_6; }
	inline InputField_t3762917431 ** get_address_of_email_6() { return &___email_6; }
	inline void set_email_6(InputField_t3762917431 * value)
	{
		___email_6 = value;
		Il2CppCodeGenWriteBarrier((&___email_6), value);
	}

	inline static int32_t get_offset_of_password_7() { return static_cast<int32_t>(offsetof(LoginScreenController_t2093126341, ___password_7)); }
	inline InputField_t3762917431 * get_password_7() const { return ___password_7; }
	inline InputField_t3762917431 ** get_address_of_password_7() { return &___password_7; }
	inline void set_password_7(InputField_t3762917431 * value)
	{
		___password_7 = value;
		Il2CppCodeGenWriteBarrier((&___password_7), value);
	}
};

struct LoginScreenController_t2093126341_StaticFields
{
public:
	// LoginScreenController LoginScreenController::instance
	LoginScreenController_t2093126341 * ___instance_2;

public:
	inline static int32_t get_offset_of_instance_2() { return static_cast<int32_t>(offsetof(LoginScreenController_t2093126341_StaticFields, ___instance_2)); }
	inline LoginScreenController_t2093126341 * get_instance_2() const { return ___instance_2; }
	inline LoginScreenController_t2093126341 ** get_address_of_instance_2() { return &___instance_2; }
	inline void set_instance_2(LoginScreenController_t2093126341 * value)
	{
		___instance_2 = value;
		Il2CppCodeGenWriteBarrier((&___instance_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGINSCREENCONTROLLER_T2093126341_H
#ifndef INAPPPURCHASE_T1703437824_H
#define INAPPPURCHASE_T1703437824_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// InAppPurchase
struct  InAppPurchase_t1703437824  : public MonoBehaviour_t3962482529
{
public:
	// Sdkbox.IAP InAppPurchase::_iap
	IAP_t1939458538 * ____iap_2;

public:
	inline static int32_t get_offset_of__iap_2() { return static_cast<int32_t>(offsetof(InAppPurchase_t1703437824, ____iap_2)); }
	inline IAP_t1939458538 * get__iap_2() const { return ____iap_2; }
	inline IAP_t1939458538 ** get_address_of__iap_2() { return &____iap_2; }
	inline void set__iap_2(IAP_t1939458538 * value)
	{
		____iap_2 = value;
		Il2CppCodeGenWriteBarrier((&____iap_2), value);
	}
};

struct InAppPurchase_t1703437824_StaticFields
{
public:
	// System.String InAppPurchase::Product_10K
	String_t* ___Product_10K_3;
	// System.String InAppPurchase::Product_25K
	String_t* ___Product_25K_4;
	// System.String InAppPurchase::Product_50K
	String_t* ___Product_50K_5;
	// System.String InAppPurchase::Product_75K
	String_t* ___Product_75K_6;
	// System.String InAppPurchase::Product_100K
	String_t* ___Product_100K_7;
	// System.String InAppPurchase::Product_500K
	String_t* ___Product_500K_8;
	// System.String InAppPurchase::Product_1M
	String_t* ___Product_1M_9;
	// System.String InAppPurchase::Product_5M
	String_t* ___Product_5M_10;
	// System.String InAppPurchase::Product_10M
	String_t* ___Product_10M_11;

public:
	inline static int32_t get_offset_of_Product_10K_3() { return static_cast<int32_t>(offsetof(InAppPurchase_t1703437824_StaticFields, ___Product_10K_3)); }
	inline String_t* get_Product_10K_3() const { return ___Product_10K_3; }
	inline String_t** get_address_of_Product_10K_3() { return &___Product_10K_3; }
	inline void set_Product_10K_3(String_t* value)
	{
		___Product_10K_3 = value;
		Il2CppCodeGenWriteBarrier((&___Product_10K_3), value);
	}

	inline static int32_t get_offset_of_Product_25K_4() { return static_cast<int32_t>(offsetof(InAppPurchase_t1703437824_StaticFields, ___Product_25K_4)); }
	inline String_t* get_Product_25K_4() const { return ___Product_25K_4; }
	inline String_t** get_address_of_Product_25K_4() { return &___Product_25K_4; }
	inline void set_Product_25K_4(String_t* value)
	{
		___Product_25K_4 = value;
		Il2CppCodeGenWriteBarrier((&___Product_25K_4), value);
	}

	inline static int32_t get_offset_of_Product_50K_5() { return static_cast<int32_t>(offsetof(InAppPurchase_t1703437824_StaticFields, ___Product_50K_5)); }
	inline String_t* get_Product_50K_5() const { return ___Product_50K_5; }
	inline String_t** get_address_of_Product_50K_5() { return &___Product_50K_5; }
	inline void set_Product_50K_5(String_t* value)
	{
		___Product_50K_5 = value;
		Il2CppCodeGenWriteBarrier((&___Product_50K_5), value);
	}

	inline static int32_t get_offset_of_Product_75K_6() { return static_cast<int32_t>(offsetof(InAppPurchase_t1703437824_StaticFields, ___Product_75K_6)); }
	inline String_t* get_Product_75K_6() const { return ___Product_75K_6; }
	inline String_t** get_address_of_Product_75K_6() { return &___Product_75K_6; }
	inline void set_Product_75K_6(String_t* value)
	{
		___Product_75K_6 = value;
		Il2CppCodeGenWriteBarrier((&___Product_75K_6), value);
	}

	inline static int32_t get_offset_of_Product_100K_7() { return static_cast<int32_t>(offsetof(InAppPurchase_t1703437824_StaticFields, ___Product_100K_7)); }
	inline String_t* get_Product_100K_7() const { return ___Product_100K_7; }
	inline String_t** get_address_of_Product_100K_7() { return &___Product_100K_7; }
	inline void set_Product_100K_7(String_t* value)
	{
		___Product_100K_7 = value;
		Il2CppCodeGenWriteBarrier((&___Product_100K_7), value);
	}

	inline static int32_t get_offset_of_Product_500K_8() { return static_cast<int32_t>(offsetof(InAppPurchase_t1703437824_StaticFields, ___Product_500K_8)); }
	inline String_t* get_Product_500K_8() const { return ___Product_500K_8; }
	inline String_t** get_address_of_Product_500K_8() { return &___Product_500K_8; }
	inline void set_Product_500K_8(String_t* value)
	{
		___Product_500K_8 = value;
		Il2CppCodeGenWriteBarrier((&___Product_500K_8), value);
	}

	inline static int32_t get_offset_of_Product_1M_9() { return static_cast<int32_t>(offsetof(InAppPurchase_t1703437824_StaticFields, ___Product_1M_9)); }
	inline String_t* get_Product_1M_9() const { return ___Product_1M_9; }
	inline String_t** get_address_of_Product_1M_9() { return &___Product_1M_9; }
	inline void set_Product_1M_9(String_t* value)
	{
		___Product_1M_9 = value;
		Il2CppCodeGenWriteBarrier((&___Product_1M_9), value);
	}

	inline static int32_t get_offset_of_Product_5M_10() { return static_cast<int32_t>(offsetof(InAppPurchase_t1703437824_StaticFields, ___Product_5M_10)); }
	inline String_t* get_Product_5M_10() const { return ___Product_5M_10; }
	inline String_t** get_address_of_Product_5M_10() { return &___Product_5M_10; }
	inline void set_Product_5M_10(String_t* value)
	{
		___Product_5M_10 = value;
		Il2CppCodeGenWriteBarrier((&___Product_5M_10), value);
	}

	inline static int32_t get_offset_of_Product_10M_11() { return static_cast<int32_t>(offsetof(InAppPurchase_t1703437824_StaticFields, ___Product_10M_11)); }
	inline String_t* get_Product_10M_11() const { return ___Product_10M_11; }
	inline String_t** get_address_of_Product_10M_11() { return &___Product_10M_11; }
	inline void set_Product_10M_11(String_t* value)
	{
		___Product_10M_11 = value;
		Il2CppCodeGenWriteBarrier((&___Product_10M_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INAPPPURCHASE_T1703437824_H
#ifndef LAUNCHIMAGE_T1920275599_H
#define LAUNCHIMAGE_T1920275599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// launchImage
struct  launchImage_t1920275599  : public MonoBehaviour_t3962482529
{
public:
	// System.Single launchImage::delayTime
	float ___delayTime_2;

public:
	inline static int32_t get_offset_of_delayTime_2() { return static_cast<int32_t>(offsetof(launchImage_t1920275599, ___delayTime_2)); }
	inline float get_delayTime_2() const { return ___delayTime_2; }
	inline float* get_address_of_delayTime_2() { return &___delayTime_2; }
	inline void set_delayTime_2(float value)
	{
		___delayTime_2 = value;
	}
};

struct launchImage_t1920275599_StaticFields
{
public:
	// System.String launchImage::constantvaluestored
	String_t* ___constantvaluestored_3;

public:
	inline static int32_t get_offset_of_constantvaluestored_3() { return static_cast<int32_t>(offsetof(launchImage_t1920275599_StaticFields, ___constantvaluestored_3)); }
	inline String_t* get_constantvaluestored_3() const { return ___constantvaluestored_3; }
	inline String_t** get_address_of_constantvaluestored_3() { return &___constantvaluestored_3; }
	inline void set_constantvaluestored_3(String_t* value)
	{
		___constantvaluestored_3 = value;
		Il2CppCodeGenWriteBarrier((&___constantvaluestored_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAUNCHIMAGE_T1920275599_H
#ifndef SLOTGAMEMANAGER_T584614006_H
#define SLOTGAMEMANAGER_T584614006_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Managers.SlotGameManager
struct  SlotGameManager_t584614006  : public MonoBehaviour_t3962482529
{
public:
	// System.String Outback.Managers.SlotGameManager::scatter
	String_t* ___scatter_2;
	// System.String Outback.Managers.SlotGameManager::wild
	String_t* ___wild_3;
	// System.String Outback.Managers.SlotGameManager::bonus
	String_t* ___bonus_4;
	// UnityEngine.UI.Text Outback.Managers.SlotGameManager::credits
	Text_t1901882714 * ___credits_5;
	// System.String Outback.Managers.SlotGameManager::gameName
	String_t* ___gameName_6;
	// System.Collections.Generic.List`1<UnityEngine.Sprite> Outback.Managers.SlotGameManager::sprites
	List_1_t1752731834 * ___sprites_7;
	// System.Int32 Outback.Managers.SlotGameManager::spriteCountForThisGame
	int32_t ___spriteCountForThisGame_8;
	// System.Int32 Outback.Managers.SlotGameManager::totalWild
	int32_t ___totalWild_9;
	// System.Int32 Outback.Managers.SlotGameManager::totalScatter
	int32_t ___totalScatter_10;
	// System.Int32 Outback.Managers.SlotGameManager::totalBonus
	int32_t ___totalBonus_11;
	// System.Collections.Generic.List`1<SlotReel> Outback.Managers.SlotGameManager::slotReels
	List_1_t3396113631 * ___slotReels_12;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> Outback.Managers.SlotGameManager::scatters
	List_1_t4142344393 * ___scatters_14;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> Outback.Managers.SlotGameManager::wilds
	List_1_t4142344393 * ___wilds_15;
	// System.Collections.Generic.List`1<UnityEngine.UI.Image> Outback.Managers.SlotGameManager::bonuses
	List_1_t4142344393 * ___bonuses_16;
	// System.Collections.Generic.List`1<System.Int32> Outback.Managers.SlotGameManager::scatterReels
	List_1_t128053199 * ___scatterReels_17;
	// System.Collections.Generic.List`1<System.Int32> Outback.Managers.SlotGameManager::wildReels
	List_1_t128053199 * ___wildReels_18;
	// System.Collections.Generic.List`1<System.Int32> Outback.Managers.SlotGameManager::bonusReels
	List_1_t128053199 * ___bonusReels_19;
	// System.Boolean Outback.Managers.SlotGameManager::stopInstantly
	bool ___stopInstantly_21;
	// System.Boolean Outback.Managers.SlotGameManager::canStopReelsInstanty
	bool ___canStopReelsInstanty_22;
	// System.Int32 Outback.Managers.SlotGameManager::numberOfFreeSpins
	int32_t ___numberOfFreeSpins_23;
	// System.Collections.Generic.List`1<UnityEngine.UI.Button> Outback.Managers.SlotGameManager::buttonsToDisableWhileSpinning
	List_1_t1232139915 * ___buttonsToDisableWhileSpinning_24;
	// System.Boolean Outback.Managers.SlotGameManager::spinBtnPressed
	bool ___spinBtnPressed_25;
	// System.Boolean Outback.Managers.SlotGameManager::stopBtnPressed
	bool ___stopBtnPressed_26;
	// System.Collections.Generic.List`1<System.Int64> Outback.Managers.SlotGameManager::baseBonusRewards
	List_1_t913674750 * ___baseBonusRewards_27;
	// DG.Tweening.Tweener Outback.Managers.SlotGameManager::calculateStoppingTime
	Tweener_t436044680 * ___calculateStoppingTime_28;
	// System.Boolean Outback.Managers.SlotGameManager::isDown
	bool ___isDown_29;
	// System.Single Outback.Managers.SlotGameManager::downTime
	float ___downTime_30;
	// System.Boolean Outback.Managers.SlotGameManager::autoSpinning
	bool ___autoSpinning_31;
	// System.Single Outback.Managers.SlotGameManager::speedOfSlots
	float ___speedOfSlots_32;

public:
	inline static int32_t get_offset_of_scatter_2() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___scatter_2)); }
	inline String_t* get_scatter_2() const { return ___scatter_2; }
	inline String_t** get_address_of_scatter_2() { return &___scatter_2; }
	inline void set_scatter_2(String_t* value)
	{
		___scatter_2 = value;
		Il2CppCodeGenWriteBarrier((&___scatter_2), value);
	}

	inline static int32_t get_offset_of_wild_3() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___wild_3)); }
	inline String_t* get_wild_3() const { return ___wild_3; }
	inline String_t** get_address_of_wild_3() { return &___wild_3; }
	inline void set_wild_3(String_t* value)
	{
		___wild_3 = value;
		Il2CppCodeGenWriteBarrier((&___wild_3), value);
	}

	inline static int32_t get_offset_of_bonus_4() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___bonus_4)); }
	inline String_t* get_bonus_4() const { return ___bonus_4; }
	inline String_t** get_address_of_bonus_4() { return &___bonus_4; }
	inline void set_bonus_4(String_t* value)
	{
		___bonus_4 = value;
		Il2CppCodeGenWriteBarrier((&___bonus_4), value);
	}

	inline static int32_t get_offset_of_credits_5() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___credits_5)); }
	inline Text_t1901882714 * get_credits_5() const { return ___credits_5; }
	inline Text_t1901882714 ** get_address_of_credits_5() { return &___credits_5; }
	inline void set_credits_5(Text_t1901882714 * value)
	{
		___credits_5 = value;
		Il2CppCodeGenWriteBarrier((&___credits_5), value);
	}

	inline static int32_t get_offset_of_gameName_6() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___gameName_6)); }
	inline String_t* get_gameName_6() const { return ___gameName_6; }
	inline String_t** get_address_of_gameName_6() { return &___gameName_6; }
	inline void set_gameName_6(String_t* value)
	{
		___gameName_6 = value;
		Il2CppCodeGenWriteBarrier((&___gameName_6), value);
	}

	inline static int32_t get_offset_of_sprites_7() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___sprites_7)); }
	inline List_1_t1752731834 * get_sprites_7() const { return ___sprites_7; }
	inline List_1_t1752731834 ** get_address_of_sprites_7() { return &___sprites_7; }
	inline void set_sprites_7(List_1_t1752731834 * value)
	{
		___sprites_7 = value;
		Il2CppCodeGenWriteBarrier((&___sprites_7), value);
	}

	inline static int32_t get_offset_of_spriteCountForThisGame_8() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___spriteCountForThisGame_8)); }
	inline int32_t get_spriteCountForThisGame_8() const { return ___spriteCountForThisGame_8; }
	inline int32_t* get_address_of_spriteCountForThisGame_8() { return &___spriteCountForThisGame_8; }
	inline void set_spriteCountForThisGame_8(int32_t value)
	{
		___spriteCountForThisGame_8 = value;
	}

	inline static int32_t get_offset_of_totalWild_9() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___totalWild_9)); }
	inline int32_t get_totalWild_9() const { return ___totalWild_9; }
	inline int32_t* get_address_of_totalWild_9() { return &___totalWild_9; }
	inline void set_totalWild_9(int32_t value)
	{
		___totalWild_9 = value;
	}

	inline static int32_t get_offset_of_totalScatter_10() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___totalScatter_10)); }
	inline int32_t get_totalScatter_10() const { return ___totalScatter_10; }
	inline int32_t* get_address_of_totalScatter_10() { return &___totalScatter_10; }
	inline void set_totalScatter_10(int32_t value)
	{
		___totalScatter_10 = value;
	}

	inline static int32_t get_offset_of_totalBonus_11() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___totalBonus_11)); }
	inline int32_t get_totalBonus_11() const { return ___totalBonus_11; }
	inline int32_t* get_address_of_totalBonus_11() { return &___totalBonus_11; }
	inline void set_totalBonus_11(int32_t value)
	{
		___totalBonus_11 = value;
	}

	inline static int32_t get_offset_of_slotReels_12() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___slotReels_12)); }
	inline List_1_t3396113631 * get_slotReels_12() const { return ___slotReels_12; }
	inline List_1_t3396113631 ** get_address_of_slotReels_12() { return &___slotReels_12; }
	inline void set_slotReels_12(List_1_t3396113631 * value)
	{
		___slotReels_12 = value;
		Il2CppCodeGenWriteBarrier((&___slotReels_12), value);
	}

	inline static int32_t get_offset_of_scatters_14() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___scatters_14)); }
	inline List_1_t4142344393 * get_scatters_14() const { return ___scatters_14; }
	inline List_1_t4142344393 ** get_address_of_scatters_14() { return &___scatters_14; }
	inline void set_scatters_14(List_1_t4142344393 * value)
	{
		___scatters_14 = value;
		Il2CppCodeGenWriteBarrier((&___scatters_14), value);
	}

	inline static int32_t get_offset_of_wilds_15() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___wilds_15)); }
	inline List_1_t4142344393 * get_wilds_15() const { return ___wilds_15; }
	inline List_1_t4142344393 ** get_address_of_wilds_15() { return &___wilds_15; }
	inline void set_wilds_15(List_1_t4142344393 * value)
	{
		___wilds_15 = value;
		Il2CppCodeGenWriteBarrier((&___wilds_15), value);
	}

	inline static int32_t get_offset_of_bonuses_16() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___bonuses_16)); }
	inline List_1_t4142344393 * get_bonuses_16() const { return ___bonuses_16; }
	inline List_1_t4142344393 ** get_address_of_bonuses_16() { return &___bonuses_16; }
	inline void set_bonuses_16(List_1_t4142344393 * value)
	{
		___bonuses_16 = value;
		Il2CppCodeGenWriteBarrier((&___bonuses_16), value);
	}

	inline static int32_t get_offset_of_scatterReels_17() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___scatterReels_17)); }
	inline List_1_t128053199 * get_scatterReels_17() const { return ___scatterReels_17; }
	inline List_1_t128053199 ** get_address_of_scatterReels_17() { return &___scatterReels_17; }
	inline void set_scatterReels_17(List_1_t128053199 * value)
	{
		___scatterReels_17 = value;
		Il2CppCodeGenWriteBarrier((&___scatterReels_17), value);
	}

	inline static int32_t get_offset_of_wildReels_18() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___wildReels_18)); }
	inline List_1_t128053199 * get_wildReels_18() const { return ___wildReels_18; }
	inline List_1_t128053199 ** get_address_of_wildReels_18() { return &___wildReels_18; }
	inline void set_wildReels_18(List_1_t128053199 * value)
	{
		___wildReels_18 = value;
		Il2CppCodeGenWriteBarrier((&___wildReels_18), value);
	}

	inline static int32_t get_offset_of_bonusReels_19() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___bonusReels_19)); }
	inline List_1_t128053199 * get_bonusReels_19() const { return ___bonusReels_19; }
	inline List_1_t128053199 ** get_address_of_bonusReels_19() { return &___bonusReels_19; }
	inline void set_bonusReels_19(List_1_t128053199 * value)
	{
		___bonusReels_19 = value;
		Il2CppCodeGenWriteBarrier((&___bonusReels_19), value);
	}

	inline static int32_t get_offset_of_stopInstantly_21() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___stopInstantly_21)); }
	inline bool get_stopInstantly_21() const { return ___stopInstantly_21; }
	inline bool* get_address_of_stopInstantly_21() { return &___stopInstantly_21; }
	inline void set_stopInstantly_21(bool value)
	{
		___stopInstantly_21 = value;
	}

	inline static int32_t get_offset_of_canStopReelsInstanty_22() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___canStopReelsInstanty_22)); }
	inline bool get_canStopReelsInstanty_22() const { return ___canStopReelsInstanty_22; }
	inline bool* get_address_of_canStopReelsInstanty_22() { return &___canStopReelsInstanty_22; }
	inline void set_canStopReelsInstanty_22(bool value)
	{
		___canStopReelsInstanty_22 = value;
	}

	inline static int32_t get_offset_of_numberOfFreeSpins_23() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___numberOfFreeSpins_23)); }
	inline int32_t get_numberOfFreeSpins_23() const { return ___numberOfFreeSpins_23; }
	inline int32_t* get_address_of_numberOfFreeSpins_23() { return &___numberOfFreeSpins_23; }
	inline void set_numberOfFreeSpins_23(int32_t value)
	{
		___numberOfFreeSpins_23 = value;
	}

	inline static int32_t get_offset_of_buttonsToDisableWhileSpinning_24() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___buttonsToDisableWhileSpinning_24)); }
	inline List_1_t1232139915 * get_buttonsToDisableWhileSpinning_24() const { return ___buttonsToDisableWhileSpinning_24; }
	inline List_1_t1232139915 ** get_address_of_buttonsToDisableWhileSpinning_24() { return &___buttonsToDisableWhileSpinning_24; }
	inline void set_buttonsToDisableWhileSpinning_24(List_1_t1232139915 * value)
	{
		___buttonsToDisableWhileSpinning_24 = value;
		Il2CppCodeGenWriteBarrier((&___buttonsToDisableWhileSpinning_24), value);
	}

	inline static int32_t get_offset_of_spinBtnPressed_25() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___spinBtnPressed_25)); }
	inline bool get_spinBtnPressed_25() const { return ___spinBtnPressed_25; }
	inline bool* get_address_of_spinBtnPressed_25() { return &___spinBtnPressed_25; }
	inline void set_spinBtnPressed_25(bool value)
	{
		___spinBtnPressed_25 = value;
	}

	inline static int32_t get_offset_of_stopBtnPressed_26() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___stopBtnPressed_26)); }
	inline bool get_stopBtnPressed_26() const { return ___stopBtnPressed_26; }
	inline bool* get_address_of_stopBtnPressed_26() { return &___stopBtnPressed_26; }
	inline void set_stopBtnPressed_26(bool value)
	{
		___stopBtnPressed_26 = value;
	}

	inline static int32_t get_offset_of_baseBonusRewards_27() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___baseBonusRewards_27)); }
	inline List_1_t913674750 * get_baseBonusRewards_27() const { return ___baseBonusRewards_27; }
	inline List_1_t913674750 ** get_address_of_baseBonusRewards_27() { return &___baseBonusRewards_27; }
	inline void set_baseBonusRewards_27(List_1_t913674750 * value)
	{
		___baseBonusRewards_27 = value;
		Il2CppCodeGenWriteBarrier((&___baseBonusRewards_27), value);
	}

	inline static int32_t get_offset_of_calculateStoppingTime_28() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___calculateStoppingTime_28)); }
	inline Tweener_t436044680 * get_calculateStoppingTime_28() const { return ___calculateStoppingTime_28; }
	inline Tweener_t436044680 ** get_address_of_calculateStoppingTime_28() { return &___calculateStoppingTime_28; }
	inline void set_calculateStoppingTime_28(Tweener_t436044680 * value)
	{
		___calculateStoppingTime_28 = value;
		Il2CppCodeGenWriteBarrier((&___calculateStoppingTime_28), value);
	}

	inline static int32_t get_offset_of_isDown_29() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___isDown_29)); }
	inline bool get_isDown_29() const { return ___isDown_29; }
	inline bool* get_address_of_isDown_29() { return &___isDown_29; }
	inline void set_isDown_29(bool value)
	{
		___isDown_29 = value;
	}

	inline static int32_t get_offset_of_downTime_30() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___downTime_30)); }
	inline float get_downTime_30() const { return ___downTime_30; }
	inline float* get_address_of_downTime_30() { return &___downTime_30; }
	inline void set_downTime_30(float value)
	{
		___downTime_30 = value;
	}

	inline static int32_t get_offset_of_autoSpinning_31() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___autoSpinning_31)); }
	inline bool get_autoSpinning_31() const { return ___autoSpinning_31; }
	inline bool* get_address_of_autoSpinning_31() { return &___autoSpinning_31; }
	inline void set_autoSpinning_31(bool value)
	{
		___autoSpinning_31 = value;
	}

	inline static int32_t get_offset_of_speedOfSlots_32() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006, ___speedOfSlots_32)); }
	inline float get_speedOfSlots_32() const { return ___speedOfSlots_32; }
	inline float* get_address_of_speedOfSlots_32() { return &___speedOfSlots_32; }
	inline void set_speedOfSlots_32(float value)
	{
		___speedOfSlots_32 = value;
	}
};

struct SlotGameManager_t584614006_StaticFields
{
public:
	// BaseGame Outback.Managers.SlotGameManager::baseGame
	BaseGame_t2336638441 * ___baseGame_13;
	// Outback.Managers.SlotGameManager Outback.Managers.SlotGameManager::instance
	SlotGameManager_t584614006 * ___instance_20;

public:
	inline static int32_t get_offset_of_baseGame_13() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006_StaticFields, ___baseGame_13)); }
	inline BaseGame_t2336638441 * get_baseGame_13() const { return ___baseGame_13; }
	inline BaseGame_t2336638441 ** get_address_of_baseGame_13() { return &___baseGame_13; }
	inline void set_baseGame_13(BaseGame_t2336638441 * value)
	{
		___baseGame_13 = value;
		Il2CppCodeGenWriteBarrier((&___baseGame_13), value);
	}

	inline static int32_t get_offset_of_instance_20() { return static_cast<int32_t>(offsetof(SlotGameManager_t584614006_StaticFields, ___instance_20)); }
	inline SlotGameManager_t584614006 * get_instance_20() const { return ___instance_20; }
	inline SlotGameManager_t584614006 ** get_address_of_instance_20() { return &___instance_20; }
	inline void set_instance_20(SlotGameManager_t584614006 * value)
	{
		___instance_20 = value;
		Il2CppCodeGenWriteBarrier((&___instance_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTGAMEMANAGER_T584614006_H
#ifndef REWARD_T2838753371_H
#define REWARD_T2838753371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Core.Reward
struct  Reward_t2838753371  : public MonoBehaviour_t3962482529
{
public:
	// System.String Outback.Core.Reward::reward
	String_t* ___reward_2;
	// System.Int32 Outback.Core.Reward::rewardMultiplier
	int32_t ___rewardMultiplier_3;
	// System.Int64 Outback.Core.Reward::bonusReward
	int64_t ___bonusReward_4;
	// UnityEngine.Sprite Outback.Core.Reward::twoTimes
	Sprite_t280657092 * ___twoTimes_5;
	// UnityEngine.Sprite Outback.Core.Reward::threeTimes
	Sprite_t280657092 * ___threeTimes_6;
	// UnityEngine.Sprite Outback.Core.Reward::fiveTimes
	Sprite_t280657092 * ___fiveTimes_7;
	// System.Int32 Outback.Core.Reward::rewardNumber
	int32_t ___rewardNumber_8;
	// System.Collections.Generic.List`1<System.Int64> Outback.Core.Reward::bonusRewards
	List_1_t913674750 * ___bonusRewards_9;
	// UnityEngine.UI.Text Outback.Core.Reward::rewardAmount
	Text_t1901882714 * ___rewardAmount_10;
	// UnityEngine.Vector3 Outback.Core.Reward::rewardPos
	Vector3_t3722313464  ___rewardPos_11;
	// System.Boolean Outback.Core.Reward::threeMultipliers
	bool ___threeMultipliers_12;
	// System.Boolean Outback.Core.Reward::twoMultipliers
	bool ___twoMultipliers_13;
	// UnityEngine.Sprite Outback.Core.Reward::originalImage
	Sprite_t280657092 * ___originalImage_14;
	// UnityEngine.GameObject Outback.Core.Reward::rewardTextPosRef
	GameObject_t1113636619 * ___rewardTextPosRef_15;

public:
	inline static int32_t get_offset_of_reward_2() { return static_cast<int32_t>(offsetof(Reward_t2838753371, ___reward_2)); }
	inline String_t* get_reward_2() const { return ___reward_2; }
	inline String_t** get_address_of_reward_2() { return &___reward_2; }
	inline void set_reward_2(String_t* value)
	{
		___reward_2 = value;
		Il2CppCodeGenWriteBarrier((&___reward_2), value);
	}

	inline static int32_t get_offset_of_rewardMultiplier_3() { return static_cast<int32_t>(offsetof(Reward_t2838753371, ___rewardMultiplier_3)); }
	inline int32_t get_rewardMultiplier_3() const { return ___rewardMultiplier_3; }
	inline int32_t* get_address_of_rewardMultiplier_3() { return &___rewardMultiplier_3; }
	inline void set_rewardMultiplier_3(int32_t value)
	{
		___rewardMultiplier_3 = value;
	}

	inline static int32_t get_offset_of_bonusReward_4() { return static_cast<int32_t>(offsetof(Reward_t2838753371, ___bonusReward_4)); }
	inline int64_t get_bonusReward_4() const { return ___bonusReward_4; }
	inline int64_t* get_address_of_bonusReward_4() { return &___bonusReward_4; }
	inline void set_bonusReward_4(int64_t value)
	{
		___bonusReward_4 = value;
	}

	inline static int32_t get_offset_of_twoTimes_5() { return static_cast<int32_t>(offsetof(Reward_t2838753371, ___twoTimes_5)); }
	inline Sprite_t280657092 * get_twoTimes_5() const { return ___twoTimes_5; }
	inline Sprite_t280657092 ** get_address_of_twoTimes_5() { return &___twoTimes_5; }
	inline void set_twoTimes_5(Sprite_t280657092 * value)
	{
		___twoTimes_5 = value;
		Il2CppCodeGenWriteBarrier((&___twoTimes_5), value);
	}

	inline static int32_t get_offset_of_threeTimes_6() { return static_cast<int32_t>(offsetof(Reward_t2838753371, ___threeTimes_6)); }
	inline Sprite_t280657092 * get_threeTimes_6() const { return ___threeTimes_6; }
	inline Sprite_t280657092 ** get_address_of_threeTimes_6() { return &___threeTimes_6; }
	inline void set_threeTimes_6(Sprite_t280657092 * value)
	{
		___threeTimes_6 = value;
		Il2CppCodeGenWriteBarrier((&___threeTimes_6), value);
	}

	inline static int32_t get_offset_of_fiveTimes_7() { return static_cast<int32_t>(offsetof(Reward_t2838753371, ___fiveTimes_7)); }
	inline Sprite_t280657092 * get_fiveTimes_7() const { return ___fiveTimes_7; }
	inline Sprite_t280657092 ** get_address_of_fiveTimes_7() { return &___fiveTimes_7; }
	inline void set_fiveTimes_7(Sprite_t280657092 * value)
	{
		___fiveTimes_7 = value;
		Il2CppCodeGenWriteBarrier((&___fiveTimes_7), value);
	}

	inline static int32_t get_offset_of_rewardNumber_8() { return static_cast<int32_t>(offsetof(Reward_t2838753371, ___rewardNumber_8)); }
	inline int32_t get_rewardNumber_8() const { return ___rewardNumber_8; }
	inline int32_t* get_address_of_rewardNumber_8() { return &___rewardNumber_8; }
	inline void set_rewardNumber_8(int32_t value)
	{
		___rewardNumber_8 = value;
	}

	inline static int32_t get_offset_of_bonusRewards_9() { return static_cast<int32_t>(offsetof(Reward_t2838753371, ___bonusRewards_9)); }
	inline List_1_t913674750 * get_bonusRewards_9() const { return ___bonusRewards_9; }
	inline List_1_t913674750 ** get_address_of_bonusRewards_9() { return &___bonusRewards_9; }
	inline void set_bonusRewards_9(List_1_t913674750 * value)
	{
		___bonusRewards_9 = value;
		Il2CppCodeGenWriteBarrier((&___bonusRewards_9), value);
	}

	inline static int32_t get_offset_of_rewardAmount_10() { return static_cast<int32_t>(offsetof(Reward_t2838753371, ___rewardAmount_10)); }
	inline Text_t1901882714 * get_rewardAmount_10() const { return ___rewardAmount_10; }
	inline Text_t1901882714 ** get_address_of_rewardAmount_10() { return &___rewardAmount_10; }
	inline void set_rewardAmount_10(Text_t1901882714 * value)
	{
		___rewardAmount_10 = value;
		Il2CppCodeGenWriteBarrier((&___rewardAmount_10), value);
	}

	inline static int32_t get_offset_of_rewardPos_11() { return static_cast<int32_t>(offsetof(Reward_t2838753371, ___rewardPos_11)); }
	inline Vector3_t3722313464  get_rewardPos_11() const { return ___rewardPos_11; }
	inline Vector3_t3722313464 * get_address_of_rewardPos_11() { return &___rewardPos_11; }
	inline void set_rewardPos_11(Vector3_t3722313464  value)
	{
		___rewardPos_11 = value;
	}

	inline static int32_t get_offset_of_threeMultipliers_12() { return static_cast<int32_t>(offsetof(Reward_t2838753371, ___threeMultipliers_12)); }
	inline bool get_threeMultipliers_12() const { return ___threeMultipliers_12; }
	inline bool* get_address_of_threeMultipliers_12() { return &___threeMultipliers_12; }
	inline void set_threeMultipliers_12(bool value)
	{
		___threeMultipliers_12 = value;
	}

	inline static int32_t get_offset_of_twoMultipliers_13() { return static_cast<int32_t>(offsetof(Reward_t2838753371, ___twoMultipliers_13)); }
	inline bool get_twoMultipliers_13() const { return ___twoMultipliers_13; }
	inline bool* get_address_of_twoMultipliers_13() { return &___twoMultipliers_13; }
	inline void set_twoMultipliers_13(bool value)
	{
		___twoMultipliers_13 = value;
	}

	inline static int32_t get_offset_of_originalImage_14() { return static_cast<int32_t>(offsetof(Reward_t2838753371, ___originalImage_14)); }
	inline Sprite_t280657092 * get_originalImage_14() const { return ___originalImage_14; }
	inline Sprite_t280657092 ** get_address_of_originalImage_14() { return &___originalImage_14; }
	inline void set_originalImage_14(Sprite_t280657092 * value)
	{
		___originalImage_14 = value;
		Il2CppCodeGenWriteBarrier((&___originalImage_14), value);
	}

	inline static int32_t get_offset_of_rewardTextPosRef_15() { return static_cast<int32_t>(offsetof(Reward_t2838753371, ___rewardTextPosRef_15)); }
	inline GameObject_t1113636619 * get_rewardTextPosRef_15() const { return ___rewardTextPosRef_15; }
	inline GameObject_t1113636619 ** get_address_of_rewardTextPosRef_15() { return &___rewardTextPosRef_15; }
	inline void set_rewardTextPosRef_15(GameObject_t1113636619 * value)
	{
		___rewardTextPosRef_15 = value;
		Il2CppCodeGenWriteBarrier((&___rewardTextPosRef_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REWARD_T2838753371_H
#ifndef LOBBYCONTROLLER_T2879421917_H
#define LOBBYCONTROLLER_T2879421917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LobbyController
struct  LobbyController_t2879421917  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Color LobbyController::colorfade
	Color_t2555686324  ___colorfade_2;
	// UnityEngine.GameObject LobbyController::lobbyPanel
	GameObject_t1113636619 * ___lobbyPanel_3;
	// UnityEngine.UI.Text LobbyController::coinsText
	Text_t1901882714 * ___coinsText_4;
	// UnityEngine.GameObject LobbyController::iapButton
	GameObject_t1113636619 * ___iapButton_6;
	// UnityEngine.GameObject LobbyController::friendsPanel
	GameObject_t1113636619 * ___friendsPanel_7;
	// UnityEngine.GameObject LobbyController::friendsToInvitePanel
	GameObject_t1113636619 * ___friendsToInvitePanel_8;
	// UnityEngine.GameObject LobbyController::friendDetailsPrefab
	GameObject_t1113636619 * ___friendDetailsPrefab_9;
	// UnityEngine.GameObject LobbyController::inviteFriendDetailsPrefab
	GameObject_t1113636619 * ___inviteFriendDetailsPrefab_10;
	// UnityEngine.GameObject LobbyController::friendsPanelParent
	GameObject_t1113636619 * ___friendsPanelParent_11;
	// UnityEngine.GameObject LobbyController::friendsToInvitePanelParent
	GameObject_t1113636619 * ___friendsToInvitePanelParent_12;
	// UnityEngine.GameObject LobbyController::friendsDisplayText
	GameObject_t1113636619 * ___friendsDisplayText_13;

public:
	inline static int32_t get_offset_of_colorfade_2() { return static_cast<int32_t>(offsetof(LobbyController_t2879421917, ___colorfade_2)); }
	inline Color_t2555686324  get_colorfade_2() const { return ___colorfade_2; }
	inline Color_t2555686324 * get_address_of_colorfade_2() { return &___colorfade_2; }
	inline void set_colorfade_2(Color_t2555686324  value)
	{
		___colorfade_2 = value;
	}

	inline static int32_t get_offset_of_lobbyPanel_3() { return static_cast<int32_t>(offsetof(LobbyController_t2879421917, ___lobbyPanel_3)); }
	inline GameObject_t1113636619 * get_lobbyPanel_3() const { return ___lobbyPanel_3; }
	inline GameObject_t1113636619 ** get_address_of_lobbyPanel_3() { return &___lobbyPanel_3; }
	inline void set_lobbyPanel_3(GameObject_t1113636619 * value)
	{
		___lobbyPanel_3 = value;
		Il2CppCodeGenWriteBarrier((&___lobbyPanel_3), value);
	}

	inline static int32_t get_offset_of_coinsText_4() { return static_cast<int32_t>(offsetof(LobbyController_t2879421917, ___coinsText_4)); }
	inline Text_t1901882714 * get_coinsText_4() const { return ___coinsText_4; }
	inline Text_t1901882714 ** get_address_of_coinsText_4() { return &___coinsText_4; }
	inline void set_coinsText_4(Text_t1901882714 * value)
	{
		___coinsText_4 = value;
		Il2CppCodeGenWriteBarrier((&___coinsText_4), value);
	}

	inline static int32_t get_offset_of_iapButton_6() { return static_cast<int32_t>(offsetof(LobbyController_t2879421917, ___iapButton_6)); }
	inline GameObject_t1113636619 * get_iapButton_6() const { return ___iapButton_6; }
	inline GameObject_t1113636619 ** get_address_of_iapButton_6() { return &___iapButton_6; }
	inline void set_iapButton_6(GameObject_t1113636619 * value)
	{
		___iapButton_6 = value;
		Il2CppCodeGenWriteBarrier((&___iapButton_6), value);
	}

	inline static int32_t get_offset_of_friendsPanel_7() { return static_cast<int32_t>(offsetof(LobbyController_t2879421917, ___friendsPanel_7)); }
	inline GameObject_t1113636619 * get_friendsPanel_7() const { return ___friendsPanel_7; }
	inline GameObject_t1113636619 ** get_address_of_friendsPanel_7() { return &___friendsPanel_7; }
	inline void set_friendsPanel_7(GameObject_t1113636619 * value)
	{
		___friendsPanel_7 = value;
		Il2CppCodeGenWriteBarrier((&___friendsPanel_7), value);
	}

	inline static int32_t get_offset_of_friendsToInvitePanel_8() { return static_cast<int32_t>(offsetof(LobbyController_t2879421917, ___friendsToInvitePanel_8)); }
	inline GameObject_t1113636619 * get_friendsToInvitePanel_8() const { return ___friendsToInvitePanel_8; }
	inline GameObject_t1113636619 ** get_address_of_friendsToInvitePanel_8() { return &___friendsToInvitePanel_8; }
	inline void set_friendsToInvitePanel_8(GameObject_t1113636619 * value)
	{
		___friendsToInvitePanel_8 = value;
		Il2CppCodeGenWriteBarrier((&___friendsToInvitePanel_8), value);
	}

	inline static int32_t get_offset_of_friendDetailsPrefab_9() { return static_cast<int32_t>(offsetof(LobbyController_t2879421917, ___friendDetailsPrefab_9)); }
	inline GameObject_t1113636619 * get_friendDetailsPrefab_9() const { return ___friendDetailsPrefab_9; }
	inline GameObject_t1113636619 ** get_address_of_friendDetailsPrefab_9() { return &___friendDetailsPrefab_9; }
	inline void set_friendDetailsPrefab_9(GameObject_t1113636619 * value)
	{
		___friendDetailsPrefab_9 = value;
		Il2CppCodeGenWriteBarrier((&___friendDetailsPrefab_9), value);
	}

	inline static int32_t get_offset_of_inviteFriendDetailsPrefab_10() { return static_cast<int32_t>(offsetof(LobbyController_t2879421917, ___inviteFriendDetailsPrefab_10)); }
	inline GameObject_t1113636619 * get_inviteFriendDetailsPrefab_10() const { return ___inviteFriendDetailsPrefab_10; }
	inline GameObject_t1113636619 ** get_address_of_inviteFriendDetailsPrefab_10() { return &___inviteFriendDetailsPrefab_10; }
	inline void set_inviteFriendDetailsPrefab_10(GameObject_t1113636619 * value)
	{
		___inviteFriendDetailsPrefab_10 = value;
		Il2CppCodeGenWriteBarrier((&___inviteFriendDetailsPrefab_10), value);
	}

	inline static int32_t get_offset_of_friendsPanelParent_11() { return static_cast<int32_t>(offsetof(LobbyController_t2879421917, ___friendsPanelParent_11)); }
	inline GameObject_t1113636619 * get_friendsPanelParent_11() const { return ___friendsPanelParent_11; }
	inline GameObject_t1113636619 ** get_address_of_friendsPanelParent_11() { return &___friendsPanelParent_11; }
	inline void set_friendsPanelParent_11(GameObject_t1113636619 * value)
	{
		___friendsPanelParent_11 = value;
		Il2CppCodeGenWriteBarrier((&___friendsPanelParent_11), value);
	}

	inline static int32_t get_offset_of_friendsToInvitePanelParent_12() { return static_cast<int32_t>(offsetof(LobbyController_t2879421917, ___friendsToInvitePanelParent_12)); }
	inline GameObject_t1113636619 * get_friendsToInvitePanelParent_12() const { return ___friendsToInvitePanelParent_12; }
	inline GameObject_t1113636619 ** get_address_of_friendsToInvitePanelParent_12() { return &___friendsToInvitePanelParent_12; }
	inline void set_friendsToInvitePanelParent_12(GameObject_t1113636619 * value)
	{
		___friendsToInvitePanelParent_12 = value;
		Il2CppCodeGenWriteBarrier((&___friendsToInvitePanelParent_12), value);
	}

	inline static int32_t get_offset_of_friendsDisplayText_13() { return static_cast<int32_t>(offsetof(LobbyController_t2879421917, ___friendsDisplayText_13)); }
	inline GameObject_t1113636619 * get_friendsDisplayText_13() const { return ___friendsDisplayText_13; }
	inline GameObject_t1113636619 ** get_address_of_friendsDisplayText_13() { return &___friendsDisplayText_13; }
	inline void set_friendsDisplayText_13(GameObject_t1113636619 * value)
	{
		___friendsDisplayText_13 = value;
		Il2CppCodeGenWriteBarrier((&___friendsDisplayText_13), value);
	}
};

struct LobbyController_t2879421917_StaticFields
{
public:
	// LobbyController LobbyController::instance
	LobbyController_t2879421917 * ___instance_5;
	// Facebook.Unity.FacebookDelegate`1<Facebook.Unity.IShareResult> LobbyController::<>f__am$cache0
	FacebookDelegate_1_t602110559 * ___U3CU3Ef__amU24cache0_14;

public:
	inline static int32_t get_offset_of_instance_5() { return static_cast<int32_t>(offsetof(LobbyController_t2879421917_StaticFields, ___instance_5)); }
	inline LobbyController_t2879421917 * get_instance_5() const { return ___instance_5; }
	inline LobbyController_t2879421917 ** get_address_of_instance_5() { return &___instance_5; }
	inline void set_instance_5(LobbyController_t2879421917 * value)
	{
		___instance_5 = value;
		Il2CppCodeGenWriteBarrier((&___instance_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_14() { return static_cast<int32_t>(offsetof(LobbyController_t2879421917_StaticFields, ___U3CU3Ef__amU24cache0_14)); }
	inline FacebookDelegate_1_t602110559 * get_U3CU3Ef__amU24cache0_14() const { return ___U3CU3Ef__amU24cache0_14; }
	inline FacebookDelegate_1_t602110559 ** get_address_of_U3CU3Ef__amU24cache0_14() { return &___U3CU3Ef__amU24cache0_14; }
	inline void set_U3CU3Ef__amU24cache0_14(FacebookDelegate_1_t602110559 * value)
	{
		___U3CU3Ef__amU24cache0_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOBBYCONTROLLER_T2879421917_H
#ifndef RADIALLOADING_T2588181753_H
#define RADIALLOADING_T2588181753_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RadialLoading
struct  RadialLoading_t2588181753  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Slider RadialLoading::radialSlider
	Slider_t3903728902 * ___radialSlider_2;

public:
	inline static int32_t get_offset_of_radialSlider_2() { return static_cast<int32_t>(offsetof(RadialLoading_t2588181753, ___radialSlider_2)); }
	inline Slider_t3903728902 * get_radialSlider_2() const { return ___radialSlider_2; }
	inline Slider_t3903728902 ** get_address_of_radialSlider_2() { return &___radialSlider_2; }
	inline void set_radialSlider_2(Slider_t3903728902 * value)
	{
		___radialSlider_2 = value;
		Il2CppCodeGenWriteBarrier((&___radialSlider_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RADIALLOADING_T2588181753_H
#ifndef PROMOCODEPANELCONTROLLER_T729267270_H
#define PROMOCODEPANELCONTROLLER_T729267270_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PromoCodePanelController
struct  PromoCodePanelController_t729267270  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.UI.Text PromoCodePanelController::promoCodeText
	Text_t1901882714 * ___promoCodeText_2;
	// UnityEngine.GameObject PromoCodePanelController::wonBonusGO
	GameObject_t1113636619 * ___wonBonusGO_3;
	// UnityEngine.UI.Text PromoCodePanelController::won_coinText
	Text_t1901882714 * ___won_coinText_4;
	// UnityEngine.UI.Text PromoCodePanelController::responseText
	Text_t1901882714 * ___responseText_5;

public:
	inline static int32_t get_offset_of_promoCodeText_2() { return static_cast<int32_t>(offsetof(PromoCodePanelController_t729267270, ___promoCodeText_2)); }
	inline Text_t1901882714 * get_promoCodeText_2() const { return ___promoCodeText_2; }
	inline Text_t1901882714 ** get_address_of_promoCodeText_2() { return &___promoCodeText_2; }
	inline void set_promoCodeText_2(Text_t1901882714 * value)
	{
		___promoCodeText_2 = value;
		Il2CppCodeGenWriteBarrier((&___promoCodeText_2), value);
	}

	inline static int32_t get_offset_of_wonBonusGO_3() { return static_cast<int32_t>(offsetof(PromoCodePanelController_t729267270, ___wonBonusGO_3)); }
	inline GameObject_t1113636619 * get_wonBonusGO_3() const { return ___wonBonusGO_3; }
	inline GameObject_t1113636619 ** get_address_of_wonBonusGO_3() { return &___wonBonusGO_3; }
	inline void set_wonBonusGO_3(GameObject_t1113636619 * value)
	{
		___wonBonusGO_3 = value;
		Il2CppCodeGenWriteBarrier((&___wonBonusGO_3), value);
	}

	inline static int32_t get_offset_of_won_coinText_4() { return static_cast<int32_t>(offsetof(PromoCodePanelController_t729267270, ___won_coinText_4)); }
	inline Text_t1901882714 * get_won_coinText_4() const { return ___won_coinText_4; }
	inline Text_t1901882714 ** get_address_of_won_coinText_4() { return &___won_coinText_4; }
	inline void set_won_coinText_4(Text_t1901882714 * value)
	{
		___won_coinText_4 = value;
		Il2CppCodeGenWriteBarrier((&___won_coinText_4), value);
	}

	inline static int32_t get_offset_of_responseText_5() { return static_cast<int32_t>(offsetof(PromoCodePanelController_t729267270, ___responseText_5)); }
	inline Text_t1901882714 * get_responseText_5() const { return ___responseText_5; }
	inline Text_t1901882714 ** get_address_of_responseText_5() { return &___responseText_5; }
	inline void set_responseText_5(Text_t1901882714 * value)
	{
		___responseText_5 = value;
		Il2CppCodeGenWriteBarrier((&___responseText_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROMOCODEPANELCONTROLLER_T729267270_H
#ifndef REAPPLYSHADERS_T3938723122_H
#define REAPPLYSHADERS_T3938723122_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ReApplyShaders
struct  ReApplyShaders_t3938723122  : public MonoBehaviour_t3962482529
{
public:
	// UnityEngine.Material ReApplyShaders::material
	Material_t340375123 * ___material_2;
	// UnityEngine.Shader ReApplyShaders::shader
	Shader_t4151988712 * ___shader_3;

public:
	inline static int32_t get_offset_of_material_2() { return static_cast<int32_t>(offsetof(ReApplyShaders_t3938723122, ___material_2)); }
	inline Material_t340375123 * get_material_2() const { return ___material_2; }
	inline Material_t340375123 ** get_address_of_material_2() { return &___material_2; }
	inline void set_material_2(Material_t340375123 * value)
	{
		___material_2 = value;
		Il2CppCodeGenWriteBarrier((&___material_2), value);
	}

	inline static int32_t get_offset_of_shader_3() { return static_cast<int32_t>(offsetof(ReApplyShaders_t3938723122, ___shader_3)); }
	inline Shader_t4151988712 * get_shader_3() const { return ___shader_3; }
	inline Shader_t4151988712 ** get_address_of_shader_3() { return &___shader_3; }
	inline void set_shader_3(Shader_t4151988712 * value)
	{
		___shader_3 = value;
		Il2CppCodeGenWriteBarrier((&___shader_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REAPPLYSHADERS_T3938723122_H
#ifndef SLOTGAMENEWYORK_T345457032_H
#define SLOTGAMENEWYORK_T345457032_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Gameplay.SlotGameNewYork
struct  SlotGameNewYork_t345457032  : public BaseGame_t2336638441
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Sprite> Outback.Gameplay.SlotGameNewYork::lines
	List_1_t1752731834 * ___lines_38;

public:
	inline static int32_t get_offset_of_lines_38() { return static_cast<int32_t>(offsetof(SlotGameNewYork_t345457032, ___lines_38)); }
	inline List_1_t1752731834 * get_lines_38() const { return ___lines_38; }
	inline List_1_t1752731834 ** get_address_of_lines_38() { return &___lines_38; }
	inline void set_lines_38(List_1_t1752731834 * value)
	{
		___lines_38 = value;
		Il2CppCodeGenWriteBarrier((&___lines_38), value);
	}
};

struct SlotGameNewYork_t345457032_StaticFields
{
public:
	// Outback.Gameplay.SlotGameNewYork Outback.Gameplay.SlotGameNewYork::instance
	SlotGameNewYork_t345457032 * ___instance_37;

public:
	inline static int32_t get_offset_of_instance_37() { return static_cast<int32_t>(offsetof(SlotGameNewYork_t345457032_StaticFields, ___instance_37)); }
	inline SlotGameNewYork_t345457032 * get_instance_37() const { return ___instance_37; }
	inline SlotGameNewYork_t345457032 ** get_address_of_instance_37() { return &___instance_37; }
	inline void set_instance_37(SlotGameNewYork_t345457032 * value)
	{
		___instance_37 = value;
		Il2CppCodeGenWriteBarrier((&___instance_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTGAMENEWYORK_T345457032_H
#ifndef SLOTGAMEIRELAND_T2086646524_H
#define SLOTGAMEIRELAND_T2086646524_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Gameplay.SlotGameIreland
struct  SlotGameIreland_t2086646524  : public BaseGame_t2336638441
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Sprite> Outback.Gameplay.SlotGameIreland::lines
	List_1_t1752731834 * ___lines_38;

public:
	inline static int32_t get_offset_of_lines_38() { return static_cast<int32_t>(offsetof(SlotGameIreland_t2086646524, ___lines_38)); }
	inline List_1_t1752731834 * get_lines_38() const { return ___lines_38; }
	inline List_1_t1752731834 ** get_address_of_lines_38() { return &___lines_38; }
	inline void set_lines_38(List_1_t1752731834 * value)
	{
		___lines_38 = value;
		Il2CppCodeGenWriteBarrier((&___lines_38), value);
	}
};

struct SlotGameIreland_t2086646524_StaticFields
{
public:
	// Outback.Gameplay.SlotGameIreland Outback.Gameplay.SlotGameIreland::instance
	SlotGameIreland_t2086646524 * ___instance_37;

public:
	inline static int32_t get_offset_of_instance_37() { return static_cast<int32_t>(offsetof(SlotGameIreland_t2086646524_StaticFields, ___instance_37)); }
	inline SlotGameIreland_t2086646524 * get_instance_37() const { return ___instance_37; }
	inline SlotGameIreland_t2086646524 ** get_address_of_instance_37() { return &___instance_37; }
	inline void set_instance_37(SlotGameIreland_t2086646524 * value)
	{
		___instance_37 = value;
		Il2CppCodeGenWriteBarrier((&___instance_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTGAMEIRELAND_T2086646524_H
#ifndef SLOTGAMEMARINEWORLD_T1002224217_H
#define SLOTGAMEMARINEWORLD_T1002224217_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Gameplay.SlotGameMarineWorld
struct  SlotGameMarineWorld_t1002224217  : public BaseGame_t2336638441
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Sprite> Outback.Gameplay.SlotGameMarineWorld::lines
	List_1_t1752731834 * ___lines_38;

public:
	inline static int32_t get_offset_of_lines_38() { return static_cast<int32_t>(offsetof(SlotGameMarineWorld_t1002224217, ___lines_38)); }
	inline List_1_t1752731834 * get_lines_38() const { return ___lines_38; }
	inline List_1_t1752731834 ** get_address_of_lines_38() { return &___lines_38; }
	inline void set_lines_38(List_1_t1752731834 * value)
	{
		___lines_38 = value;
		Il2CppCodeGenWriteBarrier((&___lines_38), value);
	}
};

struct SlotGameMarineWorld_t1002224217_StaticFields
{
public:
	// Outback.Gameplay.SlotGameMarineWorld Outback.Gameplay.SlotGameMarineWorld::instance
	SlotGameMarineWorld_t1002224217 * ___instance_37;

public:
	inline static int32_t get_offset_of_instance_37() { return static_cast<int32_t>(offsetof(SlotGameMarineWorld_t1002224217_StaticFields, ___instance_37)); }
	inline SlotGameMarineWorld_t1002224217 * get_instance_37() const { return ___instance_37; }
	inline SlotGameMarineWorld_t1002224217 ** get_address_of_instance_37() { return &___instance_37; }
	inline void set_instance_37(SlotGameMarineWorld_t1002224217 * value)
	{
		___instance_37 = value;
		Il2CppCodeGenWriteBarrier((&___instance_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTGAMEMARINEWORLD_T1002224217_H
#ifndef SLOTGAMEBOXING_T1523269123_H
#define SLOTGAMEBOXING_T1523269123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Gameplay.SlotGameBoxing
struct  SlotGameBoxing_t1523269123  : public BaseGame_t2336638441
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Sprite> Outback.Gameplay.SlotGameBoxing::lines
	List_1_t1752731834 * ___lines_38;

public:
	inline static int32_t get_offset_of_lines_38() { return static_cast<int32_t>(offsetof(SlotGameBoxing_t1523269123, ___lines_38)); }
	inline List_1_t1752731834 * get_lines_38() const { return ___lines_38; }
	inline List_1_t1752731834 ** get_address_of_lines_38() { return &___lines_38; }
	inline void set_lines_38(List_1_t1752731834 * value)
	{
		___lines_38 = value;
		Il2CppCodeGenWriteBarrier((&___lines_38), value);
	}
};

struct SlotGameBoxing_t1523269123_StaticFields
{
public:
	// Outback.Gameplay.SlotGameBoxing Outback.Gameplay.SlotGameBoxing::instance
	SlotGameBoxing_t1523269123 * ___instance_37;

public:
	inline static int32_t get_offset_of_instance_37() { return static_cast<int32_t>(offsetof(SlotGameBoxing_t1523269123_StaticFields, ___instance_37)); }
	inline SlotGameBoxing_t1523269123 * get_instance_37() const { return ___instance_37; }
	inline SlotGameBoxing_t1523269123 ** get_address_of_instance_37() { return &___instance_37; }
	inline void set_instance_37(SlotGameBoxing_t1523269123 * value)
	{
		___instance_37 = value;
		Il2CppCodeGenWriteBarrier((&___instance_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTGAMEBOXING_T1523269123_H
#ifndef SLOTGAMEFARM_T876359397_H
#define SLOTGAMEFARM_T876359397_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Gameplay.SlotGameFarm
struct  SlotGameFarm_t876359397  : public BaseGame_t2336638441
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Sprite> Outback.Gameplay.SlotGameFarm::lines
	List_1_t1752731834 * ___lines_38;

public:
	inline static int32_t get_offset_of_lines_38() { return static_cast<int32_t>(offsetof(SlotGameFarm_t876359397, ___lines_38)); }
	inline List_1_t1752731834 * get_lines_38() const { return ___lines_38; }
	inline List_1_t1752731834 ** get_address_of_lines_38() { return &___lines_38; }
	inline void set_lines_38(List_1_t1752731834 * value)
	{
		___lines_38 = value;
		Il2CppCodeGenWriteBarrier((&___lines_38), value);
	}
};

struct SlotGameFarm_t876359397_StaticFields
{
public:
	// Outback.Gameplay.SlotGameFarm Outback.Gameplay.SlotGameFarm::instance
	SlotGameFarm_t876359397 * ___instance_37;

public:
	inline static int32_t get_offset_of_instance_37() { return static_cast<int32_t>(offsetof(SlotGameFarm_t876359397_StaticFields, ___instance_37)); }
	inline SlotGameFarm_t876359397 * get_instance_37() const { return ___instance_37; }
	inline SlotGameFarm_t876359397 ** get_address_of_instance_37() { return &___instance_37; }
	inline void set_instance_37(SlotGameFarm_t876359397 * value)
	{
		___instance_37 = value;
		Il2CppCodeGenWriteBarrier((&___instance_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTGAMEFARM_T876359397_H
#ifndef SLOTGAMECIRCUS_T1905421599_H
#define SLOTGAMECIRCUS_T1905421599_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Gameplay.SlotGameCircus
struct  SlotGameCircus_t1905421599  : public BaseGame_t2336638441
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Sprite> Outback.Gameplay.SlotGameCircus::lines
	List_1_t1752731834 * ___lines_38;

public:
	inline static int32_t get_offset_of_lines_38() { return static_cast<int32_t>(offsetof(SlotGameCircus_t1905421599, ___lines_38)); }
	inline List_1_t1752731834 * get_lines_38() const { return ___lines_38; }
	inline List_1_t1752731834 ** get_address_of_lines_38() { return &___lines_38; }
	inline void set_lines_38(List_1_t1752731834 * value)
	{
		___lines_38 = value;
		Il2CppCodeGenWriteBarrier((&___lines_38), value);
	}
};

struct SlotGameCircus_t1905421599_StaticFields
{
public:
	// Outback.Gameplay.SlotGameCircus Outback.Gameplay.SlotGameCircus::instance
	SlotGameCircus_t1905421599 * ___instance_37;

public:
	inline static int32_t get_offset_of_instance_37() { return static_cast<int32_t>(offsetof(SlotGameCircus_t1905421599_StaticFields, ___instance_37)); }
	inline SlotGameCircus_t1905421599 * get_instance_37() const { return ___instance_37; }
	inline SlotGameCircus_t1905421599 ** get_address_of_instance_37() { return &___instance_37; }
	inline void set_instance_37(SlotGameCircus_t1905421599 * value)
	{
		___instance_37 = value;
		Il2CppCodeGenWriteBarrier((&___instance_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTGAMECIRCUS_T1905421599_H
#ifndef SLOTGAMEEGYPT_T1252059770_H
#define SLOTGAMEEGYPT_T1252059770_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Gameplay.SlotGameEgypt
struct  SlotGameEgypt_t1252059770  : public BaseGame_t2336638441
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Sprite> Outback.Gameplay.SlotGameEgypt::lines
	List_1_t1752731834 * ___lines_38;

public:
	inline static int32_t get_offset_of_lines_38() { return static_cast<int32_t>(offsetof(SlotGameEgypt_t1252059770, ___lines_38)); }
	inline List_1_t1752731834 * get_lines_38() const { return ___lines_38; }
	inline List_1_t1752731834 ** get_address_of_lines_38() { return &___lines_38; }
	inline void set_lines_38(List_1_t1752731834 * value)
	{
		___lines_38 = value;
		Il2CppCodeGenWriteBarrier((&___lines_38), value);
	}
};

struct SlotGameEgypt_t1252059770_StaticFields
{
public:
	// Outback.Gameplay.SlotGameEgypt Outback.Gameplay.SlotGameEgypt::instance
	SlotGameEgypt_t1252059770 * ___instance_37;

public:
	inline static int32_t get_offset_of_instance_37() { return static_cast<int32_t>(offsetof(SlotGameEgypt_t1252059770_StaticFields, ___instance_37)); }
	inline SlotGameEgypt_t1252059770 * get_instance_37() const { return ___instance_37; }
	inline SlotGameEgypt_t1252059770 ** get_address_of_instance_37() { return &___instance_37; }
	inline void set_instance_37(SlotGameEgypt_t1252059770 * value)
	{
		___instance_37 = value;
		Il2CppCodeGenWriteBarrier((&___instance_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTGAMEEGYPT_T1252059770_H
#ifndef SLOTGAMESAFARI_T3944626951_H
#define SLOTGAMESAFARI_T3944626951_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Gameplay.SlotGameSafari
struct  SlotGameSafari_t3944626951  : public BaseGame_t2336638441
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Sprite> Outback.Gameplay.SlotGameSafari::lines
	List_1_t1752731834 * ___lines_38;

public:
	inline static int32_t get_offset_of_lines_38() { return static_cast<int32_t>(offsetof(SlotGameSafari_t3944626951, ___lines_38)); }
	inline List_1_t1752731834 * get_lines_38() const { return ___lines_38; }
	inline List_1_t1752731834 ** get_address_of_lines_38() { return &___lines_38; }
	inline void set_lines_38(List_1_t1752731834 * value)
	{
		___lines_38 = value;
		Il2CppCodeGenWriteBarrier((&___lines_38), value);
	}
};

struct SlotGameSafari_t3944626951_StaticFields
{
public:
	// Outback.Gameplay.SlotGameSafari Outback.Gameplay.SlotGameSafari::instance
	SlotGameSafari_t3944626951 * ___instance_37;

public:
	inline static int32_t get_offset_of_instance_37() { return static_cast<int32_t>(offsetof(SlotGameSafari_t3944626951_StaticFields, ___instance_37)); }
	inline SlotGameSafari_t3944626951 * get_instance_37() const { return ___instance_37; }
	inline SlotGameSafari_t3944626951 ** get_address_of_instance_37() { return &___instance_37; }
	inline void set_instance_37(SlotGameSafari_t3944626951 * value)
	{
		___instance_37 = value;
		Il2CppCodeGenWriteBarrier((&___instance_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTGAMESAFARI_T3944626951_H
#ifndef PLAYERMANAGER_T1841934776_H
#define PLAYERMANAGER_T1841934776_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Managers.PlayerManager
struct  PlayerManager_t1841934776  : public Player_t2753795864
{
public:
	// BaseGame Outback.Managers.PlayerManager::currentGame
	BaseGame_t2336638441 * ___currentGame_11;
	// System.Boolean Outback.Managers.PlayerManager::loggedInAsGuest
	bool ___loggedInAsGuest_12;

public:
	inline static int32_t get_offset_of_currentGame_11() { return static_cast<int32_t>(offsetof(PlayerManager_t1841934776, ___currentGame_11)); }
	inline BaseGame_t2336638441 * get_currentGame_11() const { return ___currentGame_11; }
	inline BaseGame_t2336638441 ** get_address_of_currentGame_11() { return &___currentGame_11; }
	inline void set_currentGame_11(BaseGame_t2336638441 * value)
	{
		___currentGame_11 = value;
		Il2CppCodeGenWriteBarrier((&___currentGame_11), value);
	}

	inline static int32_t get_offset_of_loggedInAsGuest_12() { return static_cast<int32_t>(offsetof(PlayerManager_t1841934776, ___loggedInAsGuest_12)); }
	inline bool get_loggedInAsGuest_12() const { return ___loggedInAsGuest_12; }
	inline bool* get_address_of_loggedInAsGuest_12() { return &___loggedInAsGuest_12; }
	inline void set_loggedInAsGuest_12(bool value)
	{
		___loggedInAsGuest_12 = value;
	}
};

struct PlayerManager_t1841934776_StaticFields
{
public:
	// Outback.Managers.PlayerManager Outback.Managers.PlayerManager::ME
	PlayerManager_t1841934776 * ___ME_10;

public:
	inline static int32_t get_offset_of_ME_10() { return static_cast<int32_t>(offsetof(PlayerManager_t1841934776_StaticFields, ___ME_10)); }
	inline PlayerManager_t1841934776 * get_ME_10() const { return ___ME_10; }
	inline PlayerManager_t1841934776 ** get_address_of_ME_10() { return &___ME_10; }
	inline void set_ME_10(PlayerManager_t1841934776 * value)
	{
		___ME_10 = value;
		Il2CppCodeGenWriteBarrier((&___ME_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYERMANAGER_T1841934776_H
#ifndef SLOTGAMEMOVIENIGHT_T2457517516_H
#define SLOTGAMEMOVIENIGHT_T2457517516_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Gameplay.SlotGameMovieNight
struct  SlotGameMovieNight_t2457517516  : public BaseGame_t2336638441
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Sprite> Outback.Gameplay.SlotGameMovieNight::lines
	List_1_t1752731834 * ___lines_38;

public:
	inline static int32_t get_offset_of_lines_38() { return static_cast<int32_t>(offsetof(SlotGameMovieNight_t2457517516, ___lines_38)); }
	inline List_1_t1752731834 * get_lines_38() const { return ___lines_38; }
	inline List_1_t1752731834 ** get_address_of_lines_38() { return &___lines_38; }
	inline void set_lines_38(List_1_t1752731834 * value)
	{
		___lines_38 = value;
		Il2CppCodeGenWriteBarrier((&___lines_38), value);
	}
};

struct SlotGameMovieNight_t2457517516_StaticFields
{
public:
	// Outback.Gameplay.SlotGameMovieNight Outback.Gameplay.SlotGameMovieNight::instance
	SlotGameMovieNight_t2457517516 * ___instance_37;

public:
	inline static int32_t get_offset_of_instance_37() { return static_cast<int32_t>(offsetof(SlotGameMovieNight_t2457517516_StaticFields, ___instance_37)); }
	inline SlotGameMovieNight_t2457517516 * get_instance_37() const { return ___instance_37; }
	inline SlotGameMovieNight_t2457517516 ** get_address_of_instance_37() { return &___instance_37; }
	inline void set_instance_37(SlotGameMovieNight_t2457517516 * value)
	{
		___instance_37 = value;
		Il2CppCodeGenWriteBarrier((&___instance_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTGAMEMOVIENIGHT_T2457517516_H
#ifndef SLOTGAMEOUTERSPACE_T3776195372_H
#define SLOTGAMEOUTERSPACE_T3776195372_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Gameplay.SlotGameOuterSpace
struct  SlotGameOuterSpace_t3776195372  : public BaseGame_t2336638441
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Sprite> Outback.Gameplay.SlotGameOuterSpace::lines
	List_1_t1752731834 * ___lines_38;

public:
	inline static int32_t get_offset_of_lines_38() { return static_cast<int32_t>(offsetof(SlotGameOuterSpace_t3776195372, ___lines_38)); }
	inline List_1_t1752731834 * get_lines_38() const { return ___lines_38; }
	inline List_1_t1752731834 ** get_address_of_lines_38() { return &___lines_38; }
	inline void set_lines_38(List_1_t1752731834 * value)
	{
		___lines_38 = value;
		Il2CppCodeGenWriteBarrier((&___lines_38), value);
	}
};

struct SlotGameOuterSpace_t3776195372_StaticFields
{
public:
	// Outback.Gameplay.SlotGameOuterSpace Outback.Gameplay.SlotGameOuterSpace::instance
	SlotGameOuterSpace_t3776195372 * ___instance_37;

public:
	inline static int32_t get_offset_of_instance_37() { return static_cast<int32_t>(offsetof(SlotGameOuterSpace_t3776195372_StaticFields, ___instance_37)); }
	inline SlotGameOuterSpace_t3776195372 * get_instance_37() const { return ___instance_37; }
	inline SlotGameOuterSpace_t3776195372 ** get_address_of_instance_37() { return &___instance_37; }
	inline void set_instance_37(SlotGameOuterSpace_t3776195372 * value)
	{
		___instance_37 = value;
		Il2CppCodeGenWriteBarrier((&___instance_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTGAMEOUTERSPACE_T3776195372_H
#ifndef SLOTGAMERETRO_T96010562_H
#define SLOTGAMERETRO_T96010562_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Outback.Gameplay.SlotGameRetro
struct  SlotGameRetro_t96010562  : public BaseGame_t2336638441
{
public:
	// System.Collections.Generic.List`1<UnityEngine.Sprite> Outback.Gameplay.SlotGameRetro::lines
	List_1_t1752731834 * ___lines_38;

public:
	inline static int32_t get_offset_of_lines_38() { return static_cast<int32_t>(offsetof(SlotGameRetro_t96010562, ___lines_38)); }
	inline List_1_t1752731834 * get_lines_38() const { return ___lines_38; }
	inline List_1_t1752731834 ** get_address_of_lines_38() { return &___lines_38; }
	inline void set_lines_38(List_1_t1752731834 * value)
	{
		___lines_38 = value;
		Il2CppCodeGenWriteBarrier((&___lines_38), value);
	}
};

struct SlotGameRetro_t96010562_StaticFields
{
public:
	// Outback.Gameplay.SlotGameRetro Outback.Gameplay.SlotGameRetro::instance
	SlotGameRetro_t96010562 * ___instance_37;

public:
	inline static int32_t get_offset_of_instance_37() { return static_cast<int32_t>(offsetof(SlotGameRetro_t96010562_StaticFields, ___instance_37)); }
	inline SlotGameRetro_t96010562 * get_instance_37() const { return ___instance_37; }
	inline SlotGameRetro_t96010562 ** get_address_of_instance_37() { return &___instance_37; }
	inline void set_instance_37(SlotGameRetro_t96010562 * value)
	{
		___instance_37 = value;
		Il2CppCodeGenWriteBarrier((&___instance_37), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SLOTGAMERETRO_T96010562_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2400 = { sizeof (TriggerBool_t501031542)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2400[4] = 
{
	TriggerBool_t501031542::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2401 = { sizeof (TriggerLifecycleEvent_t3193146760)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2401[9] = 
{
	TriggerLifecycleEvent_t3193146760::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2402 = { sizeof (TriggerOperator_t3611898925)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2402[9] = 
{
	TriggerOperator_t3611898925::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2403 = { sizeof (TriggerType_t105272677)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2403[5] = 
{
	TriggerType_t105272677::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2404 = { sizeof (TriggerListContainer_t2032715483), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2404[1] = 
{
	TriggerListContainer_t2032715483::get_offset_of_m_Rules_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2405 = { sizeof (EventTrigger_t2527451695), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2405[12] = 
{
	EventTrigger_t2527451695::get_offset_of_m_IsTriggerExpanded_0(),
	EventTrigger_t2527451695::get_offset_of_m_Type_1(),
	EventTrigger_t2527451695::get_offset_of_m_LifecycleEvent_2(),
	EventTrigger_t2527451695::get_offset_of_m_ApplyRules_3(),
	EventTrigger_t2527451695::get_offset_of_m_Rules_4(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerBool_5(),
	EventTrigger_t2527451695::get_offset_of_m_InitTime_6(),
	EventTrigger_t2527451695::get_offset_of_m_RepeatTime_7(),
	EventTrigger_t2527451695::get_offset_of_m_Repetitions_8(),
	EventTrigger_t2527451695::get_offset_of_repetitionCount_9(),
	EventTrigger_t2527451695::get_offset_of_m_TriggerFunction_10(),
	EventTrigger_t2527451695::get_offset_of_m_Method_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2406 = { sizeof (OnTrigger_t4184125570), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2407 = { sizeof (TrackableTrigger_t621205209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2407[2] = 
{
	TrackableTrigger_t621205209::get_offset_of_m_Target_0(),
	TrackableTrigger_t621205209::get_offset_of_m_MethodPath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2408 = { sizeof (TriggerMethod_t582536534), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2409 = { sizeof (TriggerRule_t1946298321), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2409[4] = 
{
	TriggerRule_t1946298321::get_offset_of_m_Target_0(),
	TriggerRule_t1946298321::get_offset_of_m_Operator_1(),
	TriggerRule_t1946298321::get_offset_of_m_Value_2(),
	TriggerRule_t1946298321::get_offset_of_m_Value2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2410 = { sizeof (U3CModuleU3E_t692745565), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2411 = { sizeof (AnimatableComponent_t332733166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2411[5] = 
{
	AnimatableComponent_t332733166::get_offset_of_anim_2(),
	AnimatableComponent_t332733166::get_offset_of_image_3(),
	AnimatableComponent_t332733166::get_offset_of_prmtrs_4(),
	AnimatableComponent_t332733166::get_offset_of_sprites_5(),
	AnimatableComponent_t332733166::get_offset_of_defaultSize_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2412 = { sizeof (U3CPlayU3Ec__AnonStorey0_t4221027112), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2412[1] = 
{
	U3CPlayU3Ec__AnonStorey0_t4221027112::get_offset_of_parameter_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2413 = { sizeof (U3CStopU3Ec__AnonStorey1_t1647417446), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2413[1] = 
{
	U3CStopU3Ec__AnonStorey1_t1647417446::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2414 = { sizeof (AssetBundleHandler_t3701416431), -1, sizeof(AssetBundleHandler_t3701416431_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2414[6] = 
{
	AssetBundleHandler_t3701416431_StaticFields::get_offset_of_instance_2(),
	AssetBundleHandler_t3701416431::get_offset_of_url_3(),
	AssetBundleHandler_t3701416431::get_offset_of_serverLink_4(),
	AssetBundleHandler_t3701416431::get_offset_of_gameNames_5(),
	AssetBundleHandler_t3701416431::get_offset_of_assetBundles_6(),
	AssetBundleHandler_t3701416431::get_offset_of_isDownload_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2415 = { sizeof (U3CprogressU3Ec__Iterator0_t2955259158), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2415[5] = 
{
	U3CprogressU3Ec__Iterator0_t2955259158::get_offset_of_req_0(),
	U3CprogressU3Ec__Iterator0_t2955259158::get_offset_of_U24this_1(),
	U3CprogressU3Ec__Iterator0_t2955259158::get_offset_of_U24current_2(),
	U3CprogressU3Ec__Iterator0_t2955259158::get_offset_of_U24disposing_3(),
	U3CprogressU3Ec__Iterator0_t2955259158::get_offset_of_U24PC_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2416 = { sizeof (U3CLoadAssetsU3Ec__Iterator1_t1305909268), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2416[10] = 
{
	U3CLoadAssetsU3Ec__Iterator1_t1305909268::get_offset_of_U3CnamesU3E__0_0(),
	U3CLoadAssetsU3Ec__Iterator1_t1305909268::get_offset_of_U3CwwwU3E__0_1(),
	U3CLoadAssetsU3Ec__Iterator1_t1305909268::get_offset_of_U3CassetBundleU3E__1_2(),
	U3CLoadAssetsU3Ec__Iterator1_t1305909268::get_offset_of_U3CsceneNamesU3E__1_3(),
	U3CLoadAssetsU3Ec__Iterator1_t1305909268::get_offset_of_U24locvar0_4(),
	U3CLoadAssetsU3Ec__Iterator1_t1305909268::get_offset_of_U24locvar1_5(),
	U3CLoadAssetsU3Ec__Iterator1_t1305909268::get_offset_of_U24this_6(),
	U3CLoadAssetsU3Ec__Iterator1_t1305909268::get_offset_of_U24current_7(),
	U3CLoadAssetsU3Ec__Iterator1_t1305909268::get_offset_of_U24disposing_8(),
	U3CLoadAssetsU3Ec__Iterator1_t1305909268::get_offset_of_U24PC_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2417 = { sizeof (BaseGame_t2336638441), -1, sizeof(BaseGame_t2336638441_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2417[35] = 
{
	BaseGame_t2336638441_StaticFields::get_offset_of_gameArray_2(),
	BaseGame_t2336638441_StaticFields::get_offset_of_gameImageArray_3(),
	BaseGame_t2336638441_StaticFields::get_offset_of_ruleGameArray_4(),
	BaseGame_t2336638441::get_offset_of_freeSpinPopUp_5(),
	BaseGame_t2336638441::get_offset_of_rewardPopUp_6(),
	BaseGame_t2336638441::get_offset_of_noRewardPopUp_7(),
	BaseGame_t2336638441::get_offset_of_spinBtn_8(),
	BaseGame_t2336638441::get_offset_of_stopBtn_9(),
	BaseGame_t2336638441::get_offset_of_freeSpinBtn_10(),
	BaseGame_t2336638441::get_offset_of_ruleLine_11(),
	BaseGame_t2336638441::get_offset_of_freeSpinRewardPopUp_12(),
	BaseGame_t2336638441::get_offset_of_bonusGamePanel_13(),
	BaseGame_t2336638441_StaticFields::get_offset_of_finalWin_14(),
	BaseGame_t2336638441::get_offset_of_coinsAnim_15(),
	BaseGame_t2336638441::get_offset_of_stopButtonSprite_16(),
	BaseGame_t2336638441::get_offset_of_spinButtonSprite_17(),
	BaseGame_t2336638441::get_offset_of_autoSpinStopButtonSprite_18(),
	BaseGame_t2336638441::get_offset_of_autoSpinStartButtonSprite_19(),
	BaseGame_t2336638441::get_offset_of_musicSource_20(),
	BaseGame_t2336638441::get_offset_of_musicSourceFreeScatter_21(),
	BaseGame_t2336638441::get_offset_of_winAmount_22(),
	BaseGame_t2336638441::get_offset_of_betAmount_23(),
	BaseGame_t2336638441::get_offset_of_betValue_24(),
	BaseGame_t2336638441::get_offset_of_currentCoins_25(),
	BaseGame_t2336638441::get_offset_of_winValue_26(),
	BaseGame_t2336638441::get_offset_of_freeSpinRemainingText_27(),
	BaseGame_t2336638441::get_offset_of_countingFreeSpins_28(),
	BaseGame_t2336638441::get_offset_of_minBet_29(),
	BaseGame_t2336638441::get_offset_of_numberOfFreeSpinsLeft_30(),
	BaseGame_t2336638441::get_offset_of_freeSpinWinnings_31(),
	BaseGame_t2336638441::get_offset_of_isBonusGame_32(),
	BaseGame_t2336638441::get_offset_of_rulePages_33(),
	BaseGame_t2336638441::get_offset_of_rewards_34(),
	BaseGame_t2336638441_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_35(),
	BaseGame_t2336638441_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2418 = { sizeof (U3CStartFreeSpinU3Ec__AnonStorey1_t680187965), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2418[2] = 
{
	U3CStartFreeSpinU3Ec__AnonStorey1_t680187965::get_offset_of_anim_0(),
	U3CStartFreeSpinU3Ec__AnonStorey1_t680187965::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2419 = { sizeof (U3CStartFreeSpinSequenceU3Ec__Iterator0_t2638780399), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2419[4] = 
{
	U3CStartFreeSpinSequenceU3Ec__Iterator0_t2638780399::get_offset_of_U24this_0(),
	U3CStartFreeSpinSequenceU3Ec__Iterator0_t2638780399::get_offset_of_U24current_1(),
	U3CStartFreeSpinSequenceU3Ec__Iterator0_t2638780399::get_offset_of_U24disposing_2(),
	U3CStartFreeSpinSequenceU3Ec__Iterator0_t2638780399::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2420 = { sizeof (U3CStartFreeSpinSequenceU3Ec__AnonStorey2_t2566881606), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2420[4] = 
{
	U3CStartFreeSpinSequenceU3Ec__AnonStorey2_t2566881606::get_offset_of_a_0(),
	U3CStartFreeSpinSequenceU3Ec__AnonStorey2_t2566881606::get_offset_of_amntWon_1(),
	U3CStartFreeSpinSequenceU3Ec__AnonStorey2_t2566881606::get_offset_of_newAmount_2(),
	U3CStartFreeSpinSequenceU3Ec__AnonStorey2_t2566881606::get_offset_of_U3CU3Ef__refU240_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2421 = { sizeof (U3CHideGameRewardsExceptU3Ec__AnonStorey3_t4084852398), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2421[1] = 
{
	U3CHideGameRewardsExceptU3Ec__AnonStorey3_t4084852398::get_offset_of_rewardNumToshow_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2422 = { sizeof (ClickHandler_t1871395319), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2423 = { sizeof (CoinsShower_t2268929218), -1, sizeof(CoinsShower_t2268929218_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2423[2] = 
{
	CoinsShower_t2268929218::get_offset_of_coinPrefab_2(),
	CoinsShower_t2268929218_StaticFields::get_offset_of_instance_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2424 = { sizeof (U3CStartAnimU3Ec__AnonStorey1_t4682627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2424[1] = 
{
	U3CStartAnimU3Ec__AnonStorey1_t4682627::get_offset_of_coin_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2425 = { sizeof (U3CStartAnimU3Ec__AnonStorey0_t4682628), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2425[2] = 
{
	U3CStartAnimU3Ec__AnonStorey0_t4682628::get_offset_of_a_0(),
	U3CStartAnimU3Ec__AnonStorey0_t4682628::get_offset_of_U3CU3Ef__refU241_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2426 = { sizeof (Player_t2753795864), -1, sizeof(Player_t2753795864_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2426[8] = 
{
	Player_t2753795864_StaticFields::get_offset_of_userId_2(),
	Player_t2753795864_StaticFields::get_offset_of_machineId_3(),
	Player_t2753795864_StaticFields::get_offset_of_userName_4(),
	Player_t2753795864_StaticFields::get_offset_of_email_5(),
	Player_t2753795864_StaticFields::get_offset_of_currentCoins_6(),
	Player_t2753795864_StaticFields::get_offset_of_spentCoins_7(),
	Player_t2753795864_StaticFields::get_offset_of_level_8(),
	Player_t2753795864_StaticFields::get_offset_of_password_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2427 = { sizeof (DailyReward_t2847634760), -1, sizeof(DailyReward_t2847634760_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2427[13] = 
{
	DailyReward_t2847634760::get_offset_of_timeLabel_2(),
	DailyReward_t2847634760::get_offset_of_timerButton_3(),
	DailyReward_t2847634760::get_offset_of_hours_4(),
	DailyReward_t2847634760::get_offset_of_minutes_5(),
	DailyReward_t2847634760::get_offset_of_seconds_6(),
	DailyReward_t2847634760::get_offset_of__timerComplete_7(),
	DailyReward_t2847634760::get_offset_of__timerIsReady_8(),
	DailyReward_t2847634760::get_offset_of_startTime_9(),
	DailyReward_t2847634760::get_offset_of_endTime_10(),
	DailyReward_t2847634760::get_offset_of_remainingTime_11(),
	DailyReward_t2847634760::get_offset_of__value_12(),
	DailyReward_t2847634760_StaticFields::get_offset_of_RewardToEarn_13(),
	DailyReward_t2847634760::get_offset_of_spinWheel_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2428 = { sizeof (U3CCheckTimeU3Ec__Iterator0_t3434459525), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2428[4] = 
{
	U3CCheckTimeU3Ec__Iterator0_t3434459525::get_offset_of_U24this_0(),
	U3CCheckTimeU3Ec__Iterator0_t3434459525::get_offset_of_U24current_1(),
	U3CCheckTimeU3Ec__Iterator0_t3434459525::get_offset_of_U24disposing_2(),
	U3CCheckTimeU3Ec__Iterator0_t3434459525::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2429 = { sizeof (DontDestroy_t1446738193), -1, sizeof(DontDestroy_t1446738193_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2429[2] = 
{
	DontDestroy_t1446738193_StaticFields::get_offset_of_instance_2(),
	DontDestroy_t1446738193::get_offset_of_gos_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2430 = { sizeof (FacebookFriend_t1860979812), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2430[3] = 
{
	FacebookFriend_t1860979812::get_offset_of_name_0(),
	FacebookFriend_t1860979812::get_offset_of_id_1(),
	FacebookFriend_t1860979812::get_offset_of_imageUrl_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2431 = { sizeof (FBHolder_t1712543493), -1, sizeof(FBHolder_t1712543493_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2431[17] = 
{
	FBHolder_t1712543493_StaticFields::get_offset_of_instance_2(),
	FBHolder_t1712543493::get_offset_of_applink_3(),
	FBHolder_t1712543493::get_offset_of_get_data_4(),
	FBHolder_t1712543493::get_offset_of_fbname_5(),
	FBHolder_t1712543493::get_offset_of_displayprofilepic_6(),
	FBHolder_t1712543493::get_offset_of_userIDSaved_7(),
	FBHolder_t1712543493::get_offset_of_userFacebookID_8(),
	FBHolder_t1712543493::get_offset_of_emailID_9(),
	FBHolder_t1712543493::get_offset_of_username_10(),
	FBHolder_t1712543493::get_offset_of_requiredData_11(),
	FBHolder_t1712543493::get_offset_of_facebook_id_12(),
	FBHolder_t1712543493::get_offset_of_emailID_user_13(),
	FBHolder_t1712543493_StaticFields::get_offset_of_BaseURL_14(),
	FBHolder_t1712543493::get_offset_of_gameDataFileName_15(),
	FBHolder_t1712543493::get_offset_of_facebookFriends_16(),
	0,
	FBHolder_t1712543493::get_offset_of_sec_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2432 = { sizeof (LoadPictureCallback_t2056340374), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2433 = { sizeof (U3CGetFriendListU3Ec__AnonStorey2_t3832777403), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2433[1] = 
{
	U3CGetFriendListU3Ec__AnonStorey2_t3832777403::get_offset_of_fbFriend_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2434 = { sizeof (U3CLoadPictureEnumeratorU3Ec__Iterator0_t1346235464), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2434[6] = 
{
	U3CLoadPictureEnumeratorU3Ec__Iterator0_t1346235464::get_offset_of_url_0(),
	U3CLoadPictureEnumeratorU3Ec__Iterator0_t1346235464::get_offset_of_U3CwwwU3E__0_1(),
	U3CLoadPictureEnumeratorU3Ec__Iterator0_t1346235464::get_offset_of_callback_2(),
	U3CLoadPictureEnumeratorU3Ec__Iterator0_t1346235464::get_offset_of_U24current_3(),
	U3CLoadPictureEnumeratorU3Ec__Iterator0_t1346235464::get_offset_of_U24disposing_4(),
	U3CLoadPictureEnumeratorU3Ec__Iterator0_t1346235464::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2435 = { sizeof (U3CLateCallRedirectU3Ec__Iterator1_t1650181486), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2435[4] = 
{
	U3CLateCallRedirectU3Ec__Iterator1_t1650181486::get_offset_of_U24this_0(),
	U3CLateCallRedirectU3Ec__Iterator1_t1650181486::get_offset_of_U24current_1(),
	U3CLateCallRedirectU3Ec__Iterator1_t1650181486::get_offset_of_U24disposing_2(),
	U3CLateCallRedirectU3Ec__Iterator1_t1650181486::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2436 = { sizeof (FBInviteManager_t4111452257), -1, sizeof(FBInviteManager_t4111452257_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2436[2] = 
{
	FBInviteManager_t4111452257::get_offset_of_id_2(),
	FBInviteManager_t4111452257_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2437 = { sizeof (SlotGameBoxing_t1523269123), -1, sizeof(SlotGameBoxing_t1523269123_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2437[2] = 
{
	SlotGameBoxing_t1523269123_StaticFields::get_offset_of_instance_37(),
	SlotGameBoxing_t1523269123::get_offset_of_lines_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2438 = { sizeof (U3CStartOffU3Ec__Iterator0_t309367319), -1, sizeof(U3CStartOffU3Ec__Iterator0_t309367319_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2438[11] = 
{
	U3CStartOffU3Ec__Iterator0_t309367319::get_offset_of_U3Carr1U3E__0_0(),
	U3CStartOffU3Ec__Iterator0_t309367319::get_offset_of_U3ClinesRulesU3E__0_1(),
	U3CStartOffU3Ec__Iterator0_t309367319::get_offset_of_U3CtotalAmountWonU3E__0_2(),
	U3CStartOffU3Ec__Iterator0_t309367319::get_offset_of_U3CnumOfLinesU3E__0_3(),
	U3CStartOffU3Ec__Iterator0_t309367319::get_offset_of_U3CallMatchedImagesU3E__0_4(),
	U3CStartOffU3Ec__Iterator0_t309367319::get_offset_of_U24this_5(),
	U3CStartOffU3Ec__Iterator0_t309367319::get_offset_of_U24current_6(),
	U3CStartOffU3Ec__Iterator0_t309367319::get_offset_of_U24disposing_7(),
	U3CStartOffU3Ec__Iterator0_t309367319::get_offset_of_U24PC_8(),
	U3CStartOffU3Ec__Iterator0_t309367319_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
	U3CStartOffU3Ec__Iterator0_t309367319_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2439 = { sizeof (SlotGameCircus_t1905421599), -1, sizeof(SlotGameCircus_t1905421599_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2439[2] = 
{
	SlotGameCircus_t1905421599_StaticFields::get_offset_of_instance_37(),
	SlotGameCircus_t1905421599::get_offset_of_lines_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2440 = { sizeof (U3CStartOffU3Ec__Iterator0_t1690707056), -1, sizeof(U3CStartOffU3Ec__Iterator0_t1690707056_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2440[11] = 
{
	U3CStartOffU3Ec__Iterator0_t1690707056::get_offset_of_U3Carr1U3E__0_0(),
	U3CStartOffU3Ec__Iterator0_t1690707056::get_offset_of_U3ClinesRulesU3E__0_1(),
	U3CStartOffU3Ec__Iterator0_t1690707056::get_offset_of_U3CtotalAmountWonU3E__0_2(),
	U3CStartOffU3Ec__Iterator0_t1690707056::get_offset_of_U3CallMatchedImagesU3E__0_3(),
	U3CStartOffU3Ec__Iterator0_t1690707056::get_offset_of_U3CnumOfLinesU3E__0_4(),
	U3CStartOffU3Ec__Iterator0_t1690707056::get_offset_of_U24this_5(),
	U3CStartOffU3Ec__Iterator0_t1690707056::get_offset_of_U24current_6(),
	U3CStartOffU3Ec__Iterator0_t1690707056::get_offset_of_U24disposing_7(),
	U3CStartOffU3Ec__Iterator0_t1690707056::get_offset_of_U24PC_8(),
	U3CStartOffU3Ec__Iterator0_t1690707056_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
	U3CStartOffU3Ec__Iterator0_t1690707056_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2441 = { sizeof (SlotGameEgypt_t1252059770), -1, sizeof(SlotGameEgypt_t1252059770_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2441[2] = 
{
	SlotGameEgypt_t1252059770_StaticFields::get_offset_of_instance_37(),
	SlotGameEgypt_t1252059770::get_offset_of_lines_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2442 = { sizeof (U3CStartOffU3Ec__Iterator0_t1448251964), -1, sizeof(U3CStartOffU3Ec__Iterator0_t1448251964_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2442[11] = 
{
	U3CStartOffU3Ec__Iterator0_t1448251964::get_offset_of_U3Carr1U3E__0_0(),
	U3CStartOffU3Ec__Iterator0_t1448251964::get_offset_of_U3ClinesRulesU3E__0_1(),
	U3CStartOffU3Ec__Iterator0_t1448251964::get_offset_of_U3CtotalAmountWonU3E__0_2(),
	U3CStartOffU3Ec__Iterator0_t1448251964::get_offset_of_U3CallMatchedImagesU3E__0_3(),
	U3CStartOffU3Ec__Iterator0_t1448251964::get_offset_of_U3CnumOfLinesU3E__0_4(),
	U3CStartOffU3Ec__Iterator0_t1448251964::get_offset_of_U24this_5(),
	U3CStartOffU3Ec__Iterator0_t1448251964::get_offset_of_U24current_6(),
	U3CStartOffU3Ec__Iterator0_t1448251964::get_offset_of_U24disposing_7(),
	U3CStartOffU3Ec__Iterator0_t1448251964::get_offset_of_U24PC_8(),
	U3CStartOffU3Ec__Iterator0_t1448251964_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
	U3CStartOffU3Ec__Iterator0_t1448251964_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2443 = { sizeof (SlotGameFarm_t876359397), -1, sizeof(SlotGameFarm_t876359397_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2443[2] = 
{
	SlotGameFarm_t876359397_StaticFields::get_offset_of_instance_37(),
	SlotGameFarm_t876359397::get_offset_of_lines_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2444 = { sizeof (U3CStartOffU3Ec__Iterator0_t4243828766), -1, sizeof(U3CStartOffU3Ec__Iterator0_t4243828766_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2444[10] = 
{
	U3CStartOffU3Ec__Iterator0_t4243828766::get_offset_of_U3Carr1U3E__0_0(),
	U3CStartOffU3Ec__Iterator0_t4243828766::get_offset_of_U3ClinesRulesU3E__0_1(),
	U3CStartOffU3Ec__Iterator0_t4243828766::get_offset_of_U3CtotalAmountWonU3E__0_2(),
	U3CStartOffU3Ec__Iterator0_t4243828766::get_offset_of_U3CallMatchedImagesU3E__0_3(),
	U3CStartOffU3Ec__Iterator0_t4243828766::get_offset_of_U3CnumOfLinesU3E__0_4(),
	U3CStartOffU3Ec__Iterator0_t4243828766::get_offset_of_U24this_5(),
	U3CStartOffU3Ec__Iterator0_t4243828766::get_offset_of_U24current_6(),
	U3CStartOffU3Ec__Iterator0_t4243828766::get_offset_of_U24disposing_7(),
	U3CStartOffU3Ec__Iterator0_t4243828766::get_offset_of_U24PC_8(),
	U3CStartOffU3Ec__Iterator0_t4243828766_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2445 = { sizeof (SlotGameIreland_t2086646524), -1, sizeof(SlotGameIreland_t2086646524_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2445[2] = 
{
	SlotGameIreland_t2086646524_StaticFields::get_offset_of_instance_37(),
	SlotGameIreland_t2086646524::get_offset_of_lines_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2446 = { sizeof (U3CStartOffU3Ec__Iterator0_t1891839980), -1, sizeof(U3CStartOffU3Ec__Iterator0_t1891839980_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2446[11] = 
{
	U3CStartOffU3Ec__Iterator0_t1891839980::get_offset_of_U3Carr1U3E__0_0(),
	U3CStartOffU3Ec__Iterator0_t1891839980::get_offset_of_U3ClinesRulesU3E__0_1(),
	U3CStartOffU3Ec__Iterator0_t1891839980::get_offset_of_U3CtotalAmountWonU3E__0_2(),
	U3CStartOffU3Ec__Iterator0_t1891839980::get_offset_of_U3CallMatchedImagesU3E__0_3(),
	U3CStartOffU3Ec__Iterator0_t1891839980::get_offset_of_U3CnumOfLinesU3E__0_4(),
	U3CStartOffU3Ec__Iterator0_t1891839980::get_offset_of_U24this_5(),
	U3CStartOffU3Ec__Iterator0_t1891839980::get_offset_of_U24current_6(),
	U3CStartOffU3Ec__Iterator0_t1891839980::get_offset_of_U24disposing_7(),
	U3CStartOffU3Ec__Iterator0_t1891839980::get_offset_of_U24PC_8(),
	U3CStartOffU3Ec__Iterator0_t1891839980_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
	U3CStartOffU3Ec__Iterator0_t1891839980_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2447 = { sizeof (SlotGameMarineWorld_t1002224217), -1, sizeof(SlotGameMarineWorld_t1002224217_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2447[2] = 
{
	SlotGameMarineWorld_t1002224217_StaticFields::get_offset_of_instance_37(),
	SlotGameMarineWorld_t1002224217::get_offset_of_lines_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2448 = { sizeof (U3CStartOffU3Ec__Iterator0_t608072064), -1, sizeof(U3CStartOffU3Ec__Iterator0_t608072064_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2448[10] = 
{
	U3CStartOffU3Ec__Iterator0_t608072064::get_offset_of_U3Carr1U3E__0_0(),
	U3CStartOffU3Ec__Iterator0_t608072064::get_offset_of_U3ClinesRulesU3E__0_1(),
	U3CStartOffU3Ec__Iterator0_t608072064::get_offset_of_U3CtotalAmountWonU3E__0_2(),
	U3CStartOffU3Ec__Iterator0_t608072064::get_offset_of_U3CallMatchedImagesU3E__0_3(),
	U3CStartOffU3Ec__Iterator0_t608072064::get_offset_of_U3CnumOfLinesU3E__0_4(),
	U3CStartOffU3Ec__Iterator0_t608072064::get_offset_of_U24this_5(),
	U3CStartOffU3Ec__Iterator0_t608072064::get_offset_of_U24current_6(),
	U3CStartOffU3Ec__Iterator0_t608072064::get_offset_of_U24disposing_7(),
	U3CStartOffU3Ec__Iterator0_t608072064::get_offset_of_U24PC_8(),
	U3CStartOffU3Ec__Iterator0_t608072064_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2449 = { sizeof (SlotGameMovieNight_t2457517516), -1, sizeof(SlotGameMovieNight_t2457517516_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2449[2] = 
{
	SlotGameMovieNight_t2457517516_StaticFields::get_offset_of_instance_37(),
	SlotGameMovieNight_t2457517516::get_offset_of_lines_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2450 = { sizeof (U3CStartOffU3Ec__Iterator0_t4188771159), -1, sizeof(U3CStartOffU3Ec__Iterator0_t4188771159_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2450[11] = 
{
	U3CStartOffU3Ec__Iterator0_t4188771159::get_offset_of_U3Carr1U3E__0_0(),
	U3CStartOffU3Ec__Iterator0_t4188771159::get_offset_of_U3ClinesRulesU3E__0_1(),
	U3CStartOffU3Ec__Iterator0_t4188771159::get_offset_of_U3CtotalAmountWonU3E__0_2(),
	U3CStartOffU3Ec__Iterator0_t4188771159::get_offset_of_U3CallMatchedImagesU3E__0_3(),
	U3CStartOffU3Ec__Iterator0_t4188771159::get_offset_of_U3CnumOfLinesU3E__0_4(),
	U3CStartOffU3Ec__Iterator0_t4188771159::get_offset_of_U24this_5(),
	U3CStartOffU3Ec__Iterator0_t4188771159::get_offset_of_U24current_6(),
	U3CStartOffU3Ec__Iterator0_t4188771159::get_offset_of_U24disposing_7(),
	U3CStartOffU3Ec__Iterator0_t4188771159::get_offset_of_U24PC_8(),
	U3CStartOffU3Ec__Iterator0_t4188771159_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
	U3CStartOffU3Ec__Iterator0_t4188771159_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2451 = { sizeof (SlotGameNewYork_t345457032), -1, sizeof(SlotGameNewYork_t345457032_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2451[2] = 
{
	SlotGameNewYork_t345457032_StaticFields::get_offset_of_instance_37(),
	SlotGameNewYork_t345457032::get_offset_of_lines_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2452 = { sizeof (U3CStartOffU3Ec__Iterator0_t278533614), -1, sizeof(U3CStartOffU3Ec__Iterator0_t278533614_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2452[11] = 
{
	U3CStartOffU3Ec__Iterator0_t278533614::get_offset_of_U3Carr1U3E__0_0(),
	U3CStartOffU3Ec__Iterator0_t278533614::get_offset_of_U3ClinesRulesU3E__0_1(),
	U3CStartOffU3Ec__Iterator0_t278533614::get_offset_of_U3CtotalAmountWonU3E__0_2(),
	U3CStartOffU3Ec__Iterator0_t278533614::get_offset_of_U3CallMatchedImagesU3E__0_3(),
	U3CStartOffU3Ec__Iterator0_t278533614::get_offset_of_U3CnumOfLinesU3E__0_4(),
	U3CStartOffU3Ec__Iterator0_t278533614::get_offset_of_U24this_5(),
	U3CStartOffU3Ec__Iterator0_t278533614::get_offset_of_U24current_6(),
	U3CStartOffU3Ec__Iterator0_t278533614::get_offset_of_U24disposing_7(),
	U3CStartOffU3Ec__Iterator0_t278533614::get_offset_of_U24PC_8(),
	U3CStartOffU3Ec__Iterator0_t278533614_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
	U3CStartOffU3Ec__Iterator0_t278533614_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2453 = { sizeof (SlotGameOuterSpace_t3776195372), -1, sizeof(SlotGameOuterSpace_t3776195372_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2453[2] = 
{
	SlotGameOuterSpace_t3776195372_StaticFields::get_offset_of_instance_37(),
	SlotGameOuterSpace_t3776195372::get_offset_of_lines_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2454 = { sizeof (U3CStartOffU3Ec__Iterator0_t282260692), -1, sizeof(U3CStartOffU3Ec__Iterator0_t282260692_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2454[11] = 
{
	U3CStartOffU3Ec__Iterator0_t282260692::get_offset_of_U3Carr1U3E__0_0(),
	U3CStartOffU3Ec__Iterator0_t282260692::get_offset_of_U3ClinesRulesU3E__0_1(),
	U3CStartOffU3Ec__Iterator0_t282260692::get_offset_of_U3CtotalAmountWonU3E__0_2(),
	U3CStartOffU3Ec__Iterator0_t282260692::get_offset_of_U3CallMatchedImagesU3E__0_3(),
	U3CStartOffU3Ec__Iterator0_t282260692::get_offset_of_U3CnumOfLinesU3E__0_4(),
	U3CStartOffU3Ec__Iterator0_t282260692::get_offset_of_U24this_5(),
	U3CStartOffU3Ec__Iterator0_t282260692::get_offset_of_U24current_6(),
	U3CStartOffU3Ec__Iterator0_t282260692::get_offset_of_U24disposing_7(),
	U3CStartOffU3Ec__Iterator0_t282260692::get_offset_of_U24PC_8(),
	U3CStartOffU3Ec__Iterator0_t282260692_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
	U3CStartOffU3Ec__Iterator0_t282260692_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2455 = { sizeof (SlotGameRetro_t96010562), -1, sizeof(SlotGameRetro_t96010562_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2455[2] = 
{
	SlotGameRetro_t96010562_StaticFields::get_offset_of_instance_37(),
	SlotGameRetro_t96010562::get_offset_of_lines_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2456 = { sizeof (U3CStartOffU3Ec__Iterator0_t2261323095), -1, sizeof(U3CStartOffU3Ec__Iterator0_t2261323095_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2456[11] = 
{
	U3CStartOffU3Ec__Iterator0_t2261323095::get_offset_of_U3Carr1U3E__0_0(),
	U3CStartOffU3Ec__Iterator0_t2261323095::get_offset_of_U3ClinesRulesU3E__0_1(),
	U3CStartOffU3Ec__Iterator0_t2261323095::get_offset_of_U3CtotalAmountWonU3E__0_2(),
	U3CStartOffU3Ec__Iterator0_t2261323095::get_offset_of_U3CallMatchedImagesU3E__0_3(),
	U3CStartOffU3Ec__Iterator0_t2261323095::get_offset_of_U3CnumOfLinesU3E__0_4(),
	U3CStartOffU3Ec__Iterator0_t2261323095::get_offset_of_U24this_5(),
	U3CStartOffU3Ec__Iterator0_t2261323095::get_offset_of_U24current_6(),
	U3CStartOffU3Ec__Iterator0_t2261323095::get_offset_of_U24disposing_7(),
	U3CStartOffU3Ec__Iterator0_t2261323095::get_offset_of_U24PC_8(),
	U3CStartOffU3Ec__Iterator0_t2261323095_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
	U3CStartOffU3Ec__Iterator0_t2261323095_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2457 = { sizeof (SlotGameSafari_t3944626951), -1, sizeof(SlotGameSafari_t3944626951_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2457[2] = 
{
	SlotGameSafari_t3944626951_StaticFields::get_offset_of_instance_37(),
	SlotGameSafari_t3944626951::get_offset_of_lines_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2458 = { sizeof (U3CStartOffU3Ec__Iterator0_t528495272), -1, sizeof(U3CStartOffU3Ec__Iterator0_t528495272_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2458[11] = 
{
	U3CStartOffU3Ec__Iterator0_t528495272::get_offset_of_U3Carr1U3E__0_0(),
	U3CStartOffU3Ec__Iterator0_t528495272::get_offset_of_U3ClinesRulesU3E__0_1(),
	U3CStartOffU3Ec__Iterator0_t528495272::get_offset_of_U3CtotalAmountWonU3E__0_2(),
	U3CStartOffU3Ec__Iterator0_t528495272::get_offset_of_U3CallMatchedImagesU3E__0_3(),
	U3CStartOffU3Ec__Iterator0_t528495272::get_offset_of_U3CnumOfLinesU3E__0_4(),
	U3CStartOffU3Ec__Iterator0_t528495272::get_offset_of_U24this_5(),
	U3CStartOffU3Ec__Iterator0_t528495272::get_offset_of_U24current_6(),
	U3CStartOffU3Ec__Iterator0_t528495272::get_offset_of_U24disposing_7(),
	U3CStartOffU3Ec__Iterator0_t528495272::get_offset_of_U24PC_8(),
	U3CStartOffU3Ec__Iterator0_t528495272_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_9(),
	U3CStartOffU3Ec__Iterator0_t528495272_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2459 = { sizeof (GameSound_t3372255689), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2460 = { sizeof (InAppPurchase_t1703437824), -1, sizeof(InAppPurchase_t1703437824_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2460[10] = 
{
	InAppPurchase_t1703437824::get_offset_of__iap_2(),
	InAppPurchase_t1703437824_StaticFields::get_offset_of_Product_10K_3(),
	InAppPurchase_t1703437824_StaticFields::get_offset_of_Product_25K_4(),
	InAppPurchase_t1703437824_StaticFields::get_offset_of_Product_50K_5(),
	InAppPurchase_t1703437824_StaticFields::get_offset_of_Product_75K_6(),
	InAppPurchase_t1703437824_StaticFields::get_offset_of_Product_100K_7(),
	InAppPurchase_t1703437824_StaticFields::get_offset_of_Product_500K_8(),
	InAppPurchase_t1703437824_StaticFields::get_offset_of_Product_1M_9(),
	InAppPurchase_t1703437824_StaticFields::get_offset_of_Product_5M_10(),
	InAppPurchase_t1703437824_StaticFields::get_offset_of_Product_10M_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2461 = { sizeof (launchImage_t1920275599), -1, sizeof(launchImage_t1920275599_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2461[2] = 
{
	launchImage_t1920275599::get_offset_of_delayTime_2(),
	launchImage_t1920275599_StaticFields::get_offset_of_constantvaluestored_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2462 = { sizeof (U3CStartU3Ec__Iterator0_t1180820131), -1, sizeof(U3CStartU3Ec__Iterator0_t1180820131_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2462[7] = 
{
	U3CStartU3Ec__Iterator0_t1180820131::get_offset_of_U3CstrlogU3E__0_0(),
	U3CStartU3Ec__Iterator0_t1180820131::get_offset_of_U24this_1(),
	U3CStartU3Ec__Iterator0_t1180820131::get_offset_of_U24current_2(),
	U3CStartU3Ec__Iterator0_t1180820131::get_offset_of_U24disposing_3(),
	U3CStartU3Ec__Iterator0_t1180820131::get_offset_of_U24PC_4(),
	U3CStartU3Ec__Iterator0_t1180820131_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_5(),
	U3CStartU3Ec__Iterator0_t1180820131_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2463 = { sizeof (LobbyController_t2879421917), -1, sizeof(LobbyController_t2879421917_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2463[13] = 
{
	LobbyController_t2879421917::get_offset_of_colorfade_2(),
	LobbyController_t2879421917::get_offset_of_lobbyPanel_3(),
	LobbyController_t2879421917::get_offset_of_coinsText_4(),
	LobbyController_t2879421917_StaticFields::get_offset_of_instance_5(),
	LobbyController_t2879421917::get_offset_of_iapButton_6(),
	LobbyController_t2879421917::get_offset_of_friendsPanel_7(),
	LobbyController_t2879421917::get_offset_of_friendsToInvitePanel_8(),
	LobbyController_t2879421917::get_offset_of_friendDetailsPrefab_9(),
	LobbyController_t2879421917::get_offset_of_inviteFriendDetailsPrefab_10(),
	LobbyController_t2879421917::get_offset_of_friendsPanelParent_11(),
	LobbyController_t2879421917::get_offset_of_friendsToInvitePanelParent_12(),
	LobbyController_t2879421917::get_offset_of_friendsDisplayText_13(),
	LobbyController_t2879421917_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2464 = { sizeof (LoadPictureCallback_t1320020716), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2465 = { sizeof (U3CPopulateFriendsPanelU3Ec__AnonStorey1_t1616196176), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2465[1] = 
{
	U3CPopulateFriendsPanelU3Ec__AnonStorey1_t1616196176::get_offset_of_friendInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2466 = { sizeof (U3CLoadPictureEnumeratorU3Ec__Iterator0_t2007900168), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2466[6] = 
{
	U3CLoadPictureEnumeratorU3Ec__Iterator0_t2007900168::get_offset_of_url_0(),
	U3CLoadPictureEnumeratorU3Ec__Iterator0_t2007900168::get_offset_of_U3CwwwU3E__0_1(),
	U3CLoadPictureEnumeratorU3Ec__Iterator0_t2007900168::get_offset_of_callback_2(),
	U3CLoadPictureEnumeratorU3Ec__Iterator0_t2007900168::get_offset_of_U24current_3(),
	U3CLoadPictureEnumeratorU3Ec__Iterator0_t2007900168::get_offset_of_U24disposing_4(),
	U3CLoadPictureEnumeratorU3Ec__Iterator0_t2007900168::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2467 = { sizeof (LoginScreenController_t2093126341), -1, sizeof(LoginScreenController_t2093126341_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2467[6] = 
{
	LoginScreenController_t2093126341_StaticFields::get_offset_of_instance_2(),
	LoginScreenController_t2093126341::get_offset_of_detailsPanel_3(),
	LoginScreenController_t2093126341::get_offset_of_guestButton_4(),
	LoginScreenController_t2093126341::get_offset_of_fbLoginButton_5(),
	LoginScreenController_t2093126341::get_offset_of_email_6(),
	LoginScreenController_t2093126341::get_offset_of_password_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2468 = { sizeof (BetManager_t1302012615), -1, sizeof(BetManager_t1302012615_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2468[7] = 
{
	BetManager_t1302012615::get_offset_of_incrementButton_2(),
	BetManager_t1302012615::get_offset_of_decrementButton_3(),
	BetManager_t1302012615_StaticFields::get_offset_of_totalBet_4(),
	BetManager_t1302012615::get_offset_of_minBet_5(),
	BetManager_t1302012615::get_offset_of_maxBet_6(),
	BetManager_t1302012615::get_offset_of_AddPlusButton_7(),
	BetManager_t1302012615_StaticFields::get_offset_of_xc_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2469 = { sizeof (MasterConfigsManager_t2615534321), -1, sizeof(MasterConfigsManager_t2615534321_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2469[14] = 
{
	MasterConfigsManager_t2615534321_StaticFields::get_offset_of_instance_2(),
	MasterConfigsManager_t2615534321::get_offset_of_lobbyAtlasAB_3(),
	MasterConfigsManager_t2615534321::get_offset_of_lobbyLayoutJson_4(),
	MasterConfigsManager_t2615534321::get_offset_of_levelUpRewardJson_5(),
	MasterConfigsManager_t2615534321::get_offset_of_masterGameConfigJson_6(),
	MasterConfigsManager_t2615534321::get_offset_of_storeInformationJson_7(),
	MasterConfigsManager_t2615534321::get_offset_of_crossPromotions_8(),
	MasterConfigsManager_t2615534321::get_offset_of_configFileBaseUrl_9(),
	MasterConfigsManager_t2615534321::get_offset_of_assetBundleBaseUrl_10(),
	MasterConfigsManager_t2615534321::get_offset_of_preloadersBaseUrl_11(),
	MasterConfigsManager_t2615534321_StaticFields::get_offset_of_playerSyncRepeatRate_12(),
	MasterConfigsManager_t2615534321::get_offset_of_COMMONS_ATLAS_13(),
	MasterConfigsManager_t2615534321::get_offset_of_LOBBY_ATLAS_14(),
	MasterConfigsManager_t2615534321::get_offset_of_LOBBY_ATLAS_FROM_AB_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2470 = { sizeof (U3CGetSpriteFromCommonsU3Ec__AnonStorey0_t1183225816), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2470[1] = 
{
	U3CGetSpriteFromCommonsU3Ec__AnonStorey0_t1183225816::get_offset_of_name_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2471 = { sizeof (U3CGetSpriteForGameUnlockU3Ec__AnonStorey1_t3737134032), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2471[1] = 
{
	U3CGetSpriteForGameUnlockU3Ec__AnonStorey1_t3737134032::get_offset_of_spriteName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2472 = { sizeof (CrossPromoCreative_t733597769), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2472[4] = 
{
	CrossPromoCreative_t733597769::get_offset_of_interstitial_0(),
	CrossPromoCreative_t733597769::get_offset_of_banner_1(),
	CrossPromoCreative_t733597769::get_offset_of_close_2(),
	CrossPromoCreative_t733597769::get_offset_of_url_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2473 = { sizeof (PlayerManager_t1841934776), -1, sizeof(PlayerManager_t1841934776_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2473[3] = 
{
	PlayerManager_t1841934776_StaticFields::get_offset_of_ME_10(),
	PlayerManager_t1841934776::get_offset_of_currentGame_11(),
	PlayerManager_t1841934776::get_offset_of_loggedInAsGuest_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2474 = { sizeof (U3CUpdateUserDetailsU3Ec__Iterator0_t3148111241), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2474[7] = 
{
	U3CUpdateUserDetailsU3Ec__Iterator0_t3148111241::get_offset_of_U3CurlU3E__0_0(),
	U3CUpdateUserDetailsU3Ec__Iterator0_t3148111241::get_offset_of_U3CformU3E__0_1(),
	U3CUpdateUserDetailsU3Ec__Iterator0_t3148111241::get_offset_of_updatefield_2(),
	U3CUpdateUserDetailsU3Ec__Iterator0_t3148111241::get_offset_of_U3CdownloadU3E__0_3(),
	U3CUpdateUserDetailsU3Ec__Iterator0_t3148111241::get_offset_of_U24current_4(),
	U3CUpdateUserDetailsU3Ec__Iterator0_t3148111241::get_offset_of_U24disposing_5(),
	U3CUpdateUserDetailsU3Ec__Iterator0_t3148111241::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2475 = { sizeof (U3CCheckPlayerU3Ec__Iterator1_t2156344797), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2475[7] = 
{
	U3CCheckPlayerU3Ec__Iterator1_t2156344797::get_offset_of_U3CurlU3E__0_0(),
	U3CCheckPlayerU3Ec__Iterator1_t2156344797::get_offset_of_U3CformU3E__0_1(),
	U3CCheckPlayerU3Ec__Iterator1_t2156344797::get_offset_of_U3CdownloadU3E__0_2(),
	U3CCheckPlayerU3Ec__Iterator1_t2156344797::get_offset_of_U24this_3(),
	U3CCheckPlayerU3Ec__Iterator1_t2156344797::get_offset_of_U24current_4(),
	U3CCheckPlayerU3Ec__Iterator1_t2156344797::get_offset_of_U24disposing_5(),
	U3CCheckPlayerU3Ec__Iterator1_t2156344797::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2476 = { sizeof (U3CCreateNewGuestU3Ec__Iterator2_t4264403073), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2476[7] = 
{
	U3CCreateNewGuestU3Ec__Iterator2_t4264403073::get_offset_of_U3CurlU3E__1_0(),
	U3CCreateNewGuestU3Ec__Iterator2_t4264403073::get_offset_of_U3CformU3E__1_1(),
	U3CCreateNewGuestU3Ec__Iterator2_t4264403073::get_offset_of_U3CdownloadU3E__1_2(),
	U3CCreateNewGuestU3Ec__Iterator2_t4264403073::get_offset_of_U24this_3(),
	U3CCreateNewGuestU3Ec__Iterator2_t4264403073::get_offset_of_U24current_4(),
	U3CCreateNewGuestU3Ec__Iterator2_t4264403073::get_offset_of_U24disposing_5(),
	U3CCreateNewGuestU3Ec__Iterator2_t4264403073::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2477 = { sizeof (U3CCreateNewFBPlayerU3Ec__Iterator3_t3275366866), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2477[7] = 
{
	U3CCreateNewFBPlayerU3Ec__Iterator3_t3275366866::get_offset_of_U3CurlU3E__0_0(),
	U3CCreateNewFBPlayerU3Ec__Iterator3_t3275366866::get_offset_of_U3CformU3E__0_1(),
	U3CCreateNewFBPlayerU3Ec__Iterator3_t3275366866::get_offset_of_U3CdownloadU3E__0_2(),
	U3CCreateNewFBPlayerU3Ec__Iterator3_t3275366866::get_offset_of_U24this_3(),
	U3CCreateNewFBPlayerU3Ec__Iterator3_t3275366866::get_offset_of_U24current_4(),
	U3CCreateNewFBPlayerU3Ec__Iterator3_t3275366866::get_offset_of_U24disposing_5(),
	U3CCreateNewFBPlayerU3Ec__Iterator3_t3275366866::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2478 = { sizeof (Purchaser_t807278424), -1, sizeof(Purchaser_t807278424_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2478[16] = 
{
	Purchaser_t807278424_StaticFields::get_offset_of_m_StoreController_2(),
	Purchaser_t807278424_StaticFields::get_offset_of_m_StoreExtensionProvider_3(),
	Purchaser_t807278424_StaticFields::get_offset_of_kProductIDConsumable_4(),
	Purchaser_t807278424_StaticFields::get_offset_of_kProductIDNonConsumable_5(),
	Purchaser_t807278424_StaticFields::get_offset_of_kProductIDSubscription_6(),
	Purchaser_t807278424_StaticFields::get_offset_of_coins10000_7(),
	Purchaser_t807278424_StaticFields::get_offset_of_coins25000_8(),
	Purchaser_t807278424_StaticFields::get_offset_of_coins50000_9(),
	Purchaser_t807278424_StaticFields::get_offset_of_coins75000_10(),
	Purchaser_t807278424_StaticFields::get_offset_of_coins100000_11(),
	Purchaser_t807278424_StaticFields::get_offset_of_coins500000_12(),
	Purchaser_t807278424_StaticFields::get_offset_of_coins1000000_13(),
	Purchaser_t807278424_StaticFields::get_offset_of_coins5000000_14(),
	Purchaser_t807278424_StaticFields::get_offset_of_coins10000000_15(),
	Purchaser_t807278424_StaticFields::get_offset_of_kProductNameAppleSubscription_16(),
	Purchaser_t807278424_StaticFields::get_offset_of_kProductNameGooglePlaySubscription_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2479 = { sizeof (ScreenManager_t3517964220), -1, sizeof(ScreenManager_t3517964220_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2479[10] = 
{
	ScreenManager_t3517964220_StaticFields::get_offset_of_SceneToJumpBack_2(),
	ScreenManager_t3517964220::get_offset_of_loadingScreenObj_3(),
	ScreenManager_t3517964220::get_offset_of_settingPanel_4(),
	ScreenManager_t3517964220::get_offset_of_iapPanel_5(),
	ScreenManager_t3517964220::get_offset_of_promocodePanel_6(),
	ScreenManager_t3517964220::get_offset_of_errorPanel_7(),
	ScreenManager_t3517964220::get_offset_of_playerDetailsPanel_8(),
	ScreenManager_t3517964220::get_offset_of_iapBackBtn_9(),
	ScreenManager_t3517964220::get_offset_of_loadingSlider_10(),
	ScreenManager_t3517964220_StaticFields::get_offset_of_instance_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2480 = { sizeof (U3CLoadingNewScreenU3Ec__Iterator0_t3188382973), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2480[6] = 
{
	U3CLoadingNewScreenU3Ec__Iterator0_t3188382973::get_offset_of_sceneName_0(),
	U3CLoadingNewScreenU3Ec__Iterator0_t3188382973::get_offset_of_U3CasynccU3E__0_1(),
	U3CLoadingNewScreenU3Ec__Iterator0_t3188382973::get_offset_of_U24this_2(),
	U3CLoadingNewScreenU3Ec__Iterator0_t3188382973::get_offset_of_U24current_3(),
	U3CLoadingNewScreenU3Ec__Iterator0_t3188382973::get_offset_of_U24disposing_4(),
	U3CLoadingNewScreenU3Ec__Iterator0_t3188382973::get_offset_of_U24PC_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2481 = { sizeof (SlotGameManager_t584614006), -1, sizeof(SlotGameManager_t584614006_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2481[31] = 
{
	SlotGameManager_t584614006::get_offset_of_scatter_2(),
	SlotGameManager_t584614006::get_offset_of_wild_3(),
	SlotGameManager_t584614006::get_offset_of_bonus_4(),
	SlotGameManager_t584614006::get_offset_of_credits_5(),
	SlotGameManager_t584614006::get_offset_of_gameName_6(),
	SlotGameManager_t584614006::get_offset_of_sprites_7(),
	SlotGameManager_t584614006::get_offset_of_spriteCountForThisGame_8(),
	SlotGameManager_t584614006::get_offset_of_totalWild_9(),
	SlotGameManager_t584614006::get_offset_of_totalScatter_10(),
	SlotGameManager_t584614006::get_offset_of_totalBonus_11(),
	SlotGameManager_t584614006::get_offset_of_slotReels_12(),
	SlotGameManager_t584614006_StaticFields::get_offset_of_baseGame_13(),
	SlotGameManager_t584614006::get_offset_of_scatters_14(),
	SlotGameManager_t584614006::get_offset_of_wilds_15(),
	SlotGameManager_t584614006::get_offset_of_bonuses_16(),
	SlotGameManager_t584614006::get_offset_of_scatterReels_17(),
	SlotGameManager_t584614006::get_offset_of_wildReels_18(),
	SlotGameManager_t584614006::get_offset_of_bonusReels_19(),
	SlotGameManager_t584614006_StaticFields::get_offset_of_instance_20(),
	SlotGameManager_t584614006::get_offset_of_stopInstantly_21(),
	SlotGameManager_t584614006::get_offset_of_canStopReelsInstanty_22(),
	SlotGameManager_t584614006::get_offset_of_numberOfFreeSpins_23(),
	SlotGameManager_t584614006::get_offset_of_buttonsToDisableWhileSpinning_24(),
	SlotGameManager_t584614006::get_offset_of_spinBtnPressed_25(),
	SlotGameManager_t584614006::get_offset_of_stopBtnPressed_26(),
	SlotGameManager_t584614006::get_offset_of_baseBonusRewards_27(),
	SlotGameManager_t584614006::get_offset_of_calculateStoppingTime_28(),
	SlotGameManager_t584614006::get_offset_of_isDown_29(),
	SlotGameManager_t584614006::get_offset_of_downTime_30(),
	SlotGameManager_t584614006::get_offset_of_autoSpinning_31(),
	SlotGameManager_t584614006::get_offset_of_speedOfSlots_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2482 = { sizeof (U3CSpinSlotsU3Ec__AnonStorey2_t3788656927), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2482[2] = 
{
	U3CSpinSlotsU3Ec__AnonStorey2_t3788656927::get_offset_of_a_0(),
	U3CSpinSlotsU3Ec__AnonStorey2_t3788656927::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2483 = { sizeof (U3CSpinReelsU3Ec__Iterator0_t73513122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2483[7] = 
{
	U3CSpinReelsU3Ec__Iterator0_t73513122::get_offset_of_U24locvar0_0(),
	U3CSpinReelsU3Ec__Iterator0_t73513122::get_offset_of_U3CslotReelU3E__1_1(),
	U3CSpinReelsU3Ec__Iterator0_t73513122::get_offset_of_U24locvar1_2(),
	U3CSpinReelsU3Ec__Iterator0_t73513122::get_offset_of_U24this_3(),
	U3CSpinReelsU3Ec__Iterator0_t73513122::get_offset_of_U24current_4(),
	U3CSpinReelsU3Ec__Iterator0_t73513122::get_offset_of_U24disposing_5(),
	U3CSpinReelsU3Ec__Iterator0_t73513122::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2484 = { sizeof (U3CStopReelsU3Ec__Iterator1_t984865831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2484[7] = 
{
	U3CStopReelsU3Ec__Iterator1_t984865831::get_offset_of_U24locvar0_0(),
	U3CStopReelsU3Ec__Iterator1_t984865831::get_offset_of_U3CslotReelU3E__1_1(),
	U3CStopReelsU3Ec__Iterator1_t984865831::get_offset_of_U3CseqU3E__0_2(),
	U3CStopReelsU3Ec__Iterator1_t984865831::get_offset_of_U24this_3(),
	U3CStopReelsU3Ec__Iterator1_t984865831::get_offset_of_U24current_4(),
	U3CStopReelsU3Ec__Iterator1_t984865831::get_offset_of_U24disposing_5(),
	U3CStopReelsU3Ec__Iterator1_t984865831::get_offset_of_U24PC_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2485 = { sizeof (PromoCodePanelController_t729267270), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2485[4] = 
{
	PromoCodePanelController_t729267270::get_offset_of_promoCodeText_2(),
	PromoCodePanelController_t729267270::get_offset_of_wonBonusGO_3(),
	PromoCodePanelController_t729267270::get_offset_of_won_coinText_4(),
	PromoCodePanelController_t729267270::get_offset_of_responseText_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2486 = { sizeof (U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2486[14] = 
{
	U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223::get_offset_of_U3Chighscore_urlU3E__0_0(),
	U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223::get_offset_of_U3CpromocodeU3E__0_1(),
	U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223::get_offset_of_U3CformU3E__0_2(),
	U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223::get_offset_of_U3CdownloadU3E__0_3(),
	U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223::get_offset_of_U3CjsonU3E__1_4(),
	U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223::get_offset_of_U3CdataU3E__1_5(),
	U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223::get_offset_of_U3CresponsecodeU3E__1_6(),
	U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223::get_offset_of_U3CresponsetextU3E__1_7(),
	U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223::get_offset_of_U3Cstr_messageU3E__2_8(),
	U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223::get_offset_of_U3CdelayU3E__2_9(),
	U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223::get_offset_of_U24this_10(),
	U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223::get_offset_of_U24current_11(),
	U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223::get_offset_of_U24disposing_12(),
	U3CCallPromoCodeServiceU3Ec__Iterator0_t2382672223::get_offset_of_U24PC_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2487 = { sizeof (RadialLoading_t2588181753), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2487[1] = 
{
	RadialLoading_t2588181753::get_offset_of_radialSlider_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2488 = { sizeof (U3CStartRadialLoadU3Ec__AnonStorey0_t2579740159), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2488[2] = 
{
	U3CStartRadialLoadU3Ec__AnonStorey0_t2579740159::get_offset_of_a_0(),
	U3CStartRadialLoadU3Ec__AnonStorey0_t2579740159::get_offset_of_U24this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2489 = { sizeof (ReApplyShaders_t3938723122), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2489[2] = 
{
	ReApplyShaders_t3938723122::get_offset_of_material_2(),
	ReApplyShaders_t3938723122::get_offset_of_shader_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2490 = { sizeof (Reward_t2838753371), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2490[14] = 
{
	Reward_t2838753371::get_offset_of_reward_2(),
	Reward_t2838753371::get_offset_of_rewardMultiplier_3(),
	Reward_t2838753371::get_offset_of_bonusReward_4(),
	Reward_t2838753371::get_offset_of_twoTimes_5(),
	Reward_t2838753371::get_offset_of_threeTimes_6(),
	Reward_t2838753371::get_offset_of_fiveTimes_7(),
	Reward_t2838753371::get_offset_of_rewardNumber_8(),
	Reward_t2838753371::get_offset_of_bonusRewards_9(),
	Reward_t2838753371::get_offset_of_rewardAmount_10(),
	Reward_t2838753371::get_offset_of_rewardPos_11(),
	Reward_t2838753371::get_offset_of_threeMultipliers_12(),
	Reward_t2838753371::get_offset_of_twoMultipliers_13(),
	Reward_t2838753371::get_offset_of_originalImage_14(),
	Reward_t2838753371::get_offset_of_rewardTextPosRef_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2491 = { sizeof (U3CGiveRewardsU3Ec__AnonStorey0_t2598583867), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2491[4] = 
{
	U3CGiveRewardsU3Ec__AnonStorey0_t2598583867::get_offset_of_a_0(),
	U3CGiveRewardsU3Ec__AnonStorey0_t2598583867::get_offset_of_amntWon_1(),
	U3CGiveRewardsU3Ec__AnonStorey0_t2598583867::get_offset_of_newAmount_2(),
	U3CGiveRewardsU3Ec__AnonStorey0_t2598583867::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2492 = { sizeof (U3CGiveRewardsU3Ec__AnonStorey1_t4164667808), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2492[3] = 
{
	U3CGiveRewardsU3Ec__AnonStorey1_t4164667808::get_offset_of_a_0(),
	U3CGiveRewardsU3Ec__AnonStorey1_t4164667808::get_offset_of_amntWon_1(),
	U3CGiveRewardsU3Ec__AnonStorey1_t4164667808::get_offset_of_newAmount_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2493 = { sizeof (Rewards_t564064347)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2493[3] = 
{
	Rewards_t564064347::get_offset_of_value___1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2494 = { sizeof (SettingsPanelController_t3191827294), -1, sizeof(SettingsPanelController_t3191827294_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2494[8] = 
{
	SettingsPanelController_t3191827294::get_offset_of_musicbutton_2(),
	SettingsPanelController_t3191827294::get_offset_of_soundOffImage_3(),
	SettingsPanelController_t3191827294::get_offset_of_soundOnImage_4(),
	SettingsPanelController_t3191827294::get_offset_of_colorfade_5(),
	SettingsPanelController_t3191827294::get_offset_of_logoutButton_6(),
	SettingsPanelController_t3191827294::get_offset_of_promocodeButton_7(),
	SettingsPanelController_t3191827294::get_offset_of_promobuttonInitPos_8(),
	SettingsPanelController_t3191827294_StaticFields::get_offset_of_instance_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2495 = { sizeof (SlotReel_t1924038889), -1, sizeof(SlotReel_t1924038889_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2495[13] = 
{
	SlotReel_t1924038889::get_offset_of_slots_2(),
	SlotReel_t1924038889::get_offset_of_freeImages_3(),
	SlotReel_t1924038889::get_offset_of_visibleSlots_4(),
	SlotReel_t1924038889::get_offset_of_spinning_5(),
	SlotReel_t1924038889::get_offset_of_speed_6(),
	SlotReel_t1924038889::get_offset_of_startingYposition_7(),
	SlotReel_t1924038889::get_offset_of_reelNumber_8(),
	SlotReel_t1924038889::get_offset_of_positionC_9(),
	SlotReel_t1924038889::get_offset_of_refGO_10(),
	SlotReel_t1924038889::get_offset_of_testRoll_11(),
	SlotReel_t1924038889_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_12(),
	SlotReel_t1924038889_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_13(),
	SlotReel_t1924038889_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2496 = { sizeof (U3CStopThisReelU3Ec__Iterator0_t4106801625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2496[4] = 
{
	U3CStopThisReelU3Ec__Iterator0_t4106801625::get_offset_of_U24this_0(),
	U3CStopThisReelU3Ec__Iterator0_t4106801625::get_offset_of_U24current_1(),
	U3CStopThisReelU3Ec__Iterator0_t4106801625::get_offset_of_U24disposing_2(),
	U3CStopThisReelU3Ec__Iterator0_t4106801625::get_offset_of_U24PC_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2497 = { sizeof (SoundManager_t2102329059), -1, sizeof(SoundManager_t2102329059_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2497[4] = 
{
	SoundManager_t2102329059::get_offset_of_backgroundMusic_2(),
	SoundManager_t2102329059::get_offset_of_gamesBackgroundMusic_3(),
	SoundManager_t2102329059::get_offset_of_musicFlag_4(),
	SoundManager_t2102329059_StaticFields::get_offset_of_instance_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2498 = { sizeof (SpinWheelController_t1228200680), -1, sizeof(SpinWheelController_t1228200680_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2498[15] = 
{
	SpinWheelController_t1228200680_StaticFields::get_offset_of_instance_2(),
	SpinWheelController_t1228200680::get_offset_of_wheel_3(),
	SpinWheelController_t1228200680::get_offset_of_spinWheel_4(),
	SpinWheelController_t1228200680::get_offset_of_pointer_5(),
	SpinWheelController_t1228200680::get_offset_of_spinButton_6(),
	SpinWheelController_t1228200680::get_offset_of_collectButton_7(),
	SpinWheelController_t1228200680::get_offset_of_winningsText_8(),
	SpinWheelController_t1228200680::get_offset_of_winningTextBG_9(),
	SpinWheelController_t1228200680::get_offset_of_title_10(),
	SpinWheelController_t1228200680::get_offset_of_stoppingValues_11(),
	SpinWheelController_t1228200680::get_offset_of_winnings_12(),
	SpinWheelController_t1228200680::get_offset_of_segments_13(),
	SpinWheelController_t1228200680::get_offset_of_numbers_14(),
	SpinWheelController_t1228200680_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_15(),
	SpinWheelController_t1228200680_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2499 = { sizeof (U3CSpinButtonClickedU3Ec__AnonStorey0_t1647695004), -1, sizeof(U3CSpinButtonClickedU3Ec__AnonStorey0_t1647695004_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2499[4] = 
{
	U3CSpinButtonClickedU3Ec__AnonStorey0_t1647695004::get_offset_of_stoppingAngle_0(),
	U3CSpinButtonClickedU3Ec__AnonStorey0_t1647695004::get_offset_of_ran_1(),
	U3CSpinButtonClickedU3Ec__AnonStorey0_t1647695004::get_offset_of_U24this_2(),
	U3CSpinButtonClickedU3Ec__AnonStorey0_t1647695004_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
